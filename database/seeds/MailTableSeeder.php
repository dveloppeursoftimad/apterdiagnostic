<?php

use Illuminate\Database\Seeder;
use App\Type;
class MailTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $type1 = Type::firstOrCreate(array('name' => 'climage-formation', 'description' => "Climage formation"));
        $type2 = Type::firstOrCreate(array('name' => 'climage-reunion', 'description' => "Climage réunion"));
        $type3 = Type::firstOrCreate(array('name' => 'climage-transformation', 'description' => "Climage transformation"));
        $type4 = Type::firstOrCreate(array('name' => 'feedback');
        for($i = 1;$i<5;$i++){
            if(${'type'.$i}->name == 'feedback'){
                $id = ${'type'.$i}->id;
                break;
            }
        }
        \DB::table('mail')->delete();
        
        \DB::table('mail')->insert(array (
            0 => 
            array (

                'type' => 'invitation repondant',
                'mail' => '<div><!--block-->%REP_CIVILITE%,<br><br></div><div><!--block-->Dans le cadre du développement professionnel de %BEN_FIRSTNAME% %BEN_LASTNAME%, nous réalisons une&nbsp;<strong>évaluation 360°Feed Back Leadership et Management.<br></strong><br></div><div><!--block-->Cette démarche a pour objectif d\'offrir un effet de levier positif et pertinent sur les collaborateurs qui en bénéficient. Elle est une opportunité de dresser un état des lieux autant en termes de compétences que de talents managériaux. Vous avez été choisi par %BEN_FIRSTNAME% pour participer à son évaluation et votre participation est importante à ses yeux.<br><br></div><div><!--block-->Notre déontologie ainsi que le process de collecte des données mis en place vous garantissent que&nbsp;<strong>vos réponses sont tout à fait confidentielles.<br></strong><br></div><div><!--block--><strong>Pour accéder en toute sécurité au questionnaire, vous trouverez ci-dessous les éléments de connexion.<br></strong><br></div><div><!--block-->Lien :&nbsp;<strong>%SITE_LIEN%<br></strong><br></div><div><!--block-->Code :&nbsp;<strong>%SITE_CODE%<br><br></strong><br></div><div><!--block-->Il est possible que les pare-feu de votre système de sécurité, bloquent l\'accès à la page du 360°. Nous vous invitons à autoriser la page&nbsp;<strong>%SITE_LIEN%&nbsp;</strong>dans votre système, de sorte que vous puissiez vous connecter plus facilement.<br><br></div><div><!--block-->Nous restons à votre disposition pour toute information complémentaire.<br><br></div><div><!--block-->Bien cordialement<br><br></div><div><!--block-->Laurence Vandeventer<br><br></div><div><!--block-->Associée Apter France&nbsp;<br><br></div><div><!--block-->laurence@apter-france.com<br><br></div><div><!--block-->laurence@apter-maroc.com<br><br></div><div><!--block-->www.aptersolutions.com&nbsp;</div>',
                'subject' => "360 Feedback LM",
                'type_id' => 4,
                'language' => 'fr',
                'active' => 1,
            ),
            1 => 
            array (

                'type' => 'relance',
                'mail' => '<div><!--block-->%REP_CIVILITE%,<br><br></div><div><!--block-->Nous vous avons récemment contacté dans le cadre de l\'évaluation 360 Feed Back Leadership et Management de %BEN_FIRSTNAME% %BEN_LASTNAME%, mais sauf erreur de notre part, n\'avons pas reçu vos réponses. Votre participation est importante pour %BEN_FIRSTNAME%, qui vous a invité.<br><br></div><div><!--block-->Vos réponses sont tout à fait confidentielles et le regard que vous porterez, permettra de cibler précisément les axes de progression sur lesquels travailler. Vous pouvez cliquer sur le lien ci-dessous pour commencer ou poursuivre cette évaluation.<br><br></div><div><!--block-->Lien :&nbsp;<strong>%SITE_LIEN%<br></strong><br></div><div><!--block-->Code :&nbsp;<strong>%SITE_CODE%<br><br></strong><br></div><div><!--block-->Groupe :<strong>&nbsp;%GROUPE%</strong>&nbsp;-<strong>&nbsp;%GROUPE_ABREV%<br></strong><br></div><div><!--block-->Il est possible que les pare-feu de votre système de sécurité, bloquent l\'accès à la page du 360°. Nous vous invitons à autoriser la page&nbsp;<strong>%SITE_LIEN%&nbsp;</strong>dans votre système, de sorte que vous puissiez vous connecter plus facilement.Nous vous remercions de votre attention et de votre disponibilité pour répondre à cette évaluation 360.Nous restons à votre disposition pour toute information complémentaire.<br><br></div><div><!--block--><br>Bien cordialement<br><br></div><div><!--block--><br>%CONS_FIRSTNAME% %CONS_LASTNAME%<br><br></div><div><!--block--><br>www.aptersolutions.com<br><br></div><div><!--block-->laurence@apter-france.com<br><br></div><div><!--block-->laurence@apter-maroc.com</div>',
                'subject' => 'Mail d\'invitation',
                'type_id' => $id,
                'language' => 'fr',
                'active' => 1,
            ),
            2 => 
            array (

                'type' => 'invitation repondant',
                'mail' => '<div><!--block-->%REP_CIVILITE%<br><br></div><div><!--block-->As part of the&nbsp;<strong>%BEN_FIRSTNAME% %BEN_LASTNAME%</strong>&nbsp;professional development, we perform a&nbsp;<strong>360° Feed Back Leadership and Management Assessment.<br></strong><br></div><div><!--block-->This approach aims to provide&nbsp;<strong>a positive and relevant leverage effect</strong>&nbsp;for the employees who benefit from it. It is an opportunity to draw up an inventory of places as much in terms of competencies as managerial talents. You have been selected by %BEN_FIRSTNAME% to participate in this assessment and your commitment is important.<br><br></div><div><!--block-->Our code of ethics as well as the data collection process ensure that&nbsp;<strong>your answers are completely confidential.<br></strong><br></div><div><!--block--><strong>To access the questionnaire safely, you will find below the connection elements.<br></strong><br></div><div><!--block--><br><br></div><div><!--block--><strong>Link : %SITE_LIEN%<br></strong><br></div><div><!--block--><strong>Code :%SITE_CODE%<br></strong><br></div><div><!--block-->The<br>&nbsp;firewalls in your security system may block access to the 360 ​° page.&nbsp;<br>We invite you to allow the page %SITE_LIEN% in your system, so that you&nbsp;<br>can connect more easily.<br><br></div><div><!--block-->We remain at your disposal for any further information.<br><br></div><div><!--block-->Best regards<br><br></div><div><!--block-->Laurence Vandeventer<br><br></div><div><!--block-->Partner Apter France&nbsp;<br><br></div><div><!--block-->laurence@apter-france.com<br><br></div><div><!--block-->laurence@apter-maroc.com<br><br></div><div><!--block-->www.aptersolutions.com<br><br></div>',
                'subject' => "360 Feedback LM",
                'type_id' => $id,
                'language' => 'en',
                'active' => 1,
            ),
            3 => 
            array (
                'type' => 'relance',
            'mail' => '<div><!--block-->%REP_CIVILITE%,</div><div><!--block--><br></div><div><!--block-->We<br>&nbsp;recently contacted you as part of the 360 Feedback Back Leadership and&nbsp;<br>Management Assessment of %BEN_FIRSTNAME% %BEN_LASTNAME%, but I&nbsp;<br>understand that we have not received your answers. Your participation is important for %BEN_FIRSTNAME%, who invited you.<br><br>Your<br>&nbsp;answers are completely confidential and the look you will take, will&nbsp;<br>precisely target the areas of progress on which to work.<br><br>You can click on the link below to start or continue this assessment.</div><div><!--block-->Lien :&nbsp;<strong>%SITE_LIEN%<br></strong><br></div><div><!--block-->Code :&nbsp;<strong>%SITE_CODE%<br></strong><br></div><div><!--block-->Nom :<strong>&nbsp;%REP_LASTNAME%<br></strong><br></div><div><!--block-->Prénom(s) :<strong>&nbsp;%REP_FIRSTNAME%<br></strong><br></div><div><!--block-->Groupe :<strong>&nbsp;%GROUPE_ABREV%<br></strong><br></div><div><!--block-->The firewalls in your security system may block access to the 360 ° page. We invite you to allow the page %SITE_LIEN% in your system, so that you can connect more easily.<br><br>Thank you for your attention and availability in responding to this assessment 360.<br><br>We remain at your disposal for any further information.<br><br><br>Best regards<br><br></div><div><!--block--><br><br></div><div><!--block-->Laurence Vandeventer<br><br></div><div><!--block-->Partner ApterSolutions<br><br></div><div><!--block-->laurence@apter-france.com<br><br></div>',
                'subject' => "360 Feedback LM",
                'type_id' => $id,
                'language' => 'en',
                'active' => 1,
            ),
            4 => 
            array (
                'type' => 'invitation beneficiaire',
            'mail' => '<div><!--block-->%BEN_FIRSTNAME%,<br><br></div><div><!--block-->Dans le cadre du coaching que nous faisons ensemble, tu as accepté de réaliser un&nbsp;<strong>360 Feed-Back Leadership et Management</strong>.&nbsp;<br><br></div><div><!--block-->Par expérience, comme nous l\'avons vu ensemble, cette démarche offre un&nbsp;<strong>effet de levier positif et pertinent sur le développement managérial</strong>&nbsp;des personnes qui en bénéficient.<br><br></div><div><!--block-->Concrètement, elle vise à recueillir une évaluation de plusieurs sources (toi bien sûr, ainsi que les participants qui figurent sur le fichier que tu m\'as envoyé). Ce processus te donne ainsi l’opportunité, d\'une part, de porter un regard différent sur tes compétences managériales et tes talents de leadership et d\'autre part, voire surtout, de connaitre la perception de ceux qui t\'entourent.Le rapport 360° feedback Leadership et Management qui te sera transmis te permettra d’identifier des éléments clés et d’élaborer un plan de progrès pertinent pour atteindre ton objectif. Il sera remis uniquement à<br>&nbsp;toi-même.&nbsp;<strong>La garantie de confidentialité&nbsp;</strong>est une exigence de qualité afin d’obtenir des réponses non biaisées et un rapport avec des résultats tout à fait pertinents.Nous debrieferons ensemble les résultats.<br><br></div><div><!--block-->Je suis à ta disposition pour échanger plus avant avec toi et faire de ce moment, une réelle source de développement.<br><br></div><div><!--block--><strong>Pour accéder en toute sécurité au questionnaire, tu trouveras ci-dessous les éléments de connexion.<br></strong><br></div><div><!--block-->Lien :&nbsp;<strong>%SITE_LIEN%<br></strong><br></div><div><!--block-->Code :&nbsp;<strong>%SITE_CODE%<br></strong><br></div><div><!--block-->Il&nbsp; est possible que les pare-feu de votre système de sécurité, bloquent&nbsp;<br>l\'accès à la page du 360°. Je t\'invite à autoriser la page&nbsp;<strong>%SITE_LIEN%&nbsp;</strong>dans ton système, de sorte que tu puisses te connecter plus facilement.<br><br>Bien cordialement<br><br></div><div><!--block-->Laurence Vandeventer<br><br></div><div><!--block-->Apter Solutions<br><br></div><div><!--block-->laurence@apter-france.com<br><br></div><div><!--block-->www.aptersolutions.com</div>',
                'subject' => "360 Feedback LM",
                'type_id' => $id,
                'language' => 'fr',
                'active' => 1,
            ),
        ));
        
        
    }
}
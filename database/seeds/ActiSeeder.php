<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;

class ActiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TYPE
        $type1 = Type::firstOrCreate(['name' => 'acti','description' => '360 Diagnostic']);

        // PAGES
        $page1 = Page::firstOrCreate(['title_fr' => 'VALEURS', "type_id" => $type1->id]);
        $page2 = Page::firstOrCreate(['title_fr' => 'EXERCICE DE L\'AUTORITÉ', "type_id" => $type1->id]);
        $page3 = Page::firstOrCreate(['title_fr' => 'MÉTHODES DE TRAVAIL', "type_id" => $type1->id]);
        $page4 = Page::firstOrCreate(['title_fr' => 'COMPORTEMENTS SOUS PRESSION', "type_id" => $type1->id]);
        $page5 = Page::firstOrCreate(['title_fr' => 'INFORMATION ET COMMUNICATION', "type_id" => $type1->id]);
        $page6 = Page::firstOrCreate(['title_fr' => 'INTELLIGENCE ÉMOTIONNELLE', "type_id" => $type1->id]);
        $page7 = Page::firstOrCreate(array('title_fr' => 'QUESTIONS OUVERTES DE FIN DE QUESTIONNAIRE', "type_id" => $type1->id));

        // SOUS PAGES
        $sp1 = Page::firstOrCreate(array('title_fr' => 'Ce qui est important pour Lui / Elle au travail','title_fr_ben' => 'Ce qui est important pour moi au travail', "page_id" => $page1->id, "type_id" => $type1->id));
        $sp2 = Page::firstOrCreate(array('title_fr' => 'Distribution du travail et passage de consignes','title_fr_ben' => 'Distribution du travail et passage de consignes', "page_id" => $page2->id, "type_id" => $type1->id));
        $sp3 = Page::firstOrCreate(array('title_fr' => 'Le pilotage des activités','title_fr_ben' => 'Le pilotage des activités', "page_id" => $page3->id, "type_id" => $type1->id));
        $sp4 = Page::firstOrCreate(array('title_fr' => 'le pilotage des ressources humaines','title_fr_ben' => 'le pilotage des ressources humaines', "page_id" => $page3->id, "type_id" => $type1->id));
        $sp5 = Page::firstOrCreate(array('title_fr' => 'Situations de changement ou de crise','title_fr_ben' => 'Situations de changement ou de crise', "page_id" => $page4->id, "type_id" => $type1->id));
        $sp9 = Page::firstOrCreate(array("page_id" => $page5->id, "type_id" => $type1->id));
        $sp6 = Page::firstOrCreate(array('title_fr' => 'Lorsque l\'ambiance est bonne, il / elle peut être perçu/e comme :','title_fr_ben' => 'Lorsque l\'ambiance est bonne, il / elle peut être perçu/e comme :', "page_id" => $page6->id, "type_id" => $type1->id));
        $sp7 = Page::firstOrCreate(array('title_fr' => 'Lorsque l\'ambiance est mauvaise, il / elle peut être perçu/e comme :','title_fr_ben' => 'Lorsque l\'ambiance est mauvaise, il / elle peut être perçu/e comme :', "page_id" => $page6->id, "type_id" => $type1->id));
        $sp8 = Page::firstOrCreate(array("page_id" => $page7->id, "type_id" => $type1->id));

        // QUESTIONS PAGE 1 - 1
        $qb1 = Question::firstOrCreate(['question_fr' => "Il / Elle m’attache à donner du sens à nos activités",'question_fr_ben' => "Je m’attache à donner du sens à nos activités",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb2 = Question::firstOrCreate(['question_fr' => "Il / Elle engage son équipe à aller au-delà de ce qui est attendu",'question_fr_ben' => "J’engage mon équipe à aller au-delà de ce qui est attendu",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb3 = Question::firstOrCreate(['question_fr' => "Il / Elle incite ses collaborateurs à être inventifs, originaux, créatifs",'question_fr_ben' => "J’incite mes collaborateurs à être inventifs, originaux, créatifs",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb4 = Question::firstOrCreate(['question_fr' => "Il / Elle incite ses collaborateurs à s’intéresser à ce qu’ils font",'question_fr_ben' => "J’incite mes collaborateurs à s’intéresser à ce qu’ils font",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb5 = Question::firstOrCreate(['question_fr' => "Il / Elle encourage ses collaborateurs à se dépasser",'question_fr_ben' => "J’encourage mes collaborateurs à se dépasser",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb6 = Question::firstOrCreate(['question_fr' => "Il / Elle montre de la considération à chacun de ses collaborateurs",'question_fr_ben' => "Je montre de la considération à chacun de mes collaborateurs",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb7 = Question::firstOrCreate(['question_fr' => "Il / Elle encourage la performance collective",'question_fr_ben' => "J’encourage la performance collective",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb8 = Question::firstOrCreate(['question_fr' => "Il / Elle invite l’équipe à entretenir de bonnes relations en son sein",'question_fr_ben' => "J’invite l’équipe à entretenir de bonnes relations en son sein",'page_id' => $sp1->id,'type_id' => $type1->id,"type" => 'radio']);     

	    // QUESTIONS PAGE 2 - 1
	    $qb9 = Question::firstOrCreate(['question_fr' => "Ses directives sont précises et structurées",'question_fr_ben' => "Mes directives sont précises et structurées",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb10 = Question::firstOrCreate(['question_fr' => "Ses consignes incitent l’équipe à faire bouger les choses",'question_fr_ben' => "Mes consignes incitent l’équipe à faire bouger les choses",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb11 = Question::firstOrCreate(['question_fr' => " Il / Elle accorde le droit à l’erreur pour favoriser la prise d’initiatives ou de risques",'question_fr_ben' => "J’accorde le droit à l’erreur pour favoriser la prise d’initiatives ou de risques",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb12 = Question::firstOrCreate(['question_fr' => "Il / Elle s’attache à confier des tâches intéressantes ou excitantes",'question_fr_ben' => "Je m’attache à confier des tâches intéressantes ou excitantes",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb13 = Question::firstOrCreate(['question_fr' => "Il / Elle délègue des missions qui responsabilisent ses collaborateurs",'question_fr_ben' => "Je délègue des missions qui responsabilisent mes collaborateurs",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb14 = Question::firstOrCreate(['question_fr' => "Il / Elle valorise mes collaborateurs méritants auprès de sa hiérarchie",'question_fr_ben' => "Je valorise mes collaborateurs méritants auprès de ma hiérarchie",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb15 = Question::firstOrCreate(['question_fr' => "Ses objectifs engagent l’équipe à se mobiliser collectivement",'question_fr_ben' => "Mes objectifs engagent l’équipe à se mobiliser collectivement",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb16 = Question::firstOrCreate(['question_fr' => "Il / Elle adapte ses demandes à un niveau de stress supportable pour autrui",'question_fr_ben' => "J’adapte mes demandes à un niveau de stress supportable pour autrui",'page_id' => $sp2->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 3 - 1
	    $qb17 = Question::firstOrCreate(['question_fr' => "Il / Elle fait des points d’avancement réguliers",'question_fr_ben' => "Je fais des points d’avancement réguliers",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb18 = Question::firstOrCreate(['question_fr' => "Il / Elle adapte les priorités quand il y a des changements à opérer",'question_fr_ben' => " J’adapte les priorités quand il y a des changements à opérer",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb19 = Question::firstOrCreate(['question_fr' => "Il / Elle sait trouver des solutions qui sortent de l’ordinaire ou du cadre",'question_fr_ben' => "Je sais trouver des solutions qui sortent de l’ordinaire ou du cadre",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb20 = Question::firstOrCreate(['question_fr' => "Il / Elle demande un travail de qualité et conforme à ce qui est attendu",'question_fr_ben' => "Je demande un travail de qualité et conforme à ce qui est attendu",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb21 = Question::firstOrCreate(['question_fr' => "Il / Elle anticipe, organise, planifie efficacement la mise en œuvre du travail",'question_fr_ben' => "J’anticipe, j’organise, je planifie efficacement la mise en œuvre du travail",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb22 = Question::firstOrCreate(['question_fr' => " Si la fin le justifie, il / elle sait prendre des décisions tranchées",'question_fr_ben' => "Si la fin le justifie, je sais prendre des décisions tranchées",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb23 = Question::firstOrCreate(['question_fr' => "Ses méthodes de travail sont inhabituelles, voire non conventionnelles",'question_fr_ben' => "Mes méthodes de travail sont inhabituelles, voire non conventionnelles",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb24 = Question::firstOrCreate(['question_fr' => "Il / Elle implique son équipe aux décisions pour les motiver dans son travail",'question_fr_ben' => "J’implique mon équipe aux décisions pour les motiver dans son travail",'page_id' => $sp3->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 3 - 2
	    $qb25 = Question::firstOrCreate(['question_fr' => "Il / Elle donne les moyens aux collaborateurs de réussir (formation, outils…)",'question_fr_ben' => "Je donne les moyens aux collaborateurs de réussir (formation, outils…)",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb26 = Question::firstOrCreate(['question_fr' => "Il / Elle sait être aimable pour donner envie de travailler avec lui / elle",'question_fr_ben' => "Je sais être aimable pour donner envie de travailler avec moi",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb27 = Question::firstOrCreate(['question_fr' => "Il / Elle organise un débriefing collectif à l’issue de chaque mission importante",'question_fr_ben' => "J’organise un débriefing collectif à l’issue de chaque mission importante",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb28 = Question::firstOrCreate(['question_fr' => "Il / Elle est attentif/ve au bien-être au travail de ses collaborateurs",'question_fr_ben' => "Je suis attentif au bien-être au travail de mes collaborateurs",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb29 = Question::firstOrCreate(['question_fr' => "Il / Elle pratique le feedback pour améliorer les performances",'question_fr_ben' => "Je pratique le feedback pour améliorer les performances",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb30 = Question::firstOrCreate(['question_fr' => "Il / Elle sait se faire humainement apprécier de ses collaborateurs",'question_fr_ben' => "Je sais me faire humainement apprécier de mes collaborateurs",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb31 = Question::firstOrCreate(['question_fr' => "Il / Elle aide les autres à réussir ou à réussir en équipe",'question_fr_ben' => "J’aide les autres à réussir ou à réussir en équipe",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb32 = Question::firstOrCreate(['question_fr' => "Il / Elle sait être à l’écoute et attentive aux autres",'question_fr_ben' => "Je sais être à l’écoute et attentif aux autres",'page_id' => $sp4->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 4 - 1
	    $qb33 = Question::firstOrCreate(['question_fr' => "Il / Elle reste organisé/e, même sous pression",'question_fr_ben' => "Je reste organisé, même sous pression",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb34 = Question::firstOrCreate(['question_fr' => "Il / Elle conduit ses missions à leurs termes quoiqu’il arrive",'question_fr_ben' => "Je conduis mes missions à leurs termes quoiqu’il arrive",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb35 = Question::firstOrCreate(['question_fr' => "Il / Elle dédramatise les enjeux et les conséquences",'question_fr_ben' => "Je dédramatise les enjeux et les conséquences",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb36 = Question::firstOrCreate(['question_fr' => "Il / Elle protège l’équipe des pressions pour qu’elle donne le meilleur d’elle-même",'question_fr_ben' => "Je protège l’équipe des pressions pour qu’elle donne le meilleur d’elle-même",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb37 = Question::firstOrCreate(['question_fr' => "Il / Elle reste maître de lui / d'elle en situations difficiles",'question_fr_ben' => "Je reste maître de moi en situations difficiles",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb38 = Question::firstOrCreate(['question_fr' => "Il / Elle attend de ses collaborateurs qu’ils soient conciliants avec lui / elle",'question_fr_ben' => "J’attends de mes collaborateurs qu’ils soient conciliants avec moi",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb39 = Question::firstOrCreate(['question_fr' => " Il / Elle incite ses collaborateurs à s’entraider, à se battre ensemble",'question_fr_ben' => "J’incite mes collaborateurs à s’entraider, à se battre ensemble",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb40 = Question::firstOrCreate(['question_fr' => "Il / Elle soutient moralement ceux qui ont des difficultés à réussir",'question_fr_ben' => "Je soutiens moralement ceux qui ont des difficultés à réussir",'page_id' => $sp5->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 5 - 1
	    $qb41 = Question::firstOrCreate(['question_fr' => "Il / Elle relaie les bonnes informations aux bons moments et aux bons niveaux",'question_fr_ben' => "Je relaie les bonnes informations aux bons moments et aux bons niveaux",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb42 = Question::firstOrCreate(['question_fr' => "Il / Elle sait dire non et s’opposer si l’enjeu est important",'question_fr_ben' => "Je sais dire non et m’opposer si l’enjeu est important",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb43 = Question::firstOrCreate(['question_fr' => "Il / Elle favorise la libre expression, le débat et la discussion",'question_fr_ben' => "Je favorise la libre expression, le débat et la discussion",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb44 = Question::firstOrCreate(['question_fr' => "Il / Elle veille à ce que l’équipe dispose de toutes les informations nécessaire à son travail",'question_fr_ben' => "Je veille à ce que l’équipe dispose de toutes les informations nécessaire à son travail",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb45 = Question::firstOrCreate(['question_fr' => "Il / Elle est à l’aise pour s’exprimer face à un public",'question_fr_ben' => "Je suis à l’aise pour m’exprimer face à un public",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb46 = Question::firstOrCreate(['question_fr' => " Il / Elle attire la sympathie et est à l’aise dans sa relation à autrui",'question_fr_ben' => "J’attire la sympathie et suis à l’aise dans ma relation à autrui",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb47 = Question::firstOrCreate(['question_fr' => "Ses réunions sont participatives et l'équipe est collectivement efficace",'question_fr_ben' => "Mes réunions sont participatives et nous sommes collectivement efficaces",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb48 = Question::firstOrCreate(['question_fr' => "Il / Elle organise des moments de convivialité avec son équipe",'question_fr_ben' => "J’organise des moments de convivialité avec mon équipe",'page_id' => $sp9->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 6 - 1
	    $qb49 = Question::firstOrCreate(['question_fr' => "rassurant/e, méthodique, porteur/se de sens…",'question_fr_ben' => "rassurant, méthodique, porteur de sens…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb50 = Question::firstOrCreate(['question_fr' => "ou au contraire, irréfléchi/e, inconsistant/e, moralisateur/trice…",'question_fr_ben' => "ou au contraire, irréfléchi, inconsistant, moralisateur…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb51 = Question::firstOrCreate(['question_fr' => 'imperturbable, "zen", résistant/e aux pressions…','question_fr_ben' => 'imperturbable, "zen", résistant aux pressions…','page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb52 = Question::firstOrCreate(['question_fr' => 'ou au contraire, blasé/e, indifférent/e, désengagé/e, "j\'menfoutiste"…','question_fr_ben' => 'ou au contraire, blasé, indifférent, désengagé, "j\'menfoutiste"…','page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb53 = Question::firstOrCreate(['question_fr' => "Il / Elle est à l’aise pour s’exprimer face à un public",'question_fr_ben' => 'ou au contraire, blasé/e, indifférent/e, désengagé/e, "j\'menfoutiste"…','page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb54 = Question::firstOrCreate(['question_fr' => "Inventif/ve, étonnant/e, humoristique…",'question_fr_ben' => "Inventif, étonnant, humoristique…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb55 = Question::firstOrCreate(['question_fr' => "ou au contraire, ironique, blessant/e, déplacé/e, cynique…",'question_fr_ben' => "ou au contraire, ironique, blessant, déplacé, cynique…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb56 = Question::firstOrCreate(['question_fr' => "Enthousiaste, passionné/e, impliqué/e…",'question_fr_ben' => "Enthousiaste, passionné, impliqué…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb57 = Question::firstOrCreate(['question_fr' => "ou au contraire, impulsif/ve, étourdi/e, inconstant/e…",'question_fr_ben' => "ou au contraire, impulsif, étourdi, inconstant…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb58 = Question::firstOrCreate(['question_fr' => "Fiable, solide, fort/e, performant/e…",'question_fr_ben' => "Fiable, solide, fort, performant…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb59 = Question::firstOrCreate(['question_fr' => "ou au contraire, arrogant/e, supérieur/e, méprisant/e…",'question_fr_ben' => "ou au contraire, arrogant, supérieur, méprisant…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb60 = Question::firstOrCreate(['question_fr' => "Aimable, sympathique, séduisant/e…",'question_fr_ben' => "Aimable, sympathique, séduisant…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb61 = Question::firstOrCreate(['question_fr' => "ou au contraire, séducteur/trice, pressant/e, simulateur/trice…",'question_fr_ben' => "ou au contraire, séducteur, pressant, simulateur…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb62 = Question::firstOrCreate(['question_fr' => "Dévoué/e, aidant/E, disponible, supporter…",'question_fr_ben' => "Dévoué, aidant, disponible, supporter…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb63 = Question::firstOrCreate(['question_fr' => "ou au contraire, hypocrite, calculateur/trice, embarrassant/e… ",'question_fr_ben' => "ou au contraire, hypocrite, calculateur, embarrassant… ",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb64 = Question::firstOrCreate(['question_fr' => "Attentionné/e, compréhensif/ve, serviable…",'question_fr_ben' => "Attentionné, compréhensif, serviable…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb65 = Question::firstOrCreate(['question_fr' => "ou au contraire, envahissant/e, débordant/e, oppressant/e…",'question_fr_ben' => "ou au contraire, envahissant, débordant, oppressant…",'page_id' => $sp6->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONS PAGE 6 - 2
	    $qb66 = Question::firstOrCreate(['question_fr' => "Vigilant/e, attentif/ve, préparé/e…",'question_fr_ben' => "Vigilant, attentif, préparé…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb67 = Question::firstOrCreate(['question_fr' => "ou au contraire, fébrile, crispé/e…",'question_fr_ben' => "ou au contraire, fébrile, crispé…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb68 = Question::firstOrCreate(['question_fr' => ' Combatif/ve, tenace, persévérant/e…','question_fr_ben' => 'Combatif, tenace, persévérant…','page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb69 = Question::firstOrCreate(['question_fr' => 'ou au contraire, agressif/ve, entêté/e, buté/e…','question_fr_ben' => 'ou au contraire, agressif, entêté, buté…','page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb70 = Question::firstOrCreate(['question_fr' => "Stimulant/e, critique (mais constructif/ve)…",'question_fr_ben' => 'Stimulant, critique (mais constructif)…','page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb71 = Question::firstOrCreate(['question_fr' => "ou au contraire, hostile, boudeur/se, bloqué/e…",'question_fr_ben' => "ou au contraire, hostile, boudeur, bloqué…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb72 = Question::firstOrCreate(['question_fr' => "Volontaire, disponible…",'question_fr_ben' => "Volontaire, disponible…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb73 = Question::firstOrCreate(['question_fr' => "ou au contraire, apathique, procrastinant/e, désengagé/e…",'question_fr_ben' => "ou au contraire, apathique, procrastinant, désengagé…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb74 = Question::firstOrCreate(['question_fr' => "Courageux/se, déterminé/e, valeureux/se…",'question_fr_ben' => "Courageux, déterminé, valeureux…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb75 = Question::firstOrCreate(['question_fr' => "ou au contraire, soumis/e, laxiste, abattu/e, indigne…",'question_fr_ben' => "ou au contraire, soumis, laxiste, abattu, indigne…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb76 = Question::firstOrCreate(['question_fr' => " Affirmé/e, confiant/e…",'question_fr_ben' => "Affirmé, confiant…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb77 = Question::firstOrCreate(['question_fr' => "ou au contraire, aigri/e, amer/amère, caliméro…",'question_fr_ben' => "ou au contraire, aigri, amer, caliméro…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb78 = Question::firstOrCreate(['question_fr' => "Honorable, respectable, loyal/e, estimable…",'question_fr_ben' => "Honorable, respectable, loyal, estimable…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb79 = Question::firstOrCreate(['question_fr' => "ou au contraire, lâche, déloyal/e, méprisable…",'question_fr_ben' => "ou au contraire, lâche, déloyal, méprisable…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb80 = Question::firstOrCreate(['question_fr' => "Sincère, vrai/e…",'question_fr_ben' => "Sincère, vrai…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    $qb81 = Question::firstOrCreate(['question_fr' => "ou au contraire, plaintif/ve, misérabiliste, complaignant/e…",'question_fr_ben' => "ou au contraire, plaintif, misérabiliste, complaignant…",'page_id' => $sp7->id,'type_id' => $type1->id,"type" => 'radio']);

	    // QUESTIONNAIRE PAGE 8
	    $qb82 = Question::firstOrCreate([
	      'question_fr' => "Que conseilleriez-vous à cette personne de changer ?",
	      'question_fr_ben' => "Que conseilleriez-vous à cette personne de changer ?",
	      'page_id' => $sp8->id,
	      'type_id' => $type1->id,
	      "type" => 'text'
	    ]);

	    $qb83 = Question::firstOrCreate([
	      'question_fr' => "Qu'est-ce que vous conseilleriez à cette personne d'améliorer ?",
	      'question_fr_ben' => "Qu'est-ce que vous conseilleriez à cette personne d'améliorer ?",
	      'page_id' => $sp8->id,
	      'type_id' => $type1->id,
	      "type" => 'text'
	    ]);

	    $qb84 = Question::firstOrCreate([
	      'question_fr' => "Qu'est-ce que vous conseilleriez à cette personne de ne pas changer ?",
	      'question_fr_ben' => "Qu'est-ce que vous conseilleriez à cette personne de ne pas changer ?",
	      'page_id' => $sp8->id,
	      'type_id' => $type1->id,
	      "type" => 'text'
	    ]);

	    // REPONSE POUR CHAQUE QUESTION
	    for($i= 1; $i< 82; $i++){

	    	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Jamais",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 0
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Très rarement",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 1
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Rarement",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 2
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Parfois",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 3
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Souvent",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 4
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Très souvent",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 5
	          	]
	        );

	        Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Toujours",
	            	"question_id" => ${"qb".$i}->id,
            		"score" => 6
	          	]
	        );

	    }

    }
}

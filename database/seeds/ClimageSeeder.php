<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;

class ClimageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = Type::firstOrCreate(array('name' => 'climage-negociation', 'description' => "Climage negociation"));

        $pageTrombi = Page::firstOrCreate(array('title_fr' => 'Mon profil personnel', 'title_en' => "My personal profile", "type_id" => $type->id));
        $pageForm1 = Page::firstOrCreate(array('title_fr' => 'Mon client a l’air de se sentir plutôt …', 'title_en' => "My client seems to feel rather...", "type_id" => $type->id));
        $pageForm2 = Page::firstOrCreate(array('title_fr' => 'Mon client a l’air de se sentir plutôt …', 'title_en' => "My client seems to feel rather...", "type_id" => $type->id));
        $pageForm3 = Page::firstOrCreate(array('title_fr' => 'Mon client trouve important 
De ne pas…						De…', 'title_en' => "My client finds it important 
Not to...                       Of...", "type_id" => $type->id));
        $pageForm4 = Page::firstOrCreate(array('title_fr' => 'Mon client trouve important 
De ne pas…						De…', 'title_en' => "My client finds it important 
Not to...                       Of...", "type_id" => $type->id));

        // trombinoscope 1
		Question::firstOrCreate(
            [
                    'question_fr' => "Mon slogan personnel",
                    "question_en" => "My personnal Motto",
                    "titre_fr" => "slogan",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                    'question_fr' => "Ma plus grande fierté",
                    "question_en" => "What makes me proud",
                    "titre_fr" => "fierte",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                    'question_fr' => "Mon plus grand défi",
                    "question_en" => "My biggest challenge",
                    "titre_fr" => "defi",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                'question_fr' => "Mes centres d'intérêts",
                "question_en" => "My main interests",
                "titre_fr" => "interets",
                'page_id' => $pageTrombi->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'text'
            ]
        );
        
        $question1 = Question::firstOrCreate(
            [
                'question_fr' => "(Répartissez 5 points en fonction des émotivations(*) les plus fréquentes de votre client (cliquez sur le compteur)",
                "question_en" => "(Divide 5 points according to your customer's most frequent emotivations(*) (click on the counter)",
                "titre_fr" => "Acte d’achat",
                "titre_en" => "deed of purchase",
                'page_id' => $pageForm1->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'checkbox'
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Inquiet, anxieux,…",
                "reponse_en" => "Worried, anxious,...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Serein, apaisé…",
                "reponse_en" => "Serene, appeabed...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Agacé, énervé…",
                "reponse_en" => "Annoyed angry...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Impassible, détaché,…",
                "reponse_en" => "Impassive, detached,...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Morose, il s’ennuie…",
                "reponse_en" => "Gloomy He's bored...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Contrarié,  de mauvaise humeur…",
                "reponse_en" => "Annoyed,bad mood...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Râleur, contrarié,  de mauvaise humeur…",
                "reponse_en" => "Grouchy, gloomy,  in a bad mood…",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Joueur, espiègle…",
                "reponse_en" => "player mischievous...",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );

        $question2 = Question::firstOrCreate(
            [
                'question_fr' => "(Répartissez 5 points en fonction des émotivations(*) les plus fréquentes de votre client (cliquez sur le compteur)",
                "question_en" => "(Divide 5 points according to your customer's most frequent emotivations(*) (click on the counter)",
                "titre_fr" => "Relations",
                "titre_en" => "Relations",
                'page_id' => $pageForm2->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'checkbox'
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Affaibli, vexé…",
                "reponse_en" => "Weakened Upset...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fier, compétent…",
                "reponse_en" => "to trust Competent...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Négligé, déçu…",
                "reponse_en" => "scruffy Disappointed...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Apprécié, Reconnaissant…",
                "reponse_en" => "Appreciated, grateful...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Embarrassé, gêné…",
                "reponse_en" => "At a loss embarrassed...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Dévoué, honnête…",
                "reponse_en" => "devoted honest...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Râleur, contrarié,  de mauvaise humeur…",
                "reponse_en" => "Grouchy, gloomy,  in a bad mood…",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Coupable, désolé…",
                "reponse_en" => "Guilty, sorry...",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );

        $question3 = Question::firstOrCreate(
            [
                'question_fr' => "(Répartissez 5 points en fonction des émotivations(*) les plus fréquentes de votre client (cliquez sur le compteur)",
                "question_en" => "(Divide 5 points according to your customer's most frequent emotivations(*) (click on the counter)",
                "titre_fr" => "Les valeurs",
                "titre_en" => "The values",
                'page_id' => $pageForm3->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'checkbox'
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Perdre son temps, se tromper de besoin…",
                "reponse_en" => "Wasting your time, getting the wrong need...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Faire un bon achat, conforme à ses attentes…",
                "reponse_en" => "Make a good purchase, according to its expectations...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Avoir tort, être contraint par des choix limitants…",
                "reponse_en" => "To be wrong, to be constrained by limiting choices...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Avoir raison, faire une affaire, payer moins cher…",
                "reponse_en" => "Being right, making a deal, paying less...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "S’ennuyer, trouver le temps long…",
                "reponse_en" => "Getting bored, finding long time...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Prendre du plaisir à acheter, trouver ce qui lui plaît…",
                "reponse_en" => "Take pleasure in buying, find what it pleases...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Être contredit, empêché de s’exprimer…",
                "reponse_en" => "To be contradicted, prevented from expressing oneself...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Trouver quelque chose de différent, d’original…",
                "reponse_en" => "Find something different, original...",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );

        $question4 = Question::firstOrCreate(
            [
                'question_fr' => "(Répartissez 5 points en fonction des émotivations(*) les plus fréquentes de votre client (cliquez sur le compteur)",
                "question_en" => "(Divide 5 points according to your customer's most frequent emotivations(*) (click on the counter)",
                "titre_fr" => "Valeurs",
                "titre_en" => "Values",
                'page_id' => $pageForm4->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'checkbox'
            ]
        );

		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Perdre, se faire avoir…",
                "reponse_en" => "Lose, be had...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Gagner, être le meilleur…",
                "reponse_en" => "Win, be the best...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Être négligé, pas pris en considération…",
                "reponse_en" => "Being neglected, not taken into consideration...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );

         Reponse::firstOrCreate(
            [
                'reponse_fr' => "Être considéré, écouté, Se sentir unique…",
                "reponse_en" => "Be considered, listened to, Feel unique...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Se retrouver  dans le produit, la marque…",
                "reponse_en" => "Find yourself in the product, the brand...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "S’identifier  à la force, la puissance du produit.",
                "reponse_en" => "Identify with the strength, the power of the product.",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Apprécier l’image de marque…",
                "reponse_en" => "Appreciate the brand image...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Aimer le produit ou la marque (sympathie)…",
                "reponse_en" => "Love the product or the brand (sympathy)...",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );       
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;
use App\ReponseRepondant;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	$test_id = 2;
		$repondants = [3,4];
		$pages = Type::where('name', "feedback")->first()->pages;

		foreach ($repondants as $key => $rep) {
			 \DB::table('test_repondant')->updateOrInsert(
	                ['test_id' => $test_id, 'repondant_id' => $rep],
	                ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
	        );



		    foreach ($pages as $key => $page) {
		      
		        foreach ($page->pages as $key3 => $section) {
		          foreach ($section->questions as $key3 => $question) {
		          		if($question->type != "radio"){
		          			continue;
		          		}
		          		$score = rand(0,4);
    					$reponse =  Reponse::where("question_id", $question->id)->where("score", $score)->first();
		            	$count = ReponseRepondant::where('repondant_id', '=' ,$rep)->where("question_id", "=", $question->id)->count();
	                    
	                    if($count == 0){
	                        $repondant_question_id = \DB::table('reponse_repondant')->updateOrInsert(
	                            ['question_id' => $question->id, 'repondant_id' => $rep, "test_id" => $test_id, "reponse_id" => $reponse->id],
	                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
	                        );
	                    } else {
	                        $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,$rep)->where("question_id", "=", $question->id)->update(
	                            ['question_id' => $question->id, 'repondant_id' => $rep, "test_id" => $test_id, "reponse_id" => $reponse->id],
	                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
	                        );
	                    }
		          }
		        }
		      
		    }

		}
			

    }
}

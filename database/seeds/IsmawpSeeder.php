<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Test;
use App\Type;
use App\ReponseRepondant;
use App\TestRepondant;
use App\Reponse;
use App\Question;
use App\User;
use App\Page;

class IsmawpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function getConsultant($prenom, $nom){
	    return $consultant = User::whereRaw("LOWER(firstname) like (?)", [strtolower($prenom)])->whereRaw("LOWER(lastname) like (?)", [strtolower($nom)])->first();
	}

	public function getReponse($rep, $idQuest){
	    $reponse = Reponse::where("question_id","=",$idQuest)->where('reponse_fr', $rep)->first();
	    if(!isset($reponse)){
	      $reponse = Reponse::where("question_id","=",$idQuest)->where('reponse_en', $rep)->first();
	    }
	    return $reponse;
	}

	public function getQuestion($cur,$page,$type){
	    switch ($cur) {
	      case "1":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime transgresser les règles",
	            "question_en" => "I like to break rules",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "2":
	        $q = Question::firstOrCreate([
	          'question_fr' => "J’adore relever des défis",
	          "question_en" => "I enjoy facing challenges",
	          'page_id' => $page["1"],
	          'type_id' => $type,
	          "type" => 'radio'
	        ]);
	        break;

	      case "3":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aide les autres à réussir",
	            "question_en" => "I help others to succeed",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "4":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime faire les choses simplement pour le plaisir",
	            "question_en" => "I like to do things merely for the fun of it",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "5":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je suis sensible à ce qui arrive aux autres",
	            "question_en" => "I care what happens to others",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "6":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je fais des choses que je considère comme importantes",
	            "question_en" => "I do things that I consider important",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "7":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je montre aux autres que je crois en leurs capacités",
	            "question_en" => "I show belief in someone else's abilities",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "8":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime plaire aux autres",
	            "question_en" => "I like to be attractive to others",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "9":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux faire ce que l'on attend de moi",
	            "question_en" => "I want to do what is expected of me",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "10":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime agir à contre-courant",
	            "question_en" => "I act in a contrary fashion",
	            'page_id' => $page["1"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "11":
	        // QUESTION PAGE 2
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux être le responsable",
	            "question_en" => "I want to be in charge",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "12":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime adopter des façons de faire reconnues",
	            "question_en" => "I like following established ways of doing things",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "13":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime être aimé",
	            "question_en" => "I like to be liked",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "14":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je suis amical envers les autres",
	            "question_en" => "I am a good friend",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "15":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’envisage les choses à long terme",
	            "question_en" => "I take a long-term perspective",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "16":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je soutiens les autres dans leurs projets",
	            "question_en" => "I help others to achieve things",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "17":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux un plaisir immédiat",
	            "question_en" => "I want immediate enjoyment",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "18":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aide les gens dans le besoin",
	            "question_en" => "I give to those in need",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "19":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je travaille sur des objectifs à long terme",
	            "question_en" => "I work for long-term goals",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "20":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime défier l’autorité",
	            "question_en" => "I enjoy defying authority",
	            'page_id' => $page["2"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "21":
	        // QUESTION PAGE 3
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je recherche les responsabilités",
	            "question_en" => "I look for responsibility",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "22":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aide les autres à croire en eux-mêmes",
	            "question_en" => "I help others believe in themselves",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "23":  
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux suivre les règles",
	            "question_en" => "I want to follow rules",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "24":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux être gentil avec autrui",
	            "question_en" => "I aim to be kind to others",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "25":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’agis avec spontanéité",
	            "question_en" => "I act spontaneously",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "26":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je m’inquiète de savoir si les autres m’apprécient",
	            "question_en" => "I worry about whether others like me",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "27":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je planifie à l'avance",
	            "question_en" => "I plan ahead",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "28":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime contrôler les choses",
	            "question_en" => "I like to be in control of things",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "29":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’ai envie de faire des choses interdites",
	            "question_en" => "I want to do things that are prohibited",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "30":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’encourage les autres à faire mieux",
	            "question_en" => "I encourage someone else to do better",
	            'page_id' => $page["3"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "31":
	        // QUESTION PAGE 4
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux vivre dans l'ici et le maintenant",
	            "question_en" => "I want to live in the here and now",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "32":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je déteste ne pas être apprécié des autres",
	            "question_en" => "I hate to feel unpopular",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "33":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux faire ce que je suis censé faire",
	            "question_en" => "I want to do what I'm supposed to do",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "34":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je veux faire des choses qui ont du sens",
	            "question_en" => "I want do things that are meaningful",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "35":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’accueille favorablement les opportunités d'éprouver mes compétences",
	            "question_en" => "I welcome the chance to test my skills",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "36":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je fais selon mon envie du moment",
	            "question_en" => "I do what I want to do at the moment",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "37":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’apprécie les occasions de faire mon devoir",
	            "question_en" => "I welcome being able to do my duty",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "38":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’aime faire des cadeaux",
	            "question_en" => "I enjoy giving presents",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "39":
	        $q = Question::firstOrCreate([
	            'question_fr' => "Je me sens rebelle",
	            "question_en" => "I feel rebellious",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      case "40":
	        $q = Question::firstOrCreate([
	            'question_fr' => "J’apprécie que l'on me porte de l'attention",
	            "question_en" => "I welcome attention from others",
	            'page_id' => $page["4"],
	            'type_id' => $type,
	            "type" => 'radio'
	        ]);
	        break;

	      default:
	          # code...
	          break;
	    }
	    return isset($q) ? $q : null;
	}

    public function run()
    {
		//ini_set('max_execution_time', 400);
		// TYPE
	    $type1 = Type::firstOrCreate(['name' => 'isma']);

	    // PAGES
	    $page1 = Page::firstOrCreate(['title_fr' => 'page1', "type_id" => $type1->id]);
	    $page2 = Page::firstOrCreate(['title_fr' => 'page2', "type_id" => $type1->id]);
	    $page3 = Page::firstOrCreate(['title_fr' => 'page3', "type_id" => $type1->id]);
	    $page4 = Page::firstOrCreate(['title_fr' => 'page4', "type_id" => $type1->id]);

	    $pages = array("1"=>$page1->id,"2"=>$page2->id,"3"=>$page3->id,"4"=>$page4->id);
	    //$consultants = \DB::connection('mysql2')->select("select * from wp_surveywp_consultant");
		
		// GET ALL USERS ISMA WPADMIN	    

	    $wp_users = \DB::connection('mysql2')->select("select max(id_coache) as id_coache, `civilite`, `nom`, `prenom`, `age`, `pays`, `phone`, max(date_add) as date_add, `id_consultant`, `link_generate`, `statut`, `email`, `lang`, `organisation` from wp_surveywp_coache group by nom, prenom order by max(id_coache) desc");    


	    // RETIRER LES USERS CONSULTANTS
	    $newUsers = array();
	    foreach ($wp_users as $key => $wp_user) {
	      	$checkons = \DB::connection('mysql2')->table('wp_surveywp_consultant')->where('nom', $wp_user->nom)->where('prenom', $wp_user->prenom)->count();
	      	if($checkons == 0){
	        	array_push($newUsers,$wp_user);
	      	}
	    }


	    $reponses = array();
	    foreach ($newUsers as $key => $user) {

	      	//CREER UN CODE ALEATOIRE
	      	$code = str_random(6);

	      	$countMail = User::where("email",'aptersolutions_'.$key.'@gmail.com')->first();

	      	if(is_null($countMail) || empty($countMail)){

	      		$checkUsername = User::where("username","like",'%'.$user->nom.'%')->first();

	      		if(is_null($checkUsername)){
	      			$keyUser = User::firstOrCreate([
			            'username' => $user->nom." ".$user->prenom,
			            "email" => 'aptersolutions_'.$key.'@gmail.com',
			            'password' => bcrypt($code),
			            'create_time' => $user->date_add,
			            'firstname' => $user->prenom,
					    'lastname' => $user->nom,
					    'organisation' => $user->organisation,
					    'civility' => $user->civilite,
					    'language' => $user->lang,
					    'role' => 'user',
					    'avatar_url' => "",
					    'age' => $user->age,
					    'pays' => $user->pays,
					    'created_at' => $user->date_add,
					    "updated_at" => $user->date_add,
					    "code" => $code,
			        ]);
	      		} else {
	      			$keyUser = $checkUsername;
	      		}
	      	} else {
	      		$keyUser = $countMail;
	      	}

	      	// GET CONSULTANT USER
	      	$consultant_wp = \DB::connection('mysql2')->table('wp_surveywp_consultant')->where('id_consultant', $user->id_consultant)->first();

	      	//var_dump(isset($consultant_wp->nom) ? $consultant_wp->nom : "");
	      	if(isset($consultant_wp)){
	        	$consultant = User::where("firstname", $consultant_wp->prenom)->where("lastname", $consultant_wp->nom)->first();
	        	if(!is_null($consultant)){
	          
			        // AJOUTER LES NOUVEAUX TESTS POUR L'USER
	        		$countTest = Test::where("name","ISMA - ".$user->prenom." ".$user->nom)->first();
			      	// AJOUTER ET RECUPERER LES NOUVEAUX TESTS (CREATE OR FIRST NE MARCHE PAS ICI)
			      	if(isset($countTest)){
						$testUser = $countTest;	      		
			      	} else {
			      		$testUser = new Test;
			      		$testUser->name = "ISMA - ".$user->prenom." ".$user->nom;
					    $testUser->description = "ancien-isma";
					    $testUser->type_id = $type1->id;
					    $testUser->langue = $user->lang;
					    $testUser->consultant_id = $consultant["id"];
					    $testUser->created_at = $user->date_add;
					    $testUser->updated_at = $user->date_add;
					    $testUser->save();
			      	}
		        }

		        // AJOUTER LES TESTS REPONDANTS
		        TestRepondant::firstOrCreate([
		            'repondant_id' => $keyUser->id,
		            'test_id' => $testUser->id,
		            "created_at" => $user->date_add,
		            "updated_at" => $user->date_add,
		            "reponse" => 1
		        ]);

		        //var_dump($consultant["id"]);
	    	}

	      	// RECUPERER LES REPONSES DANS WP

	      	$reponses = \DB::connection('mysql2')->table('wp_surveywp_coache_reponse')->where('id_coache', $user->id_coache)->get();

	      	foreach ($reponses as $key => $reps) {
	      		if(isset($reps->question) && isset($reps->reponse)){
	      			$quest = $this->getQuestion($reps->question,$pages,$type1->id); 
	      			if(isset($quest->id)){
	      				$rep = $this->getReponse($reps->reponse,$quest->id);


					    ReponseRepondant::firstOrCreate([
				            'repondant_id' => $keyUser->id,
				            "question_id" => $quest->id,
				            'reponse_id' => $rep->id,
				            'test_id' => $testUser->id,
				            "created_at" => $user->date_add,
				            "updated_at" => $user->date_add
				        ]);

	      			}	
	      		}
	      	}
	    }
    }	
}

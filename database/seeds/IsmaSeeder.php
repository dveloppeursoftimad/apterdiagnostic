<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;
use App\Category;
use App\Category_question;

class IsmaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// TYPE
  		$type1 = Type::firstOrCreate(['name' => 'isma']);

  		// PAGES
    	$page1 = Page::firstOrCreate(['title_fr' => 'page1', "type_id" => $type1->id]);
    	$page2 = Page::firstOrCreate(['title_fr' => 'page2', "type_id" => $type1->id]);
    	$page3 = Page::firstOrCreate(['title_fr' => 'page3', "type_id" => $type1->id]);
    	$page4 = Page::firstOrCreate(['title_fr' => 'page4', "type_id" => $type1->id]);

        // QUESTIONS

        //QUEST PAGE 1
        $q1 = Question::firstOrCreate([
	      	'question_fr' => "J’aime transgresser les règles",
	      	"question_en" => "I like to break rules",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q2 = Question::firstOrCreate([
	      	'question_fr' => "J’adore relever des défis",
	      	"question_en" => "I enjoy facing challenges",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q3 = Question::firstOrCreate([
	      	'question_fr' => "J’aide les autres à réussir",
	      	"question_en" => "I help others to succeed",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q4 = Question::firstOrCreate([
	      	'question_fr' => "J’aime faire les choses simplement pour le plaisir",
	      	"question_en" => "I like to do things merely for the fun of it",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q5 = Question::firstOrCreate([
	      	'question_fr' => "Je suis sensible à ce qui arrive aux autres",
	      	"question_en" => "I care what happens to others",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q6 = Question::firstOrCreate([
	      	'question_fr' => "Je fais des choses que je considère comme importantes",
	      	"question_en" => "I do things that I consider important",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q7 = Question::firstOrCreate([
	      	'question_fr' => "Je montre aux autres que je crois en leurs capacités",
	      	"question_en" => "I show belief in someone else's abilities",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q8 = Question::firstOrCreate([
	      	'question_fr' => "J’aime plaire aux autres",
	      	"question_en" => "I like to be attractive to others",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q9 = Question::firstOrCreate([
	      	'question_fr' => "Je veux faire ce que l'on attend de moi",
	      	"question_en" => "I want to do what is expected of me",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q10 = Question::firstOrCreate([
	      	'question_fr' => "J’aime agir à contre-courant",
	      	"question_en" => "I act in a contrary fashion",
	      	'page_id' => $page1->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    // QUESTION PAGE 2
	    $q11 = Question::firstOrCreate([
	      	'question_fr' => "Je veux être le responsable",
	      	"question_en" => "I want to be in charge",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q12 = Question::firstOrCreate([
	      	'question_fr' => "J’aime adopter des façons de faire reconnues",
	      	"question_en" => "I like following established ways of doing things",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q13 = Question::firstOrCreate([
	      	'question_fr' => "J’aime être aimé",
	      	"question_en" => "I like to be liked",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q14 = Question::firstOrCreate([
	      	'question_fr' => "Je suis amical envers les autres",
	      	"question_en" => "I am a good friend",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q15 = Question::firstOrCreate([
	      	'question_fr' => "J’envisage les choses à long terme",
	      	"question_en" => "I take a long-term perspective",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q16 = Question::firstOrCreate([
	      	'question_fr' => "Je soutiens les autres dans leurs projets",
	      	"question_en" => "I help others to achieve things",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q17 = Question::firstOrCreate([
	      	'question_fr' => "Je veux un plaisir immédiat",
	      	"question_en" => "I want immediate enjoyment",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q18 = Question::firstOrCreate([
	      	'question_fr' => "J’aide les gens dans le besoin",
	      	"question_en" => "I give to those in need",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q19 = Question::firstOrCreate([
	      	'question_fr' => "Je travaille sur des objectifs à long terme",
	      	"question_en" => "I work for long-term goals",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q20 = Question::firstOrCreate([
	      	'question_fr' => "J’aime défier l’autorité",
	      	"question_en" => "I enjoy defying authority",
	      	'page_id' => $page2->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    // QUESTION PAGE 3
	    $q21 = Question::firstOrCreate([
	      	'question_fr' => "Je recherche les responsabilités",
	      	"question_en" => "I look for responsibility",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q22 = Question::firstOrCreate([
	      	'question_fr' => "J’aide les autres à croire en eux-mêmes",
	      	"question_en" => "I help others believe in themselves",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q23 = Question::firstOrCreate([
	      	'question_fr' => "Je veux suivre les règles",
	      	"question_en" => "I want to follow rules",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q24 = Question::firstOrCreate([
	      	'question_fr' => "Je veux être gentil avec autrui",
	      	"question_en" => "I aim to be kind to others",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q25 = Question::firstOrCreate([
	      	'question_fr' => "J’agis avec spontanéité",
	      	"question_en" => "I act spontaneously",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q26 = Question::firstOrCreate([
	      	'question_fr' => "Je m’inquiète de savoir si les autres m’apprécient",
	      	"question_en" => "I worry about whether others like me",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q27 = Question::firstOrCreate([
	      	'question_fr' => "Je planifie à l'avance",
	      	"question_en" => "I plan ahead",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q28 = Question::firstOrCreate([
	      	'question_fr' => "J’aime contrôler les choses",
	      	"question_en" => "I like to be in control of things",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q29 = Question::firstOrCreate([
	      	'question_fr' => "J’ai envie de faire des choses interdites",
	      	"question_en" => "I want to do things that are prohibited",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q30 = Question::firstOrCreate([
	      	'question_fr' => "J’encourage les autres à faire mieux",
	      	"question_en" => "I encourage someone else to do better",
	      	'page_id' => $page3->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    // QUESTION PAGE 4
	    $q31 = Question::firstOrCreate([
	      	'question_fr' => "Je veux vivre dans l'ici et le maintenant",
	      	"question_en" => "I want to live in the here and now",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q32 = Question::firstOrCreate([
	      	'question_fr' => "Je déteste ne pas être apprécié des autres",
	      	"question_en" => "I hate to feel unpopular",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q33 = Question::firstOrCreate([
	      	'question_fr' => "Je veux faire ce que je suis censé faire",
	      	"question_en" => "I want to do what I'm supposed to do",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q34 = Question::firstOrCreate([
	      	'question_fr' => "Je veux faire des choses qui ont du sens",
	      	"question_en" => "I want do things that are meaningful",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q35 = Question::firstOrCreate([
	      	'question_fr' => "J’accueille favorablement les opportunités d'éprouver mes compétences",
	      	"question_en" => "I welcome the chance to test my skills",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q36 = Question::firstOrCreate([
	      	'question_fr' => "Je fais selon mon envie du moment",
	      	"question_en" => "I do what I want to do at the moment",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q37 = Question::firstOrCreate([
	      	'question_fr' => "J’apprécie les occasions de faire mon devoir",
	      	"question_en" => "I welcome being able to do my duty",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q38 = Question::firstOrCreate([
	      	'question_fr' => "J’aime faire des cadeaux",
	      	"question_en" => "I enjoy giving presents",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q39 = Question::firstOrCreate([
	      	'question_fr' => "Je me sens rebelle",
	      	"question_en" => "I feel rebellious",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    $q40 = Question::firstOrCreate([
	      	'question_fr' => "J’apprécie que l'on me porte de l'attention",
	      	"question_en" => "I welcome attention from others",
	      	'page_id' => $page4->id,
	      	'type_id' => $type1->id,
	      	"type" => 'radio'
	    ]);

	    // LES CATEGORIES
	    $cat1 = Category::firstOrCreate([
	    	'name' => "serieux",
	    	'type_id' => $type1->id
	    ]);

	    $cat2 = Category::firstOrCreate([
	    	'name' => "enjoue",
	    	'type_id' => $type1->id
	    ]);

	    $cat3 = Category::firstOrCreate([
	    	'name' => "conforme",
	    	'type_id' => $type1->id
	    ]);

	    $cat4 = Category::firstOrCreate([
	    	'name' => "transgressif",
	    	'type_id' => $type1->id
	    ]);

	    $cat5 = Category::firstOrCreate([
	    	'name' => "Autrui",
	    	'type_id' => $type1->id
	    ]);

	    $cat6 = Category::firstOrCreate([
	    	'name' => "soi",
	    	'type_id' => $type1->id
	    ]);

	    $cat7 = Category::firstOrCreate([
	    	'name' => "maitrise",
	    	'type_id' => $type1->id
	    ]);

	    $cat8 = Category::firstOrCreate([
	    	'name' => "sympathie",
	    	'type_id' => $type1->id
	    ]);

	    // CATEGORIE QUESTIONS SERIEUX
	    Category_question::firstOrCreate(["category_id" => $cat1->id, "question_id" => $q6->id]);
	    Category_question::firstOrCreate(["category_id" => $cat1->id, "question_id" => $q15->id]);
	    Category_question::firstOrCreate(["category_id" => $cat1->id, "question_id" => $q19->id]);
	    Category_question::firstOrCreate(["category_id" => $cat1->id, "question_id" => $q27->id]);
	    Category_question::firstOrCreate(["category_id" => $cat1->id, "question_id" => $q34->id]);
	    
	    // CATEGORIE QUESTIONS ENJOUE
	    Category_question::firstOrCreate(["category_id" => $cat2->id, "question_id" => $q36->id]);
	    Category_question::firstOrCreate(["category_id" => $cat2->id, "question_id" => $q31->id]);
	    Category_question::firstOrCreate(["category_id" => $cat2->id, "question_id" => $q25->id]);
	    Category_question::firstOrCreate(["category_id" => $cat2->id, "question_id" => $q17->id]);
	    Category_question::firstOrCreate(["category_id" => $cat2->id, "question_id" => $q4->id]);

	    // CATEGORIE QUEST CONFORME
	    Category_question::firstOrCreate(["category_id" => $cat3->id, "question_id" => $q37->id]);
	    Category_question::firstOrCreate(["category_id" => $cat3->id, "question_id" => $q33->id]);
	    Category_question::firstOrCreate(["category_id" => $cat3->id, "question_id" => $q23->id]);
	    Category_question::firstOrCreate(["category_id" => $cat3->id, "question_id" => $q12->id]);
	    Category_question::firstOrCreate(["category_id" => $cat3->id, "question_id" => $q9->id]);

	    // CATEGORIE QUEST TRANSGRESSIF
	    Category_question::firstOrCreate(["category_id" => $cat4->id, "question_id" => $q39->id]);
	    Category_question::firstOrCreate(["category_id" => $cat4->id, "question_id" => $q29->id]);
	    Category_question::firstOrCreate(["category_id" => $cat4->id, "question_id" => $q20->id]);
	    Category_question::firstOrCreate(["category_id" => $cat4->id, "question_id" => $q10->id]);
	    Category_question::firstOrCreate(["category_id" => $cat4->id, "question_id" => $q1->id]);

	    // CATEGORIE QUEST AUTRUIS
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q30->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q20->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q16->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q7->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q3->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q38->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q24->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q18->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q14->id]);
	    Category_question::firstOrCreate(["category_id" => $cat5->id, "question_id" => $q5->id]);

	    // CATEGORIE QUEST SOIS
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q40->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q32->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q26->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q13->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q8->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q28->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q35->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q21->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q11->id]);
	    Category_question::firstOrCreate(["category_id" => $cat6->id, "question_id" => $q2->id]);

	    // CATEGORIE QUEST MAITRISE
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q30->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q22->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q16->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q7->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q3->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q28->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q35->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q21->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q11->id]);
	    Category_question::firstOrCreate(["category_id" => $cat7->id, "question_id" => $q2->id]);

	    // CATEGORIE QUEST SYMPHATIE
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q40->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q32->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q26->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q13->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q8->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q38->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q24->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q18->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q14->id]);
	    Category_question::firstOrCreate(["category_id" => $cat8->id, "question_id" => $q5->id]);


		// REPONSE POUR CHAQUE QUESTION
    	for($i= 1; $i< 41; $i++){

    		Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Jamais",
	            	"reponse_en" => "Never",
	            	"score" => 1,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

        	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Rarement",
	            	"reponse_en" => "Rarely",
	            	"score" => 2,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

        	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Parfois",
	            	"reponse_en" => "Sometimes",
	            	"score" => 3,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

        	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Souvent",
	            	"reponse_en" => "Often",
	            	"score" => 4,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

        	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Très souvent",
	            	"reponse_en" => "Very often",
	            	"score" => 5,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

        	Reponse::firstOrCreate(
	          	[
	            	'reponse_fr' => "Toujours",
	            	"reponse_en" => "Always",
	            	"score" => 6,
	            	"question_id" => ${"q".$i}->id
	          	]
        	);

    	}	    

    }
}

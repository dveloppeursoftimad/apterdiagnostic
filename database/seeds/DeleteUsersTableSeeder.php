<?php

use Illuminate\Database\Seeder;
use App\Test;
use App\ReponseRepondant;
use App\TestRepondant;
use App\User;

class DeleteUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where("email","like","aptersolutions_%")->get();

	    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
	    foreach ($users as $key => $user) {
	      
	      	ReponseRepondant::where("repondant_id","=",$user->id)->delete();
	      	TestRepondant::where("repondant_id",$user->id)->delete();
	      	User::destroy($user->id);
	      
	    }
	    Test::where("description","=","ancien-isma")->delete();
	    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

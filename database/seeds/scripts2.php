<?php

$tableauDeDonnees = array(

    5 => array( // page

        1 => array( // section

            'quest' => [
                [
                    'ben' => "Je relaie les bonnes informations aux bons moments et aux bons niveaux",
                    'obs' => "Il / Elle relaie les bonnes informations aux bons moments et aux bons niveaux"   
                ],
                [
                    'ben' => "Je sais dire non et m’opposer si l’enjeu est important ",
                    'obs' => "Il / Elle sait dire non et s’opposer si l’enjeu est important "   
                ],
                [
                    'ben' => 'Je favorise la libre expression, le débat et la discussion',
                    'obs' => 'Il / Elle favorise la libre expression, le débat et la discussion'
                ],
                [
                    'ben' => 'Je veille à ce que l’équipe dispose de toutes les informations nécessaire à son travail',
                    'obs' => 'Il / Elle veille à ce que l’équipe dispose de toutes les informations nécessaire à son travail'
                ],
                [
                    'ben' => 'Je suis à l’aise pour m’exprimer face à un public',
                    'obs' => 'Il / Elle est à l’aise pour s’exprimer face à un public',
                ],
                [
                    'ben' => 'J’attire la sympathie et suis à l’aise dans ma relation à autrui',
                    'obs' => 'Il / Elle attire la sympathie et est à l’aise dans sa relation à autrui'
                ],
                [
                    'ben' => 'Mes réunions sont participatives et nous sommes collectivement efficaces',
                    'obs' => 'Ses réunions sont participatives et l\'équipe est collectivement efficace'
                ],
                [
                    'ben' => 'J’organise des moments de convivialité avec mon équipe',
                    'obs' => 'Il / Elle organise des moments de convivialité avec son équipe'
                ],
            ]

        ),

    ),

    6 => array( // page

        1 => array( // section 1

            'quest' => [

                [
                    'ben' => 'rassurant, méthodique, porteur de sens…',
                    'obs' => 'rassurant/e, méthodique, porteur/se de sens…'
                ],
                [   
                    'ben' => 'ou au contraire, irréfléchi, inconsistant, moralisateur…',
                    'obs' => 'ou au contraire, irréfléchi/e, inconsistant/e, moralisateur/trice…'
                ],
                [
                    'ben' => 'imperturbable, "zen", résistant aux pressions…',
                    'obs' => 'imperturbable, "zen", résistant/e aux pressions…'
                ],
                [
                    'ben' => 'ou au contraire, blasé, indifférent, désengagé, "j\'menfoutiste"…',
                    'obs' => 'ou au contraire, blasé/e, indifférent/e, désengagé/e, "j\'menfoutiste"…'
                ],
                [   
                    'ben' => 'Inventif, étonnant, humoristique…',
                    'obs' => 'Inventif/ve, étonnant/e, humoristique…'
                ],
                [
                    'ben' => 'ou au contraire, ironique, blessant, déplacé, cynique…',
                    'obs' => 'ou au contraire, ironique, blessant/e, déplacé/e, cynique…'
                ],
                [
                    'ben' => 'Enthousiaste, passionné, impliqué…',
                    'obs' => 'Enthousiaste, passionné/e, impliqué/e…',
                ],
                [
                    'ben' => 'ou au contraire, impulsif, étourdi, inconstant…',
                    'obs' => 'ou au contraire, impulsif/ve, étourdi/e, inconstant/e…'
                ],
                [
                    'ben' => 'Fiable, solide, fort, performant…',
                    'obs' => 'Fiable, solide, fort/e, performant/e…'
                ],
                [
                    'ben' => 'ou au contraire, arrogant, supérieur, méprisant…',
                    'obs' => 'ou au contraire, arrogant/e, supérieur/e, méprisant/e…'
                ],
                [
                    'ben' => 'Aimable, sympathique, séduisant…',
                    'obs' => 'Aimable, sympathique, séduisant/e…'
                ],
                [
                    'ben' => 'ou au contraire, séducteur, pressant, simulateur…',
                    'obs' => 'ou au contraire, séducteur/trice, pressant/e, simulateur/trice…'
                ],
                [
                    'ben' => 'Dévoué, aidant, disponible, supporter…',
                    'obs' => 'Dévoué/e, aidant/E, disponible, supporter…'
                ],
                [
                    'ben' => 'ou au contraire, hypocrite, calculateur, embarrassant…',
                    'obs' => 'ou au contraire, hypocrite, calculateur/trice, embarrassant/e…'
                ],
                [
                    'ben' => 'Attentionné, compréhensif, serviable…',
                    'obs' => 'Attentionné/e, compréhensif/ve, serviable…'
                ],
                [
                    'ben' => 'ou au contraire, envahissant, débordant, oppressant…',
                    'obs' => 'ou au contraire, envahissant/e, débordant/e, oppressant/e…'
                ]
            ],
            'titre_ben' => 'Lorsque l\'ambiance est bonne, je peux être perçu comme :',
            'titre_obs' => 'Lorsque l\'ambiance est bonne, il / elle peut être perçu/e comme :'
        ),

        2 => array( // section 2

            'quest' => [
                [
                    'ben' => 'Vigilant, attentif, préparé…',
                    'obs' => 'Vigilant/e, attentif/ve, préparé/e…'
                ],
                [
                    'ben' => 'ou au contraire, fébrile, crispé…',
                    'obs' => 'ou au contraire, fébrile, crispé/e…'
                ],
                [
                    'ben' => 'Combatif, tenace, persévérant…',
                    'obs' => 'Combatif/ve, tenace, persévérant/e…'
                ],
                [
                    'ben' => 'ou au contraire, agressif, entêté, buté…',
                    'obs' => 'ou au contraire, agressif/ve, entêté/e, buté/e…'
                ],
                [
                    'ben' => 'Stimulant, critique (mais constructif)…',
                    'obs' => 'Stimulant/e, critique (mais constructif/ve)…'
                ],
                [
                    'ben' => 'ou au contraire, hostile, boudeur, bloqué…',
                    'obs' => 'ou au contraire, hostile, boudeur/se, bloqué/e…'
                ],
                [
                    'ben' => 'Volontaire, disponible…',
                    'obs' => 'Volontaire, disponible…'
                ],
                [
                    'ben' => 'ou au contraire, apathique, procrastinant, désengagé…',
                    'obs' => 'ou au contraire, apathique, procrastinant/e, désengagé/e…'
                ],
                [
                    'ben' => 'Courageux, déterminé, valeureux…',
                    'obs' => 'Courageux/se, déterminé/e, valeureux/se…'
                ],
                [
                    'ben' => 'ou au contraire, soumis, laxiste, abattu, indigne…',
                    'obs' => 'ou au contraire, soumis/e, laxiste, abattu/e, indigne…'
                ],
                [
                    'ben' => 'Affirmé, confiant…',
                    'obs' => 'Affirmé/e, confiant/e…'
                ],
                [
                    'ben' => 'ou au contraire, aigri, amer, caliméro…',
                    'obs' => 'ou au contraire, aigri/e, amer/amère, caliméro…'
                ],
                [
                    'ben' => 'Honorable, respectable, loyal, estimable…',
                    'obs' => 'Honorable, respectable, loyal/e, estimable…'
                ],
                [
                    'ben' => 'ou au contraire, lâche, déloyal, méprisable…',
                    'obs' => 'ou au contraire, lâche, déloyal/e, méprisable…'
                ],
                [
                    'ben' => 'Sincère, vrai…',
                    'obs' => 'Sincère, vrai/e…'
                ],
                [
                    'ben' => 'ou au contraire, plaintif, misérabiliste, complaignant…',
                    'obs' => 'ou au contraire, plaintif/ve, misérabiliste, complaignant/e…'
                ],
            ],
            'titre_ben' => 'Lorsque l\'ambiance est mauvaise, je peux être perçu comme :',
            'titre_obs' => 'Lorsque l\'ambiance est mauvaise, il / elle peut être perçu/e comme :'

        ),


    ),

);

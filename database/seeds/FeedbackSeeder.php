<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;


class FeedbackSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TYPE

  	$type1 = Type::firstOrCreate(['name' => 'feedback']);

    // PAGES
    $page1 = Page::firstOrCreate(['title_fr' => 'MANAGEMENT ET COMMUNICATION', 'title_en' => "MANAGEMENT AND COMMUNICATION", "type_id" => $type1->id]);
    $page2 = Page::firstOrCreate(array('title_fr' => 'LEADERSHIP', 'title_en' => "LEADERSHIP", "type_id" => $type1->id));
    $page3 = Page::firstOrCreate(array('title_fr' => 'TRANSVERSALITE et MANAGEMENT DE PROJETS MATRICIEL', 'title_en' => "TRANSVERSALITY and MATRICIAL PROJECT MANAGEMENT", "type_id" => $type1->id));
    $page4 = Page::firstOrCreate(array('title_fr' => 'ATTITUDE FACE AU CHANGEMENT','title_en' => "ATTITUDE FACING CHANGE", "type_id" => $type1->id));
    $page5 = Page::firstOrCreate(array('title_fr' => 'PILOTAGE DE L’ACTIVITE', 'title_en' => "PILOTAGE OF ACTIVITY", "type_id" => $type1->id));
    $page6 = Page::firstOrCreate(array('title_fr' => 'IMPLICATION', 'title_en' => "INVOLVEMENT", "type_id" => $type1->id));
    $page7 = Page::firstOrCreate(array('title_fr' => 'ADHESION AUX VALEURS DE L’ENTREPRISE', 'title_en' => "ACCESSION TO THE VALUES OF THE COMPANY", "type_id" => $type1->id));
    $page8 = Page::firstOrCreate(array('title_fr' => 'QUESTIONS OUVERTES DE FIN DE QUESTIONNAIRE', 'title_en' => "OPEN QUESTIONS OF END SURVEY", "type_id" => $type1->id));

    // SOUS PAGE 1
    $sp1 = Page::firstOrCreate(array('title_fr' => 'Communication orale/écrite', 'title_en' => "Oral/written communication", "page_id" => $page1->id, "type_id" => $type1->id));
    $sp2 = Page::firstOrCreate(array('title_fr' => 'Écoute/efficacité relationnelle', 'title_en' => "Listening/relational efficiency", "page_id" => $page1->id, "type_id" => $type1->id));

    // SOUS PAGE 2
    $sp3 = Page::firstOrCreate(array('title_fr' => 'Stratégie/Vision', 'title_en' => "Strategy / Vision", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp4 = Page::firstOrCreate(array('title_fr' => 'Management de l’activité', 'title_en' => "Management of the activity", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp5 = Page::firstOrCreate(array('title_fr' => 'Accompagnement des femmes et des hommes de sa direction', 'title_en' => "Accompaniment of women and men in their management", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp6 = Page::firstOrCreate(array('title_fr' => 'Prise de décision', 'title_en' => "Decision making", "page_id" => $page2->id, "type_id" => $type1->id));

    // SOUS PAGE 3
    $sp7 = Page::firstOrCreate(array('title_fr' => 'Travail d’équipe', 'title_en' => "Teamwork", "page_id" => $page3->id, "type_id" => $type1->id));
    $sp8 = Page::firstOrCreate(array('title_fr' => 'Entraide/solidarité', 'title_en' => "Mutual assistance/solidarity", "page_id" => $page3->id, "type_id" => $type1->id));

    // SOUS PAGE 4
    $sp9 = Page::firstOrCreate(array('title_fr' => 'Efficacité sous pression', 'title_en' => "Efficiency under pressure", "page_id" => $page4->id, "type_id" => $type1->id));

    $sp10_1 = Page::firstOrCreate(array('title_fr' => 'Flexibilité/créativité ', 'title_en' => "Flexibility/creativity", "page_id" => $page4->id, "type_id" => $type1->id));

    $sp10 = Page::firstOrCreate(array('title_fr' => 'Prise de risques', 'title_en' => "Risk taking", "page_id" => $page4->id, "type_id" => $type1->id));

    // SOUS PAGE 5
    $sp11 = Page::firstOrCreate(array('title_fr' => 'Analyse/pro-activité', 'title_en' => "Analysis/proactivity", "page_id" => $page5->id, "type_id" => $type1->id));
    $sp12 = Page::firstOrCreate(array('title_fr' => 'Mise en œuvre', 'title_en' => "Implementation", "page_id" => $page5->id, "type_id" => $type1->id));

    // SOUS PAGE 6    
    $sp13 = Page::firstOrCreate(array('title_fr' => 'Engagement/endurance/résistance', 'title_en' => "Commitment/endurance/strength", "page_id" => $page6->id, "type_id" => $type1->id));

    // SOUS PAGE 7
    $sp14 = Page::firstOrCreate(array('title_fr' => 'Intégrité/congruence', 'title_en' => "Integrity/congruence", "page_id" => $page7->id, "type_id" => $type1->id));

    // SOUS PAGE 8
    $sp15 = Page::firstOrCreate(array("page_id" => $page8->id, "type_id" => $type1->id));

    // QUESTIONS PAGE 1 - 1
    $q1 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) est à l'aise quand il(elle) fait une présentation orale.",
      "question_en" => "Q1 - He (she) is comfortable when he (she) makes an oral presentation.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q2 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) structure clairement ce qu'il(elle) dit ou écrit.",
      "question_en" => "Q2 - He (she) clearly structures what he (she) says or writes.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q3 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) n’adapte pas ses messages à ses interlocuteurs.",
      "question_en" => "Q3 - He (she) does not adapt his messages to his interlocutors.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q4 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) rédige des textes clairs et concis",
      "question_en" => "Q4 - He / she writes clear and concise texts.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q5 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) utilise l’oral et l’écrit à bon escient dans une communication bienveillante.",
      "question_en" => "Q5 - He (she) uses the spoken word and writes it wisely in a benevolent communication.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 1 - 2
    $q6 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) monopolise la parole en réunion.",
      "question_en" => "Q1 - He (she) monopolizes the speech in meeting.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q7 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) répond avec transparence et discernement aux questions qui lui sont posées.",
      "question_en" => "Q2 - He (she) responds with transparency and discernment to the questions put to him.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q8 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) traite avec patience les réserves et objections.",
      "question_en" => "Q3 - He (she) patiently deals with reservations and objections.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q9 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) sait trouver des ressources pour garder la relation, même en situation délicate.",
      "question_en" => "Q4 - He (she) knows how to find resources to keep the relationship, even in a delicate situation.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q10 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) sait écouter et est capable de reformuler des positions différentes des siennes.",
      "question_en" => "Q5 - He (she) knows how to listen and is able to reformulate positions different from hers.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q11 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) définit les objectifs de son entité à partir des objectifs de l’entreprise.",
      "question_en" => "Q6 - He (she) defines the objectives of his entity based on the objectives of the company.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

     //QUESTION PAGE 2 - 1
    $q12 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) connait et comprend les enjeux globaux de l’entreprise.",
      "question_en" => "Q1 - He (she) knows and understands the global issues of the company.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q13 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) cherche à obtenir l’adhésion de ses collaborateurs aux objectifs de l’entreprise.",
      "question_en" => "Q2 - He (she) seeks to obtain the adhesion of his collaborators to the objectives of the company.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q14 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il définit les objectifs de son entité sans y associer ses collaborateurs.",
      "question_en" => "Q3 - It defines the objectives of its entity without associating its collaborators.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q15 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) définit clairement les priorités de son entité.",
      "question_en" => "Q4 - He (she) clearly defines the priorities of his entity.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q16 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) se positionne dans une perspective client en visant l’amélioration du Delivery",
      "question_en" => "Q5 - He (she) positions himself in a customer perspective by aiming at the improvement of the Delivery",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 2 - 2
    $q17 = Question::firstOrCreate([
      'question_fr' => "Q1 - Ses collaborateurs ont une vision claire des objectifs et des priorités.",
      "question_en" => "Q1 - Its employees have a clear vision of objectives and priorities.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q18 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) identifie et diffuse l’information pertinente de façon satisfaisante.",
      "question_en" => "Q2 - He (she) identifies and disseminates relevant information satisfactorily.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q19 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) organise la remontée des informations issues de son environnement.",
      "question_en" => "Q3 - He (she) organizes the feedback of information from his environment.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q20 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) court-circuite ses collaborateurs (ne respecte pas les niveaux de délégation).",
      "question_en" => "Q4 - He (she) bypasses his collaborators (does not respect the levels of delegation).",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q21 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle ) appuie sa crédibilité opérationnelle sur les différents niveaux de management (direction et terrain).",
      "question_en" => "Q5 - He (she) supports his operational credibility on the different levels of management (management and field).",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);    

    // QUESTION PAGE 2 - 3
    $q22 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) aide ses collaborateurs à progresser.",
      "question_en" => "Q1 - He (she) helps his employees to progress.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q23 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) ne valorise pas les succès de ses collaborateurs.",
      "question_en" => "Q2 - He (she) does not value the success of his collaborators.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q24 = Question::firstOrCreate([
      'question_fr' => "Q3 -  Il(elle) est disponible pour ses collaborateurs.",
      "question_en" => "Q3 - He (she) is available for his collaborators.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q25 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) recadre ou sanctionne ses collaborateurs quand c’est nécessaire.",
      "question_en" => "Q4 - He (she) reframes or sanctions his collaborators when it is necessary.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q26 = Question::firstOrCreate([
      'question_fr' => "Il(elle) est à l’écoute de ses collaborateurs même quand ceux-ci rencontrent des difficultés personnelles.",
      "question_en" => "Q5 - He (she) listens to his collaborators even when they face personal difficulties.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);     

    // QUESTION PAGE 2 - 4
    $q27 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) tranche quand c'est à lui(elle) de décider.",
      "question_en" => "Q1 - He (she) slice when it is up to him (her) to decide.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q28 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) recueille le point de vue de ses collaborateurs quand la décision les implique.",
      "question_en" => "Q2 - He (she) collects the point of view of his collaborators when the decision implicates them",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q29 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) délègue avec le niveau de contrôle suffisant.",
      "question_en" => "Q3 - He (she) delegates with the sufficient level of control.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q30 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) manifeste de l’initiative dans son domaine de responsabilité.",
      "question_en" => "Q4 - He (she) demonstrates initiative in his area of ​​responsibility.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q31 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) prend ses décisions sans consulter les personnes concernées.",
      "question_en" => "Q5 - He (she) makes his decisions without consulting the persons concerned.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 3 - 1
    $q32 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) contribue activement à la coopération entre directions.",
      "question_en" => "Q1 - He (she) actively contributes to cooperation between directorates.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q33 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il (elle) consacre du temps pour faire avancer les projets des autres.",
      "question_en" => "Q2 - He (she) devotes time to advance the projects of the others.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q34 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) comprend les contraintes et le fonctionnement des autres directions.",
      "question_en" => "Q3 - He (she) understands the constraints and the functioning of the other directions.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q35 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ne tient pas compte des contraintes des autres directions.",
      "question_en" => "Q4 - He (she) does not take into account the constraints of the other directions.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q36 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) est aussi sensible aux résultats de l’entreprise qu’aux résultats de sa direction.",
      "question_en" => "Q5 - He (she) is as sensitive to the results of the company as to the results of his management.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q37 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) protège l’équipe du stress extérieur.",
      "question_en" => "Q6 - He (she) protects the team from outside stress.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);        

    // QUESTION PAGE 3 - 2
    $q38 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) encourage la coopération entre directions d'une façon générale.",
      "question_en" => "Q1 - He (she) encourages cooperation between directorates generally.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q39 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) fait passer l’intérêt collectif avec les intérêts individuels.",
      "question_en" => "Q2 - It (it) makes pass the collective interest with the individual interests.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q40 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) sensibilise ses collaborateurs à l’esprit d’entraide au sein de l’entreprise.",
      "question_en" => "Q3 - He (she) sensitizes his collaborators to the spirit of mutual aid within the company.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q41 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) organise régulièrement des moments de convivialité dans son service.",
      "question_en" => "Q4 - He (she) regularly organizes moments of conviviality in his service.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q42 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) hésite à proposer son aide aux autres directions. ",
      "question_en" => "Q5 - He (she) is reluctant to offer his help to other directions.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q43 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) défend avec conviction l’intérêt de l’équipe auprès de ses Responsables.",
      "question_en" => "Q6 - He (she) defends with conviction the interest of the team with its managers.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 1
    $q44 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) maîtrise les situations de crise.",
      "question_en" => "Q1 - He (she) controls crisis situations.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q45 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) accepte les changements imprévus de priorités avec sérénité.",
      "question_en" => "Q2 - He (she) accepts unforeseen changes of priorities with serenity.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q46 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il (elle) est la force de propositions pour améliorer les livrables.",
      "question_en" => "Q3 - He (she) is the force of proposals to improve the deliverables.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q47 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) a du mal à accélérer son rythme en cas de besoin.",
      "question_en" => "Q4 - He (she) is struggling to accelerate his pace when needed.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q48 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) reste organisé(e) même sous la pression de l'urgence.",
      "question_en" => "Q5 - He (she) remains organized (e) even under the pressure of the urgency.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q49 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) rassure ses collaborateurs dans les situations de crise.",
      "question_en" => "Q6 - He (she) reassures his collaborators in crisis situations.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 2
    $q50 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) est stimulé par le changement.",
      "question_en" => "Q1 - He (she) is stimulated by change.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q51 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) favorise l’innovation pour améliorer l’efficacité.",
      "question_en" => "Q2 - He (she) promotes innovation to improve efficiency.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q52 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) semble regretter le passé.",
      "question_en" => "Q3 - He (she) seems to regret the past.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q53 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) peut travailler dans des environnements différents.",
      "question_en" => "Q4 - He (she) can work in different environments.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q54 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) manifeste des facilités à élaborer des solutions originales.",
      "question_en" => "Q5 - He (she) manifests facilities to develop original solutions.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 3
    $q55 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) recherche systématiquement les solutions les plus « confortables » (et non les solutions les plus pertinentes)",
      "question_en" => "Q1 - He (she) systematically searches for the most 'comfortable' solutions (and not the most relevant solutions).",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q56 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) accepte facilement de sortir des routines quand cela est pertinent.",
      "question_en" => "Q2 - He (she) readily agrees to exit routines when relevant.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q57 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) prend des risques réfléchis pour agir de façon efficace.",
      "question_en" => "Q3 - He (she) takes thoughtful risks to act effectively.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q58 = Question::firstOrCreate([
      'question_fr' => "Q4 - Dans une nouvelle démarche, il(elle) pense autant aux avantages possibles qu'aux inconvénients.",
      "question_en" => "Q4 - In a new approach, he (she) thinks as much about the possible advantages as the disadvantages.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q59 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) identifie les problèmes et alerte suffisamment tôt.",
      "question_en" => "Q5 - He (she) identifies problems and alerts early enough.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);       

    // QUESTION PAGE 5 - 1
    $q60 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) élabore et compare différentes solutions si nécessaire.",
      "question_en" => "Q1 - He (she) elaborates and compares different solutions if necessary",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q61 = Question::firstOrCreate([
      'question_fr' => "Q2 - Face à un problème, il(elle) recueille toute l’information pertinente à la décision.",
      "question_en" => "Q2 - Faced with a problem, he (she) collects all the information relevant to the decision.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q62 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) présente la situation avec une vue globale sur les enjeux.",
      "question_en" => "Q3 - He (she) presents the situation with a global view of the issues.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q63 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) n’anticipe pas les situations de crise.",
      "question_en" => "Q4 - He (she) does not anticipate crisis situations.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q64 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) propose des solutions constructives aux problèmes rencontrés.",
      "question_en" => "Q5 - He (she) proposes constructive solutions to the problems encountered.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q65 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) sait équilibrer les objectifs du client et ceux de son entreprise.",
      "question_en" => "Q6 - He (she) knows how to balance the objectives of the client and those of his company.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 5 - 2
    $q66 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) donne des objectifs précis et atteignables à son équipe.",
      "question_en" => "Q1 - He (she) gives precise objectives and achievable to his team.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q67 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) bâtit des plans d'actions pour soutenir ses projets.",
      "question_en" => "Q2 - He (she) builds action plans to support his projects.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q68 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) applique et fais appliquer rapidement les décisions prises.",
      "question_en" => "Q3 - He/she applies and quickly enforces the decisions made.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q69 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) suit l’avancement des plans d’actions jusqu’à leur aboutissement.",
      "question_en" => "Q4 - He (she) follows the progress of the action plans until their completion.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q70 = Question::firstOrCreate([
      'question_fr' => "Q5 - En cas de problème/crise, il(elle) identifie les moyens nécessaires aux actions et propose leur adaptation si nécessaire.",
      "question_en" => "Q5 - In the event of a problem/crisis, he (she) identifies the means necessary for the actions and proposes their adaptation if necessary.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q71 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) ne tient pas compte du contexte avant d'agir.",
      "question_en" => "Q6 - He (she) does not consider the context before acting.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q72 = Question::firstOrCreate([
      'question_fr' => "Q7 - Il(elle) se donne les moyens de présenter un travail fiable.",
      "question_en" => "Q7 - He (she) gives himself the means to present a reliable work.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 6 - 1
    $q73 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) assume les décisions même lorsqu'il(elle) ne les partage pas.",
      "question_en" => "Q1 - He (she) assumes the decisions even when he/she does not share them.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q74 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) a un haut niveau d’exigence pour lui(elle)-même.",
      "question_en" => "Q2 - He (she) has a high level of requirement for him (it) himself.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q75 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) va jusqu’au bout de ce qu’il(elle) entreprend.",
      "question_en" => "Q3 - He (she) goes to the end of what he (she) undertakes.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q76 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ajuste ses exigences à la situation.",
      "question_en" => "Q4 - He (she) adjusts his requirements to the situation.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q77 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) se décourage facilement devant les difficultés.",
      "question_en" => "Q5 - He (she) is easily discouraged by the difficulties.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 7
    $q78 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) connait et promeut les valeurs et la stratégie de l'entreprise.",
      "question_en" => "Q1 - He (she) knows and promotes the values ​​and strategy of the company.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q79 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) s’intéresse réellement aux autres.",
      "question_en" => "Q2 - He (she) really cares about others.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q80 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) traite de façon digne et respectueuse l’ensemble de ses interlocuteurs (internes et externes).",
      "question_en" => "Q3 - He (she) treats in a dignified and respectful way all his interlocutors (internal and external).",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q81 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ne respecte pas les procédures en vigueur dans l’entreprise.",
      "question_en" => "Q4 - He (she) does not respect the procedures in force in the company.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q82 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) incarne dans son management les valeurs de l’entreprise (sécurité, honnêteté, responsabilité, respect des personnes).",
      "question_en" => "Q5 - He (she) embodies in his management the values ​​of the company (safety, honesty, responsibility, respect for people).",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q83 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) fait remonter les infos pertinentes sur des risques potentiels.",
      "question_en" => "Q6 - He (she) traced the relevant information on potential risks.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTIONNAIRE PAGE 8 - 1
    $q84 = Question::firstOrCreate([
      'question_fr' => "Q1 - Quelles sont les qualités que vous appréciez chez cette personne ?",
      "question_en" => "Q1 - What qualities do you like about this person ?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    $q85 = Question::firstOrCreate([
      'question_fr' => "Q2 - Concernant son management, quelles sont les pistes d’amélioration que vous lui suggèreriez ?",
      "question_en" => "Q2 - Regarding its management, what are the areas of improvement that you would suggest to him ?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    $q86 = Question::firstOrCreate([
      'question_fr' => "Q3 - De façon plus générale, quelles propositions lui feriez-vous afin qu’elle soit encore plus efficiente ?",
      "question_en" => "Q3 - More generally, what proposals would you make to make it even more efficient?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    // REPONSE POUR CHAQUE QUESTION
    for($i= 1; $i< 84; $i++){

      // POSITIVE
      if(${"q".$i}->negative == 0){

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "pas du tout d'accord",
            "reponse_en" => "not agree at all",
            "score" => 1,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt pas d'accord",
            "reponse_en" => "rather disagree",
            "score" => 2,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt d'accord",
            "reponse_en" => "somewhat agree",
            "score" => 3,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "tout à fait d'accord",
            "reponse_en" => "Totally agree",
            "score" => 4,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "je ne sais pas",
            "reponse_en" => "I don't know",
            "score" => 0,
            "question_id" => ${"q".$i}->id
          ]
        );

      } else { // NEGATIVE

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "pas du tout d'accord",
            "reponse_en" => "not agree at all",
            "score" => 4,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt pas d'accord",
            "reponse_en" => "rather disagree",
            "score" => 3,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt d'accord",
            "reponse_en" => "somewhat agree",
            "score" => 2,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "tout à fait d'accord",
            "reponse_en" => "Totally agree",
            "score" => 1,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "je ne sais pas",
            "reponse_en" => "I don't know",
            "score" => 0,
            "question_id" => ${"q".$i}->id
          ]
        );
      }
    }
  }
}

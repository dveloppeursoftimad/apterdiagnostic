<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Question;
use App\Page;
use App\Reponse;
class InterlocuteurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = Type::firstOrCreate(array('name' => 'climage-interlocuteur', 'description' => "Climage interlocuteur"));
        $pageTrombi = Page::firstOrCreate(array('title_fr' => 'Mon profil personnel', 'title_en' => "My personal profile", "type_id" => $type->id));
        $pageForm1 = Page::firstOrCreate(array('title_fr' => 'Mon interlocuteur est…  (1/2)', 'title_en' => "My interlocutor is...  (1/2)", "type_id" => $type->id, 'subtitle_fr'=> "Je perçois que mon interlocuteur est plutôt …","subtitle_en"=>"I perceive that my interlocutor is rather..."));
        $pageForm2 = Page::firstOrCreate(array('title_fr' => 'Mon interlocuteur est…  (2/2)', 'title_en' => "My interlocutor is...  (2/2)", "type_id" => $type->id));
        // trombinoscope 1
		Question::firstOrCreate(
            [
                    'question_fr' => "Mon slogan personnel",
                    "question_en" => "My personnal Motto",
                    "titre_fr" => "slogan",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                    'question_fr' => "Ma plus grande fierté",
                    "question_en" => "What makes me proud",
                    "titre_fr" => "fierte",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                    'question_fr' => "Mon plus grand défi",
                    "question_en" => "My biggest challenge",
                    "titre_fr" => "defi",
                    'page_id' => $pageTrombi->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'text'
                ]
        );

        Question::firstOrCreate(
            [
                'question_fr' => "Mes centres d'intérêts",
                "question_en" => "My main interests",
                "titre_fr" => "interets",
                'page_id' => $pageTrombi->id,
                'type_id' => $type->id,
                "require" => 0,
                "type" => 'text'
            ]
        );

        for ($i=1; $i < 5; $i++) { 
        	${"question".$i} = Question::firstOrCreate(
                [
                    'question_fr' => "question".$i,
                    "question_en" => "question".$i,
                    'page_id' => $pageForm1->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'range'
                ]
            );
        }

        for ($i=5; $i < 9; $i++) { 
        	${"question".$i} = Question::firstOrCreate(
                [
                    'question_fr' => "question".$i,
                    "question_en" => "question".$i,
                    'page_id' => $pageForm2->id,
                    'type_id' => $type->id,
                    "require" => 0,
                    "type" => 'range'
                ]
            );
        }

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Insensé",
                "reponse_en" => "Senseless",
                "score" => 7,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Irréfléchi",
                "reponse_en" => "unthinking",
                "score" => 6,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Insouciant",
                "reponse_en" => "Insouciant",
                "score" => 5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Rassurant",
                "reponse_en" => "Reassuring",
                "score" => 4,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Serein",
                "reponse_en" => "Peaceful",
                "score" => 3,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Calme",
                "reponse_en" => "Calm",
                "score" => 2,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Apaisé",
                "reponse_en" => "Appeased",
                "score" => 1,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question1->id
            ]
        );

       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Négligeant",
                "reponse_en" => "Neglecting",
                "score" => 7,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Blasé",
                "reponse_en" => "Blasé",
                "score" => 6,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Indifférent",
                "reponse_en" => "Indifferent",
                "score" => 5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Détaché",
                "reponse_en" => "Detached",
                "score" => 4,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Imperturbable",
                "reponse_en" => "imperturbable",
                "score" => 3,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Flegmatique",
                "reponse_en" => "phlegmatic",
                "score" => 2,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Impassible",
                "reponse_en" => "Impassive",
                "score" => 1,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Combatif",
                "reponse_en" => "Combative",
                "score" => -1,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Impatient",
                "reponse_en" => "Impatient",
                "score" => -2,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Agacé",
                "reponse_en" => "Annoyed",
                "score" => -3,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Énervé",
                "reponse_en" => "Angry",
                "score" => -4,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Irrité",
                "reponse_en" => "Angry",
                "score" => -5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Exaspéré",
                "reponse_en" => "Exasperated",
                "score" => -6,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question2->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Agressif",
                "reponse_en" => "harsh",
                "score" => -7,
                "question_id" => $question2->id
            ]
        );

       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Contestataire",
                "reponse_en" => "Protest",
                "score" => 7,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question3->id
            ]
        );        

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Ironique",
                "reponse_en" => "Ironic",
                "score" => 6,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Moqueur",
                "reponse_en" => "Mocking",
                "score" => 5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Espiègle",
                "reponse_en" => "Mischievous",
                "score" => 4,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Joueur",
                "reponse_en" => "Player",
                "score" => 3,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Étonnant",
                "reponse_en" => "astonishing",
                "score" => 2,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Inventif",
                "reponse_en" => "Inventive",
                "score" => 1,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Impulsif",
                "reponse_en" => "Impulsive",
                "score" => 7,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question4->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Étourdi",
                "reponse_en" => "Thoughtless",
                "score" => 6,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Exubérant",
                "reponse_en" => "Exuberant",
                "score" => 5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Enthousiaste",
                "reponse_en" => "Enthusiastic",
                "score" => 4,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Gai",
                "reponse_en" => "Gai",
                "score" => 3,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Impliqué",
                "reponse_en" => "Involved",
                "score" => 2,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Passionné",
                "reponse_en" => "Passionate",
                "score" => 1,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question4->id
            ]
        );


		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Attentif",
                "reponse_en" => "Attentive",
                "score" => -1,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question1->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Vigilant",
                "reponse_en" => "Vigilant",
                "score" => -2,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Inquiet",
                "reponse_en" => "Worried",
                "score" => -3,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Anxieux",
                "reponse_en" => "Anxious",
                "score" => -4,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Effrayé",
                "reponse_en" => "Scared",
                "score" => -5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Crispé",
                "reponse_en" => "Uptight",
                "score" => -6,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question1->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fébrile",
                "reponse_en" => "Febrile",
                "score" => -7,
                "question_id" => $question1->id
            ]
        );        


        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Stimulant",
                "reponse_en" => "Stimulant",
                "score" => -1,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question3->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Effronté",
                "reponse_en" => "Cheeky",
                "score" => -2,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Critique",
                "reponse_en" => "Critical",
                "score" => -3,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Mécontent",
                "reponse_en" => "Discontented",
                "score" => -4,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Boudeur",
                "reponse_en" => "Sulky",
                "score" => -5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Râleur",
                "reponse_en" => "Grouch",
                "score" => -6,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Hostile",
                "reponse_en" => "Hostile",
                "score" => -7,
                "question_id" => $question3->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Attentif",
                "reponse_en" => "Attentive",
                "score" => -1,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question4->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Réservé",
                "reponse_en" => "Reserved",
                "score" => -2,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Désœuvré",
                "reponse_en" => "Désœuvré",
                "score" => -3,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Las (ennui)",
                "reponse_en" => "Boredom",
                "score" => -4,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Morose",
                "reponse_en" => "Morose",
                "score" => -5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fatigué",
                "reponse_en" => "Tired",
                "score" => -6,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question4->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Apathique",
                "reponse_en" => "Apathetic",
                "score" => -7,
                "question_id" => $question4->id
            ]
        );

		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Arrogant",
                "reponse_en" => "Arrogant",
                "score" => 7,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question5->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Supérieur",
                "reponse_en" => "Superior",
                "score" => 6,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Triomphant",
                "reponse_en" => "Triumphant",
                "score" => 5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fier",
                "reponse_en" => "To trust",
                "score" => 4,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Respecté",
                "reponse_en" => "Respected",
                "score" => 3,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fort",
                "reponse_en" => "Strong",
                "score" => 2,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Puissant",
                "reponse_en" => "Powerful",
                "score" => 1,
                "question_id" => $question5->id
            ]
        ); 

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question5->id
            ]
        ); 

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question5->id
            ]
        );     

		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Dépendant",
                "reponse_en" => "Reliant",
                "score" => 7,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question6->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Égocentré",
                "reponse_en" => "Aspirant",
                "score" => 6,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Comblé",
                "reponse_en" => "Fulfilled",
                "score" => 5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Reconnaissant",
                "reponse_en" => "Grateful",
                "score" => 4,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Redevable",
                "reponse_en" => "Indebted",
                "score" => 3,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Apprécié",
                "reponse_en" => "Enjoyed",
                "score" => 2,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Charmant",
                "reponse_en" => "Charming",
                "score" => 1,
                "question_id" => $question6->id
            ]
        ); 

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question6->id
            ]
        );

		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Flatteur",
                "reponse_en" => "Flattering",
                "score" => 7,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question7->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Hypocrite",
                "reponse_en" => "Hypocritical",
                "score" => 6,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Effacé",
                "reponse_en" => "Mousy",
                "score" => 5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Humble",
                "reponse_en" => "Humble",
                "score" => 4,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Honnête",
                "reponse_en" => "Honest",
                "score" => 3,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 2.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Modeste",
                "reponse_en" => "Modest",
                "score" => 2,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Dévoué",
                "reponse_en" => "Devoted",
                "score" => 1,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Envahissant",
                "reponse_en" => "Invasive",
                "score" => 7,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 6.5,
                "question_id" => $question8->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Débordant",
                "reponse_en" => "Overflowing",
                "score" => 6,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 5.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Clément",
                "reponse_en" => "Merticul",
                "score" => 5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 4.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Altruiste",
                "reponse_en" => "Altruistic",
                "score" => 4,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Bienveillant",
                "reponse_en" => "Benevolent",
                "score" => 3,
                "question_id" => $question8->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 3.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Généreux",
                "reponse_en" => "Generous",
                "score" => 2,
                "question_id" => $question8->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 1.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Vertueux",
                "reponse_en" => "Virtuous",
                "score" => 1,
                "question_id" => $question8->id
            ]
        );
        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => 0,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -0.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Courageux",
                "reponse_en" => "Courageous",
                "score" => -1,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question5->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Digne",
                "reponse_en" => "Worthy",
                "score" => -2,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Vexé",
                "reponse_en" => "Merticul",
                "score" => -3,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Rabaissé",
                "reponse_en" => "Belittled",
                "score" => -4,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Humilié",
                "reponse_en" => "Humiliated",
                "score" => -5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Ridiculisé",
                "reponse_en" => "Ridiculed",
                "score" => -6,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Abattu",
                "reponse_en" => "Shot",
                "score" => -7,
                "question_id" => $question5->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Affirmé",
                "reponse_en" => "Said",
                "score" => -1,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question6->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Demandeur",
                "reponse_en" => "Seeker",
                "score" => -2,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Déçu",
                "reponse_en" => "Disappointed",
                "score" => -3,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Négligé",
                "reponse_en" => "Scruffy",
                "score" => -4,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Rancunier",
                "reponse_en" => "Resentful",
                "score" => -5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Aigri",
                "reponse_en" => "bitter",
                "score" => -6,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Haineux",
                "reponse_en" => "Hate",
                "score" => -7,
                "question_id" => $question6->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Honorable",
                "reponse_en" => "Honourable",
                "score" => -1,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question7->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Bon perdant",
                "reponse_en" => "Good loser",
                "score" => -2,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Confus",
                "reponse_en" => "Confused",
                "score" => -3,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Honteux",
                "reponse_en" => "Shameful",
                "score" => -4,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "En tort",
                "reponse_en" => "Wrong",
                "score" => -5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Indigne",
                "reponse_en" => "Indignant",
                "score" => -6,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question7->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Lâche",
                "reponse_en" => "Loose",
                "score" => -7,
                "question_id" => $question7->id
            ]
        );

		Reponse::firstOrCreate(
            [
                'reponse_fr' => "Sincère",
                "reponse_en" => "sincere",
                "score" => -1,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -1.5,
                "question_id" => $question8->id
            ]
        );
        
       	Reponse::firstOrCreate(
            [
                'reponse_fr' => "Désolé",
                "reponse_en" => "Sorry",
                "score" => -2,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -2.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Navré",
                "reponse_en" => "Sorry",
                "score" => -3,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -3.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Coupable",
                "reponse_en" => "Guilty",
                "score" => -4,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -4.5,
                "question_id" => $question8->id
            ]
        );        

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Fautif",
                "reponse_en" => "Offending",
                "score" => -5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -5.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Caliméro",
                "reponse_en" => "Calimero",
                "score" => -6,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "",
                "reponse_en" => "",
                "score" => -6.5,
                "question_id" => $question8->id
            ]
        );

        Reponse::firstOrCreate(
            [
                'reponse_fr' => "Plaintif",
                "reponse_en" => "plaintive",
                "score" => -7,
                "question_id" => $question8->id
            ]
        );        
    }
}
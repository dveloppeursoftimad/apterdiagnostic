
  @extends('diagnostic')
  @section('content')

<div ng-app="climageFrontApp" ng-controller="climageFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">


			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('climage-front') }}">{{ __('message.climage') }}</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $climage->name }} </span> </h2>
						<hr>

						
						<div class="btn-group btn-group-toggle" style="font-family: Roboto,sans serif; display: block;">
							@foreach($pages as $p)
								
								<label ng-click="page('{{ $p->url }}')" class="btn btn-secondary {{ $page->id == $p->id ? 'active' : '' }}">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $p->{'title_'.$lang} }}
								</label>
								
								
							@endforeach
								<label ng-click="page('/diagnostic/climage/{{ $climage->id }}/{{ $climage->code }}/merci')" class="btn btn-secondary">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $lang == "en" ? "Result"  : "Résultat"}}
								</label>
						  		

						</div>


				
							<div class="row">
		

							  <div class="col-md-12">
							      <div class="card">
							          <div class="card-header card-header-text card-header-warning">
							            <div class="card-text">
							              <h3>{{ $page->{'title_'.$lang} }}</h3>
							            </div>
							          </div>
							          <div class="card-body">

							          	

							          	@foreach ($errors->all() as $error)
							                <div class="alert alert-danger">
											    <div class="container-fluid">
												  <div class="alert-icon">
												    <i class="material-icons">error_outline</i>
												  </div>
												  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true"><i class="material-icons">clear</i></span>
												  </button>
											      {{ $error }}
											    </div>
											</div>
							            @endforeach


							          	<form role="form" action="{{ route('climage-front-personal-post') }}" method="POST" enctype="multipart/form-data">
							                @csrf
							                @method('POST')
							                	<input type="hidden" name="code" value="{{ $climage->code }}">
							                	<input type="hidden" name="id" value="{{ $climage->id }}">
							                	

							                	<div class="col-md-12 row">
							                		<div class="col-md-3 text-center" ng-if="!loading" style="display:block;">
							                				@if(Auth::user()->avatar_url)
							                					<img style="height: 150px" id="imageSrc" src="{{ Auth::user()->avatar_url }}" alt="Avatar" class="rounded img-fluid">
							                				@else
							                					<img style="height: 150px" id="imageSrc" src="{{ url('/img/avatar.png') }}" alt="Avatar" class="rounded img-fluid">
							                				@endif
							                				

							                				
							                		</div>
							                		<div class="col-md-3 text-center" ng-if="loading">
							                				<div class="spinner-border text-warning" role="status">
															  <span class="sr-only">Loading...</span>
															</div>
							                		</div>
							                		<div class="col-md-3" style="display:block;">
							                				 <div class="form-group">
															    <label class="btn btn-warning  btn-round btn-reponse" for="avatar">@lang('message.avatar')</label>
															    <input type="file" class="form-control-file" id="avatar" name="avatar" onchange="previewFile()" >
															    <!-- <input type="file" class="form-control-file" id="avatar" name="avatar" ng-click="prepareImageUpload($event)"> -->
															  </div>
							                		</div>
							                		<div class="col-md-6" style="display:block;">
							                			<h4 class="card-title">@lang('message.mon_nom')</h4>
											            <input type="text" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);" class="form-control" name="nom"  placeholder="{{ __('message.mon_nom') }}" value="{{Auth::user()->firstname}}">

											            <h4 class="card-title">@lang('message.mon_prenom')</h4>
											            <input type="text" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);" class="form-control" name="prenom"  placeholder="{{ __('message.mon_prenom') }}" value="{{Auth::user()->lastname}}">

											            <h4 class="card-title">@lang('message.mon_organisation')</h4>
											            <input type="text" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);" class="form-control" name="organisation" value="{{ Auth::user()->organisation }}" placeholder="{{ __('message.mon_organisation') }}" >
							                		</div>
							                	</div>
									            @foreach ($page->questions as $quest)
									            	<h4 class="card-title">{{ $quest->{'question_'.$lang} }}</h4>
									            	@if ($quest->type == "text")
									            		<input 
									            		value="{{ $reponses[$quest->id]['reponse_text'] }}" 
									            		type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
									            		
									            	@endif

									            	@if($quest->type == "checkbox")
									            		<div class="row">
									            			@foreach ($quest->reponses as $reponse)
									            			<div class="col-md-6 col-sm-6">
									            				<div class="form-check">

									            					<div class="btn-group-toggle" data-toggle="buttons">
																	  <label class="btn btn-warning btn-round">
																	    <input type="checkbox" checked autocomplete="off" > {{ $reponse->{'reponse_'.$lang} }}
																	  </label>
																	</div>
																	      
																</div>
									            			</div>
									            			@endforeach
									            		</div>

												            		
									            		
									            	@endif

									            @endforeach

									            <br>
									            <div class="text-center">
									            	<button type="submit" style="font-family: inherit;" class="btn btn-warning btn-round" data-dismiss="modal" >{{ $lang == "en" ? "Save"  : "Enregistrer"}}</button>
									            </div></button>
									            </div>
							            </form>
							          </div>
							      </div>
							  </div>
							</div>

							
						    
						
						<!-- <h2 class="inscription">Accueil <i class="material-icons">chevron_right</i> Climage <i class="material-icons">chevron_right</i> </h2> -->
					</div>
		
			</div>
					

	
		</div>
	</div>

	



	

</div>
<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: inherit !important;
	}

	.reverse{
		-moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        filter: FlipH;
        -ms-filter: "FlipH";
	}

	.progress{
		height: 10px;
	}

	.btn-link{
		color: #FFB301 !important;
	}
</style>
<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });
	});

	function previewFile() {
	  var preview = document.getElementById('imageSrc');
	  var file    = document.querySelector('input[type=file]').files[0];
	  var reader  = new FileReader();

	  reader.addEventListener("load", function () {
	  	document.getElementById("imageSrc").src = reader.result;
	  	document.getElementById("avatar_url").value = reader.result;
	    preview.src = reader.result;
	  }, false);

	  if (file) {
	    reader.readAsDataURL(file);
	  }
	}
</script>
 <script type="text/javascript">
      var climageFrontApp = angular.module('climageFrontApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      climageFrontApp.controller('climageFrontController', function PhoneListController($scope) {
            $scope.cur = 0;
            $scope.question = [];
            $scope.imageSrc = null;
            $scope.loading = false;
            @foreach ($reponses as $rep)
            	$scope.question["reponse_"+"{{ $rep['id'] }}"] = "{{ $rep['reponse_text'] }}";

            @endforeach
            $scope.page = function(page){
            	window.location = page;
            }

            
      });


    </script>
<!--/div-->
@endsection('content')

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/403', function () {
    return view('403');
})->name('403');

// Route::get('/isma', function () {
//     return view('403');
// });

Route::get('/matrice', function () {
    return view('403');
});

Route::get('/updateapp', function()
{
    \Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});


// Route::get('/', 'HomeController@index')->name('home');


Route::redirect('/', '/diagnostics/');
// Route::get("/climage", "ClimageController@index")->name("climage");

Route::get('/locale/{locale}',function($locale){
	Session::put('locale',$locale);
	App::setLocale($locale);
	return redirect()->back();
});





// Route::resource('climage', 'ClimageController');


Route::get('/securite/oubliee', 'SecuriteController@oubliee')->name('oubliee');
Route::post('/securite/sendEmail', 'SecuriteController@sendEmail')->name('securite.sendEmail');
Route::get('/securite/changeMdp/{token}/{id}', 'SecuriteController@changeMdp')->name('securite.changeMdp');
Route::post('/securite/confirmChangeMdp', 'SecuriteController@confirmChangeMdp')->name('securite.confirmChangeMdp');

Route::get('/diagnostics/inscription', 'DiagnosticController@inscription')->name('inscription');

Route::get('/diagnostics/connexion', 'DiagnosticController@connexion')->name('connexion');

Route::get('/diagnostics/connexion-test/{isma}',function($isma){
	Session::put('isma',$isma);
	App::setLocale($isma);
	return view("/diagnostic/connexion");
});

Route::get('/diagnostics/connexion-test', 'DiagnosticController@connexion_test')->name('connexion-test');

Route::get('/diagnostics/deconnexion', 'DiagnosticController@deconnexion')->name('deconnexion');

Route::post('/diagnostics/authenticate', 'DiagnosticController@authenticate')->name('diagnostic-login');
Route::post('/diagnostics/store', 'DiagnosticController@store')->name('diagnostic.store');

Route::get('/diagnostics/feedback/{id}/{code}/questions/{idpage?}/{numero?}/{idRepondant?}', 'FeedbackFrontController@questions')->name("feedback-front-questions");

Route::get('/diagnostics/acti/{id}/{code}/questions_rep/{idpage?}/{numero?}/{idRepondant?}', 'ActiFrontController@questions_rep')->name("acti-front-questions");
//Route::get('/diagnostics/acti/{id}/{code}/questions_ben/{idpage?}/{numero?}/{idBeneficiaire?}', 'ActiFrontController@questions_ben')->name("acti-front-questions-beneficiaire");

//Route::get('/diagnostics/getConnexion', 'DiagnosticController@getConnexion')->name('getConnexion');

Auth::routes();

Route::get("/climage/codes", "ClimageController@codes");
Route::post("/climage/heures", "ClimageController@heures");
Route::post("/climage/resultat2", "ClimageController@resultat2");
Route::get("/feedback/codes", "FeedbackController@codes");
Route::get("/acti/codes", "ActiController@codes");

Route::group(['middleware' => 'App\Http\Middleware\ConsultantMiddleware'], function(){


	Event::listen('JeroenNoten\LaravelAdminLte\Events\BuildingMenu', function ($event) {    
        if(Auth::user()->role == "consultant" || Auth::user()->role == "partenaire"){
        	$event->menu->add([
	            'text' => trans('message.accueil'),
	            'url' => '/diagnostics',
	            'icon' => 'home',

	        ],
	        [
	            'text'    => trans('message.isma'),
	            'url'     => '/isma-liste',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/isma-liste',
	                  'icon' => 'list',
	                ]
	           	]
	        ],
	  		[
	            'text'    => trans('message.perf_motiv2'),
	            'url'     => '/acti',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/acti',
	                  'icon' => 'list',
	                ],
	                [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/acti/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text' => 'APTER 360° FB',
	           
	            'icon' => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/feedback',
	                  'icon' => 'list',
	                ],   
	                 [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/feedback/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text'    => 'APTER '.trans('message.climage'),
	            'url'     => '/climage',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/climage',
	                  'icon' => 'list',
	                ]
	            ]
	        ],
	        [
	            'text'    => trans('message.travaux_admin'),
	            'url'     => '/travaux-academic',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/travaux-academic',
	                  'icon' => 'list',
	                ]
	           	]
	        ]);
        } else if(Auth::user()->role == "admin") {
        	$event->menu->add([
	            'text' => trans('message.accueil'),
	            'url' => '/diagnostics',
	            'icon' => 'home',

	        ],
	        [
	            'text'    => trans('message.isma'),
	            'url'     => '/isma',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/isma',
	                  'icon' => 'list',
	                ],
	                [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/isma/questionnaire',
	                  'icon' => 'tasks',
	                ]
	           	]
	        ],
	  		[
	            'text'    => trans('message.perf_motiv2'),
	            'url'     => '/acti',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/acti',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/acti/questionnaire',
	                  'icon' => 'tasks',
	                ],
	                [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/acti/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text' => 'APTER 360° FB',
	           
	            'icon' => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/feedback',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/feedback/questionnaire',
	                  'icon' => 'tasks',
	                ],
	                
	                 [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/feedback/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text'    => 'APTER '.trans('message.climage'),
	            'url'     => '/climage',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/climage',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/climage/questionnaire',
	                  'icon' => 'tasks',
	                ]
	            ]
	        ],
	        [
	            'text'    => trans('message.travaux_admin'),
	            'url'     => '/travaux-academic',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/travaux-academic',
	                  'icon' => 'list',
	                ]
	           	]
	        ],       
	        [
	            'text'    => 'Consultants',
	            'url'     => 'consultant',
	            'icon'    => 'user-md',
	        ],
	        [
	            'text'    => trans('message.gest_mail'),
	            'url'     => '/mail',
	            'icon'    => 'envelope',
	        ],
	  
	        [
	            'text'    => trans('message.matr_rapport'),
	            'url'     => '/matrice',
	            'icon'    => 'file-text',
	        ]);
        } else if(Auth::user()->role == "admin_acad"){
        	$event->menu->add([
	            'text' => trans('message.accueil'),
	            'url' => '/diagnostics',
	            'icon' => 'home',

	        ],
	        [
	            'text'    => trans('message.isma'),
	            'url'     => '/isma-liste',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/isma-liste',
	                  'icon' => 'list',
	                ],
	                [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/isma/questionnaire',
	                  'icon' => 'tasks',
	                ]
	           	]
	        ],
	  		[
	            'text'    => trans('message.perf_motiv2'),
	            'url'     => '/acti',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/acti',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/acti/questionnaire',
	                  'icon' => 'tasks',
	                ],
	                [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/acti/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text' => 'APTER 360° FB',
	           
	            'icon' => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/feedback',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/feedback/questionnaire',
	                  'icon' => 'tasks',
	                ],
	                
	                 [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/feedback/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text'    => 'APTER '.trans('message.climage'),
	            'url'     => '/climage',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/climage',
	                  'icon' => 'list',
	                ],
	                 [
	                  'text' => trans('message.questionnaires'),
	                  'url'  => '/climage/questionnaire',
	                  'icon' => 'tasks',
	                ]
	            ]
	        ],
	        [
	            'text'    => trans('message.travaux_admin'),
	            'url'     => '/travaux-academic',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/travaux-academic',
	                  'icon' => 'list',
	                ]
	           	]
	        ]);
        } else if(Auth::user()->role == "admin_rech_acad"){
        	$event->menu->add([
	            'text' => trans('message.accueil'),
	            'url' => '/diagnostics',
	            'icon' => 'home',

	        ],
	        [
	            'text'    => trans('message.isma'),
	            'url'     => '/isma-liste',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/isma-liste',
	                  'icon' => 'list',
	                ]
	           	]
	        ],
	  		[
	            'text'    => trans('message.perf_motiv2'),
	            'url'     => '/acti',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/acti',
	                  'icon' => 'list',
	                ],
	                [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/acti/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text' => 'APTER 360° FB',
	           
	            'icon' => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/feedback',
	                  'icon' => 'list',
	                ],   
	                 [
	                  'text' => trans('message.gest_mail'),
	                  'url'  => '/feedback/mail',
	                  'icon' => 'envelope'
	                ]
	            ]
	        ],
	        [
	            'text'    => 'APTER '.trans('message.climage'),
	            'url'     => '/climage',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/climage',
	                  'icon' => 'list',
	                ]
	            ]
	        ],
	        [
	            'text'    => trans('message.travaux_admin'),
	            'url'     => '/travaux-academic',
	            'icon'    => 'bar-chart',
	            'submenu' => [
	                [
	                  'text' => trans('message.liste'),
	                  'url'  => '/travaux-academic',
	                  'icon' => 'list',
	                ]
	           	]
	        ]);
        }
    });

    Route::group(['middleware' => 'App\Http\Middleware\ActiAcess'], function(){

    	
		Route::get("/acti/questionnaire", "QuestionnaireController@acti")->name("acti-questionnaire");
		Route::get("/acti/mail", "ActiController@mail")->name("acti-mail");
		Route::get("/acti/mail/{id}/edit", "ActiController@mailEditActi")->name("acti-mail.edit");
		Route::get("/acti/resultatActi/{id}","ActiController@resultatActi");
		Route::resource('acti', 'ActiController');
	

	});

	Route::group(['middleware' => 'App\Http\Middleware\FeedbackAccess'], function(){


		Route::get("/feedback/questionnaire/{idQuestion?}/{active?}", "QuestionnaireController@feedback")->name("feedback-questionnaire");
		Route::get("/feedback/mail", "FeedbackController@mail")->name("feedback-mail");
		Route::get("/feedback/mail/{id}/edit", "FeedbackController@mailEdit")->name("feedback-mail.edit");
		Route::post("/feedback/mail/update", "FeedbackController@mailUpdate")->name("feedback-mail.update");
		Route::resource('feedback', 'FeedbackController');
		Route::post("/feedback/addTemplate", "FeedbackController@addTemplate")->name("feedback.addTemplate");
		Route::post("/feedback/addSPage", "FeedbackController@addSPage")->name("feedback.addSPage");


	});



	Route::group(['middleware' => 'App\Http\Middleware\IsmaAccess'], function(){
		Route::get('isma/questionnaire', 'QuestionnaireController@isma')->name("isma-questionnaire");
		Route::get('isma/isma_ajax', 'QuestionnaireController@isma_ajax');
		Route::get("isma/resultatIsma/{id}", "IsmaController@rapportIsma");
		Route::get('/isma/rapport_long/{id}/{date}', 'IsmaController@rapport_long')->name("isma-rapport-long");
		Route::get('/isma/rapport_court_fr/{id}/{date}', 'IsmaController@rapport_court_fr')->name("isma-rapport-court-fr");
		Route::post('/isma/rapport_court_fr_new_pptx', 'IsmaController@rapport_court_fr_new_pptx')->name("isma-rapport-court-fr-pptx");
		Route::get('/isma/rapport_court_fr_new/{id}/{date}/{type}', 'IsmaController@rapport_court_fr_new')->name("isma-rapport-court-fr-new");
		Route::get('/isma/liste_rapport_isma/{id}', 'IsmaController@liste_rapport_isma')->name("liste-rapport-isma");
		Route::get('/isma/rapport_court_en/{id}/{date}', 'IsmaController@rapport_court_en')->name("isma-rapport-court-en");
		Route::get('/isma/rapport_court_en_new/{id}/{date}/{type}', 'IsmaController@rapport_court_en_new')->name("isma-rapport-court-en-new");
		Route::post('/isma/rapport_court_en_new_pptx', 'IsmaController@rapport_court_en_new_pptx')->name("isma-rapport-court-en-new-pptx");
		Route::get('isma-liste', 'IsmaController@index');
		Route::resource('isma', 'IsmaController');


	});

	Route::group(['middleware' => 'App\Http\Middleware\ClimageAccess'], function(){
		// Event::listen('JeroenNoten\LaravelAdminLte\Events\BuildingMenu', function ($event) {

  //           $event->menu->add(
  //               [
	 //            'text'    => 'APTER Climage',
	 //            'url'     => '/climage',
	 //            'icon'    => 'bar-chart',
	 //            'submenu' => [
	 //                            [
	 //                              'text' => 'Liste',
	 //                              'url'  => '/climage',
	 //                              'icon' => 'list',
	 //                            ],
	 //                             [
	 //                              'text' => 'Questionnaires',
	 //                              'url'  => '/climage/questionnaire',
	 //                              'icon' => 'tasks',
	 //                            ]
	 //                         ]
  //       	]
  //      	);
  //       });

		Route::get('climage/questionnaire', 'QuestionnaireController@climage')->name("climage-questionnaire");
		Route::resource('climage', 'ClimageController');
	});

	Route::resource('mail', 'MailController');
	Route::get('/mails', 'MailController@index')->name("mails");
	// Route::get('climage/questionnaire', 'QuestionnaireController@climage')->name("climage-questionnaire");
	// Route::resource('climage', 'ClimageController');



	Route::get('/consultant/getData/{id}','ConsultantController@getData');
	Route::post('/consultant/sendMail','ConsultantController@sendMail');

	Route::get("/climage/resultat/{id}", "ClimageController@resultat")->name("climage.resultat");

	Route::get("/climage/resultatClimage2/{id}", "ClimageController@tableauClimage2")->name("climage.tableauClimage2");


	Route::get("/climage/reponse/{id}", "ClimageController@reponse")->name("climage.reponse");
	Route::get("/climage/resultatClimage/{id}", "ClimageController@tableauClimage")->name("climage.tableauClimage");


	Route::post("/climage/deleteInfoClimage", "ClimageController@deleteInfoClimage");


	Route::resource('questions', 'QuestionController');
	Route::resource('reponses', 'ReponseController');
	Route::post('reponses/question', 'ReponseController@question');
	Route::post('reponses/questions', 'ReponseController@questions');
	Route::post('questions/question', 'QuestionController@question');
	Route::post('questions/supp_question', 'QuestionController@supp_question');
	Route::post('questions/souspages', 'QuestionController@souspages');
	Route::post('questions/suppSouspages', 'QuestionController@suppSouspages');
	Route::post('questions/pages', 'QuestionController@pages');
	/*Route::get('reponses/questions', 'ReponseController@get');*/
	Route::post('questions/question_isma', 'QuestionController@question_isma');
	Route::post('reponses/reponse_isma', 'ReponseController@question_isma');






	Route::get("/acti/repondantActi/{id}", "ActiController@repondants");
	Route::get("/feedback/repondant/{id}", "QuestionnaireController@repondant");
	Route::get("/feedback/resultatFeedback/{id}", "FeedbackController@resultatFeedback");
	Route::get("/feedback/rapport/{id}", "FeedbackController@rapport");
	Route::get("/acti/rapportActi/{id}/{type}", "ActiController@rapportActi");
	Route::post("/acti/rapportActiPPTX", "ActiController@rapportActiPPTX");
	Route::get("/feedback/getTestRepondant", "QuestionnaireController@getTestRepondant");
  Route::post("/feedback/downloadDocx", "FeedbackController@downloadDocx");
  Route::post("/feedback/downloadPptx", "FeedbackController@downloadPptx");
  Route::post("/feedback/downloadPdf/", "FeedbackController@downloadPdf");
  //Route::get("/feedback/downloadPdf/{id}", "FeedbackController@downloadPdf");

	//Route::post("/feedback/createrepondant", "QuestionnaireController@createrepondant");
	Route::post("/feedback/deleteFromTest","QuestionnaireController@deleteFromTest");
	Route::post("/feedback/sendMailSpec","QuestionnaireController@sendMailSpec");
	Route::post("/feedback/sendMailRelance","QuestionnaireController@sendMailRelance");
	Route::post("/feedback/sendMailGlobaly","QuestionnaireController@sendMailGlobaly");
	Route::post("/feedback/checkGlobaly","QuestionnaireController@checkGlobaly");
	Route::post("/feedback/checkMail", "QuestionnaireController@checkMail")->name("QuestionnaireController.checkMail");
	Route::post("/feedback/checkMails", "QuestionnaireController@checkMails")->name("QuestionnaireController.checkMails");

	Route::post("/acti/sendMailRelanceActi","QuestionnaireController@sendMailRelanceActi");
	Route::post("/acti/sendMailGlobalyActi","QuestionnaireController@sendMailGlobalyActi");
	Route::post("/acti/checkGlobalyActi","QuestionnaireController@checkGlobalyActi");
	Route::post("/acti/sendMailSpecActi","QuestionnaireController@sendMailSpecActi");
	Route::post("/acti/deleteFromTestActi","QuestionnaireController@deleteFromTestActi");
	Route::post("/acti/checkMailsActi", "QuestionnaireController@checkMailsActi")->name("QuestionnaireController.checkMailsActi");
	Route::post("/acti/checkMailActi", "QuestionnaireController@checkMailActi")->name("QuestionnaireController.checkMailActi");

	Route::post("/feedback/createrepondant", "ConsultantController@createrepondant");
	Route::post("/feedback/editionRepondant/{id}", "ConsultantController@updateRepondant");
	Route::post("/feedback/addRepondantTo", "ConsultantController@addRepondantTo");
	Route::get("/feedback/getReponseRepondant/{id}", "RapportController@getReponseRepondant");
	Route::get("/feedback/allQuestion/{id}", "RapportController@allQuestion");

	Route::post("/acti/addRepondantToActi", "ConsultantController@addRepondantToActi");
	Route::post("/acti/editionRepondantActi/{id}", "ConsultantController@updateRepondantActi");
	Route::post("/acti/createrepondantActi", "ConsultantController@createrepondantActi");


	Route::get('/mail', 'MailController@index');
	Route::post('/mail/modification', 'MailController@modification');
	Route::post('/mail/modificationPartielle', 'MailController@modificationPartielle');
	Route::post('/mail/updateTemplate', 'MailController@updateTemplate');
	Route::get('/mail/getMail', 'MailController@getMail');

	Route::get('/mail/getType', 'MailController@getType');
	Route::post('/mail/addSubject', 'MailController@addSubject');
	Route::post('/mail/saveSubject', 'MailController@saveSubject');


	//Route::resource('feedback', 'FeedbackController');

	Route::get('/travaux-academic','TravauxController@index');

});



Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function(){
	// Event::listen('JeroenNoten\LaravelAdminLte\Events\BuildingMenu', function ($event) {

 //            $event->menu->add(
 //                [
	// 	            'text'    => 'Consultants',
	// 	            'url'     => 'consultant',
	// 	            'icon'    => 'user-md',
	// 	        ],
	// 	        [
	// 	            'text'    => 'Gestion des mails',
	// 	            'url'     => '/mail',
	// 	            'icon'    => 'envelope',
	// 	        ]
 //       	);
 //    });



	Route::get('/consultant/getData/{id}','ConsultantController@getData');

	Route::get('/consultant/getConsultantAccess/{id}','ConsultantController@getConsultantAccess');
	Route::post('/consultant/setConsultantAccess','ConsultantController@setConsultantAccess')->name("consultant.setConsultantAccess");
	Route::resource('consultant', 'ConsultantController');


});


Route::group(['middleware' => 'App\Http\Middleware\RepondantMiddleware'], function(){
	Route::get('diagnostics', 'DiagnosticController@index')->name('diagnostics');
	Route::get('/diagnostics/home', 'DiagnosticController@index')->name('diagnostic');
	Route::get('/diagnostics/climage', 'ClimageFrontController@index')->name("climage-front");

	Route::get('/diagnostics/feedback', 'FeedbackFrontController@index')->name("feedback-front");
	Route::get('/diagnostics/acti', 'ActiFrontController@index')->name("acti-front");
	Route::post('/diagnostics/climage/deleteInfoClimage', 'ClimageFrontController@deleteInfoClimage');
	
	Route::get('/diagnostics/climage/{id}/{code}/personal', 'ClimageFrontController@personal')->name("climage-front-personal");


	Route::post('/diagnostics/climage/personal', 'ClimageFrontController@savePersonal')->name("climage-front-personal-post");
	Route::get('/diagnostics/climage/{id}/{code}/formTwo', 'ClimageFrontController@formTwo')->name("climage-front-formTwo");

	Route::post('/diagnostics/climage/formTwo', 'ClimageFrontController@saveFormTwo')->name("climage-front-formTwo-post");

	Route::get('/diagnostics/climage/{id}/{code}/formThree', 'ClimageFrontController@formThree')->name("climage-front-formThree");

	Route::post('/diagnostics/climage/formThree', 'ClimageFrontController@saveFormThree')->name("climage-front-formThree-post");

	Route::get('/diagnostics/climage/{id}/{code}/merci', 'ClimageFrontController@merci')->name("climage-front-merci");

	Route::get('/diagnostics/climage/{id}/{code}/trombinoscope', 'ClimageFrontController@trombinoscope')->name("climage-front-trombinoscope");

	Route::post('/diagnostics/feedback/saveFeedback', 'FeedbackFrontController@saveReponse')->name("feedback-front-save");
	Route::post('/diagnostics/acti/saveActi', 'ActiFrontController@saveReponse')->name("acti-front-save");
	
	Route::post('/diagnostics/feedback/saveUsers', 'FeedbackFrontController@saveUsers')->name("feedback-front-saveUser");
	Route::post('/diagnostics/acti/saveUsers', 'ActiFrontController@saveUsers')->name("360diagnostic-front-saveUser");

	// ISMA
	Route::get('/diagnostics/isma/{idConsultant}/form', 'IsmaFrontController@questionnaire')->name("isma-front-form");
	Route::post('/diagnostics/isma/save', 'IsmaFrontController@save')->name("isma-front-save");



});

Route::post('/diagnostics/isma/saveUser', 'IsmaFrontController@saveUser')->name("isma-front-saveUser");
Route::post('/diagnostics/climage/saveUsers', 'ClimageFrontController@saveUsers')->name("climage-front-saveUser");

// Route::post('/diagnostic/login', 'DiagnosticController@login')->name('login');




Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

// Route::get('/consultant/index', 'ConsultantController@index')->name('consultant');

//Route::get('/importisma', 'wpismaController@importer');
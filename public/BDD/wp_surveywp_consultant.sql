-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: db692731209.db.1and1.com
-- Generation Time: Sep 10, 2019 at 07:33 AM
-- Server version: 5.5.60-0+deb7u1-log
-- PHP Version: 7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db692731209`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_surveywp_consultant`
--

CREATE TABLE `wp_surveywp_consultant` (
  `id_consultant` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `organisation` varchar(255) NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `add_by` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_surveywp_consultant`
--

INSERT INTO `wp_surveywp_consultant` (`id_consultant`, `nom`, `prenom`, `phone`, `organisation`, `date_add`, `add_by`, `password`, `statut`, `email`) VALUES
(13, 'Vandeventer', 'Laurence', '+33615571870', 'Apter France', '2017-07-06 18:41:13', 2, '7ca570d575380f66bb9553e251a73daa', 'active', 'laurence@apter-france.com'),
(18, 'Lunacek', 'Christophe', '+33609651791', 'Apter France', '2017-07-07 09:22:38', 3, '7ca570d575380f66bb9553e251a73daa', 'active', 'christophe@apter-france.com'),
(19, 'Lincker', 'Frédéric', '+33 6 85 30 43 01', 'Apter France', '2017-07-13 18:16:35', 3, '7ca570d575380f66bb9553e251a73daa', 'active', 'frederic@apter-france.com'),
(20, 'Mathiaux', 'Françoise', '+33 6 88 87 12 98', 'Ressources et Vous', '2017-07-28 17:41:14', 3, '064ca6422cc869d56c28b1a54fe768d6', 'active', 'fmathiaux@ressource-et-vous.com'),
(21, 'Taylor-Knox', 'Helena', '+44 (0)7939 576595', 'One Continuum Ltd 139b County Road, Swindon. Wiltshire. SN12EB', '2017-09-28 09:44:09', 3, 'c243b5f4592da0ec2892c3d3f08783d0', 'active', 'helena@onecontinuum.co.uk'),
(23, 'Andrianarimanana', 'Tsiory', '+261332043107', 'Développeur Malt', '2017-10-25 08:45:50', 6, 'e10adc3949ba59abbe56e057f20f883e', 'active', 'tsiory.andrianarimanana@gmail.com'),
(24, 'DUVALLET', 'Alexandra', '+212691410163', 'Apter Maroc', '2017-11-27 09:20:06', 3, 'fe427ccadcebd83ef041570107545128', 'active', 'alexandra@apter-maroc.com'),
(25, 'EL-MAZIL', 'Imane', '+212 660 17 17 35', 'Apter Maroc', '2017-11-27 09:24:01', 3, 'fe427ccadcebd83ef041570107545128', 'active', 'imane@apter-maroc.com'),
(26, 'LUTAAYA', 'RONALD', '+441111111111', 'Lutaaya', '2018-10-05 08:30:08', 9, 'a3dd5d9a5a65ef6c7bfd816f24d3f882', 'active', 'lutaaya903@btinternet.com'),
(27, 'Andrianaivomanana', 'Nirina', '0000000000', 'Optimus', '2018-10-11 13:46:52', 3, 'bb3cef07be30270e6f02f8af1f9a24c3', 'active', 'nandrianaivomanana@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_surveywp_consultant`
--
ALTER TABLE `wp_surveywp_consultant`
  ADD PRIMARY KEY (`id_consultant`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_surveywp_consultant`
--
ALTER TABLE `wp_surveywp_consultant`
  MODIFY `id_consultant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

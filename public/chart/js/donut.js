function test(vari){
  new Chart(document.getElementById("radar-chart"), {
      type: 'radar',
      data: {
        labels: ["SERIEUX ("+vari+")", "Transgressif Liberte", "Enjoue Activite", "Conforme Devoir"],
        datasets: [
          {
            fill: true,
            backgroundColor: "rgba(179,181,198,1)",
            borderColor: "rgba(179,181,198,1)",
            pointBorderColor: "#fff",
            pointBackgroundColor: "rgba(179,181,198,1)",
            data: [73,30,24,59]
          }
        ]
      },
      options: {
        maintainAspectRatio: true,
        responsive:true,
        legend:{
          display:false,
        },
        scale: {
           ticks: {
              display: false,
              maxTicksLimit: 1
           },
           pointLabels:{
             fontColor:"#FFF",
          },
        },
        gridLines: {
           display: false
        },
      }
    });
}

$(function(){

  var ctx1 = $("#doughnut-chartcanvas-1");

  var data1 = {
    labels: [],
    datasets: [{
      data: [50, 100, 50],
      backgroundColor: ["#b8cef9", "#efdf8d", "#b8cef9"],
      borderWidth: [0, 0, 0]
    }]
  };

  var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "L'ACTIVITE",
      fontSize: 14,
      fontColor: "#111"
    },
    cutoutPercentage: 75,
  };

  var chart1 = new Chart(ctx1, {
    type: "doughnut",
    data: data1,
    options: options
  });

  var ctx2 = $("#doughnut-chartcanvas-2");

  var data2 = {
    datasets: [{
      data: [50, 100, 50],
      backgroundColor: ["#d8adff", "#c4e2b7", "#d8adff"],
      borderWidth: [0, 0, 0]
    }],
    labels: [/*'January', 'February', 'March', 'April'*/]
  };

  var options2 = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "LA RELATION",
      fontSize: 14,
      fontColor: "#111"
    },
    cutoutPercentage: 75,
  };

  var chart2 = new Chart(ctx2, {
    type: "doughnut",
    data: data2,
    options: options2
  });

  // var c = document.getElementById("myCanvas");
  // var ctx = c.getContext("2d");
  // ctx.fillStyle = "rgba(179,181,198,1)";
  // ctx.fillRect(38, 18, 126, 20);

  // var c2 = document.getElementById("myCanvas2");
  // var ctx3 = c2.getContext("2d");
  // ctx3.fillStyle = "rgba(179,181,198,1)";
  // ctx3.fillRect(48, 18, 126, 20);

  // var c3 = document.getElementById("myCanvas3");
  // var ctx4 = c3.getContext("2d");
  // ctx4.fillStyle = "rgba(179,181,198,1)";
  // ctx4.fillRect(48, 18, 36, 20);
  
  var conforme = "Conforme"

  test(10);

  new Chart(document.getElementById("radar-chart1"), {
    type: 'radar',
    data: {
      labels: ["SERIEUX (realisation)", "Transgressif Liberte", "Enjoue Activite", conforme],
      datasets: [
        {
        	backgroundColor: "rgba(179,181,198,1)",
          borderColor: "rgba(179,181,198,1)",
          pointBorderColor: "#fff",
          pointBackgroundColor: "rgba(179,181,198,1)",
          data: [30,76,59,24],
        }
      ]
    },
    options: {
    	maintainAspectRatio: true,
    	responsive:true,
    	legend:{
    		display:false,
    	},
    	scale: {
			   ticks: {
			      display: false,
			      maxTicksLimit: 1
			   },
			},
			gridLines: {
			  display: false
			},
    }
  });

});

var app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});
app.controller('myCtrl', function($scope,$http) { 
  var ctx = document.getElementById('myChart');
  var ctx1 = document.getElementById('myChart1');
  var dataSet ={
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [{
              label: '# of Votes',
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      };
  var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: dataSet,
      options: {
          scales: {
              xAxes: [{
                  stacked: false,
                 
              }],
              yAxes: [{
                  stacked: false,

              }]
          },
          animation: false
      }
  });
  var myChart1 = new Chart(ctx1, {
      type: 'horizontalBar',
      data: dataSet,
      options: {
          scales: {
              xAxes: [{
                  stacked: false,
                 
              }],
              yAxes: [{
                  stacked: false,

              }]
          },
          animation: false
      }
  });
  var dataUrl = ctx.toDataURL('image/png', 1);
  var dataUrl1 = ctx1.toDataURL('image/png', 1);
  $scope.src = dataUrl;
  $scope.src1 = dataUrl1;
  console.log(dataUrl)
});
var app = angular.module('app', ['angularTrix'],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});
app.controller('myCtrl', function($scope,$http) {
	let response = [];
	$scope.relanceBeneficiaireId = undefined;
	$scope.subjectBenInvitationFR=[];
	$scope.subjectBenRemerciementFR=[];
	$scope.subjectRepInvitationFR=[];
	$scope.subjectRepRemerciementFR=[];
	$scope.subjectRelanceFR=[];

	$scope.subjectBenInvitationEN=[];
	$scope.subjectBenRemerciementEN=[];
	$scope.subjectRepInvitationEN=[];
	$scope.subjectRepRemerciementEN=[];
	$scope.subjectRelanceEN=[];

	$scope.templateBenInvitationFR=[];
	$scope.templateBenRemerciementFR=[];
	$scope.templateRepInvitationFR=[];
	$scope.templateRepRemerciementFR=[];
	$scope.templateRelanceFR=[];

	$scope.templateBenInvitationEN=[];
	$scope.templateBenRemerciementEN=[];
	$scope.templateRepInvitationEN=[];
	$scope.templateRepRemerciementEN=[];
	$scope.templateRelanceEN=[];


	$scope.beneficiaireInvitationFR=[];
	$scope.beneficiaireRemerciementFR=[];
	$scope.repondantInvitationFR=[];
	$scope.repondantRemerciementFR=[];
	$scope.relanceFR=[];

	$scope.beneficiaireInvitationEN=[];
	$scope.beneficiaireRemerciementEN=[];
	$scope.repondantInvitationEN=[];
	$scope.repondantRemerciementEN=[];
	$scope.relanceEN=[];
	let localhostUrl=window.location.protocol + '//' + window.location.host //'http://localhost:8000'; 

    var events = ['trixInitialize', 'trixChange', 'trixSelectionChange', 'trixFocus', 'trixBlur', 'trixFileAccept', 'trixAttachmentAdd', 'trixAttachmentRemove'];

    for (var i = 0; i < events.length; i++) {
        $scope[events[i]] = function(e) {
            console.info('Event type:', e.type);
            console.log($scope.repondantFR);
        }
    };
    $http({
    		method:'GET',
  			url:localhostUrl+'/mail/getMail',
  			headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
    }).then(function successCallback(data){ 
    		response = data;
            //console.log('mail',response);
           // console.log('constulatn', response.data[2]);
            for(var k =0;k<response.data.length;k++){
            	if(response.data[k].type=='consultant' && response.data[k].language=='fr' && response.data[k].active==1){
            		$scope.bodyConsultantFR=response.data[k].mail
            		$scope.subjectConsultantFR=response.data[k].subject;
            		$scope.consultantFR=response.data[k].id;
            	}
            	if(response.data[k].type=='consultant' && response.data[k].language=='en' && response.data[k].active==1){
            		$scope.bodyConsultantEN=response.data[k].mail
            		$scope.subjectConsultantEN=response.data[k].subject;
            		$scope.consultantEN=response.data[k].id;
            	}
            }
            $http({
		    		method:'GET',
		  			url:localhostUrl+'/mail/getType',
		  			headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
		    }).then(function successCallback(data){ 
		    		type = data.data;
		            for(var i=0;i<type.length;i++){
		            	for(var j=0;j<response.data.length;j++){
		            		if(response.data[j].type_id == type[i].id && response.data[j].active ==1 &&response.data[j].language =='fr'){
		            			switch(response.data[j].type){
		            				case 'invitation beneficiaire':
									    //code
									    $scope.templateBenInvitationFR[type[i].id] = response.data[j].id;
									    $scope.beneficiaireInvitationFR[type[i].id] = response.data[j].mail;
									    $scope.subjectBenInvitationFR[type[i].id]=response.data[j].subject;
									    console.log('invitation beneficiaire',$scope.templateBenInvitationFR[type[i].id]);
									    console.log($scope.templateBenInvitationFR);
									    console.log(type[i].id);
									    break;

									case 'remerciement beneficiaire':
									    //code
									    $scope.templateBenRemerciementFR[type[i].id] = response.data[j].id;
									    $scope.beneficiaireRemerciementFR[type[i].id] = response.data[j].mail;
									    $scope.subjectBenRemerciementFR[type[i].id] = response.data[j].subject;
									    break;
									case 'invitation repondant':
									    //code
									    $scope.templateRepInvitationFR[type[i].id] = response.data[j].id;
									    $scope.repondantInvitationFR[type[i].id] = response.data[j].mail;
									    $scope.subjectRepInvitationFR[type[i].id] = response.data[j].subject;
									    break;
									case 'remerciement repondant':
									    //code
									    $scope.templateRepRemerciementFR[type[i].id] = response.data[j].id;
									    $scope.repondantRemerciementFR[type[i].id] = response.data[j].mail;
									    $scope.subjectRepRemerciementFR[type[i].id] = response.data[j].subject;
									    break;
									case 'relance':
									    //code
									    $scope.templateRelanceFR[type[i].id] = response.data[j].id;
									    $scope.relanceFR[type[i].id] = response.data[j].mail;
									    $scope.subjectRelanceFR[type[i].id] = response.data[j].subject;
									    break;
									 default:
					    				// code block
		            			}
		            		}
		            		else if(response.data[j].type_id == type[i].id && response.data[j].active ==1 &&response.data[j].language =='en'){
		            			switch(response.data[j].type){
		            				case 'invitation beneficiaire':
									    //code
									    $scope.templateBenInvitationEN[type[i].id] = response.data[j].id;
									    $scope.beneficiaireInvitationEN[type[i].id] = response.data[j].mail;
									    $scope.subjectBenInvitationEN[type[i].id]=response.data[j].subject;
									    break;

									case 'remerciement beneficiaire':
									    //code
									    $scope.templateBenRemerciementEN[type[i].id] = response.data[j].id;
									    $scope.beneficiaireRemerciementEN[type[i].id] = response.data[j].mail;
									    $scope.subjectBenRemerciementEN[type[i].id]=response.data[j].subject;
									    break;
									case 'invitation repondant':
									    //code
									    $scope.templateRepInvitationEN[type[i].id] = response.data[j].id;
									    $scope.repondantInvitationEN[type[i].id] = response.data[j].mail;
									    $scope.subjectRepInvitationEN[type[i].id]=response.data[j].subject;
									    break;
									case 'remerciement repondant':
									    //code
									    $scope.templateRepRemerciementEN[type[i].id] = response.data[j].id;
									    $scope.repondantRemerciementEN[type[i].id] = response.data[j].mail;
									    $scope.subjectRepRemerciementEN[type[i].id]=response.data[j].subject;
									    break;
									case 'relance':
									    //code
									    $scope.templateRelanceEN[type[i].id] = response.data[j].id;
									    $scope.relanceEN[type[i].id] = response.data[j].mail;
									    $scope.subjectRelanceEN[type[i].id]=response.data[j].subject;
									    break;
									 default:
					    				// code block
		            			}
		            		}
		            	}
		            }

		    	},function errorCallback(response){
		    		console.log(response);
		    });
    	},function errorCallback(response){
    		console.log(response);
    });
		    
    function setUpMail(){
        
    };
    $scope.subjectFR = function subjectFR(){
    	console.log("reponse",response.data);
    	console.log('id',$scope.consultantFR)
    	for(var i =0;i<response.data.length;i++){
    		if(response.data[i].id==$scope.consultantFR){
    			$scope.bodyConsultantFR=response.data[i].mail;
    			$scope.subjectConsultantFR=response.data[i].subject;
    		}
    	}	 
    }
    $scope.subjectEN = function subjectEN(){
    	console.log("reponse",response.data);
    	console.log('id',$scope.consultantEN)
    	for(var i =0;i<response.data.length;i++){
    		if(response.data[i].id==$scope.consultantEN){
    			$scope.bodyConsultantEN=response.data[i].mail;
    			$scope.subjectConsultantEN=response.data[i].subject;
    		}
    	}	
    }
    $scope.update = function update(idType,nomVariable){
    	console.log('idtype',idType);
    	console.log(nomVariable);
    	console.log($scope.templateBenInvitation);
    	switch(nomVariable) {
			  case 'beneficiaireInvitationFR':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateBenInvitationFR[idType]){
	    				$scope.beneficiaireInvitationFR[idType] = response.data[i].mail;
	    				$scope.subjectBenInvitationFR[idType]= response.data[i].subject;
	    				console.log($scope.beneficiaireInvitationFR[idType])
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'beneficiaireRemerciementFR':
			    // code block
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateBenRemerciementFR[idType]){
	    				$scope.beneficiaireRemerciementFR[idType] = response.data[i].mail;
	    				$scope.subjectBenRemerciementFR[idType]= response.data[i].subject;
	    				console.log($scope.beneficiaireRemerciementFR[idType])
	    				break;
	    			}
	    		}
			    break;

			  case 'repondantInvitationFR':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRepInvitationFR[idType]){
	    				$scope.repondantInvitationFR[idType] = response.data[i].mail;
	    				$scope.subjectRepInvitationFR[idType] = response.data[i].subject;
	    				console.log($scope.repondantInvitationFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'repondantRemerciementFR':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRepRemerciementFR[idType]){
	    				$scope.repondantRemerciementFR[idType] = response.data[i].mail;
	    				$scope.subjectRepRemerciementEN[idType] = response.data[i].subject;
	    				console.log($scope.repondantRemerciementFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'relanceFR':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRelanceFR[idType]){
	    				$scope.relanceFR[idType] = response.data[i].mail;
	    				$scope.subjectRelanceFR[idType] = response.data[i].subject;
	    				console.log($scope.relanceFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;    
			  
			  case 'beneficiaireInvitationEN':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateBenInvitationEN[idType]){
	    				$scope.beneficiaireInvitationEN[idType] = response.data[i].mail;
	    				console.log($scope.beneficiaireInvitationEN[idType])
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'beneficiaireRemerciementEN':
			    // code block
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateBenRemerciementEN[idType]){
	    				$scope.beneficiaireRemerciementEN[idType] = response.data[i].mail;
	    				console.log($scope.beneficiaireRemerciementEN[idType])
	    				break;
	    			}
	    		}
			    break;

			  case 'repondantInvitationEN':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRepInvitationEN[idType]){
	    				$scope.repondantInvitationEN[idType] = response.data[i].mail;
	    				console.log($scope.repondantInvitationFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'repondantRemerciementEN':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRepRemerciementEN[idType]){
	    				$scope.repondantRemerciementEN[idType] = response.data[i].mail;
	    				console.log($scope.repondantRemerciementFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;
			  case 'relanceEN':
			    // code block
			    console.log('tafiditra');
			    for(var i =0;i<response.data.length;i++){
			    	console.log(response.data[i].id )
			    	console.log('idtype',idType);
	    			if(response.data[i].id == $scope.templateRelanceEN[idType]){
	    				$scope.relanceEN[idType] = response.data[i].mail;
	    				console.log($scope.relanceFR)
	    				break;
	    			}
	    		}
	    		console.log('tafavoaka');
			    break;

			  default:
			    // code block
		}
    }
});
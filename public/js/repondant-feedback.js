var app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

app.controller('myCtrl', function($scope,$http) {
  let localhostUrl=window.location.protocol + '//' + window.location.host //'http://localhost:8000'; 
  //console.log('locahostUrl',localhostUrl)
  let listOfUser= [];
  $scope.listOfUser =[];
  $scope.beneficiaires=[];
  $scope.checkrelance=[];
  $scope.enableCheckrelanceValue = false;
  $scope.enableCheckrelance= function(){
    let retour = false;
    for(var i=0;i<listOfUser.length;i++){
      if($scope.checkrelance[listOfUser[i].repondant_id]== true){
        retour = true;
        break;
      }
    }
    $scope.enableCheckrelanceValue = retour;
  }
  $scope.configMail = function (idRepondant,idTest,motif){
  	$scope.mailRepondantId=idRepondant;
  	$scope.mailTestId=idTest;
  	$scope.mailMotif=motif;
  	console.log(idRepondant)
  	console.log(idTest)
  	console.log(motif)
  }
  $scope.isThanksSend= function isThanksSend(id){
  	let retour = false;
  	for(var i=0;i<listOfUser.length;i++){
  		if (id == listOfUser[i].repondant_id && listOfUser[i].reponse == 1){
  			retour= true;
  			console.log(id);
  			break;
  		}
  	}
  	return retour;
  }
  $scope.isMailSend= function isMailSend(id){
  	let retour = false;
  	for(var i=0;i<listOfUser.length;i++){
  		if (id == listOfUser[i].repondant_id && listOfUser[i].mail_send == 1){
  			retour= true;
  			console.log(id);
  			break;
  		}
  	}
  	return retour;
  }
  $scope.isUserIdHere=function isUserhere(id){
  	let retour = false;
  	for(var i=0;i<listOfUser.length;i++){
  		if (id == listOfUser[i].repondant_id){
  			retour= true;
  			console.log(id);
  			break;
  		}
  	}
  	return retour;
  };
  $http({
  	method:'GET',
  	url:localhostUrl+'/feedback/getTestRepondant'
  }).then(function successCallback(response){
  	console.log("idTest", $scope.idTest);
  	console.log(response.data);
  	for(var i=0 ;i < response.data.length;i++){
  		if(response.data[i].test_id == $scope.idTest){
  			listOfUser.push(response.data[i]);
  		}
  	}
  	console.log("listeOfuser", listOfUser);
  	$scope.listOfUser = listOfUser;
  }, function errorCallback(response){

  })

  function setBeneficiaire(data){
		console.log("dataObjects",data);
		console.log("dataObjects stringify",JSON.stringify(data));
		var numlignebeneficiaire=0;
		var numlignecoach=0;
		var beneficiaire = [];
		//test du debut de l'information
		for( var i=0;i<data.length;i++){
			if(data[i].B =="1. Informations du bénéficiaire de la démarche"|| data[i].__rowNum__==13 ){ 
				numlignebeneficiaire = i;
				break;
			}
		}
		//test du limite de l'information
		for (i = numlignebeneficiaire;i<data.length;i++){
			if(data[i].A =="civilitecoach"|| data[i].__rowNum__==22 ){ 
				numlignecoach = i;
				break;
			}
		}
		//complete l objet beneficiaire
		for(i=numlignebeneficiaire+1;i<numlignecoach;i++){
			beneficiaire.push(data[i]);
		}
		return beneficiaire;
		
  }
  function setRepondant(data){
  		console.log("dataRange",data);
  		var numlignerepondant=0;
		var numlignelimiterepondant=0;
		var repondant=[];
		for(var i = 0 ; i<data.length;i++){
			if(data[i].B =="nom du groupe de répondants (1)"|| data[i].__rowNum__==34 ){ 
				numlignerepondant = i;
				break;
			}
		}
		for(i=numlignerepondant+1;i<data.length;i++){
			if(data[i].B =="Légende"){ 
				numlignelimiterepondant = i;
				break;
			}
		}
		for(i= numlignerepondant+1;i<numlignelimiterepondant;i++){
			if(data[i].G){
				repondant.push(data[i]);
			}
		}
		console.log("repondant",repondant);
		return repondant;
  }
  $scope.deleteData = function function_name(event,id) {
  	// body...
  	$scope.idDelete = id;
  	console.log(id);
  }
  $scope.setImage = function(element) {
  	   $scope.image_source="";
	   $scope.currentFile = element.files[0];
	   var reader = new FileReader();

	  reader.onload = function(event) {
	    $scope.image_source = event.target.result
	    console.log($scope.image_source);
	    $scope.$apply();
	  }
	  // when the file is read it triggers the onload event above.
	  reader.readAsDataURL(element.files[0]);
  }
  $scope.editRequest = function launch(event,id){
  	
  	$scope.id = id;
  	console.log(id);
  	$http({
	  method: 'GET',
	  url: localhostUrl+'/consultant/getData/'+id
		}).then(function successCallback(response) {
		    // this callback will be called asynchronously
		    // when the response is available
		    console.log(response);
        $scope.editfirstname =response.data.firstname; 
		    $scope.editlastname =response.data.lastname; 
		    $scope.editemail =response.data.email; 
		    $scope.editphone =response.data.phone; 
		    $scope.editorganisation =response.data.organisation; 
		    $scope.editcivility =response.data.civility; 
		    $scope.editlanguage =response.data.language; 
		    $scope.edituserid =response.data.edituserid;  
		    $scope.image_source=response.data.avatar_url;
		    $scope.$apply();
		    console.log("$scope.image_source",$scope.image_source);
		  }, function errorCallback(response) {
		  	$scope.image_source="";
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log(response);
	});
  }
  $scope.setFile = function(element) {
  	
  	   console.log(element.files[0]);
	   var file = element.files[0];
       var reader = new FileReader();  
  
       reader.onload = function (e) {  
  
            var data = e.target.result;  
  
            var workbook = XLSX.read(data, { type: 'binary' });  
       		console.log("workbook",workbook);
            var first_sheet_name = workbook.SheetNames[0];  
  			console.log("first_sheet_name",first_sheet_name);
            var dataObjects = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name],{header:"A"});
            $scope.beneficiaires = setBeneficiaire(dataObjects);
            $scope.repondants= setRepondant(dataObjects);
            $scope.$apply();
            
        }  
        reader.onerror = function (ex) {  
  
        }  
        reader.readAsBinaryString(file);   
        
  }
});
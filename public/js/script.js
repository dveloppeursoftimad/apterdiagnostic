// Code goes here

angular.module( 'app', [
  'tc.chartjs',
  'app.radar',
] );


angular.module( 'app.radar', [] )
  .controller( 'RadarCtrl', function ( $scope ) {
    $scope.data = {
      labels : [ 'DNS', 'OpenDNSSEC', 'Puppet', 'Git', 'MySQL', 'Bash', 'Ubuntu', 'Backup' ],
      datasets : [
        {
          fillColor : 'rgba(220,220,220,0.5)',
          strokeColor : 'rgba(220,220,220,1)',
          pointColor : 'rgba(220,220,220,1)',
          pointStrokeColor : '#fff',
          data : [ 5, 4, 3, 3, 4, 3, 4, 3]
        },
        {
          fillColor : 'rgba(151,187,205,0.5)',
          strokeColor : 'rgba(151,187,205,1)',
          pointColor : 'rgba(151,187,205,1)',
          pointStrokeColor : '#fff',
          data : [ 3, 2, 4, 2, 4, 3, 3, 2 ]
        }
      ]
    };
    $scope.options =  {
      segmentShowStroke : true,
      segmentStrokeColor : '#fff',
      segmentStrokeWidth : 24,
      percentageInnerCutout : 50,
      animation : true,
      animationSteps : 25,
      animationEasing : 'easeOutQuart',
      animateRotate : true,
      animateScale : false,
      onAnimationComplete : null,
      scaleOverride: true,
      scaleSteps: 5,
      scaleStepWidth: 1,
      scaleStartValue: 0,
    };
  });

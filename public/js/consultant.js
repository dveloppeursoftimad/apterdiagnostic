var app = angular.module('myApp', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });
var localhostUrl=window.location.protocol + '//' + window.location.host  //'http://localhost:8000';
app.controller('myCtrl', function($scope, $http) {

  $scope.consultantAccess = null;
  $scope.setFile = function(element) {
  	   $scope.image_source="";
	   $scope.currentFile = element.files[0];
	   var reader = new FileReader();

	  reader.onload = function(event) {
	    $scope.image_source = event.target.result
	    //$scope.$apply();
	  }
	  // when the file is read it triggers the onload event above.
	  reader.readAsDataURL(element.files[0]);
  }


  $scope.deleteData = function function_name(event,id) {
  	// body...
  	$scope.idDelete = id;
  	console.log(id);
  }
  $scope.setImageUrl = function setImageUrl() {
  	// body...
  	$scope.image_source="";
  	//$scope.$apply();
  }

  $scope.keyRequest= function launch(event,id,types){
  	console.log("types:",types);
  	$scope.idkey = id;
  	$scope.description = [];
  	console.log(id);
  	
  	$http({
	  method: 'GET',
	  url: localhostUrl+'/consultant/getData/'+id
		}).then(function successCallback(response) {
		    // this callback will be called asynchronously
		    // when the response is available
		    consultantData = response;
		    console.log(response);
		    $scope.editlastname =response.data.lastname; 
		    $scope.editfirstname =response.data.firstname; 
		    $scope.editusername =response.data.username; 
		    $scope.editemail =response.data.email; 
		    $scope.editphone =response.data.phone; 
		    $scope.editorganisation =response.data.organisation; 
		    $scope.editcivility =response.data.civility; 
		    $scope.editlanguage =response.data.language; 
			$scope.editrole = response.data.role; 
		    $scope.image_source=response.data.avatar_url;
		    
		    console.log("$scope.image_source",$scope.image_source);
		  }, function errorCallback(response) {
		  	$scope.image_source="";
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log('error get consultant getData');
		});
	$http({

		method: 'GET',
		url:localhostUrl+'/consultant/getConsultantAccess/'+id
	}).then(function successCallback(reponse){
		$scope.consultantAccess = reponse.data;
		for(var i=0;i<$scope.consultantAccess.length;i++){
			for(var j=0;j<types.length;j++){
				if(types[j].id == $scope.consultantAccess[i].type_id){
					$scope.description[types[j].id]= true;
					break;
				}
			}
		}
		if($scope.editrole=='admin') {
				$scope.admin=true;
		}
		else if($scope.editrole=='consultant'){
				$scope.admin=false;
		}
		console.log("consultant access:",$scope.consultantAccess);
		//edit admin
		
	},function errorCallback(reponse){
		console.log(reponse);

	})
  }
  $scope.set= function set(editlastname,editfirstname,editphone,editemail,editorganisation,editcivility,editlanguage,editrole,image_source){
  	$scope.editlastname =editlastname; 
	$scope.editfirstname =editfirstname; 
	//$scope.editusername =response.data.username; 
	$scope.editemail =editemail
	$scope.editphone =editphone
	$scope.editorganisation =editorganisation; 
	$scope.editcivility =editcivility
	$scope.editlanguage =editlanguage
	$scope.edituserid =edituserid
	$scope.image_source=image_source;
	console.log('im setting this thank ')
  }
  $scope.editRequest= function launch(event,id){
  	
  	$scope.id = id;
  	console.log(id);
  	$http({
	  method: 'GET',
	  url: localhostUrl+'/consultant/getData/'+id
		}).then(function successCallback(response) {
		    // this callback will be called asynchronously
		    // when the response is available
		    console.log(response);
		    $scope.editlastname =response.data.lastname; 
		    $scope.editfirstname =response.data.firstname; 
		    $scope.editusername =response.data.username; 
		    $scope.editemail =response.data.email; 
		    $scope.editphone =response.data.phone; 
		    $scope.editorganisation =response.data.organisation; 
		    $scope.editcivility =response.data.civility; 
		    $scope.editlanguage =response.data.language; 
		    $scope.edituserid =response.data.edituserid;  
		    $scope.image_source=response.data.avatar_url;
		    //$scope.$apply();
		    console.log("$scope.image_source",$scope.image_source);
		  }, function errorCallback(response) {
		  	$scope.image_source="";
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		    console.log(response);
	});
  }
});

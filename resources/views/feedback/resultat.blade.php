{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    360 Diagnostic       
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> 360 Diagnostic</a></li>
    <li class="fbve">@lang('message.liste')</li>
  </ol>
@stop
@section('content')
	<div class="content" style="overflow-x:scroll;">
		<table class="table table-striped" id="myTable">
			<thead>
				<tr>
					<th>@lang("message.repondants")</th>
					<th>@lang("message.groupe")</th>
					<?php $count = 0; ?>
					@foreach ($fb->pages()->where('page_id','!=',NULL)->get() as $pa => $page)
						@foreach($page->questions as $s => $question)
						<?php $count = $count + 1;
							$p = $pa +1;
                			$q = $s +1;
						?>
							<th>{{  "P.".$p." ".$page->title_fr." | ".$question->question_fr }}</th>
						@endforeach
					@endforeach
				</tr>
			</thead>
			<tbody>
				<?php $countdata = 0; ?>
				@foreach($data_users as $du => $user)

					<tr>
						<td>
							{{ $user["nom"] }}
						</td>
						<td>
							{{ $user["groupe"] }}
						</td>
						@foreach ($fb->pages()->where('page_id','!=',NULL)->get() as $pa => $page)
							@foreach($page->questions as $s => $question)
							<?php $count = $count + 1; ?>
								<!-- <td>{{ array_key_exists($question->id, $user['score']) ? ($user['score'][$question->id] > 2 ? 1 : '' )  : ""  }}</td> -->
								<td>{{ array_key_exists($question->id, $user['score']) ? $user['score'][$question->id]  : ""  }}</td>
							@endforeach
						@endforeach

						<!-- @if($user["score"])
							@if(count($user["score"]) == 84)
								@foreach($user["score"] as $sc => $score)
									<td>{{ $score }}</td>
									<?php $countdata = $countdata + 1; ?>
								@endforeach
							@else
								<?php $td = 84 - count($user["score"]); ?>
								@foreach($user["score"] as $sc => $score)
									<td>{{ $score }}</td>
									<?php $countdata = $countdata + 1; ?>
								@endforeach
								@for($i=0;$i<$td;$i++)
									<td>&nbsp;</td>
								@endfor
							@endif
						@else
							@for($i=0;$i<84;$i++)
								<td>&nbsp;</td>
							@endfor
						@endif -->
					</tr>
				@endforeach
					<!-- <tr><td>{{ $count }}</td><td>{{ $countdata }}</td></tr> -->
			</tbody>
		</table>
	</div>
@stop

@section('js')
<script> console.log('Hi!'); </script>
  	<script type="text/javascript">
	  	$(document).ready(function(){
	    	$('#myTable').DataTable({
		        dom: 'Bfrtip',
		        buttons: [ {
	            extend: 'excelHtml5',
		            customize: function ( xlsx ){
		                var sheet = xlsx.xl.worksheets['sheet1.xml'];
		                // jQuery selector to add a border
		                $('row c[r*="10"]', sheet).attr( 's', '25' );
		            }
	        	}]
		    });
		});
  </script>
@stop
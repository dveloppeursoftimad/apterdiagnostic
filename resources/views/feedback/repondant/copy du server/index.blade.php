{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')

  <link rel="stylesheet" href="/css/styleFeedback.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('content_header')
      <h1>
        <span class="glyphicon glyphicon-stats"></span>
        {{$test->name}}
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> 360 FB</a></li>
        <li class="active">{{$test->name}}</li>
      </ol>
@stop
@section('content')
    <div ng-app = "app" ng-controller="myCtrl">


        <input type="hidden" ng-model="idTest ={{$idtest}}">
        @if (session('error_email'))
                            <div class="alert alert-danger">
                              <div class="alert-icon">
                                <i class="material-icons"></i>
                              </div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                              </button>
                              {{ session('error_email') }}
                            </div>
        @endif
        @if(count($repondants) >0)
        <div class="box box-primary">

            <div class="box-header">
              <h3 class="box-title">Bénéficiaire</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Nom</th>
                  <th>Societe</th>
                  <th>Email</th>
                  <th>Téléphone</th>
                </tr>
                @foreach($users as  $user)
                    @if($user->type == 'beneficiaire')
                      <tr>
                        <td scope="col">
                         <span>{{$user->civility}} {{$user->username}}</span>
                        </td>
                        <td scope="col">
                          {{$user->organisation}}
                        </td>
                        <td scope="col">
                          {{$user->email}}
                        </td>
                        <td scope="col">
                          {{$user->phone}}
                        </td>
                      </tr>
                    @endif
                 @endforeach
              </table>
            </div>
            <!-- /.box-body -->
           {{--  <div class="box-footer ">
                
            </div> --}}
        </div>
         
        @endif

        
        <div ng-if="!(repondants.length >0)" class="row">
          <div class="col-sm-12 col-md-4">
                @if(count($repondants) > 0)
                <form style="padding-top: 1em;" role="form" method="POST" action="<% '/feedback/sendMailGlobaly' %>"  >
                      @csrf
                      @method('POST')
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="idTest" value="{{$idtest}}">
                      <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                      <input type="hidden" name="motif" value="<% mailMotif%>">

                      <button type="submit" class="btn btn-primary ">Envoyer mail aux repondants </button>
                      

                </form>
                @endif
           </div>


              <div class="col-sm-12 col-md-4">
                <form style="padding-top: 1em;" id="relanceMail" role="form" method="POST" action="<% '/feedback/checkGlobaly' %>"  >
                        @csrf
                        @method('POST')
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="idTest" value="{{$idtest}}">
                        <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                        <input type="hidden" name="motif" value="<% mailMotif%>">
                        <button type="submit" ng-if="enableCheckrelanceValue"  class="btn btn-primary ">Relancer mail</button>
                </form>
              </div>
        </div>

        @if(count($repondants) >0)
        <div ng-if="!(repondants.length >0)" class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Répondants</h3>

              {{-- <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <th>Nom du Groupe</th>
                  <th>Nom - Email</th>
                  <th>Mot de passe</th>
                  <th>Envoi Mail</th>
                  <th>Réponse</th>
                  <th></th>
                </tr>
                @foreach($repondants as  $user)
                   @if($user->type =="repondant" || $user->type =="beneficiaire")
                         <tr>
                        <td scope="col">
                          <input type="hidden" name="checkrelance[{{$user->id}}]" form="relanceMail" value="<%checkrelance[{{$user->id}}]%>" >
                          <!-- <input type="checkbox" ng-model="checkrelance[{{$user->id}}]" [ng-true-value="on"][ng-false-value="false"] ng-change="enableCheckrelance()"> -->
                        </td>
                        <td scope="col">
                          @foreach($groupe as $gp)
                            @if($gp->id == $user->groupe_id)
                              {{$gp->name}}
                            @endif
                          @endforeach
                        </td>
                        <td scope="col">
                          <span>{{$user->civility}} {{$user->firstname}} {{$user->lastname}}</span>
                          <br>
                          <span> {{$user->email}}</span>
                        </td>
                        <td scope="col">
                          {{$user->code}}
                        </td>
                        <td scope="col">
                          <span class="cursor"  ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation')" data-toggle="modal" data-target="#mail"><i class="fa fa-send  "></i> Envoyer</span>
                          <span class="cursor"  ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance')" data-toggle="modal" data-target="#exampleModalCenter" ><i class="fa fa-send  "></i> Relancer</span>
                          <span class="cursor" ng-if="isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ><i class="fa fa-send  " ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement')" data-toggle="modal" data-target="#mail"></i> Remercier</span>
                        </td>
                        <td scope="col">
                          <span ng-if="isThanksSend($id={{$user->id}})">
                            Oui
                          </span>
                          <span ng-if="!isThanksSend($id={{$user->id}})">
                            Non
                          </span>
                        </td>
                        <td scope="col">
                          <!-- Button trigger modal -->
                                <span ng-click="editRequest($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#edition"><i class="fa fa-edit fa-2x"></i></span>
                                <button ng-if="!isThanksSend($id={{$user->id}})" ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" class="btn btn-box-tool"><i class="fa fa-trash fa-2x"></i></button>
                        </td>
                      </tr>                 
                   @endif
                 @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- Button trigger modal -->
                <div  class="col-sm-3">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#repondant">
                  Ajouter Répondant
                </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="repondant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                            <form role="form" method="POST" action="<% '/feedback/addRepondantTo' %>"  >
                           @csrf
                           @method('POST')
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                                <label class="col-sm-3 col-form-label">Nom *:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="lastname" name="lastname" class="form-control" value="{{ old('username') }}"
                                           placeholder="Nom"  >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                                <label class="col-sm-3 col-form-label">Prénom*:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="firstname" name="firstname" class="form-control" value="{{ old('username') }}"
                                           placeholder="Prénom"  >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Email*:</label>
                                  <div class="col-sm-9">
                                    <input type="email" ng-model = "email"  name="email" class="form-control" value="{{ old('email') }}"
                                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Mot de passe*:</label>
                                   <div class="col-sm-9">
                                     <input type="text"  name="password" class="form-control"
                                           placeholder="Mot de passe">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              {{-- Groupe --}}
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Groupe:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model = "groupe"   name="groupe" class="form-control"
                                           placeholder="groupe">
                                    <span class="glyphicon glyphicon-flag form-control-feedback"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('groupes') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              {{-- Abreviation groupe --}}
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Abbreviation:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="abreviation"  name="abreviation" class="form-control"
                                           placeholder="Abbreviation groupe">
                                    <span class=" form-control-feedback"><i class="fa fa-bookmark"></i></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('abreviation') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              <!-- PHONE -->
                              <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Téléphone:</label>
                                  <div class="col-sm-9">
                                    <input type="text"  name="phone" class="form-control" value="{{ old('phone') }}"
                                           placeholder="Téléphone">
                                    <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <!-- ORGANISATION -->
                              {{-- <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Organisation:</label>
                                  <div class="col-sm-9">
                                    <input type="text" ng-model="editorganisation" name="organisation" class="form-control" value="{{ old('organisation') }}"
                                           placeholder="ORGANISATION">
                                    <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                                    @if ($errors->has('organisation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('organisation') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div> --}}
                              <!-- CIVILITY -->
                              <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Civilité:</label>
                                  <div class="col-sm-9 row">
                                    <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                                           placeholder="CIVILITY"> -->
                                           <div class="col-sm-6">
                                             <input type="radio"  class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                                 >
                                            <span for="huey">Monsieur</span>
                                           </div>
                                           <div class="col-sm-6">
                                             <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Madame"
                                                   >
                                             <span for="huey">Madame</span>
                                           </div>
                                    @if ($errors->has('civility'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('civility') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <!-- LANGUAGE -->
                              <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Language:</label>
                                  
                                    <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                                           placeholder="LANGUAGE">
                                    <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                                    <div class="col-sm-9 ">
                                      <select class="custom-select custom-select-lg mb-3 form-control"  name="language">
                                          <option  selected value="fr">Français</option>
                                          <option  value="en">Anglais</option>
                                       </select>
                                       @if ($errors->has('language'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('language') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                 
                              </div>
                              <!-- AVATAR FILE CHOOSER -->
                              <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Avatar:</label>
                                    <div class="col-sm-9 ">
                                      <div class="custom-file">
                                        <input type="file" name="editavatar" class="custom-file-input" onchange="angular.element(this).scope().setImage(this)" id="editavatar">
                                        <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                                      </div>
                                       @if ($errors->has('avatar'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('avatar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                 
                              </div>
                              <input type="hidden" value="<%image_source%>" name="avatar_url">
                              <input type="hidden" name="idTest" value="{{$idtest}}">
                              <button ng-disabled="!firstname||!lastname ||!email || !groupe || !abreviation" type="submit"
                                      class="btn btn-primary btn-block btn-flat"
                              >Enregistrer</button>
                          </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        @endif
        <div style="visibility: hidden;display: none;">
         @if($users)
              <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">
                      Nom du Beneficiaire
                    </th>
                    <th scope="col">
                      Email
                    </th>
                    <th scope="col">
                      Phone
                    </th>
                    <th scope="col">
                      Societe
                    </th>
                    <th scope="col">
                      Civilite
                    </th>
                    <th scope="col">
                      @foreach($users as $user)
                       @if($user->role =='beneficiaire')
                        <div ng-if="isUserIdHere($id={{$user->id}})">
                           <span ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation')" data-toggle="modal" data-target="#mail"><i class="fa fa-envelope  "></i></span>
                          <span ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance')" data-toggle="modal" data-target="#exampleModalCenter" ><i class="fa fa-envelope  "></i></span>
                          <span ng-if="isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ><i class="fa fa-envelope  " ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement')" data-toggle="modal" data-target="#mail"></i></span>
                        </div>
                       @endif
                      @endforeach
                    </th>
                  </tr>
                </thead>
              

                <tbody>
                 @foreach($users as  $user)
                    @if($user->role == 'beneficiaire')
                      <tr ng-if="isUserIdHere($id={{$user->id}})">
                        <td scope="col">
                          {{$user->username}}
                        </td>
                        <td scope="col">
                          {{$user->email}}
                        </td>
                        <td scope="col">
                          {{$user->phone}}
                        </td>
                        <td scope="col">
                          {{$user->organisation}}
                        </td>
                        <td scope="col">
                          {{$user->civility}}
                        </td>
                      </tr>
                    @endif
                 @endforeach
                </tbody>
              </table>
         @endif
         @if($users)
         <div class="row">
          @foreach($users as  $user)
                
                 @if($user->role == 'repondant')
                  <div ng-if="isUserIdHere($id={{$user->id}})" class="col-sm-6 col-md-4 col-lg-3">
                     <div class="box" data-widget="box-widget">
                        <div class="box-header">
                          <div class="box-tools">
                            <!-- This will cause the box to be removed when clicked -->      
                              <button ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" class="btn btn-box-tool"><i class="fa fa-trash fa-2x"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                            <div class="box-name">
                                {{$user->username}}
                            </div>
                            <div class="titre-apter">
                               Apter-France
                            </div>
                            <div class="coordonne">
                               <span>{{$user->email}}</span>
                            </div>
                            <div class="coordonne">
                              <span>{{$user->phone}}</span>
                            </div>
                            <div class="coordonne"></div>
                            <div class="center">
                              <div>
                                <span ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation')" data-toggle="modal" data-target="#mail"><i class="fa fa-envelope  "></i></span>
                                <span ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance')" data-toggle="modal" data-target="#exampleModalCenter" ><i class="fa fa-envelope  "></i></span>
                                <span ng-if="isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ><i class="fa fa-envelope  " ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement')" data-toggle="modal" data-target="#mail"></i></span>
                                <!-- Button trigger modal -->
                                <span ng-click="editRequest($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#edition"><i class="fa fa-edit fa-2x"></i></span>
                              </div>
                            </div>
                        </div>
                        
                      </div>
                   </div>
               @endif
          @endforeach
         </div> 
        @endif
        </div>
        <div>
          <div ng-if="!(repondants.length >0)" class="box box-default" data-widget="box-widget">
            <div class="box-header">
              <form>
                <div class="form-group">
                  <label for="exampleFormControlFile1">Importation Excel :</label>
                  <input type="file" class="form-control-file" onchange="angular.element(this).scope().setFile(this)" id="exampleFormControlFile1">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div ng-if="listOfUser.length >0 && false">
          <div class="box box-default" data-widget="box-widget">
            <div class="box-header">
              
                <div class="form-group">
                  <label for="exampleFormControlFile1">Cliquer ici pour envoyer tous les mails:</label>
                  <form role="form" method="POST" action="<% '/feedback/sendMailGlobaly' %>"  >
                      @csrf
                      @method('POST')
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="idTest" value="{{$idtest}}">
                      <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                      <input type="hidden" name="motif" value="<% mailMotif%>">
                      <button type="submit" class="btn btn-primary ">Envoyer Mail</button>
                  </form>
                </div>
             
            </div>
          </div>
        </div>
        
        <div class="beneficiaire" ng-if="repondants.length>0">
          <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Bénéficiaire trouvé</h3>
            
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col" ng-repeat="beneficiaire in beneficiaires">
                  <% beneficiaire.B %>
                  <input type="hidden" name="labelbeneficiaire[<% $index %>]" form="valider" value="<% beneficiaire.B %>">    
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>           
                <td scope="col" ng-repeat="beneficiaire in beneficiaires">
                  <%beneficiaire.C%>
                  <input type="hidden" name="beneficiaire[<% $index %>]" form="valider" value="<% beneficiaire.C %>">       
                </td>
              </tr>
            </tbody>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        </div>

        <div ng-if="repondants.length>0" class="repondant" style="overflow: auto;">
          <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Répondants trouvés</h3>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table">
            <thead ng-if="repondants.length>0" class="thead-light">
              <tr>
                <th scope="col" >Nom du groupes</th>
                <th scope="col" >Abreviations</th>
                <th scope="col" >Civilité</th>
                <th scope="col" >Nom et prenoms</th>
                <th scope="col" >Email</th>
                <th scope="col" >Langue</th>
              </tr>
            </thead>
            <tbody >
              <tr ng-repeat="repondant in repondants">           
                <td scope="col">
                  <%repondant.B%>
                  <input type="hidden" name="groupeRepondant[<% $index %>]" value="<%repondant.B%>" form="valider">  
                </td>
                <td scope="col">
                  <%repondant.C%>
                  <input type="hidden" name="abreviationRepondant[<% $index %>]" value="<%repondant.C%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.D%>
                  <input type="hidden" name="civiliteRepondant[<% $index %>]" value="<%repondant.D%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.E%> <%repondant.F%>
                  <input type="hidden" name="nomRepondant[<% $index %>]" value="<%repondant.F%>" form="valider">
                  <input type="hidden" name="prenomRepondant[<% $index %>]" value="<%repondant.E%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.G%>
                  <input type="hidden" name="emailRepondant[<% $index %>]" value="<%repondant.G%>" form="valider">
                </td>
                <td scope="col"><% repondant.H ? repondant.H :'français' %>
                  <input type="hidden" name="langageRepondant[<% $index %>]" value="<% repondant.H ? repondant.H :'français' %>" form="valider">
                </td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
          
        </div>
        <form ng-if="repondants.length >0" id="valider" action="/feedback/createrepondant" method="POST">
            @csrf
            @method('POST')
            <input type="hidden" value="{{$idtest}}" name="idTest">
            <button type="submit"  class="btn btn-primary">
               Valider
            </button>
          </form>
        {{-- Mail Relance --}}
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              
              <div class="modal-body">
                Relance Mail
              </div>
              <div class="modal-footer">
                <form role="form" method="POST" action="<% '/feedback/sendMailRelance' %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                  <input type="hidden" name="motif" value="<% mailMotif%>">
                  <button type="submit" class="btn btn-primary ">Envoyer Mail</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        {{-- Modal Mail specifique --}}
        <div class="modal fade" id="mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
                Confirmation          
            </div>
            <div class="modal-footer">
              <form role="form" method="POST" action="<% '/feedback/sendMailSpec' %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                  <input type="hidden" name="motif" value="<% mailMotif%>">
                  <button type="submit" class="btn btn-primary ">Envoyer Mail</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      {{-- Edition Modal --}}
      <div class="modal fade" id="edition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" method="POST" action="<% '/feedback/editionRepondant/'+id %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Nom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control" value="{{ old('username') }}"
                               placeholder="Nom "  ng-model="editlastname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Prénom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" value="{{ old('username') }}"
                               placeholder="Prénom"  ng-model="editfirstname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Email*:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="editemail" name="email" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Mot de passe*:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="editpassword" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Téléphone:</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="editphone" name="phone" class="form-control" value="{{ old('phone') }}"
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- ORGANISATION -->
                 {{--  <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Organisation:</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="editorganisation" name="organisation" class="form-control" value="{{ old('organisation') }}"
                               placeholder="ORGANISATION">
                        <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('organisation') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div> --}}
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Civilité:</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" ng-model="editcivility" class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                     >
                                <span for="huey">Monsieur</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Madame"
                                       >
                                 <span for="huey">Madame</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Language:</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" ng-model="editlanguage" name="language">
                              <option selected value="Français">Français</option>
                              <option value="Anglais">Anglais</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
                  <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file">
                            <input type="file" name="editavatar" class="custom-file-input" onchange="angular.element(this).scope().setImage(this)" id="editavatar">
                            <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <input type="hidden" value="<%image_source%>" name="avatar_url">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <button ng-disabled="!editlastname|| !editlastname ||!editemail" type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >Enregistrer</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
        <!-- Modal suppression -->

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body" style="text-align: center;">
               Voulez-vous supprimer le consultant du test?
              </div>
              <div class="modal-footer">
                <form role="form" method="POST" action="<% '/feedback/deleteFromTest/' %>"  >
                           @csrf
                           @method('POST')
                          <input type="hidden" name="idDelete" value="<% idDelete %>">
                          <input type="hidden" name="idTest" value="{{$idtest}}">
                          <button type="submit" class="btn btn-danger">Suppression</button>
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type="text/javascript" src="/js/xlsx.full.min.js"></script>
    <script type="text/javascript">
      var app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });

      app.controller('myCtrl', function($scope,$http) {
        let localhostUrl=window.location.protocol + '//' + window.location.host //'http://localhost:8000'; 
        //console.log('locahostUrl',localhostUrl)
        let listOfUser= [];
        $scope.listOfUser =[];
        $scope.beneficiaires=[];
        $scope.checkrelance=[];
        $scope.enableCheckrelanceValue = false;
        $scope.enableCheckrelance= function(){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if($scope.checkrelance[listOfUser[i].repondant_id]== true){
              retour = true;
              break;
            }
          }
          $scope.enableCheckrelanceValue = retour;
        }
        $scope.configMail = function (idRepondant,idTest,motif){
          $scope.mailRepondantId=idRepondant;
          $scope.mailTestId=idTest;
          $scope.mailMotif=motif;
          console.log(idRepondant)
          console.log(idTest)
          console.log(motif)
        }
        $scope.isThanksSend= function isThanksSend(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id && listOfUser[i].reponse == 1){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        }
        $scope.isMailSend= function isMailSend(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id && listOfUser[i].mail_send == 1){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        }
        $scope.isUserIdHere=function isUserhere(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        };
        $http({
          method:'GET',
          url:localhostUrl+'/feedback/getTestRepondant'
        }).then(function successCallback(response){
          console.log("idTest", $scope.idTest);
          console.log(response.data);
          for(var i=0 ;i < response.data.length;i++){
            if(response.data[i].test_id == $scope.idTest){
              listOfUser.push(response.data[i]);
            }
          }
          console.log("listeOfuser", listOfUser);
          $scope.listOfUser = listOfUser;
        }, function errorCallback(response){

        })

        function setBeneficiaire(data){
          console.log("dataObjects",data);
          console.log("dataObjects stringify",JSON.stringify(data));
          var numlignebeneficiaire=0;
          var numlignecoach=0;
          var beneficiaire = [];
          //test du debut de l'information
          for( var i=0;i<data.length;i++){
            if(data[i].B =="1. Informations du bénéficiaire de la démarche"|| data[i].__rowNum__==13 ){ 
              numlignebeneficiaire = i;
              break;
            }
          }
          //test du limite de l'information
          for (i = numlignebeneficiaire;i<data.length;i++){
            if(data[i].A =="civilitecoach"|| data[i].__rowNum__==22 ){ 
              numlignecoach = i;
              break;
            }
          }
          //complete l objet beneficiaire
          for(i=numlignebeneficiaire+1;i<numlignecoach;i++){
            beneficiaire.push(data[i]);
          }
          return beneficiaire;
          
        }
        function setRepondant(data){
            console.log("dataRange",data);
            var numlignerepondant=0;
          var numlignelimiterepondant=0;
          var repondant=[];
          for(var i = 0 ; i<data.length;i++){
            if(data[i].B =="nom du groupe de répondants (1)"|| data[i].__rowNum__==34 ){ 
              numlignerepondant = i;
              break;
            }
          }
          for(i=numlignerepondant+1;i<data.length;i++){
            if(data[i].B =="Légende"){ 
              numlignelimiterepondant = i;
              break;
            }
          }
          for(i= numlignerepondant+1;i<numlignelimiterepondant;i++){
            if(data[i].G){
              repondant.push(data[i]);
            }
          }
          console.log("repondant",repondant);
          return repondant;
        }
        $scope.deleteData = function function_name(event,id) {
          // body...
          $scope.idDelete = id;
          console.log(id);
        }
        $scope.setImage = function(element) {
             $scope.image_source="";
           $scope.currentFile = element.files[0];
           var reader = new FileReader();

          reader.onload = function(event) {
            $scope.image_source = event.target.result
            console.log($scope.image_source);
            $scope.$apply();
          }
          // when the file is read it triggers the onload event above.
          reader.readAsDataURL(element.files[0]);
        }
        $scope.editRequest = function launch(event,id){
          
          $scope.id = id;
          console.log(id);
          $http({
          method: 'GET',
          url: localhostUrl+'/consultant/getData/'+id
          }).then(function successCallback(response) {
              // this callback will be called asynchronously
              // when the response is available
              console.log(response);
              $scope.editfirstname =response.data.firstname; 
              $scope.editlastname =response.data.lastname; 
              $scope.editemail =response.data.email; 
              $scope.editphone =response.data.phone; 
              $scope.editorganisation =response.data.organisation; 
              $scope.editcivility =response.data.civility; 
              $scope.editlanguage =response.data.language; 
              $scope.edituserid =response.data.edituserid;  
              $scope.image_source=response.data.avatar_url;
              $scope.$apply();
              console.log("$scope.image_source",$scope.image_source);
            }, function errorCallback(response) {
              $scope.image_source="";
              // called asynchronously if an error occurs
              // or server returns response with an error status.
              console.log(response);
        });
        }
        $scope.setFile = function(element) {
          
             console.log(element.files[0]);
           var file = element.files[0];
             var reader = new FileReader();  
        
             reader.onload = function (e) {  
        
                  var data = e.target.result;  
        
                  var workbook = XLSX.read(data, { type: 'binary' });  
                console.log("workbook",workbook);
                  var first_sheet_name = workbook.SheetNames[0];  
              console.log("first_sheet_name",first_sheet_name);
                  var dataObjects = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name],{header:"A"});
                  $scope.beneficiaires = setBeneficiaire(dataObjects);
                  $scope.repondants= setRepondant(dataObjects);
                  $scope.$apply();
                  
              }  
              reader.onerror = function (ex) {  
        
              }  
              reader.readAsBinaryString(file);   
              
        }
});
    </script>
@stop
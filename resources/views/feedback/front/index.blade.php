
  @extends('diagnostic')
  @section('content')
<div ng-app="feedbackFrontApp" ng-controller="feedbackFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container" style="position: relative;">

			<div>
					<div class="title_mobile">
						<h2 class="font"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a><i class="material-icons">chevron_right</i><a href="{{ route('feedback-front') }}"> <span class="text-warning">{{ __('message.feedback') }}</span></a></h2>
					</div>

					<div class="row">

						@forelse ($feedbacks as $feedback)
						    <div class="card">
								  <div class="card-body row">
								    <div class="col-sm-8">
								    	<h4><i class="large material-icons">chevron_right</i> {{ $feedback->name }}</h4>
								    </div>
								    <div class="col-sm-4">
								    	<button class="btn btn-primary btn-round" style="width: 100%" ng-click="selectfeedback(<?php echo htmlspecialchars(json_encode($feedback)) ?>)" data-toggle="modal" data-target="#participer">
							                <i class="large material-icons">arrow_forward</i> <b>{{ __('message.participer') }}</b>
							              <div class="ripple-container"></div>
							          	</button>
								    </div>
								  </div>
							</div>
						@empty
						    <div class="card">
								  <div class="card-body row">
								    <div class="col-sm-8">
								    	<h4>Pas de FeedBack</h4>
								    </div>
								    
								  </div>
							</div>
						@endforelse
							
						
						
					</div>
			</div>

			
					

	
		</div>
	</div>

	



	<!-- Modal -->
	<div class="modal fade" id="participer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">{{ __('message.code_feedback') }}</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
			    <label for="code" style="color: gray;">Code</label>
			    <input type="text" class="form-control" id="code" ng-model="code"  placeholder="{{ __('message.entrer_code') }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
			   
			 </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('message.annuler')}}</button>
	        <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">{{__('message.participer')}}</button>
	      </div>
	    </div>
	  </div>
	</div>

</div>
<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });
	});
</script>
 <script type="text/javascript">
      var feedbackFrontApp = angular.module('feedbackFrontApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });
      feedbackFrontApp.controller('feedbackFrontController', function PhoneListController($scope) {
            $scope.feedback = undefined;
            $scope.curfeedback = undefined;

            $scope.selectfeedback = function(feedback){
            	console.log("feedback %o", feedback);
            	$scope.curfeedback = feedback;
            }

            $scope.saveCode = function(){
            	if($scope.code ==  $scope.curfeedback.code){
            		$scope.feedback = $scope.curfeedback;
            		window.location = "/diagnostic/feedback/"+$scope.curfeedback.id+"/"+$scope.curfeedback.code+"/questions";
            	}else{
            		alert("Le code entré ne correspond pas");
            	}
            }
      });


    </script>
<!--/div-->
@endsection('content')

{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
      <h1>
        <span class="glyphicon glyphicon-stats"></span>
        @lang('message.feedback_leadership')       
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.feedback_leadership')</a></li>
        <li class="active">@lang('message.liste')</li>
      </ol>
@stop
@section('content')

<div ng-app="feedbackApp" ng-controller="feedbackController">
      <div>
        @if(session('error_code'))
          <div class="alert alert-danger">
            <div class="alert-icon">
              <i class="material-icons">error_outline</i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            <b>{{ session('error_code') }}</b>
          </div>
        @endif
      </div>
      <div class="row"  >
        @if(Auth::user()->role !== 'admin_acad')
          <div class="col-sm-12 col-md-6 text-left">
            <button class="btn btn-primary" ng-click="addFb()" data-toggle="modal" data-target="#add-feedback"><i class="fa fa-plus"></i> @lang('message.ajouter') </button>
          </div>
        
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <div class="input-group" >
                <input type="text" class="form-control timepicker" id="myInput" onkeyup="myFunction()">

                <div class="input-group-addon">
                  <i class="fa fa-search"></i>
                </div>
              </div>
              <!-- /.input group -->
            </div>
          </div>
        @endif
        
        @foreach ($tests as $f=>$fb)
          <div class="col-sm-12 col-md-6" id="h3_{{$f}}">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">{{ $fb->name }}</h3>
                
                  <p class="text-secondary">Date : {{ $fb->created_at }}</p>
                 
                  @if(Auth::user()->role !== 'admin_acad')
                    <div class="box-tools pull-right">
                      <!-- Buttons, labels, and many other things can be placed here! -->
                      <!-- Here is a label for example -->
                      <span class="label label-default" ng-click="editFb(<?php echo htmlspecialchars(json_encode($fb)) ?>)" data-toggle="modal" data-target="#edit-feedback"><i class="fa fa-pencil"></i></span>
                      <span class="label label-default" ng-click="deleteFb(<?php echo htmlspecialchars(json_encode($fb)) ?>)" data-toggle="modal" data-target="#delete-fb"><i class="fa fa-trash"></i></span>
                    </div>
                  @endif
                  <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body text-right row">
                  <span class="col-sm-2"></span>
                  <a href="/feedback/repondant/{{$fb->id}}"  class="text-warning col-sm-3"> <span class="glyphicon glyphicon-user"></span> @lang('message.repondants')</a>
                  <a href="/feedback/rapport/{{$fb->id}}" class="text-warning col-sm-3"> <span class="glyphicon glyphicon-stats"></span> @lang('message.resultat')</a>
                  <a href="/feedback/resultatFeedback/{{$fb->id}}" class="text-warning col-sm-3"> <span class="glyphicon glyphicon-sort-by-attributes-alt"></span> @lang("message.tabRep")</a>
                </div>
                <!-- /.box-body -->
              <!--   <div class="box-footer">
                  The footer of the box
                </div> -->
                <!-- box-footer -->
              </div>
              <!-- /.box -->
          </div>
        @endforeach
      </div>

      <div class="modal fade" id="delete-fb">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-trash"></i> Suppression</h4>
              </div>

              <form role="form" action="{% '/feedback/'+fb.id %}" method="POST">
                @csrf
                @method('DELETE')
                            <div class="modal-body">
            
                                <p>@lang('message.sup_climage') {% fb.name %}?</p>



                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary">@lang('message.supprimer')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


      <div class="modal fade" id="add-feedback">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-plus"></i> @lang('message.ajout_feedback')</h4>
              </div>

              <form role="form" action="{{ route('feedback.store') }}" method="POST">
                @csrf
                @method('POST')
                          <div class="modal-body">
            
                              <div class="form-group">
                                <label for="name">@lang('message.titre')</label>
                                <input type="text" class="form-control" ng-change="checkValue()" name="name" id="name" placeholder="" ng-model="fb.name">
                              </div>

                              <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" class="form-control" ng-change="checkValue()" name="code" id="code" placeholder="" ng-model="fb.code">
                              </div>

                              <div class="form-group">
                                <label for="code">Questionnaire</label>
                                <select class="custom-select custom-select-lg mb-3 form-control" name="template_id">
                                        @foreach($templates as  $template)
                                          <option selected value="{{ $template->id }}">{{ $template->description }}</option>
                                        @endforeach
                                </select>
                              </div>

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary" ng-disabled="checkValue()">@lang('message.ajouter')</button>
                            </div>
                          </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

      <div class="modal fade" id="edit-feedback">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-plus"></i> @lang('message.modifier') {% fb.name %} </h4>
              </div>

              <form role="form" action="{% '/feedback/'+fb.id %}" method="POST">
                @csrf
                @method('PUT')
                        <div class="modal-body">
            
                              <div class="form-group">
                                <label for="name">@lang('message.titre')</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="" ng-model="fb.name">
                              </div>

                              <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" class="form-control" name="code" id="code" placeholder="" ng-model="fb.code">
                              </div>

                              <!-- <div class="form-group">
                                <label for="code">Questionnaire</label>
                                <select class="custom-select custom-select-lg mb-3 form-control" name="template_id" ng-model="fb.template_id">
                                        @foreach($templates as  $template)
                                          <option selected value="{{ $template->id }}">{{ $template->description }}</option>
                                        @endforeach
                                </select>
                              </div> -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary" ng-disabled="!fb.name || !fb.code">@lang('message.modifier')</button>
                            </div>


                        </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

      
        {{ $tests->links() }}

        
        


</div>
@stop


@section('js')
    <script> console.log('Hi!');
      function myFunction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        h3 = document.getElementsByTagName("h3");
        for (i = 0; i < h3.length; i++) {
          a = h3[i];
          txtValue = a.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            document.getElementById("h3_"+i).style.display = "";
              //h3[i].style.display = "";
          } else {
            document.getElementById("h3_"+i).style.display = "none";
              //h3[i].style.display = "none";
          }
        }
      }
    </script>
    <script type="text/javascript">
      var feedbackApp = angular.module('feedbackApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      feedbackApp.controller('feedbackController', function($scope,$http) {
            
            $scope.fb = {};

            $scope.test = {};

             $scope.editFb = function(fb){
              console.log(fb);
               $scope.fb = fb;
              
             }

             $scope.deleteFb = function(fb){

               $scope.fb = fb;
             }

             $scope.addFb = function(){
                 $scope.fb ={}
             }

            $http({
              method: 'GET',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              dataType: 'json',
              url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/feedback/codes"
            }).then(function successCallback(response) {
              $scope.test = response.data;
            }, function errorCallback(response) {

              //console.log(response);
            });

            $scope.checkValue = function(){

              let retour = false;

              for(var i= 0; i< $scope.test.length; i++){
                if($scope.test[i].code == $scope.fb.code || $scope.fb.name == $scope.test[i].name){
                  retour = true;
                  break;
                }
              }
              console.log(retour);
              return retour;

            }

             
      });


    </script>
@stop
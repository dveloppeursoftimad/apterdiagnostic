{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/styleFeedback.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
  <link rel="stylesheet" href="/chart/chartphp.css">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <style type="text/css">
    button{
      margin-right: 10px;
    }
  </style>
@stop
@section('content_header')
     <h1>
        <span class="glyphicon glyphicon-stats"></span>
        {{ $test->name }}

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> 360 FB</a></li>
        <li class="active">Rapport</li>
      </ol>
@stop
@section('content')

<div class="row" ng-app="app" ng-controller="rapportCtrl">
    <div class="col-xs-12" >
      <div ng-cloak>
        <button class="btn btn-primary pull-right" ng-if="!loading && !rapportPptx" ng-click="generatePptx()" >
          Générer le rapport pptx
        </button>
        <button class="btn btn-primary pull-right" ng-if="!loading && !rapportPdf" ng-click="generatePdf()" >
          Générer le rapport pdf
        </button>
        <!-- <a href="/feedback/downloadPdf/{{$id}}" class="btn btn-primary pull-right"> -->

        <button class="btn btn-primary pull-right" ng-if="!loading && !rapport" ng-click="generate()" >
          Générer le rapport docx
        </button>

        <button class="btn btn-primary pull-right" ng-if="rapport">
          <a href="{% doc %}" style="color: white">
            <i class="fa fa-download"></i> Télécharger le rapport docx
          </a>

        </button>
        
        <button class="btn btn-primary pull-right" ng-if="rapportPptx">
          <a href="{% pptx %}" style="color: white">
            <i class="fa fa-download"></i> Télécharger le rapport pptx
          </a>

        </button>

         <button class="btn btn-primary pull-right" ng-if="rapportPdf"  >
          <a href="{% pdf %}" style="color: white">
            <i class="fa fa-download"></i> Télécharger le rapport pdf
          </a>

        </button>

        <button class="btn btn-primary pull-right" ng-if="loading"  disabled>
               <i class="fa fa-spinner fa-spin"></i> Chargement ...
        </button>
      </div>
        
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#radar" data-toggle="tab">Graphiques</a></li>
         <!--  <li class="active"><a href="#principales" data-toggle="tab">Principales</a></li>
          <li class=""><a href="#details" data-toggle="tab">Détaillées</a></li>
          <li class=""><a href="#questions" data-toggle="tab">Par question</a></li>
          <li class=""><a href="#ecartees" data-toggle="tab">10 questions ecartées</a></li>
          <li class=""><a href="#serees" data-toggle="tab">10 questions serrées</a></li>
          <li class=""><a href="#ouvertes" data-toggle="tab">Questions ouvertes</a></li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="radar">
            <section>
                <h4>Radar</h4>
                <hr>
                {!! $chart_radar->render('radar_1') !!}

            </section>


            <section>
                <h4>Dimensions principales</h4>
                <hr>
                <?php

                  foreach ($chart_principales as $key => $value) {
                    echo $value->render('principale_'.$key);
                  }

                 ?>

            </section>

            <section>
                <h4>Dimensions détaillées</h4>
                <hr>
                <?php

                  foreach ($chart_details as $key => $value) {
                    echo $value->render('detail_'.$key);
                  }

                 ?>

            </section>

             <section>
                <h4>Moyenne par question</h4>
                <hr>
                <?php

                  foreach ($chart_questions as $key => $value) {
                    echo $value->render('question_'.$key);
                  }

                 ?>

            </section>
          </div>

          <!-- <div class="tab-pane active" id="principales">
            <section>



            </section>
          </div>

          <div class="tab-pane" id="details">
            <section>

            </section>
          </div>

          <div class="tab-pane" id="questions">
            <section>

            </section>
          </div>

          <div class="tab-pane" id="ecartees">
            <section>

            </section>
          </div>
          <div class="tab-pane" id="serees">
            <section>

            </section>
          </div>

          <div class="tab-pane " id="ouvertes">
            <section>

            </section>
          </div> -->


        </div>
      </div>
    </div>
  </div>


@stop
@section('js')
<script> console.log('Hi!');
$( document ).ready(function() {

//window.chart = require('chart.js');


});
 </script>

<script type="text/javascript">


let app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
});
app.controller('rapportCtrl', function($scope,$http) {
    $scope.loading = false;
    $scope.generate = function(){
      $scope.loading = true;
      let chart_questions = [];
      let chart_details = [];
      let chart_principales = [];
      let radar_1 = "";
      let radar_2 = "";
      let elmnt = "";
      let id = "";

      let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      elmnt = document.getElementById("radar_1").getElementsByTagName('canvas')[0];
      radar_1 = elmnt.toDataURL("image/png",0.5);

      <?php
      foreach ($chart_questions as $key => $value) {
        ?>
        id = "question_"+"{{ $key }}";

         elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
        chart_questions["{{ $key }}"] = (elmnt.toDataURL("image/png",0.5));

        <?php
      }
       ?>

       <?php
       foreach ($chart_details as $key => $value) {
         ?>
         id = "detail_"+"{{ $key }}";

          elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
          chart_details.push(elmnt.toDataURL("image/png",0.5));

         <?php
       }
        ?>

        <?php
        foreach ($chart_principales as $key => $value) {
          ?>
          id = "principale_"+"{{ $key }}";

           elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
           chart_principales.push(elmnt.toDataURL("image/png",0.5));

          <?php
        }
         ?>
         let data = {
                      _token: CSRF_TOKEN,
     									id : {{ $id }},
     									chart_questions: chart_questions,
     									chart_principales: chart_principales,
     									chart_details: chart_details,
     									radar_1: radar_1,
                      serrees: {!! json_encode($serrees) !!},
                      ecartees: {!! json_encode($ecartees) !!}

     								}

                    $http({
  									  method: 'POST',
  									  url: "{{ url('feedback/downloadDocx') }}",
  									  async :false,
  									  data: data
  									}).then(function successCallback(response) {
                      console.log(response);
  										$scope.loading = false;

  									    if(response.data.generated){
                          console.log(response.data.doc);
  									    	$scope.rapport = true;
  									    	$scope.doc = response.data.doc;
  									    }
  									    else{
  									    	$scope.error = response.data.message;
  									    }

  									  }, function errorCallback(response) {

  									  });

    }

    $scope.generatePdf = function(){
     $scope.loading = true;
      let chart_questions = [];
      let chart_details = [];
      let chart_principales = [];
      let radar_1 = "";
      let radar_2 = "";
      let elmnt = "";
      let id = "";

      let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      elmnt = document.getElementById("radar_1").getElementsByTagName('canvas')[0];
      radar_1 = elmnt.toDataURL("image/png",0.5);

      <?php
      foreach ($chart_questions as $key => $value) {
        ?>
        id = "question_"+"{{ $key }}";

         elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
        chart_questions["{{ $key }}"] = (elmnt.toDataURL("image/png",0.5));

        <?php
      }
       ?>

       <?php
       foreach ($chart_details as $key => $value) {
         ?>
         id = "detail_"+"{{ $key }}";

          elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
          chart_details.push(elmnt.toDataURL("image/png",0.5));

         <?php
       }
        ?>

        <?php
        foreach ($chart_principales as $key => $value) {
          ?>
          id = "principale_"+"{{ $key }}";

           elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
           chart_principales.push(elmnt.toDataURL("image/png",0.5));

          <?php
        }
         ?>
         let data = {
                      _token: CSRF_TOKEN,
                      id : {{ $id }},
                      chart_questions: chart_questions,
                      chart_principales: chart_principales,
                      chart_details: chart_details,
                      radar_1: radar_1,
                      serrees: {!! json_encode($serrees) !!},
                      ecartees: {!! json_encode($ecartees) !!}

                    }
    

                    $http({
                      method: 'POST',
                      async : false,
                      data: data,
                   
                      url: "{{ url('feedback/downloadPdf') }}",
                    }).then(function successCallback(response) {
                        $scope.loading = false;

                        if(response.data.generated){
                          console.log(response.data.doc);
                          $scope.rapportPdf = true;
                          $scope.pdf = response.data.doc;
                        }
                        else{
                          $scope.error = response.data.message;
                        }

                      }, function errorCallback(response) {

                      });

    }


    $scope.generatePptx = function(){
     $scope.loading = true;
    let chart_questions = [];
    let chart_details = [];
    let chart_principales = [];
    let radar_1 = "";
    let radar_2 = "";
    let elmnt = "";
    let id = "";

    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    elmnt = document.getElementById("radar_1").getElementsByTagName('canvas')[0];
    radar_1 = elmnt.toDataURL("image/png",0.5);

    <?php
    foreach ($chart_questions as $key => $value) {
      ?>
      id = "question_"+"{{ $key }}";

       elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
      chart_questions["{{ $key }}"] = (elmnt.toDataURL("image/png",0.5));

      <?php
    }
     ?>

     <?php
     foreach ($chart_details as $key => $value) {
       ?>
       id = "detail_"+"{{ $key }}";

        elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
        chart_details.push(elmnt.toDataURL("image/png",0.5));

       <?php
     }
      ?>

      <?php
      foreach ($chart_principales as $key => $value) {
        ?>
        id = "principale_"+"{{ $key }}";

         elmnt = document.getElementById(id).getElementsByTagName('canvas')[0];
         chart_principales.push(elmnt.toDataURL("image/png",0.5));

        <?php
      }
       ?>
       let data = {
          _token: CSRF_TOKEN,
          id : {{ $id }},
          chart_questions: chart_questions,
          chart_principales: chart_principales,
          chart_details: chart_details,
          radar_1: radar_1,
          serrees: {!! json_encode($serrees) !!},
          ecartees: {!! json_encode($ecartees) !!}

        }


        $http({
          method: 'POST',
          async : false,
          data: data,
       
          url: "{{ url('feedback/downloadPptx') }}",
        }).then(function successCallback(response) {
            $scope.loading = false;

            if(response.data.generated){
              console.log(response.data.doc);
              $scope.rapportPptx = true;
              $scope.pptx = response.data.doc;
            }
            else{
              $scope.error = response.data.message;
            }

          }, function errorCallback(response) {

          });

    }

});
</script>
@stop

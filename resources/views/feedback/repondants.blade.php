{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/styleFeedback.css">
  <link rel="stylesheet" href="/css/repondant.css">
  <link rel="stylesheet" href="/css/.css">
@stop
@section('content_header')
  	<img src="/img/apter_solutions_android-2-tranparent.png"
    				srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x"
     				class="apter_solutions_android-2-tranparent">
    
@stop
	
@section('content')
    <div class="container" style=" padding-right: 5%; padding-left: 5%;">
        <p class="-Feedback---John-Smith">360 Feedback - John Smith</p>
        <br></br>
        <div class="row" >
          <ul style="margin-bottom: 0px; height: 30px; margin-left: -45px;">
             <li class="Repondant" style="color: #757575"><i class="fas fa-user-friends"></i><a href="#" class="lienrep">Répondants</a></li>
             <li class="Resultat" style="color: #757575"><i class="fa fa-bar-chart "></i><a href="#" class="lienrep">Résultat</a></li>
          </ul>
          <hr class="ligne2">
        </div>
        <p class="Clients">Clients</p>
        <div class="col-lg-6 col-md-6 col-sm-6" style="padding-right:  0px !important;max-width: 500px;">
          <div class="col-sm  BG2" style="margin-left: -15px !important;">
             <div class="row">
               <p class=" Names">Monsieur John Smih</p>
               <div class=" icons">
                <a href="#"><i class="fa fa-edit icon1"></i></a>
                <a href="#"><i class="fa fa-close icon1"></i></a>
               </div>
             </div>
            <div class="row " style="padding-right: 45px;">
              <div class="col-lg-6  col-md-6 col-sm-12 bar3 " >
                  <p class="Monsieur">Monsieur John Smith</p>
                  <p class="Monsieur">Société : Smith &amp; CO</p>
              </div>
              <div class="col-lg-6 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-6  col-sm-offset-0 bar3 " >
                   <p class="Monsieur">johnsmith@gmail.com</p>
                   <p class="num">+26133000000</p>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6  col-md-6 col-sm-6 col-xs-8"  style="display: inline-block;">
                  <p class="date">Date : 01 Janvier 2018</p>
              </div>
            </div>
              <hr>
               <div class="row " style="height: 30px;">
                
               </div>
            </div>
          </div>
           <div class="col-lg-6 col-md-6 col-sm-6" style="padding-right:  0px !important;max-width: 500px;">
          <div class="col-sm  BG2" style="margin-left: -15px !important;">
             <div class="row">
               <p class="Importer-les-repondants">Importer les répondants</p>
             </div>
            <div class="row" style="padding-left: 40px;"> 
              <button class="Rectangle-199">Parcourir</button>
              <p class="Pas-de-fichier-selection">Pas de fichier selectionné</p>
            </div>
            <div class="row" style="padding-left: 40px; margin-top: 24px;">
              <button class="Rectangle-166"><p class="Importer">Importer</p></button>
            </div>
              <hr style="border:none !important;">
               <div class="row " style="height: 30px;">
                
               </div>
            </div>
          </div>
        </div>
        <br>
        <br>
        <div class="col-sm  BG4">
          <div style="height:50px;">
            <p class="Repondants">Répondants</p>
          </div>
          <br>
          <div style="margin-left: 20px;margin-right: 40px;">
          <table class="table">
            <thead class="HeadBG">
               <tr>
                  <th style="text-align: left !important">NOM</th>
                  <th>GROUPE</th>
                  <th>EMAIL</th>
                  <th>CODE</th>
                  <th>REPONSE</th>
                  <th></th>
                  <th style="width: 145px;"></th>
               </tr>
            </thead>
            <tbody>
                @for ($i = 0 ; $i < 6 ; $i++)
                <tr>
                    <td class="gauche">John</td>
                    <td class="centre">Doe</td>
                    <td class="centre">john@example.com</td>
                    <td class="centre">1234567890</td>
                    <td class="centre">Non</td>
                    <td style="border:none !important;"><button class="Rectangle-166A"><p class="Relancer">Relancer</p></button></td>
                    <td style="border:none !important;color:#a4afb7"><i class="fas fa-edit"></i><a href="#" class="lien">Modifier</a></td>
                </tr>
                @endfor
            </tbody>
          </table>
        </div>  
        </div>
    </div>
@stop


@section('js')
    <script> console.log('Hi!'); </script>
@stop
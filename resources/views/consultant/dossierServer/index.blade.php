{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  	<h1>
        <span class="glyphicon glyphicon-user"></span>
        Consultant
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> Consultant</a></li>
        <li class="active">Liste</li>
      </ol>
@stop
@section('js')
    <!-- <script type="text/javascript" src="/js/angular.js"></script> -->
    <!-- <script type="text/javascript" src="/js/consultant.js"></script> -->
    <script> console.log('Hi!'); </script>
    <script type="text/javascript">
      function previewFile() {
        // body...
        console.log('argument');
      }
    </script>
@stop	
@section('content')

  <div ng-app="myApp" ng-controller="myCtrl"> 
   <div class=".content-wrapper">
     <!-- Button trigger modal -->

      <button type="button"  ng-click="setImageUrl()" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
       <span class="glyphicon glyphicon-plus"></span>   Ajouter
      </button>
     </div>
     <br>

      @if (session('error_email'))
                            <div class="alert alert-danger">
                              <div class="alert-icon">
                                <i class="material-icons">error_outline</i>
                              </div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                              </button>
                              {{ session('error_email') }}
                            </div>
      @endif
      <!-- LIST OF CONSULTANT -->
      @if($users)
      <div class="row">
        @foreach($users as  $user)
             @if($user->role == 'consultant' || $user->role == 'admin')
             <div class="col-sm-12 col-md-6 col-lg-4">
               <div class="box Rectangle-582" data-widget="box-widget">
                  <div class="box-header">
                    <div class="center">
                      <h3 class="box-title title ">
                        <div>
                          <img style="vertical-align: middle;width: 80px;height: 80px;border-radius: 50%;" src="{{$user->avatar_url ? $user->avatar_url : asset('img/avatar.png')}}">
                        </div>
                      </h3>
                    </div>
                    <div class="box-tools">
                      <!-- This will cause the box to be removed when clicked -->      
                        <button ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" type="submit" class="btn btn-box-tool"><i class="fa fa-trash box-icon"></i></button>

                        <!-- This will cause the box to collapse when clicked -->
                       <!--  <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus box-icon"></i></button> -->
                    </div>
                  </div>
                  <div class="box-body">
                      <div class="box-name">
                          {{$user->lastname}} {{$user->firstname}}
                      </div>
                      <div class="titre-apter">
                         Apter-France
                      </div>
                      <div class="coordonne">
                         <span>{{$user->email}}</span>
                      </div>
                      <div class="coordonne">
                        <span>{{$user->phone}}</span>
                      </div>
                      <div class="coordonne"></div>
                      <div class="center">
                        <div class="row justify-center">
                          <div class="col-sm-4">

                            <span style="cursor: pointer;" ng-click="keyRequest($event,$id ='<?php echo $user->id ;?>',$types= {{$types}})" data-toggle="modal" data-target="#consultantKey"><i class="fa fa-key "></i><br>Acces</span>
                          </div>
                          <!-- Button trigger modal -->
                          <div class="col-sm-4">
                            <span style="cursor: pointer;" ng-click="editConsultant(<?php echo htmlspecialchars(json_encode($user)) ?>)" data-toggle="modal" data-target="#edition" ><i class="fa fa-edit "></i><br>Modifier</span>
                            <!-- <span style="cursor: pointer;"  ng-click="editRequest($event,$id ='<?php echo $user->id ;?>') " data-toggle="modal" data-target="#edition" ><i class="fa fa-edit "></i><br>Editer</span> -->
                          </div>
                          <div class="col-sm-4">
                            <span style="cursor: pointer;" ng-click="editRequest($event,$id ='<?php echo $user->id ;?>') " data-toggle="modal" data-target="#mail" ><i class="fa fa-envelope "></i><br>{{$user->mail_send==1 ? 'Envoyer une invitation': 'Envoyer une invitation'}}</span>

                          </div>
                        </div>
                      </div>
                  </div>
                  
                </div>
             </div>
             @endif
        @endforeach
      </div>
      @endif
      <!-- Modal Mail -->
      <div class="modal fade" id="mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
              Envoi Mail

            </div>
            <div class="modal-footer">
              
              <form method="POST" action="{{url('consultant/sendMail')}}">
                @csrf
                @method('POST')
                <input type="hidden" name="motif" value="lancer">
                <input type="hidden" name="consultant_id" value="<%id%>">
                <button type="submit" class="btn btn-primary">Envoyer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      <!-- Modal Key -->
      <div class="modal fade" id="consultantKey" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-title" id="exampleModalCenterTitle"><i class="fa fa-key"></i>Autorisation Consultant <!-- <% idkey %> --></div>
              <h3><% editusername %></h3>
              
            </div>
            <div class="modal-body">
              <table class="table">
                <thead>
                  <tr>
                     @foreach($types as  $type)
                        <th scope="col">{{$type->description}}</th>
                     @endforeach
                        <th scope="col">Admin</th>
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>

                     @foreach($types as  $type)
                      <td >
                        <label class="switch">
                          <input type="checkbox"  name="description[{{$type->id}}]" ng-model="description[{{$type->id}}]" form="setKey">

                          <span class="slider round"></span>
                        </label>
                       
                      
                        <input type="hidden" name="type[{{$type->id}}]" value="<%description[{{$type->id}}]%>" form ="setKey">
                      </td>

                      <!-- <% description[{{-- {{$type->id}} --}}]%> -->
                     @endforeach
                     <td>
                       <label class="switch">
                          <input type="checkbox"  name="admin" ng-model="admin" form="setKey">
                          <span class="slider round"></span>
                        </label>
                     </td>

                  </tr>
                          
                </tbody>
              </table>

              <form id="setKey" method="POST" action="{{route('consultant.setConsultantAccess')}}">
                 @csrf
                 @method('POST')
                <input type="hidden" name="consultant_id" value="<%idkey%>">
                <button type="submit" class="btn btn-primary">Enregistrer</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>              
            </div>
          </div>
        </div>
      </div>
      <!-- Modal suppression -->

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
             Voulez-vous supprimer le consultant?
            </div>
            <div class="modal-footer">
              <form role="form" method="POST" action="<% '/consultant/'+idDelete %>"  >
                         @csrf
                         @method('DELETE')
                        <button type="submit" class="btn btn-danger">Suppression</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal edition -->
      <div class="modal fade" id="edition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <form role="form" action="<% '/consultant/'+id %>" method="POST" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  @method('PATCH')
                  @csrf
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Nom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control" value="{{ old('lastname') }}"
                               placeholder="Nom"  ng-model="consultant.firstname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Prénom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" value="{{ old('firstname') }}"
                               placeholder="Prénom"  ng-model="consultant.lastname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Email*:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="consultant.email" name="email" class="form-control" value="{{ old('email') }}"

                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Mot de passe*:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="consultant.password" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <!-- <div class="form-group row has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Confirmation Mot de passe*: </label>
                       <div class="col-sm-9">
                         <input type="password" ng-model="editpassword_confirmation" name="password_confirmation" class="form-control"
                               placeholder="Confirmation Mot de passe*">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> -->
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Téléphone:</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="consultant.phone" name="phone" class="form-control" value="{{ old('phone') }}"
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- ORGANISATION -->
                  <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Organisation:</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="consultant.organisation" name="organisation" class="form-control" value="{{ old('organisation') }}"
                               placeholder="ORGANISATION">
                        <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('organisation') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Civilité:</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" ng-model="consultant.civility" class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                     >
                                <span for="huey">Monsieur</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" ng-model="consultant.civility" name="civility" value="Madame"
                                       >
                                 <span for="huey">Madame</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Langue:</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" ng-model="consultant.language" name="language">
                              <option ng-value="Français" value="Français">Français</option>
                              <option ng-value="Anglais" value="Anglais">Anglais</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
                  <!-- <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file">
                            <input type="file" name="editavatar" class="custom-file-input" onchange="angular.element(this).scope().setFile(this)" id="editavatar">
                            <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div> -->
                  
                  <button ng-disabled="!consultant.lastname || !consultant.firstname || !consultant.email" type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >Modifier</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal creation -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLongTitle">Ajouter</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('consultant.store') }}" method="post" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  {{-- <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Nom et Prénom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="username" class="form-control" value="{{ old('username') }}"
                               placeholder="Nom et Prénom"  ng-model="username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> --}}
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Nom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control" value="{{ old('username') }}"
                               placeholder="Nom"  ng-model="lastnameCrt">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">Prénom*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" value="{{ old('username') }}"
                               placeholder="Prénom"  ng-model="firstnameCrt">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Email*:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="email" name="email" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Mot de passe*:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="consultant.password" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                 <!--  <div class="form-group row has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Confirmation Mot de passe*: </label>
                       <div class="col-sm-9">
                         <input type="password" ng-model="password_confirmation" name="password_confirmation" class="form-control"
                               placeholder="Confirmation Mot de passe*">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> -->
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Téléphone:</label>
                      <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}"
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- ORGANISATION -->
                  <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Organisation:</label>
                      <div class="col-sm-9">
                        <input type="text" name="organisation" class="form-control" value="{{ old('organisation') }}"
                               placeholder="ORGANISATION">
                        <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('organisation') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Civilité:</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                     >
                                <span for="huey">Monsieur</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Madame"
                                       >
                                 <span for="huey">Madame</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Langue :</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" name="language">
                              <option selected value="Français">Français</option>
                              <option value="Anglais">Anglais</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
                 <!--  <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file"> 
                            <input type="file" name="avatar" class="custom-file-input" onchange="angular.element(this).scope().setFile(this)">
                            <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div> -->
                  <!-- ROLE 
                   <div class="form-group row has-feedback {{ $errors->has('role') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Rôle:</label>
                      <div class="col-sm-9">
                        <input type="text" name="role" class="form-control" value="{{ old('role') }}"
                               placeholder="ROLE">
                        <span class="glyphicon glyphicon glyphicon-tag form-control-feedback"></span>
                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div> -->
                  <button ng-disabled="!consultant.lastname || !consultant.firstname || !consultant.email || !consultant.password " type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >Enregistrer</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
          </div>
        </div><!-- Modal div -->
      </div><!-- Contenair div -->
   </div> 
  </div>

  <script type="text/javascript">
        var app = angular.module('myApp', [],function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
              });
        var localhostUrl=window.location.protocol + '//' + window.location.host  //'http://localhost:8000';
        app.controller('myCtrl', function($scope, $http) {

          $scope.consultant = {};
          $scope.consultantAccess = null;

          $scope.editConsultant = function(consultant){
            $scope.consultant = consultant;
          }
          $scope.setFile = function(element) {
               $scope.image_source="";
             $scope.currentFile = element.files[0];
             var reader = new FileReader();

            reader.onload = function(event) {
              $scope.image_source = event.target.result
              //$scope.$apply();
            }
            // when the file is read it triggers the onload event above.
            reader.readAsDataURL(element.files[0]);
          }


          $scope.deleteData = function function_name(event,id) {
            // body...
            $scope.idDelete = id;
            console.log(id);
          }
          $scope.setImageUrl = function setImageUrl() {
            // body...
            $scope.image_source="";
            //$scope.$apply();
          }

          $scope.keyRequest= function launch(event,id,types){
            console.log("types:",types);
            $scope.idkey = id;
            $scope.description = [];
            console.log(id);
            
            $http({
            method: 'GET',
            url: localhostUrl+'/consultant/getData/'+id
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                consultantData = response;
                console.log(response);
                $scope.editlastname =response.data.lastname; 
                $scope.editfirstname =response.data.firstname; 
                $scope.editusername =response.data.username; 
                $scope.editemail =response.data.email; 
                $scope.editphone =response.data.phone; 
                $scope.editorganisation =response.data.organisation; 
                $scope.editcivility =response.data.civility; 
                $scope.editlanguage =response.data.language; 
              $scope.editrole = response.data.role; 
                $scope.image_source="";
                
                console.log("$scope.image_source",$scope.image_source);
              }, function errorCallback(response) {
                $scope.image_source="";
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log('error get consultant getData');
            });
          $http({

            method: 'GET',
            url:localhostUrl+'/consultant/getConsultantAccess/'+id
          }).then(function successCallback(reponse){
            $scope.consultantAccess = reponse.data;
            for(var i=0;i<$scope.consultantAccess.length;i++){
              for(var j=0;j<types.length;j++){
                if(types[j].id == $scope.consultantAccess[i].type_id){
                  $scope.description[types[j].id]= true;
                  break;
                }
              }
            }
            if($scope.editrole=='admin') {
                $scope.admin=true;
            }
            else if($scope.editrole=='consultant'){
                $scope.admin=false;
            }
            console.log("consultant access:",$scope.consultantAccess);
            //edit admin
            
          },function errorCallback(reponse){
            console.log(reponse);

          })
          }
          $scope.set= function set(editlastname,editfirstname,editphone,editemail,editorganisation,editcivility,editlanguage,editrole,image_source){
            $scope.editlastname =editlastname; 
          $scope.editfirstname =editfirstname; 
          //$scope.editusername =response.data.username; 
          $scope.editemail =editemail
          $scope.editphone =editphone
          $scope.editorganisation =editorganisation; 
          $scope.editcivility =editcivility
          $scope.editlanguage =editlanguage
          $scope.edituserid =edituserid
          $scope.image_source=image_source;
          console.log('im setting this thank ')
          }
          $scope.editRequest= function launch(event,id){
            
            $scope.id = id;
            console.log(id);
            $http({
            method: 'GET',
            url: localhostUrl+'/consultant/getData/'+id
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);
                $scope.editlastname =response.data.lastname; 
                $scope.editfirstname =response.data.firstname; 
                $scope.editusername =response.data.username; 
                $scope.editemail =response.data.email; 
                $scope.editphone =response.data.phone; 
                $scope.editorganisation =response.data.organisation; 
                $scope.editcivility =response.data.civility; 
                $scope.editlanguage =response.data.language; 
                $scope.edituserid =response.data.edituserid;  
                $scope.image_source=response.data.avatar_url;
                //$scope.$apply();
                console.log("$scope.image_source",$scope.image_source);
              }, function errorCallback(response) {
                $scope.image_source="";
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);
          });
          }
        });

  </script>
@stop
@section('css')
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/fine-uploader-gallery.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
@stop

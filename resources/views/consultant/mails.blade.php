{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')


@if(isset($feedback))
  @section('content_header')
        <h1>
          <span class="glyphicon glyphicon-stats"></span>
          360 Feedback | Mails
          
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> 360 Feedback</a></li>
          <li class="active">Mails</li>
        </ol>
  @stop

@else
  @section('content_header')
      <img src="/img/apter_solutions_android-2-tranparent.png"
              srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x"
              class="apter_solutions_android-2-tranparent">
      
  @stop

@endif
@section('content')
    <div>
         <div class="box box-primary">

            <div class="box-header">
              <h3 class="box-title">Mails</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <th>Type</th>
                  <th></th>
                 
                </tr>
                
                @foreach($mails as $mail)
                <tr>
                  <td></td>
                  <td>{{ ucfirst($mail->type) }}</td>
                  <td>
                    <a href="{{url('feedback/mail/'.$mail->id.'/edit')}}"> <i class="fa fa-edit"></i> Modifier</a>
                  </td>
                </tr>
                @endforeach
                 
                  
              </table>
            </div>
          </div>



    </div>
@stop
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet"  href="/css/trix.css">
	<link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop


{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        <span class="glyphicon glyphicon-user"></span>
        Consultant
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> Consultant</a></li>
        <li class="active" style="cursor:pointer">Liste</li>
        <!-- <li class="active" id="liste" style="cursor:pointer">Détails</li> -->
        <!-- <li class="active" id="details" style="cursor:pointer;">@lang('message.liste') Excel</li> -->
      </ol>
      <br>
      <h6 class="text-right">

        <span id="details" style="cursor:pointer; padding:11px;background-color:#c0c2c3;font-weight: 600;color:white;"><i class="fa fa-reorder box-icon" style="color:white;"></i> @lang('message.details')</span><span id="liste" style="cursor:pointer; padding:11px; color:#838383; background-color: rgba(192, 194, 195, 0.2); "><i class="fa fa-file-excel-o box-icon"></i>  @lang('message.liste') Excel</span></h6>
@stop
@section('js')
    <!-- <script type="text/javascript" src="/js/angular.js"></script> -->
    <!-- <script type="text/javascript" src="/js/consultant.js"></script> -->
    <script> console.log('Hi!'); </script>
    <script type="text/javascript">
      function previewFile() {
        // body...
        console.log('argument');
      }
    </script>
@stop 
@section('content')

  <div ng-app="myApp" ng-controller="myCtrl"> 
   <div class=".content-wrapper">
     <!-- Button trigger modal -->

      <button type="button"  ng-click="setImageUrl()" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
       <span class="glyphicon glyphicon-plus"></span>   @lang("message.ajouter")
      </button>
     </div>
     <br>

      @if (session('error_email'))
                            <div class="alert alert-danger">
                              <div class="alert-icon">
                                <i class="material-icons">error_outline</i>
                              </div>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                              </button>
                              {{ session('error_email') }}
                            </div>
      @endif
      <!-- LIST OF CONSULTANT -->
      @if(count($users) >0)
      <div class="row" id="listeUsers">
        @foreach($users as  $user)
             
             <div class="col-sm-12 col-md-6 col-lg-4">
               <div class="box Rectangle-582" data-widget="box-widget">
                  <div class="box-header">
                    <div class="center">
                      <h3 class="box-title title ">
                        <div>
                          <img style="vertical-align: middle;width: 80px;height: 80px;border-radius: 50%;" src="{{$user->avatar_url ? $user->avatar_url : asset('img/avatar.png')}}">
                        </div>
                      </h3>
                    </div>
                    <div class="box-tools">
                      <!-- This will cause the box to be removed when clicked -->      
                        <button ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" type="submit" class="btn btn-box-tool"><i class="fa fa-trash box-icon"></i></button>

                        <!-- This will cause the box to collapse when clicked -->
                       <!--  <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus box-icon"></i></button> -->
                    </div>
                  </div>
                  <div class="box-body">
                      <div class="box-name">
                          {{$user->lastname}} {{$user->firstname}}
                      </div>
                      <div class="titre-apter">
                         {{$user->organisation}}
                      </div>
                      <div class="coordonne">
                         <span>{{$user->email}}</span>
                      </div>
                      <div class="coordonne">
                        <span>{{$user->phone}}</span>
                      </div>
                      <!-- <div class="coordonne">
                        <span>@lang("message.mdp") : {{$user->code}}</span>
                      </div> -->
                      <div class="coordonne"></div>
                      <div class="center">
                        <div class="row justify-center">
                          <div class="col-sm-4">

                            <span style="cursor: pointer;" ng-click="keyRequest($type = '<?php echo htmlspecialchars(json_encode($user->testAccess));?> ',$user = '<?php echo htmlspecialchars(json_encode($user)); ?>',$id ='<?php echo $user->id ;?>',$types= {{$types}})" data-toggle="modal" data-target="#consultantKey"><i class="fa fa-key "></i><br>@lang('message.acces')</span>
                          </div>
                          <!-- Button trigger modal -->
                          <div class="col-sm-4">
                            <span style="cursor: pointer;" ng-click="editConsultant(<?php echo htmlspecialchars(json_encode($user)) ?>)" data-toggle="modal" data-target="#edition" ><i class="fa fa-edit "></i><br>@lang('message.modifier')</span>
                            <!-- <span style="cursor: pointer;"  ng-click="editRequest($event,$id ='<?php echo $user->id ;?>') " data-toggle="modal" data-target="#edition" ><i class="fa fa-edit "></i><br>Editer</span> -->
                          </div>
                          <div class="col-sm-4">
                            <span style="cursor: pointer;" ng-click="editRequest($event,$id ='<?php echo $user->id ;?>') " data-toggle="modal" data-target="#mail" ><i class="fa fa-envelope"></i><br>{{ !empty($user->typeinv) ? __("message.reenvoye_mail") : __("message.envoye_inv") }}</span>

                          </div>
                        </div>
                      </div>
                  </div>
                  
                </div>
             </div>
             
        @endforeach
      </div>
      <!-- <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              @foreach($types as $type)
              <li><a href="#{{ $type->name }}" data-toggle="tab"></a>{{ $type->name }}</li>
              @endforeach
            </ul>
            <div class="tab-content">
              
              
            </div>
          </div> -->

      <div class="row" id="detailsUsers" style="display:none;">
        <div class="content" style="overflow-x:scroll;">

          <div class="box" style="padding: 2%">
            
     

                <select id="selectType" class="form-control pull-right" style="width: 20%;

margin-left: 20px;

height: 31px;">
                  <option value=" " selected="selected">@lang('message.type_test') - @lang('message.tous')</option>
                  @foreach($types2 as $type)
                  <option value="{{ $type->description }}"> {{ $type->description }}</option>
                  @endforeach 
                </select>           
              
                <div id="tous" class="tab-pane active">
                 <table class="table table-bordered" id="myTable" style="width: 100%">
                    <thead>
                   <!--    <tr >
                        <th style="text-align: center" colspan="2">Tests</th>
                        <th style="text-align: center" colspan="3">Consultants</th>
                      </tr> -->
                      <tr>

                        <th>Type</th>
                        <th>@lang('message.nom_test')</th>
                        <th>@lang('message.consultant')</th> 
<<<<<<< HEAD

=======
                        <th>@lang('message.role')</th> 
>>>>>>> 1c31161f4839c8a5e67c9178b8f50b114a91cc18
                        <th>@lang('message.organisation')</th>
                        <th>@lang('message.date_inser')</th>
                      </tr>
                    </thead>
                    <tbody>

                    @foreach($types2 as $type)
                      @foreach($tests[$type->id] as $consultant )
                        <tr>
                          <?php  
                            $date = explode(" ", $consultant->ua);
                            if($type->description == "Inventaire de Styles Motivationnels d’Apter"){
                            $title = $consultant->testName;    
                            $array = array("ISMA - ","AMSI - ");
                            $testname = str_ireplace($array, '', $title);
                          ?>
                            <td> {{ $type->description }} </td>
                            <td>{{ $testname }}</td>
                            <td>{{ $consultant->firstname }} {{ $consultant->lastname }} </td>
<<<<<<< HEAD
=======
                            <td>{{ $consultant->role == 'admin' ? __('message.admin') : ($consultant->role == 'partenaire' ? __('message.partenaire') : 'Consultant' ) }}</td>
>>>>>>> 1c31161f4839c8a5e67c9178b8f50b114a91cc18
                            <td>{{ $consultant->organisation }}</td>
                            <td>{{ $date[0] }}</td>

                          <?php  
                            } else {
                          ?>
                            <td> {{ $type->description }} </td>
                            <td>{{ $consultant->testName }}</td>
                            <td>{{ $consultant->firstname }} {{ $consultant->lastname }} </td>
<<<<<<< HEAD
=======
                            <td>{{ $consultant->role == 'admin' ? __('message.admin') : ($consultant->role == 'partenaire' ? __('message.partenaire') : 'Consultant' ) }}</td>
>>>>>>> 1c31161f4839c8a5e67c9178b8f50b114a91cc18
                            <td>{{ $consultant->organisation }}</td>
                            <td>{{ $date[0] }}</td>
                          <?php
                          }
                        ?>
                        </tr>
                        
                      @endforeach  
                      @endforeach
                      

                      
                    
                    
                    
                  </tbody>
                  </table>
                </div>
          </div>
                    
              
        </div>
      </div>

      @endif
      <!-- Modal Mail -->
      <div class="modal fade" id="mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <!-- <div class="modal-body">
              @lang("message.env_mail")

            </div> -->
            <form class="form-horizontal" method="POST" action="{{url('consultant/sendMail')}}">
              @csrf
            <input type="hidden" name="mail1" id="mail1">
            <input type="hidden" name="id" value="<% curMail %>">
            <input type="hidden" name="motif" value="lancer">
            <input type="hidden" name="consultant_id" value="<%id%>">
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                <div class="col-sm-10">
                  <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                    @foreach($mails as $m)
                    <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ? "- ".$m->updated_at : "" }}</option>
      
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                <div class="col-sm-10">
                  <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail->subject }}">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                <div class="col-sm-10">
                  <!-- tools box -->
                  <!-- <div id="mailHtml" text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-text-editor-class="form-control myform2-height" ta-html-editor-class="form-control myform2-height"></div> -->


                  <div id="summernote" name="text" ng-model="text">
                    
                  </div>


                  <!-- /. tools -->
                </div>
              </div>
          
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-default"  data-dismiss="modal">Cancel</button>

              <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
              
            </div>
            <div class="modal-footer">
              
              <!-- <form method="POST" action="{{url('consultant/sendMail')}}">
                @csrf
                @method('POST')
                <input type="hidden" name="motif" value="lancer">
                <input type="hidden" name="consultant_id" value="<%id%>">
                <button type="submit" class="btn btn-primary">@lang("message.envoyer")</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </form> -->
              
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- Modal Key -->
      <div class="modal fade" id="consultantKey" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="modal-title" id="exampleModalCenterTitle"><i class="fa fa-key"></i>@lang('message.autor_cons') <!-- <% idkey %> --></div>
              <h3><% editusername %></h3>
              
            </div>
            <form id="setKey" method="POST" action="{{route('consultant.setConsultantAccess')}}">
            @csrf
             @method('POST')
              <input type="hidden" name="consultant_id" value="<%idkey%>">
            <div class="modal-body">
              <table class="table">
                <thead>
                  <tr>
                    
                     @foreach($types as  $type)
                        <th scope="col">{{$type->description}}</th>
                     @endforeach
                        
                  </tr>
                </thead>
                <tbody>
                  
                  <tr>

                     @foreach($types as  $type)
                      <td >
                        <label class="switch">
                          <input type="checkbox"  name="description[{{$type->id}}]" ng-model="description[{{$type->id}}]" form="setKey">

                          <span class="slider round"></span>
                        </label>
                       
                      
                        <input type="hidden" name="type[{{$type->id}}]" value="<%description[{{$type->id}}]%>" form ="setKey">
                      </td>

                      <!-- <% description[{{-- {{$type->id}} --}}]%> -->
                     @endforeach
                     <!-- <td>
                       <label class="switch">
                          <input type="checkbox"  name="admin" ng-model="admin" form="setKey">
                          <span class="slider round"></span>
                       </label>
                     </td> -->

                  </tr>
                          
                </tbody>
              </table>

              <hr>
              <div class="form-group">
                  <label>@lang('message.role')</label>
                  <select class="form-control" name="role" ng-model="user.role">
                    <option value="consultant">Consultant</option>
                    <option value="admin">@lang('message.admin')</option>
                    <option value="partenaire">@lang('message.partenaire')</option>
                    <option value="admin_acad">@lang('message.admin_acad')</option>
                    <option value="admin_rech_acad">@lang('message.admin_rech_acad')</option>
                  </select>
               </div>
               
               
             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>   
               <button type="submit" class="btn btn-primary pull-right">@lang('message.enregistrer')</button>           
            </div>
             </form>
          </div>
        </div>
      </div>
      <!-- Modal suppression -->

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
             @lang('message.supp_cons2')
            </div>
            <div class="modal-footer">
              <form role="form" method="POST" action="<% '/consultant/'+idDelete %>"  >
                         @csrf
                         @method('DELETE')
                        <button type="submit" class="btn btn-danger">@lang('message.supprimer')</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.annuler')</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal edition -->
      <div class="modal fade" id="edition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" id="editDialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <form role="form" action="<% '/consultant/'+consultant.id %>" method="POST" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  @method('PUT')
                  @csrf
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.nom')*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control" value="{{ old('lastname') }}"
                               placeholder="Nom"  ng-model="consultant.lastname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.prenom')*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" value="{{ old('firstname') }}"
                               placeholder="Prénom"  ng-model="consultant.firstname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.email')*:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="consultant.email" name="email" class="form-control" value="{{ old('email') }}"

                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.mdp')*:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="consultant.password" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <!-- <div class="form-group row has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Confirmation Mot de passe*: </label>
                       <div class="col-sm-9">
                         <input type="password" ng-model="editpassword_confirmation" name="password_confirmation" class="form-control"
                               placeholder="Confirmation Mot de passe*">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> -->
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.phone'):</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="consultant.phone" name="phone" class="form-control" value="{{ old('phone') }}"
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- ORGANISATION -->
                  <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.organisation')</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="consultant.organisation" name="organisation" class="form-control" value="{{ old('organisation') }}"
                               placeholder="ORGANISATION">
                        <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('organisation') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang("message.civilite"):</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" ng-model="consultant.civility" class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                     >
                                <span for="huey">@lang('message.mr')</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" ng-model="consultant.civility" name="civility" value="Madame"
                                       >
                                 <span for="huey">@lang('message.mde')</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Langue:</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" ng-model="consultant.language" name="language">
                              <option value="fr">@lang('message.francais')</option>
                              <option value="en">@lang('message.anglais')</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
                  <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file">
                            <div class="row">
                              <div class="col-md-7">
                                <img id="imageSrc" src="<%consultant.avatar_url%>" alt="..." style="width: 250px;height: 250px;" >
                                <div><p style="text-align: center">@lang('message.SelectionnerPourRogner')</p></div>
                              </div>
                              <div class="col-md-5">
                                <!-- <input type="file" name="editavatar" class="custom-file-input" onchange="angular.element(this).scope().setFile(this)" id="editavatar"> -->
                                <a class="btn btn-primary  btn-round imgbtn" onclick="MyMoveTo()"> <span class="glyphicon glyphicon-repeat"></span>  @lang('message.tournerDroite')</a>
                                <label class="btn btn-primary  btn-round btn-reponse imgbtn" for="avatar" > <span class="  glyphicon glyphicon-download-alt"></span>  @lang('message.changer')</label>
                                <input type="file" name="avatar" id="avatar" class="custom-file-input" onchange="angular.element(this).scope().setFile(this)" id="editavatar">
                                <a class="btn btn-primary  btn-round imgbtn" onclick="recadrer()"> <span class="glyphicon glyphicon-retweet"></span> @lang('message.recadrer')</a>
                                <input type="hidden" name="x1" value="" />
                                <input type="hidden" name="y1" value="" />
                                <input type="hidden" name="w" value="" />
                                <input type="hidden" name="h" value="" />
                                <input type="hidden" name="rotate" id="inputRotate" value="">
                              </div>
                            </div>
                            
                            <style type="text/css">
                              #avatar{
                                position: absolute; 
                                top: 0;
                                right: 0;
                                bottom: 0;
                                left: 0;
                                width: 100%;
                                height: 100%;
                                z-index: -1;
                              }
                              .imgbtn{
                                display: block;
                                margin-top: 2px;
                              }
                            </style>
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>   
                     
                  </div>
                  
                  <button ng-disabled="!consultant.lastname || !consultant.firstname || !consultant.email" type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >@lang('message.modifier')</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal creation -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLongTitle">Ajouter</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('consultant.store') }}" method="post" enctype="multipart/form-data">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  {{-- <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('n_p')*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="username" class="form-control" value="{{ old('username') }}"
                               placeholder="Nom et Prénom"  ng-model="username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> --}}
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.nom')*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control" value="{{ old('username') }}"
                               placeholder="Nom"  ng-model="consultant.lastname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.prenom')*:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" value="{{ old('username') }}" placeholder="Prénom"  ng-model="consultant.firstname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.email')*:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="consultant.email" name="email" class="form-control" value="{{ old('email') }}"
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.mdp')*:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="consultant.password" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                 <!--  <div class="form-group row has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Confirmation Mot de passe*: </label>
                       <div class="col-sm-9">
                         <input type="password" ng-model="password_confirmation" name="password_confirmation" class="form-control"
                               placeholder="Confirmation Mot de passe*">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div> -->
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.phone'):</label>
                      <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control" value="{{ old('phone') }}"
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- ORGANISATION -->
                  <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.organisation')</label>
                      <div class="col-sm-9">
                        <input type="text" name="organisation" class="form-control" value="{{ old('organisation') }}"
                               placeholder="ORGANISATION">
                        <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                        @if ($errors->has('organisation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('organisation') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.civilite'):</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Monsieur"
                                     >
                                <span for="huey">@lang('message.mr')</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" class="custom-control-input"  id="huey" name="civility" value="Madame"
                                       >
                                 <span for="huey">@lang('message.mde')</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Langue :</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" name="language">
                              <option value="fr">@lang('message.francais')</option>
                              <option value="en">@lang('message.anglais')</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
                  <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file"> 
                            <input type="file" name="avatar" class="custom-file-input" onchange="angular.element(this).scope().setFile(this)">
                            <img src="<% consultant.avatar_url %>" alt="..." style="width: 80px;height: 80px;" >
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- ROLE 
                   <div class="form-group row has-feedback {{ $errors->has('role') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Rôle:</label>
                      <div class="col-sm-9">
                        <input type="text" name="role" class="form-control" value="{{ old('role') }}"
                               placeholder="ROLE">
                        <span class="glyphicon glyphicon glyphicon-tag form-control-feedback"></span>
                        @if ($errors->has('role'))
                            <span class="help-block">
                                <strong>{{ $errors->first('role') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div> -->
                  <button ng-disabled="!consultant.lastname || !consultant.firstname || !consultant.email || !consultant.password" type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >@lang('message.enregistrer')</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
              <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
          </div>
        </div><!-- Modal div -->
      </div><!-- Contenair div -->
   </div> 
  </div>
  <script type="text/javascript">
        $(document).ready(function(){

          var all_mails = [];

          $("document").ready(function(){
            @foreach($mails as $m)
              all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
            @endforeach
            $('#summernote').summernote('code', all_mails["{{ $mail->id }}"]);
            $('select').on('change', function (e) {
              valueSelected = this.value;
              $("#mail_id").val(valueSelected);
              $('#summernote').summernote('code', all_mails[valueSelected]);
            });
          });

          $('#liste').click(function() {
            $('#listeUsers').css('display', 'none');
            $('#detailsUsers').css('display', 'block');
            // $(this).css({
            //   background-Color:#b5b4b4;font-weight: 535;
            // });
            $(this).css('background-color','#c0c2c3');
            $(this).css('font-weight',600);
            $(this).css('color',"white");
            $('.fa-file-excel-o').css('color','white');

            $("#details").css("background-color", "rgba(192, 194, 195, 0.2)");
            $("#details").css('font-weight',400);
            $("#details").css('color',"#838383");
            $('.fa-reorder').css('color','#838383');
            
          });

          $('#details').click(function() {
            $('#detailsUsers').css('display', 'none');
            $('#listeUsers').css('display', 'block');
            $(this).css('background-color','#c0c2c3');
            $(this).css('font-weight',600);
            $(this).css('color',"white");
            $('.fa-reorder').css('color','white');

            $("#liste").css("background-color", "rgba(192, 194, 195, 0.2)");
            $("#liste").css('font-weight',400);
            $("#liste").css('color',"#838383");
            $('.fa-file-excel-o').css('color','#838383');
          });
          var table=$('#myTable').DataTable({
            dom: 'Bfrtip',
            buttons: [ {
              extend: 'excelHtml5',
                customize: function ( xlsx ){
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    // jQuery selector to add a border
                    $('row c[r*="10"]', sheet).attr( 's', '25' );
                }
              }],
            });
        
          $("#selectType").change(function(){
            table.search( $(this).val() ).draw();
          });
          
         
          

        });


        
        var app = angular.module('myApp', [],function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
              });
        
        var localhostUrl=window.location.protocol + '//' + window.location.host  //'http://localhost:8000';
        app.controller('myCtrl', function($scope, $http) {


          //$('select').on('change', function (e) {
            $scope.curMail = "{{ $mail->id }}";
            //console.log($scope.curMail);
          //});
          $scope.mail = {};
          $scope.mails = {};

          @foreach($mails as $m)

              $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

         //  console.log($scope.mails)
         $scope.text = $scope.mails[$scope.curMail];

         
          $scope.change = function(){
            $scope.text = $scope.mails[$scope.curMail];
          }

          $scope.save = function(e){

              //e.preventDefault();
              console.log($("#mail1").val($(".note-editable").eq(0).html())[0].defaultValue);
              $("#mail1").val($(".note-editable").eq(0).html())[0].defaultValue;

              
          }

          $scope.consultant = {};
          $scope.user = {};
          $scope.consultantAccess = null;
          $scope.editConsultant = function(consultant){
            $scope.consultant = consultant;
          }
          $scope.setFile = function(element) {
            
             $scope.currentFile = element.files[0];
             var reader = new FileReader();

            reader.onload = function(event) {
              $scope.consultant.avatar_url = event.target.result
              $scope.$apply();
            }
            // when the file is read it triggers the onload event above.
            reader.readAsDataURL(element.files[0]);
          }


          $scope.deleteData = function function_name(event,id) {
            // body...
            $scope.idDelete = id;
            console.log(id);
          }
          $scope.setImageUrl = function setImageUrl() {
            // body...
            $scope.consultant = {};
            $scope.image_source="";
            $scope.$apply();
          }

          $scope.keyRequest= function launch(type,user,id,types){
            console.log("user:",JSON.parse(user));
            console.log("type:",JSON.parse(type));
            $scope.idkey = id;
            $scope.description = [];
            $scope.user = JSON.parse(user);
            var type = JSON.parse(type);
            var user = JSON.parse(user);
            console.log(id);
            for (var k = 0;k<type.length;k++){
              $scope.description[type[k].id] = true;
              console.log('$scope.description['+type[k].id+']',$scope.description[type[k].id]);
            }
            if(user.role =='admin'){
              $scope.admin = true;
            }
            else{
              $scope.admin = false;
            }
            $scope.editusername = user.lastname+' '+user.firstname;

          }
          $scope.editRequest= function launch(event,id){
            
            $scope.id = id;
            console.log(id);
            $http({
            method: 'GET',
            url: localhostUrl+'/consultant/getData/'+id
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response);
                $scope.editlastname =response.data.lastname; 
                $scope.editfirstname =response.data.firstname; 
                $scope.editusername =response.data.username; 
                $scope.editemail =response.data.email; 
                $scope.editphone =response.data.phone; 
                $scope.editorganisation =response.data.organisation; 
                $scope.editcivility =response.data.civility; 
                $scope.editlanguage =response.data.language; 
                $scope.edituserid =response.data.edituserid;  
                $scope.image_source=response.data.avatar_url;
                //$scope.$apply();
                console.log("$scope.image_source",$scope.image_source);
              }, function errorCallback(response) {
                $scope.image_source="";
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.log(response);
          });
          }
        });

  </script>
  <script>
        jQuery(function($) {

 
            $('#imageSrc').imgAreaSelect({
                onSelectEnd: function (img, selection) {
                    $('input[name="x1"]').val(selection.x1);
                    $('input[name="y1"]').val(selection.y1);
                    $('input[name="w"]').val(selection.width);
                    $('input[name="h"]').val(selection.height);            
                },
                aspectRatio: '4:4',
                parent:'#editDialog',
               
                
            });
        });
        function recadrer(){
          var ias = $('#imageSrc').imgAreaSelect({ instance: true });
          ias.setSelection(50, 50, 200, 200, true);
          $('input[name="x1"]').val('50');
          $('input[name="y1"]').val('50');
          $('input[name="w"]').val('200');
          $('input[name="h"]').val('200'); 
          ias.setOptions({ show: true });
          ias.update();
        }
        
    </script>
    <script type="text/javascript">
      var angle=0;
      
      function MyMoveTo(){
        var img=document.getElementById('imageSrc');
         angle = (angle+90)%360;
         img.className = " rotate"+angle;
        document.getElementById('inputRotate').value=angle;
        
    }


    </script>
@stop
@section('css')
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/fine-uploader-gallery.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
  <link rel="stylesheet" type="text/css" href="/css/imgareaselect.css">
@stop

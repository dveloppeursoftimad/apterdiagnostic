@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="vendor/adminlte/style/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
            <div class="login-box Rectangle-565">
                <div class="login-logo" style="margin-top: -40px;">
                    <img src="img/apter_solutions_android-2-tranparent.png" width="315px" height="140px">
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body Rectangle-orange do35a-iStock_890386528_copie">
                    <p class="login-box-msg ESPACE-CONSULTANT"><i class="fas fa-user" style="width: 19.3px;height: 22px;color: #ffff;"></i>&nbsp;&nbsp;ESPACE CONSULTANT</p>
                    <form class="form-group" action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                        {!! csrf_field() !!}
                        <div class="row" style="padding-left: 25px;padding-bottom: 20px;margin-top: -15px;">
                            <div class="col-md-12">
                             <div class="input-group Rectangle-569">
                                  <span class="input-group-addon" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;border: none;"><i class="far fa-envelope" style="width: 16px;height: 14px;color: #757575;padding-left: 10px;"></i></span> 
                                  <input type="text" name="email" class="form-control Rectangle-569 Email" value="{{ old('email') }}" placeholder="{{ trans('adminlte::adminlte.email') }}" style="border: none;padding-left: 15px;">
                             </div>
                             @if ($errors->has('email'))
                                <span >
                                    <strong class="Email" style="color: #6d0000;">{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                            <div class="row" style="padding-left: 25px;padding-bottom: 20px;">
                                <div class="col-md-12">
                                 <div class="input-group Rectangle-569"style="border: none; margin-bottom:10px">
                                    <span class="input-group-addon" style="border-top-left-radius: 20px;border-bottom-left-radius: 20px;border: none;"><i class="fas fa-key" style="width: 16px;height: 14px;color: #757575;padding-left: 10px;"></i></span>
                                    <input type="text" id="myInput" name="password" class="form-control Rectangle-569 Mot-de-passe" placeholder="Mot de passe"  style="border: none;padding-left: 15px;"> 
                                 </div>
                                 
                                 @if ($errors->has('password'))
                                    <span>
                                        <strong class="Mot-de-passe" style="color: #6d0000;">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-12" style="color:white;" onclick="myFunction()">
                                    <div style="border: none;margin-left: 20px;text-decoration: underline; font-weight:normal" id="eye">
                                        <i id="eye2" style="font-weight:500;text-decoration: underline;" class="fas fa-eye">&nbsp;Cacher le mot de passe</i>
                                    </div>
                                </div>


                            </div>
                                <div class="row" style="padding-top: 15px;">
                                <div class="col-md-12 text-center">
                                    <!-- /.col -->
                                    <button type="submit" class="btn Rectangle-567 Se-connecter">Se connecter</button>
                                    <!-- /.col -->
                                </div>
                                </div>
                        </form>
                    </div>  
                </div>
                <!-- /.login-box-body -->
<!-- /.login-box -->


@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    <script type="text/javascript">
        function myFunction() {
            var x = document.getElementById("myInput");
            if (x.type === "text") {
                $('#eye #eye2').removeClass('fas fa-eye');
                $('#eye #eye2').addClass('fas fa-eye-slash');
                $("#eye #eye2").text(" Afficher le mot de passe");
                x.type = "password";
            } else {
                $('#eye #eye2').removeClass('far fa-eye-slash');
                $('#eye #eye2').addClass('fas fa-eye');
                x.type = "text";
                $("#eye #eye2").text(" Cacher le mot de passe");
            }
        }
    </script>
    @yield('js')
@stop

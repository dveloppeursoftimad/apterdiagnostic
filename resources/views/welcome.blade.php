{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')
     <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('content_header')
    <img src="/img/apter_solutions_android-2-tranparent.png" class="apter_solutions_android-2-tranparent">
@stop

@section('content')
    <div class="container" style=" padding-right: 5%; padding-left: 15px">
    <p class="Bienvenu">Bienvenu sur le site d&apos;administration d&apos;APTER!</p>
    <div class="row" >
      <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-sm BG">
            <div class="Rectangle-2">
                <div class="Rectangle-82">
                    <p class="APTER-360-Feedback">APTER 360 Feedback</p>
                    <i class="fa fa-bar-chart fa-3x icon_dashboard" ></i>
                </div>
            </div>  
            <button class="Rectangle-3">Questionaire</button>
            <button class="Rectangle-4">Voir Tests</button>
            
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-sm BG">
            <div class="Rectangle-2">
                <div class="Rectangle-82">
                    <p class="APTER-360-Feedback">APTER 360 Flexibilité et Perception</p>
                    <i class="fa fa-bar-chart fa-3x icon_dashboard" ></i>
                </div>
            </div>  
             <button class="Rectangle-3">Questionaire</button>
            <button class="Rectangle-4">Voir Tests</button>
        </div>
     </div>
     <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-sm BG">
            <div class="Rectangle-2">
                <div class="Rectangle-82">
                    <p class="APTER-360-Feedback">APTER Climats Managériaux</p>
                    <i class="fa fa-bar-chart fa-3x icon_dashboard" ></i>
                </div>
            </div>  
             <button class="Rectangle-3">Questionaire</button>
            <button class="Rectangle-4">Voir Tests</button>
        </div>
     </div>
     <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-sm BG">
            <div class="Rectangle-2">
                <div class="Rectangle-82">
                    <p class="APTER-360-Feedback">Inventaire de Styles
Motivationnels</p>
                    <i class="fa fa-bar-chart fa-3x icon_dashboard" ></i>
                </div>
            </div>  
            <button class="Rectangle-3">Questionaire</button>
            <button class="Rectangle-4">Voir Tests</button>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-sm BG">
            <div class="Rectangle-2">
                <div class="Rectangle-82">
                    <p class="APTER-360-Feedback">APTER Climage</p>
                    <i class="fa fa-bar-chart fa-3x icon_dashboard" ></i>
                </div>
            </div>  
             <button class="Rectangle-3">Questionaire</button>
            <button class="Rectangle-4">Voir Tests</button>
        </div>
    </div>
 </div>
</div>
@stop


@section('js')
    <script> console.log('Hi!'); </script>
@stop
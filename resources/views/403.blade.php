{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <!-- 	<h1>
        <span class="glyphicon glyphicon-remove"></span>
        Vous n'êtes pas autorisé à accéder à cette page.
        
      </h1> -->
    
@stop
@section('js')
    <script type="text/javascript" src="/js/angular.js"></script>
    <script type="text/javascript" src="/js/myscript.js"></script>
    <script> console.log('Hi!'); </script>
@stop	
@section('content')
     <!-- Main content -->
    <section class="content">
      <div class="error-page">
       <!--  <h2 class="headline text-yellow"> 404</h2>
 -->
        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Vous n’avez pas encore accès à cet outil.</h3>

          <p>
           Pour l’activer et avoir plus d’informations, merci de nous contacter : <a>contact@apter-france.com</a>
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
<!-- /.content -->
@stop
@section('css')
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
@stop

{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        <span class="glyphicon glyphicon-stats"></span>
        @lang('message.travaux_admin2')
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.travaux_admin2')</a></li>
        <li class="active">@lang('message.liste')</li>
      </ol>
    
@stop
@section('content')
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
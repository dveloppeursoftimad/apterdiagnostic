{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  	<img src="/img/apter_solutions_android-2-tranparent.png"
    				srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x"
     				class="apter_solutions_android-2-tranparent">
    
@stop
	
@section('content')
    <div class="container" style="padding-right: 5%;">
        <p>Questionnaire</p>
    </div>
@stop
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
@extends('diagnostic-login')
@section('content')

	<div class="section section-basic section-connexion" ng-app="mdpApp" ng-controller="mdpController">
		<div class="container" style="position: relative;">
			<div class="title text-center">
				<h2 class="connexion">
					<!-- <i class="fa fa-user"></i> -->
					 @lang('message.connexion')</h2>
			</div>
			<div class="container" >
				<form method="post" action="{{ route('diagnostic-login') }}">
					@csrf
					<div class="row">
						@if (session('error_log'))
							<div class="alert alert-danger alert-block">
								<div class="alert-icon">
							    <i class="material-icons">error_outline</i>
							  </div>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
							    <span aria-hidden="true"><i class="material-icons">clear</i></span>
							  </button>	
							    <strong>@lang('message.error_log')</strong>
							</div>
						@endif
						@if(Session::has('mdp_change'))
			                <div class="alert alert-success alert-dismissible">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <h4><i class="icon fa fa-check"></i> Succès!</h4>
			                    {{ Session::get('mdp_change') }}
			                </div>
			            @endif
						<div class="col-sm-12">
							<div class="form-group has-success">
								<div class="form-group">
									<label style="color: gray;">@lang('message.email')</label>
								
									<input type="text" name="email" class="form-control" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);" value="{{ old('email') }}" placeholder="{{ __('message.votre_mail') }}">
									</div>
									<i class="fa fa-envelope form-control-feedback" style="color: gray;"></i>
							</div>
							<div>
									@if ($errors->has('email'))
	                                <span >
	                                    <strong class="Email" style="color: #6d0000;">{{ $errors->first('email') }}</strong>
	                                </span>
	                                @endif
							</div>
						</div>
				
						<div class="col-sm-12">
							<div class="form-group has-success">
								<div class="form-group">
									<label style="color: gray;">@lang('message.mdp')</label>
									<input id="myInput" type="<% input_type %>" ng-model="showHide" class="form-control" name="password" placeholder="{{ __('message.votre_pass') }}"  style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
								</div>
								<i class="fa fa-key form-control-feedback" style="color: gray;"></i>
								<!-- <i ng-click="showPassword()" class="<% showHideClass %>" style="color: gray; cursor:pointer"></i> -->
								<input type="checkbox" ng-click="showPassword()"> @lang("message.affichePass")
							</div>
							<div>
								 	@if ($errors->has('password'))
	                                <span>
	                                    <strong class="Mot-de-passe" style="color: #6d0000;">{{ $errors->first('password') }}</strong>
	                                </span>
	                                @endif
							</div>
						</div>
						<br/>
						<!-- <div class="col-sm-10">
						</div>
						<div class="col-lg-3 col-sm-6">
							
						</div>
						 -->
						<div class="col-sm-12 text-center" style="margin-top: 20px">

							<button type="submit" class="btn btn-primary btn-round" style="background-color: #ffd85b;">
								@lang('message.connecter')
							</button>	

							<!-- <a href="{{ route('inscription') }}" class="lien">
								@lang('message.inscrire')  
							</a> -->

						</div>

						<div class="col-md-2 text-left" style="margin-top: 20px">
							


						</div>
						<div class="col-md-10 text-right" style="margin-top: 20px">
							@lang('message.insc_compte')
							<a href="{{ route('inscription') }}" class="lien">
								@lang('message.cliquer')	  
							</a>
							<!-- <a href="/securite/oubliee" class="lien">
								oublié	  
							</a> -->
							<br>
							<!-- @if (Route::has('password.request'))
		                        <a href="{{ route('password.request') }}" class="lien"><i class="fa fa-question-circle"></i> 
		                            @lang('message.oublie')
		                        </a>
		                    @endif -->
		                    <a href="/securite/oubliee" class="lien">
								<i class="fa fa-question-circle"></i>@lang('message.oublie')	  
							</a>
							

						</div>

					</div>
				</form>
			</div>
			
		</div>
	</div>						

</div>
</div>
<!--/div-->

<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });
	});
</script>

<script type="text/javascript">
    var app = angular.module('mdpApp', [],function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
    app.controller('mdpController', function($scope) {
    	$scope.input_type = "text";
    	$scope.showHideClass = "fa fa-eye";
    	$scope.showPassword = function(){
    		
    		if($scope.showHide != null){
    			if($scope.input_type == "text"){
    				$scope.input_type = 'password';
    				$scope.showHideClass = "fa fa-eye";
    			} else {
    				$scope.input_type = "text";
    				$scope.showHideClass = "fa fa-eye-slash";	
    			}
    		}
    	}
   	});
</script>


@endsection('content')
@extends('diagnostic')
@section('content')
<div ng-app="homeApp" ng-controller="homeController" class="section section-basic mobile" style="padding: 10px 0;">
		<div class="container" style="position: relative;">
			<br/>
			<div class="title_mobile">
				<h3 class="font text-center" style="color: #3c4858;">@lang('message.mess_bienvenue')</h3>
			</div>
			<br/>
			<br/>
			<div>
				@if (session('success'))
					<div class="alert alert-success alert-dismissible">
				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				      	<h4><i class="icon fa fa-check"></i> Succès!</h4>
				      	{{ Session::get('success') }}
				    </div>
				@endif
			</div>
			<div>
				@if(session('error_code'))
					<div class="alert alert-danger">
						<div class="alert-icon">
				      <i class="material-icons">error_outline</i>
				    </div>
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				      <span aria-hidden="true"><i class="material-icons">clear</i></span>
				    </button>
						@lang('message.err_code')
					</div>
				@endif
			</div>

			<div>
				@if(session('error_consultant'))
					<div class="alert alert-danger">
						<div class="alert-icon">
				      <i class="material-icons">error_outline</i>
				    </div>
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				      <span aria-hidden="true"><i class="material-icons">clear</i></span>
				    </button>
						@lang('message.consultant_not_found')
					</div>
				@endif
			</div>
			<div>
				@if(session('error_role'))
					<div class="alert alert-danger">
						<div class="alert-icon">
				      <i class="material-icons">error_outline</i>
				    </div>
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				      <span aria-hidden="true"><i class="material-icons">clear</i></span>
				    </button>
						@lang('message.error_role')
					</div>
				@endif
			</div>
			<div>
				@if(session('error_testcode'))
					<div class="alert alert-danger">
						<div class="alert-icon">
				      <i class="material-icons">error_outline</i>
				    </div>
				    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				      <span aria-hidden="true"><i class="material-icons">clear</i></span>
				    </button>
						@lang('message.err_test')
					</div>
				@endif
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-12 menu">
					<a data-toggle="modal" data-target="#climage">
						<button class="btn btn-primary btn-round btn-reponse" style="width: 100%">
			                <i class="large material-icons">assessment</i> <b>@lang('message.climage')</b>
			              <div class="ripple-container"></div>
			          	</button>
					</a>
						
				</div>

				<div class="col-md-6 col-sm-12 menu" id="dataIsma">
					<a data-toggle="modal" data-target="#isma">
						<button class="btn btn-primary btn-round btn-reponse" style="width: 100%">

			                <i class="large material-icons">assessment</i> <b>@lang('message.inventaire')</b>

			              <div class="ripple-container"></div>
			          	</button>
			         </a>
				</div>

				<div class="col-md-6 col-sm-12 menu">

					<a data-toggle="modal" data-target="#feedback">


						<button class="btn btn-primary btn-round btn-reponse" style="width: 100%">

			                <i class="large material-icons">assessment</i> <b>@lang('message.feedback')</b>

			              <div class="ripple-container"></div>
			          	</button>
					</a>

					<!--a data-toggle="modal" data-target="#feedback">
						<button class="btn btn-primary btn-round btn-reponse" style="width: 100%">
			                <i class="large material-icons">help</i> <b>@lang('message.feedback')</b>
			              <div class="ripple-container"></div>
			          	</button>
					</a-->
						
				</div>
				<div class="col-md-6 col-sm-12 menu">
					<a data-toggle="modal" data-target="#360diagnostic">
						<button class="btn btn-primary btn-round btn-reponse" style="width: 100%">
			                <i class="large material-icons">assessment</i> <b>@lang('message.perf_motiv')</b>
			              <div class="ripple-container"></div>
			          	</button>
			         </a>
				</div>
	
		</div>
		
	</div>
	<div class="modal fade" id="360diagnostic">
        <div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                
	                <h4 class="modal-title titre_modal" style="text-align:center">@lang('message.entre_info')</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  	<span aria-hidden="true">&times;</span></button>
	            </div>

              	<form action="{{ route('360diagnostic-front-saveUser') }}" method="POST">
                	@csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="code" style="color: gray;">@lang('message.code_360diagnostic')</label>
                            <input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="code" id="code" placeholder="Code @lang('message.perf_motiv')">
                        </div>
                          
                    </div>
                        <!-- /.box-body -->

                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                      <button type="submit" class="btn btn-primary">@lang('message.participer')</button>
                    </div>
              </form>
            </div>
		</div>	
	</div>
	<div class="modal fade" id="climage">
        <div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                
	                <h4 class="modal-title titre_modal" style="text-align:center">@lang('message.entre_info')</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  	<span aria-hidden="true">&times;</span></button>
	            </div>

              	<form action="{{ route('climage-front-saveUser') }}" method="POST">
                	@csrf
                    <div class="modal-body">

                        <!-- <div class="form-group">
                            <label for="code" style="color: gray;">Code</label>
                            <input type="text" class="form-control" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);" name="code" id="code" placeholder="">
                        </div> -->

                        <div class="row">
                        	@if(Auth::user())
	                        	<div class="col-sm-12">
	                        		<label for="code" style="color: gray;">@lang("message.code_climage")</label>
	                        		<!-- <label for="code" style="color: gray;">Code</label> -->

	                            	<input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="code" id="code" placeholder="Code @lang('message.climage')">

	                        	</div>
	                        @else
	                        	<div class="col-sm-6">
	                        		<label for="code" style="color: gray;">@lang('message.votre_mail') :</label>

	                        		<input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="email" id="email"  placeholder="@lang('message.votre_mail')">

	                        	</div>
	                        	<div class="col-sm-6">
	                        		<label for="code" style="color: gray;">@lang("message.code_climage")</label>
	                        		<!-- <label for="code" style="color: gray;">Code</label> -->

	                            	<input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="code" id="code" placeholder="Code @lang('message.climage')">

	                        	</div>
                        	@endif
                        	
                        </div>
                          
                    </div>
                        <!-- /.box-body -->

                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                      <button type="submit" class="btn btn-primary">@lang('message.participer')</button>
                    </div>
              </form>
            </div>
		</div>	
	</div>
	
	<div class="modal fade" id="feedback">
        <div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                
	                <h4 class="modal-title titre_modal" style="text-align:center">@lang('message.entre_info')</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  	<span aria-hidden="true">&times;</span></button>
	            </div>

              	<form action="{{ route('feedback-front-saveUser') }}" method="POST">
                	@csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="code" style="color: gray;">@lang("message.code_feedback")</label>
                            <input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="code" id="code" placeholder="Code @lang('message.feedback')">
                        </div>
                          
                    </div>
                        <!-- /.box-body -->

                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                      <button type="submit" class="btn btn-primary">@lang('message.participer')</button>
                    </div>
              </form>
            </div>
		</div>	
	</div>

	<div class="modal fade" id="isma">
        <div class="modal-dialog">
            <div class="modal-content">
              	@if(Auth::user())
	              		<div class="modal-header">
	                
		                <h4 class="modal-title titre_modal" style="text-align:center">@lang('message.isma') - @lang('message.info_isma')</h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  	<span aria-hidden="true">&times;</span></button>
		            </div>

	              	<form action="{{ route('isma-front-saveUser') }}" method="POST">
	                	@csrf
	                	<input type="hidden" name="idUser" value="" >
	                    <div class="modal-body">

	                        <div class="form-group">
	                            <label for="code" style="color: gray;"><b>@lang('message.votre_consultant')</b></label>
	                            <div class="row">
	                            	<div class="col-sm-6">
	                            		<input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="consultant_prenom"  placeholder="@lang('message.prenom')">
	                            	</div>
	                            	<div class="col-sm-6">
	                            		 <input type="text" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" name="consultant_nom"  placeholder="@lang('message.nom')">
	                            		
	                            	</div>
	                            </div>
	                           
	                        </div>
	                          
	                    </div>
	                        <!-- /.box-body -->

	                    <div class="modal-footer">
	                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
	                      <button type="submit" type="submit" class="btn btn-primary">@lang('message.participer')</button>
	                    </div>
	              </form>
              	@else
					<div class="modal-header">
	                
		                <h4 class="modal-title titre_modal" style="text-align:center">@lang('message.isma') - @lang('message.connexion')</h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  	<span aria-hidden="true">&times;</span></button>
		            </div>
					<div class="modal-body">
						<form method="post" action="{{ route('diagnostic-login') }}">
							@csrf
							<div class="row">
								<input type="hidden" name="redirect" value="isma">
								<div class="col-sm-6">
									<div class="form-group">
										<label style="color: gray;">@lang('message.email')</label>
									
										<input type="text" name="email" class="form-control" style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);" value="{{ old('email') }}" placeholder="{{ __('message.votre_mail') }}">
									</div>
									<i class="fa fa-envelope form-control-feedback" style="color: gray;"></i>
								</div>

								<div class="col-sm-6">

									<div class="form-group">
										<label style="color: gray;">@lang('message.mdp')</label>
										<input id="myInput" type="<% input_type %>" ng-model="showHide" class="form-control" name="password" placeholder="{{ __('message.votre_pass') }}"  style="background-image: linear-gradient(0deg,#bad2de 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#bad2de 1px,hsla(0,0%,82%,0) 0);">
									</div>
									<i class="fa fa-key form-control-feedback" style="color: gray;"></i>
									<!-- <i ng-click="showPassword()" class="<% showHideClass %>" style="color: gray; cursor:pointer"></i> -->
									<input type="checkbox" ng-click="showPassword()"> @lang("message.affichePass")
								
									<div>
									 	@if ($errors->has('password'))
		                                <span>
		                                    <strong class="Mot-de-passe" style="color: #6d0000;">{{ $errors->first('password') }}</strong>
		                                </span>
		                                @endif
									</div>
								</div>
								<br/>
								<!-- <div class="col-sm-10">
								</div>
								<div class="col-lg-3 col-sm-6">
									
								</div>
								 -->
								<div class="col-sm-12 text-right" style="margin-top: 20px">

									 <button type="submit" type="submit" class="btn btn-primary">@lang('message.connecter')</button>	

									<!-- <a href="{{ route('inscription') }}" class="lien">
										@lang('message.inscrire')  
									</a> -->

								</div>

								<div class="col-md-2 text-left" style="margin-top: 20px">
									


								</div>
								<div class="col-md-10 text-right" style="margin-top: 20px">
									@lang('message.insc_compte')
									<a href="{{ route('inscription') }}" class="lien">
										@lang('message.cliquer')	  
									</a>
									<!-- <a href="/securite/oubliee" class="lien">
										oublié	  
									</a> -->
									<br>
									<!-- @if (Route::has('password.request'))
				                        <a href="{{ route('password.request') }}" class="lien"><i class="fa fa-question-circle"></i> 
				                            @lang('message.oublie')
				                        </a>
				                    @endif -->
				                    <a href="/securite/oubliee" class="lien">
										<i class="fa fa-question-circle"></i>@lang('message.oublie')	  
									</a>
									

								</div>

							</div>
								
						</form>
					</div>
              		
              	@endif
            </div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });

	    @if(\Session::get('isma'))
	    	$("#isma").appendTo("body").modal("show");
	    	{!! json_encode(\Session::forget('isma')) !!};
	    @endif


   
	});

</script>

<script type="text/javascript">
        var app = angular.module('homeApp', [],function($interpolateProvider) {
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
              });
        var localhostUrl=window.location.protocol + '//' + window.location.host  //'http://localhost:8000';
        app.controller('homeController', function($scope, $http) {
        	// $scope.user ={};
        	// $scope.user = {!! json_encode(Auth::user()) !!};
        	// console.log($scope.user);
       });


  </script>

<!--/div-->
@endsection('content')

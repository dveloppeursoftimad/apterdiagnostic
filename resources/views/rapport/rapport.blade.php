{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="/css/styleFeedback.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
  <link rel="stylesheet" href="/chart/chartphp.css">
@stop
@section('content_header')
     <h1>
        <span class="glyphicon glyphicon-stats"></span>
        Rapport
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> 360 FB</a></li>
        <li class="active">Rappor</li>
      </ol> 
@stop
@section('content')

  
  <div ng-app = "app" ng-controller="myCtrl">
     <input type="hidden" name="url" id="url" value="{{url('feedback/getReponseRepondant/'.$id)}}">
     <input type="hidden" name="urlGetQuestion" id="urlGetQuestion" value="{{url('feedback/allQuestion/'.$id)}}">
     {{-- <input type="hidden" name="img" value="{{$img}}"> --}}
     <img src="<%src%>">
     <img src="<%src1%>">
     <div style="width: 400px;height: 400px;">
       <canvas id="myChart" width="200" height="200"style="display: none;"></canvas>
     </div>
      
     <div style="width: 400px;height: 400px;">
       <canvas id="myChart1" width="200" height="200"style="display: none;"></canvas>
     </div>
  </div>
 
@stop
@section('js')
<script> console.log('Hi!'); </script>
<script type="text/javascript" src="/chart/jquery.min.js"></script>
<script type="text/javascript" src="/chart/js/Chart.min.js"></script>
<script type="text/javascript" src="js/angular.js"></script>
<script type="text/javascript">


var app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});
app.controller('myCtrl', function($scope,$http) { 
  

  var url = document.getElementById('url');
  var urlGetQuestion = document.getElementById('urlGetQuestion');
  var ctx = document.getElementById('myChart');
  var ctx1 = document.getElementById('myChart1');
  var dataSet ={
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [{
              label: '# of Votes',
              data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      };
  var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: dataSet,
      options: {
          scales: {
              xAxes: [{
                  stacked: false,
                 
              }],
              yAxes: [{
                  stacked: false,

              }]
          },
          animation: false
      }
  });
  var myChart1 = new Chart(ctx1, {
      type: 'horizontalBar',
      data: dataSet,
      options: {
          scales: {
              xAxes: [{
                  stacked: false,
                 
              }],
              yAxes: [{
                  stacked: false,

              }]
          },
          animation: false
      }
  });
  var dataUrl = ctx.toDataURL('image/png', 1);
  var dataUrl1 = ctx1.toDataURL('image/png', 1);
  $scope.src = dataUrl;
  $scope.src1 = dataUrl1;
  console.log(urlGetQuestion.value)
  $http({
    method:'GET',
    url:url.value,
  }).then(function successCallback(data){
    console.log("tous les reponses",data);
    var reponses = data.data;
    $http({
      method:'GET',
      url:urlGetQuestion.value
    }).then(function successCallback(data){
      console.log("tous les questions",data);
      var questions = data.data;
      var resulatFinal =[];
      for(var i=0;i<questions.length;i++){
        let dataPush = [];
        for(var j=0;j<reponses.length;j++){
          if(reponses[j].question_id == questions[i].id){
            dataPush.push(reponses[j]);
          }
        }
        resulatFinal.push(dataPush);
      }
      console.log(resulatFinal);
    },function errorCallback(data){

    })
  }, function errorCallback(data){
    console.log(data)
  });
});
</script>
@stop
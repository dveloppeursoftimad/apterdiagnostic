{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    @lang('message.climage')  > {{ $climage->name }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.climage')</a></li>
    <li class="active">@lang('message.resultat')</li>
  </ol>
@stop
@section('content')
@php
$i=1;	
@endphp 
<div class="box-body">
  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
        	@foreach($page2 as $p => $page)
				@if($p > 0)
					@if($lang=='fr')
						<li class="@if($i==1) active @endif"><a href="#resultat{{ $i }}" data-toggle="tab"> @lang('message.resultat') {{ $i }}</a></li>
						
					@else
						<li class="@if($i==1) active @endif"><a href="#resultat{{ $i }}" data-toggle="tab"> @lang('message.resultat') {{ $i }}</a></li>
					@endif	
					@php
					$i++;	
					@endphp
				@endif
			@endforeach

        </ul>
        
        <div class="tab-content  table-responsive">
        	@php
        	$j=1;
        	@endphp
        	@foreach($page2 as $p => $page)
				@if($p > 0)
					
					<div id="resultat{{ $j }}" class="tab-pane @if($j==1) active @endif">
						<h3>
							@if($lang=='fr')
								{{ $page->title_fr }}
							@else
								{{ $page->title_en }}
							@endif
						</h3>
						@php
						$frontdate=[];
						$fronttime=[];
						$i=0;
						@endphp
						@foreach($resultats as $date => $resultat)
							@php
								$frontdate[$i]= explode(" ", $date)[0];
								$fronttime[$i.' '.$frontdate[$i]]= explode(" ", $date)[1];
								 $i++;
							@endphp
						@endforeach
						@php
							$frontdate=array_unique($frontdate);
							sort($frontdate);
						@endphp
						<div style="display: flex; justify-content: space-around;">
							<select id="selectdate{{ $j }}">
								<option value=" " class="all" selected>all</option>
									@foreach($frontdate as $dateFr )
									
									
									<option value="{{ $dateFr }}">{{ $dateFr }}</option>
									@endforeach
							</select>
							<select id="selecttime{{ $j }}">
								<option value=" " class="all" selected>all</option>
									@foreach($fronttime as $datet => $dateFr )
									
									
									<option class="{{ $datet }} " value="{{ $dateFr }}">{{ $dateFr }}</option>
									@endforeach
								
							</select>
						</div>
						<div class="table-responsive">
			
			
			
		<table class="table table-striped table-bordered" id="myTable{{ $j }}">
			<thead>
				<tr>
					<th style="text-align: center;" rowspan="2">Date</th>
					<th rowspan="2">@lang('message.repondants')</th>
					
					@foreach ($page->questions as $key => $quest)
						<th colspan="{{ count($quest->reponses) }}">{{ $quest->question_fr }}</th>
					@endforeach
				</tr>
					
					@foreach ($page->questions as $key => $quest)
						@foreach ($quest->reponses as $rep => $reponse)
							@if($lang=='fr')
								<th>{{ $reponse->reponse_fr }}</th>
							@else
								<th> {{ $reponse->reponse_en }}</th>
							@endif
							
						@endforeach
					@endforeach
						
				</tr>
			</thead>
			<tbody>
				
				@foreach($resultats as $date => $resultat)
					
					@foreach($resultat as $rep => $reponses)
						<tr>
							<td>{{ $date }}</td>
							<td>{{ $rep }}</td>
							
									@foreach ($page->questions as $key => $quest)
										@foreach ($quest->reponses as $rep => $reponse)
											@if(array_key_exists($reponse->id,$reponses))
												<td style="text-align: center">1</td>
											@else
												<td> </td>
											@endif
										@endforeach
									@endforeach
								
						</tr>
					@endforeach
				@endforeach

			</tbody>
		</table>
						</div>
        			</div>
					@php
        				$j++;
        			@endphp
				@endif
			@endforeach
        	
        </div>
      </div>
    </div>
  </div>
</div>

@stop


@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop



@section('js')
<script> console.log('Hi!'); </script>
  <script type="text/javascript">

	$(document).ready(function(){
	    @php
    	$k=1;
    	@endphp
    	@foreach($page2 as $p => $page)
			@if($p > 0)
			    var table{{ $k }}=$('#myTable{{ $k }}').DataTable({
		        	dom: 'Bfrtip',
			        buttons: [ {
		            extend: 'excelHtml5',
		            customize: function ( xlsx ){
		                var sheet = xlsx.xl.worksheets['sheet1.xml'];
		 
		                // jQuery selector to add a border
			                $('row c[r*="10"]', sheet).attr( 's', '25' );
		            	}
		        	}]
			    });
			    $("#selectdate{{ $k }}").change(function(){
			    	table{{ $k }}.search( $(this).val()+' '+$("#selecttime{{ $k }}").val() ).draw();
			    	$('#selecttime{{ $k }} option').hide();
			    	$('#selecttime{{ $k }} option.all').show();
			    	if($(this).val()==" "){
			    		$('#selecttime{{ $k }} option').show();
			    	}

			    	else
						$('#selecttime{{ $k }} option.'+$(this).val()).show();			    		

			    });
			    $("#selecttime{{ $k }}").change(function(){
			    	table{{ $k }}.search( $("#selectdate{{ $k }}").val()+' '+$(this).val() ).draw();
			    });
			@php
    			$k++;
    		@endphp
	    	@endif
	    @endforeach
	});
  </script>
  
@stop
{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    @lang('message.climage')  > {{ $climage->name }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.climage')</a></li>
    <li class="active">@lang('message.resultat')</li>
  </ol>
@stop
  
@section('content')
<div ng-app="resultatApp" ng-controller="resultatController">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">@lang('message.resultat')</h3>
        </div>
            <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  @if($climage->type->name != "climage-interlocuteur")
                    <li class="active"><a href="#resultat1" data-toggle="tab">@lang('message.resultat')1</a></li>
                  @endif
                  <li><a href="#resultat2" data-toggle="tab">@lang('message.resultat')2</a></li>
                  @if($climage->type->name != "climage-interlocuteur")
                    <li><a href="#resultat3" data-toggle="tab">@lang('message.resultat')3</a></li>
                  @endif
                  @if($climage->type->name == "climage-negociation")
                    <li><a href="#resultat4" data-toggle="tab">@lang('message.resultat')4</a></li>
                  @endif
                  <li><a href="#trombinoscope" data-toggle="tab">Trombinoscope</a></li>
                </ul>
                <div class="tab-content">
                  <div id="resultat1" class="tab-pane active">
                    @if($climage->type->name != "climage-interlocuteur")
                      <div class="tab-pane active show" id="resultat1">
                        <input type="hidden" name="" value="{{ $lang == "en" ? "The activity" : "L'activité" }}" id="text_parse">
                        <input type="hidden" name="" value="{{ $id }}" id="test_id">
                        <div class="row">
                          <div class="col-sm-2 text-right">
                            Date :
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="day">
                              <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.heure") :
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="hours">
                              <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.repondants") :
                          </div>
                          <div class="col-sm-2">
                            {% dates[day].nbRep[hours] %}
                          </div>
                          <div ng-if="day!=undefined">
                            @foreach($page2->questions as $q=>$question)
                              @if($q==0)
                                <div class="col-sm-12 text-center">
                                  <h3>{{ $question->{'titre_'.$lang} }}</h3>
                                </div>
                                        
                                <div class="col-xs-12 row">
                                  @foreach($question->reponses as $key=>$reponse)
                                    <div class="col-xs-6 col-md-6">
                                    
                                      @if($loop->iteration  % 2 == 0)
                                        <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
                                          <br>
                                          <span>{% dayH["{{$key}}"]["{{$question->id}}"] %} %</span>
                                       
                                        </p>
                                        <div class="progress reverse" style="height: 20px;">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#2196f3' : ($key == 3 ? '#2196f3' : ($key == 5 ? '#ffc107' : ($key == 7 ? '#ffc107' : '' )))))}};width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
                                            <span class="sr-only">30% Complete</span>
                                          </div>
                                        </div>
                                        <br>
                                      @else
                                        <p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
                                        <br>
                                        <span>{% dayH["{{$key}}"]["{{$question->id}}"] %} %</span>
                                        </p>
                                        <div class="progress reverse" style="height: 20px;">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#2196f3" : (($key == 2)? "#2196f3" : (($key == 4)? "#ffc107" : (($key == 6)? "#ffc107" : ""))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%;float: right;">
                                          <span class="sr-only">30% Complete</span>
                                          </div>
                                        </div>
                                      @endif                                    
                                    </div>
                                  @endforeach
                                </div>
                              @endif
                              @if($q==1)
                                <div class="col-sm-12 text-center">
                                  <h3>{{ $question->{'titre_'.$lang} }}</h3>
                                </div>        
                                <div class="col-xs-12 row">
                                  @foreach($question->reponses as $key=>$reponse)
                                    <div class="col-xs-6 col-md-6">
                                      
                                      @if($loop->iteration  % 2 == 0)
                                        <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
                                          <br>
                                          <span>{% dayH["{{$key}}"]["{{$question->id}}"] %} %</span>
                                        </p>
                                        <div class="progress" style="height: 20px;">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#e91e63' : ($key == 3 ? '#e91e63' : ($key == 5 ? '#4caf50' : ($key == 7 ? '#4caf50' : '' )))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
                                            <span class="sr-only">30% Complete</span>
                                          </div>
                                        </div>
                                        <br>
                                      @else
                                        <p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
                                          <br>
                                          <!-- <span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span> -->
                                          <span>{% dayH["{{$key}}"]["{{$question->id}}"] %} %</span>
                                        </p>
                                        <div class="progress reverse" style="height: 20px;">
                                          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#e91e63" : (($key == 2)? "#e91e63" : (($key == 4)? "#4caf50" : (($key == 6)? "#4caf50" : ""))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%;float: right;">
                                          <span class="sr-only">30% Complete</span>
                                          </div>
                                        </div>
                                      @endif             
                                    </div>
                                  @endforeach  
                                </div>
                              @endif                          
                              <hr>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    @endif
                  </div>

                  <div class="tab-pane {{ $climage->type->name == 'climage-interlocuteur' ? 'active show' : '' }}" id="resultat2">
                    <div class="tab-pane active show" id="resultat2">
                      @if($climage->type->name == "climage-negociation")
                        
                      @elseif($climage->type->name == "climage-interlocuteur")
                        <div ng-if="dates[day].climage">
                          <div  class="row">
                            <div class="col-sm-2 text-right">
                              Date :
                            </div>
                            <div class="col-sm-2">
                              <select class="form-control" ng-change="dateChange1()" ng-model="day">
                                <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
                              </select>
                            </div>
                            <div class="col-sm-2 text-right">
                              @lang("message.heure") :
                            </div>
                            <div class="col-sm-2">
                              <select class="form-control" ng-change="dateChange1()" ng-model="hours">
                                <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
                              </select>
                            </div>
                            <div class="col-sm-2 text-right">
                              @lang("message.repondants") :
                            </div>
                            <div class="col-sm-2">
                              {% dates[day].nbRep[hours] %}
                            </div>
                            <div class="col-sm-9 container" style="position: relative;height:300px;    margin-left: 100px;" id="desktop">
                              <br/>
                              <div class="contains">
                                <img src="{{ url('/img/climage2.png') }}" height="295px">
                              </div>
                              <div class="contains" style="margin-top:60px">
                                <canvas class="chart chart-radar" chart-data="data5" width="350" height="150" chart-options="options1" chart-labels="labels5" style="margin-left: -5px;"> </canvas>
                              </div>
                              <div class="contains" style="margin-top:138px;margin-left:185px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total1"] %}%</div>
                              </div>
                              <div class="contains" style="margin-left:170px;">
                                <div style="margin-top:60px;font-size:11px;">{% dates[day].climage[hours]["serieux"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSerieux"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:125px;">
                                <div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["conforme"] %}%</div>
                                  <div style="font-size:11px;">({% dates[day].climage[hours]["signeConforme"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:235px;">
                                  <div style="margin-top:125px; font-size:11px;">{% dates[day].climage[hours]["transgressif"] %}%</div>
                                  <div style="font-size:11px;">({% dates[day].climage[hours]["signeTransgressif"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:200px;">
                                  <div style="margin-top:190px; font-size:11px;">{% dates[day].climage[hours]["enjoue"] %}%</div>
                                  <div style="font-size:11px;">({% dates[day].climage[hours]["signeEnjoue"] %})</div>
                              </div>
                              <div class="contains" style="margin-top:105px;margin-left:5px;">
                                <canvas class="chart chart-radar" chart-data="data6" chart-options="options1" chart-labels="labels5" width="350" height="150" style="    margin-top: -43px;margin-left: 270px;"> </canvas>
                              </div>
                              <div class="contains" style="margin-left:465px">
                                <div style="margin-top:140px;font-size:11px;">{% dates[day].climage[hours]["total2"] %}%</div>
                              </div>
                              <div class="contains" style="margin-left:450px">
                                <div style="margin-top:60px;font-size:11px;">{% dates[day].climage[hours]["soi"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSoi"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:410px">
                                <div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["sympathie"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSymphatie"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:480px">
                                <div style="margin-top:200px;font-size:11px;">{% dates[day].climage[hours]["autrui"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeAutrui"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:520px">
                                <div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["maitrise"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeMaitrise"] %})</div>
                              </div> 
                            </div>
                            <div id="phone" style="position:relative; height:700px;">
                              <div style=" position:absolute">
                                <img src="{{ url('/img/interg2.png') }}" height="350px">
                              </div>
                              <div class="contains" style="margin-top:82px; margin-left:-320px;">
                                <canvas class="chart chart-radar" chart-data="data5" width="350" height="150" chart-options="options1" chart-labels="labels5" style="margin-left: 340px;"> </canvas>
                              </div>
                              <div class="contains" style="margin-top:158px;margin-left:209px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total1"] %}%</div>
                              </div>
                              <div class="contains" style="margin-left:177px;">
                                <div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["serieux"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSerieux"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:133px;">
                                <div style="margin-top:160px;">{% dates[day].climage[hours]["conforme"] %}%</div>
                                  <div style="font-size:11px;">({% dates[day].climage[hours]["signeConforme"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:140px;">
                                <div style="margin-top:220px;margin-left:90px; font-size:11px;">{% dates[day].climage[hours]["transgressif"] %}%</div>
                                <div style="font-size:11px;margin-left:90px;">({% dates[day].climage[hours]["signeTransgressif"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:290px;">
                                <div style="margin-top:125px; font-size:11px;">{% dates[day].climage[hours]["enjoue"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeEnjoue"] %})</div>
                              </div>
                              <div style="margin-top:350px; position:absolute">
                                <img src="{{ url('/img/interd2.png') }}" height="350px">
                              </div>
                              
                              <div class="contains" style="margin-top:475px;">
                                <canvas class="chart chart-radar" chart-data="data6" chart-options="options1" chart-labels="labels5" width="350" height="150" style="    margin-top: -43px;margin-left: 15px;"> </canvas>
                              </div>
                              <div class="contains" style="margin-top:505px;margin-left:205px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total2"] %}%</div>
                              </div>
                              <div class="contains" style="margin-left:180px;margin-top:355px">
                                <div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["soi"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSoi"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:135px;margin-top:390px">
                                <div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["sympathie"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeSymphatie"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:235px;margin-top:380px;">
                                <div style="margin-top:190px;font-size:11px;">{% dates[day].climage[hours]["autrui"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeAutrui"] %})</div>
                              </div>
                              <div class="contains" style="margin-left:275px;margin-top:360px">
                                <div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["maitrise"] %}%</div>
                                <div style="font-size:11px;">({% dates[day].climage[hours]["signeMaitrise"] %})</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @else
                        <div class="row">
                          <div class="col-sm-2 text-right">
                            Date :
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="day">
                              <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.heure") :
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="hours">
                              <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.repondants") :
                          </div>
                          <div class="col-sm-2">
                            {% dates[day].nbRep[hours] %}
                          </div>

                          <!-- CLIMAGE RESULTAT 2 WEB -->
                          <div class="col-sm-9 container" ng-if="dates[day].graphe" style="position: relative;height:550px; margin-left:120px;" id="desktop">
                            <br/> 
                            <div style="position:absolute;font-weight:bold;">
                              <img style="margin-top: {{ $lang == 'fr' ? '' : '19px' }};margin-left:{{ $lang == 'fr' ? '' : '459px' }};" src="{{ $lang == "fr" ? url('/img/pt3.png') : url('/img/Climage_Graph_2.png') }}" height="450px" id="img_g">
                            </div>
                            <div class="contains text" id="serieux">
                              {% dates[day].graphe[hours]["serieux"] %}%
                            </div>
                            <div class="contains" id="serieuxSVG">
                              <svg height="60" width="60">
                                <circle id="bleu2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['serieux'] == 0 ? 1 : (dates[day].graphe[hours]['serieux'] > 0 ? ((dates[day].graphe[hours]['serieux'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>

                            <div class="contains text" id="conforme">
                              {% dates[day].graphe[hours]["conforme"] %}%
                            </div>
                            <div class="contains" id="conformeSVG">
                              <svg height="60" width="60">
                                <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['conforme'] == 0 ? 1 : (dates[day].graphe[hours]['conforme'] > 0 ? ((dates[day].graphe[hours]['conforme'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>

                            <div class="contains" id="enjoueSVG">
                              <svg height="60" width="60">
                                <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['enjoue'] == 0 ? 1 : (dates[day].graphe[hours]['enjoue'] > 0 ? ((dates[day].graphe[hours]['enjoue'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains text" id="enjoue">
                              {% dates[day].graphe[hours]['enjoue'] %}%
                            </div>
                            
                            <div class="contains" id="transgressifSVG">
                              <svg height="60" width="60">
                                <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['transgressif'] == 0 ? 1 : (dates[day].graphe[hours]['transgressif'] > 0 ? ((dates[day].graphe[hours]['transgressif'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains text" id="transgressif">
                              {% dates[day].graphe[hours]['transgressif'] %}%
                            </div>

                            <div class="contains text" id="soi">
                              {% dates[day].graphe[hours]["soi"] %}%
                            </div>
                            <div class="contains" id="soiSVG" style="margin-left: {{ $lang == 'fr' ? '419px' : '414px' }};">
                              <svg height="60" width="60">
                                <circle id="vert2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['soi'] == 0 ? 1 : (dates[day].graphe[hours]['soi'] > 0 ? ((dates[day].graphe[hours]['soi'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
      
                            <div class="contains text" id="sympathie">
                              {% dates[day].graphe[hours]["sympathie"] %}%
                            </div>
                            <div class="contains" id="sympathieSVG">
                              <svg height="60" width="60">
                                <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['sympathie'] == 0 ? 1 : (dates[day].graphe[hours]['sympathie'] > 0 ? ((dates[day].graphe[hours]['sympathie'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div> 

                            <div class="contains" id="autruiSVG" style="margin-left:{{ $lang == 'fr' ? '419px' : '414px' }};">
                              <svg height="60" width="60">
                                <circle id="rose2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['autrui'] == 0 ? 1 : (dates[day].graphe[hours]['autrui'] > 0 ? ((dates[day].graphe[hours]['autrui'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains text" id="autrui">
                              {% dates[day].graphe[hours]['autrui'] %}%
                            </div>                    

                            <div class="contains text" id="maitrise">
                              {% dates[day].graphe[hours]["maitrise"] %}%
                            </div>
                            <div class="contains" id="maitriseSVG">
                              <svg height="60" width="60">
                                <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['maitrise'] == 0 ? 1 : (dates[day].graphe[hours]['maitrise'] > 0 ? ((dates[day].graphe[hours]['maitrise'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>  
                              
                          </div>


                          <!-- CLIMAGE RESULTAT 2 MOBILE -->
                          <div class="row" id="phone" style="margin-left: 10px;width:330px; height:860px;">
                            <div class="col-sm-12 col-md-6 text" style="position:relative;">
                              <div style="position:absolute;">
                                <img src="{{ $lang == "fr" ? url('/img/plg2.png') : url('/img/plg2_en.png') }}" width="75%">  
                              </div>

                              <div class="contains font" id="serieux2" style="margin-left:{{ $lang == 'fr' ? '113px' : '105px' }};margin-top:{{ $lang == 'fr' ? '110px' : '95px' }}">
                                {% dates[day].graphe[hours]["serieux"] %}%
                              </div>
                              <div class="contains" id="serieuxSVG2" style="margin-left:{{ $lang == 'fr' ? '97px' : '87px' }};margin-top:{{ $lang == 'fr' ? '119px' : '102px' }}">
                                <svg height="60" width="60">
                                  <circle id="bleu2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['serieux'] == 0 ? 1 : (dates[day].graphe[hours]['serieux'] > 0 ? ((dates[day].graphe[hours]['serieux'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>

                              <div class="contains text" id="conforme2" style="margin-left:{{ $lang == 'fr' ? '45px' : '25px' }};margin-top:{{ $lang == 'fr' ? '147px' : '136px' }}">
                                {% dates[day].graphe[hours]["conforme"] %}%
                              </div>
                              <div class="contains" id="conformeSVG2" style="margin-left:{{ $lang == 'fr' ? '60px' : '51px' }};margin-top:{{ $lang == 'fr' ? '147px' : '135px' }}">
                                <svg height="60" width="60">
                                  <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['conforme'] == 0 ? 1 : (dates[day].graphe[hours]['conforme'] > 0 ? ((dates[day].graphe[hours]['conforme'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>

                              <div class="contains" id="enjoueSVG2" style="margin-left:{{ $lang == 'fr' ? '96px' : '86px' }};">
                                <svg height="60" width="60">
                                  <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['enjoue'] == 0 ? 1 : (dates[day].graphe[hours]['enjoue'] > 0 ? ((dates[day].graphe[hours]['enjoue'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>
                              <div class="contains text" id="enjoue2" style="margin-left:{{ $lang == 'fr' ? '113px' : '105px' }};margin-top:{{ $lang == 'fr' ? '228px' : '226px' }}">
                                {% dates[day].graphe[hours]['enjoue'] %}%
                              </div>

                              <div class="contains" id="transgressifSVG2" style="margin-left:{{ $lang == 'fr' ? '135px' : '121px' }};margin-top:{{ $lang == 'fr' ? '147px' : '135px' }}">
                                <svg height="60" width="60">
                                  <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['transgressif'] == 0 ? 1 : (dates[day].graphe[hours]['transgressif'] > 0 ? ((dates[day].graphe[hours]['transgressif'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>
                              <div class="contains text" id="transgressif2" style="margin-top:{{ $lang == 'fr' ? '147px' : '136px' }}">
                                {% dates[day].graphe[hours]['transgressif'] %}%
                              </div>

                            </div>
                            <div class="col-sm-12 col-md-6" style="position:relative;margin-top:347px;">
                              <!-- <h4 class="text-center"><b>{{ __('message.relation') }}</b></h4> -->
                              <div style="position:absolute;">
                                <img style="margin-top: {{ $lang == 'fr' ? '' : '-31px' }};" src="{{ $lang == "fr" ? url('/img/pld2.png') : url('/img/pld_en.png') }}" width="75%">  
                              </div>
                              
                              <div class="contains text" id="soi2" style="margin-left: {{ $lang == 'fr' ? '100px' : '109px' }};margin-top:{{ $lang == 'fr' ? '105px' : '59px' }};">
                                {% dates[day].graphe[hours]["soi"] %}%
                              </div>
                              <div class="contains" id="soiSVG2" style="margin-left: {{ $lang == 'fr' ? '78px' : '87px' }};margin-top:{{ $lang == 'fr' ? '112px' : '66px' }};">
                                <svg height="60" width="60">
                                  <circle id="vert2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['soi'] == 0 ? 1 : (dates[day].graphe[hours]['soi'] > 0 ? ((dates[day].graphe[hours]['soi'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>

                              <div class="contains text" id="symphatie2" style="margin-left: {{ $lang == 'fr' ? '21px' : '27px' }};margin-top:{{ $lang == 'fr' ? '151px' : '98px' }};">
                                {% dates[day].graphe[hours]["sympathie"] %}%
                              </div>
                              <div class="contains" id="symphatieSVG2" style="margin-left: {{ $lang == 'fr' ? '40px' : '51px' }};margin-top:{{ $lang == 'fr' ? '146px' : '99px' }};">
                                <svg height="60" width="60">
                                  <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['sympathie'] == 0 ? 1 : (dates[day].graphe[hours]['sympathie'] > 0 ? ((dates[day].graphe[hours]['sympathie'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>

                              <div class="contains" id="autruiSVG2" style="margin-left: {{ $lang == 'fr' ? '77px' : '87px' }};margin-top:{{ $lang == 'fr' ? '182px' : '135px' }};">
                                <svg height="60" width="60">
                                  <circle id="rose2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['autrui'] == 0 ? 1 : (dates[day].graphe[hours]['autrui'] > 0 ? ((dates[day].graphe[hours]['autrui'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>
                              <div class="contains text" id="autrui2" style="margin-left: {{ $lang == 'fr' ? '100px' : '107px' }};margin-top:{{ $lang == 'fr' ? '232px' : '187px' }};">
                                {% dates[day].graphe[hours]['autrui'] %}%
                              </div>

                              <div class="contains text" id="maitrise2" style="margin-left: {{ $lang == 'fr' ? '172px' : '190px' }};margin-top:{{ $lang == 'fr' ? '150px' : '99px' }};">
                                {% dates[day].graphe[hours]["maitrise"] %}%
                              </div>
                              <div class="contains" id="maitriseSVG2" style="margin-left: {{ $lang == 'fr' ? '110px' : '119px' }};margin-top:{{ $lang == 'fr' ? '146px' : '98px' }};">
                                <svg height="60" width="60">
                                  <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['maitrise'] == 0 ? 1 : (dates[day].graphe[hours]['maitrise'] > 0 ? ((dates[day].graphe[hours]['maitrise'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
                                  Sorry, your browser does not support inline SVG.  
                                </svg>
                              </div>      
                            </div>
                          </div>
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="tab-pane" id="resultat3">
                    <div class="tab-pane active show" id="resultat2">
                      <div class="row">
                        @if($climage->type->name == "climage-negociation")

                        @else

                          <!--  CLIMAGE RESULTAT 3 WEB -->
                          <div class="col-sm-2 text-right">
                            Date :
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="day">
                              <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.heure"):
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" ng-change="dateChange1()" ng-model="hours">
                              <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                            @lang("message.repondants"):
                          </div>
                          <div class="col-sm-2">
                            {% dates[day].nbRep[hours] %}
                          </div>
                          <div class="col-sm-9 container text" ng-if="dates[day].climage" style="position: relative;height:400px;" id="desktop">
                            <br/>
                            <div style="position:absolute;font-weight:bold;margin-left:205px;">
                              <img src="{{ $lang == "fr" ? url('/img/ptresult4.png') : url('/img/climage_3.png') }}" height="350px" id="img_g">
                            </div>
                            <div class="contains" style="margin-left:145px; color:#7f7f7f;">
                              <h4 class="text-right" style=" font-weight:700;margin-top:0px">@lang("message.climat_dom")</h4>
                            </div>
                            <div class="contains" id="vigilance">
                              {% dates[day].climage[hours]["vigilance"] %}%
                            </div>
                            <div class="contains" id="vigilanceSVG">
                              <svg height="60" width="60">

                                <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['vigilance'] == 0 ? 1 : (dates[day].climage[hours]['vigilance'] > 0 ? ((dates[day].climage[hours]['vigilance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="depassement">
                              {% dates[day].climage[hours]["depassement"] %}%
                            </div>
                            <div class="contains" id="depassementSVG">
                              <svg height="60" width="60">

                                <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['depassement'] == 0 ? 1 : (dates[day].climage[hours]['depassement'] > 0 ? ((dates[day].climage[hours]['depassement']*25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="implicationSVG">
                              <svg height="60" width="60">

                                <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['creative'] == 0 ? 1 : (dates[day].climage[hours]['creative'] > 0 ? ((dates[day].climage[hours]['creative'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="implication">
                              {% dates[day].climage[hours]["creative"] %}%
                              
                            </div>
                            <div class="contains" id="creativeSVG">
                              <svg height="60" width="60">

                                <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['implication'] == 0 ? 1 : (dates[day].climage[hours]['implication'] > 0 ? ((dates[day].climage[hours]['implication'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="creative">
                              {% dates[day].climage[hours]["implication"] %}%
                              
                              
                            </div>
                            <div class="contains" id="reconnaissance">
                              {% dates[day].climage[hours]["reconnaissance"] %}%
                            </div>
                            <div class="contains" id="reconnaissanceSVG">
                              <svg height="60" width="60">

                                <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['reconnaissance'] == 0 ? 1 : (dates[day].climage[hours]['reconnaissance'] > 0 ? ((dates[day].climage[hours]['reconnaissance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="competition">
                              {% dates[day].climage[hours]["competition"] %}%
                            </div>
                            <div class="contains" id="competitionSVG">
                              <svg height="60" width="60">

                                <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['competition'] == 0 ? 1 : (dates[day].climage[hours]['competition'] > 0 ? ((dates[day].climage[hours]['competition'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="convivialiteSVG">
                              <svg height="60" width="60">

                                <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['convivialite'] == 0 ? 1 : (dates[day].climage[hours]['convivialite'] > 0 ? ((dates[day].climage[hours]['convivialite'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="convivialite">
                              {% dates[day].climage[hours]["convivialite"] %}%
                            </div>
                            <div class="contains" id="collaborationSVG">
                              <svg height="60" width="60">

                                <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['collaboration'] == 0 ? 1 : (dates[day].climage[hours]['collaboration'] > 0 ? ((dates[day].climage[hours]['collaboration'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="collaboration">
                              {% dates[day].climage[hours]["collaboration"] %}%
                            </div>
                          </div>

                          <!-- CLIMAGE SIMPLE RESULTAT 3 MOBILE -->

                          <div id="phone" style="position:relative;height:720px;">
                            <div class="contains" style="margin-top:20px;">
                              <p><img src="{{ $lang == "fr" ? url('/img/ptresult3g.png') : url('/img/climage3g.png') }}" height="350" style="margin-left:{{ $lang == "fr" ? '0px' : '0px' }};margin-top:{{ $lang == "fr" ? '15px' : '15px' }};"></p>
                            </div>
                            <div class="contains font" id="vigilance2">
                              {% dates[day].climage[hours]["vigilance"] %}%
                            </div>
                            <div class="contains" id="vigilanceSVG2" style="margin-left:{{ $lang == "fr" ? '80px' : '73px' }};margin-top:{{ $lang == "fr" ? '158px' : '154px' }};">
                              <svg height="60" width="60">
                                <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['vigilance'] == 0 ? 1 : (dates[day].climage[hours]['vigilance'] > 0 ? ((dates[day].climage[hours]['vigilance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="depassement2">
                              {% dates[day].climage[hours]["depassement"] %}%
                            </div>
                            <div class="contains" id="depassementSVG2" style="margin-left:{{ $lang == "fr" ? '130px' : '128px' }};margin-top:{{ $lang == "fr" ? '158px' : '155px' }};">
                              <svg height="60" width="60">
                                <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['depassement'] == 0 ? 1 : (dates[day].climage[hours]['depassement'] > 0 ? ((dates[day].climage[hours]['depassement'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="implicationSVG2" style="margin-left:{{ $lang == "fr" ? '76px' : '73px' }};">
                              <svg height="60" width="60">
                                <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['creative'] == 0 ? 1 : (dates[day].climage[hours]['creative'] > 0 ? ((dates[day].climage[hours]['creative'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="implication2">
                              {% dates[day].climage[hours]["creative"] %}%
                            </div>
                            <div class="contains" id="creativeSVG2">
                              <svg height="60" width="60">
                                <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['implication'] == 0 ? 1 : (dates[day].climage[hours]['implication'] > 0 ? ((dates[day].climage[hours]['implication'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="creative2">
                              {% dates[day].climage[hours]["implication"] %}%
                              
                            </div>
                            <div class="contains" style="margin-top:320px;">
                              <p><img src="{{ $lang == "fr" ? url('/img/ptresult3d.png') : url('/img/climage3d.png') }}" height="350" style="margin-left:{{ $lang == "fr" ? '0px' : '0px' }};margin-top:{{ $lang == "fr" ? '62px' : '62px' }};"></p>
                            </div>
                            <div class="contains">
                              <h4 style="margin-left:50px; color:#7f7f7f; font-weight:700;">@lang("message.climat_dom")</h4>
                            </div>
                            <div class="contains" id="reconnaissance2">
                              {% dates[day].climage[hours]["reconnaissance"] %}%
                            </div>
                            <div class="contains" id="reconnaissanceSVG2" style="margin-left:{{ $lang == "fr" ? '79px' : '75px' }};">
                              <svg height="60" width="60">
                                <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['reconnaissance'] == 0 ? 1 : (dates[day].climage[hours]['reconnaissance'] > 0 ? ((dates[day].climage[hours]['reconnaissance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="competition2">
                              {% dates[day].climage[hours]["competition"] %}%
                            </div>
                            <div class="contains" id="competitionSVG2" style="margin-left:{{ $lang == "fr" ? '132px' : '130px' }};">
                              <svg height="60" width="60">
                                <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['competition'] == 0 ? 1 : (dates[day].climage[hours]['competition'] > 0 ? ((dates[day].climage[hours]['competition'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains" id="convivialiteSVG2" style="margin-left:{{ $lang == "fr" ? '82px' : '77px' }};">
                              <svg height="60" width="60">
                                <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['convivialite'] == 0 ? 1 : (dates[day].climage[hours]['convivialite'] > 0 ? ((dates[day].climage[hours]['convivialite'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="convivialite2">
                              {% dates[day].climage[hours]["convivialite"] %}%
                            </div>
                            <div class="contains" id="collaborationSVG2" style="margin-left:{{ $lang == "fr" ? '130px' : '127px' }}; margin-top:{{ $lang == "fr" ? '552px' : '555px' }};">
                              <svg height="60" width="60">
                                <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['collaboration'] == 0 ? 1 : (dates[day].climage[hours]['collaboration'] > 0 ? ((dates[day].climage[hours]['collaboration'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
                                Sorry, your browser does not support inline SVG.  
                              </svg>
                            </div>
                            <div class="contains font" id="collaboration2">
                              {% dates[day].climage[hours]["collaboration"] %}%
                            </div>
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="resultat4">
                    <div class="col-sm-2 text-right">
                      Date :
                    </div>
                    <div class="col-sm-2">
                      <select class="form-control" ng-change="dateChange1()" ng-model="day">
                        <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
                      </select>
                    </div>
                    <div class="col-sm-2 text-right">
                      @lang("message.heure"):
                    </div>
                    <div class="col-sm-2">
                      <select class="form-control" ng-change="dateChange1()" ng-model="hours">
                        <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
                      </select>
                    </div>
                    <div class="col-sm-2 text-right">
                      @lang("message.repondants"):
                    </div>
                    <div class="col-sm-2">
                      {% dates[day].nbRep[hours] %}
                    </div>
                    <div class="col-sm-9 container" style="position: relative;height:300px;" id="desktop">
                      <br/>
                      <div class="contains">
                        <canvas id="doughnut" class="chart chart-doughnut"
                          chart-data="data5" chart-colors="color5" chart-options="options5" chart-labels="labels5" style="width:350px; height:150px;">
                        </canvas> 
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="trombinoscope">
                    <div class="row">
                      @foreach($climage->testRepondants as $rep)

                        <div class="col-sm-12 col-md-4">
                          <div class="box">
                            @if(Auth::user()->id == $rep->user->id || Auth::user()->role == "consultant" || Auth::user()->role == "admin")
                                                <div class="card-body text-right" style="padding:0;cursor:context-menu;" data-toggle="modal" data-target="#delete-climageUser" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)">
                                                  <i class="fa fa-trash"></i>
                                                </div>  
                                              @endif
                            <div class="box-header with-border text-center">
                                        <div class="col-sm-12 text-center">
                                                @if($rep->user->avatar_url)
                                                  <img style="height: 70px; width: 70px"  id="imageSrc" src="{{ $rep->user->avatar_url }}" alt="Avatar" class="rounded-circle">
                                                @else
                                                  <img style="height: 70px; width: 70px"  class="img-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
                                                @endif 
                                        </div>
                              <p class="text-secondary">{{ $rep->user->username }}</p>
                              <p class="text-secondary">{{ $rep->user->email }} </p>
                            </div>
                            <div class="box-body text-center">
                              <button class="btn btn-warning" data-toggle="modal" data-target="#profil" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)" style="border-radius: 30px;">Profil</button>
                            </div>
                          </div>
                        </div>
                      @endforeach
                        
                      
                    </div>
              </div>  

                </div>
                    
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body 
        <div class="box-footer">
            
        </div>
        <!-- box-footer
      </div>
      <!-- /.box -->
    </div>
        
  </div>

  <div class="modal fade" id="delete-climageUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h4>Suppression</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">         
          @lang("message.infoClimage")
            </div>
          <div class="modal-footer">
              <button type="button" ng-click="deleteInfo()" class="btn btn-danger" data-dismiss="modal">@lang('message.supprimer')</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
              <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
          </div>
        </div>
      </div>
    </div>

  <div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h5 class="modal-title" id="exampleModalLabel">
                                        <div class="col-sm-12 text-center">
                                                <img ng-if="user.avatar_url" style="height: 70px; width: 70px"  id="imageSrc" src="{% user.avatar_url %}" alt="Avatar" class="rounded-circle">
                                             
                                                <img ng-if="!user.avatar_url" style="height: 70px; width: 70px"  class="img-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
                                        </div>
                                        <div class="col-sm-12 text-center">
                                          {% user.username %} - {% user.email %}
                                        </div>
              
          </h5>
        </div>
        <div class="modal-body">
          <div class="card" ng-repeat="reponse in reponses">
            <div class="card-body row">
              <div class="col-sm-8">
                <p><b> <i class="fa fa-arrow-right"></i> {% profil[reponse.question_id]['{{ $lang }}'] %}</b></p>
                <p>{% reponse.reponse_text %}</p>
              </div>
              
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button></div>
        </div>
    </div>
  </div>
      

</div>

<script type="text/javascript">
  //alert(window.screen.width);
  function affiche(text){
    console.log(text);
    return  text;
  }

  $(function() {

      window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });

      //@if($data)

        //le petit carre milieu
        //var c = document.getElementById("myCanvas");
        //var ctx = c.getContext("2d");
        //ctx.fillStyle = "rgba(179,181,198,1)";
        //ctx.fillRect(23, 18, 164, 23);
    //@endif
    });

</script>

@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
  <style type="text/css">
    #radar1,#radar2{
      position: absolute;
      top: 0;
      /*  width: 270px;
      height: 140px; */
      /*margin-top: 11.5%;*/   
    }
        
  </style>
@stop

@section('js')
  <script> console.log('Hi!'); </script>
  <script type="text/javascript">
    var resultatApp = angular.module('resultatApp', ['chart.js'], function($interpolateProvider) {
      $interpolateProvider.startSymbol('{%');
      $interpolateProvider.endSymbol('%}');
    });

    resultatApp.controller('resultatController', function($scope,$http) {

      $scope.user = {};
      $scope.profil = [];
      $scope.dates = {!! json_encode($dates) !!};

        if($scope.dates){
          $scope.stringD = ($scope.dates.length-1).toString();
          $scope.stringH =$scope.dates[$scope.stringD] != undefined ? ($scope.dates[$scope.stringD].heures.length-1).toString() : "";
          $scope.day =  $scope.dates[0] ? $scope.stringD : undefined;
          $scope.hours = $scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined;
          console.log($scope.dates);
        }

      $scope.dateChange1 = function(){
        //$scope.hours = $scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined;     

        if(!$scope.hours){
          $scope.hours = "0";  
        }
        //$scope.hours == null ? ($scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined) : undefined;
        if($scope.dates[$scope.day]){
            $scope.dayH = $scope.dates[$scope.day].vote[$scope.hours]
            console.log($scope.dayH);

            $scope.data1 = [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];

            $scope.data2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];

            $scope.labels1 =[$scope.dates[$scope.day].graphe[$scope.hours]["serieux"]+"%","                   "+$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]+"%                    "];

            $scope.labels2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"]+"%","                   "+$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["autrui"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]+"%                    "];


            $scope.data3 = [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];

            $scope.data4 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];

            $scope.labels3 =[$scope.dates[$scope.day].graphe[$scope.hours]["serieux"]+"%","                       "+$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]+"%                        "];

            $scope.labels4 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"]+"%","                      "+$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["autrui"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]+"%                        "];

            $scope.data5 = [$scope.dates[$scope.day].climage[$scope.hours]["serieux"],$scope.dates[$scope.day].climage[$scope.hours]["transgressif"],$scope.dates[$scope.day].climage[$scope.hours]["enjoue"],$scope.dates[$scope.day].climage[$scope.hours]["conforme"]];

            $scope.data6 = [$scope.dates[$scope.day].climage[$scope.hours]["soi"],$scope.dates[$scope.day].climage[$scope.hours]["maitrise"],$scope.dates[$scope.day].climage[$scope.hours]["autrui"],$scope.dates[$scope.day].climage[$scope.hours]["sympathie"]];

            $scope.labels5 =['','','',''];
        }
            

      }

      $scope.dateChange1();

      // $scope.grapheData = function(){
      //   return [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];
      // }

      // $scope.grapheData2 = function(){
      //   return [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];
      // }

      //$scope.labels5 = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
      //$scope.data5 = [300, 500, 100];
      //$scope.options5 = {cutoutPercentage: 75};
      //$scope.Color5= ['red', 'green', 'white'];

      $scope.options3 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }
      $scope.options1 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          } 

          $scope.options2 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }   

          $scope.options4 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          } 

      let temp = "<?php echo htmlspecialchars(json_encode($questionProfil)) ?>";
      $scope.profil = JSON.parse(temp.replace(/&quot;/g,'"'));

      $scope.reponse = [];

      $scope.page = function(page){
        window.location = page;
      }
      $scope.showProfil = function(user, reponses){
        $scope.user = user;
        console.log(user.username);
          
        console.log($scope.profil);
        $scope.reponses = reponses.filter(function(item){
          return item.reponse_text != null
        });
        console.log($scope.reponses);

      }
      
    $scope.deleteInfo = function(){

            $http({
              method: 'POST',
              data:{"idClimage": '{{ $climage->id }}', "user":$scope.user},
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/climage/deleteInfoClimage"
            }).then(function successCallback(response) {
              if(response.data.status == 1){
                location.reload();
              }
            }, function errorCallback(response) {

              //console.log(response);
            });

          }
    });


  </script>

@stop
{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>
        <span class="glyphicon glyphicon-stats"></span>
        @lang('message.climage')
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.climage')</a></li>
        <li class="active">@lang('message.liste')</li>
      </ol>
    
@stop
  
@section('content')
<div ng-app="climageApp" ng-controller="climageController">
    <div>
      @if(session('error_code'))
        <div class="alert alert-danger">
          <div class="alert-icon">
            <i class="material-icons">error_outline</i>
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
          <b>{{ session('error_code') }}</b>
        </div>
      @endif
    </div>
    <div class="row">
      @if(Auth::user()->role !== 'admin_acad')
        <div class="col-xs-6">
          <div class="form-group">
            <div class="input-group" >
              <input type="text" class="form-control timepicker" id="myInput" onkeyup="myFunction()">

              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
            <!-- /.input group -->
          </div>
        </div>
        <div class="col-xs-6 text-right">
            <button class="btn btn-primary" ng-click="addClimage()" data-toggle="modal" data-target="#add-climage"><i class="fa fa-plus"></i>&nbsp;@lang('message.ajouter')</button>
        </div>
      @endif
        <div class="col-xs-12 row">
          @foreach ($climages as $f=>$climage)
                <div class="col-sm-12 col-md-6" id="h3_{{$f}}">
                    <div class="box">
                      <div class="box-header with-border">
                        <h3 class="box-title">{{ $climage->name }}</h3>
                        <p class="text-secondary">@lang('message.langage') : {{ strtoupper($climage->langue) }}</p>
                        <p class="text-secondary">Date : {{ $climage->created_at }}</p>
                        <p class="text-secondary">@lang('message.n_p') : {{ $climage->user }}</p>
                        @if(Auth::user()->role !== 'admin_acad')
                          <div class="box-tools pull-right">
                            <!-- Buttons, labels, and many other things can be placed here! -->
                            <!-- Here is a label for example -->
                            <span class="label label-default" ng-click="editClimage(<?php echo htmlspecialchars(json_encode($climage)) ?>)" data-toggle="modal" data-target="#edit-climage"><i class="fa fa-pencil"></i></span>
                            <span class="label label-default" ng-click="deleteClimage(<?php echo htmlspecialchars(json_encode($climage)) ?>)" data-toggle="modal" data-target="#delete-climage"><i class="fa fa-trash"></i></span>
                          </div>
                        @endif
                        <!-- /.box-tools -->
                      </div>
                      <!-- /.box-header -->

                      <div class="box-body text-right">
                        <!-- <a href="{{ route('climage.resultat', ['id' => $climage->id]) }}" class="text-warning"> <span class="glyphicon glyphicon-stats"></span> @lang('message.resultat')</a> -->
                        
                        <div class="col-sm-6">
                          <a href="{{ route('climage.resultat', ['id' => $climage->id]) }}" class="text-warning"><span class="glyphicon glyphicon-stats"></span> @lang('message.resultat')</a>  
                        </div>

                        <!-- <div class="col-sm-5">
                          <a href="{{ route('climage.tableauClimage2', ['id' => $climage->id]) }}" class="text-warning"><span class="glyphicon glyphicon-stats"></span> @lang('message.tabRep')</a>  --> 

                        <div class="col-sm-6">
                          <a href="{{ route('climage.tableauClimage', ['id' => $climage->id]) }}" class="text-warning"><span class="glyphicon glyphicon-stats"></span> @lang('message.tabRep')</a>
                          <!-- <a href="{{ route('climage.reponse', ['id' => $climage->id]) }}" class="text-warning"> <span class="glyphicon glyphicon-sort-by-attributes-alt"></span> @lang('message.tabRep')</a>   -->

                        </div>
                      </div>
                      <!-- /.box-body -->
                    <!--   <div class="box-footer">
                        The footer of the box
                      </div> -->
                      <!-- box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
          @endforeach


                

          
        </div>
    </div>
    {{ $climages->links() }}


    <div class="modal fade" id="add-climage">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-plus"></i>&nbsp;@lang('message.ajout_climage')</h4>
              </div>

              <form role="form" action="{{ route('climage.store') }}" method="POST">
                @csrf
                @method('POST')
                            <div class="modal-body">
            
                              <div class="form-group">
                                <label for="name">@lang('message.nom')</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="" ng-model="climage.name">
                              </div>

                              <div class="form-group">
                                  <label>Type</label>
                                  <select class="form-control" ng-change="checkValue()" name="type_id" ng-model="climage.type_id">
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}"> {{ $type->description }}</option>
                                    @endforeach
                                    
                                  
                                  </select>
                              </div>

                              <div class="form-group">
                                  <label>@lang('message.langage')</label>
                                  <select class="form-control" name="langue" ng-model="climage.langue">
                                    
                                        <option value="fr"> FR</option>
                                        <option value="en"> EN</option>
                                 
                                    
                                  
                                  </select>
                              </div>

                              <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" ng-change="checkValue()" class="form-control"  name="code" id="code" placeholder="" ng-model="climage.code">
                              </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary" ng-disabled="checkValue()">@lang('message.ajouter')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="delete-climage">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-trash"></i> Suppression</h4>
              </div>

              <form role="form" action="<% '/climage/'+climage.id %>" method="POST">
                @csrf
                @method('DELETE')
                            <div class="modal-body">
            
                                <p>@lang('message.sup_climage') <% climage.name %>?</p>



                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary">@lang('message.supprimer')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="edit-climage">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <i class="fa fa-pencil"></i> @lang('message.modifier') : <% climage.name %></h4>
              </div>

              <form role="form" action="<% '/climage/'+climage.id %>" method="POST">
                @csrf
                @method('PUT')
                            <div class="modal-body">
            
                              <div class="form-group">
                                <label for="name">@lang('message.nom')</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="" ng-model="climage.name">
                              </div>

                              <div class="form-group">
                                  <label>Type</label>
                                  <select class="form-control" name="type_id" ng-model="climage.type_id">
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}"> {{ $type->description }}</option>
                                    @endforeach
                                    
                                  
                                  </select>
                              </div>

                              <div class="form-group">
                                  <label>@lang('message.langue')</label>
                                  <select class="form-control" name="langue" ng-model="climage.langue">
                                    
                                        <option value="fr"> FR</option>
                                        <option value="en"> EN</option>
                                 
                                    
                                  
                                  </select>
                              </div>

                              <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" class="form-control" name="code" id="code" placeholder="" ng-model="climage.code">
                              </div>





                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <button type="submit" class="btn btn-primary" ng-disabled="!climage.name || !climage.type_id || !climage.langue || !climage.code">@lang('message.modifier')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        

</div>
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
    
      
    </style>
@stop

@section('js')
    <script> console.log('Hi!');
      function myFunction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        h3 = document.getElementsByTagName("h3");
        for (i = 0; i < h3.length; i++) {
          a = h3[i];
          txtValue = a.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            document.getElementById("h3_"+i).style.display = "";
              //h3[i].style.display = "";
          } else {
            document.getElementById("h3_"+i).style.display = "none";
              //h3[i].style.display = "none";
          }
        }
      }
    </script>
    <script type="text/javascript">
      var climageApp = angular.module('climageApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });
      climageApp.controller('climageController', function($scope,$http) {
             $scope.climage = {};

             $scope.editClimage = function(climage){
               $scope.climage = climage;
               $scope.climage.type_id = climage.type_id.toString();
             }

             $scope.deleteClimage = function(climage){

               $scope.climage = climage;
             }

             $scope.addClimage = function(){
                 $scope.climage ={}
             }

             $http({
              method: 'GET',
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              dataType: 'json',
              url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/climage/codes"
            }).then(function successCallback(response) {
              $scope.test = response.data;
            }, function errorCallback(response) {

              //console.log(response);
            });

            $scope.headerColor = '#cd0a0a';
              $scope.backColor = {'background-color':$scope.headerColor};
              $scope.setColor = function() {
                $scope.backColor = {'background-color':$scope.headerColor};
            };

            $scope.checkValue = function(){

              let retour = false;

              for(var i= 0; i< $scope.test.length; i++){
                if($scope.test[i].type_id == $scope.climage.type_id && $scope.test[i].code == $scope.climage.code){
                  retour = true;
                  break;
                }
              }
              return retour;

            }


      });


    </script>
@stop
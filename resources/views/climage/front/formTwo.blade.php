
  @extends('diagnostic')
  @section('content')
<div ng-app="climageFrontApp" ng-controller="climageFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">


			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;">{{ __('message.climage') }}</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $climage->name }} </span> </h2>
						<hr>

						<div class="btn-group btn-group-toggle" style="font-family: Roboto,sans serif; display: block;">
							@foreach($pages as $p)
								
								<label ng-click="page('{{ $p->url }}')" class="btn btn-secondary {{ $page->id == $p->id ? 'active' : '' }}">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $p->{'title_'.$lang} }}
								</label>
								
								
							@endforeach
								

							<label ng-click="page('/diagnostics/climage/{{ $climage->id }}/{{ $climage->code }}/merci')" class="btn btn-secondary">
							    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $lang == "en" ? "Results"  : "Résultats"}}
							</label>
							<label ng-click="page('/diagnostics/climage/{{ $climage->id }}/{{ $climage->code }}/trombinoscope')" class="btn btn-secondary">
							    <input type="radio" name="options" id="option1" autocomplete="off" checked> @lang("message.trombinoscope")
							</label>

						  
						</div>
				
							<div class="row">
		

							  <div class="col-md-12  mobile-card">
							      <div class="card">
							          <div class="card-header card-header-text card-header-warning">
							            <div class="card-text">
							              <h3>{{ $page->{'title_'.$lang} }}</h3>
							            </div>
							          </div>
							          <div class="card-body">

							          	@foreach ($errors->all() as $error)
							                <div class="alert alert-danger">
											    <div class="container-fluid">
												  <div class="alert-icon">
												    <i class="material-icons">error_outline</i>
												  </div>
												  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true"><i class="material-icons">clear</i></span>
												  </button>
											      {{ $error }}
											    </div>
											</div>
							            @endforeach
							          	<form role="form" action="{{ route('climage-front-formTwo-post') }}" method="POST">
							                @csrf
							                @method('POST')
							                	<input type="hidden" name="code" value="{{ $climage->code }}">
							                	<input type="hidden" name="id" value="{{ $climage->id }}">

							                	<!-- NEGOCIATION -->

							                	@if($climage->type->name == "climage-negociation")
							            			@foreach ($page->questions as $key => $quest)
										            	<h4 class="card-title">{{ $quest->{'question_'.$lang} }}</h4>

														<p style="color: red"> {% error_question[{{ $quest->id }}] %} </p>
										            	@if($key == 0)
															@if ($quest->type == "text")
											            		<input type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
											            		
											            	@endif

											            	@if($quest->type == "checkbox")
											            		<div class="row">
											            			@foreach ($quest->reponses as $rep => $reponse)
											            			<div class="col-md-6 col-sm-6">
											            				<div class="form-check">

											            					<div class="btn-group-toggle" >
																			  <label style=' background-Color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "#FFF" : "{{(($rep==0 ? '#76a7f0' : ($rep==1 ? '#b3cff7' : ($rep==2 ? '#9bdeff' : ($rep==3 ? '#47c2ff' : ($rep==4? '#ffc000' : ($rep==5 ? '#ffe389': ($rep==6? '#4caf50' : ($rep==7 ? '#ffff00' : '' ))))))))) }}" %}; color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "black" : "black" %}; border:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "1px solid black" : "" %}' class="btn {% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? 'btn-default' : '' %} btn-round btn-reponse">
																			    <input name="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]" type="checkbox" checked autocomplete="off" ng-model="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]"> <i class="large material-icons">chevron_right</i> {{ $reponse->{'reponse_'.$lang} }}
																			    
																			  </label>
																			</div>
																			      
																		</div>
											            			</div>
											            			@endforeach
											            		</div>

														            		
											            		
											            	@endif
										            	@endif

														@if($key == 1)
															@if ($quest->type == "text")
											            		<input type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
											            		
											            	@endif

											            	@if($quest->type == "checkbox")
											            		<div class="row">
											            			@foreach ($quest->reponses as $rep => $reponse)
											            			<div class="col-md-6 col-sm-6">
											            				<div class="form-check">

											            					<div class="btn-group-toggle" >
																			  <label style=' background-Color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "#FFF" : "{{(($rep==0 ? '#f59fca' : ($rep==1 ? '#fcdfed' : ($rep==2 ? '#ffa295' : ($rep==3 ? '#ffe0dc' : ($rep==4? '#a0e886' : ($rep==5 ? '#dff7d7': ($rep==6? '#99d5ad' : ($rep==7 ? '#c6e8d1' : '' ))))))))) }}" %}; color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "black" : "black" %}; border:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "1px solid black" : "" %}' class="btn {% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? 'btn-default' : '' %} btn-round btn-reponse">
																			    <input name="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]" type="checkbox" checked autocomplete="off" ng-model="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]"> <i class="large material-icons">chevron_right</i> {{ $reponse->{'reponse_'.$lang} }}
																			   
																			  </label>
																			</div>
																			      

																		</div>
											            			</div>
											            			@endforeach
											            		</div>        		
											            		
											            	@endif
										            	@endif

										            @endforeach
													
													<!-- AUTRE QUE NEGOCIATION -->

							            		@elseif($climage->type->name=="climage-interlocuteur")
								            		<div class="row">	
								            			@foreach ($page->questions as $key => $quest)
								            				<div class="col-md-3 col-sm-3">
								            					<!-- <h4 class="card-title"></h4> -->

																<p style="color: red"> {% error_question[{{ $quest->id }}] %} </p>
								            					@if ($quest->type == "text")
												            		<input type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
												            		
												            	@endif

												            	@if($quest->type == "range")
												            		<div class="row">
												            			<div class="col-md-8 col-sm-8 col-8">
													            			@foreach ($quest->reponses as $rep => $reponse)
														            			<div class="col-md-12 col-sm-12 col-12" style="text-align:right; color:{{!empty($reponse->reponse_fr) ? '' : 'white'}}">
																					{{!empty($reponse->reponse_fr) ? $reponse->reponse_fr : "-"}}
														            			</div>
														            		@endforeach
													            		</div>
													            		<div text-left class="col-md-1 col-sm-1 col-1" style="background-image:{{ (($key == 0 ? "linear-gradient(0deg, #0e41ce,white, #0e41ce)" : ($key == 1 ? "linear-gradient(0deg, #0b82c6,white, #0b82c6)" : ($key == 2 ? "linear-gradient(0deg, #e2ab00,white, #e2ab00)" : ($key == 3 ? "linear-gradient(0deg, #fefa45,white, #fefa45)" : "")))))}};height:690px;">
													            			<input class="input-range custom-slider" orient="vertical" type="range" step="0.5" value="0" min="-7" max="7" name="reponse_question[{{ $quest->id }}]">
													            		</div>
													            	</div>
													            	<br/>
													            @endif
											            	</div>
											            @endforeach
											        </div>
										        @else
										        	@foreach ($page->questions as $key => $quest)
										            	<h4 class="card-title">{{ $quest->{'question_'.$lang} }}</h4>

														<p style="color: red"> {% error_question[{{ $quest->id }}] %} </p>
										            	@if($key == 0)
															@if ($quest->type == "text")
											            		<input type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
											            		
											            	@endif

											            	@if($quest->type == "checkbox")
											            		<div class="row">
											            			@foreach ($quest->reponses as $rep => $reponse)
											            			<div class="col-md-6 col-sm-6">
											            				<div class="form-check">

											            					<div class="btn-group-toggle" >
																			  <label style=' background-Color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "btn-default" : "{{(($rep==0)?"#2196f3":(($rep==1)?"#2196f3":(($rep==2)?"#2196f3":(($rep==3)?"#2196f3":(($rep==4)?"#ffc107":(($rep==5)?"#ffc107":($rep==6)?"#ffc107":"#ffc107"))))))}}" %}; color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "" : "" %};' class="btn {% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? 'btn-default' : '' %} btn-round btn-reponse">
																			    <input name="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]" type="checkbox" checked autocomplete="off" ng-model="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]"> <i class="large material-icons">chevron_right</i> {{ $reponse->{'reponse_'.$lang} }}
																			   
																			  </label>
																			</div>
																			      
																		</div>
											            			</div>
											            			@endforeach
											            		</div>

														            		
											            		
											            	@endif
										            	@endif

														@if($key == 1)
															@if ($quest->type == "text")
											            		<input type="text" class="form-control" name="reponse_question[{{ $quest->id }}]"  placeholder="{{ $quest->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
											            		
											            	@endif

											            	@if($quest->type == "checkbox")
											            		<div class="row">
											            			@foreach ($quest->reponses as $rep => $reponse)
											            			<div class="col-md-6 col-sm-6">
											            				<div class="form-check">

											            					<div class="btn-group-toggle" >
																			  <label style=' background-Color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "btn-default" : "{{(($rep==0)?"#e91e63":(($rep==1)?"#e91e63":(($rep==2)?"#e91e63":(($rep==3)?"#e91e63":(($rep==4)?"#4caf50":(($rep===5)?"#4caf50":($rep===6)?"#4caf50":"#4caf50"))))))}}" %}; color:{% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? "white" : "" %};' class="btn {% reponse_question[{{ $quest->id }}][{{ $reponse->id }}] ? 'btn-default' : '' %} btn-round btn-reponse">
																			    <input name="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]" type="checkbox" checked autocomplete="off" ng-model="reponse_question[{{ $quest->id }}][{{ $reponse->id }}]"> <i class="large material-icons">chevron_right</i> {{ $reponse->{'reponse_'.$lang} }}
																			   
																			  </label>
																			</div>
																			      

																		</div>
											            			</div>
											            			@endforeach
											            		</div>        		
											            		
											            	@endif
										            	@endif

										            @endforeach		
							            		@endif
									            @if($climage->type->name != "climage-interlocuteur")
									            	<div class="offset-2 col-sm-12">
						                    			<h4 >@lang('message.emotivation')</h4>
						                    		</div>
												@endif
									            
									            <div class="text-center">
									            	<button type="submit" style="font-family: inherit;" class="btn btn-warning btn-round" data-dismiss="modal" ng-click="save($event)">{{ $lang == "en" ? "Save"  : "Enregistrer"}}</button>
									            </div>
									            <br>
							            </form>
							          </div>
							      </div>
							  </div>
							</div>
						    
						
						<!-- <h2 class="inscription">Accueil <i class="material-icons">chevron_right</i> Climage <i class="material-icons">chevron_right</i> </h2> -->
					</div>
		
			</div>
					

	
		</div>
	</div>

	



	

</div>
<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: Roboto, sans serif !important;
	    font-weight: bold;
	}
</style>
<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });
	});
</script>
 <script type="text/javascript">
      var climageFrontApp = angular.module('climageFrontApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      climageFrontApp.controller('climageFrontController', function PhoneListController($scope) {
            $scope.cur = 0;
            $scope.error = false;
            $scope.error_question = [];
            $scope.reponse_question = [];
            $scope.next = function(){
            	$scope.cur +=1;
            }
            $scope.questions = [];
            @foreach($page->questions as $quest)
            	$scope.questions.push(<?php echo htmlspecialchars(json_encode($quest->id)) ?>);
            @endforeach
            console.log($scope.questions);

            $scope.page = function(page){
            	window.location = page;
            }

            $scope.save = function(event){
            	$scope.error = false;
            	window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
            	window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });
            	$scope.error_question = [];
            	for(let i in $scope.questions){
            		@if($climage->type->name != "climage-interlocuteur")
	            		if(!$scope.reponse_question[$scope.questions[i]]){
	            			$scope.error = true;
	            			$scope.error_question[$scope.questions[i]] = "{{ __('message.requis') }}";
	            		}else{
	            			console.log($scope.reponse_question[$scope.questions[i]]);
	            		}
            		@endif
            		@if($climage->type->name == "climage-negociation")
            			if($scope.reponse_question[$scope.questions[i]] && $scope.size($scope.reponse_question[$scope.questions[i]]) > 5){
	            			$scope.error = true;
	            			$scope.error_question[$scope.questions[i]] = "{{ __('message.choisir_mot') }}";
	            		}
            		@else
            			if($scope.reponse_question[$scope.questions[i]] && $scope.size($scope.reponse_question[$scope.questions[i]]) > 3){
	            			$scope.error = true;
	            			$scope.error_question[$scope.questions[i]] = "{{ __('message.choisir_mot') }}";
	            		}
            		@endif	
            	}
            	console.log($scope.error_question);
            	if($scope.error){	
            		event.preventDefault();
            	}

            	// alert($scope.error);
            	
            }

            $scope.size = function(object){
            	let res =0; 
            	for(let i in object){
            		if(object[i]){
            			res = res+1;
            		}
            		
            	}
            	return res;
            }
      });


    </script>
<!--/div-->
@endsection('content')

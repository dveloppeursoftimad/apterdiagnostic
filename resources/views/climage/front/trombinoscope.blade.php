@extends('diagnostic')
@section('content')

<div ng-app="climageFrontApp" ng-controller="climageFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">
			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;">{{ __('message.climage') }}</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $climage->name }} </span>
					</h2>
					<hr>
					<div class="btn-group btn-group-toggle" style="font-family: Roboto,sans serif; display: block;">
						@foreach($pages as $p)
							<label ng-click="page('{{ $p->url }}')" class="btn btn-secondary">
							    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $p->{'title_'.$lang} }}
							</label>			
						@endforeach				
						<label ng-click="page('/diagnostics/climage/{{ $climage->id }}/{{ $climage->code }}/merci')" class="btn btn-secondary">
						    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $lang == "en" ? "Results"  : "Résultats"}}
						</label>
						<label ng-click="page('/diagnostics/climage/{{ $climage->id }}/{{ $climage->code }}/trombinoscope')" class="btn btn-secondary active">
						    <input type="radio" name="options" id="option1" autocomplete="off" checked> @lang("message.trombinoscope")
						</label>
					</div>
					<div class="row">
						<div class="col-md-12 mobile-card">
							<div class="card">
							    <div class="card-header card-header-text card-header-warning">
							        <div class="card-text">
							            <h3>@lang('message.trombinoscope')</h3>
							        </div>
							    </div>
							    <div class="card-body">
									<div class="row">
				                      	@foreach($climage->testRepondants as $rep)
				                      		<div class="col-sm-12 col-md-4">
			                      				<div class="card">
			                      					@if(Auth::user()->id == $rep->user->id || Auth::user()->role == "consultant" || Auth::user()->role == "admin")
				                      					<div class="card-body text-right" style="padding:0;cursor:context-menu;" data-toggle="modal" data-target="#delete-climageUser" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)">
				                      						<i class="material-icons">delete_outline</i>
				                      					</div>	
										            @endif
										            <div class="card-body row">
														<div class="col-sm-12 text-center">
															@if($rep->user->avatar_url)
							                					<img style="height: 70px; width: 70px"  id="imageSrc" src="{{ $rep->user->avatar_url }}" alt="Avatar" class="rounded-circle">
							                				@else
							                					<img style="height: 70px; width: 70px"  class="rounded-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
							                				@endif 
														</div>
												  		<div class="col-sm-12">
												  			{{ $rep->user->firstname }} {{ $rep->user->lastname }}<br>
												  			{{ $rep->user->email }} <br>

												  		</div>
												  		<div class="col-sm-12 text-center">
												  			<button class="btn btn-warning btn-round" data-toggle="modal" data-target="#profil" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)" >PROFILE</button>
												  		</div>
													</div>
												</div>
											</div>
						              	@endforeach
							                      			
						              	<div class="col-sm-12 text-center">
						                	<a href="{{ route('diagnostic') }}" class="btn btn-round btn-warning">
												@lang('message.retour')     	
											</a>
						              	</div>
						            </div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="delete-climageUser">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4>Suppression</h4>
	      			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
		      	<div class="modal-body">			   
		    	@lang("message.infoClimage")
		      	</div>
			    <div class="modal-footer">
			        <button type="button" ng-click="deleteInfo()" class="btn btn-danger" data-dismiss="modal">@lang('message.supprimer')</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
    </div>

	<!-- Modal -->
	<div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
			      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
		        	<h5 class="modal-title" id="exampleModalLabel">
		        		
		        	</h5>
	        
	      		</div>
		      	<div class="modal-body">
      				<div class="row">
      					<div class="col-sm-12 text-center">
				  				<img ng-if="user.avatar_url" style="height: 70px; width: 70px"  id="imageSrc" src="{% user.avatar_url %}" alt="Avatar" class="rounded-circle">
                                     
                                <img ng-if="!user.avatar_url" style="height: 70px; width: 70px"  class="img-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
	        		
				  		</div>
				  		<div class="col-sm-12 text-center">

				  			<b>{% user.firstname %} {% user.lastname %} </b>
				  			<br>
				  			{% user.email %}
				  			<p class="text-secondary">{% user.organisation %}</p>

				  		</div>
      				</div>
      				<div ng-if="reponses.length > 0">
      					<div class="card" ng-repeat="reponse in reponses">
						  	<div class="card-body row">	
						    	<div class="col-sm-12">
						    		<p><b> <i class="large material-icons">chevron_right</i> {% profil[reponse.question_id]['{{ $lang }}'] %}</b></p>
						    		<p>{% reponse.reponse_text %}</p>
						    	</div>
						    
						  	</div>
						</div>
      				</div>
      				<div ng-if="reponses.length < 1">
      					<div class="card">
						  	<div class="card-body row">
						    	<div class="col-sm-12">
						    		<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My personnal Motto" : "Mon slogan personnel" }}' %}</b></p>
						    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>								    		
						    	</div>
						  	</div>
						</div>
						<div class="card">
							<div class="card-body row">
								<div class="col-sm-12">
									<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "What makes me proud" : "Ma plus grande fierté" }}' %}</b></p>
						    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
						    	</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body row">
								<div class="col-sm-12">
									<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My biggest challenge" : "Mon plus grand défi" }}' %}</b></p>
						    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
						    	</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body row">
								<div class="col-sm-12">
									<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My main interests" : "Mes centres d\' intérêts" }}' %}</b></p>
						    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
						    	</div>
							</div>
						</div>
      				</div>			   
		      	</div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
	</div>
</div>
<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: inherit !important;
	}

	.reverse{
		-moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        filter: FlipH;
        -ms-filter: "FlipH";
	}

	.progress{
		height: 10px;
	}

	.btn-link{
		color: #FFB301 !important;
	}
</style>
<script type="text/javascript">
	$(function() {
	    window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });
	});

	function previewFile(a) {
		var preview = document.getElementById(a);
		var file    = document.querySelector('input[type=file]').files[0];
		var reader  = new FileReader();

	  	reader.addEventListener("load", function () {
	  		document.getElementById("imageSrc").src = reader.result;
	  		document.getElementById("avatar_url").value = reader.result;
	    	preview.src = reader.result;
	  	}, false);

	  	if (file) {
	    	reader.readAsDataURL(file);
	  	}
	}
</script>
<script type="text/javascript">
    var climageFrontApp = angular.module('climageFrontApp', ['chart.js'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
    });
    climageFrontApp.controller('climageFrontController', function PhoneListController($scope,$http) {
    	$scope.user = {};
        $scope.profil = [];

       	let temp = "<?php echo htmlspecialchars(json_encode($questionProfil)) ?>";
       	$scope.profil = JSON.parse(temp.replace(/&quot;/g,'"'));
       	$scope.reponse = [];
       	$scope.page = function(page){
        	window.location = page;
        }
        $scope.showProfil = function(user, reponses){
       		$scope.user = user;
       		console.log(user);
       		
       		console.log($scope.profil);
       		$scope.reponses = reponses.filter(function(item){
       			return item.reponse_text != null
       		});
       		console.log($scope.reponses);

        }

       	$scope.deleteInfo = function(){

	        $http({
	          method: 'POST',
	          data:{"idClimage": '{{ $climage->id }}', "user":$scope.user},
	          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/diagnostics/climage/deleteInfoClimage"
	        }).then(function successCallback(response) {
	          if(response.data.status == 1){
	            location.reload();
	          }
	        }, function errorCallback(response) {

	          //console.log(response);
	        });
      	}
    });


</script>
<script src="{{ asset('js/jquery.imgareaselect.min.js') }}"></script>
@endsection('content')

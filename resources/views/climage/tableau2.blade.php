{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    Climage  > {{ $climage->name }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> Climage</a></li>
    <li class="active">@lang('message.resultat')</li>
  </ol>
@stop
  
@section('content')
	<div class="row">
	    <div class="col-xs-12">
	      	<div class="box">
	        	<div class="box-header with-border">
	          		<h3 class="box-title">@lang('message.resultat')</h3>
	        	</div>
	            <!-- /.box-header -->
	        	<div class="box-body">
	          		<div class="row">
	            		<div class="col-xs-12">
	              			<div class="nav-tabs-custom">
	                			<ul class="nav nav-tabs">
				                  	<li class="active"><a href="#resultat1" data-toggle="tab">@lang('message.resultat')1</a></li>
				                  	<li><a href="#resultat2" data-toggle="tab">@lang('message.resultat')2</a></li>
				                  	<li><a href="#resultat3" data-toggle="tab">@lang('message.resultat')3</a></li>
				                </ul>
	                			<div class="tab-content">
	                				<div id="resultat1" class="tab-pane active">
	                					<div class="content" style="overflow-x:scroll;">
			
											<table class="table table-striped" id="myTable">
												<thead>
													<tr>
														<th>Date</th>
														<th>@lang('message.repondants')</th>

														@foreach ($page->questions as $key => $quest)
															@foreach ($quest->reponses as $rep => $reponse)
																@if($lang=='fr')
																	<th>{{ $page->title_fr }} - {{ $reponse->reponse_fr }}</th>
																@else
																	<th>{{ $page->title_en }} - {{ $reponse->reponse_en }}</th>
																@endif
																
															@endforeach
														@endforeach

													</tr>
												</thead>
												<tbody>
													
													@foreach($resultats as $date => $resultat)
														
														@foreach($resultat as $rep => $reponses)
															<tr>
																<td>{{ $date }}</td>
																<td>{{ $rep }}</td>

																@foreach ($page->questions as $key => $quest)
																	@foreach ($quest->reponses as $rep => $reponse)
																		@if(array_key_exists($reponse->id,$reponses))
																			<td>1</td>
																		@else
																			<td>0</td>
																		@endif
																	@endforeach
																@endforeach

															</tr>
														@endforeach
													@endforeach

												</tbody>
											</table>
										</div>
	                				</div>
	                				<div id="resultat2" class="tab-pane">
	                					<div class="content" style="overflow-x:scroll;">
			
											<table class="table table-striped" id="myTable2">
												<thead>
													<tr>
														<th>Date</th>
														<th>@lang('message.repondants')</th>

														@foreach ($page2->questions as $key => $quest)
															@foreach ($quest->reponses as $rep => $reponse)
																@if($lang=='fr')
																	<th>{{ $page2->title_fr }} - {{ $reponse->reponse_fr }}</th>
																@else
																	<th>{{ $page2->title_en }} - {{ $reponse->reponse_en }}</th>
																@endif
																
															@endforeach
														@endforeach

													</tr>
												</thead>
												<tbody>
													
													@foreach($resultats2 as $date => $resultat)
														
														@foreach($resultat as $rep => $reponses)
															<tr>
																<td>{{ $date }}</td>
																<td>{{ $rep }}</td>

																@foreach ($page2->questions as $key => $quest)
																	@foreach ($quest->reponses as $rep => $reponse)
																		@if(array_key_exists($reponse->id,$reponses))
																			<td>1</td>
																		@else
																			<td>0</td>
																		@endif
																	@endforeach
																@endforeach

															</tr>
														@endforeach
													@endforeach

												</tbody>
											</table>
										</div>
	                				</div>
									
									<div id="resultat3" class="tab-pane">
	                					<div class="content" style="overflow-x:scroll;">
			
											<table class="table table-striped" id="myTable3">
												<thead>
													<tr>
														<th>Date</th>
														<th>@lang('message.repondants')</th>

														@foreach($question3->reponses as $reponse)
															@if($lang=='fr')
																	<th>{{ $reponse->reponse_fr }}</th>
																@else
																	<th>{{ $reponse->reponse_en }}</th>
																@endif
														@endforeach
														@foreach ($page->questions as $key => $quest)
															@foreach ($quest->reponses as $rep => $reponse)
																@if($lang=='fr')
																	<th>{{ $page->title_fr }} - {{ $reponse->reponse_fr }}</th>
																@else
																	<th>{{ $page->title_en }} - {{ $reponse->reponse_en }}</th>
																@endif
																
															@endforeach
														@endforeach
													</tr>
												</thead>
												<tbody>

													@foreach($resultats3 as $date => $resultat)
														@foreach($resultat as $rep => $reponses)
															<tr>
																<td>{{ $date }}</td>
																<td>{{ $rep }}</td>
																@foreach ($question3->reponses as $rep => $reponse)
																	@if(array_key_exists($reponse->id,$reponses))
																		<td>1</td>
																	@else
																		<td>0</td>
																	@endif
																@endforeach
															</tr>
														@endforeach
													@endforeach										
												</tbody>
											</table>
										</div>
	                				</div>

	                			</div>
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop
@section('js')
<script> console.log('Hi!'); </script>
  <script type="text/javascript">
	$(document).ready(function(){
	    $('#myTable').DataTable({
        	dom: 'Bfrtip',
	        buttons: [ {
            extend: 'excelHtml5',
            customize: function ( xlsx ){
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                // jQuery selector to add a border
	                $('row c[r*="10"]', sheet).attr( 's', '25' );
            	}
        	}]
	    });
	    $('#myTable2').DataTable({
        	dom: 'Bfrtip',
	        buttons: [ {
            extend: 'excelHtml5',
            customize: function ( xlsx ){
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                // jQuery selector to add a border
	                $('row c[r*="10"]', sheet).attr( 's', '25' );
            	}
        	}]
	    });

	    $('#myTable3').DataTable({
        	dom: 'Bfrtip',
	        buttons: [ {
            extend: 'excelHtml5',
            customize: function ( xlsx ){
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
 
                // jQuery selector to add a border
	                $('row c[r*="10"]', sheet).attr( 's', '25' );
            	}
        	}]
	    });
	});
  </script>
@stop
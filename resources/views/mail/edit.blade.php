 {{-- resources/views/admin/dashboard.blade.php --}}

<style type="text/css">
  .btn-toolbar{
    display: none;
  }
</style>

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
      <img src="/img/apter_solutions_android-2-tranparent.png"
              srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x"
              class="apter_solutions_android-2-tranparent">
@stop
  
@section('content')
    <div ng-app="mailApp" ng-controller="mailController">
         <div class="box box-primary">

            <div class="box-header">
              <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail->type) - {{ $mail->language == 'fr' ? "Français" : "English"}}</h3>
            </div>
            <!-- /.box-header -->
            
                <form class="form-horizontal" method="POST" action="{% '/mail/'+curMail %}">
                  @csrf 
                  @method('PUT')
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail" id="mail">
                  <input type="hidden" name="id" value="{% curMail %}">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ? "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">
                        <!-- tools box -->
                        <!-- <div id="mailHtml" text-angular="text-angular" name="htmlcontent" ng-model="htmlcontent" ta-text-editor-class="form-control myform2-height" ta-html-editor-class="form-control myform2-height"></div> -->


                        <div id="summernote" name="text" ng-model="text">
                          
                        </div>


                        <!-- /. tools -->
                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <!-- <button type="submit" class="btn btn-default">Cancel</button> -->
                    <button type="submit" class="btn btn-default" ng-click="addTemplate($event)">@lang('message.ajout_hist')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.enregistrer')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>
           
          </div>

    </div>

   




@stop

@section('css')
  <style type="text/css">
    /* ====== ====== */
.ta-editor.form-control.myform1-height, .ta-scroll-window.form-control.myform1-height  {
    min-height: 300px;
    height: auto;
    overflow: auto;
    font-family: inherit;
    font-size: 100%;
}

.form-control.myform1-height > .ta-bind {
    height: auto;
    min-height: 300px;
    padding: 6px 12px;
}



/* ====== ====== */

.ta-editor.form-control.myform2-height, .ta-scroll-window.form-control.myform2-height  {
    height: 300px;
    min-height: 300px;
    overflow: auto;
    font-family: inherit;
    font-size: 100%;
}
.form-control.myform2-height > .ta-bind {
    min-height: 300px;
    padding: 6px 12px;
}

  </style>
@stop

@section('js')
   <script> console.log('Hi!'); </script>
   <!-- <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}"> -->
  <!-- <script src="{{ asset('js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script> -->
  <script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.2.4/angular-sanitize.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/textAngular/1.1.2/textAngular.min.js'></script>
    <script type="text/javascript">


      var all_mails = [];

      $("document").ready(function(){
        @foreach($mails as $m)
          all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
        @endforeach
        $('#summernote').summernote('code', all_mails["{{ $mail->id }}"]);
        $('select').on('change', function (e) {
          valueSelected = this.value;
          $("#mail_id").val(valueSelected);
          $('#summernote').summernote('code', all_mails[valueSelected]);
        });
      });

      var mailApp = angular.module('mailApp', ["textAngular"], function($interpolateProvider) {

        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      mailApp.controller('mailController', function PhoneListController($scope) {

            //$('select').on('change', function (e) {
              $scope.curMail = "{{ $mail->id }}";
              //console.log($scope.curMail);
            //});
            $scope.mail = {};
            $scope.mails = {};

            @foreach($mails as $m)

                $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
            @endforeach

           //  console.log($scope.mails)
           $scope.text = $scope.mails[$scope.curMail];

           
            $scope.change = function(){
              $scope.text = $scope.mails[$scope.curMail];
            }

            $scope.addTemplate = function(e){
              $("#template").val(true);
              //console.log($("#mail").val($(".note-editable").eq(0).html())[0].defaultValue);
              $("#mail").val($(".note-editable").eq(0).html())[0].defaultValue;
            }

            $scope.save = function(e){

                //e.preventDefault();
                console.log($("#mail").val($(".note-editable").eq(0).html())[0].defaultValue);
                $("#mail").val($(".note-editable").eq(0).html())[0].defaultValue;

                
            }
      });


    </script>
 

@stop


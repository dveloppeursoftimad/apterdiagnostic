{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')



  @section('content_header')
      <img src="/img/apter_solutions_android-2-tranparent.png"
              srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x"
              class="apter_solutions_android-2-tranparent">
      
  @stop


@section('content')
    <div class="row">
      <div class="col-md-6">
            <div class=" box box-primary">

              <div class="box-header">
                <h3 class="box-title">Mails Français</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    <th></th>
                    <th>Type</th>
                    <th></th>
                   
                  </tr>
                  
                  @foreach($mails_fr as $mail)
                  <tr>
                    <td></td>
                    <td>{{ ucfirst($mail->type) }}</td>
                    <td>
                      <a href="{{url('/mail/'.$mail->id.'/edit')}}"> <i class="fa fa-edit"></i> Modifier</a>
                    </td>
                  </tr>
                  @endforeach
                   
                    
                </table>
              </div>
            </div>
      </div>
      <div class="col-md-6">
            <div class=" col-md-6  box box-primary">

              <div class="box-header">
                <h3 class="box-title">Mails Anglais</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <tr>
                    <th></th>
                    <th>Type</th>
                    <th></th>
                   
                  </tr>
                  
                  @foreach($mails_en as $mail)
                  <tr>
                    <td></td>
                    <td>{{ ucfirst($mail->type) }}</td>
                    <td>
                      <a href="{{url('/mail/'.$mail->id.'/edit')}}"> <i class="fa fa-edit"></i> Modifier</a>
                    </td>
                  </tr>
                  @endforeach
                   
                    
                </table>
              </div>
            </div>
      </div>
           

            



    </div>
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet"  href="/css/trix.css">
  <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop


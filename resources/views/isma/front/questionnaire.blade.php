
  @extends('diagnostic')
  @section('content')
<div ng-app="ismaApp" ng-controller="ismaCtrl">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">
			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('feedback-front') }}">{{ __('message.inventaire') }}</a></span> 
					</h2>
					<hr>
					

					<!-- <div class="row">
					</div> -->
					<div class="row" >
							<div class="col-md-12">
								<div class="row">
									
								</div>
								
							</div>
				  			<div class="col-md-12">
				      			<div class="card">
				          			
				           			<div class="card-header card-header-text card-header-warning">
						            	<div class="card-text">
						              		<h3>@lang('message.inventaire')</h3>
						            	</div>
						        	</div>
						        	<div class="card-body">

						        		
							        		
						        		<form role="form" action="{{ route('isma-front-save') }}" method="POST">
							                @csrf
							                @method('POST')
							                <input type="hidden" name="idConsultant" value="{{ $idConsultant }}">
								            <div ng-if="consigne">
							        			<h4>@lang('message.consigne')</h4>
								        		<hr>
								        		<div>
								        			@lang("message.consigne_isma")
								        		</div>

							        		</div>
							               
							               			<div ng-repeat="(key, page) in pages">
							               				<div ng-repeat="(key2, question) in page.questions" ng-show="key == pageNum">
							               					<div class="col-md-12" id="question_{% question.id %}">
													    		<h4 style="font-size: 1.6em; color: #3c4858;" class="card-title">
													    			{% question['question_'+lang] %}
													    			
													    		<p style="color: red"> 
													    			{% question_error[question.id]  %}
													    		 </p>
													    			
													    	</div>

					                                            	<div class="col-md-6 col-xs-6" ng-repeat="(key2, reponse) in question.reponses">
											            				<div class="form-check" style="padding-top: 0px;">
											            					<div class="btn-group-toggle" >
																				<label class="form-check-label">

																                  	<input class="form-check-input" type="radio" name="reponse_question[{% question.id %}]" 
																                  	ng-model="question.reponse" value="{% reponse.id %}"

																                  	ng-click="selectRadio(question.id)"

																                  	>{% reponse['reponse_'+lang] %}
																                  	
																                  	<span class="circle" style="border:1px solid #ffd85b;">
																                    <span class="check" style="background-color: #ffd85b; border:1px solid #ffd85b;"></span>
																                  	</span>
																                </label>
																			</div>     
																		</div>
											            			</div>
					                                        	
					               
					                                        	<br>
							               				</div>
							               					
							               			</div>
													    	
											    
									
											    
							               		<div class="row">
														
															<div class="col-sm-12 text-right">
												            	<button  ng-if="!last" ng-click="next($event)" name="next" style="font-family: inherit;" class="btn btn-warning btn-round">
												            		{{ $lang == "en" ? "Next"  : "Suivant"}}
												            		<i class="material-icons">chevron_right</i></button>

												            	<button  ng-if="last" ng-click="next($event)" name="next" style="font-family: inherit;" class="btn btn-warning btn-round">
												            		{{ $lang == "en" ? "Save"  : "Enregistrer"}}
												            		<i class="material-icons">chevron_right</i></button>
												            </div>
												        
												     
												</div>

									
											
										</form>

												

									</div>
									<br>
					            </div>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	



	

</div>
<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: Roboto, sans serif !important;
	    font-weight: bold;
	}
</style>
<script type="text/javascript">
	$(function() {

        setTimeout(function(){
	    
			            window.scrollTo({ left: 0, top: document.body.scrollHeight/2, behavior: 'smooth' });
		}, 250);
	});
</script>
 <script type="text/javascript">
    var ismaApp = angular.module('ismaApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
    });



    ismaApp.controller('ismaCtrl', function PhoneListController($scope) {
    	$scope.reponse_question = [];
    	$scope.pages = [];
    	$scope.pages = {!! json_encode($pages) !!};
    	$scope.question_error = [];
    	console.log($scope.pages);
    	$scope.pageNum = undefined;
    	$scope.consigne = true;

    	$scope.lang = {!! json_encode($lang) !!};
    	$scope.last = false;
    	$scope.next = function(e){
    		
    		
	    	if($scope.consigne){
	    		e.preventDefault();
	    		$scope.consigne = false;
	    		$scope.pageNum = 0;
	    		setTimeout(function(){
	    
			            window.scrollTo({ left: 0, top: document.body.scrollHeight/10, behavior: 'smooth' });
		    	}, 250);
	    	}else{
	    		let pnext = $scope.pageNum+1;
	    		if( $scope.check()){
	    			if(pnext < $scope.pages.length){
	    				setTimeout(function(){
	    
				            window.scrollTo({ left: 0, top: document.body.scrollHeight/10, behavior: 'smooth' });
			    		}, 250);
		    			$scope.pageNum = pnext;
		    			if($scope.pageNum == ($scope.pages.length-1)){
		    				$scope.last = true;
		    			}else{
		    				e.preventDefault();
		    			}
	    			}
	    		} else {
	    			setTimeout(function(){
	    
			            window.scrollTo({ left: 0, top: document.body.scrollHeight/10, behavior: 'smooth' });
		    		}, 250);
		    		e.preventDefault();
	    		} 
	    	}
	    		
    	}

    	$scope.check = function(){
    		let res = true;
    		$scope.question_error =[];
    		let firstError = "";
    		for(let i in $scope.pages[$scope.pageNum].questions){
    			if(!$scope.pages[$scope.pageNum].questions[i].reponse){
    				if(res){
    					firstError = $scope.pages[$scope.pageNum].questions[i].id;
    				}
    				res = false;
    				$scope.question_error[$scope.pages[$scope.pageNum].questions[i].id] = "{{ ($lang == 'fr') ? 'Réponse requise' : 'Required answer' }}";
    				
    				
    			}
    		}
    		console.log("firstError %o", firstError);
    		if(!res){
    			setTimeout(function(){
    				let elmnt = document.getElementById("question_"+firstError);
		    		if(elmnt) elmnt.scrollIntoView({ block: 'center',  behavior: 'smooth' });
    			}, 250)
	    			
    		}
	    		
    		return res;
    	}

    	$scope.selectRadio = function(id){
				console.log(id);
				let next = parseInt(id)+1
				let elmnt = document.getElementById("question_"+next);
    			if(elmnt) elmnt.scrollIntoView({ block: 'center',  behavior: 'smooth' });
    			
    			
    			
			}

    });


    </script>
<!--/div-->
@endsection('content')

{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    @lang('message.isma')
    
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.isma')</a></li>
    <li class="active">@lang('message.questionnaire')</li>
  </ol>
@stop
  
@section('content')
<div ng-app="questionApp" ng-controller="questionController">
  @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Succès!</h4>
      {{ Session::get('message') }}
    </div>
  @endif
  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#formation" data-toggle="tab">@lang('message.isma')</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="formation">
            <section id="new">
              <h4 class="page-header">@lang('message.questionnaire') @lang('message.isma')</h4>
              @foreach ($isma->questions as $q => $question)
                <input type="hidden" name="first_quest" id="quest" value="{{$isma->questions[0]->id}}">
                <div class="box collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title" data-widget="collapse">{{ $question->question_fr }}</h3> - 
                    <h3 class="box-title" data-widget="collapse">{{ $question->question_en }}</h3>
                    <div class="box-tools pull-right">
                      <!-- <button class="btn btn-box-tool" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>)" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit"></i> </button> -->
                      <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-plus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-header with-border">
                    <div class="box-body">
                      <div class="alert alert-success alert-dismissible" ng-if="success[{{ $question->id }}]">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Succès!</h4>
                        {{ Session::get('message') }}
                      </div>
                      <div class="box">
                          <div class="box-body row">
                            <div class="col-sm-10">
                              <p><b>{{ $question->question_fr }}</b></p>
                              <p><b>{{ $question->question_en }}</b></p>
                              <p class="text-secondary"> @lang('message.reponse') : {{ $question->type }}</p>
                            </div>
                            @if(Auth::user()->role !== 'admin_acad')
                              <div class="col-sm-2 text-right">
                                <button class="btn" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>,{{$question->id}})" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit fa-2x"></i> </button>
                              </div>
                            @endif

                            <div class="col-sm-12">
                              @if (count($question->reponses) >0)
                                <div class="box box-warning">
                                  <div class="alert alert-success alert-dismissible" ng-if="success[{{ $q }}]">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-check"></i> Succès!</h4>
                                    {{ Session::get('message') }}
                                  </div>
                                  <div class="box-header with-border">
                                    <h3 class="box-title">@lang('message.reponses')</h3>
                                    @if(Auth::user()->role !== 'admin_acad')
                                      <div class="box-tools pull-right">
                                        <!-- <button type="button" class="btn btn-box-tool">
                                          <i class="fa fa-plus"></i>
                                        </button> -->
                                        <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($question->reponses)) ?>,<?php echo htmlspecialchars(json_encode($isma->questions[0]->id)) ?>,{{$q}})" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button>
                                      </div>
                                    @endif
                                  </div>
                                  <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                      @foreach ($question->reponses as $reponse)
                                        <li>
                                          <a > {{ $reponse->reponse_fr }} - {{ $reponse->reponse_en }}  
                                            <!-- <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($reponse)) ?>)" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button> -->
                                          </a>

                                        </li>
                                      @endforeach
                                    </ul>
                                  </div>
                                </div>
                              @endif
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </section>
          </div>
        </div>
      </div>
    </div>
  </div> 
    <div class="modal fade" id="edit-question">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">@lang('message.modifier') Question : <% question.titre | uppercase %></h4>
          </div>

          <!--form role="form" action="<% '/questions/'+question.id %>" method="POST"-->
            @csrf
            @method('PUT')
            <div class="modal-body">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <div class="form-group">
                <label for="question-fr">Question FR</label>
                <input type="text" class="form-control" name="question_fr" id="question_fr" placeholder="" ng-model="question.question_fr">
              </div>

              <div class="form-group">
                <label for="question-en">Question EN</label>
                <input type="text" class="form-control" name="question_en" id="question_en" placeholder="" ng-model="question.question_en">
              </div>

            </div>
            <!-- /.box-body -->

            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.fermer')</button>
              <input type="hidden" name="active" value="<%typeActives%>">
              <button type="submit" class="btn btn-primary" ng-disabled="!question.question_fr || !question.question_en" ng-click="editQuestions()">@lang('message.modifier')</button>
            </div>
          <!--/form-->
        </div>
      </div>
    </div>

    <div class="modal fade" id="edit-reponse">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">@lang('message.modifier') @lang('message.reponse') : <% reponse.titre | uppercase %></h4>
          </div>

          <!--form role="form"  method="POST" action="<% '/reponses/update' %>"-->
            <input type="hidden" name="id_quest" value="<%quest%>">
            @csrf
            @method('PUT')
            <div class="modal-body">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <div ng-repeat="rep in reponse">
                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                  <label for="question-fr">@lang('message.reponse') FR</label>
                  <input type="text" class="form-control" name="reponse_question[<% rep.id %>][fr]" id="reponse_question[<% rep.id %>][fr]" placeholder="" ng-model="rep.reponse_fr" >
                </div>

                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                  <label for="question-en">@lang('message.reponse') EN</label>
                  <input type="text" class="form-control" name="reponse_question[<% rep.id %>][en]" id="reponse_question[<% rep.id %>][en]" placeholder="" ng-model="rep.reponse_en">
                </div>

                <div class="form-group col-md-4 col-sm-4 col-xs-12">
                  <label for="question-en col-md-3">@lang('message.score')</label>
                  <input type="hidden" name="reponse_question[<% rep.id %>][score]" id="reponse_question[<% rep.id %>][score]" value="<% rep.score %>">
                  <select disabled="disabled" name="" class="form-control">
                    <option><% rep.score %></option>
                    <option ng-repeat="liste in liste_score" ng-if="liste != reponse.score"><% liste %></option>               
                  </select>
                </div>
              </div>
              <div class="form-group" style="display: none;">
                <input type="hidden" class="form-control" name="type" placeholder="" value="feedback">
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
              <input type="hidden" name="active" value="<% typeActives %>">
              <button ng-click="modifier_simple()" type="submit" name="" class="btn btn-primary">@lang('message.modif_quest')</button>
              <button ng-click="modifier_all()" type="submit" name="tous" class="btn btn-primary">@lang('message.modif_allquest')</button>
            </div>
          <!--/form-->
        </div>
      </div>
    </div>

</div>
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
  <script type="text/javascript">
    var questionApp = angular.module('questionApp', [], function($interpolateProvider) {
      $interpolateProvider.startSymbol('<%');
      $interpolateProvider.endSymbol('%>');
    });

    questionApp.controller('questionController', function PhoneListController($scope,$http) {
      $scope.question  = undefined; 
      $scope.reponse  = undefined; 
      $scope.typeActives='';
      
      @if($curQuestion)
          $scope.question = <?php echo stripcslashes($curQuestion)  ?>;
      @endif

      if($scope.question){
          $('#edit-question').modal('show');
      }

      console.log("id question %o", $scope.question )
      $scope.setActives= function setActives(typeActive){
        $scope.typeActives =typeActive;
      }

      // $http({
      //     method: 'GET',
      //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      //     url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/isma/isma_ajax"
      //   }).then(function successCallback(response) {
      //     if(response.data.status == 1){
      //       $scope.test = response.data;
      //     }
      //   }, function errorCallback(response) {

      //     //console.log(response);
      //   });

      var key = 0;
      var s = 0;
      var p = 0;
      var sec_id = 0;

      $scope.editQuestion = function(question,key_q){
        console.log("edit question %o", question);
        
        //$scope.check = question.negative == 0 ? false : true;
        $scope.check = null;

        $scope.question = question;
        s = key_q;
      
        
      }

      $scope.data_rep = [];
      $scope.editReponse = function(reponse,quest,rep){
        //console.log("edit reponse %o", reponse);
        key = rep;
        $scope.reponse = reponse;
        $scope.quest = quest;
        
        // for(var i=0;i<$scope.reponse.length;i++){
        //   console.log($scope.reponse[i].id);
        //   $scope.data_rep.push($("#reponse_question["+$scope.reponse[i].id+"][fr]").val());
        // }
        // console.log($scope.data_rep);

      }

      $scope.success = [];

      $scope.liste_score = [1,2,3,4,5,6,7];

      $scope.neg = [0,1];


      $scope.editQuestions = function(){

        $http({
          method: 'POST',
          data:{"questions": $scope.question, "check":$scope.check},
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/questions/question_isma"
        }).then(function successCallback(response) {
          if(response.data.status == 1){
            location.reload();
            $scope.success[s] = true;
            $('#edit-question').modal('hide');
          }
        }, function errorCallback(response) {

          //console.log(response);
        });


      }

      $scope.modifier_simple = function(){
      //$(document).on("click","#modifier_simple",function(){
        $http({
          method: 'POST',
          data:{"reponse": $scope.reponse},
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/reponses/question"
          }).then(function successCallback(response) {
            if(response.data.status == 1){
              $scope.success[key] = true;
              location.reload();
              $('#edit-reponse').modal('hide');
            }
          }, function errorCallback(response) {

              //console.log(response);
          });
      }

      $scope.modifier_all = function(){
        $http({
          method: 'POST',
          data:{"reponse": $scope.reponse, "quest": $scope.quest},
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          cache : false,
          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/reponses/questions",
        }).then(function successCallback(response) {
          if(response.data.status == 1){
            $scope.success[key] = true;
            console.log(response);
            location.reload();
            $('#edit-reponse').modal('hide');
          }

        }, function errorCallback(response) {

        });

      }


    });
  </script>  
@stop
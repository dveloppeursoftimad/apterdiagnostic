{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-user"></span>
    Consultant
    
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> Consultant</a></li>
    <li class="active" style="cursor:pointer">Liste</li>
    <!-- <li class="active" id="liste" style="cursor:pointer">Détails</li> -->
    <!-- <li class="active" id="details" style="cursor:pointer;">@lang('message.liste') Excel</li> -->
  </ol>
  <br>
  <h6 class="text-right"><span id="details" style="cursor:pointer; padding:11px;background-Color:#c0c2c3;font-weight: 600;color:white;"><i class="fa fa-reorder box-icon" style="color:white;"></i> @lang('message.details')</span><span id="liste" style="cursor:pointer; padding:11px;background-color:#eceeef; color:#838383;"><i class="fa fa-file-excel-o box-icon"></i>  @lang('message.liste') Excel</span></h6>
@stop
@section('content')

<div ng-app="ismaApp" ng-controller="ismaCtrl">
  <div>
    @if(session('error_code'))
      <div class="alert alert-danger">
        <div class="alert-icon">
          <i class="material-icons">error_outline</i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        <b>{{ session('error_code') }}</b>
      </div>
    @endif
  </div>
  <div class="row"  >

    <div class="col-sm-12 col-md-6">
      <div class="form-group">
        <div class="input-group" >
          <!-- <input type="text" class="form-control timepicker"  id="myInput" onkeyup="myFunction()"> -->
          <input type="text" class="form-control timepicker" placeholder="Filter..." ng-model="filterText">

          <div class="input-group-addon">
            <i class="fa fa-search"></i>
          </div>
        </div>
        <!-- /.input group -->
      </div>
    </div>
    <div class="row col-sm-12" style="min-height: 500px;" id="listeUsers">
    <!-- <ul>
  
      <li ng-repeat="item in filterData = (totalItems | filter : {name: filterText}) | limitTo:10:3*(page-1)">
      
        <div>{% item.name %}</div>

      </li>

    </ul> -->
    
      <!-- <uib-pagination ng-model="currentPage" total-items="tests.length" max-size="maxSize" items-per-page="numPerPage" boundary-links="true"></uib-pagination> -->

      <div class="col-sm-12 col-md-6" ng-repeat="(key, test) in filterData = (totalItems | filter : {name: filterText}) | limitTo:10:3*(page-1)">
          <div class="box" id="h3_{% key %}">
            <div class="box-header with-border">
              <span ng-if="test.description">
                <h3 class="box-title">{% test.name %} - WP @lang("message.isma") </h3>
              </span>
              <span ng-if="!test.description">
                <h3 class="box-title">{% test.name %} {% test.description %} </h3>
              </span>
              <p class="text-secondary">Consultant : {% test.consultant.firstname %} {% test.consultant.lastname %}</p>
              <p class="text-secondary">@lang('message.coach') : {% test.users[0].firstname %} {% test.users[0].lastname %}</p>
              <p class="text-secondary">Date : {% test.created_at %}</p>
              @if(Auth::user()->role !== 'admin_acad')
                <div class="box-tools pull-right">

                  <span class="label label-default" ng-click="deleteTest(test)" data-toggle="modal" data-target="#delete-fb"><i class="fa fa-trash"></i></span>
                </div>
              @endif
            </div>
            <!-- <div class="box-body text-right row">
              @if(App::getLocale() == 'fr')
                <span class="col-sm-4"></span>
                <a href="/isma/rapport_court_fr/{% test.id %}" class="text-warning col-sm-4"> <span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court')</a>
                <a href="/isma/rapport_long/{% test.id %}" class="text-warning col-sm-4"> <span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_long')</a>
              @else
                <span class="col-sm-8"></span>
                <a href="/isma/rapport_court_en/{% test.id %}" class="text-warning col-sm-4"> <span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court')</a>
              @endif
            </div> -->
            <div class="box-body text-right row">
              <span class="col-sm-3"></span>
              <a href="/isma/liste_rapport_isma/{% test.id %}" class="text-warning col-sm-4"> <span class="glyphicon glyphicon-stats"></span> @lang("message.liste_isma")</a>
              <a href="/isma/resultatIsma/{% test.id %}" class="text-warning col-sm-5"> <span class="glyphicon glyphicon-sort-by-attributes-alt"></span> @lang("message.tabRep")</a>
            </div>
          </div>

      </div>
              
    </div>

    <div class="row col-sm-12" style="min-height: 500px; display:none;" id="detailsUsers">

      <div class="content" style="overflow-x:scroll;">
        <table class="table table-striped" id="myTable">
          <thead>
            <tr>
              <th>@lang('message.nom')</th>
              <th>Consultant</th>
              <th>@lang('message.coach')</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($tests as $test)
              <tr>
                <td>{{$test->name}}</td>
                <td>{{$test->consultant->firstname}} {{$test->consultant->lastname}}</td>
                <td>{{$test->users[0]->firstname}} {{$test->users[0]->lastname}}</td>
                <td>{{$test->users[0]->created_at}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
            
    </div>

    <div class="container col-sm-12" id="paginion">
      <div class="row" data-ng-show="tests.length == 0">
        <div class="col-span-12">
        No results found
        </div>
      </div>
      <div class="row" data-ng-show="tests.length > 0">
        <div class="col-span-12">
          <!-- <uib-pagination ng-model="currentPage" total-items="tests.length" max-size="maxSize" items-per-page="numPerPage" boundary-links="true"></uib-pagination> -->
          <uib-pagination class="pagination-sm pagination" total-items="tests.length" ng-model="page"
      ng-change="pageChanged()" previous-text="&lsaquo;" next-text="&rsaquo;" max-size="maxSize" items-per-page=10></uib-pagination>
        </div>
      </div>
    </div>
    <div class="modal fade" id="delete-fb">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> <i class="fa fa-trash"></i> Suppression</h4>
          </div>

          <form role="form" action="{% '/isma/'+test.id %}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-body">
              <p>@lang('message.sup_test') {% test.name %}?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
              <button type="submit" class="btn btn-primary">@lang('message.supprimer')</button>
            </div>
          </form>
            
        </div>
      </div>
    </div>
  </div>

  <style type="text/css">
    .text-secondary{
      margin-bottom: 0 !important;
    }
  </style>
@stop


@section('js')
  <script> console.log('Hi!');
    function myFunction() {
      var input, filter, ul, li, a, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      h3 = document.getElementsByTagName("h3");
      for (i = 0; i < h3.length; i++) {
        a = h3[i];
        txtValue = a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          document.getElementById("h3_"+i).style.display = "";
            //h3[i].style.display = "";
        } else {
          document.getElementById("h3_"+i).style.display = "none";
            //h3[i].style.display = "none";
        }
      }
    }
  </script>
  <script type="text/javascript">
    $(document).ready(function(){

        $('#liste').click(function() {
          $("#paginion").css("display","none");
          $('#listeUsers').css('display', 'none');
          $('#detailsUsers').css('display', 'block');
          // $(this).css({
          //   background-Color:#b5b4b4;font-weight: 535;
          // });
          $(this).css('background-color','#c0c2c3');
          $(this).css('font-weight',600);
          $(this).css('color',"white");
          $('.fa-file-excel-o').css('color','white');

          $("#details").css('background-color','#e0dede');
          $("#details").css('font-weight',400);
          $("#details").css('color',"#838383");
          $('.fa-reorder').css('color','#838383');
          
        });

        $('#details').click(function() {
          $("#paginion").css("display","block");
          $('#detailsUsers').css('display', 'none');
          $('#listeUsers').css('display', 'block');
          $(this).css('background-color','#c0c2c3');
          $(this).css('font-weight',600);
          $(this).css('color',"white");
          $('.fa-reorder').css('color','white');

          $("#liste").css('background-color','#e0dede');
          $("#liste").css('font-weight',400);
          $("#liste").css('color',"#838383");
          $('.fa-file-excel-o').css('color','#838383');
        });

        $('#myTable').DataTable({
          dom: 'Bfrtip',
          buttons: [ {
            extend: 'excelHtml5',
              customize: function ( xlsx ){
                  var sheet = xlsx.xl.worksheets['sheet1.xml'];
                  // jQuery selector to add a border
                  $('row c[r*="10"]', sheet).attr( 's', '25' );
              }
          }],
        });

      });
    var ismaApp = angular.module('ismaApp',  ['ui.bootstrap'], function($interpolateProvider) {
      $interpolateProvider.startSymbol('{%');
      $interpolateProvider.endSymbol('%}');
    });
    ismaApp.controller('ismaCtrl', function($scope,$http) {
          $scope.test = {};
          $scope.deleteTest = function(test){
            $scope.test = test;
          }
          $scope.tests = [];
          $scope.tests = {!! json_encode($tests) !!};


          console.log($scope.tests );
          $scope.filteredTests = [];
          $scope.currentPage = 1;
          $scope.numPerPage = 10;
          $scope.maxSize = 5;


          $scope.$watch('currentPage + numPerPage', updateFilteredItems);

          function updateFilteredItems() {
            var begin = (($scope.currentPage - 1) * $scope.numPerPage),
              end = begin + $scope.numPerPage;

            $scope.filteredTests = $scope.tests.slice(begin, end);
          }

            $scope.page = 1;
            $scope.totalItems = [];
            $scope.totalItems = $scope.tests;
            console.log("item",$scope.totalItems);
                                  
            $scope.displayItems = $scope.totalItems.slice(0, 3);
            $scope.pageChanged = function() {
              var startPos = ($scope.page - 1) * 3;
            console.log($scope.page);
          };
      
    });


  </script>
@stop
{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    @lang('message.isma') > {{ $nom }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.isma')</a></li>
    <li class="active">@lang('message.liste_isma')</li>
  </ol>
@stop
  
@section('content')
	<div ng-app="ismaApp" ng-controller="ismaCtrl">
		<div class="row col-sm-12" style="min-height: 500px;">
	      	<div class="content">
	        	<table class="table table-striped" id="myTable">
	          		<thead>
	            		<tr>
			              	<th>Dates</th>
			              	<th colspan="2" class="text-center">Rapport @lang('message.isma')</th>
			            </tr>
	          		</thead>
	          		<tbody>
		              	<tr ng-repeat="liste in listes">
		              		<td> {% liste | date:'dd/MM/yyyy' %}</td>
		              		@if(App::getLocale() == 'fr')
			                	<!-- <td class="text-center"><a class="text-warning" href="/isma/rapport_court_fr/{% id %}/{% liste %}"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court')</a></td> -->
			                	<td class="text-center"><a class="text-warning" href="/isma/rapport_court_fr_new/{% id %}/{% liste %}/pdf"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PDF</a></td>
			                	<!-- <td class="text-center"><a class="text-warning" href="/isma/rapport_court_fr_new_pptx/{% id %}/{% liste %}/pptx"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PPTX</a></td> -->

								
								<td class="text-center" id="{% liste %}" ng-if="loading !== liste  && !rapportPptx" ><a class="text-warning" href=""ng-click="generatePptx(<?php echo htmlspecialchars(json_encode($id)) ?>,liste)"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PPTX</a></td>

								<td class="text-center"  ng-if="loading == liste && !rapportPptx"><a class="col-sm-9" style="cursor: pointer; background-color:transparent;border:none; color:#FFC107 !important" disabled>
					               <i class="fa fa-spinner fa-spin"></i> Chargement ...
					            </a></td>

					            <td  class="text-center" ng-if="loading == liste && rapportPptx">
					            	<a style="cursor: pointer; border:none; color:#FFC107 !important" href="{% pptx %}" style="color: white" class="col-sm-9">
						                <i class="fa fa-download"></i> Télécharger le rapport pptx
						            </a>
					            </td>

			                	<!-- <td class="text-center"><a class="text-warning" href="/isma/rapport_long/{% id %}/{% liste %}"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_long')</a></td> -->

			                @else 

								<td class="text-center"><a class="text-warning" href="/isma/rapport_court_en_new/{% id %}/{% liste %}/pdf"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PDF</a></td>
								<!-- <td class="text-center"><a class="text-warning" href="/isma/rapport_court_en_new_pptx/{% id %}/{% liste %}/pptx"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PPTX</a></td> -->

								<td class="text-center" id="{% liste %}" ng-if="loading !== liste  && !rapportPptx" ><a class="text-warning" href=""ng-click="generatePptxEn(<?php echo htmlspecialchars(json_encode($id)) ?>,liste)"><span class="glyphicon glyphicon-stats"></span> @lang('message.rapport_court') PPTX</a></td>

								<td class="text-center"  ng-if="loading == liste && !rapportPptx"><a class="col-sm-9" style="cursor: pointer; background-color:transparent;border:none; color:#FFC107 !important" disabled>
					               <i class="fa fa-spinner fa-spin"></i> Chargement ...
					            </a></td>

					            <td  class="text-center" ng-if="loading == liste && rapportPptx">
					            	<a style="cursor: pointer; border:none; color:#FFC107 !important" href="{% pptx %}" style="color: white" class="col-sm-9">
						                <i class="fa fa-download"></i> Télécharger le rapport pptx
						            </a>
					            </td>

			                @endif
		              	</tr>
		          	</tbody>
	        	</table>
	      	</div>  
	    </div>
	</div>
@stop
@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/style.css">
@stop

@section('js')
  	<script type="text/javascript">
  		var ismaApp = angular.module('ismaApp',  ['ui.bootstrap'], function($interpolateProvider) {
	      	$interpolateProvider.startSymbol('{%');
	      	$interpolateProvider.endSymbol('%}');
	    });

	    ismaApp.controller('ismaCtrl', function($scope,$http) {

          	$scope.tests = [];
          	$scope.loading = [];
          	$scope.id = undefined;
          	$scope.id = {!! json_encode($id) !!};
          	$scope.listes = {!! json_encode($listes) !!};
          	console.log($scope.listes);

          	$scope.loading = null;

          	$scope.generatePptx = function(id,liste){
          		$scope.loading = liste;



		        let data = {
		          id : id,
		          date : liste,
		          genre : "pptx"
		        }

		        $http({
		          method: 'POST',
		          async : false,
		          data: data,
		          url: '{{ url("isma/rapport_court_fr_new_pptx/") }}',
		        }).then(function successCallback(response) {
		            console.log("la reponse "+JSON.stringify(response));
		            //$scope.loading = false;
		          if(response.data.generated){
		              console.log(response.data.doc);
		              $scope.rapportPptx = true;
		              $scope.pptx = response.data.doc;
		          }
		          else{
		            $scope.error = response.data.message;
		          }
		        }, function errorCallback(response) {

		        });
	      	}

	      	$scope.generatePptxEn = function(id,liste){
          		$scope.loading = liste;



		        let data = {
		          id : id,
		          date : liste,
		          genre : "pptx"
		        }

		        $http({
		          method: 'POST',
		          async : false,
		          data: data,
		          url: '{{ url("isma/rapport_court_en_new_pptx/") }}',
		        }).then(function successCallback(response) {
		            console.log("la reponse "+JSON.stringify(response));
		            //$scope.loading = false;
		          if(response.data.generated){
		              console.log(response.data.doc);
		              $scope.rapportPptx = true;
		              $scope.pptx = response.data.doc;
		          }
		          else{
		            $scope.error = response.data.message;
		          }
		        }, function errorCallback(response) {

		        });
	      	}
          
    	});

  	</script>
@stop
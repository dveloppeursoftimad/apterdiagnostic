@extends('diagnostic')
	@section('content')
		<div ng-app="climageFrontApp" ng-controller="climageFrontController">
			<div class="section section-basic" style="padding: 10px 0;" >
				<div class="container climage-form" style="position: relative;">
					<div>
						<div class="title_mobile">
							<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('climage-front') }}">{{ __('message.climage') }}</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $climage->name }} </span> </h2>
							<hr>
							<input type="hidden" name="" value="{{ $lang == "en" ? "The activities" : "L'activité" }}" id="text_parse">
							<div class="btn-group btn-group-toggle" style="font-family: Roboto,sans serif; display: block;">
								@foreach($pages as $p)
								
									<label ng-click="page('{{ $p->url }}')" class="btn btn-secondary">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $p->{'title_'.$lang} }}
									</label>
								
								@endforeach
								<label class="btn btn-secondary active">
							    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $lang == "en" ? "Results"  : "Résultats"}}
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" id="mobile_padding">
								<div class="card card-nav-tabs">
							    <div class="card-header card-header-warning">
							      <div class="nav-tabs-navigation">
							        <div class="nav-tabs-wrapper">
							          <ul class="nav nav-tabs" data-tabs="tabs">
						              @if($climage->type->name != "climage-interlocuteur")
						                <li class="nav-item">
		                          <a class="nav-link active show" href="#profile" data-toggle="tab">
		                            <i class="material-icons">bar_chart</i> {{ __('message.resultat') }} 1
		                          <div class="ripple-container"></div></a>
						                </li>
						              @endif
					                <li class="nav-item">
	                          <a class="nav-link" href="#messages" data-toggle="tab">
	                            <i class="material-icons">bar_chart</i> {{ __('message.resultat2') }} 2
	                          <div class="ripple-container"></div></a>
	                        </li>
	                        @if($climage->type->name != "climage-interlocuteur")
		                        <li class="nav-item">
		                          <a class="nav-link" href="#messages2" data-toggle="tab">
		                            <i class="material-icons">bar_chart</i> {{ __('message.resultat2') }} 3
		                          <div class="ripple-container"></div></a>
		                        </li>
	                        @endif
													@if($climage->type->name == "climage-negociation")
														<li class="nav-item">
		                          <a class="nav-link" href="#messages2" data-toggle="tab">
		                            <i class="material-icons">bar_chart</i> {{ __('message.resultat2') }} 4
		                          <div class="ripple-container"></div></a>
		                        </li>
													@endif
                         	<li class="nav-item">
                          	<a class="nav-link" href="#trombinoscope" data-toggle="tab">
                            	<i class="material-icons">face</i> @lang('message.trombinoscope')
                          	<div class="ripple-container"></div></a>
                        	</li>
						            </ul>
						          </div>
						        </div>
						     	</div>
						      <div class="card-body">
	                  <div class="tab-content text-center">
	                  	@if($climage->type->name != "climage-interlocuteur")
	                    	<div class="tab-pane active show" id="profile">
													<div class="row">
														<div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
								            <div ng-if="day!=undefined">
						                  @foreach($page2->questions as $q=>$question)
							                  @if($q==0)
								                  <div class="col-sm-12">
				                    				<h3>{{ $question->{'titre_'.$lang} }}</h3>
				                    			</div>
							                    			
																	<div class="col-xs-12 row">
								                    @foreach($question->reponses as $key=>$reponse)
									                    <div class="col-xs-6 col-md-6">
									                    	@if($loop->iteration  % 2 == 0)
																		      <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
																		       	<br>
																		        <span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		      </p>
									                    		<div class="progress" style="height: 20px;">
																						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#2196f3' : ($key == 3 ? '#2196f3' : ($key == 5 ? '#ffc107' : ($key == 7 ? '#ffc107' : '' )))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																							<span class="sr-only">30% Complete</span>
																						</div>
																					</div>
																					<br>
																		    @else
																		    	<p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
																		    		<br>
																		    		<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		    	</p>
																		    	<div class="progress reverse" style="height: 20px;">
																						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#2196f3" : (($key == 2)? "#2196f3" : (($key == 4)? "#ffc107" : (($key == 6)? "#ffc107" : ""))))}};width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																							<span class="sr-only">30% Complete</span>
																						</div>
																					</div>
																		    @endif
																			</div>
								                    @endforeach			
								                  </div>
							                  @endif
							                  @if($q==1)
				                    			<div class="col-sm-12">
				                    				<h3>{{ $question->{'titre_'.$lang} }}</h3>
				                    			</div>			
																	<div class="col-xs-12 row">
				                    				@foreach($question->reponses as $key=>$reponse)
					                    				<div class="col-xs-6 col-md-6">
					                    					@if($loop->iteration  % 2 == 0)
																	        <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
																	        	<br>
																	        	<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																	        </p>
									                    		<div class="progress" style="height: 20px;">
																						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#e91e63' : ($key == 3 ? '#e91e63' : ($key == 5 ? '#4caf50' : ($key == 7 ? '#4caf50' : '' )))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																							<span class="sr-only">30% Complete</span>
																						</div>
																					</div>
																					<br>
																		    @else
																		    	<p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
																		    		<br>
																		    		<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		    	</p>
																		    	<div class="progress reverse" style="height: 20px;">
																						<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#e91e63" : (($key == 2)? "#e91e63" : (($key == 4)? "#4caf50" : (($key == 6)? "#4caf50" : ""))))}};width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																							<span class="sr-only">30% Complete</span>
																						</div>
																					</div>
																		    @endif
									                    </div>
								                    @endforeach
																	</div>
							                  @endif  			
							                  <hr>
							                @endforeach
							              </div>
						              </div>
												</div>
						          @endif
						          <div class="tab-pane {{ $climage->type->name == 'climage-interlocuteur' ? 'active show' : '' }}" id="messages">             		
												@if($climage->type->name == "climage-negociation")
														
												@elseif($climage->type->name == "climage-interlocuteur")
													
													<div class="row" ng-if="dates[day].climage">
														<div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>

				                    <div class="col-sm-9 container" style="position: relative;height:300px;" id="desktop">
			                        <br/>
			                        <div class="contains">
		                            <img src="{{ url('/img/climage2.png') }}" height="295px">
		                          </div>
		                          <div class="contains" style="margin-top:60px">
		                          	<canvas class="chart chart-radar" chart-data="data5" width="350" height="150" chart-options="options1" chart-labels="labels5" style="margin-left: -5px;"> </canvas>
		                          </div>
		                          <div class="contains" style="margin-top:135px;margin-left:-5px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total1"] %}%</div>
                              </div>
		                          <div class="contains" style="margin-left:-15px;">
		                          	<div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["serieux"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSerieux"] %})</div>
		                          </div>
		                          <div class="contains">
		                          	<div style="margin-top:125px;margin-left:-130px; font-size:11px;">{% dates[day].climage[hours]["conforme"] %}%</div>
			                            <div style="font-size:11px;margin-left:-130px;">({% dates[day].climage[hours]["signeConforme"] %})</div>
			                        </div>
			                        <div class="contains">
	                          		<div style="margin-top:125px;margin-left:90px; font-size:11px;">{% dates[day].climage[hours]["transgressif"] %}%</div>
		                            <div style="font-size:11px;margin-left:90px;">({% dates[day].climage[hours]["signeTransgressif"] %})</div>
			                        </div>
			                        <div class="contains" style="margin-left:5px;">
		                          	<div style="margin-top:190px; font-size:11px;">{% dates[day].climage[hours]["enjoue"] %}%</div>
			                          <div style="font-size:11px;">({% dates[day].climage[hours]["signeEnjoue"] %})</div>
			                        </div>
		                          <div class="contains" style="margin-top:105px;margin-left:5px;">
		                          	<canvas class="chart chart-radar" chart-data="data6" chart-options="options1" chart-labels="labels5" width="350" height="150" style="    margin-top: -43px;margin-left: 270px;"> </canvas>
		                          </div>
		                          <div class="contains" style="margin-left:275px">
	                              <div style="margin-top:138px;font-size:11px;">{% dates[day].climage[hours]["total2"] %}%</div>
	                            </div>
		                          <div class="contains" style="margin-left:265px">
		                          	<div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["soi"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSoi"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:220px">
		                          	<div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["sympathie"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSymphatie"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:285px">
		                          	<div style="margin-top:190px;font-size:11px;">{% dates[day].climage[hours]["autrui"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeAutrui"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:330px">
		                          	<div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["maitrise"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeMaitrise"] %})</div>
		                          </div>
		                   			</div>
		                   			<div id="phone" style="position:relative; height:700px;">
		                   				<div style="margin-left:-343px; position:absolute">
		                   					<img src="{{ url('/img/interg2.png') }}" height="350px">
		                   				</div>
		                   				<div class="contains" style="margin-top:82px; margin-left:-320px;">
		                          	<canvas class="chart chart-radar" chart-data="data5" width="350" height="150" chart-options="options1" chart-labels="labels5" style="margin-left: -5px;"> </canvas>
		                          </div>
		                          <div class="contains" style="margin-top:156px;margin-left:-325px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total1"] %}%</div>
                              </div>
		                          <div class="contains" style="margin-left:-350px;">
		                          	<div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["serieux"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSerieux"] %})</div>
		                          </div>
		                          <div class="contains">
		                          	<div style="margin-left:-785px;margin-top:160px;">{% dates[day].climage[hours]["conforme"] %}%</div>
			                            <div style="font-size:11px;margin-left:-790px;">({% dates[day].climage[hours]["signeConforme"] %})</div>
			                        </div>
			                        <div class="contains" style="margin-left:-350px;">
	                          		<div style="margin-top:220px;margin-left:90px; font-size:11px;">{% dates[day].climage[hours]["transgressif"] %}%</div>
		                            <div style="font-size:11px;margin-left:90px;">({% dates[day].climage[hours]["signeTransgressif"] %})</div>
			                        </div>
			                        <div class="contains" style="margin-left:-250px;">
		                          	<div style="margin-top:125px; font-size:11px;">{% dates[day].climage[hours]["enjoue"] %}%</div>
			                          <div style="font-size:11px;">({% dates[day].climage[hours]["signeEnjoue"] %})</div>
			                        </div>
			                        <div style="margin-left:-343px; margin-top:350px; position:absolute">
		                   					<img src="{{ url('/img/interd2.png') }}" height="350px">
		                   				</div>
		                   				
		                   				<div class="contains" style="margin-top:475px; margin-left:-600px;">
		                          	<canvas class="chart chart-radar" chart-data="data6" chart-options="options1" chart-labels="labels5" width="350" height="150" style="    margin-top: -43px;margin-left: 270px;"> </canvas>
		                          </div>
		                          <div class="contains" style="margin-top:505px;margin-left:-325px;">
                                <div style="font-size:11px;">{% dates[day].climage[hours]["total2"] %}%</div>
                              </div>
		                          <div class="contains" style="margin-left:-348px;margin-top:355px">
		                          	<div style="margin-top:55px;font-size:11px;">{% dates[day].climage[hours]["soi"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSoi"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:-405px;margin-top:385px">
		                          	<div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["sympathie"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeSymphatie"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:-305px;margin-top:370px;">
		                          	<div style="margin-top:190px;font-size:11px;">{% dates[day].climage[hours]["autrui"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeAutrui"] %})</div>
		                          </div>
		                          <div class="contains" style="margin-left:-250px;margin-top:345px">
		                          	<div style="margin-top:125px;font-size:11px;">{% dates[day].climage[hours]["maitrise"] %}%</div>
		                            <div style="font-size:11px;">({% dates[day].climage[hours]["signeMaitrise"] %})</div>
		                          </div>
		                   			</div>
			                		</div>
												@else
												
													<!-- CLIMAGE RESULTAT 2 SIMPLE -->

													<div class="row" id="desktop" ng-if="dates[day].climage">
														<div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
					          	<div class="col-sm-9 container" style="position: relative;height:450px;">
					            	<div style="position:absolute;width:400px;font-weight:bold;">
										<img src="{{ $lang == "en" ? url('/img/CG_2.png') :url('/img/pt3.png') }}" height="450px" style="margin-left:50px;" id="img_g">
									</div>
									<div class="contains text" id="serieux" style="margin-left:{{ $lang == 'fr' ? '25px' : '25px'}}; margin-top:{{ $lang == 'fr' ? '150px' : '160px' }};">
			                          {% dates[day].graphe[hours]["serieux"] %}%
			                        </div>
			                        <div class="contains" id="serieuxSVG" style="margin-left:{{ $lang == 'fr' ? '24px' : '21px'}}; margin-top:{{ $lang == 'fr' ? '163px' : '174px' }};">
			                          <svg height="60" width="60">
			                            <circle id="bleu2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['serieux'] == 0 ? 1 : (dates[day].graphe[hours]['serieux'] > 0 ? ((dates[day].graphe[hours]['serieux'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
															
									<div class="contains text" id="conforme" style="margin-left:{{ $lang == 'fr' ? '-64px' : '-70px'}}; margin-top:{{ $lang == 'fr' ? '196px' : '206px' }};">
			                          {% dates[day].graphe[hours]["conforme"] %}%
			                        </div>
			                        <div class="contains" id="conformeSVG" style="margin-left:{{ $lang == 'fr' ? '-18px' : '-21px'}}; margin-top:{{ $lang == 'fr' ? '199px' : '211px' }};">
			                          <svg height="60" width="60">
			                            <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['conforme'] == 0 ? 1 : (dates[day].graphe[hours]['conforme'] > 0 ? ((dates[day].graphe[hours]['conforme'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>

			                        <div class="contains" id="enjoueSVG" style="margin-left:{{ $lang == 'fr' ? '23px' : '20px'}}; margin-top:{{ $lang == 'fr' ? '236px' : '241px' }};">
			                          <svg height="60" width="60">
			                            <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['enjoue'] == 0 ? 1 : (dates[day].graphe[hours]['enjoue'] > 0 ? ((dates[day].graphe[hours]['enjoue'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="enjoue" style="margin-left:{{ $lang == 'fr' ? '27px' : '24px'}}; margin-top:{{ $lang == 'fr' ? '285px' : '292px' }};">
			                          {% dates[day].graphe[hours]['enjoue'] %}%
			                        </div>

			                        <div class="contains" id="transgressifSVG" style="margin-left:{{ $lang == 'fr' ? '62px' : '62px'}}; margin-top:{{ $lang == 'fr' ? '199px' : '211px' }};">
			                          <svg height="60" width="60">
			                            <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['transgressif'] == 0 ? 1 : (dates[day].graphe[hours]['transgressif'] > 0 ? ((dates[day].graphe[hours]['transgressif'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="transgressif" style="margin-left:{{ $lang == 'fr' ? '117px' : '116px'}}; margin-top:{{ $lang == 'fr' ? '198px' : '206px' }};">
			                          {% dates[day].graphe[hours]['transgressif'] %}%
			                        </div>

			                        <div class="contains text" id="soi" style="margin-left:{{ $lang == 'fr' ? '300px' : '302px'}}; margin-top:{{ $lang == 'fr' ? '149px' : '159px' }};">
			                          {% dates[day].graphe[hours]["soi"] %}%
			                        </div>
			                        <div class="contains" id="soiSVG" style="margin-left:{{ $lang == 'fr' ? '299px' : '302px'}}; margin-top:{{ $lang == 'fr' ? '163px' : '168px' }};">
			                          <svg height="60" width="60">
			                            <circle id="vert2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['soi'] == 0 ? 1 : (dates[day].graphe[hours]['soi'] > 0 ? ((dates[day].graphe[hours]['soi'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
	
									<div class="contains text" id="sympathie" style="margin-top:{{ $lang == 'fr' ? '196px' : '208px' }};">
			                          	{% dates[day].graphe[hours]["sympathie"] %}%
			                        </div>
			                        <div class="contains" id="sympathieSVG" style="margin-left:{{ $lang == 'fr' ? '258px' : '260px'}}; margin-top:{{ $lang == 'fr' ? '199px' : '211px' }};">
			                          <svg height="60" width="60">
			                            <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['sympathie'] == 0 ? 1 : (dates[day].graphe[hours]['sympathie'] > 0 ? ((dates[day].graphe[hours]['sympathie'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div> 	

			                        <div class="contains" id="autruiSVG" style="margin-left:{{ $lang == 'fr' ? '299px' : '302px'}}; margin-top:{{ $lang == 'fr' ? '236px' : '243px' }};">
			                          <svg height="60" width="60">
			                            <circle id="rose2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['autrui'] == 0 ? 1 : (dates[day].graphe[hours]['autrui'] > 0 ? ((dates[day].graphe[hours]['autrui'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="autrui" style="margin-left:{{ $lang == 'fr' ? '299px' : '302px'}}; margin-top:{{ $lang == 'fr' ? '286px' : '299px' }};">
			                          {% dates[day].graphe[hours]['autrui'] %}%
			                        </div>

			                        <div class="contains text" id="maitrise" style="margin-left:{{ $lang == 'fr' ? '391px' : '399px'}}; margin-top:{{ $lang == 'fr' ? '195px' : '207px' }};">
			                          {% dates[day].graphe[hours]["maitrise"] %}%
			                        </div>
			                        <div class="contains" id="maitriseSVG" style="margin-left:{{ $lang == 'fr' ? '340px' : '340px'}}; margin-top:{{ $lang == 'fr' ? '199px' : '210px' }};">
			                          <svg height="60" width="60">
			                            <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['maitrise'] == 0 ? 1 : (dates[day].graphe[hours]['maitrise'] > 0 ? ((dates[day].graphe[hours]['maitrise'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div> 														
								</div>
							</div>

							<!-- CLIMAGE RESULTAT 2 MOBILE -->

							<div class="row" id="phone" style="margin-left: -50px;width:330px; height:860px;">
								<div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
                      			<div class="col-sm-12 col-md-6" style="position:relative;">
                      				<!-- <h4 class="text-center"><b>{{ __('message.activite') }}</b></h4> -->
                      				<div style="position:absolute;">
                      					<img src="{{ $lang == "fr" ? url('/img/plg2.png') : url('/img/plg2_en.png') }}" width="75%">	
                      				</div>
                      				<div class="contains text2" id="serieux2" style="margin-left:{{ $lang == 'fr' ? '-34px' : '-43px' }};margin-top:{{ $lang == 'fr' ? '116px' : '89px' }};">
		                            {% dates[day].graphe[hours]["serieux"] %}%
		                          </div>
		                          <div class="contains" id="serieuxSVG2" style="margin-left:{{ $lang == 'fr' ? '-31px' : '-44px' }};margin-top:{{ $lang == 'fr' ? '107px' : '100px' }};">
		                            <svg height="60" width="60">
		                              <circle id="bleu2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['serieux'] == 0 ? 1 : (dates[day].graphe[hours]['serieux'] > 0 ? ((dates[day].graphe[hours]['serieux'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>

			                        <div class="contains text2" id="conforme2" style="margin-left:{{ $lang == 'fr' ? '-109px' : '-125px' }};margin-top:{{ $lang == 'fr' ? '150px' : '132px' }};">
		                            {% dates[day].graphe[hours]["conforme"] %}%
		                          </div>
		                          <div class="contains" id="conformeSVG2" style="margin-left:{{ $lang == 'fr' ? '-67px' : '-79px' }};margin-top:{{ $lang == 'fr' ? '148px' : '135px' }};">
		                            <svg height="60" width="60">
		                              <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['conforme'] == 0 ? 1 : (dates[day].graphe[hours]['conforme'] > 0 ? ((dates[day].graphe[hours]['conforme'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>

			                        <div class="contains" id="enjoueSVG2" style="margin-left:{{ $lang == 'fr' ? '-33px' : '-43px' }};margin-top:{{ $lang == 'fr' ? '180px' : '170px' }};">
		                            <svg height="60" width="60">
		                              <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['enjoue'] == 0 ? 1 : (dates[day].graphe[hours]['enjoue'] > 0 ? ((dates[day].graphe[hours]['enjoue'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>
		                          <div class="contains text2" id="enjoue2" style="margin-left:{{ $lang == 'fr' ? '-30px' : '-41px' }};margin-top:{{ $lang == 'fr' ? '225px' : '218px' }};">
		                            {% dates[day].graphe[hours]['enjoue'] %}%
		                          </div>

                      				<div class="contains" id="transgressifSVG2" style="margin-left:{{ $lang == 'fr' ? '' : '-9px' }};margin-top:{{ $lang == 'fr' ? '148px' : '135px' }};">
		                            <svg height="60" width="60">
		                              <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['transgressif'] == 0 ? 1 : (dates[day].graphe[hours]['transgressif'] > 0 ? ((dates[day].graphe[hours]['transgressif'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>
		                          <div class="contains text2" id="transgressif2" style="margin-left:{{ $lang == 'fr' ? '50px' : '46px' }};margin-top:{{ $lang == 'fr' ? '149px' : '133px' }};">
		                            {% dates[day].graphe[hours]['transgressif'] %}%
		                          </div>

                      			</div>
                      			<div class="col-sm-12 col-md-6" style="position:relative;margin-top:347px;">
                      				<!-- <h4 class="text-center"><b>{{ __('message.relation') }}</b></h4> -->
                      				<div style="position:absolute;">
                      					<img src="{{ $lang == "fr" ? url('/img/pld2.png') : url('/img/pld_en.png') }}" width="75%">	
                      				</div>
									<div class="contains text2" id="soi2" style="margin-left:{{ $lang == 'fr' ? '-52px' : '-44px' }};margin-top:{{ $lang == 'fr' ? '105px' : '83px' }};">
		                            {% dates[day].graphe[hours]["soi"] %}%
		                          </div>
		                          <div class="contains" id="soiSVG2" style="margin-left:{{ $lang == 'fr' ? '-53px' : '-42px' }};margin-top:{{ $lang == 'fr' ? '116px' : '86px' }};">
		                            <svg height="60" width="60">
		                              <circle id="vert2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['soi'] == 0 ? 1 : (dates[day].graphe[hours]['soi'] > 0 ? ((dates[day].graphe[hours]['soi'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>

			                        <div class="contains text2" id="symphatie2" style="margin-left:{{ $lang == 'fr' ? '-130px' : '-126px' }};margin-top:{{ $lang == 'fr' ? '147px' : '129px' }};">
		                            {% dates[day].graphe[hours]["sympathie"] %}%
		                          </div>
		                          <div class="contains" id="symphatieSVG2" style="margin-left:{{ $lang == 'fr' ? '-88px' : '-79px' }};margin-top:{{ $lang == 'fr' ? '147px' : '130px' }};">
		                            <svg height="60" width="60">
		                              <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['sympathie'] == 0 ? 1 : (dates[day].graphe[hours]['sympathie'] > 0 ? ((dates[day].graphe[hours]['sympathie'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>

			                       <div class="contains" id="autruiSVG2" style="margin-left:{{ $lang == 'fr' ? '-53px' : '-44px' }};margin-top:{{ $lang == 'fr' ? '179px' : '162px' }};">
		                            <svg height="60" width="60">
		                              <circle id="rose2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['autrui'] == 0 ? 1 : (dates[day].graphe[hours]['autrui'] > 0 ? ((dates[day].graphe[hours]['autrui'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>
		                          <div class="contains text2" id="autrui2" style="margin-left:{{ $lang == 'fr' ? '-50px' : '-44px' }};margin-top:{{ $lang == 'fr' ? '225px' : '211px' }};">
		                            {% dates[day].graphe[hours]['autrui'] %}%
		                          </div>     				
                      				<div class="contains text2" id="maitrise2" style="margin-left:{{ $lang == 'fr' ? '27px' : '44px' }};margin-top:{{ $lang == 'fr' ? '147px' : '130px' }};">
		                            {% dates[day].graphe[hours]["maitrise"] %}%
		                          </div>
		                          <div class="contains" id="maitriseSVG2" style="margin-left:{{ $lang == 'fr' ? '-19px' : '-10px' }};margin-top:{{ $lang == 'fr' ? '147px' : '129px' }};">
		                            <svg height="60" width="60">
		                              <circle id="gris2" cx="30" cy="30" ng-attr-r="{% dates[day].graphe[hours]['maitrise'] == 0 ? 1 : (dates[day].graphe[hours]['maitrise'] > 0 ? ((dates[day].graphe[hours]['maitrise'] * 15)/75) : 0) %}" stroke-width="3" fill="white" />
		                              Sorry, your browser does not support inline SVG.  
		                            </svg>
		                          </div>         				
							              </div>
							            </div>
												@endif
											</div>
						          <div id="messages2" class="tab-pane">
	                    	@if($climage->type->name == "climage-negociation")
	                    		<div class="row" id="desktop">
														<div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
									          <div class="col-sm-9 container" style="position: relative;height:300px;">
															<div style="position:absolute;width:400px;font-weight:bold;">
																<img src="{{ url('/img/climage4.png') }}" height="300px" id="img_g">
															</div>
															<div style="margin-left:-135px; position: absolute;width: 400px;    font-weight: 600;color: darkgray;">	
																@lang("message.debriefing") 
															</div>
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 140px;margin-left: -83px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_utile2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_utile2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_utile2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
										          </div>
															<div style="position:absolute;font-weight:bold;margin-top: 159px;margin-left: 107px;font-size: 9px;">{% dates[day].climage[hours]["achat_utile2"] %} %</div>
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 140px;margin-left: -28px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_affaire2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_affaire2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_affaire2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
				                    	</div>
															<div style="position:absolute;font-weight:bold;margin-top: 159px;margin-left: 163px;font-size: 9px;">{% dates[day].climage[hours]["achat_affaire2"] %} %</div>
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 196px;margin-left: -83px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_plaisir2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_plaisir2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_plaisir2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
				                    	</div>
															<div style="position:absolute;font-weight:bold;margin-top: 214px;margin-left: 107px;font-size: 9px;">{% dates[day].climage[hours]["achat_plaisir2"] %} %</div>
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 196px;margin-left: -28px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_impulsion2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_impulsion2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_impulsion2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
										          </div>
															<div style="position:absolute;font-weight:bold;margin-top: 214px;margin-left: 163px;font-size: 9px;">{% dates[day].climage[hours]["achat_impulsion2"] %} %</div>
															
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 139px;margin-left: 156px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_bien2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_bien2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_bien2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
										          </div>
															<div style="position:absolute;font-weight:bold;margin-top: 158px;margin-left: 346px;font-size: 9px;">{% dates[day].climage[hours]["achat_bien2"] %} %</div>

															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 139px;margin-left: 210px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_conquete2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_conquete2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_conquete2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
				                    	</div>
															<div style="position:absolute;font-weight:bold;margin-top: 158px;margin-left: 401px;font-size: 9px;">{% dates[day].climage[hours]["achat_conquete2"] %} %</div>
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 196px;margin-left: 155px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_mode2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_mode2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_mode2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
										          </div>
															<div style="position:absolute;font-weight:bold;margin-top: 214px;margin-left: 346px;font-size: 9px;">{% dates[day].climage[hours]["achat_mode2"] %} %
																
															</div>
	
															<div style="position: absolute;width: 400px;font-weight:bold;margin-top: 196px;margin-left: 210px;">
				                        <svg height="60" width="60">
				                          <circle stroke="silver" cx="30" cy="30" ng-attr-r="{% ((dates[day].climage[hours]['achat_marque2']*15)/50) < 12 ? 12 : (((dates[day].climage[hours]['achat_marque2']*15)/50) > 25 ? 25 : ((dates[day].climage[hours]['achat_marque2']*15)/50)) %}" stroke-width="1" fill="white" />
				                          Sorry, your browser does not support inline SVG.  
				                        </svg>
					                    </div>
															<div style="position:absolute;font-weight:bold;margin-top: 214px;margin-left: 401px;font-size: 9px;">{% dates[day].climage[hours]["achat_marque2"] %} %
																
															</div>														
											 			</div>
									        </div>
						            @else
													
													<!-- CLIMAGE RESULTAT 3 SIMPLE WEB -->

													<div class="row" id="desktop" ng-if="dates[day].climage">
						                    				
                  					<div class="col-sm-2 text-right">
			                        Date :
			                      </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
			                    	<div class="col-sm-9 container" style="position: relative;height:390px;    margin-left: 207px">
										          <br/>
										          <div style="position:absolute;width:400px;font-weight:bold;margin-top: 40px;">
																<img src="{{ $lang == "en" ? url('/img/climage_3.png') : url('/img/ptresult4.png') }}" height="350px" id="img_g">
															</div>
															<div class="contains" style="color:#7f7f7f;">
			                          <h4 class="text-left" style="margin-left:180px;font-weight:700;">@lang("message.climat_dom")</h4>
			                        </div>
			                        <div class="contains text" id="vigilance">
			                          {% dates[day].climage[hours]["vigilance"] %}%
			                        </div>
			                        <div class="contains" id="vigilanceSVG">
			                          <svg height="60" width="60">

			                            <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['vigilance'] == 0 ? 1 : (dates[day].climage[hours]['vigilance'] > 0 ? ((dates[day].climage[hours]['vigilance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="depassement">
			                          {% dates[day].climage[hours]["depassement"] %}%
			                        </div>
			                        <div class="contains" id="depassementSVG" style="margin-left:{{ $lang == "en" ? '-38px' : '-41px' }}">
			                          <svg height="60" width="60">

			                            <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['depassement'] == 0 ? 1 : (dates[day].climage[hours]['depassement'] > 0 ? ((dates[day].climage[hours]['depassement']*25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains" id="implicationSVG">
			                          <svg height="60" width="60">

			                            <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['creative'] == 0 ? 1 : (dates[day].climage[hours]['creative'] > 0 ? ((dates[day].climage[hours]['creative'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="implication">
			                          	{% dates[day].climage[hours]["creative"] %}%
			                        </div>

			                        <div class="contains" id="creativeSVG">
			                          <svg height="60" width="60">

			                            <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['implication'] == 0 ? 1 : (dates[day].climage[hours]['implication'] > 0 ? ((dates[day].climage[hours]['implication'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="creative">
			                        	{% dates[day].climage[hours]["implication"] %}%
			                        	
			                        	
			                          
			                        </div>

			                        <div class="contains text" id="reconnaissance">
			                          {% dates[day].climage[hours]["reconnaissance"] %}%
			                        </div>
			                        <div class="contains" id="reconnaissanceSVG">
			                          <svg height="60" width="60">

			                            <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['reconnaissance'] == 0 ? 1 : (dates[day].climage[hours]['reconnaissance'] > 0 ? ((dates[day].climage[hours]['reconnaissance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="competition">
			                          {% dates[day].climage[hours]["competition"] %}%
			                        </div>
			                        <div class="contains" id="competitionSVG">
			                          <svg height="60" width="60">

			                            <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['competition'] == 0 ? 1 : (dates[day].climage[hours]['competition'] > 0 ? ((dates[day].climage[hours]['competition'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains" id="convivialiteSVG" style="margin-left:{{ $lang == "en" ? '187px' : '184px' }}; margin-top:{{ $lang == 'fr' ? '215px' : '215px' }}">
			                          <svg height="60" width="60">

			                            <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['convivialite'] == 0 ? 1 : (dates[day].climage[hours]['convivialite'] > 0 ? ((dates[day].climage[hours]['convivialite'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="convivialite">
			                          {% dates[day].climage[hours]["convivialite"] %}%
			                        </div>
			                        <div class="contains" id="collaborationSVG">
			                          <svg height="60" width="60">

			                            <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['collaboration'] == 0 ? 1 : (dates[day].climage[hours]['collaboration'] > 0 ? ((dates[day].climage[hours]['collaboration'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />

			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="collaboration">
			                          {% dates[day].climage[hours]["collaboration"] %}%
			                        </div>
										        </div>	
													</div>
													
													<!-- RESULTAT 3 CLIMAGE MOBILE -->

									        <div class="row" style="height:810px;" id="phone" ng-if="dates[day].climage">
						                <div class="col-sm-2 text-right">
		                          Date :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="day">
		                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2 text-right">
		                          @lang("message.heure") :
		                        </div>
		                        <div class="col-sm-2">
		                          <select class="custom-select" ng-change="dateChange1()" ng-model="hours">
		                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
		                          </select>
		                        </div>
		                        <div class="col-sm-2">
		                        	@lang("message.repondants") :
		                        </div>
		                        <div class="col-sm-2 text-left">
		                        	{% dates[day].nbRep[hours] %}
		                        </div>
						      	<div class="col-sm-9 container" style="position: relative;height:300px;">
						        	<br/>
						        	<div style="position:absolute;width:400px;font-weight:bold;">
										<img src="{{ $lang == "fr" ? url('/img/ptresult3g.png') : url('/img/climage3g.png') }}" height="300px" style="margin-left: -180px;margin-top: 33px;" id="img_g">
									</div>
									<div class="contains" style="margin-left:20px; color:#7f7f7f;">
			                          	<h4 class="text-left" style=" font-weight:700;">@lang("message.climat_dom")</h4>
			                        </div>
			                        <div class="contains text" id="vigilance2">
			                          {% dates[day].climage[hours]["vigilance"] %}%
			                        </div>
			                        <div class="contains" id="vigilanceSVG2" style="margin-left:{{ $lang == "fr" ? '-109px' : '-116px' }};margin-top:{{ $lang == "fr" ? '135px' : '129px' }};">
			                          <svg height="60" width="60">
			                            <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['vigilance'] == 0 ? 1 : (dates[day].climage[hours]['vigilance'] > 0 ? ((dates[day].climage[hours]['vigilance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="depassement2">
			                          {% dates[day].climage[hours]["depassement"] %}%
			                        </div>
			                        <div class="contains" id="depassementSVG2" style="margin-left:{{ $lang == "fr" ? '67px' : '-67px' }};margin-top:{{ $lang == "fr" ? '135px' : '130px' }};">
			                          <svg height="60" width="60">
			                            <circle id="bleu" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['depassement'] == 0 ? 1 : (dates[day].climage[hours]['depassement'] > 0 ? ((dates[day].climage[hours]['depassement'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains" id="implicationSVG2" style="margin-left:{{ $lang == "fr" ? '-111px' : '-117px' }};">
			                          <svg height="60" width="60">
			                            <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['creative'] == 0 ? 1 : (dates[day].climage[hours]['creative'] > 0 ? ((dates[day].climage[hours]['creative'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="implication2">
			                          {% dates[day].climage[hours]["creative"] %}%
			                        </div>
			                        <div class="contains" id="creativeSVG2">
			                          <svg height="60" width="60">
			                            <circle id="jaune" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['implication'] == 0 ? 1 : (dates[day].climage[hours]['implication'] > 0 ? ((dates[day].climage[hours]['implication'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="creative2">
			                        {% dates[day].climage[hours]["implication"] %}%
			                          
			                        </div>  
							              </div>

								    <div class="col-sm-9 container" style="position: relative;height:350px;">
								        <br/>
							        <div style="position:absolute;width:400px;font-weight:bold;">
										<img src="{{ $lang == "fr" ? url('/img/ptresult3d.png') : url('/img/climage3d.png') }}" style="margin-left: -180px;margin-top: 28px;" height="300px" id="img_d">
									</div>
									<div class="contains text" id="reconnaissance2">
			                          {% dates[day].climage[hours]["reconnaissance"] %}%
			                        </div>
										          <div class="contains" id="reconnaissanceSVG2" style="margin-left:{{ $lang == "fr" ? '-111px' : '-115px' }};">
			                          <svg height="60" width="60">
			                            <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['reconnaissance'] == 0 ? 1 : (dates[day].climage[hours]['reconnaissance'] > 0 ? ((dates[day].climage[hours]['reconnaissance'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="competition2">
			                          {% dates[day].climage[hours]["competition"] %}%
			                        </div>
			                        <div class="contains" id="competitionSVG2">
			                          <svg height="60" width="60">
			                            <circle id="rose" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['competition'] == 0 ? 1 : (dates[day].climage[hours]['competition'] > 0 ? ((dates[day].climage[hours]['competition'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains" id="convivialiteSVG2" style="margin-left:{{ $lang == "fr" ? '-108px' : '-115px' }};margin-top:{{ $lang == 'fr' ? '172px' : '176px' }}">
			                          <svg height="60" width="60">
			                            <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['convivialite'] == 0 ? 1 : (dates[day].climage[hours]['convivialite'] > 0 ? ((dates[day].climage[hours]['convivialite'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="convivialite2">
			                          {% dates[day].climage[hours]["convivialite"] %}%
			                        </div>
			                        <div class="contains" id="collaborationSVG2" style="margin-left:{{ $lang == "fr" ? '-66px' : '-70px' }};">
			                          <svg height="60" width="60">
			                            <circle id="vert" cx="30" cy="30" ng-attr-r="{% dates[day].climage[hours]['collaboration'] == 0 ? 1 : (dates[day].climage[hours]['collaboration'] > 0 ? ((dates[day].climage[hours]['collaboration'] * 25)/50) : 0) %}" stroke-width="3" fill="white" />
			                            Sorry, your browser does not support inline SVG.  
			                          </svg>
			                        </div>
			                        <div class="contains text" id="collaboration2">
			                          {% dates[day].climage[hours]["collaboration"] %}%
			                        </div>
			                    	</div>	
						              </div>
												@endif
						          </div>

	                    <div class="tab-pane" id="trombinoscope">
	                      <div class="row">
	                      	@foreach($climage->testRepondants as $rep)
	                      		<div class="col-sm-12 col-md-4">
                      				<div class="card">
                      					@if(Auth::user()->id == $rep->user->id || Auth::user()->role == "consultant" || Auth::user()->role == "admin")
	                      					<div class="card-body text-right" style="padding:0;cursor:context-menu;" data-toggle="modal" data-target="#delete-climageUser" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)">
	                      						<i class="material-icons">delete_outline</i>
	                      					</div>	
							                  @endif
							                  <div class="card-body row">
														  		<div class="col-sm-12 text-center">
														  			@if($rep->user->avatar_url)
						                					<img style="height: 70px; width: 70px"  id="imageSrc" src="{{ $rep->user->avatar_url }}" alt="Avatar" class="rounded-circle">
						                				@else
						                					<img style="height: 70px; width: 70px"  class="rounded-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
						                				@endif 
														  		</div>
														  		<div class="col-sm-12">
														  			{{ $rep->user->firstname }} {{ $rep->user->lastname }}<br>
														  			{{ $rep->user->email }} <br>

														  		</div>
														  		<div class="col-sm-12 text-center">
														  			<button class="btn btn-warning btn-round" data-toggle="modal" data-target="#profil" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)" >PROFILE</button>
														  		</div>
																</div>
															</div>
														</div>
						              @endforeach
							                      			
						              <div class="col-sm-12">
						                <a href="{{ route('diagnostic') }}" class="btn btn-round btn-warning">
															@lang('message.retour')     	
														</a>
						              </div>
						            </div>
						          </div>               
						        </div>
						     	</div>
						    </div>
							</div>
						</div>
					</div>
				</div>
			</div>

	<!-- Modal -->
	<div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
			      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
		        	<h5 class="modal-title" id="exampleModalLabel">
		        		
		        	</h5>
	        
	      		</div>
		      	<div class="modal-body">
		      				<div class="row">
		      					<div class="col-sm-12 text-center">
						  				<img ng-if="user.avatar_url" style="height: 70px; width: 70px"  id="imageSrc" src="{% user.avatar_url %}" alt="Avatar" class="rounded-circle">
                                             
                                        <img ng-if="!user.avatar_url" style="height: 70px; width: 70px"  class="img-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
			        		
						  		</div>
						  		<div class="col-sm-12 text-center">

						  			<b>{% user.firstname %} {% user.lastname %} </b>
						  			<br>
						  			{% user.email %}
						  			<p class="text-secondary">{% user.organisation %}</p>

						  		</div>
		      				</div>
		      				<div ng-if="reponses.length > 0">
		      					<div class="card" ng-repeat="reponse in reponses">
								  	<div class="card-body row">
								  		
								  		
								    	<div class="col-sm-12">
								    		<p><b> <i class="large material-icons">chevron_right</i> {% profil[reponse.question_id]['{{ $lang }}'] %}</b></p>
								    		<p>{% reponse.reponse_text %}</p>
								    	</div>
								    
								  	</div>
								</div>
		      				</div>
		      				<div ng-if="reponses.length < 1">
		      					<div class="card">
								  	<div class="card-body row">
								    	<div class="col-sm-12">
								    		<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My personnal Motto" : "Mon slogan personnel" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>								    		
								    	</div>
								  	</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "What makes me proud" : "Ma plus grande fierté" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My biggest challenge" : "Mon plus grand défi" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My main interests" : "Mes centres d\' intérêts" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
		      				</div>			   
		    
		      	</div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
	</div>

	<div class="modal fade" id="delete-climageUser">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4>Suppression</h4>
	      			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
		      	<div class="modal-body">			   
		    	@lang("message.infoClimage")
		      	</div>
			    <div class="modal-footer">
			        <button type="button" ng-click="deleteInfo()" class="btn btn-danger" data-dismiss="modal">@lang('message.supprimer')</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
    </div>


	

</div>

	

<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: inherit !important;
	}

	.reverse{
		-moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        filter: FlipH;
        -ms-filter: "FlipH";
	}

	.progress{
		height: 10px;
	}

	.btn-link{
		color: #FFB301 !important;
	}
</style>
<script type="text/javascript">
	
	function affiche(text){
		console.log(text);
		return  text;
	}

	$(function() {

	    window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });

	    //@if($data)

	    	//le petit carre milieu
	    	//var c = document.getElementById("myCanvas");
		  	//var ctx = c.getContext("2d");
		  	//ctx.fillStyle = "rgba(179,181,198,1)";
		  	//ctx.fillRect(23, 18, 164, 23);

		//@endif
		});

</script>
 <script type="text/javascript">
      var climageFrontApp = angular.module('climageFrontApp', ['chart.js'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      climageFrontApp.controller('climageFrontController', function PhoneListController($scope,$http) {
           $scope.user = {};
           $scope.profil = [];

           $scope.dates = {!! json_encode($dates) !!};

        if($scope.dates){
          $scope.stringD = ($scope.dates.length-1).toString();
          //$scope.stringH = ($scope.dates[$scope.stringD].heures.length-1).toString();
          $scope.day =  $scope.dates[0] ? $scope.stringD : undefined;
          $scope.hours = $scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined;
          console.log($scope.dates);
        }

      $scope.dateChange1 = function(){
        //$scope.hours = $scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined;     

        if(!$scope.hours){
          $scope.hours = "0";  
        }
        //$scope.hours == null ? ($scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined) : undefined;

        $scope.dayH = $scope.dates[$scope.day].vote[$scope.hours]
        console.log($scope.dayH);

        $scope.data1 = [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];

	    	$scope.data2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];

	    	$scope.labels1 =[$scope.dates[$scope.day].graphe[$scope.hours]["serieux"]+"%","                       "+$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]+"%                        "];

      		$scope.labels2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"]+"%","                      "+$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["autrui"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]+"%                        "];
      		$scope.data3 = [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];

	        $scope.data4 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];

	        $scope.data5 = [$scope.dates[$scope.day].climage[$scope.hours]["serieux"],$scope.dates[$scope.day].climage[$scope.hours]["transgressif"],$scope.dates[$scope.day].climage[$scope.hours]["enjoue"],$scope.dates[$scope.day].climage[$scope.hours]["conforme"]];

	        $scope.data6 = [$scope.dates[$scope.day].climage[$scope.hours]["soi"],$scope.dates[$scope.day].climage[$scope.hours]["maitrise"],$scope.dates[$scope.day].climage[$scope.hours]["autrui"],$scope.dates[$scope.day].climage[$scope.hours]["sympathie"]];

	        $scope.labels3 =[$scope.dates[$scope.day].graphe[$scope.hours]["serieux"]+"%","                       "+$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]+"%                        "];

	        $scope.labels5 =['','','',''];

	        $scope.labels4 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"]+"%","                      "+$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["autrui"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]+"%                        "];

      }

      $scope.dateChange1();

      $scope.options2 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }
         $scope.options3 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }
          $scope.options4 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }
      $scope.options1 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }

           let temp = "<?php echo htmlspecialchars(json_encode($questionProfil)) ?>";
           $scope.profil = JSON.parse(temp.replace(/&quot;/g,'"'));

           $scope.reponse = [];

           $scope.page = function(page){
            	window.location = page;
            }
           $scope.showProfil = function(user, reponses){
           		$scope.user = user;
           		console.log(user);
           		
           		console.log($scope.profil);
           		$scope.reponses = reponses.filter(function(item){
           			return item.reponse_text != null
           		});
           		console.log($scope.reponses);


           }

           $scope.deleteInfo = function(){

		        $http({
		          method: 'POST',
		          data:{"idClimage": '{{ $climage->id }}', "user":$scope.user},
		          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/diagnostic/climage/deleteInfoClimage"
		        }).then(function successCallback(response) {
		          if(response.data.status == 1){
		            location.reload();
		          }
		        }, function errorCallback(response) {

		          //console.log(response);
		        });

	      	}


      });


    </script>
<!--/div-->
@endsection('content')


  @extends('diagnostic')
  @section('content')
<div ng-app="climageFrontApp" ng-controller="climageFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">


			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('climage-front') }}">{{ __('message.climage') }}</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $climage->name }} </span> </h2>
						<hr>
						<input type="hidden" name="" value="{{ $lang == "en" ? "The activities" : "L'activité" }}" id="text_parse">




						<div class="btn-group btn-group-toggle" style="font-family: Roboto,sans serif; display: block;">
							@foreach($pages as $p)
								
								<label ng-click="page('{{ $p->url }}')" class="btn btn-secondary">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $p->{'title_'.$lang} }}
								</label>
								
								
							@endforeach
								<label class="btn btn-secondary active">
								    <input type="radio" name="options" id="option1" autocomplete="off" checked> {{ $lang == "en" ? "Results"  : "Résultats"}}
								</label>

						  
						</div>
					
						</div>
				
							<div class="row">
		

							  <div class="col-md-12" id="mobile_padding">
							      <div class="card card-nav-tabs">
						                <div class="card-header card-header-warning">
						                  <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
						                  <div class="nav-tabs-navigation">
						                    <div class="nav-tabs-wrapper">
						                      <ul class="nav nav-tabs" data-tabs="tabs">
						                        <li class="nav-item">
						                          <a class="nav-link active show" href="#profile" data-toggle="tab">
						                            <i class="material-icons">bar_chart</i> {{ __('message.resultat') }} 1
						                          <div class="ripple-container"></div></a>
						                        </li>
						                        <li class="nav-item">
						                          <a class="nav-link" href="#messages" data-toggle="tab">
						                            <i class="material-icons">bar_chart</i> {{ __('message.resultat2') }}
						                          <div class="ripple-container"></div></a>
						                        </li>

						                         <li class="nav-item">
						                          <a class="nav-link" href="#trombinoscope" data-toggle="tab">
						                            <i class="material-icons">face</i> @lang('message.trombinoscope')
						                          <div class="ripple-container"></div></a>
						                        </li>
						                        
						                      </ul>
						                    </div>
						                  </div>
						                </div>
						                <div class="card-body ">

						                  <div class="tab-content text-center">
						                    <div class="tab-pane active show" id="profile">
						                      
						                    		<div class="row">
														<div class="col-sm-2 text-right">
								                          Date :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select" ng-model="day">
								                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
								                          </select>
								                        </div>
								                        <div class="col-sm-2 text-right">
								                          Heures :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select" ng-model="hours">
								                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
								                          </select>
								                        </div>
								                        <div ng-if="day!=undefined">
						                    			@foreach($page2->questions as $q=>$question)
							                    			@if($q==0)
								                    			<div class="col-sm-12">
								                    				<h3>{{ $question->{'titre_'.$lang} }}</h3>
								                    			</div>
							                    			
																<div class="col-xs-12 row">
								                    				@foreach($question->reponses as $key=>$reponse)
									                    				<div class="col-xs-6 col-md-6">
									                    					
									                    					@if($loop->iteration  % 2 == 0)
																		        <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
																		        	<br>
																		        	<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		        </p>
									                    						<div class="progress" style="height: 20px;">
																					<div class="progress-bar

																					
																					" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#2196f3' : ($key == 3 ? '#2196f3' : ($key == 5 ? '#ffc107' : ($key == 7 ? '#ffc107' : '' )))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																					<span class="sr-only">30% Complete</span>
																					</div>
																				</div>
																				<br>
																		    @else
																		    	<p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
																		    		<br>
																		    		<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		    	</p>
																		    	<div class="progress reverse" style="height: 20px;">
																					<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#2196f3" : (($key == 2)? "#2196f3" : (($key == 4)? "#ffc107" : (($key == 6)? "#ffc107" : ""))))}};width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																					<span class="sr-only">30% Complete</span>
																					</div>
																				</div>

																		    @endif
									                    					
									                    					
									                    				</div>
								                    				@endforeach
								                    				
								                    			</div>
							                    			@endif
							                    			@if($q==1)
								                    			<div class="col-sm-12">
								                    				<h3>{{ $question->{'titre_'.$lang} }}</h3>
								                    			</div>
							                    			
																<div class="col-xs-12 row">
								                    				@foreach($question->reponses as $key=>$reponse)
									                    				<div class="col-xs-6 col-md-6">
									                    					
									                    					@if($loop->iteration  % 2 == 0)
																		        <p class="text-left">{{ $reponse->{'reponse_'.$lang} }}
																		        	<br>
																		        	<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		        </p>
									                    						<div class="progress" style="height: 20px;">
																					<div class="progress-bar

																					
																					" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 1 ? '#e91e63' : ($key == 3 ? '#e91e63' : ($key == 5 ? '#4caf50' : ($key == 7 ? '#4caf50' : '' )))))}}; width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																					<span class="sr-only">30% Complete</span>
																					</div>
																				</div>
																				<br>
																		    @else
																		    	<p class="text-right">{{ $reponse->{'reponse_'.$lang} }}
																		    		<br>
																		    		<span>{% dates[day].vote[hours]["{{$key}}"]["{{$question->id}}"] %} %</span>
																		    	</p>
																		    	<div class="progress reverse" style="height: 20px;">
																					<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color:{{(($key == 0)? "#e91e63" : (($key == 2)? "#e91e63" : (($key == 4)? "#4caf50" : (($key == 6)? "#4caf50" : ""))))}};width:{% dates[day].vote[hours]['{{$key}}']['{{$question->id}}'] %}%">
																					<span class="sr-only">30% Complete</span>
																					</div>
																				</div>

																		    @endif
									                    					
									                    					
									                    				</div>
								                    				@endforeach
								                    				
								                    			</div>
							                    			@endif
							                    			
							                    			<hr>
							                    		@endforeach
							                    	</div>
						                    		</div>

						                    </div>
						                    <div class="tab-pane" id="messages">
						                      		<div class="row" id="desktop">
														<div class="col-sm-2 text-right">
								                          Date :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select"  ng-change="dateChange()" ng-model="day">
								                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
								                          </select>
								                        </div>
								                        <div class="col-sm-2 text-right">
								                          Heures :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select" ng-change="dateChange()" ng-model="hours">
								                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
								                          </select>
								                        </div>
						                      			<div class="col-sm-12 col-md-6" id="gauche">
						                      				<h4 class="text-center" style="margin-left:40px;"><b>{{ __('message.activite') }}</b></h4>
						                      				<img src="{{ url('/img/vg.png') }}" width="55%" style="margin-left:40px;">
															<canvas id="doughnut-chartcanvas-1" style="display: block; position:absolute; top:1px;"></canvas>
															<!-- <canvas id="radar1" width="310" height="110" style="display: block; margin-left:20px;margin-top: 80px;"></canvas> -->
															<canvas id="radar1" class="chart chart-radar" chart-data="data1" chart-options="options1" chart-labels="labels1" width="310" height="110" style="display: block; margin-left:20px;margin-top: 80px;"> </canvas>
															<canvas id="myCanvas2" width="80" height="50" style="position: absolute;right:72.5%;top: 43%;"></canvas>
															
						                      			</div>
						                      			<div class="col-sm-12 col-md-6" id="droite">
						                      				<h4 class="text-center" style="margin-left:-40px;"><b>{{ __('message.relation') }}</b></h4>
						                      				<img src="{{ url('/img/vd.png') }}" width="55%" style="margin-left:-45px;">
						                      				<canvas id="doughnut-chartcanvas-2" style="display: block; position:absolute; top:1px;"></canvas>
						                      				<canvas id="myCanvas3"width="80" height="50" style="left: 62.5%;top: 43%;position: absolute;"></canvas>
												      		<!-- <canvas id="radar2" width="310" height="110" style="display: block; margin-left: -20px;margin-top:80px;"></canvas> -->
												      		<!-- <canvas id="radar2" width="310" height="110"></canvas> -->
                        <canvas id="radar2" class="chart chart-radar" chart-data="data2" chart-options="options2" chart-labels="labels2" width="310" height="110" style="display: block; margin-left: -20px;margin-top:80px;"> </canvas>

						                      			</div>
						                      			<div class="col-sm-12 col-md-6" style="position: absolute;left: 24.2%;bottom: 38.8%;">
						                      				<canvas id="myCanvas" style="display:none"></canvas>
						                      				<img id="lunette" style="height: 55px; width: 132px;border-radius:30px;margin-bottom:-10px;" src="{{ url('/img/1.jpg') }}">
						                      			</div>
						                      		</div>

													<div class="row" id="phone">
														<div class="col-sm-2 text-right">
								                          Date :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select" ng-model="day">
								                            <option ng-repeat="(key, value) in dates" value="{% key %}">{% value.date %}</option>
								                          </select>
								                        </div>
								                        <div class="col-sm-2 text-right">
								                          Heures :
								                        </div>
								                        <div class="col-sm-3">
								                          <select class="custom-select" ng-model="hours">
								                            <option ng-repeat="(key_h, value) in dates[day].heures" value="{% key_h %}">{% value %}</option>
								                          </select>
								                        </div>
						                      			<div class="col-sm-12 col-md-6">
						                      				<h4 class="text-center"><b>{{ __('message.activite') }}</b></h4>
						                      				<img src="{{ url('/img/vg.png') }}" width="75%">
												      		<canvas id="radar3" width="400" height="135" style="left:-80px;margin-top:2px;position:absolute;"></canvas>
						                      			</div>
						                      			<div class="col-sm-12 col-md-6">
						                      				<h4 class="text-center"><b>{{ __('message.relation') }}</b></h4>
						                      				<img src="{{ url('/img/vd.png') }}" width="75%">
												      		<canvas id="radar4" width="400" height="135" style="left:-80px;margin-top:2px;position:absolute;"></canvas>
						                      			</div>
						                      		</div>

						                    </div>

						                    <div class="tab-pane" id="trombinoscope">
						                      		<div class="row">
						                      			@foreach($climage->testRepondants as $rep)
						                      			
						                      				<div class="col-sm-12 col-md-4">
							                      				<div class="card">
							                      					@if(Auth::user()->id == $rep->user->id || Auth::user()->role == "consultant" || Auth::user()->role == "admin")
								                      					<div class="card-body text-right" style="padding:0;cursor:context-menu;" data-toggle="modal" data-target="#delete-climageUser" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)">
								                      						<i class="material-icons">delete_outline</i>
								                      					</div>	
							                      					@endif
							                      					
																	<div class="card-body row">
																	  		<div class="col-sm-12 text-center">
																	  			@if($rep->user->avatar_url)
												                					<img style="height: 70px; width: 70px"  id="imageSrc" src="{{ $rep->user->avatar_url }}" alt="Avatar" class="rounded-circle">
												                				@else
												                					<img style="height: 70px; width: 70px"  class="rounded-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
												                				@endif 
																	  		</div>
																	  		<div class="col-sm-12">
																	  			{{ $rep->user->firstname }} {{ $rep->user->lastname }}<br>
																	  			{{ $rep->user->email }} <br>

																	  		</div>
																	  		<div class="col-sm-12 text-center">
																	  			<button class="btn btn-warning btn-round" data-toggle="modal" data-target="#profil" ng-click="showProfil(<?php echo htmlspecialchars(json_encode($rep->user)) ?>,<?php echo htmlspecialchars(json_encode($rep->user->reponseRepondants()->where('test_id', $climage->id)->get() )) ?>)" >PROFILE</button>
																	  		</div>
																	    	
																	   
																	  </div>
																</div>

							                      			</div>
						                      			@endforeach
							                      			
						                      		<div class="col-sm-12">
						                      			<a href="{{ route('diagnostic') }}" class="btn btn-round btn-warning">
															
												                @lang('message.retour')
												              
												          	
														</a>
						                      		</div>
						                      		</div>
						                    </div>
						                   
						                  </div>
						                </div>
						              </div>


							  </div>
							</div>
						    
						
						<!-- <h2 class="inscription">Accueil <i class="material-icons">chevron_right</i> Climage <i class="material-icons">chevron_right</i> </h2> -->
					</div>
		
			</div>
					

	
		</div>
	

	<!-- Modal -->
	<div class="modal fade" id="profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
			      	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
		        	<h5 class="modal-title" id="exampleModalLabel">
		        		
		        	</h5>
	        
	      		</div>
		      	<div class="modal-body">
		      				<div class="row">
		      					<div class="col-sm-12 text-center">
						  				<img ng-if="user.avatar_url" style="height: 70px; width: 70px"  id="imageSrc" src="{% user.avatar_url %}" alt="Avatar" class="rounded-circle">
                                             
                                        <img ng-if="!user.avatar_url" style="height: 70px; width: 70px"  class="img-circle" id="imageSrc" src="{{ url('/img/avatar.png') }}">
			        		
						  		</div>
						  		<div class="col-sm-12 text-center">

						  			<b>{% user.firstname %} {% user.lastname %} </b>
						  			<br>
						  			{% user.email %}
						  			<p class="text-secondary">{% user.organisation %}</p>

						  		</div>
		      				</div>
		      				<div ng-if="reponses.length > 0">
		      					<div class="card" ng-repeat="reponse in reponses">
								  	<div class="card-body row">
								  		
								  		
								    	<div class="col-sm-12">
								    		<p><b> <i class="large material-icons">chevron_right</i> {% profil[reponse.question_id]['{{ $lang }}'] %}</b></p>
								    		<p>{% reponse.reponse_text %}</p>
								    	</div>
								    
								  	</div>
								</div>
		      				</div>
		      				<div ng-if="reponses.length < 1">
		      					<div class="card">
								  	<div class="card-body row">
								    	<div class="col-sm-12">
								    		<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My personnal Motto" : "Mon slogan personnel" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>								    		
								    	</div>
								  	</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "What makes me proud" : "Ma plus grande fierté" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My biggest challenge" : "Mon plus grand défi" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
								<div class="card">
									<div class="card-body row">
										<div class="col-sm-12">
											<p><b> <i class="large material-icons">chevron_right</i> {% '{{ $lang == "en" ? "My main interests" : "Mes centres d\' intérêts" }}' %}</b></p>
								    		<p>{{ $lang == "en" ? "To fill" : "à remplir" }}</p>
								    	</div>
									</div>
								</div>
		      				</div>			   
		    
		      	</div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
	</div>

	<div class="modal fade" id="delete-climageUser">
		<div class="modal-dialog" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4>Suppression</h4>
	      			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
		      	<div class="modal-body">			   
		    	@lang("message.infoClimage")
		      	</div>
			    <div class="modal-footer">
			        <button type="button" ng-click="deleteInfo()" class="btn btn-danger" data-dismiss="modal">@lang('message.supprimer')</button>
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
			        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">Participer</button> -->
			    </div>
		    </div>
	  	</div>
    </div>


	

</div>

	

<style type="text/css">
	.btn-reponse{
	    white-space:normal !important;
	    width:100% !important;
	    font-family: inherit !important;
	}

	.reverse{
		-moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
        filter: FlipH;
        -ms-filter: "FlipH";
	}

	.progress{
		height: 10px;
	}

	.btn-link{
		color: #FFB301 !important;
	}
</style>
<script type="text/javascript">
	
	function affiche(text){
		console.log(text);
		return  text;
	}

	$(function() {

	    window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });

	    @if($data)

	    	//le petit carre milieu
	    	var c = document.getElementById("myCanvas");
		  	var ctx = c.getContext("2d");
		  	ctx.fillStyle = "rgba(179,181,198,1)";
		  	ctx.fillRect(23, 18, 164, 23);

		@endif
		});

</script>
 <script type="text/javascript">
      var climageFrontApp = angular.module('climageFrontApp', ['chart.js'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('{%');
        $interpolateProvider.endSymbol('%}');
      });
      climageFrontApp.controller('climageFrontController', function PhoneListController($scope,$http) {
           $scope.user = {};
           $scope.profil = [];

           $scope.dates = {!! json_encode($dates) !!};
	      if($scope.dates){
	      	$scope.day =  $scope.dates[0] ? "0" : undefined;
		    $scope.hours = $scope.dates[0] ? ($scope.dates[0].heures[0] ? "0" : undefined) : undefined;
		    console.log($scope.dates[$scope.day].vote[$scope.hours]);
	      }



	    $scope.dateChange = function(){
	    	$scope.data1 = [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];

	    	$scope.data2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];
	    }
	    
	    $scope.dateChange();

      $scope.grapheData = function(){
        return [$scope.dates[$scope.day].graphe[$scope.hours]["serieux"],$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"],$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"],$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]];
      }

      $scope.grapheData2 = function(){
        return [$scope.dates[$scope.day].graphe[$scope.hours]["soi"],$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"],$scope.dates[$scope.day].graphe[$scope.hours]["autrui"],$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]];
      }

      $scope.labels1 =[$scope.dates[$scope.day].graphe[$scope.hours]["serieux"]+"%","                       "+$scope.dates[$scope.day].graphe[$scope.hours]["transgressif"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["enjoue"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["conforme"]+"%                        "];

      $scope.labels2 = [$scope.dates[$scope.day].graphe[$scope.hours]["soi"]+"%","                      "+$scope.dates[$scope.day].graphe[$scope.hours]["maitrise"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["autrui"]+"%",$scope.dates[$scope.day].graphe[$scope.hours]["sympathie"]+"%                        "];

      $scope.options2 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }
      $scope.options1 = {
            scale: {
               ticks: {
                  display: false,
                  maxTicksLimit: 1,
                  min:0,
                  max:100
               },
                pointLabels :{
                      fontStyle: "bold",
                      fontSize:11
                  }
            },
            gridLines: {
              display: false
            },
          }

           let temp = "<?php echo htmlspecialchars(json_encode($questionProfil)) ?>";
           $scope.profil = JSON.parse(temp.replace(/&quot;/g,'"'));

           $scope.reponse = [];

           $scope.page = function(page){
            	window.location = page;
            }
           $scope.showProfil = function(user, reponses){
           		$scope.user = user;
           		console.log($scope.user);
           		
           		console.log($scope.profil);
           		$scope.reponses = reponses.filter(function(item){
           			return item.reponse_text != null
           		});
           		console.log($scope.reponses);


           	}

           	$scope.deleteInfo = function(){

		        $http({
		          method: 'POST',
		          data:{"idClimage": '{{ $climage->id }}', "user":$scope.user},
		          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		          url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/diagnostic/climage/deleteInfoClimage"
		        }).then(function successCallback(response) {
		          if(response.data.status == 1){
		            location.reload();
		          }
		        }, function errorCallback(response) {

		          //console.log(response);
		        });

	      	}


      });


    </script>
<!--/div-->
@endsection('content')

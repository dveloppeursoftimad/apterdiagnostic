 {{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
      <h1>
        <span class="glyphicon glyphicon-stats"></span>
        Climage
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> Climage</a></li>
        <li class="active">@lang('message.questionnaire')</li>
      </ol>
@stop
	
@section('content')
<div ng-app="questionApp" ng-controller="questionController">
  
    @if(Session::has('message'))
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Succès!</h4>
                {{ Session::get('message') }}
              </div>
    
    @endif
    <div class="row">
        <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              @if(Session::has('message'))
               <p style="display: none;">{{$active=Session::get('active')}}</p>
              @endif
              <?php if(!isset($active)) $active ='formation';?>
              <li class="{{$active=='formation'? 'active':''}}" ng-click="setActives('formation')"><a href="#formation" data-toggle="tab">Formation</a></li>
              <li class="{{$active=='reunion'? 'active':''}}" ng-click="setActives('reunion')"><a href="#reunion" data-toggle="tab">@lang('message.reunion')</a></li>
              <li class="{{$active=='transformation'? 'active':''}}" ng-click="setActives('transformation')"><a href="#transformation" data-toggle="tab">Transformation</a></li>
              <li class="{{$active=='negociation'? 'active':''}}" ng-click="setActives('negociation')"><a href="#negociation" data-toggle="tab">@lang('message.negociation')</a></li>
            
            </ul>
            <div class="tab-content">
           
              <div class="tab-pane {{$active=='formation'? 'active':''}}" id="formation">
                <section id="new">
                    <h4 class="page-header">@lang('message.questionnaire') Formation</h4>

                    @foreach ($formation->pages as $page)

                        <div class="box collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title" data-widget="collapse">{{ $page->title_fr }}</h3> - 
                              <h3 class="box-title" data-widget="collapse">{{ $page->title_en }}</h3>
                              <div class="box-tools pull-right">
                                <!-- Buttons, labels, and many other things can be placed here! -->
                                <!-- Here is a label for example -->
                                <!-- <span class="label label-primary">Modifier</span> -->
                                @if(Auth::user()->role !== 'admin_acad')
                                  <button type="button" class="btn btn-box-tool" ng-click="editPage(<?php echo htmlspecialchars(json_encode($page)) ?>)" data-toggle="modal" data-target="#edit-page">
                                    <i class="fa fa-edit"></i>
                                  </button>
                                @endif
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                  <i class="fa fa-plus"></i>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                @foreach ($page->questions as $question)
                                    <div class="box">
                                      <!-- <div class="box-header with-border"> -->
                                        <!-- <h3 class="box-title">{{ $question->titre }} </h3> -->
                                        <!-- <div class="box-tools pull-right"> -->
                                          <!-- Buttons, labels, and many other things can be placed here! -->
                                          <!-- Here is a label for example -->
                                          <!-- <span class="label label-default">Modifier</span> -->
                                        <!-- </div> -->
                                        <!-- /.box-tools -->
                                      <!-- </div> -->
                                      <!-- /.box-header -->
                                      <div class="box-body row">
                                        <div class="col-sm-2">
                                              <h4 class="text-center text-secondary">{{ $question->titre_fr }} </h4>
                                        </div>
                                        <div class="col-sm-8">
                                              <p><b>{{ $question->question_fr }}</b></p>
                                              <p><b>{{ $question->question_en }}</b></p>
                                              
                                              <p class="text-secondary"> @lang('message.reponse') : {{ $question->type }} 

                      
                                              </p>

                                        </div>
                                        @if(Auth::user()->role !== 'admin_acad')
                                          <div class="col-sm-2 text-right">
                                            <button class="btn" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>)" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit fa-2x"></i> </button>
                                          </div>
                                        @endif

                                        <div class="col-sm-12">
                                            @if (count($question->reponses) >0)
                                                <div class="box box-warning">
                                                  <div class="box-header with-border" data-widget="collapse">
                                                    <h3 class="box-title">@lang('message.reponses')</h3>
                                                    <div class="box-tools pull-right">
                                                      <button type="button" class="btn btn-box-tool">
                                                        <i class="fa fa-plus"></i>
                                                      </button>
                                                      <!-- Buttons, labels, and many other things can be placed here! -->
                                                      <!-- Here is a label for example -->
                                                      <!-- <span class="label label-primary">Label</span> -->
                                                    </div>
                                                    <!-- /.box-tools -->
                                                  </div>
                                                  <!-- /.box-header -->
                                                 <!--  <div class="box-body row">
                                                    The body of the box
                                                  </div> -->
                                                  <!-- /.box-body -->
                                                  <div class="box-footer no-padding">
                                                      <ul class="nav nav-stacked">
                                                        @foreach ($question->reponses as $reponse)
                                                            <li>
                                                              <a > {{ $reponse->reponse_fr }} - {{ $reponse->reponse_en }}
                                                              @if(Auth::user()->role !== 'admin_acad')  
                                                                <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($reponse)) ?>)" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button>
                                                              @endif
                                                            </a>

                                                            </li>
                                                        @endforeach
                                                        
                                                      </ul>
                                                  </div>
                                                  <!-- box-footer -->
                                                </div>
                                                <!-- /.box -->
                                            @endif
                                        </div>

                                        
                                      </div>
                                      <!-- /.box-body -->
                                      <!-- <div class="box-footer"> -->
                                        <!-- The footer of the box -->
                                      <!-- </div> -->
                                      <!-- box-footer -->
                                    </div>
                                    <!-- /.box -->
                                @endforeach



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <!-- The footer of the box -->
                            </div>
                          </div>
                       
                    @endforeach
                          

                </section>
              </div>
            

              <div class="tab-pane {{$active=='reunion'? 'active':''}}" id="reunion">
                <section id="new">
                    <h4 class="page-header">@lang('message.questionnaire') @lang('message.reunion')</h4>

                    @foreach ($reunion->pages as $page)

                        <div class="box collapsed-box">
                            <div class="box-header with-border" data-widget="collapse">
                              <h3 class="box-title">{{ $page->title_fr }}</h3> - 
                              <h3 class="box-title">{{ $page->title_en }}</h3>
                              <div class="box-tools pull-right">
                                <!-- Buttons, labels, and many other things can be placed here! -->
                                <!-- Here is a label for example -->
                                <!-- <span class="label label-primary">Modifier</span> -->
                                @if(Auth::user()->role !== 'admin_acad')
                                  <button type="button" class="btn btn-box-tool">
                                    <i class="fa fa-edit"></i>
                                  </button>
                                @endif
                                <button type="button" class="btn btn-box-tool">
                                  <i class="fa fa-plus"></i>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                @foreach ($page->questions as $question)
                                    <div class="box">
                                      <!-- <div class="box-header with-border"> -->
                                        <!-- <h3 class="box-title">{{ $question->titre }} </h3> -->
                                        <!-- <div class="box-tools pull-right"> -->
                                          <!-- Buttons, labels, and many other things can be placed here! -->
                                          <!-- Here is a label for example -->
                                          <!-- <span class="label label-default">Modifier</span> -->
                                        <!-- </div> -->
                                        <!-- /.box-tools -->
                                      <!-- </div> -->
                                      <!-- /.box-header -->
                                      <div class="box-body row">
                                        <div class="col-sm-2">
                                              <h4 class="text-center text-secondary">{{ $question->titre }} </h4>
                                        </div>
                                        <div class="col-sm-8">
                                              <p><b>{{ $question->question_fr }}</b></p>
                                              <p><b>{{ $question->question_en }}</b></p>
                                              
                                              <p class="text-secondary"> @lang('message.reponse') : {{ $question->type }} 

                      
                                              </p>

                                        </div>
                                        <div class="col-sm-2 text-right">
                                          @if(Auth::user()->role !== 'admin_acad')
                                              <button class="btn" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>)" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit fa-2x"></i> </button>
                                          @endif
                                        </div>

                                        <div class="col-sm-12">
                                            @if (count($question->reponses) >0)
                                                <div class="box box-warning">
                                                  <div class="box-header with-border" data-widget="collapse">
                                                    <h3 class="box-title">@lang('message.reponses')</h3>
                                                    <div class="box-tools pull-right">
                                                      <button type="button" class="btn btn-box-tool">
                                                        <i class="fa fa-plus"></i>
                                                      </button>
                                                      <!-- Buttons, labels, and many other things can be placed here! -->
                                                      <!-- Here is a label for example -->
                                                      <!-- <span class="label label-primary">Label</span> -->
                                                    </div>
                                                    <!-- /.box-tools -->
                                                  </div>
                                                  <!-- /.box-header -->
                                                 <!--  <div class="box-body row">
                                                    The body of the box
                                                  </div> -->
                                                  <!-- /.box-body -->
                                                  <div class="box-footer no-padding">
                                                      <ul class="nav nav-stacked">
                                                        @foreach ($question->reponses as $reponse)
                                                            <li>
                                                              <a > {{ $reponse->reponse_fr }} - {{ $reponse->reponse_en }} 
                                                              @if(Auth::user()->role !== 'admin_acad') 
                                                                <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($reponse)) ?>)" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button>
                                                              @endif
                                                            </a>

                                                            </li>
                                                        @endforeach
                                                        
                                                      </ul>
                                                  </div>
                                                  <!-- box-footer -->
                                                </div>
                                                <!-- /.box -->
                                            @endif
                                        </div>

                                        
                                      </div>
                                      <!-- /.box-body -->
                                      <!-- <div class="box-footer"> -->
                                        <!-- The footer of the box -->
                                      <!-- </div> -->
                                      <!-- box-footer -->
                                    </div>
                                    <!-- /.box -->
                                @endforeach



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <!-- The footer of the box -->
                            </div>
                          </div>
                       
                    @endforeach

                </section>
              </div>

              <div class="tab-pane {{$active=='transformation'? 'active':''}}" id="transformation">
                <section id="new">
                    <h4 class="page-header">@lang('message.questionnaire') Transformation</h4>

                    @foreach ($transformation->pages as $page)

                        <div class="box collapsed-box">
                            <div class="box-header with-border" data-widget="collapse">
                              <h3 class="box-title">{{ $page->title_fr }}</h3> - 
                              <h3 class="box-title">{{ $page->title_en }}</h3>
                              <div class="box-tools pull-right">
                                <!-- Buttons, labels, and many other things can be placed here! -->
                                <!-- Here is a label for example -->
                                <!-- <span class="label label-primary">Modifier</span> -->
                                @if(Auth::user()->role !== 'admin_acad')
                                  <button type="button" class="btn btn-box-tool">
                                    <i class="fa fa-edit"></i>
                                  </button>
                                @endif
                                <button type="button" class="btn btn-box-tool">
                                  <i class="fa fa-plus"></i>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                @foreach ($page->questions as $question)
                                    <div class="box">
                                      <!-- <div class="box-header with-border"> -->
                                        <!-- <h3 class="box-title">{{ $question->titre }} </h3> -->
                                        <!-- <div class="box-tools pull-right"> -->
                                          <!-- Buttons, labels, and many other things can be placed here! -->
                                          <!-- Here is a label for example -->
                                          <!-- <span class="label label-default">Modifier</span> -->
                                        <!-- </div> -->
                                        <!-- /.box-tools -->
                                      <!-- </div> -->
                                      <!-- /.box-header -->
                                      <div class="box-body row">
                                        <div class="col-sm-2">
                                              <h4 class="text-center text-secondary">{{ $question->titre }} </h4>
                                        </div>
                                        <div class="col-sm-8">
                                              <p><b>{{ $question->question_fr }}</b></p>
                                              <p><b>{{ $question->question_en }}</b></p>
                                              
                                              <p class="text-secondary"> @lang('message.reponse') : {{ $question->type }} 

                      
                                              </p>

                                        </div>
                                        <div class="col-sm-2 text-right">
                                            @if(Auth::user()->role !== 'admin_acad')
                                              <button class="btn" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>)" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit fa-2x"></i> </button>
                                            @endif
                                        </div>

                                        <div class="col-sm-12">
                                            @if (count($question->reponses) >0)
                                                <div class="box box-warning">
                                                  <div class="box-header with-border" data-widget="collapse">
                                                    <h3 class="box-title">@lang('message.reponses')</h3>
                                                    <div class="box-tools pull-right">
                                                      <button type="button" class="btn btn-box-tool">
                                                        <i class="fa fa-plus"></i>
                                                      </button>
                                                      <!-- Buttons, labels, and many other things can be placed here! -->
                                                      <!-- Here is a label for example -->
                                                      <!-- <span class="label label-primary">Label</span> -->
                                                    </div>
                                                    <!-- /.box-tools -->
                                                  </div>
                                                  <!-- /.box-header -->
                                                 <!--  <div class="box-body row">
                                                    The body of the box
                                                  </div> -->
                                                  <!-- /.box-body -->
                                                  <div class="box-footer no-padding">
                                                      <ul class="nav nav-stacked">
                                                        @foreach ($question->reponses as $reponse)
                                                            <li>
                                                              <a > {{ $reponse->reponse_fr }} - {{ $reponse->reponse_en }} 
                                                              @if(Auth::user()->role !== 'admin_acad') 
                                                                <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($reponse)) ?>)" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button>
                                                              @endif
                                                            </a>

                                                            </li>
                                                        @endforeach
                                                        
                                                      </ul>
                                                  </div>
                                                  <!-- box-footer -->
                                                </div>
                                                <!-- /.box -->
                                            @endif
                                        </div>

                                        
                                      </div>
                                      <!-- /.box-body -->
                                      <!-- <div class="box-footer"> -->
                                        <!-- The footer of the box -->
                                      <!-- </div> -->
                                      <!-- box-footer -->
                                    </div>
                                    <!-- /.box -->
                                @endforeach



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <!-- The footer of the box -->
                            </div>
                          </div>
                       
                    @endforeach

                </section>
              </div>

              <div class="tab-pane {{$active=='negociation'? 'active':''}}" id="negociation">
                <section id="new">
                    <h4 class="page-header">@lang('message.questionnaire') @lang('message.negociation')</h4>

                    @foreach ($negociation->pages as $page)

                        <div class="box collapsed-box">
                            <div class="box-header with-border" data-widget="collapse">
                              <h3 class="box-title">{{ $page->title_fr }}</h3> - 
                              <h3 class="box-title">{{ $page->title_en }}</h3>
                              <div class="box-tools pull-right">
                                <!-- Buttons, labels, and many other things can be placed here! -->
                                <!-- Here is a label for example -->
                                <!-- <span class="label label-primary">Modifier</span> -->
                                @if(Auth::user()->role !== 'admin_acad')
                                  <button type="button" class="btn btn-box-tool">
                                    <i class="fa fa-edit"></i>
                                  </button>
                                @endif
                                <button type="button" class="btn btn-box-tool">
                                  <i class="fa fa-plus"></i>
                                </button>
                              </div>
                              <!-- /.box-tools -->
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                @foreach ($page->questions as $question)
                                    <div class="box">
                                      <!-- <div class="box-header with-border"> -->
                                        <!-- <h3 class="box-title">{{ $question->titre }} </h3> -->
                                        <!-- <div class="box-tools pull-right"> -->
                                          <!-- Buttons, labels, and many other things can be placed here! -->
                                          <!-- Here is a label for example -->
                                          <!-- <span class="label label-default">Modifier</span> -->
                                        <!-- </div> -->
                                        <!-- /.box-tools -->
                                      <!-- </div> -->
                                      <!-- /.box-header -->
                                      <div class="box-body row">
                                        <div class="col-sm-2">
                                              <h4 class="text-center text-secondary">{{ $question->titre }} </h4>
                                        </div>
                                        <div class="col-sm-8">
                                              <p><b>{{ $question->question_fr }}</b></p>
                                              <p><b>{{ $question->question_en }}</b></p>
                                              
                                              <p class="text-secondary"> @lang('message.reponse') : {{ $question->type }} 

                      
                                              </p>

                                        </div>
                                        @if(Auth::user()->role !== 'admin_acad')
                                          <div class="col-sm-2 text-right">
                                                <button class="btn" ng-click="editQuestion(<?php echo htmlspecialchars(json_encode($question)) ?>)" data-toggle="modal" data-target="#edit-question"><i class="fa fa-edit fa-2x"></i> </button>
                                          </div>
                                        @endif

                                        <div class="col-sm-12">
                                            @if (count($question->reponses) >0)
                                                <div class="box box-warning">
                                                  <div class="box-header with-border" data-widget="collapse">
                                                    <h3 class="box-title">@lang('message.reponses')</h3>
                                                    <div class="box-tools pull-right">
                                                      <button type="button" class="btn btn-box-tool">
                                                        <i class="fa fa-plus"></i>
                                                      </button>
                                                      <!-- Buttons, labels, and many other things can be placed here! -->
                                                      <!-- Here is a label for example -->
                                                      <!-- <span class="label label-primary">Label</span> -->
                                                    </div>
                                                    <!-- /.box-tools -->
                                                  </div>
                                                  <!-- /.box-header -->
                                                 <!--  <div class="box-body row">
                                                    The body of the box
                                                  </div> -->
                                                  <!-- /.box-body -->
                                                  <div class="box-footer no-padding">
                                                      <ul class="nav nav-stacked">
                                                        @foreach ($question->reponses as $reponse)
                                                            <li>
                                                              <a > {{ $reponse->reponse_fr }} - {{ $reponse->reponse_en }}  
                                                              @if(Auth::user()->role !== 'admin_acad')
                                                                <button ng-click="editReponse(<?php echo htmlspecialchars(json_encode($reponse)) ?>)" data-toggle="modal" data-target="#edit-reponse" class="pull-right btn"><i class="fa fa-edit"></i></button>
                                                              @endif
                                                            </a>

                                                            </li>
                                                        @endforeach
                                                        
                                                      </ul>
                                                  </div>
                                                  <!-- box-footer -->
                                                </div>
                                                <!-- /.box -->
                                            @endif
                                        </div>

                                        
                                      </div>
                                      <!-- /.box-body -->
                                      <!-- <div class="box-footer"> -->
                                        <!-- The footer of the box -->
                                      <!-- </div> -->
                                      <!-- box-footer -->
                                    </div>
                                    <!-- /.box -->
                                @endforeach



                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                              <!-- The footer of the box -->
                            </div>
                          </div>
                       
                    @endforeach

                </section>
              </div>

            </div>


            </div>
          </div>
        </div>



        <div class="modal fade" id="edit-question">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('message.modifier') Question : <% question.titre | uppercase %></h4>
              </div>

              <form role="form" action="<% '/questions/'+question.id %>" method="POST">
                @csrf
                @method('PUT')
                            <div class="modal-body">
                              @if ($errors->any())
                                  <div class="alert alert-danger">
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div>
                              @endif
                              <div class="form-group">
                                <label for="question-fr">Question FR</label>
                                <input type="text" class="form-control" name="question_fr" id="question_fr" placeholder="" ng-model="question.question_fr">
                              </div>

                              <div class="form-group">
                                <label for="question-en">Question EN</label>
                                <input type="text" class="form-control" name="question_en" id="question_en" placeholder="" ng-model="question.question_en">
                              </div>



                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <input type="hidden" name="active" value="<%typeActives%>">
                              <button type="submit" class="btn btn-primary" ng-disabled="!question.question_fr || !question.question_en">@lang('message.modifier')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        
        <div class="modal fade" id="edit-page">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('message.modifier') page : </h4>
              </div>

              <!-- <form role="form" action="<% '/page/'+page.id %>" method="POST"> -->
                @csrf
                @method('PUT')
                            <div class="modal-body">
                              @if ($errors->any())
                                  <div class="alert alert-danger">
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div>
                              @endif
                              <div class="form-group">
                                <label>Page FR</label>
                                <input type="text" class="form-control" ng-model="page.title_fr">
                              </div>

                              <div class="form-group">
                                <label for="question-en">Page EN</label>
                                <input type="text" class="form-control" ng-model="page.title_en">
                              </div>



                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <input type="hidden" name="active" value="<%typeActives%>">
                              <button type="submit" ng-click="editP()" class="btn btn-primary" ng-disabled="!page.title_fr || !page.title_en">@lang('message.modifier')</button>
                            </div>
              <!-- </form> -->
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="edit-reponse">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">@lang('message.modifier') @lang('message.reponse') : <% reponse.titre | uppercase %></h4>
              </div>

              <form role="form" action="<% '/reponses/'+reponse.id %>" method="POST">
                @csrf
                @method('PUT')
                            <div class="modal-body">
                              @if ($errors->any())
                                  <div class="alert alert-danger">
                                      <ul>
                                          @foreach ($errors->all() as $error)
                                              <li>{{ $error }}</li>
                                          @endforeach
                                      </ul>
                                  </div>
                              @endif
                              <div class="form-group">
                                <label for="question-fr">@lang('message.reponse') FR</label>
                                <input type="text" class="form-control" name="reponse_fr" id="reponse_fr" placeholder="" ng-model="reponse.reponse_fr">
                              </div>

                              <div class="form-group">
                                <label for="question-en">@lang('message.reponse') EN</label>
                                <input type="text" class="form-control" name="reponse_en" id="reponse_en" placeholder="" ng-model="reponse.reponse_en">
                              </div>



                              
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                              <input type="hidden" name="active" value="<%typeActives%>">
                              <button type="submit" class="btn btn-primary" ng-disabled="!reponse.reponse_fr || !reponse.reponse_en">@lang('message.modifier')</button>
                            </div>
              </form>
            
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
</div>
@stop
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script type="text/javascript">
      var questionApp = angular.module('questionApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });
      questionApp.controller('questionController', function PhoneListController($scope,$http) {
            $scope.question  = undefined; 
            $scope.reponse  = undefined; 
            $scope.page = undefined;
           $scope.typeActives='';
            
            @if($curQuestion)
                $scope.question = <?php echo stripcslashes($curQuestion)  ?>;
            @endif

            if($scope.question){
                $('#edit-question').modal('show');
            }

            $scope.editPage = function(page){
              $scope.page = page;      
            }
            
            console.log("id question %o", $scope.question )
            $scope.setActives= function setActives(typeActive){
              $scope.typeActives =typeActive;
            }
            $scope.editQuestion = function(question){
              console.log("edit question %o", question);
        
              $scope.question = question;
            
              
            }

            $scope.editP = function(){

              $http({
                method: 'POST',
                data:{"pages": $scope.page},
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/questions/pages"
              }).then(function successCallback(response) {
                if(response.data.status == 1){
                  location.reload();
                  $('#edit-page').modal('hide');
                }
              }, function errorCallback(response) {

                //console.log(response);
              });

            }

            $scope.editReponse = function(reponse){
              console.log("edit reponse %o", reponse);
        
              $scope.reponse = reponse;
            
            }
      });


    </script>
@stop
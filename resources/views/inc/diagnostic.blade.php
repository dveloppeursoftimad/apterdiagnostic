<!DOCTYPE html>
<html>
<head>

	<title></title>

		

	<title>Apter Diagnostic</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" />
		<link href="{{asset('css/diagnostic.css')}}" rel="stylesheet" />
		<link href="{{asset('css/assets/css/material-kit.min.css')}}" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  	<!-- CSS Files -->

    <link rel="stylesheet" type="text/css" href="{{ asset('chart/css/style.css') }}">
    <script src="{{asset('css/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/angular.js') }}"></script>

      <script src="{{ asset('chart/js/Chart.min.js') }}"></script>
<!--   <script src="{{ asset('chart/js/donut.js') }}"></script> -->
  
	</head>
	<body>
		<nav class="navbar navbar-default navbar-expand-lg navbar_modif" role="navigation-demo" id="navbar_modif">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-translate">
          <img src="/img/apter.jpg"  style="height: 78px;">
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="{{ route('climage.index') }}" class="nav-link">
                <b>ESPACE CONSULTANT</b>
              </a>
            </li>
            <li class="nav-item">
            </li>
            <li class="nav-item">
              @if(Auth::user())
                <li class="dropdown nav-item">
                  <a href="#" class="profile-photo dropdown-toggle nav-link" data-toggle="dropdown">
                    @if(Auth::user() -> avatar_url)
                      <div class="profile-photo-small taille" style="height: auto;">
                        <img src="<?php echo Auth::user() -> avatar_url ?>" alt="Circle Image" class="rounded-circle img-fluid">
                        <!--i class="material-icons">account_circle</i-->
                      </div>
                    @else

                      <div class="profile-photo-small taille" style="height: auto;">
                        <img src="/img/user.png" alt="Circle Image" class="rounded-circle img-fluid">
                        <!--i class="material-icons">account_circle</i-->
                      </div>
                    @endif
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <h4 class="dropdown-item" id="username">Bienvenue {{ Auth::user()->username }}</h4>
                    <a href="{{ route('deconnexion') }}" class="dropdown-item" id="deconnexion" class="nav-link">
                      <b>Déconnexion</b>
                    </a>
                  </div>
                </li>
              @else
                <a href="{{ route('connexion') }}" class="nav-link">
                  <b>CONNEXION</b>
                </a>
              @endif
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-->
    </nav>
	

		<div class="page-header header-filter clear-filter" id="fond" data-parallax="true" style="background-image:url({{asset('img/fond.png')}});">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto">
					<div class="brand" style="text-align: center;">
						<h1 class="titre_diag">Outils de diagnostics d'Apter Solutions</h1>
            <h2>Les Motivations au cœur de la Performance</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--div class="main-raised col-lg-12 col-md-6 ml-auto mr-auto"-->
	<div class="col-lg-10 col-md-11 col-sm-11 ml-auto mr-auto" style="margin: -60px 30px 0;">
		<div class="card card-login" style="box-shadow: none; border-radius: 30px; padding: 10px;">

			<div class="card card-header text-center col-lg-3 col-md-4 col-sm-4 ml-auto mr-auto" id="front">
			  <h4 class="card-title"><img class="logo-front" src="/img/apter.jpg" height="50"></h4>
	    </div>
	@yield('content')

  <script src="{{asset('css/assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('css/assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/https://maps.googleapis.com/maps/api/js')}}"></script>
  <script src="{{asset('css/assets/js/material-kit.js')}}" type="text/javascript"></script>

  

</body>
</html>
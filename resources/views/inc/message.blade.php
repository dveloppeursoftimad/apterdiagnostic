@if(count($errors) > 0)
	@foreach($errors -> all() as $error)
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>	
			{{ $error }}
		</div>		
	@endforeach
@endif

@if (session('success'))
<div class="alert alert-success alert-block">
	<div class="alert-icon">
    <i class="material-icons">check</i>
  </div>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true"><i class="material-icons">clear</i></span>
  </button>
    <strong>{{ session('success') }}</strong>
</div>
@endif

@if (session('error_vide'))
<div class="alert alert-danger alert-block">
	<div class="alert-icon">
    <i class="material-icons">error_outline</i>
  </div>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true"><i class="material-icons">clear</i></span>
  </button>	
    <strong>Champs obligatoire</strong>
</div>
@endif

@if(session('error_email'))
	<div class="alert alert-danger">
		<div class="alert-icon">
      <i class="material-icons">error_outline</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
		{{ session('error_email') }}
	</div>
@endif


@if(session('error_mdp'))
	<div class="alert alert-danger alert-block">
		<div class="alert-icon">
      <i class="material-icons">error_outline</i>
    </div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true"><i class="material-icons">clear</i></span>
    </button>
		{{ session('error_mdp') }}
	</div>
@endif
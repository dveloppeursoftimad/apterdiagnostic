<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>

  <title>Apter Diagnostic</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{asset('css/front.css')}}" rel="stylesheet" />
    <link href="{{asset('css/assets/css/material-kit.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->

    <link rel="stylesheet" type="text/css" href="{{ asset('chart/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/diagnostic.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('summer/summernote-bs4.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('summer/summernote.css') }}">
    <!-- imgAreaSelect -->
    <link rel="stylesheet" href="{{ asset('css/imgareaselect.css') }}">
    <style type="text/css" rel="stylesheet">
      @font-face {
        font-family: "GOTHIC";
        src: url("{{ asset('fonts/GOTHIC.ttf') }}") format('truetype');
      }
      
      @font-face {
        font-family: "Comfortaa-Bold";
        src: url("{{ asset('fonts/Comfortaa-Bold.ttf') }}") format('truetype');
      }

      @font-face {
        font-family: "Lato-Light_0";
        src: url("{{ asset('fonts/Lato-Light_0.ttf') }}") format('truetype');
      }
      
      h1, h2, h3, h4, h5, h6 {
        font-family: "Comfortaa-Bold";
      }

      .solutions{
        font-family: "GOTHIC";
      }

      div{
        font-family: "Lato-Light_0";
      }

    </style>
    <script src="{{asset('css/assets/js/core/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/angular.js') }}"></script>

      <script src="{{ asset('chart/js/Chart.min.js') }}"></script>
<!--   <script src="{{ asset('chart/js/donut.js') }}"></script>  -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.0/angular.min.js"></script> -->
  
  </head>
  <body>
    <nav style="position: fixed;z-index: 99;" class="navbar navbar-default navbar-expand-lg navbar_modif" role="navigation-demo" id="navbar_modif">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-translate">

          <img src="{{asset('img/logo_apter.jpg')}}" height="41">

          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item" id="large_lang">
              <li class="dropdown nav-item">
                  <a id="lang_hide" href="#" class="profile-photo dropdown-toggle nav-link" data-toggle="dropdown">
                    @if(App::getLocale() == 'fr')
                      <img src="{{asset('img/fr.jpg')}}" alt="Circle Image" width="37" height="35" class="rounded-circle img-fluid">
                    @else
                      <img src="{{asset('img/en.png')}}" alt="Circle Image" width="40" height="40" class="rounded-circle img-fluid">
                    @endif
                    <b><i class="material-icons">keyboard_arrow_down</i></b>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a href="/locale/fr" class="dropdown-item" id="username">
                      <img src="{{asset('img/fr.jpg')}}" alt="Circle Image" width="37" height="35" class="rounded-circle img-fluid"><b style="margin-left:5px;">&nbsp;@lang('message.francais')</b>
                    </a>
                    <a href="/locale/en" class="dropdown-item" id="deconnexion">
                      <img src="{{asset('img/en.png')}}" alt="Circle Image" width="40" height="40" class="rounded-circle img-fluid"><b style="margin-left:5px;">@lang('message.anglais')</b>
                    </a>
                  </div>
                </li>
            </li>
            <li class="" id="small_lang">
              <a href="/locale/fr" class="dropdown-item" class="nav-link">
                <img src="{{asset('img/fr.jpg')}}" alt="Circle Image" width="37" height="35" class="rounded-circle img-fluid"><b style="margin-left:5px;">&nbsp;@lang('message.francais')</b>
              </a>
              <a href="/locale/en" class="dropdown-item" class="nav-link">
                <img src="{{asset('img/en.png')}}" alt="Circle Image" width="40" height="40" class="rounded-circle img-fluid"><b style="margin-left:5px;">@lang('message.anglais')</b>
              </a>
            </li>
            <li class="dropdown nav-item" id="large_cons">
              <a href="{{ route('climage.index') }}" class="nav-link">
                <b>@lang('message.espace_consultant')</b>
              </a>
            </li>
            <li class="" id="small_cons">
              <a href="{{ route('climage.index') }}" class="dropdown-item">
                <b>@lang('message.espace_consultant')</b>
              </a>
            </li>
            <li class="nav-item">
            </li>
            <li class="nav-item large_user" id="large_user">
              @if(Auth::user())
                <li class="dropdown nav-item">
                  <a id="user_hide" href="#" class="profile-photo dropdown-toggle nav-link" data-toggle="dropdown">
                    @if(Auth::user() -> avatar_url)
                      <div class="profile-photo-small taille" style="height: auto;">
                        <img src="{{ Auth::user()->avatar_url }}" style="height: 40px; width: 40px" alt="Circleee Image" class="rounded-circle">
                        <!--i class="material-icons">account_circle</i-->
                      </div>
                    @else

                      <div class="profile-photo-small taille" style="height: auto;">
                        <img src="{{asset('img/user.png')}}" alt="Circle Image" class="rounded-circle img-fluid">
                        <!--i class="material-icons">account_circle</i-->
                      </div>
                    @endif
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <h4 class="dropdown-item" id="username">{{ __('message.bienvenue') }} {{ Auth::user()->username }}</h4>
                    <a href="{{ route('deconnexion') }}" class="dropdown-item" id="deconnexion" class="nav-link">
                      <b>{{ __('message.deconnexion') }}</b>
                    </a>
                  </div>
                </li>
              @else
                <a href="{{ route('connexion') }}" class="nav-link">
                  <b>{{ __('message.connexion') }}</b>
                </a>
              @endif
            </li>
            <li class="nav-item" id="small_screen">
              @if(Auth::user())
                <h4 class="dropdown-item">{{ __('message.bienvenue') }} {{ Auth::user()->username }}</h4>
                <a href="{{ route('deconnexion') }}" class="dropdown-item">
                  <b>{{ __('message.deconnexion') }}</b>
                </a>
              @else
                <a href="{{ route('connexion') }}" class="dropdown-item">
                  <b>{{ __('message.connexion') }}</b>
                </a>
              @endif
            </li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-->
    </nav>

    <div class="page-header header-filter clear-filter" id="fond" data-parallax="true" style="background-image:url({{asset('img/fonds2.jpg')}});">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          @if(App::getLocale() == 'fr')
            <div class="brand" id="max" style="text-align: center;">
              <h1 class="titre_diag">Outils de diagnostics d'Apter <span class="solutions">Solutions</span></h1>
              <h2 id="titre">@lang('message.coeur')</h2>
            </div>
            <div class="brand" id="min" style="text-align: center;">
              <h1 class="titre_diag2">Outils de diagnostics d'Apter <span class="solutions">Solutions</span></h1>
              <h2 class="titre2">@lang('message.coeur')</h2>
            </div>
          @else
            <div class="brand" id="max" style="text-align: center;">
              <h1 class="titre_diag">Apter <span class="solutions">Solutions</span> Diagnostic Tools</h1>
              <h2 id="titre">@lang('message.coeur')</h2>
            </div>
            <div class="brand" id="min" style="text-align: center;">
              <h1 class="titre_diag2">Apter <span class="solutions">Solutions</span> Diagnostic Tools</h1>
              <h2 class="titre2">@lang('message.coeur')</h2>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <!--div class="main-raised col-lg-12 col-md-6 ml-auto mr-auto"-->
  <div class="col-lg-10 col-md-11 col-sm-11 ml-auto mr-auto" style="margin: -60px 30px 0;">
    <div class="card card-login" style="box-shadow: none; border-radius: 30px; padding: 10px;">


      <!-- <div class="card card-header text-center ml-auto mr-auto" id="front">
        <h4 class="card-title"><img class="logo-front" src="/img/apter.png" id="logo_head"></h4>

      </div> -->
  @yield('content')
  <script src="{{asset('summer/summernote.js')}}"></script>
  <script src="{{asset('summer/summernote-bs4.js')}}"></script>
  <script src="{{asset('css/assets/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('css/assets/js/plugins/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
  <script src="{{asset('css/assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <!-- <script src="{{asset('css/https://maps.googleapis.com/maps/api/js')}}"></script> -->
  <script src="{{asset('css/assets/js/material-kit.js')}}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-chart.js/1.0.3/angular-chart.min.js"></script>
  

</body>
</html>
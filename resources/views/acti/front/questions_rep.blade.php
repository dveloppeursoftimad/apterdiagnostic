@extends('diagnostic')
@section('content')
<div ng-app="actiFrontApp" ng-controller="actiFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container climage-form" style="position: relative;">
			<div>
				<div class="title_mobile">
					<h2 class="font" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a> <i class="material-icons">chevron_right</i> <span class="" style="color: #3c4858;"><a style="color: #3c4858;" href="{{ route('acti-front') }}">@lang('message.perf_motiv')</a></span> <i class="material-icons">chevron_right</i> <span class="text-warning">{{ $test->name }} </span> 
					</h2>
					<hr>

					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-2 offset-md-1 text-left">
									<h6 class="font2" style="color: #3c4858;">Pages :</h6>
								</div>
								<div class="col-md-8 text-left">
									<h6 class="font3" style="color: #3c4858;">{{ $numero }} / 7</h6>
								</div>	
								<div class="col-md-2 offset-md-1 text-left">
									<div class="title_mobile">
										<h6 class="font2" style="color: #3c4858;">Progression :</h6>
									</div>
								</div>
								<div class="col-md-7 text-left">
									<div class="progress progress-line-primary" style="background-color: #aaa; !important;margin-top:15px;margin-bottom:0px;}">
										<div id="progress" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{ ((float)($numero - 1) / 7.0)*100 }}%;">
											<span class="sr-only"></span>
										</div>
									</div>
								</div>
								<div class="col-md-1 text-left" style="margin-top:5px;">
									{{ (int)(((float)($numero -1) / 7.0)*100) }} %
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="card">
								<div class="card-header card-header-text card-header-warning">
									<div class="card-text">
										<h3>{{ $pages->{'title_'.$lang} }}</h3>
									</div>
								</div>
								<div class="card-body">
									<form role="form" action="{{ route('acti-front-save') }}" method="POST">
										@csrf
										@method('POST')
										<input type="hidden" name="id" value="{{ $data['id'] }}">
										<input type="hidden" name="code" value="{{ $data['code'] }}">
										<input type="hidden" name="idpage" value="{{ $data['idpage'] }}">
										<input type="hidden" name="numero" value="{{ $numero }}">
										<input type="hidden" name="terminer_id" value="{{ $terminer_id }}">
										<input type="hidden" name="progression" value="{{ $progression }}">
										<input type="hidden" name="nbQuest" value="{% nbQuest %}">
										@foreach ($pages->pages as $p => $section)
											@if($type_rep->role == "beneficiaire")
												<h3 class="card-title">{{ $section->{'title_'.$lang.'_ben'} }}</h3>
											@else
												<h3 class="card-title">{{ $section->{'title_'.$lang} }}</h3>
											@endif
											@foreach ($section->questions as $key => $question)
												<div class="col-md-12" id="question_{{$question->id}}">
													@if($type_rep->role == "beneficiaire")
														@if($p == 5)
							                              	@if($key == 4 || $key == 8 || $key == 12)
							                                	<h3 class="card-title">{{ $section->{'title_'.$lang.'_ben'} }}</h3>
							                              	@endif
							                            @endif
														<h5 class="card-title">{{ $question->{'question_'.$lang.'_ben'} }}</h5>
													@else
														@if($p == 5)
							                              	@if($key == 4 || $key == 8 || $key == 12)
							                                	<h3 class="card-title">{{ $section->{'title_'.$lang} }}</h3>
							                              	@endif
							                            @endif
														<h5 class="card-title">{{ $question->{'question_'.$lang} }}</h5>
													@endif
													<p style="color: red"> {% error_question[{{ $question->id }}] %} </p>
												</div>
												@if ($question->type == "radio")
													@foreach ($question->reponses as $reponse)
														<div class="col-md-6 col-xs-6">
															<div class="form-check" style="padding-top: 0px;">
																<div class="btn-group-toggle" ng-click="selectRadio(<?php echo htmlspecialchars(json_encode($question->id)) ?>)">
																	<label class="form-check-label" style="font-size:17px;">

																		<input class="form-check-input" type="radio" name="reponse_question[{{ $question->id }}]" value="{{ $reponse->id }}"
																		ng-model="reponse_question[{{ $question->id }}]"
																		>{{ $reponse->{'reponse_'.$lang} }}

																		<span class="circle" style="border:1px solid #ffd85b;">
																			<span class="check" style="background-color: #ffd85b; border:1px solid #ffd85b;"></span>
																		</span>
																	</label>
																</div>     
															</div>
														</div>
													@endforeach

													<br>
												@endif
												@if ($question->type == "text")
													<div class="col-md-12 col-xs-6">
														@if($type_rep->role == "beneficiare")
															<input type="text" class="form-control" name="reponse_question[{{ $question->id }}]"  placeholder="{{ $question->{'question_'.$lang.'_ben'} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
														@else
															<input type="text" class="form-control" name="reponse_question[{{ $question->id }}]"  placeholder="{{ $question->{'question_'.$lang} }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">
														@endif
													</div>
													<br>
												@endif

											@endforeach
										@endforeach
										<div class="row">
											@if($numero==1)
												<div class="col-sm-12 text-right">
													<button ng-click="save()" type="submit" name="next" style="font-family: inherit;" class="btn btn-warning btn-round">{{ $lang == "en" ? "Next"  : "Suivant"}}<i class="material-icons">chevron_right</i></button>
												</div>
											@endif
											@if($numero>1 && $numero<7)
												<div class="col-sm-12 text-left">
													<button type="submit" name="previous" style="font-family: inherit; margin-right:20px;" class="btn btn-default btn-round"><i class="material-icons">chevron_left</i>{{ $lang == "en" ? "Previous"  : "Précedent"}}</button>
													<button ng-click="save($event)"  type="submit" name="next" style="font-family: inherit;" class="btn btn-warning btn-round">{{ $lang == "en" ? "Next"  : "Suivant"}}<i class="material-icons">chevron_right</i></button>
												</div>
											@endif
											@if($numero==7)
												<div class="col-sm-6 text-left">
													<button type="submit" name="previous" style="font-family: inherit;" class="btn btn-default btn-round"><i class="material-icons">chevron_left</i>{{ $lang == "en" ? "Previous"  : "Précedent"}}</button>
												</div>
												<div class="{{$col}} text-right">
													<button type="submit" ng-click="save($event)" name="final" style="font-family: inherit;" class="btn btn-warning btn-round">{{ $lang == "en" ? "End"  : "Terminer"}}<i class="material-icons">chevron_right</i></button>
												</div>
											@endif
										</div>
									</form>
								</div>
								<br>
							</div>			
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.btn-reponse{
		white-space:normal !important;
		width:100% !important;
		font-family: Roboto, sans serif !important;
		font-weight: bold;
	}
</style>
<script type="text/javascript">
	$(function() {
		var width = 0;
		var element = document.getElementById("q"+width);
		// $('input[type="radio"]').each(function(){
		// 	$(this).click(function(){
		// 		if ($(this).is(':checked'))
		// 		{
		// 			$('html, body').animate({
		// 				scrollTop: $(window).scrollTop() + 290
		// 			}, 400);

		// 		} else {
		// 			$(this).prop('checked', false);
		// 		}
		// 	});
		// });
		window.scrollTo({ left: 0, top: document.body.scrollHeight/6, behavior: 'smooth' });
	});
</script>
<script type="text/javascript">
	var actiFrontApp = angular.module('actiFrontApp', [], function($interpolateProvider) {
		$interpolateProvider.startSymbol('{%');
		$interpolateProvider.endSymbol('%}');
	});
	actiFrontApp.controller('actiFrontController', function PhoneListController($scope) {
		$scope.questions = [];
		$scope.reponse_question = [];
		@foreach ($pages->pages as $section)
		@foreach ($section->questions as $quest)
		$scope.questions.push(<?php echo htmlspecialchars(json_encode($quest->id)) ?>);
		@endforeach
		@endforeach

		$scope.nextPage = function(id,code,idpage){     		
			window.location = "/diagnostic/acti/"+id+"/"+code+"/questions_rep/"+idpage;
		}
		$scope.save = function(e){
			$scope.error = false;
			window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
			window.scrollTo({ left: 0, top: document.body.scrollHeight/4, behavior: 'smooth' });
			$scope.error_question = [];
			let numPage = {{ $numero }};
			numPage = parseInt(numPage);
			if(numPage != 7){
				for(let i in $scope.questions){

					if(!$scope.reponse_question[$scope.questions[i]]){
						$scope.error = true;
						$scope.error_question[$scope.questions[i]] = "{{ __('message.requis') }}";
					}else{
						console.log($scope.reponse_question[$scope.questions[i]]);
					}
				}
				console.log($scope.error_question);
				if($scope.error ){	
					event.preventDefault();
				}
			}
		}

		$scope.selectRadio = function(id){
			console.log(id);
			let next = parseInt(id)+1
			let elmnt = document.getElementById("question_"+next);
			if(elmnt) elmnt.scrollIntoView({ block: 'start',  behavior: 'smooth' });
			
		}

	});
</script>
			<!--/div-->
@endsection('content')

@extends('diagnostic')
@section('content')
<div ng-app="actiFrontApp" ng-controller="actiFrontController">
	<div class="section section-basic" style="padding: 10px 0;" >
		<div class="container" style="position: relative;">
			<div>
				<div class="title_mobile">
					<h2 class="font"><a style="color: #3c4858;" href="{{ route('diagnostic') }}">{{ __('message.accueil') }}</a><i class="material-icons">chevron_right</i><a href="{{ route('acti-front') }}"> <span class="text-warning">@lang('message.perf_motiv')</span></a></h2>
				</div>
				<div class="row">
					@forelse ($actis as $acti)
					<div class="card">
						<div class="card-body row">
							<div class="col-sm-8">
								<h4><i class="large material-icons">chevron_right</i> {{ $acti->name }}</h4>
							</div>
							<div class="col-sm-4">
								<button class="btn btn-primary btn-round" style="width: 100%" ng-click="selectacti(<?php echo htmlspecialchars(json_encode($acti)) ?>)" data-toggle="modal" data-target="#participer">
									<i class="large material-icons">arrow_forward</i> <b>{{ __('message.participer') }}</b>
									<div class="ripple-container"></div>
								</button>
							</div>
						</div>
					</div>
					@empty
					<div class="card">
						<div class="card-body row">
							<div class="col-sm-8">
								<h4>Pas de @lang('message.perf_motiv')</h4>
							</div>

						</div>
					</div>
					@endforelse
				</div>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="participer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">{{ __('message.code_360diagnostic') }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="code" style="color: gray;">Code</label>
						<input type="text" class="form-control" id="code" ng-model="code"  placeholder="{{ __('message.entrer_code') }}" style="background-image: linear-gradient(0deg,#ffd85b 2px,rgba(76,175,80,0) 0),linear-gradient(0deg,#ffd85b 1px,hsla(0,0%,82%,0) 0);">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('message.annuler')}}</button>
					<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="saveCode()">{{__('message.participer')}}</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
	window.scrollTo({ left: 0, top: document.body.scrollHeight, behavior: 'smooth' });
});
</script>
<script type="text/javascript">
var actiFrontApp = angular.module('actiFrontApp', [], function($interpolateProvider) {
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
});
actiFrontApp.controller('actiFrontController', function PhoneListController($scope) {
	$scope.acti = undefined;
	$scope.curacti = undefined;

	$scope.selectacti = function(acti){
		console.log("acti %o", acti);
		$scope.curacti = acti;
	}

	$scope.saveCode = function(){
		if($scope.code ==  $scope.curacti.code){
			$scope.acti = $scope.curacti;
			var role = "{{ $role }}";
			console.log(role);
			if(role == "repondant"){
				window.location = "/diagnostic/acti/"+$scope.curacti.id+"/"+$scope.curacti.code+"/questions_rep";
			} else {
				window.location = "/diagnostic/acti/"+$scope.curacti.id+"/"+$scope.curacti.code+"/questions_rep";
			}
		}else{
			alert("Le code entré ne correspond pas");
		}
	}
});
</script>
<!--/div-->
@endsection('content')

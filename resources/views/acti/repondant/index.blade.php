{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')
@section('css')

  <link rel="stylesheet" href="/css/styleFeedback.css">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/admin_custom.css">
@stop
@section('content_header')
      <h1>
        <span class="glyphicon glyphicon-stats"></span>
        {{$test->name}}
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.perf_motiv')</a></li>
        <li class="active">@lang('message.repondants')</li>
      </ol>
@stop
@section('content')
    <div ng-app = "app" ng-controller="myCtrl">


        {{-- <input type="hidden" ng-model="idTest ={{$idtest}}"> --}}
        <input type="hidden" id="idTest" value="{{$idtest}}">
        <input type="hidden" id="testRepondant" value="<?php echo htmlspecialchars(json_encode($testrepondant));?>">
        @if (session('error_email'))
          <div class="alert alert-danger">
            <div class="alert-icon">
              <i class="material-icons"></i>
            </div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true"><i class="material-icons">clear</i></span>
            </button>
            {{ session('error_email') }}
          </div>
        @endif
        @if(count($repondants) >0)
        <div class="box box-primary">

            <div class="box-header">
              <h3 class="box-title">@lang('message.beneficiaire')</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>@lang('message.nom')</th>
                  <th>@lang('message.societe')</th>
                  <th>@lang('message.email')</th>
                  <th>@lang('message.phone')</th>
                </tr>

                      <tr>
                        <td scope="col">
                         <span>{{$beneficiaire->civility}} {{$beneficiaire->username}}</span>
                        </td>
                        <td scope="col">
                          {{$beneficiaire->organisation}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->email}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->phone}}
                        </td>
                      </tr>
                   
              </table>
            </div>
            <!-- /.box-body -->
           {{--  <div class="box-footer ">
                
            </div> --}}
        </div>
         
        @endif

        
        <div ng-if="!(repondants.length >0)" class="row">
          <div class="col-sm-12 col-md-4">
                @if(count($repondants) > 0)
                <form style="padding-top: 1em;" role="form" method="POST" action="<% '/acti/sendMailGlobalyActi' %>"  >
                      @csrf
                      @method('POST')
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="idTest" value="{{$idtest}}">
                      <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                      <input type="hidden" name="motif" value="<% mailMotif%>">

                      <button type="submit" class="btn btn-primary ">@lang('message.env_mail_rep')</button>
                      

                </form>
                @endif
           </div>


              <div class="col-sm-12 col-md-4">
                <form style="padding-top: 1em;" id="relanceMail" role="form" method="POST" action="<% '/acti/checkGlobalyActi' %>"  >
                        @csrf
                        @method('POST')
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="idTest" value="{{$idtest}}">
                        <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                        <input type="hidden" name="motif" value="<% mailMotif%>">
                        <button type="submit" ng-if="enableCheckrelanceValue"  class="btn btn-primary ">@lang('message.relance_mail')</button>
                </form>
              </div>
        </div>

        @if(count($repondants) >0)
        <div ng-if="!(repondants.length >0)" class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">@lang('message.repondants')</h3>

              {{-- <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <th>@lang('message.nom_groupe')</th>
                  <th>@lang('message.nom') - @lang('message.email')</th>
                  <th>@lang('message.mdp')</th>
                  <th>@lang('message.env_mail')</th>
                  <th>@lang('message.reponse')</th>
                  <th></th>
                </tr>
                @foreach($repondants as  $user)
  
                         <tr>
                        <td scope="col">

                          <input ng-model="selectedList[{{ $user->id }}]" type="checkbox" name="typeMail[{{ $user->id }}][{{ $user->mail}}]" ng-click='configMail2("invitation","{{$user->groupe['name']}}")' ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})">
                          <input ng-model="selectedList[{{ $user->id }}]" type="checkbox" name="typeMail[{{ $user->id }}][{{ $user->mail}}]" ng-click='configMail2("relance","{{$user->groupe['name']}}")' ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})">
                          <input ng-model="selectedList[{{ $user->id }}]" type="checkbox" name="typeMail[{{ $user->id }}][{{ $user->mail}}]" ng-click='configMail2("remerciement","{{$user->groupe['name']}}")' ng-if="isThanksSend($id={{$user->id}})">
                          


                          <input type="hidden" name="checkrelance[{{$user->id}}]" form="relanceMail" value="<%checkrelance[{{$user->id}}]%>" >
                          <!-- <input type="checkbox" ng-model="checkrelance[{{$user->id}}]" [ng-true-value="on"][ng-false-value="false"] ng-change="enableCheckrelance()"> -->
                        </td>
                        <td scope="col">
        
                              {{ $user->groupe['name'] }}
                    
                        </td>
                        <td scope="col">
                          <span>{{$user->civility}} {{$user->firstname}} {{$user->lastname}}</span>
                          <br>
                          <span> {{$user->email}}</span>
                          <br>
                           <span>URL : <a href="{{$user->url}}">{{$user->url}}</a></span>
                        </td>
                        <td scope="col">
                          {{$user->code}}
                        </td>
                        <td scope="col">

                          <span class="cursor"  ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation', userType={{$user->groupe['name']}})" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailinvBenef' : '#mailinvRep'}}"><i class="fa fa-send  "></i> @lang('message.envoyer')</span>

                          <span class="cursor"  ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailrelBenef' : '#mailrelRep'}}"><i class="fa fa-send  "></i> @lang('message.relancer')</span>
                          
                          <span class="cursor" ng-if="isThanksSend($id={{$user->id}})" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailThankBenef' : '#mailThankRep'}}" style="padding-right: 25%;" ><i class="fa fa-send"></i> @lang('message.remercier')</span>

                        </td>
                        <td scope="col">
                          <span ng-if="isThanksSend($id={{$user->id}})">
                            @lang('message.oui')
                          </span>
                          <span ng-if="!isThanksSend($id={{$user->id}})">
                            @lang('message.non')
                          </span>
                        </td>
                        <td scope="col">
                          <!-- Button trigger modal -->
                                <span ng-click="editRepondant(<?php echo htmlspecialchars(json_encode($user)) ?>)" data-toggle="modal" data-target="#edition"><i class="fa fa-edit fa-2x"></i></span>

                                <button ng-if="!isThanksSend($id={{$user->id}})" ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" class="btn btn-box-tool"><i class="fa fa-trash fa-2x"></i></button>
                        </td>
                      </tr>                 
                  
                 @endforeach
              </table>
              <div style="margin-left:25px;margin-bottom:5px;">
                <button class="btn btn-primary" ng-click="submit()" data-toggle="modal" data-target="#multipleMail" type="submit">@lang('message.envoye_check')</button>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- Button trigger modal -->
                <div  class="col-sm-3">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#repondant">
                  @lang('message.ajout_rep')
                </button>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="repondant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                            <form role="form" method="POST" action="<% '/acti/addRepondantToActi' %>"  >
                           @csrf
                           @method('POST')
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                                <label class="col-sm-3 col-form-label">@lang('message.nom') *:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="lastname" name="lastname" class="form-control" value="{{ old('username') }}"
                                           placeholder="Nom"  >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                                <label class="col-sm-3 col-form-label">@lang('message.prenom') *:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="firstname" name="firstname" class="form-control" value="{{ old('username') }}"
                                           placeholder="Prénom"  >
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.email') *:</label>
                                  <div class="col-sm-9">
                                    <input type="email" ng-model = "email"  name="email" class="form-control" value="{{ old('email') }}"
                                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.mdp') *:</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="password"  name="password" class="form-control"
                                           placeholder="Mot de passe">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              {{-- Groupe --}}
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang("message.groupe"):</label>
                                   <div class="col-sm-9">
                                     <select class="custom-select custom-select-lg mb-3 form-control" ng-model="groupe_id" name="groupe_id">
                                        @foreach($groupes as  $groupe)
                                          <option selected value="{{ $groupe->id }}">{{ strtoupper($groupe->abbreviation) }}</option>
                                        @endforeach
                                      </select>
                                  
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('groupes') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              {{-- Abreviation groupe --}}
                             <!--  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.ab'):</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="abreviation"  name="abreviation" class="form-control"
                                           placeholder="Abbreviation groupe">
                                    <span class=" form-control-feedback"><i class="fa fa-bookmark"></i></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('abreviation') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div> -->
                              <!-- PHONE -->
                              <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.phone'):</label>
                                  <div class="col-sm-9">
                                    <input type="text"  name="phone" class="form-control" value="{{ old('phone') }}"
                                           placeholder="Téléphone">
                                    <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <!-- ORGANISATION -->
                              {{-- <div class="form-group row has-feedback {{ $errors->has('organisation') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.organisation'):</label>
                                  <div class="col-sm-9">
                                    <input type="text" ng-model="editorganisation" name="organisation" class="form-control" value="{{ old('organisation') }}"
                                           placeholder="ORGANISATION">
                                    <span class="glyphicon glyphicon glyphicon-tags form-control-feedback"></span>
                                    @if ($errors->has('organisation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('organisation') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div> --}}
                              <!-- CIVILITY -->
                              <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.civilite'):</label>
                                  <div class="col-sm-9 row">
                                    <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                                           placeholder="CIVILITY"> -->
                                           <div class="col-sm-6">
                                             <input type="radio"  class="custom-control-input"  id="huey" name="civility" value="@lang('message.mr')"
                                                 >
                                            <span >@lang('message.mr')</span>
                                           </div>
                                           <div class="col-sm-6">
                                             <input type="radio" class="custom-control-input"   name="civility" value="@lang('message.mr')"
                                                   >
                                             <span>@lang('message.mde')</span>
                                           </div>
                                    @if ($errors->has('civility'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('civility') }}</strong>
                                        </span>
                                    @endif
                                  </div>
                              </div>
                              <!-- LANGUAGE -->
                              <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.langue'):</label>
                                  
                                    <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                                           placeholder="LANGUAGE">
                                    <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                                    <div class="col-sm-9 ">
                                      <select class="custom-select custom-select-lg mb-3 form-control"  name="language">
                                          <option  selected value="fr">@lang('message.francais')</option>
                                          <option  value="en">@lang('message.anglais')</option>
                                       </select>
                                       @if ($errors->has('language'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('language') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                 
                              </div>
                              <!-- AVATAR FILE CHOOSER -->
                              <!-- <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">Avatar:</label>
                                    <div class="col-sm-9 ">
                                      <div class="custom-file">
                                        <input type="file" name="editavatar" class="custom-file-input" onchange="angular.element(this).scope().setImage(this)" id="editavatar">
                                        <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                                      </div>
                                       @if ($errors->has('avatar'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('avatar') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                 
                              </div> -->
                              <input type="hidden" value="<%image_source%>" name="avatar_url">
                              <input type="hidden" name="idTest" value="{{$idtest}}">
                              <button ng-disabled="!firstname||!lastname ||!email || !password || !groupe_id" type="submit"
                                      class="btn btn-primary btn-block btn-flat"
                              >@lang('message.send_save')</button>
                          </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                        <!-- <button type="button" class="btn btn-primary">@lang('message.save_change')</button> -->
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        @endif
        <div style="visibility: hidden;display: none;">
         @if($beneficiaire)
              <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">
                      @lang('message.nom_benef')
                    </th>
                    <th scope="col">
                      @lang('message.email')
                    </th>
                    <th scope="col">
                      @lang('message.phone')
                    </th>
                    <th scope="col">
                      @lang('message.societe')
                    </th>
                    <th scope="col">
                      @lang('message.civilite')
                    </th>
                    <th scope="col">
    
                        <div ng-if="isUserIdHere($id={{$beneficiaire->id}})">
                          <span class="cursor"  ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation', userType={{$user->groupe['name']}})" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailinvBenef' : '#mailinvRep'}}"><i class="fa fa-send  "></i> @lang('message.envoyer')</span>

                          <span class="cursor"  ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailrelBenef' : '#mailrelRep'}}"><i class="fa fa-send  "></i> @lang('message.relancer')</span>
                          
                          <span class="cursor" ng-if="isThanksSend($id={{$user->id}})" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailThankBenef' : '#mailThankRep'}}" style="padding-right: 25%;" ><i class="fa fa-send"></i> @lang('message.remercier')</span>
                        </div>
                      
                    </th>
                  </tr>
                </thead>
              

                <tbody>

                      <tr ng-if="isUserIdHere($id={{$beneficiaire->id}})">
                        <td scope="col">
                          {{$beneficiaire->username}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->email}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->phone}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->organisation}}
                        </td>
                        <td scope="col">
                          {{$beneficiaire->civility}}
                        </td>
                      </tr>
                   
                </tbody>
              </table>
         @endif
         @if(count($repondants))
         <div class="row">
          @foreach($repondants as  $user)
                
                 @if($user->role == 'repondant')
                  <div ng-if="isUserIdHere($id={{$user->id}})" class="col-sm-6 col-md-4 col-lg-3">
                     <div class="box" data-widget="box-widget">
                        <div class="box-header">
                          <div class="box-tools">
                            <!-- This will cause the box to be removed when clicked -->      
                              <button ng-click="deleteData($event,$id ='<?php echo $user->id ;?>')" data-toggle="modal" data-target="#exampleModal" class="btn btn-box-tool"><i class="fa fa-trash fa-2x"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                            <div class="box-name">
                                {{$user->username}}
                            </div>
                            <div class="titre-apter">
                               Apter-France
                            </div>
                            <div class="coordonne">
                               <span>{{$user->email}}</span>
                            </div>
                            <div class="coordonne">
                              <span>{{$user->phone}}</span>
                            </div>
                            <div class="coordonne"></div>
                            <div class="center">
                              <div>
                                <span class="cursor"  ng-if="!isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='invitation', userType={{$user->groupe['name']}})" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailinvBenef' : '#mailinvRep'}}"><i class="fa fa-send  "></i> @lang('message.envoyer')</span>

                                <span class="cursor"  ng-if="isMailSend($id={{$user->id}}) && !isThanksSend($id={{$user->id}})"   style="padding-right: 25%;" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='relance', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailrelBenef' : '#mailrelRep'}}"><i class="fa fa-send  "></i> @lang('message.relancer')</span>
                                
                                <span class="cursor" ng-if="isThanksSend($id={{$user->id}})" ng-click="configMail(idRepondant={{$user->id}},idTest={{$idtest}},motif='remerciement', '{{$user->groupe['name']}}')" data-toggle="modal" data-target="{{$user->groupe['name'] == 'Beneficiaire' ? '#mailThankBenef' : '#mailThankRep'}}" style="padding-right: 25%;" ><i class="fa fa-send"></i> @lang('message.remercier')</span>
                              </div>
                            </div>
                        </div>
                        
                      </div>
                   </div>
               @endif
          @endforeach
         </div> 
        @endif
        </div>
        <div>
          <div ng-if="!(repondants.length >0)" class="box box-default" data-widget="box-widget">
            <div class="box-header">
              <form>
                <div class="form-group">
                  <label for="exampleFormControlFile1">@lang('message.import_exel') :</label>
                  <input type="file" class="form-control-file" onchange="angular.element(this).scope().setFile(this)" id="exampleFormControlFile1">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div ng-if="listOfUser.length >0 && false">
          <div class="box box-default" data-widget="box-widget">
            <div class="box-header">
              
                <div class="form-group">
                  <label for="exampleFormControlFile1">@lang('message.clic_mail')</label>
                  <form role="form" method="POST" action="<% '/acti/sendMailGlobalyActi' %>"  >
                      @csrf
                      @method('POST')
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="idTest" value="{{$idtest}}">
                      <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                      <input type="hidden" name="motif" value="<% mailMotif%>">
                      <button type="submit" class="btn btn-primary ">@lang('message.env_mail')</button>
                  </form>
                </div>
             
            </div>
          </div>
        </div>
        
        <div class="beneficiaire" ng-if="repondants.length>0">
          <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('message.benef_trouve')</h3>
            
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <table class="table">
            <thead class="thead-light">
              <tr>
                <!-- <th scope="col" ng-repeat="beneficiaire in beneficiaires">
                  <% beneficiaire.B %>
                  <input type="hidden" name="labelbeneficiaire[<% $index %>]" form="valider" value="<% beneficiaire.B %>">    
                </th> -->
                <th scope="col" >@lang('message.type-civ')</th>
                <th scope="col" >@lang('message.prenom')</th>
                <th scope="col" >@lang('message.nom')</th>
                <th scope="col" >@lang('message.societe')</th>
                <th scope="col" >@lang('message.initiales')</th>
                <th scope="col" >@lang('message.email')</th>
                <th scope="col" >@lang('message.langue')</th>
                <th scope="col" >@lang('message.tel_portable')</th>
              </tr>
            </thead>
            <tbody>
              <tr>           
                <td scope="col" ng-repeat="beneficiaire in beneficiaires">
                  <%beneficiaire.C%>
                  <input type="hidden" name="beneficiaire[<% $index %>]" form="valider" value="<% beneficiaire.C %>">       
                </td>
              </tr>
            </tbody>
          </table>
          </div>
          <!-- /.box-body -->
        </div>
        </div>

        <div ng-if="repondants.length>0" class="repondant" style="overflow: auto;">
          <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('message.repond_trouve')</h3>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table">
            <thead ng-if="repondants.length>0" class="thead-light">
              <tr>
                <th scope="col" >@lang('message.nom_groupe')</th>
                <th scope="col" >@lang('message.abs')</th>
                <th scope="col" >@lang('message.civilite')</th>
                <th scope="col" >@lang('message.n_p')</th>
                <th scope="col" >@lang('message.email')</th>
                <th scope="col" >@lang('message.langue')</th>
              </tr>
            </thead>
            <tbody >
              <tr ng-repeat="repondant in repondants">           
                <td scope="col">
                  <%repondant.B%>
                  <input type="hidden" name="groupeRepondant[<% $index %>]" value="<%repondant.B%>" form="valider">  
                </td>
                <td scope="col">
                  <%repondant.C%>
                  <input type="hidden" name="abreviationRepondant[<% $index %>]" value="<%repondant.C%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.D%>
                  <input type="hidden" name="civiliteRepondant[<% $index %>]" value="<%repondant.D%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.E%> <%repondant.F%>
                  <input type="hidden" name="nomRepondant[<% $index %>]" value="<%repondant.F%>" form="valider">
                  <input type="hidden" name="prenomRepondant[<% $index %>]" value="<%repondant.E%>" form="valider">
                </td>
                <td scope="col">
                  <%repondant.G%>
                  <input type="hidden" name="emailRepondant[<% $index %>]" value="<%repondant.G%>" form="valider">
                </td>
                <td scope="col"><% repondant.H ? repondant.H :'français' %>
                  <input type="hidden" name="langageRepondant[<% $index %>]" value="<% repondant.H ? repondant.H :'français' %>" form="valider">
                </td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
          
        </div>
        <form ng-if="repondants.length >0" id="valider" action="/acti/createrepondantActi" method="POST">
            @csrf
            @method('POST')
            <input type="hidden" value="{{$idtest}}" name="idTest">
            <button type="submit"  class="btn btn-primary">
               @lang('message.valider')
            </button>
          </form>
        {{-- Mail Relance --}}
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              
              <div class="modal-body">

                @lang('relance_mail')

              </div>
              <div class="modal-footer">
                <form role="form" method="POST" action="<% '/acti/sendMailRelanceActi' %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                  <input type="hidden" name="motif" value="<% mailMotif%>">
                  <button type="submit" class="btn btn-primary ">@lang('message.envoye_mail')</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
              </form>
              </div>
            </div>
          </div>
        </div>
        {{-- Modal Mail specifique --}}
        <div class="modal fade" id="mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-body">
                <% labelMotif %>       
            </div>
            <div class="modal-footer">
              <form role="form" method="POST" action="<% '/acti/sendMailSpecActi' %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                  <input type="hidden" name="motif" value="<% mailMotif%>">
                  <button type="submit" class="btn btn-primary ">@lang('message.envoye_mail')</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
              </form>
              
            </div>
          </div>
        </div>
      </div>
      {{-- Edition Modal --}}
      <div class="modal fade" id="edition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" method="POST" action="<% '/acti/editionRepondantActi/'+user.id %>"  >
                           @csrf
                           @method('POST')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.nom') *:</label>
                       <div class="col-sm-9">
                         <input type="text" name="lastname" class="form-control"
                               placeholder="Nom "  ng-model="user.lastname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row  has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">  
                    <label class="col-sm-3 col-form-label">@lang('message.prenom') *:</label>
                       <div class="col-sm-9">
                         <input type="text" name="firstname" class="form-control" 
                               placeholder="Prénom"  ng-model="user.firstname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.email') *:</label>
                      <div class="col-sm-9">
                        <input type="email" ng-model="user.email" name="email" class="form-control" 
                               placeholder="{{ trans('adminlte::adminlte.email') }}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.mdp') *:</label>
                       <div class="col-sm-9">
                         <input type="text" ng-model="user.code" name="password" class="form-control"
                               placeholder="Mot de passe">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       </div> 
                  </div>

                            {{-- Groupe --}}
                              <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang("message.groupe"):</label>
                                   <div class="col-sm-9">
                                    
                                      <select class="custom-select custom-select-lg mb-3 form-control" ng-model="user.groupe_id" name="groupe_id">
                                        @foreach($groupes as  $groupe)
                                          <option selected value="{{ $groupe->id }}">{{ strtoupper($groupe->abbreviation) }}</option>
                                        @endforeach
                                      </select>
                                
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('groupes') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div>
                              {{-- Abreviation groupe --}}
                            <!--   <div class="form-group row has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-sm-3 col-form-label">@lang('message.ab'):</label>
                                   <div class="col-sm-9">
                                     <input type="text" ng-model="abreviation"  name="abreviation" class="form-control"
                                           placeholder="Abbreviation groupe">
                                    <span class=" form-control-feedback"><i class="fa fa-bookmark"></i></span>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('abreviation') }}</strong>
                                        </span>
                                    @endif
                                   </div> 
                              </div> -->
                  <!-- PHONE -->
                  <div class="form-group row has-feedback {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.phone') :</label>
                      <div class="col-sm-9">
                        <input type="text" ng-model="user.phone" name="phone" class="form-control" 
                               placeholder="Téléphone">
                        <span class="glyphicon glyphicon glyphicon-phone-alt form-control-feedback"></span>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
         
            
                  <!-- CIVILITY -->
                  <div class="form-group row has-feedback {{ $errors->has('civility') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.civilite') :</label>
                      <div class="col-sm-9 row">
                        <!-- <input type="text" name="civility" class="form-control" value="{{ old('civility') }}"
                               placeholder="CIVILITY"> -->
                               <div class="col-sm-6">
                                 <input type="radio" ng-model="user.civility" class="custom-control-input" name="civility" value="@lang('message.mr')"
                                     >
                                <span for="huey">@lang('message.mr')</span>
                               </div>
                               <div class="col-sm-6">
                                 <input type="radio" g-model="user.civility" class="custom-control-input"  name="civility" value="@lang('message.mr')"
                                       >
                                 <span for="huey">@lang('message.mde')</span>
                               </div>
                        @if ($errors->has('civility'))
                            <span class="help-block">
                                <strong>{{ $errors->first('civility') }}</strong>
                            </span>
                        @endif
                      </div>
                  </div>
                  <!-- LANGUAGE -->
                  <div class="form-group row has-feedback {{ $errors->has('language') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">@lang('message.langue') :</label>
                      
                        <!-- <input type="text" name="language" class="form-control" value="{{ old('language') }}"
                               placeholder="LANGUAGE">
                        <span class="glyphicon glyphicon glyphicon-menu-hamburger form-control-feedback"></span> -->
                        <div class="col-sm-9 ">
                          <select class="custom-select custom-select-lg mb-3 form-control" ng-model="user.language" name="language">
                              <option selected value="fr">@lang('message.francais')</option>
                              <option value="en">@lang('message.anglais')</option>
                           </select>
                           @if ($errors->has('language'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('language') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div>
                  <!-- AVATAR FILE CHOOSER -->
    <!--               <div class="form-group row has-feedback {{ $errors->has('avatar') ? 'has-error' : '' }}">
                    <label class="col-sm-3 col-form-label">Avatar:</label>
                        <div class="col-sm-9 ">
                          <div class="custom-file">
                            <input type="file" name="user.avatar" class="custom-file-input" onchange="angular.element(this).scope().setImage(this)" id="editavatar">
                            <img src="<%image_source%>" alt="..." style="width: 80px;height: 80px;" >
                          </div>
                           @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                     
                  </div> -->
                  <input type="hidden" value="<%image_source%>" name="avatar_url">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <button ng-disabled="!user.lastname|| !user.lastname ||!user.email || !user.groupe_id" type="submit"
                          class="btn btn-primary btn-block btn-flat"
                  >@lang('message.send_save')</button>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
            </div>
          </div>
        </div>
      </div>
        <!-- Modal suppression -->

      <div class="modal fade" id="mailrelBenef" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_rel_benef->type) - {{ $mail_rel_benef->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                  @csrf 
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail4" id="mail4">
                  <input type="hidden" name="id" value="<% curMail %>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails_rel_benef as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rel_benef->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">
                        <div id="summernote3" name="text" ng-model="text"></div>
                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idTest" value="{{$idtest}}">
                    <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                    <input type="hidden" name="motif" value="<% mailMotif%>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>            
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="mailrelRep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_rel_rep->type) - {{ $mail_rel_rep->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                  @csrf 
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail3" id="mail3">
                  <input type="hidden" name="id" value="<% curMail %>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails_rel_rep as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rel_rep->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">

                        <div id="summernote4" name="text" ng-model="text"></div>

                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idTest" value="{{$idtest}}">
                    <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                    <input type="hidden" name="motif" value="<% mailMotif%>">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>            
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="mailinvBenef" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_inv_benef->type) - {{ $mail_inv_benef->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                  @csrf 
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail5" id="mail5">
                  <input type="hidden" name="id" value="<% curMail %>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails_inv_benef as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_inv_benef->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">
  
                        <div id="summernote5" name="text" ng-model="text">                        
                          
                        </div>

                        <!-- /. tools -->
                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idTest" value="{{$idtest}}">
                    <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                    <input type="hidden" name="motif" value="<% mailMotif%>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>            
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="mailinvRep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_inv_rep->type) - {{ $mail_inv_rep->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                  @csrf 
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail6" id="mail6">
                  <input type="hidden" name="id" value="<% curMail %>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails_inv_rep as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rep->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">
  
                        <div id="summernote6" name="text" ng-model="text">                        
                          
                        </div>

                        <!-- /. tools -->
                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idTest" value="{{$idtest}}">
                    <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                    <input type="hidden" name="motif" value="<% mailMotif%>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>            
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="mailThankBenef" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_benef->type) - {{ $mail_benef->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="template" id="template">
                    <input type="hidden" name="mail2" id="mail2">
                    <input type="hidden" name="id" value="<% curMail %>">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                        <div class="col-sm-10">
                          <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                            @foreach($mails_benef as $m)
                            <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
              
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                        <div class="col-sm-10">
                          <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_benef->subject }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                        <div class="col-sm-10">

                          <div id="summernote" name="text" ng-model="text">                        
                            
                          </div>

                          <!-- /. tools -->
                        </div>
                      </div>
                  
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="idTest" value="{{$idtest}}">
                      <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                      <input type="hidden" name="motif" value="<% mailMotif%>">
                      <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                      
                    </div>
                    <!-- /.box-footer -->
                  </form>            
                </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="mailThankRep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">

                  <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_rep->type) - {{ $mail_rep->language == 'fr' ? "Français" : "English"}}</h3>
                  <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMailActi')}}">
                  @csrf 
                  
                  <input type="hidden" name="template" id="template">
                  <input type="hidden" name="mail1" id="mail1">
                  <input type="hidden" name="id" value="<% curMail %>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                      <div class="col-sm-10">
                        <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                          @foreach($mails_rep as $m)
                          <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
            
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                      <div class="col-sm-10">
                        <input type="text" name="subject" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rep->subject }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                      <div class="col-sm-10">

                        <div id="summernote2" name="text" ng-model="text">                        
                          
                        </div>

                        <!-- /. tools -->
                      </div>
                    </div>
                
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idTest" value="{{$idtest}}">
                    <input type="hidden" name="idRepondant" value="<%mailRepondantId %>">
                    <input type="hidden" name="motif" value="<% mailMotif%>">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                    <button type="submit" class="btn btn-primary pull-right" ng-click="save($event)">@lang('message.send_save')</button>
                    
                  </div>
                  <!-- /.box-footer -->
                </form>            
              </div>

            </div>
          </div>
        </div>

        <div class="modal fade" id="multipleMail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <!-- <form class="form-horizontal" method="POST" action="{{route('QuestionnaireController.checkMails')}}"> -->
              @csrf 
            <div class="modal-content">
              <div class="modal-body">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li ng-if="beneficiaire && mailinvbenef" class="active"><a href="#invitBenef" data-toggle="tab">@lang("message.invitation beneficiaire")</a></li>
                    <li ng-if="beneficiaire && mailrelbenef"><a href="#relanceBenef" data-toggle="tab">@lang('message.relance beneficiaire')</a></li>
                    <li ng-if="beneficiaire && mailrembenef"><a href="#remerciementBenef" data-toggle="tab">@lang('message.remerciement beneficiaire')</a></li>
                    <li ng-if="simple  && mailinvrep"><a href="#invit" data-toggle="tab">@lang("message.invitation repondant")</a></li>
                    <li ng-if="simple  && mailrelrep"><a href="#relance" data-toggle="tab">@lang('message.relance repondant')</a></li>
                    <li ng-if="simple  && mailremrep"><a href="#remerciement" data-toggle="tab">@lang('message.remerciement repondant')</a></li>
                  </ul>
                  <div class="tab-content">
                    <input type="hidden" id="listeUser" name="listeUser" ng-model="listeUser">
                    <div class="tab-pane active" id="invitBenef">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_inv_benef->type) - {{ $mail_inv_benef->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">
                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail7" id="mail7">
                        <input type="hidden" name="idinvbenef" value="<% curMail %>">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="idTest" value="{{$idtest}}">
                        <input type="hidden" name="motifinvbenef" value="invitation beneficiaire">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>

                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail" ng-change="change()">
                                @foreach($mails_inv_benef as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectinvbenef" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_inv_benef->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">
        
                              <div id="summernote7" name="text" ng-model="text">                        
                                
                              </div>

                              <!-- /. tools -->
                            </div>
                          </div>    
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="relanceBenef" ng-click="relanceBenef()">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_benef->type) - {{ $mail_benef->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">
                        
                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail8" id="mail8">
                        <input type="hidden" name="idrelbenef" value="<% curMail2 %>">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="idTest" value="{{$idtest}}">
                        <input type="hidden" name="motifrelbenef" value="relance beneficiaire">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail2" ng-change="change()">
                                @foreach($mails_rel_benef as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectrelbenef" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rel_benef->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">
                              <div id="summernote8" name="text2" ng-model="text">                        
                                
                              </div>

                              <!-- /. tools -->
                            </div>
                          </div>
                      
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="remerciementBenef">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_benef->type) - {{ $mail_benef->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">

                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail9" id="mail9">
                        <input type="hidden" name="idrembenef" value="<% curMail3 %>">
                        <input type="hidden" name="motifrembenef" value="remerciement beneficiaire">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail3" ng-change="change()">
                                @foreach($mails_benef as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectrembenef" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_benef->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">

                              <div id="summernote9" name="text3" ng-model="text">                        
                                
                              </div>

                              <!-- /. tools -->
                            </div>
                          </div>
                      
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="invit">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_inv_rep->type) - {{ $mail_inv_rep->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">
                        
                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail10" id="mail10">
                        <input type="hidden" name="idinvrep" value="<% curMail4 %>">
                        <input type="hidden" name="motifinvrep" value="invitation repondant">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail4" ng-change="change()">
                                @foreach($mails_inv_rep as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectinvrep" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_inv_rep->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">

                              <div id="summernote10" name="text4" ng-model="text">                        
                                
                              </div>
                            </div>
                          </div>
                      
                        </div>
                    </div>
                    </div>
                    <div class="tab-pane" id="relance">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_rel_rep->type) - {{ $mail_rel_rep->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">                        
                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail11" id="mail11">
                        <input type="hidden" name="idrelrep" value="<% curMail5 %>">
                        <input type="hidden" name="motifrelrep" value="relance repondant">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail5" ng-change="change()">
                                @foreach($mails_rel_rep as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectrelrep" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rel_rep->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">

                              <div id="summernote11" name="text5" ng-model="text">                        
                                
                              </div>

                              <!-- /. tools -->
                            </div>
                          </div>
                      
                        </div>

                    </div>
                    </div>
                    <div class="tab-pane" id="remerciement">
                      <h3 class="box-title">@lang('message.modifier') mail :  @lang('message.'.$mail_rep->type) - {{ $mail_rep->language == 'fr' ? "Français" : "English"}}</h3>
                      <div class="form-horizontal">
                        
                        <input type="hidden" name="template" id="template">
                        <input type="hidden" name="mail12" id="mail12">
                        <input type="hidden" name="idremrep" value="<% curMail7 %>">
                        <input type="hidden" name="motifrelbenef" value="remerciement repondant">
                        <div class="box-body">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.historique')</label>
                            <div class="col-sm-10">
                              <select class="form-control" name="current" ng-model="curMail7" ng-change="change()">
                                @foreach($mails_rep as $m)
                                <option value="{{ $m->id }}">{{ $m->type }} {{ isset($m->user) ? "- ".$m->user : "" }} {{ isset($m->updated_at) ?  "- ".$m->updated_at : "" }}</option>
                  
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">@lang('message.sujet')</label>

                            <div class="col-sm-10">
                              <input type="text" name="subjectremrep" class="form-control" id="inputEmail3" placeholder="Sujet" value="{{ $mail_rep->subject }}">
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">@lang('message.contenu')</label>

                            <div class="col-sm-10">
        
                              <div id="summernote13" name="text7" ng-model="text">                        
                                
                              </div>

                              <!-- /. tools -->
                            </div>
                          </div>
                      
                        </div>
                        <!-- /.box-body -->
                      <!-- /.box-footer -->
                    </div>
                    </div>
                  </div>
                    
                </div>
                       
              </div>
              <div class="modal-footer">
                <div class="box-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.fermer')</button>
                  <button type="submit" class="btn btn-primary pull-right" ng-click="save2($event)">@lang('message.send_save')</button>  
                </div>
              </div>
            </div>
          <!-- </form> -->
          </div>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body" style="text-align: center;">
               @lang('message.supp_cons')
              </div>
              <div class="modal-footer">
                <form role="form" method="POST" action="<% '/acti/deleteFromTestActi/' %>"  >
                  @csrf
                  @method('POST')
                  <input type="hidden" name="idDelete" value="<% idDelete %>">
                  <input type="hidden" name="idTest" value="{{$idtest}}">
                  <button type="submit" class="btn btn-danger">Suppression</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('message.annuler')</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@stop
@section('js')
    <script> console.log('Hi!'); </script>
    <script type="text/javascript" src="/js/xlsx.full.min.js"></script>
    <script type="text/javascript">
      var all_mails = [];
      var app = angular.module('app', [],function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
      });

      app.controller('myCtrl', function($scope,$http) {

        $scope.mail = {};
        $scope.mails = {};

        $scope.selectedList = {};

        /////////////// LES SUMMERNOTES ET LES MAILS ID DOIVENT ETRE UNQUE ///////////////////////// 

        /////////////// pour les checklist ////////////////////////

        var mailinvbenef = [];
        var mailinvrep = [];
        var mailrelbenef = [];
        var mailrelrep = [];
        var mailrembenef = [];
        var mailremrep = [];

        $scope.mailsinvbenef = {};
        $scope.mailsrelbenef = {};
        $scope.mailsinvrep = {};
        $scope.mailsrelrep = {};
        $scope.mailsrembenef = {};
        $scope.mailsremrep = {};

        $scope.submit = function () {
          $scope.listeUser = [];
          console.log($scope.selectedList);
          angular.forEach($scope.selectedList, function (selected, user) {
            if (selected) {
               $scope.listeUser.push(user);
            }
          });
          console.log($scope.listeUser);
          // inv benef
          @foreach($mails_inv_benef as $m)
            all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote7').summernote('code', all_mails["{{ $mail_inv_benef->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote7').summernote('code', all_mails[valueSelected]);
          });

          $scope.curMail = "{{ $mail_inv_benef->id }}";
          $scope.subjectinvbenef = "{{ $mail_inv_benef->subject }}";
          $scope.text = $scope.mails[$scope.curMail];

          @foreach($mails_inv_benef as $m)
            $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

          // relance benef
          @foreach($mails_rel_benef as $m)
            mailrelbenef["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote8').summernote('code', mailrelbenef["{{ $mail_rel_benef->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote8').summernote('code', mailrelbenef[valueSelected]);
          });

          $scope.curMail2 = "{{ $mail_rel_benef->id }}";
          $scope.subjectrelbenef = "{{ $mail_rel_benef->subject }}";
          $scope.text2 = $scope.mailsrelbenef[$scope.curMail2];

          @foreach($mails_inv_benef as $m)
            $scope.mailsrelbenef["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

          // remercier benef
          @foreach($mails_benef as $m)
            mailrembenef["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote9').summernote('code', mailrembenef["{{ $mail_benef->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote9').summernote('code', mailrembenef[valueSelected]);
          });

          $scope.curMail3 = "{{ $mail_benef->id }}";
          $scope.subjectrembenef = "{{ $mail_benef->subject }}";
          $scope.text3 = $scope.mailsrembenef[$scope.curMail3];

          @foreach($mails_benef as $m)
            $scope.mailsrembenef["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

          // inv rep
          @foreach($mails_inv_rep as $m)
            mailinvrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote10').summernote('code', mailinvrep["{{ $mail_inv_rep->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote10').summernote('code', mailinvrep[valueSelected]);
          });

          $scope.curMail4 = "{{ $mail_inv_rep->id }}";
          $scope.subjectinvrep = "{{ $mail_inv_rep->subject }}";
          $scope.text4 = $scope.mailsinvrep[$scope.curMail4];

          @foreach($mails_inv_rep as $m)
            $scope.mailsinvrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

          // relance rep
          @foreach($mails_rel_rep as $m)
            mailrelrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote11').summernote('code', mailrelrep["{{ $mail_rel_rep->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote11').summernote('code', mailrelrep[valueSelected]);
          });

          $scope.curMail5 = "{{ $mail_rel_rep->id }}";
          $scope.subjectrelrep = "{{ $mail_rel_rep->subject }}";
          $scope.text5 = $scope.mailsrelrep[$scope.curMail5];

          @foreach($mails_rel_rep as $m)
            $scope.mailsrelrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach

          // remerciement rep
          @foreach($mails_rep as $m)
            mailremrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach
          $('#summernote13').summernote('code', mailremrep["{{ $mail_rep->id }}"]);
          $('select').on('change', function (e) {
            valueSelected = this.value;
            $("#mail_id").val(valueSelected);
            $('#summernote13').summernote('code', mailremrep[valueSelected]);
          });

          $scope.curMail7 = "{{ $mail_rep->id }}";
          $scope.subjectremrep = "{{ $mail_rep->subject }}";
          $scope.text7 = $scope.mailsremrep[$scope.curMail7];

          @foreach($mails_rep as $m)
            $scope.mailsremrep["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
          @endforeach   

          /////////////////////////////////    

        };

        let localhostUrl=window.location.protocol + '//' + window.location.host //'http://localhost:8000'; 
        //console.log('locahostUrl',localhostUrl)

        let listOfUser= [];
        $scope.idTest = document.getElementById("idTest").value;
        var testrepondant = JSON.parse(document.getElementById("testRepondant").value);
        for(var i=0 ;i < testrepondant.length;i++){
            if(testrepondant[i].test_id == $scope.idTest){
              listOfUser.push(testrepondant[i]);
            }
          }
        console.log("listeOfuser", listOfUser);
        $scope.listOfUser = listOfUser;
        $scope.listOfUser =[];
        $scope.beneficiaires=[];
        $scope.checkrelance=[];
        $scope.user={};
        $scope.enableCheckrelanceValue = false;

        $scope.beneficiaire = false;
        $scope.mailinvbenef = false;
        $scope.mailrelbenef = false;
        $scope.mailrembenef = false;
        $scope.mailinvrep = false;
        $scope.mailrelrep = false;
        $scope.mailremrep = false;
        $scope.simple = false;

        $scope.configMail2 = function (motif,type){

          if(type === "Beneficiaire"){
            $scope.beneficiaire = true;
            switch (motif) {
              case 'invitation':
                $scope.mailinvbenef = true;
                break;
              case 'relance':
                $scope.mailrelbenef = true;
                break;
              case 'remerciement':
                $scope.mailrembenef = true;
                break;
              default:
            }
          } else {
            $scope.simple = true;
            switch (motif) {
              case 'invitation':
                $scope.mailinvrep = true;
                break;
              case 'relance':
                $scope.mailrelrep = true;
                break;
              case 'remerciement':
                $scope.mailremrep = true;
                break;
              default:
            }
          }
        }

        $scope.enableCheckrelance= function(){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if($scope.checkrelance[listOfUser[i].repondant_id]== true){
              retour = true;
              break;
            }
          }
          $scope.enableCheckrelanceValue = retour;
        }

        $scope.configMail = function (idRepondant,idTest,motif,userType){
          $scope.mailRepondantId=idRepondant;
          $scope.mailTestId=idTest;
          $scope.mailMotif=motif;
          $scope.userType=userType;
          switch (motif) {
            case 'invitation':
              $scope.labelMotif = "Inviter";

              if(userType == "beneficiaire"){

                @foreach($mails_inv_benef as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote5').summernote('code', all_mails["{{ $mail_inv_benef->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote5').summernote('code', all_mails[valueSelected]);
                });

                $scope.curMail = "{{ $mail_inv_benef->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_inv_benef as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach

              } else {

                $scope.curMail = "{{ $mail_inv_rep->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_inv_rep as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote6').summernote('code', all_mails["{{ $mail_inv_rep->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote6').summernote('code', all_mails[valueSelected]);
                });

                @foreach($mails_inv_rep as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
              }

              break;
            case 'relance':
              $scope.labelMotif = "Relancer";

              if(userType == "beneficiaire"){

                @foreach($mails_rel_benef as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote3').summernote('code', all_mails["{{ $mail_rel_benef->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote3').summernote('code', all_mails[valueSelected]);
                });

                $scope.curMail = "{{ $mail_rel_benef->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_rel_benef as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach

              } else {

                $scope.curMail = "{{ $mail_rel_rep->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_rel_rep as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote4').summernote('code', all_mails["{{ $mail_rel_rep->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote4').summernote('code', all_mails[valueSelected]);
                });

                @foreach($mails_rel_rep as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
              }

              break;
            case 'remerciement':
              $scope.labelMotif = "Remercier";

              if(userType == "Beneficiaire"){

                @foreach($mails_benef as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote').summernote('code', all_mails["{{ $mail_benef->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote').summernote('code', all_mails[valueSelected]);
                });

                $scope.curMail = "{{ $mail_benef->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_benef as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach

              } else {

                @foreach($mails_rep as $m)
                  all_mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
                $('#summernote2').summernote('code', all_mails["{{ $mail_rep->id }}"]);
                $('select').on('change', function (e) {
                  valueSelected = this.value;
                  $("#mail_id").val(valueSelected);
                  $('#summernote2').summernote('code', all_mails[valueSelected]);
                });

                $scope.curMail = "{{ $mail_rep->id }}";
                $scope.text = $scope.mails[$scope.curMail];

                @foreach($mails_rep as $m)
                  $scope.mails["{{ $m->id }}"] = <?php echo json_encode($m->mail) ?>;
                @endforeach
              }

              break;
            default:
          }

          console.log(idRepondant)
          console.log(idTest)
          console.log(motif)
        }

        $scope.change = function(){
          $scope.text = $scope.mails[$scope.curMail];
          $scope.text2 = $scope.mails[$scope.curMail2];
        }

        $scope.save = function(e){

            //e.preventDefault();
            
            $("#mail6").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail5").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail4").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail3").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail2").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail1").val($(".note-editable").eq(0).html())[0].defaultValue;
            $("#mail").val($(".note-editable").eq(0).html())[0].defaultValue;

            
        }

        $scope.save2 = function(e){
          // $("#mail12").val($(".note-editable").eq(0).html())[0].defaultValue;
          // $("#mail11").val($(".note-editable").eq(0).html())[0].defaultValue;
          // $("#mail10").val($(".note-editable").eq(0).html())[0].defaultValue;
          // $("#mail9").val($(".note-editable").eq(0).html())[0].defaultValue;
          // $("#mail8").val($(".note-editable").eq(0).html())[0].defaultValue;
          // $("#mail7").val($(".note-editable").eq(0).html())[0].defaultValue;
          let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          let data = {
            _token: CSRF_TOKEN,
            idinvbenef : $scope.curMail,
            idinvrep : $scope.curMail4,
            idrelbenef : $scope.curMail2,
            idrelrep : $scope.curMail5,
            idrembenef : $scope.curMail3,
            idremrep : $scope.curMail7,
            idTest : {{$idtest}},

            subjectinvbenef : $scope.subjectinvbenef,
            subjectinvrep : $scope.subjectinvrep,
            subjectrelbenef : $scope.subjectrelbenef,
            subjectrelrep : $scope.subjectrelrep,
            subjectrembenef : $scope.subjectrembenef,
            subjectremrep : $scope.subjectremrep,   

            mail7 : $("#mail7").val($(".note-editable").eq(0).html())[0].defaultValue,
            mail8 : $("#mail8").val($(".note-editable").eq(0).html())[0].defaultValue,
            mail9 : $("#mail9").val($(".note-editable").eq(0).html())[0].defaultValue,
            mail10 : $("#mail10").val($(".note-editable").eq(0).html())[0].defaultValue,
            mail11 : $("#mail11").val($(".note-editable").eq(0).html())[0].defaultValue,
            mail12 : $("#mail12").val($(".note-editable").eq(0).html())[0].defaultValue,

            listeUser : $scope.listeUser
          }


          $http({
            method: 'POST',
            async : false,
            data: data,
         
            url: "{{ url('acti/checkMailsActi') }}",
          }).then(function successCallback(response) {
            location.reload();            
          }, function errorCallback(response) {

          });

        }

        $scope.haveSend = function haveSend(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id && listOfUser[i].thanks_mail_send == 1){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        }
        $scope.isThanksSend= function isThanksSend(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id && listOfUser[i].reponse == 1){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        }

        $scope.isMailSend= function isMailSend(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id && listOfUser[i].mail_send == 1){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        }
        $scope.isUserIdHere=function isUserhere(id){
          let retour = false;
          for(var i=0;i<listOfUser.length;i++){
            if (id == listOfUser[i].repondant_id){
              retour= true;
              console.log(id);
              break;
            }
          }
          return retour;
        };

        function setBeneficiaire(data){
          console.log("dataObjects",data);
          console.log("dataObjects stringify",JSON.stringify(data));
          var numlignebeneficiaire=0;
          var numlignecoach=0;
          var beneficiaire = [];
          //test du debut de l'information
          for( var i=0;i<data.length;i++){
            if(data[i].B =="1. Informations du bénéficiaire de la démarche"|| data[i].__rowNum__==13 ){ 
              numlignebeneficiaire = i;
              break;
            }
          }
          //test du limite de l'information
          for (i = numlignebeneficiaire;i<data.length;i++){
            if(data[i].A =="civilitecoach"|| data[i].__rowNum__==22 ){ 
              numlignecoach = i;
              break;
            }
          }
          //complete l objet beneficiaire
          for(i=numlignebeneficiaire+1;i<numlignecoach;i++){
            beneficiaire.push(data[i]);
          }
          return beneficiaire;
          
        }
        function setRepondant(data){
            console.log("dataRange",data);
            var numlignerepondant=0;
          var numlignelimiterepondant=0;
          var repondant=[];
          for(var i = 0 ; i<data.length;i++){
            if(data[i].B =="nom du groupe de répondants (1)"|| data[i].__rowNum__==34 ){ 
              numlignerepondant = i;
              break;
            }
          }
          for(i=numlignerepondant+1;i<data.length;i++){
            if(data[i].B =="Légende"){ 
              numlignelimiterepondant = i;
              break;
            }
          }
          for(i= numlignerepondant+1;i<numlignelimiterepondant;i++){
            if(data[i].G){
              repondant.push(data[i]);
            }
          }
          console.log("repondant",repondant);
          return repondant;
        }
        $scope.deleteData = function function_name(event,id) {
          // body...
          $scope.idDelete = id;
          console.log(id);
        }
        $scope.setImage = function(element) {
             $scope.image_source="";
           $scope.currentFile = element.files[0];
           var reader = new FileReader();

          reader.onload = function(event) {
            $scope.image_source = event.target.result
            console.log($scope.image_source);
            $scope.$apply();
          }
          // when the file is read it triggers the onload event above.
          reader.readAsDataURL(element.files[0]);
        }

        $scope.editRepondant = function(user){
          $scope.user = user;
          $scope.user.groupe_id = user.groupe.id.toString();
          console.log("edit user %o", user);
        }
        // $scope.editRequest = function launch(event,id){
          
        //   $scope.id = id;
        //   console.log(id);
        //   $http({
        //   method: 'GET',
        //   url: localhostUrl+'/consultant/getData/'+id
        //   }).then(function successCallback(response) {
        //       // this callback will be called asynchronously
        //       // when the response is available
        //       console.log("reponses : %o",response);
        //       $scope.editfirstname =response.data.firstname; 
        //       $scope.editlastname =response.data.lastname; 
        //       $scope.editemail =response.data.email; 
        //       $scope.editphone =response.data.phone; 
        //       $scope.editorganisation =response.data.organisation; 
        //       $scope.editcivility =response.data.civility; 
        //       $scope.editlanguage =response.data.language; 
        //       $scope.edituserid =response.data.edituserid;  
        //       $scope.image_source=response.data.avatar_url;
        //       $scope.$apply();
        //       console.log("$scope.image_source",$scope.image_source);
        //     }, function errorCallback(response) {
        //       $scope.image_source="";
        //       // called asynchronously if an error occurs
        //       // or server returns response with an error status.
        //       console.log(response);
        // });
        // }
        $scope.setFile = function(element) {
          
             console.log(element.files[0]);
           var file = element.files[0];
             var reader = new FileReader();  
        
             reader.onload = function (e) {  
        
                  var data = e.target.result;  
        
                  var workbook = XLSX.read(data, { type: 'binary' });  
                console.log("workbook",workbook);
                  var first_sheet_name = workbook.SheetNames[0];  
              console.log("first_sheet_name",first_sheet_name);
                  var dataObjects = XLSX.utils.sheet_to_json(workbook.Sheets[first_sheet_name],{header:"A"});
                  $scope.beneficiaires = setBeneficiaire(dataObjects);
                  $scope.repondants= setRepondant(dataObjects);
                  $scope.$apply();
                  
              }  
              reader.onerror = function (ex) {  
        
              }  
              reader.readAsBinaryString(file);   
              
        }
});
    </script>
@stop
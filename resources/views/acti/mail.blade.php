{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
	<img src="/img/apter_solutions_android-2-tranparent.png"srcset="/img/apter_solutions_android-2-tranparent@2x.png 2x,img/apter_solutions_android-2-tranparent@3x.png 3x" class="apter_solutions_android-2-tranparent">
@stop
@section('content')
    <div class="row">
      <div class="col-md-6">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">@lang('message.fr_mail')</a></li>
              <li><a href="#tab_2" data-toggle="tab">@lang('message.eng_mail')</a></li>
              
          
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <table class="table table-hover table-responsive">
                  <tr>
                    <th></th>
                    <th>Type</th>
                    <th></th>
                   
                  </tr>
                  
                  @foreach($mails_fr as $mail)
                  <tr>
                    <td></td>
                    <td>@lang('message.'.$mail->type)</td>
                    <td>
                      <a href="{{url('/acti/mail/'.$mail->id.'/edit')}}"> <i class="fa fa-edit"></i> @lang('message.modifier')</a>
                    </td>
                  </tr>
                  @endforeach
                   
                    
                </table>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <table class="table table-hover table-responsive">
                  <tr>
                    <th></th>
                    <th>Type</th>
                    <th></th>
                   
                  </tr>
                  
                  @foreach($mails_en as $mail)
                  <tr>
                    <td></td>
                    <td>@lang('message.'.$mail->type)</td>
                    <td>
                      <a href="{{url('/acti/mail/'.$mail->id.'/edit')}}"> <i class="fa fa-edit"></i> @lang('message.modifier')</a>
                    </td>
                  </tr>
                  @endforeach            
                </table>
              </div>
              <!-- /.tab-pane -->
            
            </div>
            <!-- /.tab-content -->
        </div>


      </div>

    </div>
@stop
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet"  href="/css/trix.css">
	<link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

{{-- resources/views/admin/dashboard.blade.php --}}
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <h1>
    <span class="glyphicon glyphicon-stats"></span>
    @lang('message.perf_motiv')      
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"> <span class="glyphicon glyphicon-stats"></span> @lang('message.perf_motiv')</a></li>
    <li class="active">@lang('message.liste')</li>
  </ol>
@stop
@section('content')
	<div ng-app="actiApp" ng-controller="actiController">
		<div>
      @if(session('error_code'))
        <div class="alert alert-danger">
          <div class="alert-icon">
            <i class="material-icons">error_outline</i>
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
          <b>{{ session('error_code') }}</b>
        </div>
      @endif
    </div>
    <div class="row">
      @if(Auth::user()->role !== 'admin_acad')
        <div class="col-sm-12 col-md-6 text-left">
          <button class="btn btn-primary" ng-click="addActi()" data-toggle="modal" data-target="#add-acti"><i class="fa fa-plus"></i> @lang('message.ajouter') </button>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group">
            <div class="input-group" >
              <input type="text" class="form-control timepicker" id="myInput" onkeyup="myFunction()">

              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>
      @endif
        
      @foreach ($tests as $f=>$acti)
        <div class="col-sm-12 col-md-6"  id="h3_{{$f}}">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $acti->name }}</h3>
              <p class="text-secondary">Date : {{ $acti->created_at }}</p>
              <p class="text-secondary">Nombre réponses : {{ $acti->nbReponses }}</p>
            
              @if(Auth::user()->role !== 'admin_acad')
                <div class="box-tools pull-right">
                  <span class="label label-default" ng-click="editActi(<?php echo htmlspecialchars(json_encode($acti)) ?>)" data-toggle="modal" data-target="#edit-acti"><i class="fa fa-pencil"></i></span>
                  <span class="label label-default" ng-click="deleteActi(<?php echo htmlspecialchars(json_encode($acti)) ?>)" data-toggle="modal" data-target="#delete-acti"><i class="fa fa-trash"></i></span>
                </div>
              @endif
            </div>
            <div class="box-body text-right row">
              <a href="/acti/repondantActi/{{$acti->id}}"  class="text-warning col-sm-3"> <span class="glyphicon glyphicon-user"></span> @lang('message.repondants')</a>
              <a style="cursor: pointer;" ng-click="result('/acti/rapportActi/{{$acti->id}}/pdf', {{ $acti->nbReponses }})" class="text-warning col-sm-3"> <span class="glyphicon glyphicon-stats"></span> @lang('message.resultat') PDF</a>
              <button style="cursor: pointer; background-color:white;border:none;" ng-if="!loading && !rapportPptx" ng-click="generatePptx(<?php echo htmlspecialchars(json_encode($acti->id)) ?>)" class="text-warning col-sm-3"> <span class="glyphicon glyphicon-stats"></span> @lang('message.resultat') PPTX</button>
              <!-- <button class="btn btn-primary" ng-if="!loading && !rapportPptx" ng-click="generatePptx(<?php echo htmlspecialchars(json_encode($acti->id)) ?>)">
                Générer le rapport pptx
              </button> -->
              <a class="col-sm-3" style="cursor: pointer; background-color:white;border:none; color:#FFC107 !important" ng-if="loading" disabled>
               <i class="fa fa-spinner fa-spin"></i> Chargement ...
              </a>

              <a ng-if="rapportPptx" style="cursor: pointer; background-color:white;border:none; color:#FFC107 !important" href="{% pptx %}" style="color: white" class="col-sm-3">
                <i class="fa fa-download"></i> Télécharger le rapport pptx
              </a>

              <a href="/acti/resultatActi/{{$acti->id}}" class="text-warning col-sm-3"> <span class="glyphicon glyphicon-sort-by-attributes-alt"></span> @lang("message.tabRep")</a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    {{ $tests->links() }}

		<div class="modal fade" id="edit-acti">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> <i class="fa fa-plus"></i> @lang('message.modifier') {% acti.name %} </h4>
          </div>
          <form role="form" action="{% '/acti/'+acti.id %}" method="POST">
            @csrf
            @method('PUT')
            <div class="modal-body">
              <div class="form-group">
                <label for="name">@lang('message.titre')</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="" ng-model="acti.name">
              </div>
              <div class="form-group">
                <label for="code">Code</label>
                <input type="text" class="form-control" name="code" id="code" placeholder="" ng-model="acti.code">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                <button type="submit" class="btn btn-primary" ng-disabled="!acti.name || !acti.code">@lang('message.modifier')</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="delete-acti">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> <i class="fa fa-trash"></i> Suppression</h4>
          </div>
          <form role="form" action="{% '/acti/'+acti.id %}" method="POST">
            @csrf
            @method('DELETE')
            <div class="modal-body">
              <p>@lang('message.sup_climage') {% acti.name %}?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
              <button type="submit" class="btn btn-primary">@lang('message.supprimer')</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="add-acti">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> <i class="fa fa-plus"></i> @lang('message.ajout_acti')</h4>
          </div>
          <form role="form" action="{{ route('acti.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="modal-body">
              <div class="form-group">
                <label for="name">@lang('message.titre')</label>
                <input type="text" class="form-control" ng-change="checkValue()" name="name" id="name" placeholder="" ng-model="acti.name">
              </div>
              <div class="form-group">
                <label for="code">Code</label>
                <input type="text" class="form-control" ng-change="checkValue()" name="code" id="code" placeholder="" ng-model="acti.code">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">@lang('message.annuler')</button>
                <button type="submit" class="btn btn-primary" ng-disabled="checkValue()">@lang('message.ajouter')</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
	</div>
@stop
@section('js')
  <script> console.log('Hi!'); 
    function myFunction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        h3 = document.getElementsByTagName("h3");
        for (i = 0; i < h3.length; i++) {
          a = h3[i];
          txtValue = a.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            document.getElementById("h3_"+i).style.display = "";
              //h3[i].style.display = "";
          } else {
            document.getElementById("h3_"+i).style.display = "none";
              //h3[i].style.display = "none";
          }
        }
      }
  </script>
  <script type="text/javascript">
    var actiApp = angular.module('actiApp', [], function($interpolateProvider) {
      $interpolateProvider.startSymbol('{%');
      $interpolateProvider.endSymbol('%}');
    });
    actiApp.controller('actiController', function($scope,$http) {
      $scope.loading = false;
      $scope.acti = {};

      $scope.test = {};

	    $scope.editActi = function(acti){
	      $scope.acti = acti;
	    }

	    $scope.deleteActi = function(acti){
	      $scope.acti = acti;
	    }

	    $scope.addActi = function(){
	      $scope.acti ={}
	    }

      $scope.result = function(url, nbReponses){
        console.log("url %o, nbrep %o", url, nbReponses);
        if(nbReponses > 0){
          window.open(url); 
        }else{
          let message = "@lang('message.not_responding')";
          alert(message);
        }
      }
      $http({
        method: 'GET',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        dataType: 'json',
        url: window.location.protocol + "//" + window.location.hostname+ (window.location.port ? ':' + window.location.port: '')+"/acti/codes"
      }).then(function successCallback(response) {
        $scope.test = response.data;
        console.log($scope.test);
      }, function errorCallback(response) {

            //console.log(response);
      });

      $scope.checkValue = function(){
        let retour = false;
        for(var i= 0; i< $scope.test.length; i++){
          if($scope.test[i].code == $scope.acti.code || $scope.acti.name == $scope.test[i].name){
            retour = true;
            break;
          }
        }
        console.log(retour);
        return retour;
      }

      $scope.generatePptx = function(id){
        $scope.loading = true;
        let data = {
          id : id
        }

        $http({
          method: 'POST',
          async : false,
          data: data,
          url: '{{ url("acti/rapportActiPPTX/") }}',
        }).then(function successCallback(response) {
            console.log("la reponse "+JSON.stringify(response));
            $scope.loading = false;
          if(response.data.generated){
              console.log(response.data.doc);
              $scope.rapportPptx = true;
              $scope.pptx = response.data.doc;
          }
          else{
            $scope.error = response.data.message;
          }
        }, function errorCallback(response) {

        });
      }

    });
  </script>
@stop
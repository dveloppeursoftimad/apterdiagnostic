<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Test;
use App\User;
use App\Type;
use App\ReponseRepondant;
use App\Question;
use App\Reponse;
use App\TestRepondant;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpWord\Shared\ZipArchive;

class ClimageController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function d($var){
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::where("name", "like", "%climage%")->get();

        /*$climages = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%climage%");
        })->latest()->get();*/

        if(Auth::user()->role == "consultant" || Auth::user()->role == "partenaire" || Auth::user()->role == "admin_rech_acad"){

            $climages = Test::whereHas('type', function ($query) {
                $query->where('name', 'like', "%climage%");
            })->where("consultant_id", "=", Auth::user()->id)->orderBy("created_at","desc")->paginate(10);   

        } else if(Auth::user()->role == "admin" || Auth::user()->role == "admin_acad"){
            $climages = Test::whereHas('type', function ($query) {
                $query->where('name', 'like', "%climage%");
            })->orderBy("created_at","desc")->paginate(10);            
        }

        if(isset($climages)){
            foreach ($climages as $key => $climage) {
                $cons = User::find($climage->consultant_id);
                $climage->user = $cons->firstname." ".$cons->lastname;
            }
        }
        
        return view('climage/index', ["climages" => $climages, "types" =>  $types]);
    }


    public function tableauClimage2($id){

        $climage = Test::find($id);
        $type = Type::find($climage->type_id);

        // RESULTAT 1 
        $result1 = array();
        $page = Type::where('name', $type->name)->first()->pages[1];
            
        foreach ($page->questions as $qkey => $question) {
            foreach ($question->reponses as $rep => $reponse) {
                $repondants = ReponseRepondant::where("test_id",$id)->where("reponse_id","=", $reponse->id)->get();

                foreach ($repondants as $key => $value) {

                    $hOnly = explode(":",$value->updated_at);
                    $newH = $hOnly[0].":00:00";
                    if(is_null($value->reponse_id)){
                        continue;
                    }

                    if(!array_key_exists($newH,$result1)){
                        $result1[$newH] = [];
                        $result1[$newH][$value->user->username] = [];
                        $result1[$newH][$value->user->username][$value->reponse_id] = true;
                    } else {
                        if(!array_key_exists($value->user->username,$result1[$newH])){
                            $result1[$newH][$value->user->username] = [];
                            $result1[$newH][$value->user->username][$value->reponse_id] = true;   
                        } else {
                            $result1[$newH][$value->user->username][$value->reponse_id] = true;
                        }
                    } 
                }
            }
        }

        // RESULTAT 2 
        $result2 = array();
        $page2 = Type::where('name', $type->name)->first()->pages[2];
            
        foreach ($page2->questions as $qkey => $question) {
            foreach ($question->reponses as $rep => $reponse) {
                $repondants = ReponseRepondant::where("test_id",$id)->where("reponse_id","=", $reponse->id)->get();

                foreach ($repondants as $key => $value) {

                    $hOnly = explode(":",$value->updated_at);
                    $newH = $hOnly[0].":00:00";
                    if(is_null($value->reponse_id)){
                        continue;
                    }

                    if(!array_key_exists($newH,$result2)){
                        $result2[$newH] = [];
                        $result2[$newH][$value->user->username] = [];
                        $result2[$newH][$value->user->username][$value->reponse_id] = true;
                    } else {
                        if(!array_key_exists($value->user->username,$result2[$newH])){
                            $result2[$newH][$value->user->username] = [];
                            $result2[$newH][$value->user->username][$value->reponse_id] = true;   
                        } else {
                            $result2[$newH][$value->user->username][$value->reponse_id] = true;
                        }
                    } 
                }
            }
        }



        // RESULTAT 3
        $question3 = Type::where('name', $type->name)->first()->pages[2]->questions[0];
        $result3 = array();
            
        foreach ($question3->reponses as $key => $reponse) {
            $repondants = ReponseRepondant::where("test_id",$id)->where("reponse_id","=", $reponse->id)->get();

            foreach ($repondants as $key => $value) {

                $hOnly = explode(":",$value->updated_at);
                $newH = $hOnly[0].":00:00";
                if(is_null($value->reponse_id)){
                    continue;
                }

                if(!array_key_exists($newH,$result3)){
                    $result3[$newH] = [];
                    $result3[$newH][$value->user->username] = [];
                    $result3[$newH][$value->user->username][$value->reponse_id] = true;
                } else {
                    if(!array_key_exists($value->user->username,$result3[$newH])){
                        $result3[$newH][$value->user->username] = [];
                        $result3[$newH][$value->user->username][$value->reponse_id] = true;   
                    } else {
                        $result3[$newH][$value->user->username][$value->reponse_id] = true;
                    }
                } 
            }
        }

        return view("climage/tableau2", ["lang" => \App::getLocale(), "page" => $page, "page2" => $page2, "question3" => $question3,"climage"=>$climage, "resultats"=>$result1,"resultats2"=>$result2, "resultats3" => $result3]);

    }

    public function tableauClimage($id){
        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        $pages = Type::where('name', $type->name)->first()->pages;
        $dates = ReponseRepondant::where("test_id", $id)->distinct()->select("updated_at")->get();

        $result1 = array();
        $allresults = ReponseRepondant::where("test_id",$id)->get();

        //foreach ($users as $key => $user) {
            // LES RESULTAT 1

        foreach ($allresults as $key => $value) {
            $hOnly = explode(":",$value->updated_at);
            $newH = $hOnly[0].":00:00";
            if(is_null($value->reponse_id)){
                continue;
            }
            if(!array_key_exists($newH,$result1)){
                $result1[$newH] = [];
                $result1[$newH][$value->user->username] = [];
                $result1[$newH][$value->user->username][$value->reponse_id] = true;
            } else {
                if(!array_key_exists($value->user->username,$result1[$newH])){
                    $result1[$newH][$value->user->username] = [];
                    $result1[$newH][$value->user->username][$value->reponse_id] = true;   
                } else {
                    $result1[$newH][$value->user->username][$value->reponse_id] = true;
                }
            }
        }

        return view("climage/tableau", ["lang" => \App::getLocale(), "page2" => $pages,"climage"=>$climage, "resultats"=>$result1]);
    }

    public function resultat($id){
        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        $nbUser = count(TestRepondant::where("test_id", $id)->get());

        $nbUsers = ReponseRepondant::where("test_id", $id)->distinct()->get();

        $page2 = Type::where('name', $type->name)->first()->pages[1];
        $page3 = Type::where('name', $type->name)->first()->pages[2];
        $dates = ReponseRepondant::where("test_id", $id)->distinct()->select("updated_at")->get();
        // graph data
        $question3 = Type::where('name', $type->name)->first()->pages[2]->questions[0];
        if($climage->type->name == "climage-interlocuteur"){
            $question4 = Type::where('name', $type->name)->first()->pages[2]->questions[1];
        }
        $countCli = count(Type::where('name', $type->name)->first()->pages[2]->questions);
        if($countCli > 1){
            $question4 = Type::where('name', $type->name)->first()->pages[2]->questions[1];
        }

        $dateArray = array();
        $dateArray2 = array();
        foreach ($dates as $kd => $date) {
            $day = explode(" ",$date->updated_at);
            $d = $day[0];
            if(!in_array($d, $dateArray)){
                $getHeures = ReponseRepondant::where("test_id",$id)->where("updated_at","like","%".$d."%")->get();
                $hArray = array();
                $repVal = [];
                $vote = [];
                $data = [];
                $dataGraph = [];
                $dataCli = [];
                $dataClimage = [];
                $dataVote = [];
                $nbRepondants = array();
                foreach ($getHeures as $kh => $heure) {
                    $hr = explode(" ",$heure->updated_at);
                    $h = $hr[1];
                    if(!in_array($h, $hArray)){
                        $a = 0;$c = 0;$g = 0;$o = 0;
                        $i = 0;$de = 0;$he = 0;$p = 0;
                        $b = 0;$e = 0;$k = 0;$q = 0;
                        $j = 0;$f = 0;$n = 0;$m = 0;
                        $countSerieux = 0;
                        $countEnjoue = 0;
                        $countSoi = 0;
                        $countAutrui = 0;

                        $nbRepondant = ReponseRepondant::where("test_id",$id)->where("updated_at","like","%".$d." ".$h."%")->get();
                        $users = array();
                        foreach ($nbRepondant as $key => $nbRep) {
                            if(!in_array($nbRep->repondant_id,$users)){
                                array_push($users, $nbRep->repondant_id);
                            }
                        }

                        // $this->d($d);
                        // $this->d($h);
                        // $this->d($users);
                        // echo "######################";


                        $countUsers = count($users);
                        
                        foreach ($page2->questions as $qkey => $question) {

                            //$nbRepQuest = ReponseRepondant::where("test_id", $id)->where("question_id", $question->id)->where("updated_at","like","%".$d." ".$h."%")->count();
                            //$nbQuestTot = $nbRepQuest / $nbUser;

                            $count = 0;
                            foreach ($question->reponses as $rep => $reponse) {

                                $repInterlocuteur = \DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get();

                                foreach ($repInterlocuteur as $key => $value) {
                                    $valScore = Reponse::where("question_id",$value->question_id)->where("id",$value->reponse_id)->first();
                                    (($qkey == 0 ? $dataVote["q1"] = $countSerieux + $valScore->score : ($qkey == 1 ? $dataVote["q2"] = $countEnjoue + $valScore->score : ($qkey == 2 ? $dataVote["q3"] = $countSoi + $valScore->score : ($qkey == 3 ? $dataVote["q4"] = $countAutrui + $valScore->score : 0)))));
                                }

                                switch ($count) {
                                    case 0:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        // if(!array_key_exists($key,$dataV)){
                                        //     $dataV[$key] = $nb;
                                        // } else {
                                        //     $dataV[$key] += $nb;
                                        // }

                                        $qkey == 0 ? $dataVote["a"] = $nb : $dataVote["i"] = $nb;
                                        break;

                                    case 1:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["b"] = $nb : $dataVote["j"] = $nb;
                                        break;

                                    case 2:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["c"] = $nb : $dataVote["k"] = $nb;
                                        break;

                                    case 3:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["d"] = $nb : $dataVote["m"] = $nb;
                                        break;

                                    case 4:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["e"] = $nb : $dataVote["n"] = $nb;
                                        break;

                                    case 5:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["f"] = $nb : $dataVote["o"] = $nb;
                                        break;

                                    case 6:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["g"] = $nb : $dataVote["p"] = $nb;
                                        break;

                                    case 7:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["h"] = $nb : $dataVote["q"] = $nb;
                                        break;
                                    
                                    default:
                                        # code...
                                        break;
                                }

                                $count += 1;

                                $nb = count(\DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get());
                                $reponse->vote = floatval($nb /$countUsers)*100;
                                if(!array_key_exists($reponse->id,$repVal)){
                                    $repVal[$rep][$question->id] = number_format($reponse->vote, 2); 
                                }
                            }
                        }

                        $countConforme = 0;
                        $countTransgressif = 0;
                        $countSympha = 0;
                        $countMaitrise = 0;
                        foreach ($page3->questions as $qkey => $question) {
                            $count = 0;
                            foreach ($question->reponses as $rep => $reponse) {
                                $repInterlocuteur2 = \DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get();

                                foreach ($repInterlocuteur2 as $key => $value) {
                                    $valScore = Reponse::where("question_id",$value->question_id)->where("id",$value->reponse_id)->first();
                                    (($qkey == 0 ? $dataVote["q5"] = $countConforme + $valScore->score : ($qkey == 1 ? $dataVote["q6"] = $countTransgressif + $valScore->score : ($qkey == 2 ? $dataVote["q7"] = $countSympha + $valScore->score : ($qkey == 3 ? $dataVote["q8"] = $countMaitrise + $valScore->score : 0)))));
                                }
                            }
                        }

                        array_push($vote, $repVal);

                        if($nbUser >0){

                            $cur = 1;

                            foreach ($question3->reponses as $key => $reponse) {
                                switch ($cur) {
                                    case 1:
                                        # 3-a
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['serieux'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["1"] = $nb;
                                      
                                        break;
                                    case 2:
                                        # 3-b
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['enjoue'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["2"] = $nb;
                                        break;
                                    case 3:
                                        # 3-c
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['conforme'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["3"] = $nb;
                                        break;
                                    case 4:
                                        # 3-d
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['transgressif'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["4"] = $nb;
                                        break;
                                    case 5:
                                        # 4-a
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['soi'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["5"] = $nb;
                                        break;
                                    case 6:
                                        # 4-b
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['autrui'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["6"] = $nb;
                                        break;
                                    case 7:
                                        # 4-c
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['sympathie'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["7"] = $nb;
                                        break;
                                    case 8:
                                        # 4-d
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        //$data['maitrise'] = number_format(floatval($nb /$nbUser)*100,0);
                                        $data['maitrise'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["8"] = $nb;
                                        break;
                                    
                                    default:
                                        # code...
                                        break;
                                }

                                $cur +=1;
                            }
                            // POUR LE CLIMAGE NEGOCIATION QUESTION 4
                            $cur = 1;
                            if(isset($question4->reponses)){
                                foreach ($question4->reponses as $key => $reponse) {
                                    switch ($cur) {
                                        case 1:
                                            # 3-a
                                            $nb = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            $dataVote["4a"] = $nb;
                                            break;
                                        case 2:
                                            # 3-b
                                            $dataVote["4b"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        case 3:
                                            # 3-c
                                            $dataVote["4c"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        case 4:
                                            # 3-d
                                            $dataVote["4d"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        case 5:
                                            # 4-a
                                            $dataVote["4e"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        case 6:
                                            # 4-b
                                            $dataVote["4f"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            
                                            break;
                                        case 7:
                                            # 4-c
                                            $dataVote["4g"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        case 8:
                                            # 4-d
                                            $dataVote["4h"] = count(\DB::table('reponse_repondant')->where([
                                                ["question_id", "=", $question4->id],
                                                ["test_id", "=", $id],
                                                ["reponse_id", "=", $reponse->id],
                                                ["updated_at","like","%".$d." ".$h."%"]
                                            ])->get());
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }

                                    $cur +=1;
                                }
                            }
                            array_push($dataGraph, $data);
                        }else{
                            $data = null;
                        }
                        // CLIMAGE 3
                        $scoreVote = 0;
                        $scoreVote2 = 0;
                        $scoreVote3 = 0;
                        $scoreVote4 = 0;
                        $scoreVote5 = 0;

                        if($climage->type->name != "climage-interlocuteur"){
                            $scoreVote = ($dataVote["1"]+$dataVote["3"]+$dataVote["a"]+$dataVote["b"]) + ($dataVote["1"]+$dataVote["4"]+$dataVote["c"]+$dataVote["d"]) + ($dataVote["2"]+$dataVote["3"]+$dataVote["e"]+$dataVote["f"]) + ($dataVote["2"]+$dataVote["4"]+$dataVote["g"]+$dataVote["h"]) + ($dataVote["5"]+$dataVote["7"]+$dataVote["k"]+$dataVote["m"]) + ($dataVote["5"]+$dataVote["8"]+$dataVote["i"]+$dataVote["j"]) +
                            ($dataVote["6"]+$dataVote["7"]+$dataVote["p"]+$dataVote["q"]) + ($dataVote["6"]+$dataVote["8"]+$dataVote["n"]+$dataVote["o"]);

                            $dataCli["vigilance"] = number_format((($dataVote["1"]+$dataVote["3"]+$dataVote["a"]+$dataVote["b"])/($scoreVote)*100),1);
                            $dataCli["depassement"] = number_format((($dataVote["1"]+$dataVote["4"]+$dataVote["c"]+$dataVote["d"])/($scoreVote)*100),1);
                            $dataCli["implication"] = number_format((($dataVote["2"]+$dataVote["3"]+$dataVote["e"]+$dataVote["f"])/($scoreVote)*100),1);
                            $dataCli["creative"] = number_format((($dataVote["2"]+$dataVote["4"]+$dataVote["g"]+$dataVote["h"])/($scoreVote)*100),1);
                            $dataCli["reconnaissance"] = number_format((($dataVote["5"]+$dataVote["7"]+$dataVote["k"]+$dataVote["m"])/($scoreVote)*100),1);
                            $dataCli["competition"] = number_format((($dataVote["5"]+$dataVote["8"]+$dataVote["i"]+$dataVote["j"])/($scoreVote)*100),1);
                            $dataCli["convivialite"] = number_format((($dataVote["6"]+$dataVote["7"]+$dataVote["p"]+$dataVote["q"])/($scoreVote)*100),1);
                            $dataCli["collaboration"] = number_format((($dataVote["6"]+$dataVote["8"]+$dataVote["n"]+$dataVote["o"])/($scoreVote)*100),1);

                        }

                        $total1 = 0;
                        $total2 = 0;
                        if($climage->type->name == "climage-interlocuteur"){
                            if(isset($dataVote["q1"])||isset($dataVote["q2"])||isset($dataVote["q3"])&&isset($dataVote["q4"])){
                                $serieux = $dataVote["q1"]+$dataVote["q2"];
                                $signeSerieux = $serieux > 0 ? "+" : "-";
                                $absSerieux = abs($dataVote["q1"])+abs($dataVote["q2"]);
                                
                                $enjoue = $dataVote["q3"]+$dataVote["q4"];
                                $signeEnjoue = $enjoue > 0 ? "+" : "-";
                                $absEnjoue = abs($dataVote["q3"])+abs($dataVote["q4"]);

                                $conforme = $dataVote["q1"]+$dataVote["q4"];
                                $signeConforme = $conforme > 0 ? "+" : "-";
                                $absConforme = abs($dataVote["q1"])+abs($dataVote["q4"]);

                                $transgressif = $dataVote["q2"]+$dataVote["q3"];
                                $signeTransgressif = $transgressif > 0 ? "+" : "-";
                                $absTransgressif = abs($dataVote["q2"])+abs($dataVote["q3"]);

                            }
                            if(isset($dataVote["q5"])||isset($dataVote["q6"])||isset($dataVote["q7"])&&isset($dataVote["q8"])){

                                $soi = $dataVote["q5"]+$dataVote["q6"];
                                $signeSoi = $soi > 0 ? "+" : "-";
                                $absSoi = abs($dataVote["q5"])+abs($dataVote["q6"]);

                                $autrui = $dataVote["q7"]+$dataVote["q8"];
                                $signeAutrui = $autrui > 0 ? "+" : "-";
                                $absAutrui = abs($dataVote["q7"])+abs($dataVote["q8"]);

                                $maitrise = $dataVote["q5"]+$dataVote["q7"];
                                $signeMaitrise = $maitrise > 0 ? "+" : "-";
                                $absMaitrise = abs($dataVote["q5"])+abs($dataVote["q7"]);

                                $sympathie = $dataVote["q6"]+$dataVote["q8"];
                                $signeSymphatie = $sympathie > 0 ? "+" : "-";
                                $absSymphatie = abs($dataVote["q6"])+abs($dataVote["q8"]);

                            }

                            $total1 = round(((isset($absSerieux) ? $absSerieux : 0)+(isset($absAutrui) ? $absAutrui : 0)+(isset($absSoi) ? $absSoi : 0)+(isset($absEnjoue) ? $absEnjoue : 0)))*2;

                            $total2 = round(((isset($absTransgressif) ? $absTransgressif : 0)+(isset($absSymphatie) ? $absSymphatie : 0)+(isset($absMaitrise) ? $absMaitrise : 0)+(isset($absConforme) ? $absConforme : 0)))*2;

                            if($total1 > 0){
                                $dataCli["serieux"] = isset($absSerieux) ? (round(($absSerieux / $total1)*100)) : 0;
                                $dataCli["signeSerieux"] = isset($signeSerieux) ? $signeSerieux : "";
                                $dataCli["enjoue"] = isset($absEnjoue) ? (round(($absEnjoue / $total1)*100)):0;
                                $dataCli["signeEnjoue"] = isset($signeEnjoue) ? $signeEnjoue : "";
                                $dataCli["soi"] = isset($absSoi) ? (round(($absSoi / $total1)*100)) : 0;
                                $dataCli["signeSoi"] = isset($signeSoi) ? $signeSoi : "";
                                $dataCli["autrui"] = isset($absAutrui) ? (round(($absAutrui / $total1)*100)) : 0;
                                $dataCli["signeAutrui"] = isset($signeAutrui) ? $signeAutrui : "";

                            }

                            if($total2 > 0){
                                $dataCli["conforme"] = isset($absConforme) ? round(($absConforme / $total2)*100) : 0;
                                $dataCli["signeConforme"] = isset($signeConforme) ? $signeConforme : "";
                                $dataCli["transgressif"] = isset($absTransgressif) ? round(($absTransgressif / $total2)*100) : 0;
                                $dataCli["signeTransgressif"] = isset($signeTransgressif) ? $signeTransgressif : "";
                                $dataCli["sympathie"] = isset($absSymphatie) ? round(($absSymphatie / $total2)*100) : 0;
                                $dataCli["signeSymphatie"] = isset($signeSymphatie) ? $signeSymphatie : "";
                                $dataCli["maitrise"] = isset($absMaitrise) ? round(($absMaitrise / $total2)*100) : 0;
                                $dataCli["signeMaitrise"] = isset($signeMaitrise) ? $signeMaitrise : "";
                            }
                            $dataCli["total1"] = ($dataCli["serieux"]+$dataCli["enjoue"]+$dataCli["conforme"]+$dataCli["transgressif"]);
                            $dataCli["total2"] = ($dataCli["soi"]+$dataCli["autrui"]+$dataCli["maitrise"]+$dataCli["sympathie"]);
                        }
                        array_push($nbRepondants, $countUsers);
                        array_push($dataClimage,$dataCli);
                        array_push($hArray, $h); 
                    }
                }
                $hArray2 = [];
                foreach ($hArray as $kh => $value) {
                    $hOnly = explode(":",$value);
                    if(!in_array($hOnly[0].":00:00",$hArray2)){
                        array_push($hArray2,$hOnly[0].":00:00");
                    }
                }
                $hArray2 = $this->my_array_unique($hArray2,false);
                if($d!=""){
                    array_push($dateArray, ["date" => $d, "heures"=>$hArray2, "vote"=>$vote, "graphe"=>$dataGraph,"climage"=>$dataClimage, "nbRep"=>$nbRepondants]);
                    //array_push($dateArray2, ["date" => $d, "heures"=>$hArray2, "graphe"=>$dataGraph]);
                }
            }
        }
    
        $dateArray = $this->my_array_unique($dateArray,false);
        // echo '<pre>';
        // var_dump($dateArray);
        // echo '</pre>';
        // die;
        //$dateArray2 = $this->my_array_unique($dateArray2,false);
        $profil = Type::where('name', $type->name)->first()->pages[0];
        $questionProfil = [];
        foreach ($profil->questions as $key => $question) {

            $questionProfil[$question->id] = [];
            $questionProfil[$question->id]['fr'] = $question->question_fr;
            $questionProfil[$question->id]['en'] = $question->question_en;
        }
        
        return view("climage/resultat", ["climage" => $climage, "lang" => $climage->langue , "data" => !isset($data) ? "" : $data, "page2" => $page2, "questionProfil" =>$questionProfil, "dates"=>$dateArray,"id"=>$id/*,"dates2"=>$dateArray2*/, "lang"=> \App::getLocale()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'type_id' => 'required',

            'langue' => 'required',
            'code' => 'required'

        ]);

        $count = Test::where('type_id', '=' ,$request->type_id)->where('code', '=' ,$request->code)->count();

        if($count == 0){

            $climage = new Test;
            $climage->name = $request->name;
            $climage->type_id = $request->type_id;
            $climage->langue = $request->langue;
            $climage->code = $request->code;
            $climage->consultant_id = $userId = Auth::user()->id;

            $climage->save();
            return redirect()->route('climage.index');

        } else {

            return redirect()->route('climage.index')->with('error_code', 'Code d\'accès déjà pris pour ce test climage! Veuillez saisir un autre.');            

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'type_id' => 'required',

            'langue' => 'required',
            'code' => 'required'

        ]);

        $climage = Test::find($id);

        $climage->name = $request->name;
        $climage->type_id = $request->type_id;
        $climage->langue = $request->langue;
        $climage->code = $request->code;
        $climage->save();
        return redirect()->route('climage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TestRepondant::where("test_id",$id)->delete();
        ReponseRepondant::where("test_id",$id)->delete();
        Test::destroy($id);
        return redirect()->route('climage.index');
    }


    public function codes(){

        $count = \DB::table("test")->get();

        echo json_encode($count);        

    }

    public function heures(){
        $data = file_get_contents('php://input');
        $day = json_decode($data)->day;
        $id = json_decode($data)->test_id;
        
        $getHeures = ReponseRepondant::where("test_id",$id)->where("updated_at","like","%".$day."%")->get();

        $hArray = array();
        foreach ($getHeures as $key => $heure) {
            $h = explode(" ",$heure->updated_at);
            if(!in_array($h[1], $hArray)){
                array_push($hArray, $h[1]);
            }
        }        
        
        echo json_encode($hArray);
    }

    function my_array_unique($array, $keep_key_assoc = false){
        $duplicate_keys = array();
        $tmp = array();       
        foreach ($array as $key => $val){
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;
            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }
        foreach ($duplicate_keys as $key)
            unset($array[$key]);
        return $keep_key_assoc ? $array : array_values($array);
    }

    public function deleteInfoClimage(){
        $data = file_get_contents('php://input');
        $id = json_decode($data)->idClimage;
        $user = json_decode($data)->user;
        $deleteInfoUser = ReponseRepondant::where("repondant_id",$user->id)->where("test_id",$id)->delete();
        $deleteInfoUser2 = TestRepondant::where("repondant_id",$user->id)->where("test_id",$id)->delete();
        echo json_encode(array("status" => 1, "message" => "ok"));

    }

    public function replace_extension($filename, $new_extension) {
        $info = pathinfo($filename);
        return $info['filename'] . '.' . $new_extension;
        //return $info['dirname']."/".$info['filename'] . '.' . $new_extension;

    }

    public function zipFolder($zip,$path){
        $rootPath = realpath($path);

        // Initialize archive object
        $zip->open('file.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }
}

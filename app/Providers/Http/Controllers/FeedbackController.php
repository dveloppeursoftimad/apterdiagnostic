<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Test;
use App\Page;
use App\Type;
use App\Mail;
use App\ReponseRepondant;
use App\Question;
use App\Reponse;
use App\TestRepondant;
use App\User;
use App\Groupe;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpWord\Shared\ZipArchive;

use \Hisune\EchartsPHP\ECharts;
use \Hisune\EchartsPHP\Doc\IDE\Series;
use \Hisune\EchartsPHP\Doc\IDE\YAxis;
use \Hisune\EchartsPHP\Doc\IDE\XAxis;
use \Hisune\EchartsPHP\Doc\IDE\Graphic;

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

class FeedbackController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function index()
  { 

      $fb = Type::where('name',"LIKE", "%feedback%")->first();
      $tests = [];
      if(Auth::user()->role == "consultant" || Auth::user()->role == "partenaire" || Auth::user()->role == "admin_rech_acad"){

        $tests = Test::whereHas('type', function ($query) {
          $query->where('name', 'like', "%feedback%");
        })->where("consultant_id", "=", Auth::user()->id)->orderBy("created_at","desc")->paginate(10);

      } else if(Auth::user()->role == "admin" || Auth::user()->role == "admin_acad"){
        $tests = Test::whereHas('type', function ($query) {
          $query->where('name', 'like', "%feedback%");
        })->orderBy("created_at","desc")->paginate(10);
      }

      foreach ($tests as $key => $test) {
        if(!$test->template_id){
          $test->template_id =  strval($fb->id);
        }else{
          $test->template_id = strval($test->template_id);
        }
      }


      $templates = Type::where("name", "LIKE", "%feedback%")->orderBy('id', 'desc')->get();



      return view('feedback/index', ["tests" =>  $tests, "templates" => $templates]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  public function addSPage(Request $request){

    $test_sp = Test::where("template_id",$request->sp_type_id)->count();

    if($test_sp == 0){
      if(is_null($request->sp_page_id)){
        $sp = new Page;
        $sp->page_id = $request->sp_id;
        $sp->type_id = $request->sp_type_id;
        $sp->title_fr = $request->sp_fr;
        $sp->title_en = $request->sp_en;
        $sp->save();
      } else {
        $question = new Question;
        $question->question_fr = $request->question_fr;
        $question->question_en = $request->question_en;
        $question->type_id = $request->sp_type_id;
        $question->page_id = $request->sp_id;
        $question->type = $request->type_input;
        $question->negative = is_null($request->check) ? 0 : 1;
        $question->save();



        if($request->type_input == "radio"){

          Reponse::firstOrCreate(
            [
              'reponse_fr' => "pas du tout d'accord",
              "reponse_en" => "not agree at all",
              "score" => is_null($request->check) ? 1 : 4,
              "question_id" => $question->id
            ]
          );

          Reponse::firstOrCreate(
            [
              'reponse_fr' => "plutôt pas d'accord",
              "reponse_en" => "rather disagree",
              "score" => is_null($request->check) ? 2 : 3,
              "question_id" => $question->id
            ]
          );

          Reponse::firstOrCreate(
            [
              'reponse_fr' => "plutôt d'accord",
              "reponse_en" => "somewhat agree",
              "score" => is_null($request->check) ? 3 : 2,
              "question_id" => $question->id
            ]
          );

          Reponse::firstOrCreate(
            [
              'reponse_fr' => "tout à fait d'accord",
              "reponse_en" => "Totally agree",
              "score" => is_null($request->check) ? 4 : 1,
              "question_id" => $question->id
            ]
          );

          Reponse::firstOrCreate(
            [
              'reponse_fr' => "je ne sais pas",
              "reponse_en" => "I don't know",
              "score" => 0,
              "question_id" => $question->id
            ]
          );
        } 
      }
    } else {
          
    }

    return \Redirect::route('feedback-questionnaire', ["idQuest" => 0, 'active' => $request->idFb]);

    //return redirect()->route('feedback-questionnaire');
  }

  public function addTemplate(Request $request){

    // $delete = [13,14];

    // \DB::statement('SET FOREIGN_KEY_CHECKS=0');
    // foreach ($delete as $key => $value) {
    //   $pages = Page::where('type_id', $value)->get();
        
    //   $questions = Question::where('type_id', $value)->get();
    //   foreach ($questions as $key => $quest) {
        
    //     foreach ($quest->reponses as $k => $rep) {
    //       $rep->delete();

    //     }
    //     $quest->delete();
    //   }
    //   foreach ($pages as $key => $page) {
    //      $page->delete();
      
    //   }

    //   Type::find($value)->delete();
    // }
    // \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    // die;


    if($request->id){
      $type = Type::find($request->id);
      $type->description = $request->name;
      $type->save();
      return redirect()->route('feedback-questionnaire'); 
    }
    $fb = Type::where('name',"LIKE", "%feedback%")->first();
    $num = strval(Type::where('name',"LIKE", "%feedback%")->count()+1);

    $type = new Type;
    $type->name = "feedback ".$num;
    $type->description = $request->name;
    $type->save();

    $this->createQuest($type);



    return redirect()->route('feedback-questionnaire');   
  }
  public function mail(){


      $type = Type::where('name', 'like', "%feedback%")->first();
      $mails_fr = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", "fr" )->orderBy('type', 'asc')->get();
      $mails_en = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", "en" )->orderBy('type', 'asc')->get();
      // foreach ($mails as $key => $value) {
      //   var_dump($value->type);
      // }
      // die();
      return view('feedback/mails',['mails_fr'=>$mails_fr, 'mails_en'=>$mails_en]);
      
  }

  public function mailEdit($id){

      $mail = Mail::find($id);
      $mails = Mail::where("type", $mail->type)->where("language", $mail->language)->get();
      foreach ($mails as $key => $m) {
        if(!is_null($m->consultant_id)){
          $m->user = User::find($m->consultant_id)->username;
        }
      }

      return view('mail/edit',['mail'=>$mail, 'mails'=>$mails]);
  }

   public function mailUpdate(Request $request){
      // var_dump("test");
      // die();
      $mail = Mail::find($request->id);
      if(!$request->template){


        // set other inactive
        $mails = Mail::where("type", $mail->type)->where("language", $mail->language)->get();
        foreach ($mails as $key => $m) {
            if($m->id != $mail->id){
                $m->active = 0;
                $m->save();
            }

        }
        $mailNew = Mail::find($request->id);
        $mailNew->subject = $request->subject;
        $mailNew->mail = $request->mail;
        $mailNew->active = 1;
        // var_dump( $request->mail);
        // var_dump($mailNew->mail);


        $mailNew->save();
      }else{
        $mails = Mail::where("type", $mail->type)->where("language", $mail->language)->get();
        foreach ($mails as $key => $m) {
                $m->active = 0;
                $m->save();
        }
        $mailNew = new Mail;
        $mailNew->subject = $request->subject;
        $mailNew->mail = $request->mail;
        $mailNew->active = 1;
        $mailNew->type = $mail->type;
        $mailNew->type_id = $mail->type_id;
        $mailNew->language = $mail->language;
        $mailNew->save();

      }
      return redirect()->route('feedback-mail');
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

        $validatedData = $request->validate([
          'name' => 'required',

          'code' => 'required'

        ]);

        $count = Test::where('name', '=' ,$request->name)->where('code', '=' ,$request->code)->count();



        if($count == 0){

          $fb = new Test;
          $fb->name = $request->name;
          $fb->template_id = $request->template_id;
          $fb->type_id = Type::firstOrCreate(array('name' => 'feedback'))->id;

          $fb->code = $request->code;
          $fb->consultant_id = $userId = Auth::user()->id;

          $fb->save();
          return redirect()->route('feedback.index');

        } else {

            return redirect()->route('feedback.index')->with('error_code', 'Code d\'accès déjà pris pour ce test feedback! Veuillez saisir un autre.');

        }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {


        $fb = Test::find($id);
        

        $fb->name = $request->name;

        $fb->code = $request->code;
        // $fb->template_id = (int)$request->template_id;

        $fb->save();
        return redirect()->route('feedback.index');
  }

  public function rapport($id){
    ///////GET Data per question

    
    $test = Test::find($id);
    $pages = Type::where('name', "feedback")->first()->pages;
    if($test->template_id){
      $pages = Type::find($test->template_id)->pages;
    }
    $groups = [];
    $repondants = $test->users;
    $lang = \App::getLocale();


        $cur = 0;
        foreach ($pages as $key1 => $page) {
        
            $cur+=1;
            if($cur < 7){
                continue;            
            }
            $sectionNum = 1;
            foreach ($page->pages as $key2 => $section) {

                foreach ($section->questions as $key3 => $question) {

                    

                    
                        
                        $reponses = ReponseRepondant::whereHas('test', function ($query) use ($id) {
                          $query->where('id', $id);
                        })->whereHas('question', function($query) use ($question) {
                          $query->where("id", $question->id);
                        })->get();
                        $data = [];
                        $legendData = [];

                        foreach ($reponses as $key4 => $rep) {
                          if($rep->user){

                            // var_dump($question->question_fr);
                            // var_dump($rep->user->email);
                            $test = TestRepondant::where("test_id", $id)->where("repondant_id", $rep->user->id)->first();
                            $test->reponse = 1;
                            $test->save();
                            
                          }
                        }
                    
                }
            }
        }







    $chart_questions =[];
    $chart_details =[];
    $chart_principales =[];

    $cur=1;

    $serrees = [];
    $ecartees = [];

    $groupColor = [];
    $colors = [ "#2EC7C9", "#B6A2DE", "#FFB980", "#8D98B3",   "#2EC7C9", "#B6A2DE", "#FFB980", "#8D98B3"];
    $colorIter = 0;
    foreach ($repondants as $key => $rep) {
      // var_dump($rep->testRepondants()->where("test_id", $id)->first());

      $repTest = TestRepondant::where("test_id", $id)->where("repondant_id",$rep->id)->first();
      
            if(!$repTest->groupe_id){
                
                $repTest->role = $rep->type;
                $repTest->groupe_id = $rep->groupe_id;
                $repTest->save();
            }
      //$rep->groupe = $rep->testRepondants()->where("test_id", $id)->first() ? $rep->testRepondants()->where("test_id", $id)->first()->groupe : null;

      $idgroupe = User::find($rep->id);
      $rep->groupe = $idgroupe->testRepondants()->where("test_id", $id)->first() ? $idgroupe->testRepondants()->where("test_id", $id)->first()->groupe : null;
   
      if(!$rep->groupe){
        continue;
      }
      $group = strtoupper($rep->groupe->abbreviation);
      if(!array_key_exists($group, $groupColor)){
        $groupColor[$group] = $colors[$colorIter];
        $colorIter +=1;
        
      }
    }



    $groupColor["VOUS"] = "#D87A80";

    foreach ($pages as $key1 => $page) {
        if($cur == 8){
          break;
        }
        $cur+=1;
        $sectionNum = 1;
        foreach ($page->pages as $key2 => $section) {
          $starQuest = 1;

          foreach ($section->questions as $key3 => $question) {
            // var_dump($question->question_fr);
            if($question->type == "radio"){






                $reponses = ReponseRepondant::whereHas('test', function ($query) use ($id) {
                  $query->where('id', $id);
                })->whereHas('question', function($query) use ($question) {
                  $query->where("id", $question->id);
                })->get();
                $data = [];
                $legendData = [];
                foreach ($reponses as $key4 => $rep) {
                  if(!$rep->user){
                    continue;
                  }
                  
                  $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;
                  $group = strtoupper($rep->user->groupe->abbreviation);

                  // $this->dump($rep->user->email);
                  // $this->dump($rep->user->id);
                  // $this->dump($group);
                  // $this->dump($rep->reponse->score);
                  // $this->dump($rep->test);


                  if(!array_key_exists($group, $data) ){
                    $data[$group] = [];
                    $legendData[] = $group;
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = 1;
                    }else{
                      $data[$group]["score"] = $rep->reponse->score;
                      $data[$group]["total"] = 1;
                    }

                  }else{
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = array_key_exists("none", $data[$group]) ? ($data[$group]["none"] + 1) : 1;
                    }else{
                      $data[$group]["score"] = array_key_exists("score", $data[$group]) ? ($data[$group]["score"] + $rep->reponse->score) : $rep->reponse->score;
                      $data[$group]["total"] = array_key_exists("total", $data[$group]) ? ($data[$group]["total"] + 1) : 1;
                    }

                  }
                  // var_dump($rep->reponse ? $rep->user->groupe->abbreviation." ".$rep->reponse->score : '');

                }


                // die;


                foreach ($data as $key => $value) {
                  if(array_key_exists('score', $value)){
                    $data[$key]['score']= $data[$key]['score']/ $data[$key]['total'];
                  }
                }
                $serrees[$question->id] = 0;
                $ecartees[$question->id] = 0;
                $total = 0;
                foreach ($data as $key => $value) {
                  if(array_key_exists('score', $value)){
                    if($key != "VOUS"){
                      $serrees[$question->id] += $data[$key]['score'];
                      $ecartees[$question->id] += $data[$key]['score'];
                      $total +=1;
                    }
                  }

                }



                // $serrees[$question->id] = $serrees[$question->id] / $total;
                // $ecartees[$question->id] = $ecartees[$question->id] / $total;
                if(array_key_exists("VOUS", $data)){
                  $serrees[$question->id] = array_key_exists('score', $data["VOUS"]) ? abs($data["VOUS"]['score'] -   $serrees[$question->id]) : 0;
                  $ecartees[$question->id] = array_key_exists('score', $data["VOUS"]) ? abs($data["VOUS"]['score'] -   $ecartees[$question->id]) : 0;
                }




                  $question->data = $data;

                  $chart = new ECharts();
                  // $chart->title = [
                  //     "text" => $page->{'title_'.$lang}." | ".$section->{'title_'.$lang},
                  //     "subtext" => wordwrap($question->{'question_'.$lang}, 100, "\n")
                  // ];
                  if($sectionNum ==1){
                    $chart->title = [
                        "text" => $page->{'title_'.$lang},
                        "subtext" => wordwrap($section->{'title_'.$lang}, 100, "\n"),
                        "x" => "center",
                        "subtextStyle" => ["fontSize" => 18]
                    ];
                  }else if($starQuest == 1){
                    $chart->title = [
                        "text" => "",
                        "subtext" => wordwrap($section->{'title_'.$lang}, 100, "\n"),
                        "x" => "center",
                        "subtextStyle" => ["fontSize" => 18]
                    ];
                  }


                 
                  $chart->tooltip->trigger = 'axis';

                  $chart->tooltip->show = true;
                  $chart->type = "bar";
                  // $chart->width = 500;
                  $chart->height = 100;
                  $chart->backgroundColor = "#fff";
                  $chart->color = [ "#8D98B3", "#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3",    "#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3"];
                  $chart->itemStyle->normal->barBorderRadius = 50;

                  $chart->tooltip->trigger = 'axis';
                  // $chart->legend->data = $legendData;
                  $chart->yAxis[] = array(
                      'type' => 'category',
                      'data' => array(wordwrap($question->{'question_'.$lang}, 50, "\n")),
                      "axisLabel" => ["textStyle" => ['fontSize' => 18]]
                  );
                  $xAxis = new XAxis();
                  $xAxis->type = 'value';
                  $xAxis->max = 4;
                  $xAxis->min = 0;

                  $chart->grid->left = "50%"; 
                  $chart->addXAxis($xAxis);

                  $chart->series[] = array(
                      'name' => "",
                      'type' => 'bar',
                      "itemStyle" => [
                          "normal"=> [
                              "barBorderRadius" => 15

                          ]
                      ],
                      "label" => [
                          "normal"=> [
                              "show" => true,
                              "formatter" => '{a} : {c}'

                          ]
                      ],
                      "markLine" => [
                          "data" => [
                              ["name" => 'Poitn de bascule', "xAxis" => 2.5, "itemStyle" => ["normal" =>["color" => '#dc143c',"label" => ["position" =>'top'] ] ] ]
                          ]
                      ]

                      );
                  $step =0;


                  foreach ($data as $key => $value) {

                    if(array_key_exists("score", $value)){
                      if(strtoupper($key) != "VOUS"){
                          $chart->series[] = array(
                                  'name' => $key,
                                  'type' => 'bar',
                                  // 'data' => array($val[$i]),
                                  "itemStyle" => [
                                      "normal"=> [
                                          "barBorderRadius" => 15

                                      ]
                                  ],
                                  "label" => [
                                      "normal"=> [
                                          "show" => FALSE,
                                          "formatter" => '{a} : {c}'

                                      ]
                                  ],
                                  "markPoint" => [

                                      "data" => [
                                          ["large" => true, "name" => $key, "xAxis" => $value["score"], "yAxis" => $step, "value" => $key."\n".number_format($value["score"],1, ',', ' '),
                                            "itemStyle" => ["color" => $groupColor[$key]]
                                            ]
                                      ]
                                  ]
                          );
                      }else{
                          $score = number_format((float)$value["score"], 1, '.', '');
                          $chart->series[] = array(
                              'name' => $key,
                              'type' => 'bar',
                              'data' => array(["value" => $score, "itemStyle" => ["color" => $groupColor[$key]]]),
                              "itemStyle" => [
                                  "normal"=> [
                                      "barBorderRadius" => 8

                                  ]
                              ],
                              "label" => [
                                  "normal"=> [
                                      "show" => true,
                                      "formatter" => '{a} : {c}'

                                  ]
                              ],

                          );
                      }

                    }

                      $step +=1;
                  }

                  $chart_questions[$question->id] = $chart;
                  // echo $chart->render('question_'.$question->id);

                // echo "<br>";
                // echo "<br>";
                // var_dump($question->data);

                // echo "<br>";
                // echo "<br>";
                // echo "<br>";
            }
            $sectionNum +=1;
            $starQuest +=1;

          }

          
        }

    }
    // die;
    asort($serrees);
    arsort($ecartees);
    // var_dump($serrees);
    // echo "<br>";
    // echo "<br>";
    // var_dump($ecartees);
    // die();


    $cur=1;
    foreach ($pages as $key1 => $page) {
        if($cur == 8){
          break;
        }
        $cur+=1;
        $sectionNum = 1;
        foreach ($page->pages as $key2 => $section) {

          $data = [];

          foreach ($section->questions as $key3 => $question) {
            // var_dump($question->question_fr);

            if($question->type == "radio"){

                $reponses = ReponseRepondant::with("user", "user.groupe")->whereHas('test', function ($query) use ($id) {
                  $query->where('id', $id);
                })->whereHas('question', function($query) use ($question) {
                  $query->where("id", $question->id);
                })->get();

                $legendData = [];
                foreach ($reponses as $key4 => $rep) {
                  if(!$rep->user){
                    continue;
                  }
                  $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

                  $group = $rep->user->groupe->abbreviation;
                  if(!array_key_exists($group, $data) ){
                    $data[$group] = [];
                    $legendData[] = $group;
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = 1;
                    }else{
                      $data[$group]["score"] = $rep->reponse->score;
                      $data[$group]["total"] = 1;
                    }

                  }else{
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = array_key_exists("none", $data[$group]) ? ($data[$group]["none"] + 1) : 1;
                    }else{
                      $data[$group]["score"] = array_key_exists("score", $data[$group]) ? ($data[$group]["score"] + $rep->reponse->score) : $rep->reponse->score;
                      $data[$group]["total"] = array_key_exists("total", $data[$group]) ? ($data[$group]["total"] + 1) : 1;
                    }

                  }
                  // var_dump($rep->reponse ? $rep->user->groupe->abbreviation." ".$rep->reponse->score : '');

                }





            }

          }


                  foreach ($data as $key => $value) {
                    if(array_key_exists('score', $value)){
                      $data[$key]['score']= $data[$key]['score']/ $data[$key]['total'];
                    }
                  }



                  $chart = new ECharts();
                  if($sectionNum ==1){
                    $chart->title = [
                        "text" => $page->{'title_'.$lang},
                        "subtext" => wordwrap($section->{'title_'.$lang}, 100, "\n"),
                        "x" => "center",
                        "subtextStyle" => ["fontSize" => 18]
                    ];
                  }else{
                    $chart->title = [
                        "text" => "",
                        "subtext" => wordwrap($section->{'title_'.$lang}, 100, "\n"),
                        "x" => "center",
                        "subtextStyle" => ["fontSize" => 18]
                    ];
                  }


                  $chart->tooltip->trigger = 'axis';

                  $chart->tooltip->show = true;
                  $chart->type = "bar";
                  // $chart->width = 500;
                  $chart->height = 100;
                  $chart->backgroundColor = "#fff";
                  $chart->color = ["#8D98B3","#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3",    "#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3"];
                  $chart->itemStyle->normal->barBorderRadius = 50;

                  $chart->tooltip->trigger = 'axis';
                  // $chart->legend->data = $legendData;
                  $chart->yAxis[] = array(
                      'type' => 'category',
                      'data' => array("")
                  );
                  $xAxis = new XAxis();
                  $xAxis->type = 'value';
                  $xAxis->max = 4;
                  $xAxis->min = 0;
                  $chart->addXAxis($xAxis);

                  $step =0;

                  $chart->series[] = array(
                      'name' => "",
                      'type' => 'bar',
                      // 'data' => array($val[$i]),
                      "itemStyle" => [
                          "normal"=> [
                              "barBorderRadius" => 15

                          ]
                      ],
                      "label" => [
                          "normal"=> [
                              "show" => true,
                              "formatter" => '{a} : {c}'

                          ]
                      ],
                      "markLine" => [
                          "data" => [
                              ["name" => 'Point de bascule', "xAxis" => 2.5, "itemStyle" => ["normal" =>["color" => '#dc143c',"label" => ["position" =>'top'] ] ] ]
                          ]

                      ]

                      );
                  foreach ($data as $key => $value) {
                    $key = strtoupper($key);
                    if(array_key_exists("score", $value)){
                      if(strtoupper($key) != "VOUS"){
                          $chart->series[] = array(
                                  'name' => $key,
                                  'type' => 'bar',
                                  // 'data' => array($val[$i]),
                                  "itemStyle" => [
                                      "normal"=> [
                                          "barBorderRadius" => 15

                                      ]
                                  ],
                                  "label" => [
                                      "normal"=> [
                                          "show" => FALSE,
                                          "formatter" => '{a} : {c}'

                                      ]
                                  ],
                                  "markPoint" => [

                                      "data" => [
                                          ["large" => true, "name" => $key, "xAxis" => $value["score"], "yAxis" => $step, "value" => $key."\n".number_format($value["score"],1, ',', ' '),
                                            "itemStyle" => ["color" => $groupColor[$key]]
                                            ]
                                      ]
                                  ]
                          );
                      }else{
                          $score = number_format((float)$value["score"], 1, '.', '');
                          $chart->series[] = array(
                              'name' => $key,
                              'type' => 'bar',
                              'data' =>[["value" => $score, "itemStyle" => ["color" => $groupColor[$key]]]],
                              "itemStyle" => [
                                  "normal"=> [
                                      "barBorderRadius" => 8

                                  ]
                              ],
                              "label" => [
                                  "normal"=> [
                                      "show" => true,
                                      "formatter" => '{a} : {c}'

                                  ]
                              ],

                          );
                      }

                    }

                      $step +=1;
                  }

                  $chart_details[$section->id] = $chart;

                $sectionNum +=1;
        }

    }



    $cur =1;
    foreach ($pages as $key1 => $page) {
        if($cur == 8){
          break;
        }
        $cur+=1;

        $data = [];
        foreach ($page->pages as $key2 => $section) {



          foreach ($section->questions as $key3 => $question) {
            // var_dump($question->question_fr);

            if($question->type == "radio"){

                $reponses = ReponseRepondant::with("user", "user.groupe")->whereHas('test', function ($query) use ($id) {
                  $query->where('id', $id);
                })->whereHas('question', function($query) use ($question) {
                  $query->where("id", $question->id);
                })->get();

                $legendData = [];
                foreach ($reponses as $key4 => $rep) {
                  if(!$rep->user){
                    continue;
                  }

                  $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

                  $group = $rep->user->groupe->abbreviation;
                  if(!array_key_exists($group, $data) ){
                    $data[$group] = [];
                    $legendData[] = $group;
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = 1;
                    }else{
                      $data[$group]["score"] = $rep->reponse->score;
                      $data[$group]["total"] = 1;
                    }

                  }else{
                    if($rep->reponse->score == 0){
                      $data[$group]["none"] = array_key_exists("none", $data[$group]) ? ($data[$group]["none"] + 1) : 1;
                    }else{
                      $data[$group]["score"] = array_key_exists("score", $data[$group]) ? ($data[$group]["score"] + $rep->reponse->score) : $rep->reponse->score;
                      $data[$group]["total"] = array_key_exists("total", $data[$group]) ? ($data[$group]["total"] + 1) : 1;
                    }

                  }

                }





            }

          }



        }


                 foreach ($data as $key => $value) {
                    if(array_key_exists('score', $value)){
                      $data[$key]['score']= $data[$key]['score']/ $data[$key]['total'];
                    }
                  }



                  $chart = new ECharts();
                    $chart->title = [
                        "text" => $page->{'title_'.$lang},
                        "x" => "center"
                    ];


                  $chart->tooltip->trigger = 'axis';

                  $chart->tooltip->show = true;
                  $chart->type = "bar";
                  // $chart->width = 500;
                  $chart->height = 100;
                  $chart->backgroundColor = "#fff";
                  $chart->color = ["#8D98B3","#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3",    "#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3"];
                  $chart->itemStyle->normal->barBorderRadius = 50;

                  $chart->tooltip->trigger = 'axis';
                  // $chart->legend->data = $legendData;
                  $chart->yAxis[] = array(
                      'type' => 'category',
                      'data' => array("")
                  );
                  $xAxis = new XAxis();
                  $xAxis->type = 'value';
                  $xAxis->max = 4;
                  $xAxis->min = 0;
                  $chart->addXAxis($xAxis);

                  $step =0;

                  $chart->series[] = array(
                      'name' => "",
                      'type' => 'bar',
                      // 'data' => array($val[$i]),
                      "itemStyle" => [
                          "normal"=> [
                              "barBorderRadius" => 15

                          ]
                      ],
                      "label" => [
                          "normal"=> [
                              "show" => true,
                              "formatter" => '{a} : {c}'

                          ]
                      ],
                      "markLine" => [
                          "data" => [
                              ["name" => 'Point de bascule', "xAxis" => 2.5, "itemStyle" => ["normal" =>["color" => '#dc143c',"label" => ["position" =>'top'] ] ] ]
                          ]

                      ]

                      );
                  foreach ($data as $key => $value) {
                    $key = strtoupper($key);
                    if(array_key_exists("score", $value)){
                      if(strtoupper($key) != "VOUS"){
                          $chart->series[] = array(
                                  'name' => $key,
                                  'type' => 'bar',
                                  // 'data' => array($val[$i]),
                                  "itemStyle" => [
                                      "normal"=> [
                                          "barBorderRadius" => 15

                                      ]
                                  ],
                                  "label" => [
                                      "normal"=> [
                                          "show" => FALSE,
                                          "formatter" => '{a} : {c}'

                                      ]
                                  ],
                                  "markPoint" => [

                                      "data" => [
                                          ["large" => true, "name" => $key, "xAxis" => $value["score"], "yAxis" => $step, "value" => $key."\n".number_format($value["score"],1, ',', ' '),
                                            "itemStyle" => ["color" => $groupColor[$key]]
                                            ]
                                      ]
                                  ]
                          );
                      }else{
                          $score = number_format((float)$value["score"], 1, '.', '');
                          $chart->series[] = array(
                              'name' => $key,
                              'type' => 'bar',
                              'data' =>[["value" => $score, "itemStyle" => ["color" => $groupColor[$key]]]],
                              "itemStyle" => [
                                  "normal"=> [
                                      "barBorderRadius" => 8

                                  ]
                              ],
                              "label" => [
                                  "normal"=> [
                                      "show" => true,
                                      "formatter" => '{a} : {c}'

                                  ]
                              ],

                          );
                      }

                    }

                      $step +=1;
                  }

                  $chart_principales[$section->id] = $chart;
                  // $this->dump($chart->series);
                    
                        
                  // echo $chart->render('page_'.$section->id);
                  // die;
                // echo "<br>";
                // echo "<br>";
                // var_dump($data);

                // echo "<br>";
                // echo "<br>";
                // echo "<br>";


    }


    $data = [];
    $cur = 1;
    foreach ($pages as $key1 => $page) {
        if($cur == 8){
          break;
        }
        $cur+=1;

        foreach ($page->pages as $key2 => $section) {
          $data[$page->id] = [];


          foreach ($section->questions as $key3 => $question) {
            // var_dump($question->question_fr);

            if($question->type == "radio"){

                $reponses = ReponseRepondant::with("user", "user.groupe")->whereHas('test', function ($query) use ($id) {
                  $query->where('id', $id);
                })->whereHas('question', function($query) use ($question) {
                  $query->where("id", $question->id);
                })->get();

                $legendData = [];
                foreach ($reponses as $key4 => $rep) {
                  if(!$rep->user){
                    continue;
                  }

                  $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

                  $group = $rep->user->groupe->abbreviation;
                  $group = strtoupper($group);
                  if($group != "VOUS"){
                    $group = "TOUS : HORS-VOUS";
                  }
                  if(!array_key_exists($group, $data[$page->id]) ){
                    $data[$page->id][$group] = [];
                    $legendData[] = $group;
                    if($rep->reponse->score == 0){
                      $data[$page->id][$group]["none"] = 1;
                    }else{
                      $data[$page->id][$group]["score"] = $rep->reponse->score;
                      $data[$page->id][$group]["total"] = 1;
                    }

                  }else{
                    if($rep->reponse->score == 0){
                      $data[$page->id][$group]["none"] = array_key_exists("none", $data[$page->id][$group]) ? ($data[$page->id][$group]["none"] + 1) : 1;
                    }else{
                      $data[$page->id][$group]["score"] = array_key_exists("score", $data[$page->id][$group]) ? ($data[$page->id][$group]["score"] + $rep->reponse->score) : $rep->reponse->score;
                      $data[$page->id][$group]["total"] = array_key_exists("total", $data[$page->id][$group]) ? ($data[$page->id][$group]["total"] + 1) : 1;
                    }

                  }
                  // var_dump($rep->reponse ? $rep->user->groupe->abbreviation." ".$rep->reponse->score : '');

                }





            }

          }



        }




    }

    
    $data1 = [];
    $data2 = [];
    foreach ($data as $k => $value) {



        if(array_key_exists("TOUS : HORS-VOUS", $value) && array_key_exists('score', $value["TOUS : HORS-VOUS"])){

          $data2[] = $value["TOUS : HORS-VOUS"]['score']/ $value["TOUS : HORS-VOUS"]['total'];
        }

        if(array_key_exists("VOUS", $value) && array_key_exists('score', $value["VOUS"])){

          $data1[] = $value["VOUS"]['score']/ $value["VOUS"]['total'];
        }


    }
    if(count($data1) <1){
      $data1 = [0,0,0,0,0,0,0];
    }

    if(count($data2) <1){
      $data2 = [0,0,0,0,0,0,0];
    }

    // $this->dump($data1);
    // echo "<br>";
    // echo "<br>";
    // $this->dump($data2);
    // die;

    // $data1 = [];
    // $data2 = [1,2,3,4,3,4,1];




      $chart = new ECharts();




        // RADAR
        $chart->tooltip->trigger = 'axis';
        $chart->backgroundColor = "#fff";
         $chart->color = ["#f44336", "#2196f3"];

        $chart->legend->data = [trans('message.you'),trans("message.others")];
        $textLegend = [];

        $num = 0;
        foreach ($pages as $key => $page) {
       
          $num +=1;
          if($num == 8) break;
          $textLegend[] =[
            "text" => $page->{'title_'.$lang},
            "max" => 4
          ];
        }

        $chart->polar->indicator = $textLegend;
        


        $series = new Series();
        $series->name = 'Times';
        $series->type = 'radar';

        $series->data = [
                [
                    "value" => $data1,
                    "name" => trans('message.you'),


                ],
                [
                    "value" => $data2,
                    "name" => trans("message.others"),
                     "areaStyle" => [
                        "normal" => [
                            "color" => '#2196f3',
                            "opacity" =>0.3
                        ]
                    ]
                ]
        ];
        $chart->addSeries($series);
        $chart_radar = $chart;
        // echo $chart->render('radar');

        // die();
        // RADAR 2
        // $radar2 = [];
        // $labels = [
        //   ["value" => 3, "name" => 'MANAGEMENT ET COMMUNICATION'],
        //         ["value" => 4, "name" => 'LEADERSHIP'],
        //         ["value" => 3, "name" => 'TRANSVERSALITE et MANAGEMENT DE PROJETS MATRICIEL'],
        //         ["value" => 4, "name" => 'ATTITUDE FACE AU CHANGEMENT'],
        //         ["value" => 2, "name" => 'PILOTAGE DE L’ACTIVITE'],
        //         ["value" => 3, "name" => 'IMPLICATION'],
        //         ["value" => 4, "name" => 'ADHESION AUX VALEURS DE L’ENTREPRISE']
        // ];
        // foreach ($data2 as $key => $value) {
        //     $radar2[] = [
        //       "value" => $value,
        //       "name" => $labels[$key]["name"]
        //     ];
        // }
        // $radar = new ECharts();
        // $radar->type = "pie";
        // $radar->roseType = 'area';
       
        // $radar->backgroundColor = "#fff";

        // $radar->series[] = [
        //     "name" => "RADAR",
        //     "type" => "pie",
        //     "radius" => [20, 110],

        //     "roseType" => 'radius',

        //     "data" => $radar2
        // ];

        // $chart_radar2 = $radar;
      
        // END RADAR 2




        // foreach ($chart_principales as $key => $value) {
        //   echo $value->render('principale_'.$key);
        // }
        // die();

      return view("feedback/rapport", [
        "chart_questions" => $chart_questions,
        "chart_details" => $chart_details,
        "chart_principales" => $chart_principales,
        "chart_radar" => $chart_radar,
        "chart_radar" => $chart_radar,
        "id" => $id,
        "ecartees" => $ecartees,
        "serrees" => $serrees,
        "test" => $test,


      ]);

  }

  public function zipFolder($zip,$path,$path2){
    $rootPath = realpath($path);

    // Initialize archive object
    $zip->open($path2.'file.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($rootPath),
        \RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
  }

  // public function removeDir($dir) {
  //       if (is_dir($dir)) {
  //         $objects = scandir($dir);
  //         foreach ($objects as $object) {
  //           if ($object != "." && $object != "..") {
  //             if (filetype($dir."/".$object) == "dir") 
  //                return $this->removeDir($dir."/".$object); 
  //             else unlink($dir."/".$object);
  //           }
  //         }
  //         reset($objects);
  //         return $this->removeDir($dir);
  //       }
       
  //     }

  public function downloadPdf(Request $request){
    //$data = file_get_contents('php://input');
    //$id = json_decode($data)->id;

    $target_dir = base_path()."/public/uploads/feedback/".$request->id."/";
    $id = $request->id;
    $file_url = url("/uploads/feedback/".$request->id);
    $response = ["message" => "Une erreur est survenu lors de l'exportation", "generated" => false];

    if (!file_exists($target_dir)) {
          mkdir($target_dir, 0777, true);
    }

    $test = Test::find($request->id);


    
    $consultant = $test->consultant;
    $repondants = $test->users;
    $nbRepondants = count($test->users);
    $repondant = $test->users[0];

    $nbReponses = TestRepondant::where("test_id", $request->id)->where("reponse", 1)->count();
    $taux = number_format(($nbReponses/$nbRepondants)*100, 1, ',',' ');

    $ben = User::find($test->beneficiaire_id);
    // foreach($repondants as $key => $rep) {
    //   if($rep->type == "beneficiaire"){
    //     $ben = $rep;
    //     break;
    //   }
    // }

    $groupe_string = [];
    $groups =[];
    foreach ($repondants as $key => $rep) {
      $rep->groupe = $rep->testRepondants()->where("test_id", $test->id)->first() ? $rep->testRepondants()->where("test_id", $test->id)->first()->groupe : null;

      if(!$rep->groupe){
        continue;
      }

      $group = strtoupper($rep->groupe->abbreviation);
      $reponse = TestRepondant::where("repondant_id", $rep->id)->where("test_id", $test->id)->where("reponse", 1)->count();

      if(!array_key_exists($group, $groups)){
        $groups[$group]['repondants'] =1;
        if($reponse > 0){
          $groups[$group]['reponses'] =1;
        }
      }else{
        $groups[$group]['repondants'] +=1;

        if($reponse > 0){
          if(!array_key_exists('reponses', $groups[$group])){
             $groups[$group]['reponses'] =1;
          }else{
              $groups[$group]['reponses'] +=1;
          }
            
        }
      }
    }

    foreach ($groups as $key => $value) {
      $nb = array_key_exists("reponses", $value) ? $value["reponses"] : 0;
      $groupe_string[$key] = $nb." / ".$value["repondants"]."\n";
    }

    //date debut - date fin
    $date_debut = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "asc")->first()->updated_at;
    $date_fin = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "desc")->first()->updated_at;

    $pdf = new PDF_Diag();
    $path = base_path().'/public/templates/feedback/';
    $pageCount = $pdf->setSourceFile($path.'/feedbackPdf.pdf');

    $tplIdx = $pdf -> importPage(1);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);

    $txtRa = iconv("UTF-8", "windows-1252","Rapport-360°-Diagnostic").str_replace(" ","-",$repondant->firstname);



    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');

    //////////////////Page 1//////////////////////////////////
    $pdf->addPage("L");

    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',20);
    $pdf->SetXY(340, 25); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252","Restitué par : ".$consultant->lastname." ".$consultant->firstname),0,'R',0);
    $pdf->SetXY(340, 40); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252","Date d'édition : ".strftime("%d/%m/%Y")),0,'R',0);

    $pdf->SetXY(335, 210); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",isset($ben->initiale) ? $ben->initiale ." - ".$ben->organisation : $ben->organisation),0,'R',0);

    //////////////////////////Page 2 ///////////////////////////////////
    $pdf->addPage("L");

    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(2, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->SetXY(133, 45); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",$nbReponses." / ".$nbRepondants),0,'R',0);
    $pdf->SetXY(175, 51); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",$taux." %"),0,'R',0);
    $pdf->SetXY(123, 65); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",count($groups)),0,'R',0);
    $pdf->SetXY(143.5, 80); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",date_format(date_create($date_debut), "d/m/Y")),0,'R',0);
    $pdf->SetXY(143.5, 97); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",date_format(date_create($date_fin), "d/m/Y")),0,'R',0);
    $pdf->SetXY(159, 128); 
    $pdf->cell(10,0, iconv("UTF-8", "windows-1252",$ben->firstname." ".$ben->lastname),0,'R',0);
    $pdf->SetXY(0, 139); 
    $pdf->Cell(196,0, iconv("UTF-8", "windows-1252",isset($groupe_string["VOUS"]) ? $groupe_string["VOUS"] : ""),0,'L',0);
    $pdf->SetXY(0, 146); 
    $pdf->Cell(196,0, iconv("UTF-8", "windows-1252",isset($groupe_string["HIER"]) ? $groupe_string["HIER"] : ""),0,'L',0);
    $pdf->SetXY(10, 153); 
    $pdf->Cell(196,0, iconv("UTF-8", "windows-1252",isset($groupe_string["PAIRS"]) ? $groupe_string["PAIRS"] : ""),0,'L',0);
    $pdf->SetXY(24, 160); 
    $pdf->Cell(196,0, iconv("UTF-8", "windows-1252",isset($groupe_string["DIRECT"]) ? $groupe_string["DIRECT"] : ""),0,'L',0);
    $pdf->SetXY(1, 168); 
    $pdf->Cell(126,0, iconv("UTF-8", "windows-1252",isset($groupe_string["EXT"]) ? $groupe_string["EXT"] : ""),0,'L',0);
    $pdf->SetXY(32, 174); 
    $pdf->Cell(176,0, iconv("UTF-8", "windows-1252",isset($groupe_string["FONCT"]) ? $groupe_string["FONCT"] : ""),0,'L',0);

    $pdf->SetXY(113, 188); 
    $pdf->Cell(140,0, iconv("UTF-8", "windows-1252",$test->name),0,'L',0);


    ///////////////////////// PAGE 3 À 11///////////////////////////////////////////////////////
    for ($i=3; $i <= 11; $i++) { 
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }


    ///////////////////////// PAGE 12 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $imageData = $request->radar_1;

    $filteredData=substr($imageData, strpos($imageData, ",")+1);
    $unencodedData=base64_decode($filteredData);


    file_put_contents($target_dir."radar_img.png", $unencodedData);

    $pdf->Image($target_dir."radar_img.png",30,80,300);
      
    

    


    ///////////////////////// PAGE 13 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i=1;
    $y = 60;
    foreach ($request->chart_principales as $key => $value) {
        if($i == 3){
          break;
        }

        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."principal_img_".$key.".png",60,$y,250);
          $y += 100;
          $i++;
    }


    ///////////////////////// PAGE 14 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i=1;
    $y = 60;
    foreach ($request->chart_principales as $key => $value) {
        if($i == 5){
          break;
        }
        if($i >=3){
          $imageData = $value;
          $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."principal_img_".$key.".png",60,$y,250);
          $y += 100;
          
        }
        $i++;
          
    }

    ///////////////////////// PAGE 15 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i=1;
    $y = 60;
    foreach ($request->chart_principales as $key => $value) {
        if($i == 7){
          break;
        }
        if($i >=5){
          $imageData = $value;
          $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."principal_img_".$key.".png",60,$y,250);
          $y += 100;
          
        }
        $i++;
          
    }

    ///////////////////////// PAGE 16 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i=1;
    $y = 60;
    foreach ($request->chart_principales as $key => $value) {
        if($i == 9){
          break;
        }
        if($i >=7){
          $imageData = $value;
          $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."principal_img_".$key.".png",60,$y,250);
          $y += 100;
          
        }
        $i++;
          
    }

    ///////////////////////// PAGE 17 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $serrees = $request->serrees;
    $ecartees = $request->ecartees;
    asort($serrees);
    arsort($ecartees);
    $y = 55;
    foreach ($serrees as $key => $value) {
        if($i == 6){
          break;
        }
        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."question_img_".$key.".png",50,$y,280);

        $pdf->setFillColor(255,255,255);
        $pdf->SetXY(50,$y);
        $pdf->Cell(250,8,"","0",0,'R',1);

        $y += 40;
        $i++;
    }

    ///////////////////////// PAGE 18////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 55;
    foreach ($serrees as $key => $value) {
        if($i == 11){
          break;
        }
        if($i >=6){
         
          $imageData = $request->chart_questions[$key];
          $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);
          file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."question_img_".$key.".png",50,$y,280);

          $pdf->setFillColor(255,255,255);
          $pdf->SetXY(50,$y);
          $pdf->Cell(250,8,"","0",0,'R',1);

          $y += 40;
        }
        $i++;
    }

     ///////////////////////// PAGE 19 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(19, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 55;
    foreach ($ecartees as $key => $value) {
        if($i == 6){
          break;
        }
        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."question_img_".$key.".png",50,$y,280);

        $pdf->setFillColor(255,255,255);
        $pdf->SetXY(50,$y);
        $pdf->Cell(250,8,"","0",0,'R',1);

        $y += 40;
        $i++;
    }

    ///////////////////////// PAGE 20////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(20, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 55;
    foreach ($ecartees as $key => $value) {
        if($i == 11){
          break;
        }
        if($i >=6){
         
          $imageData = $request->chart_questions[$key];
          $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);
          file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);
          $pdf->Image($target_dir."question_img_".$key.".png",50,$y,280);

          $pdf->setFillColor(255,255,255);
          $pdf->SetXY(50,$y);
          $pdf->Cell(250,8,"","0",0,'R',1);

          $y += 40;
        }
        $i++;
    }

    ///////////////////////// PAGE 21 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(21, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
        if($i == 3){
          break;
        }
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);


        $y += 60;
        $i++;
    }

    ///////////////////////// PAGE 22 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(22, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
        if($i == 7){
          break;
        }
      if($i >=3){
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);

        $y += 50;
      }
        
        $i++;
    }

    ///////////////////////// PAGE 23 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(23, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
        if($i == 9){
          break;
        }
      if($i >=7){
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);

        $y += 60;
      }
        
        $i++;
    }


    ///////////////////////// PAGE 24 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(24, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
      if($i == 12){
          break;
      }
      if($i >=9){
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);

        $y += 60;
      }
        
        $i++;
    }

    ///////////////////////// PAGE 25 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(25, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
      if($i == 14){
          break;
      }
      if($i >=12){
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);

        $y += 60;
      }
        
        $i++;
    }


    ///////////////////////// PAGE 26 ////////////////////////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(26, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $i = 1;
    $y = 60;
    foreach ($request->chart_details as $key => $value) {
      if($i == 16){
          break;
      }
      if($i >=14){
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);

        file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
        $pdf->Image($target_dir."detail_img_".$key.".png",30,$y,280);

        $y += 80;
      }
        
        $i++;
    }

    ///////////////////////// PAGE 27 ////////////////////////////////////////////////////////////

    $pageNum = 27;

    for ($j=0; $j <17 ; $j++) { 
        $pdf->addPage("L");
        $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
        $pageId = $pdf->importPage($pageNum, PdfReader\PageBoundaries::MEDIA_BOX);
        $pdf->useImportedPage($pageId);

        $i = 0;
        $y = 55;
        
        foreach ($request->chart_questions as $key => $value) {
          if(!$value){
            continue;
          }
          if($i == (($j+1)*5)){
              break;
          }
          if($i >=($j*5)){
            $imageData = $value;
            $filteredData=substr($imageData, strpos($imageData, ",")+1);
            $unencodedData=base64_decode($filteredData);

            file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);

            $pdf->Image($target_dir."question_img_".$key.".png",30,$y,280);

            $y += 40;
          }
            
            $i++;
        }

        $pageNum +=1;
    }
        




    $lang = \App::getLocale();

    /////////////////////PAGE 67 //////////////////////////
    //$pages = Type::where('name', "feedback")->first()->pages;
    $cur =0;
    // $text  = [];
    // $repText = [];
    // foreach ($pages as $key1 => $page) {
    //     $cur+=1;
    //     if($cur != 8){
    //       continue;
    //     }

    //     foreach ($page->pages as $key2 => $section) {
    //       $num = 0;
    //       foreach ($section->questions as $key3 => $question) {
    //         $num +=1;
    //         $text[$num] = htmlspecialchars($question->{'question_'.$lang})."\n";

    //         //$template->setValue('question_'.$num, htmlspecialchars($question->{'question_'.$lang}));

    //         $reponses = ReponseRepondant::where("test_id", $test->id)->where("question_id", $question->id)->get();
    //         $group =[];

    //         foreach ($reponses as $key4 => $rep) {
    //           if(!$rep->user){
    //               continue;
    //           }

    //           $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

    //           $g = strtoupper($rep->user->groupe->abbreviation);
    //           if(!array_key_exists($g, $group)){
    //             $group[$g] = $rep->reponse_text ?  $rep->reponse_text : "";
    //           }else{
    //             $group[$g] .= ", ".$rep->reponse_text;
    //           }
    //         }


    //         $repText[$num] = "\n";

    //         foreach ($group as $key5 => $grp) {
    //           $repText[$num] .= $key5." : ".$grp."\n\n";
    //         }


    //       //$template->setValue('reponse_'.$num, $repText);

    //         //$text[$num] .= "\n";
    //       }
    //     }
    // }
    $typ =  $test->template_id == NULL ? Type::find($test->type_id) : Type::find($test->template_id);
    $pages = $typ->pages;
    $array1 = array();
    $array2 = array();
    $array3 = array();
    foreach ($pages as $key1 => $page) {
      $cur+=1;
      if($cur != 8){
        continue;
      }
      foreach ($page->pages as $key2 => $section) {
        $num = 0;
        foreach ($section->questions as $key3 => $question) {
          $num +=1;
          if($cur == 8 && $num == 1){
            $reponseText1 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
            foreach ($reponseText1 as $key => $rep1) {
              if(!is_null($rep1->reponse_text)){
                array_push($array1,'- '.$rep1->reponse_text);
              }
            }
          } else if($cur == 8 && $num == 2){
            $reponseText2 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
            foreach ($reponseText2 as $key => $rep2) {
              if(!is_null($rep2->reponse_text)){
                array_push($array2,'- '.$rep2->reponse_text);
              } 
            }
          } else if($cur == 8 && $num == 3){
            $reponseText3 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
            foreach ($reponseText3 as $key => $rep3) {
              if(!is_null($rep3->reponse_text)){
                array_push($array3,'- '.$rep3->reponse_text);
              }
            }
          }
        }
      }
    }

    $imp_array1 = implode(",\n",(array)$array1);
    $imp_array2 = implode(",\n",(array)$array2);
    $imp_array3 = implode(",\n",(array)$array3);

    ///////////////////////// page 44 ///////////////////////
    $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(44, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);

    ///////////////////////// PAGE 45 - 47////////////////////////////////////////////////////////////
    $j=1;
    for($i=45;$i<=47;$i++){
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
      $pdf->SetFont('RobotoCondensed-Regular','',15);
      $pdf->SetXY(26, 80);
      $pdf->Multicell(325,6, iconv("UTF-8", "windows-1252", ${"imp_array".$j}),0,'L',0);

      // $pdf->SetXY(25, 125);
      // $pdf->Multicell(325,6, iconv("UTF-8", "windows-1252",isset($repText["PAIRS"]) ? (isset($repText["PAIRS"][$j]) ? $repText["PAIRS"][$j] : "") : ""),0,'L',0);

      // $pdf->SetXY(26, 170);
      // $pdf->Multicell(325,6, iconv("UTF-8", "windows-1252",isset($repText["DIRECT"]) ? (isset($repText["DIRECT"][$j]) ? $repText["DIRECT"][$j] : "") : ""),0,'L',0);

      // $pdf->SetXY(26, 215);
      // $pdf->Multicell(325,6, iconv("UTF-8", "windows-1252",isset($repText["EXT"]) ? (isset($repText["EXT"][$j]) ? $repText["EXT"][$j] : "") : ""),0,'L',0);
      $j+=1;
    }

    ///////////////////// PAGE 48-51 /////////////////////
    for($i=48;$i<=51;$i++){
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }

    $nomFichier = $target_dir.'360_FB_'.$ben->username.".pdf";
    $filename=$target_dir."Rapport_360_feedback.pdf";
    $pdf->Output($nomFichier,'F');

    if(file_exists($nomFichier)){
        $response["generated"] = true;
        $response["doc"] = $file_url."/360_FB_".$ben->username.".pdf";
    }

    return response()->json($response);

  }

  public function downloadPptx(Request $request){

    $target_dir = base_path()."/public/uploads/feedback/".$request->id."/pptx";
    $file_url = url("/uploads/feedback/".$request->id."/pptx");

    if (!file_exists($target_dir)) {
      mkdir($target_dir, 0777, true);
    }

    $id = $request->id;
    $response = ["message" => "Une erreur est survenu lors de l'exportation", "generated" => false];

    $test = Test::find($request->id);
    
    $consultant = $test->consultant;
    $repondants = $test->users;
    $nbRepondants = count($test->users);
    $repondant = $test->users[0];

    $nbReponses = TestRepondant::where("test_id", $request->id)->where("reponse", 1)->count();
    $taux = number_format(($nbReponses/$nbRepondants)*100, 1, ',',' ');

    $ben = User::find($test->beneficiaire_id);
    // foreach($repondants as $key => $rep) {
    //   if($rep->type == "beneficiaire"){
    //     $ben = $rep;
    //     break;
    //   }
    // }
    $groupe_string = [];
    $groups =[];
    foreach ($repondants as $key => $rep) {
      $rep->groupe = $rep->testRepondants()->where("test_id", $test->id)->first() ? $rep->testRepondants()->where("test_id", $test->id)->first()->groupe : null;

      if(!$rep->groupe){
        continue;
      }

      $group = strtoupper($rep->groupe->abbreviation);
      $reponse = TestRepondant::where("repondant_id", $rep->id)->where("test_id", $test->id)->where("reponse", 1)->count();

      if(!array_key_exists($group, $groups)){
        $groups[$group]['repondants'] =1;
        if($reponse > 0){
          $groups[$group]['reponses'] =1;
        }
      }else{
        $groups[$group]['repondants'] +=1;

        if($reponse > 0){
          if(!array_key_exists('reponses', $groups[$group])){
             $groups[$group]['reponses'] =1;
          }else{
              $groups[$group]['reponses'] +=1;
          }
            
        }
      }
    }

    foreach ($groups as $key => $value) {
      $nb = array_key_exists("reponses", $value) ? $value["reponses"] : 0;
      $groupe_string[$key] = $nb." / ".$value["repondants"]."\n";
    }

    //date debut - date fin
    $date_debut = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "asc")->first()->updated_at;
    $date_fin = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "desc")->first()->updated_at;

    $path = base_path().'/public/templates/feedback/';
    //var_dump(realpath($path.'feedbackPPTX2.pptx'));

    //var_dump($this->replace_extension($path.'11.pptx','zip'));
    if(!is_dir($path."/archive/".$request->id) == true){
      mkdir($path."/archive/".$request->id,0777,true);   
    }
    copy($path.'feedbackPPTX2.pptx',$path.'/archive/feedbackPPTX2.zip');
    $filename = $path.'/archive/feedbackPPTX2.zip';
    $zip = new ZipArchive;
    $res = $zip->open($filename);
    if ($res === TRUE) {
      // Unzip path
      $newpath = $path."/archive/".$request->id;

      // Extract file
      $zip->extractTo($newpath);
      $zip->close();

      ////////////////////// SLIDE 1 /////////////////////////
      $xml_external_path1 = $newpath."/ppt/slides/slide1.xml";
      $xml1 = simplexml_load_file($xml_external_path1);

      $t1 = ['text1', 'text2', 'text3'];
      $r1 = [isset($consultant->lastname) ? $consultant->lastname." ".$consultant->firstname : "", strftime("%d/%m/%Y"),isset($ben->initiale) ? $ben->initiale ."-".$ben->organisation : $ben->organisation];

      $n1 = simplexml_load_string( str_replace( $t1, $r1, $xml1->asXml()));
      $n1->asXml($newpath."/ppt/slides/slide1.xml");
      /////////////////////////////////////////////////////////

      /////////////////////// SLIDE 2 /////////////////////////////
      $xml_external_path2 = $newpath."/ppt/slides/slide2.xml";
      $xml2 = simplexml_load_file($xml_external_path2);

      $t2 = ['text4', 'text5', 'text6','text7', 'text8', 'text9','text10', 'text11','text12', 'text13', 'text14','text15', 'text16'];
      $r2 = [isset($nbReponses) ? $nbReponses : "",isset($nbRepondants) ? $nbRepondants : "",isset($taux) ? $taux : "", count($groups), isset($date_debut) ? date_format(date_create($date_debut), "d/m/Y") : "",isset($date_fin) ? date_format(date_create($date_fin), "d/m/Y") : "",isset($ben->firstname) ? $ben->firstname." ".$ben->lastname : "",isset($groupe_string["VOUS"]) ? $groupe_string["VOUS"] : "",isset($groupe_string["HIER"]) ? $groupe_string["HIER"] : "", isset($groupe_string["PAIRS"]) ? $groupe_string["PAIRS"] : "", isset($groupe_string["EXT"]) ? $groupe_string["EXT"] : "",isset($groupe_string["FONCT"]) ? $groupe_string["FONCT"] : ""];

      $n2 = simplexml_load_string( str_replace( $t2, $r2, $xml2->asXml()));
      $n2->asXml($newpath."/ppt/slides/slide2.xml");

      ////////////////////////////////////////////////////////////// 

      /////////////// SLIDE 12 ////////////////////////////////////////////

      if($request->radar_1){
        $imageData = $request->radar_1;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."/radar_img.png", $unencodedData);
        copy($target_dir."/radar_img.png",$path."/archive/".$request->id."/ppt/media/image12.png");
      } else {
        copy(base_path()."/public/uploads/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image12.png");
      }

      //////////////////////////////////////////////////////////////////////

      ////////////////////////// SLIDE 13 - SLIDE 14 //////////////////////////////////

      $i=1;
      $numero = 13;
      $Imax = 7;
      foreach ($request->chart_principales as $key => $value) {
        if($i == 9){
          break;
        }
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."/principal_img_".$key.".png", $unencodedData);
        if(file_exists($target_dir."/principal_img_".$key.".png")){
          $this->principaleChart($target_dir."/principal_img_".$key.".png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
          $numero2 = $i;
          $numero3 = $numero;
        }
        
        $i++;
        $numero++;
      }

      $vide = abs($numero2 - $Imax);
      if($vide > 0){
        $suivant = $numero3++;
        for ($i=0; $i < $vide; $i++) { 
          $this->principaleChart(base_path()."/public/templates/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$suivant.".png");
          $suivant++;
        }
      }

      ////////////////////////////////////////////////////////////////////////

      ////////////////////// SLIDE 15 - SLIDE 16 - SLIDE 17 //////////////////////////////////////

      $i = 1;
      $numero = 20;
      $Imax = 10;
      $serrees = $request->serrees;
      $ecartees = $request->ecartees;
      asort($serrees);
      arsort($ecartees);
      foreach ($serrees as $key => $value) {
        if($i == 11){
          break;
        }

        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."/question_img_".$key.".png", $unencodedData);
        if (file_exists($target_dir."/question_img_".$key.".png")) {
          $this->principaleChart($target_dir."/question_img_".$key.".png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
          $numero2 = $i;
          $numero3 = $numero;
        }
        $i++;
        $numero++;
      }

      $vide = abs($numero2 - $Imax);
      
      if($vide > 0){
        $suivant = $numero3++;
        for ($i=0; $i < $vide; $i++) { 
          $this->principaleChart(base_path()."/public/templates/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$suivant.".png");
          $suivant++;
        }
      }

      ////////////////////////////////////////////////////////////////////


      ///////////////////////////// SLIDE 18 - SLIDE 19 - SLIDE 20 //////////////////////////////

      $i = 1;
      $numero = 30;
      $i = 1;
      $Imax = 10;
      foreach ($ecartees as $key => $value) {
        if($i == 11){
          break;
        }
        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."/question_img_".$key.".png", $unencodedData);

        if (file_exists($target_dir."/question_img_".$key.".png")) {
          $this->principaleChart($target_dir."/question_img_".$key.".png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
          $numero2 = $i;
          $numero3 = $numero;
        }
        $numero++;
        $i++;
      } 

      $vide = abs($numero2 - $Imax);
      
      if($vide > 0){
        $suivant = $numero3++;
        for ($i=0; $i < $vide; $i++) { 
          $this->principaleChart(base_path()."/public/templates/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$suivant.".png");
          $suivant++;
        }
      }     

      /////////////////////////////////////////////////////////////////////


      //// SLIDE 21 - SLIDE 22 - SLIDE 23 - SLIDE 24 - SLIDE 25 - SLIDE 26 ///////////////////////////////////////

      $i = 1;
      $Imax = 15;
      $numero = 40;
      foreach ($request->chart_details as $key => $value) {
        if($i == 16){
            break;
        }
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents($target_dir."/detail_img_".$key.".png", $unencodedData);
        if(file_exists($target_dir."/detail_img_".$key.".png")){
          $this->principaleChart($target_dir."/detail_img_".$key.".png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
          $numero2 = $i;
          $numero3 = $numero;
        }
        $numero++;
        $i++;
      }

      $vide = abs($numero2 - $Imax);
      
      if($vide > 0){
        $suivant = $numero3++;
        $vide++;
        for ($i=0; $i < $vide; $i++) { 
          $this->principaleChart(base_path()."/public/templates/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$suivant.".png");
          $suivant++;
        }
      }      

      //////////////////////////////////////////////////////////////////////


      ////////////////// SLIDE 27 - SLIDE 56 ///////////////////////////////////////

      for ($j=0; $j <17 ; $j++) { 
        $i = 0;
        $Imax = 83;
        $numero = 55;
        foreach ($request->chart_questions as $key => $value) {
          if(!$value){
            continue;
          }
          if($i == (($j+1)*5)){
              break;
          }
          if($i >=($j*5)){
            $imageData = $value;
            $filteredData=substr($imageData, strpos($imageData, ",")+1);
            $unencodedData=base64_decode($filteredData);
            file_put_contents($target_dir."/question_img_".$key.".png", $unencodedData);
            if(file_exists($target_dir."/question_img_".$key.".png")){
              $this->principaleChart($target_dir."/question_img_".$key.".png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
              $numero2 = $i;
              $numero3 = $numero;
            } else {
              $this->principaleChart(base_path()."/public/uploads/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$numero.".png");
            }
          }  
          $i++;
          $numero++;
        }
      }
      $vide = abs($numero2 - $Imax);
      
      if($vide > 0){
        $suivant = $numero3++;
        for ($i=0; $i < $vide; $i++) { 
          $this->principaleChart(base_path()."/public/templates/feedback/image/v1.png",$path."/archive/".$request->id."/ppt/media/image".$suivant.".png");
          $suivant++;
        }
      }

      //////////////////////////////////////////////////////////////////

      ///////////////// SLIDE 58 - SLIDE 59 - SLIDE 60 ////////////////////////////////////////

      $lang = \App::getLocale();
      $typ =  $test->template_id == NULL ? Type::find($test->type_id) : Type::find($test->template_id);

      //$pages = Type::where('name', "feedback")->first()->pages;
      $pages = $typ->pages;
      $cur =0;
      $text  = [];
      $repText = [];

      $array1 = array();
      $array2 = array();
      $array3 = array();
      foreach ($pages as $key1 => $page) {
        $cur+=1;
        if($cur != 8){
          continue;
        }
        foreach ($page->pages as $key2 => $section) {
          $num = 0;
           foreach ($section->questions as $key3 => $question) {
            $num +=1;
            if($cur == 8 && $num == 1){
              $reponseText1 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
              foreach ($reponseText1 as $key => $rep1) {
                if(!is_null($rep1->reponse_text)){
                  array_push($array1,'- '.$rep1->reponse_text);
                }
              }
            } else if($cur == 8 && $num == 2){
              $reponseText2 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
              foreach ($reponseText2 as $key => $rep2) {
                if(!is_null($rep2->reponse_text)){
                  array_push($array2,'- '.$rep2->reponse_text);
                } 
              }
            } else if($cur == 8 && $num == 3){
              $reponseText3 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
              foreach ($reponseText3 as $key => $rep3) {
                if(!is_null($rep3->reponse_text)){
                  array_push($array3,'- '.$rep3->reponse_text);
                }
              }
            }
          }
        }
      } 
      // foreach ($pages as $key1 => $page) {
      //   $cur+=1;
      //   if($cur != 8){
      //     continue;
      //   }
      //   foreach ($page->pages as $key2 => $section) {
      //     $num = 0;
      //     foreach ($section->questions as $key3 => $question) {
      //       $num +=1;
      //       $text[$num] = htmlspecialchars($question->{'question_'.$lang})."\n";

      //       //$template->setValue('question_'.$num, htmlspecialchars($question->{'question_'.$lang}));

      //       $reponses = ReponseRepondant::where("test_id", $test->id)->where("question_id", $question->id)->get();
      //       $group =[];

      //       foreach ($reponses as $key4 => $rep) {
      //         if(!$rep->user){
      //             continue;
      //         }

      //         $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

      //         $g = strtoupper($rep->user->groupe->abbreviation);
      //         if(!array_key_exists($g, $group)){
      //           $group[$g] = $rep->reponse_text ?  $rep->reponse_text : "";
      //         }else{
      //           $group[$g] .= ", ".$rep->reponse_text;
      //         }
      //       }


      //       $repText[$num] = "\n";

      //       foreach ($group as $key5 => $grp) {
      //         $repText[$num] .= $key5." : ".$grp."\n\n";
      //       }
      //     }
      //   }
      // }
      $imp_array1 = implode(",\n",(array)$array1);
      $xml_external_path58 = $newpath."/ppt/slides/slide58.xml";
      $xml58 = simplexml_load_file($xml_external_path58);
      $t58 = ['text17'];  

      $r58 = [$imp_array1];    

      $n58 = simplexml_load_string( str_replace($t58, $r58, $xml58->asXml()));
      $n58->asXml($newpath."/ppt/slides/slide58.xml");

      $imp_array2 = implode(",\n",(array)$array2);
      $xml_external_path59 = $newpath."/ppt/slides/slide59.xml";
      $xml59 = simplexml_load_file($xml_external_path59);
      $t59 = ['text18'];  

      $r59 = [$imp_array2];    

      $n59 = simplexml_load_string( str_replace($t59, $r59, $xml59->asXml()));
      $n59->asXml($newpath."/ppt/slides/slide59.xml");   

      $imp_array3 = implode(",\n",(array)$array3);
      $xml_external_path60 = $newpath."/ppt/slides/slide60.xml";
      $xml60 = simplexml_load_file($xml_external_path60);
      $t60 = ['text19'];  

      $r60 = [$imp_array3];    

      $n60 = simplexml_load_string( str_replace($t60, $r60, $xml60->asXml()));
      $n60->asXml($newpath."/ppt/slides/slide60.xml");       

      /////////////////////////////////////////////////////////////////

      ////////////////////// SLIDE 61 /////////////////////////////////

      $xml_external_path61 = $newpath."/ppt/slides/slide61.xml";
      $xml61 = simplexml_load_file($xml_external_path61);
      $t61 = ['text18']; 
      $j=1;
      for($i=45;$i<=47;$i++){
        $r61 = [isset($repText[$j]) ? $repText[$j] : ""];
        $j+=1;
      } 

      $n61 = simplexml_load_string( str_replace($t61, $r61, $xml61->asXml()));
      $n61->asXml($newpath."/ppt/slides/slide61.xml");      

      /////////////////////////////////////////////////////////////////


      /////////////////// SLIDE 62 /////////////////////////////////////

        $xml_external_path62 = $newpath."/ppt/slides/slide62.xml";
        $xml62 = simplexml_load_file($xml_external_path62);
        $t62 = ['text19']; 
        $j=1;
        for($i=45;$i<=47;$i++){
          $r62 = [isset($repText[$j]) ? $repText[$j] : ""];
          $j+=1;
        } 

        $n62 = simplexml_load_string( str_replace($t62, $r62, $xml62->asXml()));
        $n62->asXml($newpath."/ppt/slides/slide62.xml");       

      ///////////////////////////////////////////////////////////////////

      $this->zipFolder($zip,$newpath,$path."/archive/".$request->id."/");
      
      if(!is_dir(base_path()."/public/uploads/feedback/".$request->id."/pptx") == true){
        mkdir(base_path()."/public/uploads/feedback/".$request->id."/pptx",0777,true);   
      }    
      $nomFichier = '360_FB_'.$ben->username.".pptx";
      copy($path."/archive/".$request->id.'/file.zip', $target_dir.'/'.$nomFichier);
      if(file_exists($target_dir.'/'.$nomFichier)){
        $response["generated"] = true;
        $response["doc"] = $file_url.'/'.$nomFichier;
        $this->deleteFolder($path."/archive");
        copy($path.'backup.pptx',$path.'/feedbackPPTX2.pptx');
      }
      return response()->json($response);
    } 
  }

  public function principaleChart($path,$path2){
    copy($path, $path2);
  }


  public function deleteFolder($path)
  {
      if (is_dir($path) === true)
      {
          $files = array_diff(scandir($path), array('.', '..'));

          foreach ($files as $file)
          {
              $this->deleteFolder(realpath($path) . '/' . $file);
          }

          return rmdir($path);
      }

      else if (is_file($path) === true)
      {
          return unlink($path);
      }

      return false;
  }

  public function downloadDocx(Request $request){

    $response = ["message" => "Une erreur est survenu lors de l'exportation", "generated" => false];
    $path = base_path().'/public/templates/feedback/';
    $target_dir = base_path()."/public/uploads/feedback/".$request->id."/";
    $file_url = url("/uploads/feedback/".$request->id);
    if (!file_exists($target_dir)) {
          mkdir($target_dir, 0777, true);
    }

    $test = Test::find($request->id);
    $id = $request->id;
    $consultant = $test->consultant;
    $repondants = $test->users;
    $nbRepondants = count($test->users);

    $nbReponses = TestRepondant::where("test_id", $request->id)->where("reponse", 1)->count();
    $taux = number_format(($nbReponses/$nbRepondants)*100, 1, ',',' ');
    $ben = "";
    foreach($repondants as $key => $rep) {
      if($rep->type == "beneficiaire"){
        $ben = $rep;
        break;
      }
    }

    $groupe_string = "";
    $groups =[];
    foreach ($repondants as $key => $rep) {

      $rep->groupe = $rep->testRepondants()->where("test_id", $id)->first() ? $rep->testRepondants()->where("test_id", $id)->first()->groupe : null;

      if(!$rep->groupe){
        continue;
      }

      $group = strtoupper($rep->groupe->abbreviation);
      $reponse = TestRepondant::where("repondant_id", $rep->id)->where("test_id", $test->id)->where("reponse", 1)->count();

      if(!array_key_exists($group, $groups)){
        $groups[$group]['repondants'] =1;
        if($reponse > 0){
          $groups[$group]['reponses'] =1;
        }
      }else{
        $groups[$group]['repondants'] +=1;
        if($reponse > 0){
          if(!array_key_exists('reponses', $groups[$group])){
             $groups[$group]['reponses'] =1;
          }else{
              $groups[$group]['reponses'] +=1;
          }
            
        }
      }
    }



    foreach ($groups as $key => $value) {
      $nb = array_key_exists("reponses", $value) ? $value["reponses"] : 0;
      $groupe_string .= "".$key." : ".$nb." / ".$value["repondants"]."</w:t><w:br/><w:t xml:space='preserve'>";
    }

    $lang = \App::getLocale();

    //date debut - date fin
    $date_debut = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "asc")->first()->updated_at;
    $date_fin = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "desc")->first()->updated_at;

    $template = new \PhpOffice\PhpWord\TemplateProcessor($path.'Template2019.docx');

    $template->setValue('ben_initiales', $ben->initiale);
    $template->setValue('ben_societe', $ben->organisation);
      // $template->setValue('cons_firstname', $consultant->user_firstname);
      // $template->setValue('cons_lastname', $consultant->user_lastname);
    $template->setValue('cons_firstname', $consultant->firstname);
    $template->setValue('cons_lastname', $consultant->lastname);

    setlocale (LC_TIME, 'fr_FR.utf8','fra');

    $template->setValue('date', strftime("%d/%m/%Y"));
      //strftime("%A %d %B %Y.")   date("D M d, Y")
    $template->setValue('nb_reponses', $nbReponses);
    $template->setValue('nb_repondants', count($repondants));
    $template->setValue('taux_reponse', $taux);
    $template->setValue('ben_civilite', $ben->civility);
    $template->setValue('ben_firstname', $ben->firstname);
    $template->setValue('ben_lastname', $ben->lastname);
    $template->setValue('quest_type', $test->name );
    $template->setValue('groupesArray', $groupe_string);
    $template->setValue('date_debut', date_format(date_create($date_debut), "d/m/Y"));
    $template->setValue('date_fin', date_format(date_create($date_fin), "d/m/Y"));


    // Questions


    $pages = Type::where('name', "feedback")->first()->pages;
    $cur = 0;
    $nbQuestions = 0;
    foreach ($pages as $key1 => $page) {
        $cur+=1;
        if($cur == 8){
          break;
        }
        foreach ($page->pages as $key2 => $section) {

          foreach ($section->questions as $key3 => $question) {
            $nbQuestions +=1;
          }
        }
    }
    $template->cloneRow('question_img_array', $nbQuestions);
    $i = 1;
    foreach ($request->chart_questions as $key => $value) {

      if(!$value){
        continue;
      }
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);

          // $template->setImageValue('question_img_array#'.($i),array('path' => $target_dir."question_img_".$key.".png", "width" => 5000));
          $template->setImageValue('question_img_array#'.($i), array('path' => $target_dir."question_img_".$key.".png",  'height' => 200 , "width" => 500, "ratio" => false));
          $i++;
    }

    $template->cloneRow('question_serrees_img_array', 10);
    $i = 1;
    $serrees = $request->serrees;
    $ecartees = $request->ecartees;
    asort($serrees);
    arsort($ecartees);

    foreach ($serrees as $key => $value) {
        if($i == 11){
          break;
        }
        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);


          // $template->setImageValue('question_img_array#'.($i),array('path' => $target_dir."question_img_".$key.".png", "width" => 5000));
          $template->setImageValue('question_serrees_img_array#'.($i), array('path' => $target_dir."question_img_".$key.".png",  'height' => 200 , "width" => 500, "ratio" => false));
          $i++;
    }

    $template->cloneRow('question_ecartees_img_array', 10);
    $i = 1;
    foreach ($ecartees as $key => $value) {
        if($i == 11){
          break;
        }
        $imageData = $request->chart_questions[$key];
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);


          // $template->setImageValue('question_img_array#'.($i),array('path' => $target_dir."question_img_".$key.".png", "width" => 5000));
          $template->setImageValue('question_ecartees_img_array#'.($i), array('path' => $target_dir."question_img_".$key.".png",  'height' => 200 , "width" => 500, "ratio" => false));
          $i++;
    }

    $template->cloneRow('detail_img_array', count($request->chart_details));

    $i = 1;
      //save datail img
    foreach ($request->chart_details as $key => $value) {
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
          $template->setImageValue('detail_img_array#'.($i), array('path' => $target_dir."detail_img_".$key.".png",  'height' => 200 , "width" => 500, "ratio" => false));


          $i++;
    }

    $template->cloneRow('principal_img_array', count($request->chart_principales));
    $i=1;
      //save principal img
    foreach ($request->chart_principales as $key => $value) {
        $imageData = $value;
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
          $unencodedData=base64_decode($filteredData);

          file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
          $template->setImageValue('principal_img_array#'.($i), array('path' => $target_dir."principal_img_".$key.".png",  'height' => 200 , "width" => 500, "ratio" => false));

          $i++;
    }

    $imageData = $request->radar_1;
    $filteredData=substr($imageData, strpos($imageData, ",")+1);
    $unencodedData=base64_decode($filteredData);

    file_put_contents($target_dir."radar_img.png", $unencodedData);
    $template->setImageValue('radar_img', array('path' => $target_dir."radar_img.png", 'height' => 600 , "width" => 600, "ratio" => true));

   //  $imageData = $request->radar_2;
    // $filteredData=substr($imageData, strpos($imageData, ",")+1);
    // $unencodedData=base64_decode($filteredData);

    // file_put_contents($target_dir."radar_img_2.png", $unencodedData);
    $template->setValue('radar_img_2', "");

    $typ =  $test->template_id == NULL ? Type::find($test->type_id) : Type::find($test->template_id);
    $pages = $typ->pages;

  //  $pages = Type::where('name', "feedback")->first()->pages;
    $cur =0;
    $text  = "";
    foreach ($pages as $key1 => $page) {
        $cur+=1;
        if($cur != 8){
          continue;
        }

        foreach ($page->pages as $key2 => $section) {
          $num = 0;
          foreach ($section->questions as $key3 => $question) {
            $num +=1;
            $text  .= "</w:t><w:br/><w:t xml:space='preserve'>".htmlspecialchars($question->{'question_'.$lang})."</w:t><w:br/><w:t xml:space='preserve'></w:t><w:br/><w:t xml:space='preserve'>";

            $template->setValue('question_'.$num, htmlspecialchars($question->{'question_'.$lang}));

            $reponses = ReponseRepondant::where("test_id", $test->id)->where("question_id", $question->id)->get();
            $group =[];
            foreach ($reponses as $key4 => $rep) {
              if(!$rep->user){
                  continue;
              }

              $rep->user->groupe = $rep->user->testRepondants()->where("test_id", $id)->first() ? $rep->user->testRepondants()->where("test_id", $id)->first()->groupe : null;

              $g = strtoupper($rep->user->groupe->abbreviation);
              if(!array_key_exists($g, $group)){
                $group[$g] = $rep->reponse_text ?  $rep->reponse_text : "";
              }else{
                $group[$g] .= ", ".$rep->reponse_text;
              }
            }


            $repText ="</w:t><w:br/><w:t xml:space='preserve'>";

            foreach ($group as $key5 => $grp) {
              $repText .= $key5." : ".$grp."</w:t><w:br/><w:t xml:space='preserve'>";
            }
            $template->setValue('reponse_'.$num, $repText);

            $text  .= "</w:t><w:br/><w:t xml:space='preserve'></w:t><w:br/><w:t xml:space='preserve'>";
          }
        }
    }

    $template->saveAs($target_dir."Rapport_360_feedback.docx");
    if(file_exists($target_dir."Rapport_360_feedback.docx")){
        $response["generated"] = true;
        $response["doc"] = $file_url."/Rapport_360_feedback.docx";
    }

    return response()->json($response);
  }

  public function resultatFeedback($id){
    $fb = Type::where('name', "feedback")->first();

    $test = Test::find($id);
    $data_users = [];
    if(!is_null($test->template_id)){
      $fb = Type::find($test->template_id);
    }
    foreach ($test->users as $key => $user) {
      $score_user = [];
      //$user->groupe = $user->testRepondants()->where("test_id", $id)->first() ? $user->testRepondants()->where("test_id", $id)->first()->groupe : null;

      $idgroupe = User::find($user->id);
      $userGroupe = $idgroupe->testRepondants()->where("test_id", $id)->first() ? $idgroupe->testRepondants()->where("test_id", $id)->first()->groupe : null;
      $scores = ReponseRepondant::where("test_id","=",$id)->where("repondant_id","=",$user->id)->get();
      
      foreach ($scores as $k => $score) {
        // var_dump($score->reponse_id);
        $repUser = Reponse::where("id",$score->reponse_id)->first();

        $score_user[$repUser["question_id"]] = $repUser["score"];
      }

    
      array_push($data_users,["groupe"=>isset($userGroupe->abbreviation)?$userGroupe->abbreviation:"AUCUNE","nom"=>$user->firstname." ".$user->lastname, "score"=>$score_user]);
    }

    return view('feedback/resultat', ["fb" => $fb, "data_users" => $data_users]);

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      ReponseRepondant::where("test_id",$id)->delete();
      TestRepondant::where("test_id",$id)->delete();
      $users = User::where("test_id", $id)->get();
      foreach ($users as $key => $user) {
        $user->test_id = NULL;
        $user->save();
      }
      Test::destroy($id);

      return redirect()->route('feedback.index');
  }

  public function codes(){

      $count = \DB::table("test")->get();

      echo json_encode($count);

  }
  public function dump($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
  }

  function createQuest($type1){

    // PAGES
    $page1 = Page::firstOrCreate(['title_fr' => 'MANAGEMENT ET COMMUNICATION', 'title_en' => "MANAGEMENT AND COMMUNICATION", "type_id" => $type1->id]);
    $page2 = Page::firstOrCreate(array('title_fr' => 'LEADERSHIP', 'title_en' => "LEADERSHIP", "type_id" => $type1->id));
    $page3 = Page::firstOrCreate(array('title_fr' => 'TRANSVERSALITE et MANAGEMENT DE PROJETS MATRICIEL', 'title_en' => "TRANSVERSALITY and MATRICIAL PROJECT MANAGEMENT", "type_id" => $type1->id));
    $page4 = Page::firstOrCreate(array('title_fr' => 'ATTITUDE FACE AU CHANGEMENT','title_en' => "ATTITUDE FACING CHANGE", "type_id" => $type1->id));
    $page5 = Page::firstOrCreate(array('title_fr' => 'PILOTAGE DE L’ACTIVITE', 'title_en' => "PILOTAGE OF ACTIVITY", "type_id" => $type1->id));
    $page6 = Page::firstOrCreate(array('title_fr' => 'IMPLICATION', 'title_en' => "INVOLVEMENT", "type_id" => $type1->id));
    $page7 = Page::firstOrCreate(array('title_fr' => 'ADHESION AUX VALEURS DE L’ENTREPRISE', 'title_en' => "ACCESSION TO THE VALUES OF THE COMPANY", "type_id" => $type1->id));
    $page8 = Page::firstOrCreate(array('title_fr' => 'QUESTIONS OUVERTES DE FIN DE QUESTIONNAIRE', 'title_en' => "OPEN QUESTIONS OF END SURVEY", "type_id" => $type1->id));

    // SOUS PAGE 1
    $sp1 = Page::firstOrCreate(array('title_fr' => 'Communication orale/écrite', 'title_en' => "Oral/written communication", "page_id" => $page1->id, "type_id" => $type1->id));
    $sp2 = Page::firstOrCreate(array('title_fr' => 'Écoute/efficacité relationnelle', 'title_en' => "Listening/relational efficiency", "page_id" => $page1->id, "type_id" => $type1->id));

    // SOUS PAGE 2
    $sp3 = Page::firstOrCreate(array('title_fr' => 'Stratégie/Vision', 'title_en' => "Strategy / Vision", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp4 = Page::firstOrCreate(array('title_fr' => 'Management de l’activité', 'title_en' => "Management of the activity", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp5 = Page::firstOrCreate(array('title_fr' => 'Accompagnement des femmes et des hommes de sa direction', 'title_en' => "Accompaniment of women and men in their management", "page_id" => $page2->id, "type_id" => $type1->id));
    $sp6 = Page::firstOrCreate(array('title_fr' => 'Prise de décision', 'title_en' => "Decision making", "page_id" => $page2->id, "type_id" => $type1->id));

    // SOUS PAGE 3
    $sp7 = Page::firstOrCreate(array('title_fr' => 'Travail d’équipe', 'title_en' => "Teamwork", "page_id" => $page3->id, "type_id" => $type1->id));
    $sp8 = Page::firstOrCreate(array('title_fr' => 'Entraide/solidarité', 'title_en' => "Mutual assistance/solidarity", "page_id" => $page3->id, "type_id" => $type1->id));

    // SOUS PAGE 4
    $sp9 = Page::firstOrCreate(array('title_fr' => 'Efficacité sous pression', 'title_en' => "Efficiency under pressure", "page_id" => $page4->id, "type_id" => $type1->id));

    $sp10_1 = Page::firstOrCreate(array('title_fr' => 'Flexibilité/créativité ', 'title_en' => "Flexibility/creativity", "page_id" => $page4->id, "type_id" => $type1->id));

    $sp10 = Page::firstOrCreate(array('title_fr' => 'Prise de risques', 'title_en' => "Risk taking", "page_id" => $page4->id, "type_id" => $type1->id));

    // SOUS PAGE 5
    $sp11 = Page::firstOrCreate(array('title_fr' => 'Analyse/pro-activité', 'title_en' => "Analysis/proactivity", "page_id" => $page5->id, "type_id" => $type1->id));
    $sp12 = Page::firstOrCreate(array('title_fr' => 'Mise en œuvre', 'title_en' => "Implementation", "page_id" => $page5->id, "type_id" => $type1->id));

    // SOUS PAGE 6    
    $sp13 = Page::firstOrCreate(array('title_fr' => 'Engagement/endurance/résistance', 'title_en' => "Commitment/endurance/strength", "page_id" => $page6->id, "type_id" => $type1->id));

    // SOUS PAGE 7
    $sp14 = Page::firstOrCreate(array('title_fr' => 'Intégrité/congruence', 'title_en' => "Integrity/congruence", "page_id" => $page7->id, "type_id" => $type1->id));

    // SOUS PAGE 8
    $sp15 = Page::firstOrCreate(array("page_id" => $page8->id, "type_id" => $type1->id));

    // QUESTIONS PAGE 1 - 1
    $q1 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) est à l'aise quand il(elle) fait une présentation orale.",
      "question_en" => "Q1 - He (she) is comfortable when he (she) makes an oral presentation.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q2 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) structure clairement ce qu'il(elle) dit ou écrit.",
      "question_en" => "Q2 - He (she) clearly structures what he (she) says or writes.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q3 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) n’adapte pas ses messages à ses interlocuteurs.",
      "question_en" => "Q3 - He (she) does not adapt his messages to his interlocutors.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q4 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) rédige des textes clairs et concis",
      "question_en" => "Q4 - He / she writes clear and concise texts.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);
    $q5 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) utilise l’oral et l’écrit à bon escient dans une communication bienveillante.",
      "question_en" => "Q5 - He (she) uses the spoken word and writes it wisely in a benevolent communication.",
      'page_id' => $sp1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 1 - 2
    $q6 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) monopolise la parole en réunion.",
      "question_en" => "Q1 - He (she) monopolizes the speech in meeting.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q7 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) répond avec transparence et discernement aux questions qui lui sont posées.",
      "question_en" => "Q2 - He (she) responds with transparency and discernment to the questions put to him.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q8 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) traite avec patience les réserves et objections.",
      "question_en" => "Q3 - He (she) patiently deals with reservations and objections.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q9 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) sait trouver des ressources pour garder la relation, même en situation délicate.",
      "question_en" => "Q4 - He (she) knows how to find resources to keep the relationship, even in a delicate situation.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q10 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) sait écouter et est capable de reformuler des positions différentes des siennes.",
      "question_en" => "Q5 - He (she) knows how to listen and is able to reformulate positions different from hers.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q11 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) définit les objectifs de son entité à partir des objectifs de l’entreprise.",
      "question_en" => "Q6 - He (she) defines the objectives of his entity based on the objectives of the company.",
      'page_id' => $sp2->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

     //QUESTION PAGE 2 - 1
    $q12 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) connait et comprend les enjeux globaux de l’entreprise.",
      "question_en" => "Q1 - He (she) knows and understands the global issues of the company.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "negative" => 1,
      "type" => 'radio'
    ]);

    $q13 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) cherche à obtenir l’adhésion de ses collaborateurs aux objectifs de l’entreprise.",
      "question_en" => "Q2 - He (she) seeks to obtain the adhesion of his collaborators to the objectives of the company.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q14 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il définit les objectifs de son entité sans y associer ses collaborateurs.",
      "question_en" => "Q3 - It defines the objectives of its entity without associating its collaborators.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q15 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) définit clairement les priorités de son entité.",
      "question_en" => "Q4 - He (she) clearly defines the priorities of his entity.",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q16 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) se positionne dans une perspective client en visant l’amélioration du Delivery",
      "question_en" => "Q5 - He (she) positions himself in a customer perspective by aiming at the improvement of the Delivery",
      'page_id' => $sp3->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 2 - 2
    $q17 = Question::firstOrCreate([
      'question_fr' => "Q1 - Ses collaborateurs ont une vision claire des objectifs et des priorités.",
      "question_en" => "Q1 - Its employees have a clear vision of objectives and priorities.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q18 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) identifie et diffuse l’information pertinente de façon satisfaisante.",
      "question_en" => "Q2 - He (she) identifies and disseminates relevant information satisfactorily.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q19 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) organise la remontée des informations issues de son environnement.",
      "question_en" => "Q3 - He (she) organizes the feedback of information from his environment.",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q20 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) court-circuite ses collaborateurs (ne respecte pas les niveaux de délégation).",
      "question_en" => "Q4 - He (she) bypasses his collaborators (does not respect the levels of delegation).",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q21 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle ) appuie sa crédibilité opérationnelle sur les différents niveaux de management (direction et terrain).",
      "question_en" => "Q5 - He (she) supports his operational credibility on the different levels of management (management and field).",
      'page_id' => $sp4->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);    

    // QUESTION PAGE 2 - 3
    $q22 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) aide ses collaborateurs à progresser.",
      "question_en" => "Q1 - He (she) helps his employees to progress.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q23 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) ne valorise pas les succès de ses collaborateurs.",
      "question_en" => "Q2 - He (she) does not value the success of his collaborators.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q24 = Question::firstOrCreate([
      'question_fr' => "Q3 -  Il(elle) est disponible pour ses collaborateurs.",
      "question_en" => "Q3 - He (she) is available for his collaborators.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q25 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) recadre ou sanctionne ses collaborateurs quand c’est nécessaire.",
      "question_en" => "Q4 - He (she) reframes or sanctions his collaborators when it is necessary.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q26 = Question::firstOrCreate([
      'question_fr' => "Il(elle) est à l’écoute de ses collaborateurs même quand ceux-ci rencontrent des difficultés personnelles.",
      "question_en" => "Q5 - He (she) listens to his collaborators even when they face personal difficulties.",
      'page_id' => $sp5->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);     

    // QUESTION PAGE 2 - 4
    $q27 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) tranche quand c'est à lui(elle) de décider.",
      "question_en" => "Q1 - He (she) slice when it is up to him (her) to decide.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q28 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) recueille le point de vue de ses collaborateurs quand la décision les implique.",
      "question_en" => "Q2 - He (she) collects the point of view of his collaborators when the decision implicates them",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q29 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) délègue avec le niveau de contrôle suffisant.",
      "question_en" => "Q3 - He (she) delegates with the sufficient level of control.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q30 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) manifeste de l’initiative dans son domaine de responsabilité.",
      "question_en" => "Q4 - He (she) demonstrates initiative in his area of ​​responsibility.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q31 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) prend ses décisions sans consulter les personnes concernées.",
      "question_en" => "Q5 - He (she) makes his decisions without consulting the persons concerned.",
      'page_id' => $sp6->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 3 - 1
    $q32 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) contribue activement à la coopération entre directions.",
      "question_en" => "Q1 - He (she) actively contributes to cooperation between directorates.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q33 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il (elle) consacre du temps pour faire avancer les projets des autres.",
      "question_en" => "Q2 - He (she) devotes time to advance the projects of the others.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q34 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) comprend les contraintes et le fonctionnement des autres directions.",
      "question_en" => "Q3 - He (she) understands the constraints and the functioning of the other directions.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q35 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ne tient pas compte des contraintes des autres directions.",
      "question_en" => "Q4 - He (she) does not take into account the constraints of the other directions.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q36 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) est aussi sensible aux résultats de l’entreprise qu’aux résultats de sa direction.",
      "question_en" => "Q5 - He (she) is as sensitive to the results of the company as to the results of his management.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q37 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) protège l’équipe du stress extérieur.",
      "question_en" => "Q6 - He (she) protects the team from outside stress.",
      'page_id' => $sp7->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);        

    // QUESTION PAGE 3 - 2
    $q38 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) encourage la coopération entre directions d'une façon générale.",
      "question_en" => "Q1 - He (she) encourages cooperation between directorates generally.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q39 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) fait passer l’intérêt collectif avec les intérêts individuels.",
      "question_en" => "Q2 - It (it) makes pass the collective interest with the individual interests.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q40 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) sensibilise ses collaborateurs à l’esprit d’entraide au sein de l’entreprise.",
      "question_en" => "Q3 - He (she) sensitizes his collaborators to the spirit of mutual aid within the company.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q41 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) organise régulièrement des moments de convivialité dans son service.",
      "question_en" => "Q4 - He (she) regularly organizes moments of conviviality in his service.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q42 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) hésite à proposer son aide aux autres directions. ",
      "question_en" => "Q5 - He (she) is reluctant to offer his help to other directions.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q43 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) défend avec conviction l’intérêt de l’équipe auprès de ses Responsables.",
      "question_en" => "Q6 - He (she) defends with conviction the interest of the team with its managers.",
      'page_id' => $sp8->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 1
    $q44 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) maîtrise les situations de crise.",
      "question_en" => "Q1 - He (she) controls crisis situations.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q45 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) accepte les changements imprévus de priorités avec sérénité.",
      "question_en" => "Q2 - He (she) accepts unforeseen changes of priorities with serenity.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q46 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il (elle) est la force de propositions pour améliorer les livrables.",
      "question_en" => "Q3 - He (she) is the force of proposals to improve the deliverables.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q47 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) a du mal à accélérer son rythme en cas de besoin.",
      "question_en" => "Q4 - He (she) is struggling to accelerate his pace when needed.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q48 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) reste organisé(e) même sous la pression de l'urgence.",
      "question_en" => "Q5 - He (she) remains organized (e) even under the pressure of the urgency.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q49 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) rassure ses collaborateurs dans les situations de crise.",
      "question_en" => "Q6 - He (she) reassures his collaborators in crisis situations.",
      'page_id' => $sp9->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 2
    $q50 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) est stimulé par le changement.",
      "question_en" => "Q1 - He (she) is stimulated by change.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q51 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) favorise l’innovation pour améliorer l’efficacité.",
      "question_en" => "Q2 - He (she) promotes innovation to improve efficiency.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q52 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) semble regretter le passé.",
      "question_en" => "Q3 - He (she) seems to regret the past.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q53 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) peut travailler dans des environnements différents.",
      "question_en" => "Q4 - He (she) can work in different environments.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q54 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) manifeste des facilités à élaborer des solutions originales.",
      "question_en" => "Q5 - He (she) manifests facilities to develop original solutions.",
      'page_id' => $sp10_1->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 4 - 3
    $q55 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) recherche systématiquement les solutions les plus « confortables » (et non les solutions les plus pertinentes)",
      "question_en" => "Q1 - He (she) systematically searches for the most 'comfortable' solutions (and not the most relevant solutions).",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q56 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) accepte facilement de sortir des routines quand cela est pertinent.",
      "question_en" => "Q2 - He (she) readily agrees to exit routines when relevant.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q57 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) prend des risques réfléchis pour agir de façon efficace.",
      "question_en" => "Q3 - He (she) takes thoughtful risks to act effectively.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q58 = Question::firstOrCreate([
      'question_fr' => "Q4 - Dans une nouvelle démarche, il(elle) pense autant aux avantages possibles qu'aux inconvénients.",
      "question_en" => "Q4 - In a new approach, he (she) thinks as much about the possible advantages as the disadvantages.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q59 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) identifie les problèmes et alerte suffisamment tôt.",
      "question_en" => "Q5 - He (she) identifies problems and alerts early enough.",
      'page_id' => $sp10->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);       

    // QUESTION PAGE 5 - 1
    $q60 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) élabore et compare différentes solutions si nécessaire.",
      "question_en" => "Q1 - He (she) elaborates and compares different solutions if necessary",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q61 = Question::firstOrCreate([
      'question_fr' => "Q2 - Face à un problème, il(elle) recueille toute l’information pertinente à la décision.",
      "question_en" => "Q2 - Faced with a problem, he (she) collects all the information relevant to the decision.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q62 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) présente la situation avec une vue globale sur les enjeux.",
      "question_en" => "Q3 - He (she) presents the situation with a global view of the issues.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q63 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) n’anticipe pas les situations de crise.",
      "question_en" => "Q4 - He (she) does not anticipate crisis situations.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q64 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) propose des solutions constructives aux problèmes rencontrés.",
      "question_en" => "Q5 - He (she) proposes constructive solutions to the problems encountered.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q65 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) sait équilibrer les objectifs du client et ceux de son entreprise.",
      "question_en" => "Q6 - He (she) knows how to balance the objectives of the client and those of his company.",
      'page_id' => $sp11->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 5 - 2
    $q66 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) donne des objectifs précis et atteignables à son équipe.",
      "question_en" => "Q1 - He (she) gives precise objectives and achievable to his team.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q67 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) bâtit des plans d'actions pour soutenir ses projets.",
      "question_en" => "Q2 - He (she) builds action plans to support his projects.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q68 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) applique et fais appliquer rapidement les décisions prises.",
      "question_en" => "Q3 - He/she applies and quickly enforces the decisions made.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q69 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) suit l’avancement des plans d’actions jusqu’à leur aboutissement.",
      "question_en" => "Q4 - He (she) follows the progress of the action plans until their completion.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q70 = Question::firstOrCreate([
      'question_fr' => "Q5 - En cas de problème/crise, il(elle) identifie les moyens nécessaires aux actions et propose leur adaptation si nécessaire.",
      "question_en" => "Q5 - In the event of a problem/crisis, he (she) identifies the means necessary for the actions and proposes their adaptation if necessary.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q71 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) ne tient pas compte du contexte avant d'agir.",
      "question_en" => "Q6 - He (she) does not consider the context before acting.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q72 = Question::firstOrCreate([
      'question_fr' => "Q7 - Il(elle) se donne les moyens de présenter un travail fiable.",
      "question_en" => "Q7 - He (she) gives himself the means to present a reliable work.",
      'page_id' => $sp12->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 6 - 1
    $q73 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) assume les décisions même lorsqu'il(elle) ne les partage pas.",
      "question_en" => "Q1 - He (she) assumes the decisions even when he/she does not share them.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q74 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) a un haut niveau d’exigence pour lui(elle)-même.",
      "question_en" => "Q2 - He (she) has a high level of requirement for him (it) himself.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q75 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) va jusqu’au bout de ce qu’il(elle) entreprend.",
      "question_en" => "Q3 - He (she) goes to the end of what he (she) undertakes.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q76 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ajuste ses exigences à la situation.",
      "question_en" => "Q4 - He (she) adjusts his requirements to the situation.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q77 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) se décourage facilement devant les difficultés.",
      "question_en" => "Q5 - He (she) is easily discouraged by the difficulties.",
      'page_id' => $sp13->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    // QUESTION PAGE 7
    $q78 = Question::firstOrCreate([
      'question_fr' => "Q1 - Il(elle) connait et promeut les valeurs et la stratégie de l'entreprise.",
      "question_en" => "Q1 - He (she) knows and promotes the values ​​and strategy of the company.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q79 = Question::firstOrCreate([
      'question_fr' => "Q2 - Il(elle) s’intéresse réellement aux autres.",
      "question_en" => "Q2 - He (she) really cares about others.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q80 = Question::firstOrCreate([
      'question_fr' => "Q3 - Il(elle) traite de façon digne et respectueuse l’ensemble de ses interlocuteurs (internes et externes).",
      "question_en" => "Q3 - He (she) treats in a dignified and respectful way all his interlocutors (internal and external).",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q81 = Question::firstOrCreate([
      'question_fr' => "Q4 - Il(elle) ne respecte pas les procédures en vigueur dans l’entreprise.",
      "question_en" => "Q4 - He (she) does not respect the procedures in force in the company.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      'negative' => 1,
      "type" => 'radio'
    ]);

    $q82 = Question::firstOrCreate([
      'question_fr' => "Q5 - Il(elle) incarne dans son management les valeurs de l’entreprise (sécurité, honnêteté, responsabilité, respect des personnes).",
      "question_en" => "Q5 - He (she) embodies in his management the values ​​of the company (safety, honesty, responsibility, respect for people).",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    $q83 = Question::firstOrCreate([
      'question_fr' => "Q6 - Il(elle) fait remonter les infos pertinentes sur des risques potentiels.",
      "question_en" => "Q6 - He (she) traced the relevant information on potential risks.",
      'page_id' => $sp14->id,
      'type_id' => $type1->id,
      "type" => 'radio'
    ]);

    // QUESTIONNAIRE PAGE 8 - 1
    $q84 = Question::firstOrCreate([
      'question_fr' => "Q1 - Quelles sont les qualités que vous appréciez chez cette personne ?",
      "question_en" => "Q1 - What qualities do you like about this person ?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    $q85 = Question::firstOrCreate([
      'question_fr' => "Q2 - Concernant son management, quelles sont les pistes d’amélioration que vous lui suggèreriez ?",
      "question_en" => "Q2 - Regarding its management, what are the areas of improvement that you would suggest to him ?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    $q86 = Question::firstOrCreate([
      'question_fr' => "Q3 - De façon plus générale, quelles propositions lui feriez-vous afin qu’elle soit encore plus efficiente ?",
      "question_en" => "Q3 - More generally, what proposals would you make to make it even more efficient?",
      'page_id' => $sp15->id,
      'type_id' => $type1->id,
      "type" => 'text'
    ]);

    // REPONSE POUR CHAQUE QUESTION
    for($i= 1; $i< 84; $i++){

      // POSITIVE
      if(${"q".$i}->negative == 0){

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "pas du tout d'accord",
            "reponse_en" => "not agree at all",
            "score" => 1,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt pas d'accord",
            "reponse_en" => "rather disagree",
            "score" => 2,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt d'accord",
            "reponse_en" => "somewhat agree",
            "score" => 3,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "tout à fait d'accord",
            "reponse_en" => "Totally agree",
            "score" => 4,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "je ne sais pas",
            "reponse_en" => "I don't know",
            "score" => 0,
            "question_id" => ${"q".$i}->id
          ]
        );

      } else { // NEGATIVE

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "pas du tout d'accord",
            "reponse_en" => "not agree at all",
            "score" => 4,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt pas d'accord",
            "reponse_en" => "rather disagree",
            "score" => 3,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "plutôt d'accord",
            "reponse_en" => "somewhat agree",
            "score" => 2,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "tout à fait d'accord",
            "reponse_en" => "Totally agree",
            "score" => 1,
            "question_id" => ${"q".$i}->id
          ]
        );

        Reponse::firstOrCreate(
          [
            'reponse_fr' => "je ne sais pas",
            "reponse_en" => "I don't know",
            "score" => 0,
            "question_id" => ${"q".$i}->id
          ]
        );
      }
    }
    }

}

class PDF_Sector extends Fpdi
{
    function Sector($xc, $yc, $r, $a, $b, $style='FD', $cw=true, $o=90)
    {
        $d0 = $a - $b;
        if($cw){
            $d = $b;
            $b = $o - $a;
            $a = $o - $d;
        }else{
            $b += $o;
            $a += $o;
        }
        while($a<0)
            $a += 360;
        while($a>360)
            $a -= 360;
        while($b<0)
            $b += 360;
        while($b>360)
            $b -= 360;
        if ($a > $b)
            $b += 360;
        $b = $b/360*2*M_PI;
        $a = $a/360*2*M_PI;
        $d = $b - $a;
        if ($d == 0 && $d0 != 0)
            $d = 2*M_PI;
        $k = $this->k;
        $hp = $this->h;
        if (sin($d/2))
            $MyArc = 4/3*(1-cos($d/2))/sin($d/2)*$r;
        else
            $MyArc = 0;
        //first put the center
        $this->_out(sprintf('%.2F %.2F m',($xc)*$k,($hp-$yc)*$k));
        //put the first point
        $this->_out(sprintf('%.2F %.2F l',($xc+$r*cos($a))*$k,(($hp-($yc-$r*sin($a)))*$k)));
        //draw the arc
        if ($d < M_PI/2){
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }else{
            $b = $a + $d/4;
            $MyArc = 4/3*(1-cos($d/8))/sin($d/8)*$r;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }
        //terminate drawing
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='b';
        else
            $op='s';
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3 )
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            $x1*$this->k,
            ($h-$y1)*$this->k,
            $x2*$this->k,
            ($h-$y2)*$this->k,
            $x3*$this->k,
            ($h-$y3)*$this->k));
    }
}

class PDF_Diag extends PDF_Sector {
    var $legends;
    var $wLegend;
    var $sum;
    var $NbVal;

    function PieChart($w, $h, $data, $format, $colors=null)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $hLegend = 5;
        $radius = min($w - $margin * 4 - $hLegend - $this->wLegend, $h - $margin * 2);
        $radius = floor($radius / 2);
        $XDiag = $XPage + $margin + $radius;
        $YDiag = $YPage + $margin + $radius;
        if($colors == null) {
            for($i = 0; $i < $this->NbVal; $i++) {
                $gray = $i * round(255 / $this->NbVal);
                $colors[$i] = array($gray,$gray,$gray);
            }
        }

        //Sectors
        $this->SetLineWidth(0.2);
        $angleStart = -90;
        $angleEnd = 270;
        $i = 0;
        foreach($data as $val) {
            $angle = ($val * 360) / doubleval($this->sum);
            if ($angle != 0) {
                $angleEnd = $angleStart + $angle;
                $this->SetDrawColor(255, 255, 255);
                $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
                $this->Sector($XDiag, $YDiag, $radius, $angleStart, $angleEnd);
                $angleStart += $angle;
            }
            $i++;
        }

        //Legends
        // $this->SetFont('Courier', '', 10);
        // $x1 = $XPage + 2 * $radius + 4 * $margin;
        // $x2 = $x1 + $hLegend + $margin;
        // $y1 = $YDiag - $radius + (2 * $radius - $this->NbVal*($hLegend + $margin)) / 2;
        // for($i=0; $i<$this->NbVal; $i++) {
        //     $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
        //     $this->Rect($x1, $y1, $hLegend, $hLegend, 'DF');
        //     $this->SetXY($x2,$y1);
        //     $this->Cell(0,$hLegend,$this->legends[$i]);
        //     $y1+=$hLegend + $margin;
        // }
    }

    function BarDiagram($w, $h, $data, $format, $color=null, $maxVal=0, $nbDiv=4)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $YDiag = $YPage + $margin;
        $hDiag = floor($h - $margin * 2);
        $XDiag = $XPage + $margin * 2 + $this->wLegend;
        $lDiag = floor($w - $margin * 3 - $this->wLegend);
        if($color == null)
            $color=array(155,155,155);
        if ($maxVal == 0) {
            $maxVal = max($data);
        }
        $valIndRepere = ceil($maxVal / $nbDiv);
        $maxVal = $valIndRepere * $nbDiv;
        $lRepere = floor($lDiag / $nbDiv);
        $lDiag = $lRepere * $nbDiv;
        $unit = $lDiag / $maxVal;
        $hBar = floor($hDiag / ($this->NbVal + 1));
        $hDiag = $hBar * ($this->NbVal + 1);
        $eBaton = floor($hBar * 80 / 100);

        $this->SetLineWidth(0.2);
        $this->Rect($XDiag, $YDiag, $lDiag, $hDiag);

        $this->SetFont('Courier', '', 10);
        $this->SetFillColor($color[0],$color[1],$color[2]);
        $i=0;
        foreach($data as $val) {
            //Bar
            $xval = $XDiag;
            $lval = (int)($val * $unit);
            $yval = $YDiag + ($i + 1) * $hBar - $eBaton / 2;
            $hval = $eBaton;
            $this->Rect($xval, $yval, $lval, $hval, 'DF');
            //Legend
            $this->SetXY(0, $yval);
            $this->Cell($xval - $margin, $hval, $this->legends[$i],0,0,'R');
            $i++;
        }

        //Scales
        for ($i = 0; $i <= $nbDiv; $i++) {
            $xpos = $XDiag + $lRepere * $i;
            $this->Line($xpos, $YDiag, $xpos, $YDiag + $hDiag);
            $val = $i * $valIndRepere;
            $xpos = $XDiag + $lRepere * $i - $this->GetStringWidth($val) / 2;
            $ypos = $YDiag + $hDiag - $margin;
            $this->Text($xpos, $ypos, $val);
        }
    }

    function SetLegends($data, $format)
    {
        $this->legends=array();
        $this->wLegend=0;
        $this->sum=array_sum($data);
        $this->NbVal=count($data);
        foreach($data as $l=>$val)
        {
            $p=sprintf('%.2f',$val/$this->sum*100).'%';
            $legend=str_replace(array('%l','%v','%p'),array($l,$val,$p),$format);
            $this->legends[]=$legend;
            $this->wLegend=max($this->GetStringWidth($legend),$this->wLegend);
        }
    }


    
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Test;
use App\Type;
use App\ReponseRepondant;
use App\Reponse;
use App\Question;
use App\TestRepondant;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Image;

class ClimageFrontController extends Controller
{

    use AuthenticatesUsers;

    public function index(){
        $question["formation"] = Type::where('name', "climage-formation")->first();
        $question["reunion"]  = Type::where('name', "climage-reunion")->first();
        $question["transformation"]  = Type::where('name', "climage-transformation")->first();

        $climages = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%climage%");
        })->get();
        
        return view("diagnostic/home");
        //return view("climage/front/index", ["climages" => $climages, "questions" => $question]);
    }

    public function personal($id, $code){
        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        $reponses = [];
   
        if($code == $climage->code ){
            $page = Type::where('name', $type->name)->first()->pages[0];

            $pages = Type::where('name', $type->name)->first()->pages;

            $cur = 1;
            foreach ($pages as $key => $value) {
                switch ($cur) {
                    case 1:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/personal";
                        break;
                    case 2:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formTwo";
                        break;
                    case 3:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formThree";
                        break;
                    default:
                        # code...
                        break;
                }
                $cur +=1;
               
            }

            foreach ($page->questions as $key => $value) {
                // $res =  ReponseRepondant::where([

                //     ['test_id',"=",  $id],
                //     ['repondant_id',"=",  Auth::user()->id],
                //     ['question_id',"=", $value->id]
                // ])->first();

                // $rep = $res ? $res['reponse_text'] : "";

                $reponses[$value->id] = ReponseRepondant::where([

                    ['test_id',"=",  $id],
                    ['repondant_id',"=",  Auth::user()->id],
                    ['question_id',"=", $value->id]
                ])->first();
            //     echo "<pre> ######";
            //     var_dump($rep);
            //     var_dump($value->id);
            //     var_dump($reponses[$value->id] ? $reponses[$value->id]['reponse_text'] : '');
            
            // echo "?????? </pre>";
            }
            
            // die();

            return view("climage/front/personal", ["climage" => $climage, "page" => $page, "lang" => \App::getLocale(), "reponses" => $reponses, "pages" => $pages]);
        }

        
        return redirect()->route("climage-front");
        
    }

    public function savePersonal(Request $request){
        ini_set('memory_limit','1024M');
        // var_dump($request->reponse_question);
        // die();
        // $validatedData = $request->validate([
        //     'reponse_question.*' => "required|string"
        // ]);
        $climage = Test::find($request->id);



        // var_dump($request->id);
        // die();
        $type = Type::find($climage->type_id);

       

        //save personal
        \DB::table('test_repondant')->updateOrInsert(
            ['test_id' => $request->id, 'repondant_id' => Auth::user()->id],
            ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
        );

        $user = User::find(Auth::user()->id);
    
           
        //on efface ce qui est avant ou ce qui a le meme nom
        // if($user->avatar_url){
        //     Storage::delete('public/'.$user->avatar_url);
        // }
        // $path =  $request->file('avatar')->storeAs(
        //     'public/climage',$user->id //$user->id est le nom du fichier a partir de maintenant
        // );
        // $pathExplode = explode('/', $path);
        // $path =$pathExplode[1].'/'.$pathExplode[2];
        // $user->avatar_url = url('/storage/'.$path);
        if (preg_match('/^[a-z0-9 .\-]+$/i', $request->nom)){

            if(preg_match('/^[a-z0-9 .\-]+$/i', $request->prenom)){

                if(preg_match('/^[a-z0-9 .\-]+$/i', $request->organisation)){

                    if($request->avatar){
                        $file=$request->avatar;
                        $img=Image::make($file)->resize(250,250);
                        }else if($user->avatar_url){
                            // $img=Image::make(asset('/storage/climage/1'))->resize(250,250);

                            $img=Image::make(public_path('/storage/climage/'.$user->id) )->resize(250,250);
                            //var_dump($img);

                        }
                        if(isset($img)){
                            
                            if($request->input('rotate')!='')
                            {
                                $img->rotate(-$request->input('rotate'));
                            }
                           // if($request->input('w')!==null && isset($request->input('h')) && isset($request->input('x1')) && isset($request->input('y1'))){

                            if (($request->input('w')!='' && $request->input('h')!='') && ($request->input('w')!='0' && $request->input('h')!='0')) {
                                $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
                            }
                            //}
                            $img->encode('jpg', 90);
                            //Supprimer l'ancien avatar
                            Storage::delete('public/climage/'.$user->id);
                            Storage::put( $user->id. '.jpg', $img);
                            Storage::move($user->id.'.jpg', 'public/climage/'.$user->id );
                            // $img->crop(0, 2, 3, 1);
                            // $img->save('public/storage/climage/','Antsa');
                               
                            
                            $user->avatar_url=url('/storage/climage/'.$user->id);
                        }
                        
                        $user->username = $request->prenom." ".$request->input('nom');
                        $user->firstname = $request->nom;
                        $user->lastname = $request->input('prenom');
                        $user->organisation = $request->input('organisation');
                        $user->save();
                        
                        /*if($request->avatar_url){
                            $user = User::find(Auth::user()->id);
                            $user->avatar_url = $request->avatar_url;
                            $user->save();
                        }*/
                        //echo asset('storage/file.txt');

                        foreach ($request->reponse_question as $key => $value) {
                            $repondant_question_id = \DB::table('reponse_repondant')->updateOrInsert(
                                ['question_id' =>  $key, 'repondant_id' => Auth::user()->id, "test_id" => $climage->id],
                                ["reponse_text" => $value]
                            );

                        }
                        $question = Type::where('name', $type->name)->first()->pages[1];

                       return redirect()->route('climage-front-formTwo', ['id' => $request->id, "code" => $request->code]);
                } else {
        //return redirect('/diagnostics/')->with('error_code', 'Auncune test climage trouvée!');
                    return redirect('/diagnostics/climage/'.$request->id.'/'.$request->code.'/personal')/*->route('climage-front-personal', ['id' => $request->id, "code" => $request->code])*/->with('error_orga',"emoticone");
                }
            } else {
                return redirect()->route('climage-front-personal', ['id' => $request->id, "code" => $request->code])->with('error_pre',"emoticone");
            }
        } else {
            return redirect()->route('climage-front-personal', ['id' => $request->id, "code" => $request->code])->with('error_nom',"emoticone");
        }       
         
    }

    public function formTwo($id, $code){
        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        if($code == $climage->code ){
            $page = Type::where('name', $type->name)->first()->pages[1];

            $pages = Type::where('name', $type->name)->first()->pages;
            
            $cur = 1;
            foreach ($page->questions as $key => $value) {
               
                $reponses[$value->id] = ReponseRepondant::where([

                    ['test_id',"=",  $id],
                    ['repondant_id',"=",  Auth::user()->id],
                    ['question_id',"=", $value->id]
                ])->first();
          
            }
            foreach ($pages as $key => $value) {
                switch ($cur) {
                    case 1:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/personal";
                        break;
                    case 2:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formTwo";
                        break;
                    case 3:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formThree";
                        break;
                    default:
                        # code...
                        break;
                }
                $cur +=1;
               
            }
            return view("climage/front/formTwo", ["climage" => $climage, "page" => $page, "lang" => \App::getLocale(), "reponses" => $reponses, "pages" => $pages]);
        }

        
        return redirect()->route("climage-front");
        
    }

    public function saveFormTwo(Request $request){
        // $validatedData = $request->validate([
        //     'reponse_question' => "required|array|min:2",
        //     'reponse_question.*' => "required|min:1|max:3"

        // ]);
        $climage = Test::find($request->id);

        $type = Type::find($climage->type_id);        

        //save personal
        \DB::table('test_repondant')->updateOrInsert(
            ['test_id' => $request->id, 'repondant_id' => Auth::user()->id],
            ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
        );

        // // delete existing answer
        $page = Type::where('name', $type->name)->first()->pages[1];
        foreach ($page->questions as $key => $question) {
            foreach ($question->reponses as $key2 => $reponse) {
                $reponses = ReponseRepondant::where([
                    [ "question_id", "=", $question->id],
                    [ "repondant_id", "=", Auth::user()->id],
                    [ "test_id", "=", $climage->id ]
                    // [ "reponse_id", "=", $reponse->id ],

                ])->get();
                foreach ($reponses as $key => $rep) {
                    $rep_date = $rep->updated_at;

                    $parseDate = new \DateTime($rep_date, new \DateTimezone('Europe/Paris'));
                    $dateActuel = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                    $interval = $dateActuel->diff($parseDate);


                    if((int)$interval->format("%y") == 0 && (int)$interval->format("%m") == 0 && (int)$interval->format("%d") == 0 && (int)$interval->format("%h") == 0){

                        ReponseRepondant::find($rep->id)->delete();
                    }
                }
            }
        }
        if($climage->type->name == "climage-interlocuteur"){
            foreach ($request->reponse_question as $key => $reponse) {
                $reponseId = Reponse::where("score",$reponse)->where("question_id",$key)->first();
                $date = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                $rep = new ReponseRepondant;
                $rep->question_id = $key;
                $rep->repondant_id = Auth::user()->id;
                $rep->test_id = $climage->id;
                $rep->reponse_id = $reponseId->id;
                $rep->updated_at = $date->format('Y-m-d H:00:00');
                $rep->save();
            }         

        } else {
            foreach ($request->reponse_question as $key => $reponse) {
                foreach ($reponse as $reponse_id => $value) {
                    $date = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                    $rep = new ReponseRepondant;
                    $rep->question_id = $key;
                    $rep->repondant_id = Auth::user()->id;
                    $rep->test_id = $climage->id;
                    $rep->reponse_id = $reponse_id;
                    $rep->updated_at = $date->format('Y-m-d H:00:00');
                    $rep->save();
                }         
            }
        }

        return redirect()->route('climage-front-formThree', ['id' => $request->id, "code" => $request->code]);
       
        
    }

    public function formThree($id, $code){

        

        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        if($code == $climage->code ){
            $question = Type::where('name', $type->name)->first()->pages[2];

            $pages = Type::where('name', $type->name)->first()->pages;

            $cur = 1;
            foreach ($pages as $key => $value) {
                switch ($cur) {
                    case 1:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/personal";
                        break;
                    case 2:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formTwo";
                        break;
                    case 3:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formThree";
                        break;

                    default:
                        # code...
                        break;
                }
                $cur +=1;
               
            }

            return view("climage/front/formThree", ["climage" => $climage, "page" => $question, "lang" => \App::getLocale(), "pages" => $pages]);
        }

        
        return redirect()->route("climage-front");
        
    }

    public function saveFormThree(Request $request){
        // $validatedData = $request->validate([
        //     'reponse_question' => "required|array",
        //     'reponse_question.*' => "required|array|min:3|max:3"

        // ]);
        $climage = Test::find($request->id);

        // var_dump($request->id);
        // die();
        $type = Type::find($climage->type_id);

       

        //save personal
        \DB::table('test_repondant')->updateOrInsert(
            ['test_id' => $request->id, 'repondant_id' => Auth::user()->id],
            ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
        );

        // // delete existing answer
        $page = Type::where('name', $type->name)->first()->pages[2];
        foreach ($page->questions as $key => $question) {
            foreach ($question->reponses as $key2 => $reponse) {
                $reponses = ReponseRepondant::where([
                    [ "question_id", "=", $question->id],
                    [ "repondant_id", "=", Auth::user()->id],
                    [ "test_id", "=", $climage->id ]
                    // [ "reponse_id", "=", $reponse->id ],

                ])->get();
                foreach ($reponses as $key => $rep) {
                    $rep_date = $rep->updated_at;

                    $parseDate = new \DateTime($rep_date, new \DateTimezone('Europe/Paris'));
                    $dateActuel = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                    $interval = $dateActuel->diff($parseDate);
                    if((int)$interval->format("%y") == 0 && (int)$interval->format("%m") == 0 && (int)$interval->format("%d") == 0 && (int)$interval->format("%h") == 0){
                        ReponseRepondant::find($rep->id)->delete();
                    }
                }
            }
        }

        if($climage->type->name == "climage-interlocuteur"){
            foreach ($request->reponse_question as $key => $reponse) {
                $reponseId = Reponse::where("score",$reponse)->where("question_id",$key)->first();
                $date = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                $rep = new ReponseRepondant;
                $rep->question_id = $key;
                $rep->repondant_id = Auth::user()->id;
                $rep->test_id = $climage->id;
                $rep->reponse_id = $reponseId->id;
                $rep->updated_at = $date->format('Y-m-d H:00:00');
                $rep->save();
            }         

        } else {
            foreach ($request->reponse_question as $key => $reponse) {
                foreach ($reponse as $reponse_id => $value) {
                    $date = new \DateTime('now', new \DateTimezone('Europe/Paris'));
                    $rep = new ReponseRepondant;
                    $rep->question_id = $key;
                    $rep->repondant_id = Auth::user()->id;
                    $rep->test_id = $climage->id;
                    $rep->reponse_id = $reponse_id;
                    $rep->updated_at = $date->format('Y-m-d H:00:00');
                    $rep->save();
                }         
            }
        }

        return redirect()->route('climage-front-merci', ['id' => $request->id, "code" => $request->code]);
       
        
    }

    public function merci($id, $code){
        $climage = Test::find($id);
        $type = Type::find($climage->type_id);
        $nbUser = count(TestRepondant::where("test_id", $id)->get());

        // graph data
        $question3 = Type::where('name', $type->name)->first()->pages[2]->questions[0];

        // if($climage->type->name == "climage-interlocuteur"){
        //     $question4 = Type::where('name', $type->name)->first()->pages[2]->questions[1];
        // }
        
        $page2 = Type::where('name', $type->name)->first()->pages[1];

        $page3 = Type::where('name', $type->name)->first()->pages[2];
        
        $dates = ReponseRepondant::where("test_id", $id)->distinct()->select("updated_at")->get();
        $dateArray = array();
        $dateArray2 = array();
        foreach ($dates as $key => $date) {
            $day = explode(" ",$date->updated_at);
            $d = $day[0];
            if(!in_array($d, $dateArray)){
                $getHeures = ReponseRepondant::where("test_id",$id)->where("updated_at","like","%".$d."%")->get();
                $hArray = array();
                $repVal = [];
                $vote = [];
                $data = [];
                $dataGraph = [];
                $dataCli = [];
                $dataClimage = [];
                $dataVote = [];
                $nbRepondants = array();
                foreach ($getHeures as $key => $heure) {
                    $hr = explode(" ",$heure->updated_at);
                    $h = $hr[1];
                    if(!in_array($h, $hArray)){
                        $a = 0;$c = 0;$g = 0;$o = 0;
                        $i = 0;$de = 0;$he = 0;$p = 0;
                        $b = 0;$e = 0;$k = 0;$q = 0;
                        $j = 0;$f = 0;$n = 0;$m = 0;
                        $countSerieux = 0;
                        $countEnjoue = 0;
                        $countSoi = 0;
                        $countAutrui = 0;

                        $nbRepondant = ReponseRepondant::where("test_id",$id)->where("updated_at","like","%".$d." ".$h."%")->get();
                        $users = array();
                        foreach ($nbRepondant as $key => $nbRep) {
                            if(!in_array($nbRep->repondant_id,$users)){
                                array_push($users, $nbRep->repondant_id);
                            }
                        }

                        $countUsers = count($users);

                        foreach ($page2->questions as $qkey => $question) {

                            $count = 0;
                            foreach ($question->reponses as $rep => $reponse) {
                                //var_dump($qkey."==>".$rep."=>".$reponse->score);

                                $repInterlocuteur = \DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get();

                                foreach ($repInterlocuteur as $key => $value) {
                                    $valScore = Reponse::where("question_id",$value->question_id)->where("id",$value->reponse_id)->first();
                                    (($qkey == 0 ? $dataVote["q1"] = $countSerieux + $valScore->score : ($qkey == 1 ? $dataVote["q2"] = $countEnjoue + $valScore->score : ($qkey == 2 ? $dataVote["q3"] = $countSoi + $valScore->score : ($qkey == 3 ? $dataVote["q4"] = $countAutrui + $valScore->score : 0)))));
                                }

                                switch ($count) {
                                    case 0:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["a"] = $nb : $dataVote["i"] = $nb;
                                        break;

                                    case 1:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["b"] = $nb : $dataVote["j"] = $nb;
                                        break;

                                    case 2:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["c"] = $nb : $dataVote["k"] = $nb;
                                        break;

                                    case 3:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["d"] = $nb : $dataVote["m"] = $nb;
                                        break;

                                    case 4:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["e"] = $nb : $dataVote["n"] = $nb;
                                        break;

                                    case 5:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["f"] = $nb : $dataVote["o"] = $nb;
                                        break;

                                    case 6:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["g"] = $nb : $dataVote["p"] = $nb;
                                        break;

                                    case 7:
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        $qkey == 0 ? $dataVote["h"] = $nb : $dataVote["q"] = $nb;
                                        break;
                                    
                                    default:
                                        # code...
                                        break;
                                }

                                $count += 1;

                                $nb = count(\DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get());
                                //$reponse->vote = floatval($nb /$countUsers)*100;
                                $reponse->vote = floatval($nb /$countUsers)*100;
                                if(!array_key_exists($reponse->id,$repVal)){
                                    $repVal[$rep][$question->id] = number_format($reponse->vote, 2); 
                                }
                            }


                            // foreach ($question->reponses as $rep => $reponse) {
                            //     $nb = count(\DB::table('reponse_repondant')->where([
                            //         ["question_id", "=", $question->id],
                            //         ["test_id", "=", $id],
                            //         ["reponse_id", "=", $reponse->id],
                            //         ["updated_at","like","%".$d." ".$h."%"]
                            //     ])->get());
                            //     $reponse->vote = floatval($nb /$nbUser)*100;
                            //     if(!array_key_exists($rep[$question->id],$repVal)){
                            //         $repVal[$rep][$question->id] = number_format($reponse->vote, 2); 
                            //     }
                            // }
                             
                        }

                        $countConforme = 0;
                        $countTransgressif = 0;
                        $countSympha = 0;
                        $countMaitrise = 0;
                        foreach ($page3->questions as $qkey => $question) {
                            $count = 0;
                            foreach ($question->reponses as $rep => $reponse) {
                                $repInterlocuteur2 = \DB::table('reponse_repondant')->where([
                                    ["question_id", "=", $question->id],
                                    ["test_id", "=", $id],
                                    ["reponse_id", "=", $reponse->id],
                                    ["updated_at","like","%".$d." ".$h."%"]
                                ])->get();

                                foreach ($repInterlocuteur2 as $key => $value) {
                                    $valScore = Reponse::where("question_id",$value->question_id)->where("id",$value->reponse_id)->first();
                                    (($qkey == 0 ? $dataVote["q5"] = $countConforme + $valScore->score : ($qkey == 1 ? $dataVote["q6"] = $countTransgressif + $valScore->score : ($qkey == 2 ? $dataVote["q7"] = $countSympha + $valScore->score : ($qkey == 3 ? $dataVote["q8"] = $countMaitrise + $valScore->score : 0)))));
                                }
                            }
                        }

                        array_push($vote, $repVal);

                        if($nbUser >0){

                            $cur = 1;
 
                            foreach ($question3->reponses as $key => $reponse) {
                                switch ($cur) {
                                    case 1:
                                        # 3-a
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());

                                        //$data['serieux'] = number_format(floatval($nb /$nbUser)*100,0);
                                        $data['serieux'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["1"] = $nb;
                                       
                                      
                                        break;
                                    case 2:
                                        # 3-b
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                         $data['enjoue'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["2"] = $nb;

                                        break;
                                    case 3:
                                        # 3-c
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['conforme'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["3"] = $nb;
                                        break;
                                    case 4:
                                        # 3-d
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['transgressif'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["4"] = $nb;
                                        break;
                                    case 5:
                                        # 4-a
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['soi'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["5"] = $nb;
                                        break;
                                    case 6:
                                        # 4-b
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['autrui'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["6"] = $nb;
                                        break;
                                    case 7:
                                        # 4-c
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['sympathie'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["7"] = $nb;
                                        break;
                                    case 8:
                                        # 4-d
                                        $nb = count(\DB::table('reponse_repondant')->where([
                                            ["question_id", "=", $question3->id],
                                            ["test_id", "=", $id],
                                            ["reponse_id", "=", $reponse->id],
                                            ["updated_at","like","%".$d." ".$h."%"]
                                        ])->get());
                                        $data['maitrise'] = number_format(floatval($nb /$countUsers)*100,0);
                                        $dataVote["8"] = $nb;
                                        break;
                                    
                                    default:
                                        # code...
                                        break;
                                }

                                $cur +=1;
                            }
                            // POUR LE CLIMAGE NEGOCIATION QUESTION 4
                            // $cur = 1;
                            // if(isset($question4) && $question4->reponses){
                            //     foreach ($question4->reponses as $key => $reponse) {
                            //         //var_dump($reponse->reponse_fr);
                            //         switch ($cur) {
                            //             case 1:
                            //                 # 3-a
                            //                 $nb = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 $dataVote["4a"] = $nb;
                            //                 break;
                            //             case 2:
                            //                 # 3-b
                            //                 $dataVote["4b"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                            //             case 3:
                            //                 # 3-c
                            //                 $dataVote["4c"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                            //             case 4:
                            //                 # 3-d
                            //                 $dataVote["4d"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                            //             case 5:
                            //                 # 4-a
                            //                 $dataVote["4e"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                            //             case 6:
                            //                 # 4-b
                            //                 $dataVote["4f"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                                            
                            //                 break;
                            //             case 7:
                            //                 # 4-c
                            //                 $dataVote["4g"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                            //             case 8:
                            //                 # 4-d
                            //                 $dataVote["4h"] = count(\DB::table('reponse_repondant')->where([
                            //                     ["question_id", "=", $question3->id],
                            //                     ["test_id", "=", $id],
                            //                     ["reponse_id", "=", $reponse->id],
                            //                     ["updated_at","like","%".$d." ".$h."%"]
                            //                 ])->get());
                            //                 break;
                                        
                            //             default:
                            //                 # code...
                            //                 break;
                            //         }

                            //         $cur +=1;
                            //     }
                            // }
                            array_push($dataGraph, $data);
                        }else{
                            $data = null;
                        }

                        // CLIMAGE 3
                        $scoreVote = 0;
                        $scoreVote2 = 0;
                        $scoreVote3 = 0;
                        $scoreVote4 = 0;
                        $scoreVote5 = 0;

                        if($climage->type->name != "climage-interlocuteur"){
                            $scoreVote = ($dataVote["1"]+$dataVote["3"]+$dataVote["a"]+$dataVote["b"]) + ($dataVote["1"]+$dataVote["4"]+$dataVote["c"]+$dataVote["d"]) + ($dataVote["2"]+$dataVote["3"]+$dataVote["e"]+$dataVote["f"]) + ($dataVote["2"]+$dataVote["4"]+$dataVote["g"]+$dataVote["h"]) + ($dataVote["5"]+$dataVote["7"]+$dataVote["k"]+$dataVote["m"]) + ($dataVote["5"]+$dataVote["8"]+$dataVote["i"]+$dataVote["j"]) +
                            ($dataVote["6"]+$dataVote["7"]+$dataVote["p"]+$dataVote["q"]) + ($dataVote["6"]+$dataVote["8"]+$dataVote["n"]+$dataVote["o"]);

                            $dataCli["vigilance"] = number_format((($dataVote["1"]+$dataVote["3"]+$dataVote["a"]+$dataVote["b"])/($scoreVote)*100),1);
                            $dataCli["depassement"] = number_format((($dataVote["1"]+$dataVote["4"]+$dataVote["c"]+$dataVote["d"])/($scoreVote)*100),1);
                            $dataCli["implication"] = number_format((($dataVote["2"]+$dataVote["3"]+$dataVote["e"]+$dataVote["f"])/($scoreVote)*100),1);
                            $dataCli["creative"] = number_format((($dataVote["2"]+$dataVote["4"]+$dataVote["g"]+$dataVote["h"])/($scoreVote)*100),1);
                            $dataCli["reconnaissance"] = number_format((($dataVote["5"]+$dataVote["7"]+$dataVote["k"]+$dataVote["m"])/($scoreVote)*100),1);
                            $dataCli["competition"] = number_format((($dataVote["5"]+$dataVote["8"]+$dataVote["i"]+$dataVote["j"])/($scoreVote)*100),1);
                            $dataCli["convivialite"] = number_format((($dataVote["6"]+$dataVote["7"]+$dataVote["p"]+$dataVote["q"])/($scoreVote)*100),1);
                            $dataCli["collaboration"] = number_format((($dataVote["6"]+$dataVote["8"]+$dataVote["n"]+$dataVote["o"])/($scoreVote)*100),1);
                            // CLIMAGE 4
                            
                            // $scoreVote2 = ($dataVote["a"]+$dataVote["b"]+$dataVote["1"]+$dataVote["2"])+($dataVote["c"]+$dataVote["d"]+$dataVote["3"]+$dataVote["4"])+($dataVote["e"]+$dataVote["f"]+$dataVote["5"]+$dataVote["6"])+($dataVote["g"]+$dataVote["h"]+$dataVote["7"]+$dataVote["8"]);
                            // $scoreVote3 = ($dataVote["k"]+$dataVote["m"]+$dataVote["4c"]+$dataVote["4d"])+($dataVote["i"]+$dataVote["j"]+$dataVote["4a"]+$dataVote["4b"])+($dataVote["p"]+$dataVote["q"]+$dataVote["4g"]+$dataVote["4h"])+($dataVote["n"]+$dataVote["o"]+$dataVote["4e"]+$dataVote["4f"]);


                            // $dataCli["achat_utile"] = number_format(((($dataVote["a"]+$dataVote["b"]+$dataVote["1"]+$dataVote["2"])/$scoreVote2)*100));
                            // $dataCli["achat_affaire"] = number_format(((($dataVote["c"]+$dataVote["d"]+$dataVote["3"]+$dataVote["4"])/$scoreVote2)*100));
                            // $dataCli["achat_plaisir"] = number_format(((($dataVote["e"]+$dataVote["f"]+$dataVote["5"]+$dataVote["6"])/$scoreVote2)*100));
                            // $dataCli["achat_impulsion"] = number_format(((($dataVote["g"]+$dataVote["h"]+$dataVote["7"]+$dataVote["8"])/$scoreVote2)*100));
                            // $dataCli["achat_bien"] = number_format(((($dataVote["k"]+$dataVote["m"]+$dataVote["4c"]+$dataVote["4d"])/$scoreVote3)*100));
                            // $dataCli["achat_conquete"] = number_format(((($dataVote["i"]+$dataVote["j"]+$dataVote["4a"]+$dataVote["4b"])/$scoreVote3)*100));
                            // $dataCli["achat_mode"] = number_format(((($dataVote["p"]+$dataVote["q"]+$dataVote["4g"]+$dataVote["4h"])/$scoreVote3)*100));
                            // $dataCli["achat_marque"] = number_format(((($dataVote["n"]+$dataVote["o"]+$dataVote["4e"]+$dataVote["4f"])/$scoreVote3)*100));

                            // $scoreVote4 = ($dataVote["a"]+$dataVote["b"]+$dataVote["1"]+$dataVote["2"])+($dataVote["c"]+$dataVote["d"]+$dataVote["3"]+$dataVote["4"])+($dataVote["2"])+($dataVote["4"]);

                            // $scoreVote5 = ($dataVote["k"]+$dataVote["m"]+$dataVote["4c"]+$dataVote["4d"])+($dataVote["i"]+$dataVote["j"]+$dataVote["4a"]+$dataVote["4b"])+($dataVote["4b"])+($dataVote["4d"]);

                            // $dataCli["achat_utile2"] = number_format(((($dataVote["a"]+$dataVote["b"]+$dataVote["1"]+$dataVote["2"])/$scoreVote4)*100));
                            // $dataCli["achat_affaire2"] = number_format(((($dataVote["c"]+$dataVote["d"]+$dataVote["3"]+$dataVote["4"])/$scoreVote4)*100));
                            // $dataCli["achat_plaisir2"] = number_format(((($dataVote["2"])/$scoreVote4)*100));
                            // $dataCli["achat_impulsion2"] = number_format(((($dataVote["4"])/$scoreVote4)*100));
                            // $dataCli["achat_bien2"] = number_format(((($dataVote["k"]+$dataVote["m"]+$dataVote["4c"]+$dataVote["4d"])/$scoreVote5)*100));
                            // $dataCli["achat_conquete2"] = number_format(((($dataVote["i"]+$dataVote["j"]+$dataVote["4a"]+$dataVote["4b"])/$scoreVote5)*100));
                            // $dataCli["achat_mode2"] = number_format(((($dataVote["4b"])/$scoreVote5)*100));
                            // $dataCli["achat_marque2"] = number_format(((($dataVote["4d"])/$scoreVote5)*100));
                        }

                        $total1 = 0;
                        $total2 = 0;
                        if($climage->type->name == "climage-interlocuteur"){
                            if(isset($dataVote["q1"])||isset($dataVote["q2"])||isset($dataVote["q3"])&&isset($dataVote["q4"])){
                                $serieux = $dataVote["q1"]+$dataVote["q2"];
                                $signeSerieux = $serieux > 0 ? "+" : "-";
                                $absSerieux = abs($dataVote["q1"])+abs($dataVote["q2"]);
                                
                                $enjoue = $dataVote["q3"]+$dataVote["q4"];
                                $signeEnjoue = $enjoue > 0 ? "+" : "-";
                                $absEnjoue = abs($dataVote["q3"])+abs($dataVote["q4"]);

                                $conforme = $dataVote["q1"]+$dataVote["q4"];
                                $signeConforme = $conforme > 0 ? "+" : "-";
                                $absConforme = abs($dataVote["q1"])+abs($dataVote["q4"]);

                                $transgressif = $dataVote["q2"]+$dataVote["q3"];
                                $signeTransgressif = $transgressif > 0 ? "+" : "-";
                                $absTransgressif = abs($dataVote["q2"])+abs($dataVote["q3"]);

                            }
                            if(isset($dataVote["q5"])||isset($dataVote["q6"])||isset($dataVote["q7"])&&isset($dataVote["q8"])){

                                $soi = $dataVote["q5"]+$dataVote["q6"];
                                $signeSoi = $soi > 0 ? "+" : "-";
                                $absSoi = abs($dataVote["q5"])+abs($dataVote["q6"]);

                                $autrui = $dataVote["q7"]+$dataVote["q8"];
                                $signeAutrui = $autrui > 0 ? "+" : "-";
                                $absAutrui = abs($dataVote["q7"])+abs($dataVote["q8"]);

                                $maitrise = $dataVote["q5"]+$dataVote["q7"];
                                $signeMaitrise = $maitrise > 0 ? "+" : "-";
                                $absMaitrise = abs($dataVote["q5"])+abs($dataVote["q7"]);

                                $sympathie = $dataVote["q6"]+$dataVote["q8"];
                                $signeSymphatie = $sympathie > 0 ? "+" : "-";
                                $absSymphatie = abs($dataVote["q6"])+abs($dataVote["q8"]);

                            }

                            $total1 = round(((isset($absSerieux) ? $absSerieux : 0)+(isset($absAutrui) ? $absAutrui : 0)+(isset($absSoi) ? $absSoi : 0)+(isset($absEnjoue) ? $absEnjoue : 0)))*2;

                            $total2 = round(((isset($absTransgressif) ? $absTransgressif : 0)+(isset($absSymphatie) ? $absSymphatie : 0)+(isset($absMaitrise) ? $absMaitrise : 0)+(isset($absConforme) ? $absConforme : 0)))*2;

                            if($total1 > 0){
                                $dataCli["serieux"] = isset($absSerieux) ? (round(($absSerieux / $total1)*100)) : 0;
                                $dataCli["signeSerieux"] = isset($signeSerieux) ? $signeSerieux : "";
                                $dataCli["enjoue"] = isset($absEnjoue) ? (round(($absEnjoue / $total1)*100)):0;
                                $dataCli["signeEnjoue"] = isset($signeEnjoue) ? $signeEnjoue : "";
                                $dataCli["soi"] = isset($absSoi) ? (round(($absSoi / $total1)*100)) : 0;
                                $dataCli["signeSoi"] = isset($signeSoi) ? $signeSoi : "";
                                $dataCli["autrui"] = isset($absAutrui) ? (round(($absAutrui / $total1)*100)) : 0;
                                $dataCli["signeAutrui"] = isset($signeAutrui) ? $signeAutrui : "";

                            }

                            if($total2 > 0){
                                $dataCli["conforme"] = round(($absConforme / $total2)*100);
                                $dataCli["signeConforme"] = isset($signeConforme) ? $signeConforme : "";;
                                $dataCli["transgressif"] = round(($absTransgressif / $total2)*100);
                                $dataCli["signeTransgressif"] = $signeTransgressif;
                                $dataCli["sympathie"] = isset($absSymphatie) ? round(($absSymphatie / $total2)*100) : 0;
                                $dataCli["signeSymphatie"] = isset($signeSymphatie) ? $signeSymphatie : "";
                                $dataCli["maitrise"] = isset($absMaitrise) ? round(($absMaitrise / $total2)*100) : 0;
                                $dataCli["signeMaitrise"] = isset($signeMaitrise) ? $signeMaitrise : "";
                            }
                            $dataCli["total1"] = ($dataCli["serieux"]+$dataCli["enjoue"]+$dataCli["conforme"]+$dataCli["transgressif"]);
                            $dataCli["total2"] = ($dataCli["soi"]+$dataCli["autrui"]+$dataCli["maitrise"]+$dataCli["sympathie"]);

                        }
                        array_push($nbRepondants, $countUsers);
                        array_push($dataClimage,$dataCli);
                        array_push($hArray, $h); 
                    }
                }

                $hArray2 = [];
                foreach ($hArray as $kh => $value) {
                    $hOnly = explode(":",$value);
                    if(!in_array($hOnly[0].":00:00",$hArray2)){
                        array_push($hArray2,$hOnly[0].":00:00");
                    }
                }

                $hArray2 = $this->my_array_unique($hArray2,false);
                if($d!=""){
                    array_push($dateArray, ["date" => $d, "heures"=>$hArray2, "vote"=>$vote, "graphe"=>$dataGraph,"climage"=>$dataClimage, "nbRep"=>$nbRepondants]);
                    array_push($dateArray2, ["date" => $d, "heures"=>$hArray2, "graphe"=>$dataGraph]);
                }
            }
        }

        $dateArray = $this->my_array_unique($dateArray,false);
        $dateArray2 = $this->my_array_unique($dateArray2,false);

        // % q 3(a)

        $pages = Type::where('name', $type->name)->first()->pages;

            $cur = 1;
            foreach ($pages as $key => $value) {
                switch ($cur) {
                    case 1:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/personal";
                        break;
                    case 2:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formTwo";
                        break;
                    case 3:
                        # code...
                        $value->url = "/diagnostics/climage/".$id."/".$code."/formThree";
                        break;

                    default:
                        # code...
                        break;
                }
                $cur +=1;
               
            }

       
        // echo "<pre>";

        // var_dump($question3->reponses);
        // echo "</pre>";
        // die();

        $profil = Type::where('name', $type->name)->first()->pages[0];
        $questionProfil = [];
        foreach ($profil->questions as $key => $question) {

            $questionProfil[$question->id] = [];
            $questionProfil[$question->id]['fr'] = $question->question_fr;
            $questionProfil[$question->id]['en'] = $question->question_en;
        }
        
        // foreach ($climage->testRepondants as $repondant) {
        //     var_dump($repondant->user->email);

        // }
        // die();
            
        if($code == $climage->code ){
           
            return view("climage/front/merci", ["climage" => $climage, "lang" => \App::getLocale(), "pages" => $pages , "data" => !isset($data) ? "" : $data , "page2" => $page2, "questionProfil" =>$questionProfil, "dates"=>$dateArray,"id"=>$id,"dates2"=>$dateArray2]);
        }
        
        return redirect()->route("climage-front");
        
    }

    public function trombinoscope(Request $request){
        $climage = Test::find($request->id);
        $type = Type::find($climage->type_id);
        $reponses = [];
   
        if($request->code == $climage->code){
            
            $pages = Type::where('name', $type->name)->first()->pages;
            $cur = 1;
            foreach ($pages as $key => $value) {
                switch ($cur) {
                    case 1:
                        # code...
                        $value->url = "/diagnostics/climage/".$request->id."/".$request->code."/personal";
                        break;
                    case 2:
                        # code...
                        $value->url = "/diagnostics/climage/".$request->id."/".$request->code."/formTwo";
                        break;
                    case 3:
                        # code...
                        $value->url = "/diagnostics/climage/".$request->id."/".$request->code."/formThree";
                        break;
                    default:
                        # code...
                        break;
                }
                $cur +=1;
               
            }

            $profil = Type::where('name', $type->name)->first()->pages[0];
            $questionProfil = [];
            foreach ($profil->questions as $key => $question) {

                $questionProfil[$question->id] = [];
                $questionProfil[$question->id]['fr'] = $question->question_fr;
                $questionProfil[$question->id]['en'] = $question->question_en;
            }

            return view("climage/front/trombinoscope", ["climage" => $climage, "lang" => \App::getLocale(), "reponses" => $reponses, "pages" => $pages, "questionProfil" =>$questionProfil, ]);
        }

        return redirect()->route("climage-front");
    }

    public function saveUsers(Request $request){

        $this->validate($request, [
          'code' => 'required',
        ]);

        $climages = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%climage%");
        })->get();

        

        foreach ($climages as $key => $climage) {
            
            $test = Test::where(['type_id' => $climage->type_id, 'code' => $request->code])->first();

            if(!empty($test)) {

                if(isset($request->email)){
                    $credentials =[];
                    $repondant = User::where("email",$request->email)->first();

                    if($repondant){

                        if($repondant->code && !empty($repondant->code)){

                            $credentials["email"] = $repondant->email;
                            $credentials["password"] = $repondant->code;
                            $auth = Auth::attempt($credentials);
                        } else {
                            User::where('email', '=' ,$request->email)->update(
                                ['password' => bcrypt($request->code), 'code' => $request->code]
                            );

                            $newUser = User::where("email",$request->email)->where("code",$request->code)->first();
                            $credentials["email"] = $newUser->email;
                            $credentials["password"] = $newUser->code;
                            $auth = Auth::attempt($credentials);

                        }
                        if(!$auth){
                            return redirect()->route("connexion");
                        }
                    } else {
                        
                        $user = new User();
                        $user->username = $request->email;
                        $user->email = $request->email;
                        $user->password = bcrypt($request->code);
                        $user->role = "user";
                        $user->code = $request->code;
                        $user -> save();
                        $credentials["email"] = $request->email;
                        $credentials["password"] = $request->code;
                        $auth = Auth::attempt($credentials);
                    }
                }
                                 
                return redirect()->route('climage-front-personal', ['id' => $test->id, "code" => $request->code]);              
            }
        }

        return redirect('/diagnostics/')->with('error_code', 'Auncune test climage trouvée!');

    }

    function my_array_unique($array, $keep_key_assoc = false){
        $duplicate_keys = array();
        $tmp = array();       
        foreach ($array as $key => $val){
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array)$val;
            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }
        foreach ($duplicate_keys as $key)
            unset($array[$key]);
        return $keep_key_assoc ? $array : array_values($array);
    }

    public function deleteInfoClimage(){
        
        $data = file_get_contents('php://input');
        $id = json_decode($data)->idClimage;
        $user = json_decode($data)->user;

        $deleteInfoUser = ReponseRepondant::where("repondant_id",$user->id)->where("test_id",$id)->delete();
        $deleteInfoUser2 = TestRepondant::where("repondant_id",$user->id)->where("test_id",$id)->delete();
        echo json_encode(array("status" => 1, "message" => "ok"));

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reponse;
use Illuminate\Support\Facades\Input;

class ReponseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           
    }

    public function questions(){

        $data = file_get_contents('php://input');
        $reponse_question = json_decode($data)->reponse;
        $quest = json_decode($data)->quest;

        foreach($reponse_question as $key => $value){
            Reponse::where("question_id", ">=", $quest)->where("score", "=", $value->score)->update(
                ['reponse_fr' => $value->reponse_fr, 'reponse_en' => $value->reponse_en],
                ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
            );

        }

        echo json_encode(array("status" => 1, "message" => "ok"));

    }

    public function question_isma(){
        $data = file_get_contents('php://input');
        $reponse_question = json_decode($data)->reponse;
        foreach ($reponse_question as $key => $value) {
           Reponse::where("id", "=", $value->id)->update(
                ['reponse_fr' => $value->reponse_fr, 'reponse_en' => $value->reponse_en],
                ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
            ); 
        }

        echo json_encode(array("status" => 1, "message" => "ok"));
    }

    public function question(){
        $data = file_get_contents('php://input');
        $reponse_question = json_decode($data)->reponse;
        
        foreach ($reponse_question as $key => $value) {
           Reponse::where("id", "=", $value->id)->update(
                ['reponse_fr' => $value->reponse_fr, 'reponse_en' => $value->reponse_en],
                ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
            ); 
        }

         echo json_encode(array("status" => 1, "message" => "ok"));

         //return redirect()->route('feedback-questionnaire',['active'=>$typeActives]);
    }


    //  public function question(){
    //     if(isset($_POST["simple"])){

    //             foreach ($request->reponse_question as $key => $reponse) {
    //                 //foreach ($reponse as $reponse_id => $value) {
    //                 Reponse::where("id", "=", $key)->update(
    //                     ['reponse_fr' => $reponse["fr"], 'reponse_en' => $reponse["en"]],
    //                     ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
    //                 );
                        
    //                 //}
    //             }

    //             echo json_encode(array("status" => 1, "message" => "ok"));

    //         } else {

    //             foreach($request->reponse_question as $key => $reponse){

    //                 Reponse::where("question_id", ">=", $request->id_quest)->where("score", "=", $reponse["score"])->update(
    //                     ['reponse_fr' => $reponse["fr"], 'reponse_en' => $reponse["en"]],
    //                     ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
    //                 );

    //             }

    //         }
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // $validatedData = $request->validate([
        //     'reponse_fr' => 'required',
        //     'reponse_en' => 'required',
        //     'score' => 'required',
        // ]);

        if($request->active) $typeActives= $request->active;
        else $typeActives='formation';

        if($request->type == "feedback"){

            if(isset($_POST["simple"])){

                foreach ($request->reponse_question as $key => $reponse) {
                    //foreach ($reponse as $reponse_id => $value) {
                    Reponse::where("id", "=", $key)->update(
                        ['reponse_fr' => $reponse["fr"], 'reponse_en' => $reponse["en"]],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );
                        
                    //}
                }

            } else {

                foreach($request->reponse_question as $key => $reponse){

                    Reponse::where("question_id", ">=", $request->id_quest)->where("score", "=", $reponse["score"])->update(
                        ['reponse_fr' => $reponse["fr"], 'reponse_en' => $reponse["en"]],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );

                }

            }

            return redirect()->route('feedback-questionnaire',['active'=>$typeActives]);
        } else {

            $question = Reponse::find($id);

            $question->reponse_fr = $request->reponse_fr;
            $question->reponse_en = $request->reponse_en;
            $question->score = $request->score;
            $question->save();
            $request->session()->flash('message', 'Réponse modifiée avec succès!'); 
            $request->session()->flash('active', $typeActives );

            return redirect()->route('climage-questionnaire',['active'=>$typeActives]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

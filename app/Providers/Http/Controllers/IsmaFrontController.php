<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Test;
use App\Type;
use App\Mail;
use App\ReponseRepondant;
use App\Question;
use App\Page;
use App\TestRepondant;
use App\User;


class IsmaFrontController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  
  // public function saveUser(Request $request){

  //     $validatedData = $request->validate([
  //           'consultant_nom' => 'required',
  //           'consultant_prenom' => 'required'

  //     ]);
  //     $array = array(["consultant","admin"]);
  //     $consultant = User::whereRaw("LOWER(firstname) like (?)", [strtolower($request->consultant_prenom)])->whereRaw("LOWER(lastname) like (?)", [strtolower($request->consultant_nom)])->where(function($q) {
  //         $q->where('role', 'consultant')
  //           ->orWhere('role', 'admin')
  //           ->orWhere('role', 'partenaire');
  //     })->first();

  //   if($consultant){

  //     $count = 0;
  //     $test =  Test::firstOrNew(["name" => "ISMA - ".Auth::user()->firstname." ".Auth::user()->lastname]);
  //     $dates = ReponseRepondant::where("test_id",$test->id)->where("repondant_id",Auth::user()->id)->get();
  //     $datePerMonth = array();
  //     foreach ($dates as $key => $date) {
  //       $d = explode(" ", $date->created_at);
  //       if(!in_array($d[0],$datePerMonth)){
  //         array_push($datePerMonth, $d[0]);
  //       }
  //     }

  //     $month = explode("-", end($datePerMonth));

  //     foreach ($datePerMonth as $key => $yearMonth) {
  //       $year = explode("-",$yearMonth);
  //       if($year[0] == date("Y")){
  //         $count = $count + 1;
  //       }
  //     }

  //     if($count > 9){
  //       if($month[1] == date('m')){
  //         return redirect()->route('isma-front-form', ['idConsultant' => $consultant->id]);
  //       } else {
  //         return redirect('diagnostics')->with('error_max', 'consultant_not_found'); 
  //       }
  //     } else {
  //       return redirect()->route('isma-front-form', ['idConsultant' => $consultant->id]);
  //     }
  //   }else{
  //     return redirect('diagnostics')->with('error_consultant', 'consultant_not_found');
  //   }

  // }

   public function saveUser(Request $request){

      $validatedData = $request->validate([
            'consultant_nom' => 'required',
            'consultant_prenom' => 'required'

      ]);
      $array = array(["consultant","admin"]);
      $consultant = User::whereRaw("LOWER(firstname) like (?)", [strtolower($request->consultant_prenom)])->whereRaw("LOWER(lastname) like (?)", [strtolower($request->consultant_nom)])->where(function($q) {
          $q->where('role', 'consultant')
            ->orWhere('role', 'admin')
            ->orWhere('role', 'partenaire');
      })->first();

    if($consultant){
        return redirect()->route('isma-front-form', ['idConsultant' => $consultant->id]);
    }else{
        return redirect('diagnostics')->with('error_consultant', 'consultant_not_found');
    }
  }

  public function questionnaire($idConsultant){
      $type = Type::where("name", "isma")->first();
      $pages = Page::where("type_id", $type->id)->with("questions", "questions.reponses")->get();

      //if(Auth::user()->role=='consultant' || Auth::user()->role=='admin'){
        return view("isma/front/questionnaire", [ "pages" => $pages, "idConsultant" => $idConsultant, "lang" => \App::getLocale()]);
      //} else {
        //return redirect('diagnostics')->with('error_role','error');
      //}
  }

  public function save(Request $request){


    //save to test_repondant
    $type = Type::where("name", "isma")->first();

    $test =  Test::firstOrNew(["name" => "ISMA - ".Auth::user()->firstname." ".Auth::user()->lastname]);
   
    $test->type_id = $type->id;
    $test->langue = \App::getLocale();
    $test->consultant_id = $request->idConsultant;
    $test->save();

    $testRep = new TestRepondant;
    $testRep->test_id = $test->id;
    $testRep->reponse = 1;
    $testRep->repondant_id = Auth::user()->id;
    $testRep->save();


    //save reponse repondant
    // var_dump(Auth::user()->id);
    // die();
    $repondant_id = Auth::user()->id;

      foreach ($request->reponse_question as $key => $value) {
        $countRep = ReponseRepondant::where("repondant_id",Auth::user()->id)->where("question_id",$key)->where("test_id",$test->id)->count();

        if($countRep == 0){
          $reponseRep2 = new ReponseRepondant; 
          $reponseRep2->repondant_id = Auth::user()->id;
          $reponseRep2->question_id = $key;
          $reponseRep2->test_id = $test->id;
          $reponseRep2->reponse_id = $value;
          $reponseRep2->save();
        } else {
          date_default_timezone_set('Europe/Paris');
          $reponseRep = ReponseRepondant::where("repondant_id",Auth::user()->id)->where("question_id",$key)->where("test_id",$test->id)->orderBy('created_at', 'desc')->first();

            $rep_date = explode(" ",$reponseRep->created_at);

            $date1 = $rep_date[0];
            $date2 = date("Y-m-d");

            $nouvdate1 = explode("-",$date1);
            $nouvdate2 = explode("-",$date2);
            
            if($nouvdate2[0] == $nouvdate1[0] && $nouvdate2[1] == $nouvdate1[1]){
              $reponseRep2 = ReponseRepondant::find($reponseRep->id); 
            } else {
              $reponseRep2 = new ReponseRepondant;
            }

            $diff = abs(strtotime($date2) - strtotime($date1));

            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            //$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

            // if((int)$years == 0 && (int)$months == 0){
            //   $reponseRep2 = ReponseRepondant::find($reponseRep->id);         
            // } else {
            //   $reponseRep2 = new ReponseRepondant;
            // }
            
            $reponseRep2->repondant_id = Auth::user()->id;
            $reponseRep2->question_id = $key;
            $reponseRep2->test_id = $test->id;
            $reponseRep2->reponse_id = $value;
            $reponseRep2->save();

        }      

        // if(!$reponseRep){
        //   $reponseRep = new ReponseRepondant;
        //   $reponseRep->repondant_id = Auth::user()->id;
        //   $reponseRep->question_id = $key;
        // }
        // $reponseRep->test_id = $test->id;
        // $reponseRep->reponse_id = $value;
        // $reponseRep->save();;

      }
    //}

    //redirect to home
    $term = \App::getLocale() == "fr" ? "Nous vous remercions pour votre confiance. Votre consultant vous contacte rapidement pour vous communiquer les résultats" : "Well done! Thank you for taking time to complete this questionnaire.
    Your feedback report is being processed and your coach will contact you to debrief it to you";
    return redirect()->route('diagnostics')->with('success', $term);

  }



}

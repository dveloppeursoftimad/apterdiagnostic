<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail;
use App\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
class MailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $mails_fr = Mail::where("type", "consultant" )->where("active", "=", 1)->where("language", "fr" )->get();
        $mails_en = Mail::where("type", "consultant" )->where("active", "=", 1)->where("language", "en" )->get();
        $types = Type::all();
        return view('mail/mails',['mails_fr'=>$mails_fr,'mails_en'=>$mails_en, 'types'=>$types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        // $validatedData = $request->validate([
        //     'name' => 'required',       
        //     'titre' => 'required'
        // ]);


        
        $mails = Mail::where("type", $request->type)->where("type_id", $request->type_id)->get();
        // var_dump(count($mails));
        foreach ($mails as $key => $value) {
           $mail = Mail::find($value->id);
           $mail->active = 0;
           $mail->save();
        }
        // die();

        $mail = new Mail;
        $mail->type = $request->type;
        $mail->type_id = $request->type_id;
        $mail->mail = $request->mail;
        $mail->subject = $request->titre;
        $mail->language = $request->langue;
        $mail->active = 1;
        $mail->save();
        return redirect()->route('feedback-mail');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
      $mail = Mail::find($id);
      $mails = Mail::where("type", $mail->type)->where("language", $mail->language)->get();
      return view('mail/edit',['mail'=>$mail, 'mails'=>$mails]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request){
        
      $mail = Mail::find($request->id);

      if(!$request->template){
        
        
        // set other inactive
        $mails = Mail::where("type", $mail->type)->where("type_id", $mail->type_id)->where("language", $mail->language)->get();
        foreach ($mails as $key => $m) {
            if($m->id != $mail->id){
                $m->active = 0;
                $m->save();
            }

        }
        $mailNew = Mail::find($request->id);
        $mailNew->subject = $request->subject;
        $mailNew->mail = $request->mail;
        $mailNew->active = 1;
        $mailNew->consultant_id = Auth::user()->id;
        // var_dump( $request->mail);
        // var_dump($mailNew->mail);
        

        $mailNew->save();
      }else{
        $mails = Mail::where("type", $mail->type)->where("type_id", $mail->type_id)->where("language", $mail->language)->get();
        foreach ($mails as $key => $m) {   
                $m->active = 0;
                $m->save();
        }
        $mailNew = new Mail;
        $mailNew->subject = $request->subject;
        $mailNew->mail = $request->mail;
        $mailNew->active = 1;
        $mailNew->type = $mail->type;
        $mailNew->type_id = $mail->type_id;
        $mailNew->language = $mail->language;
        $mailNew->consultant_id = Auth::user()->id;
        $mailNew->save();

    }
        
    $count = Type::where('name', 'like', "%feedback%")->where("id","=",$mail->type_id)->count();
    $count1 = Type::where('name', 'like', "%acti%")->where("id","=",$mail->type_id)->count();
    
    if($count == 1){
        return redirect()->route('feedback-mail');
    } 
    if($count1 == 1){
        return redirect()->route('acti-mail');
    }

    return redirect('consultant');

      // if($mail->type_id){
      //   return redirect()->route('feedback-mail');
      // }else{
      //   return redirect()->route('mails');
      // }

      
    }

    public function modification(Request $request){
        $mailBeneficiaires = DB::table('mail')->where(['type'=> "benficiaire",'type_id'=> $request->typeId,'language'=>$request->langue]);
        if(count($mailBeneficiaires->get()) == 0){
            $mailData =[
                'type_id' => $request->typeId,
                'language' =>$request->langue,
                'type'=> "benficiaire",
                'mail' => $request->beneficiaireFR[$request->typeId]
            ];
            $mail = new Mail($mailData);
            $mail->save();
        }elseif (count($mailBeneficiaires->get()) == 1) {
            # code...
            $mail = Mail::find($mailBeneficiaires->get()[0]->id);
            $mail->type_id = $request->typeId;
            $mail->language = $request->langue;
            $mail->mail = $request->beneficiaireFR[$request->typeId];
            $mail->save();
        }
        $mailRepondant = DB::table('mail')->where(['type'=> "repondant",'type_id'=> $request->typeId,'language'=>$request->langue]);
        if(count($mailRepondant->get()) == 0){
            $mailData =[
                'type_id' => $request->typeId,
                'language' =>$request->langue,
                'type'=> "repondant",
                'mail' => $request->beneficiaireFR[$request->typeId]
            ];
            $mail = new Mail($mailData);
            $mail->save();
        }elseif (count($mailRepondant->get()) == 1) {
            # code...
            $mail = Mail::find($mailRepondant->get()[0]->id);
            $mail->type_id = $request->typeId;
            $mail->language = $request->langue;
            $mail->mail = $request->beneficiaireFR[$request->typeId];
            $mail->save();
        }
        $mailRelance = DB::table('mail')->where(['type'=> "relance",'type_id'=> $request->typeId,'language'=>$request->langue]);
        if(count($mailRelance->get()) == 0){
            $mailData =[
                'type_id' => $request->typeId,
                'language' =>$request->langue,
                'type'=> "relance",
                'mail' => $request->beneficiaireFR[$request->typeId]
            ];
            $mail = new Mail($mailData);
            $mail->save();
        }elseif (count($mailRelance->get()) == 1) {
            # code...
            $mail = Mail::find($mailRelance->get()[0]->id);
            $mail->type_id = $request->typeId;
            $mail->language = $request->langue;
            $mail->mail = $request->beneficiaireFR[$request->typeId];
            $mail->save();
        }
        return redirect ('mail');
        
    }
    public function modificationPartielle(Request $request)
    {
        # code...
       /* var_dump($request->typeId);
        var_dump($request->type[$request->typeId]);
        var_dump($request->langue);
        var_dump($request->beneficiaireInvitationFR[$request->typeId]);
        */

        /*switch (explode(" ",$request->type[$request->typeId])[0]) {
            case 'invitation':
                # code...
                if($request->langue=='fr'){
                    $subject = "Mail d'invitation";
                }
                if($request->langue=='en'){
                    $subject = "Invitation mail";
                }
                break;
            case 'remerciement':
                # code...
                if($request->langue=='fr'){
                    $subject = "Mail de remerciement";
                }
                if($request->langue=='en'){
                    $subject = "Thanks mail";
                }
                break;
            case 'relance':
                # code...
                if($request->langue=='fr'){
                    $subject = "Mail d'invitation";
                }
                if($request->langue=='en'){
                    $subject = "Invitation mail";
                }
                break;
            default:
               
                break;
        }*/
        $mailData = [
            'type_id'=>$request->typeId,
            'type'=>$request->type[$request->typeId],
            'language'=>$request->langue,
            'subject'=>$request->subject,
            'mail'=>$request->beneficiaireInvitationFR[$request->typeId],
            'active'=> true
        ];
        $mail = DB:: table('mail')->where(['type'=>$request->type[$request->typeId],'mail'=>$request->beneficiaireInvitationFR[$request->typeId],'language'=>$request->langue]);
        
        /*var_dump($request->type[$request->typeId]);
        var_dump($mailData);
        var_dump(count($mail->get()));
        die();*/
        
        if(count($mail->get()) == 0){
            $newmail = new Mail($mailData);
            $newmail->save();
            $newmail = DB::table('mail')->where($mailData)->get()[0];
            
        }
        elseif (count($mail->get())==1) {
            # code...
            $newmail = Mail::find($mail->get()[0]->id);
            $newmail->subject = $request->subject;
            $newmail->active = true;
            $newmail->save();
        }
        

        $mails = DB::table('mail')->where(['type'=>$request->type[$request->typeId],'language'=>$request->langue]);
        for($i=0;$i<count($mails->get());$i++){
            if(isset($newmail)){
                if($mails->get()[$i]->id != $newmail->id){
                    $mail = Mail::find($mails->get()[$i]->id);
                    $mail->active = false;
                    $mail->save();
                }
            }
            else{
                if($mails->get()[$i]->id != $mail->get()[0]->id){
                    $mail = Mail::find($mails->get()[$i]->id);
                    $mail->active = false;
                    $mail->save();
                }
            }    
        }
        return redirect('mail');
    }
    public function updateTemplate(Request $request)
    {
        # code...
        if(isset($request->idMail)){
            $mail = Mail::find($request->idMail);
            $mail->mail= $request->beneficiaireInvitationFR[$request->typeId];
            $mail->subject= $request->subject;
            $mail->save();
        }
        return redirect('mail');
    }
    public function getMail()
     {
         # code...
        $mails = DB::table('mail')->get(); 
        echo json_encode($mails);
     }

    public function getType()
     {
         # code...
        $types = DB::table('type')->get(); 
        echo json_encode($types);
     }
    public function saveSubject(Request $request){
        $mail = mail::find($request->idMail);
        $mail->active = 1;
        $mail->subject = $request->subjectConsultantFR;
        $mail->mail= $request->bodyConsultantFR;
        $mail->save();
        $mails = Mail::all();
        foreach ($mails as $key => $ml) {
            if($ml->id != $request->idMail && $ml->type =='consultant' && $ml->language ==$request->language){
                $toUpdate =Mail::find($ml->id);
                $toUpdate->active =0;
                $toUpdate->save();
            }
        }
        return redirect('mail');
    }
    public function addSubject(Request $request){

        $mail = Mail::updateOrCreate(
            ['mail' => $request->bodyConsultantFR,'type'=>'consultant'],
            ['subject' =>$request->subjectConsultantFR,'active'=>1,'language'=>$request->language]
        );
        
        $id = mail::where('mail','=',$mail->mail)->get()[0]->id;

        $mails= Mail::all();
        foreach ($mails as $key => $ml) {
            if($ml->id != $id && $ml->type =='consultant' && $ml->language ==$request->language){
                $toUpdate =Mail::find($ml->id);
                $toUpdate->active =0;
                $toUpdate->save();
            }
        }
        return redirect('mail');       
    }
}

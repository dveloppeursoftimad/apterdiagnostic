<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Page;
use App\Reponse;
use App\Test;
use App\Type;
use App\ReponseRepondant;
class QuestionController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function pages(){

        $data = file_get_contents('php://input');
        $sp = json_decode($data)->pages;

        $page = Page::find($sp->id);
        $page->title_fr = $sp->title_fr;
        $page->title_en = $sp->title_en;
        $page->title_en_ben = $sp->title_en_ben;
        $page->title_fr_ben = $sp->title_fr_ben;

        $page->save();

        echo json_encode(array("status" => 1, "message" => "ok"));       

    }

    public function souspages(){

        $data = file_get_contents('php://input');
        $sp = json_decode($data)->souspages;

        $page = Page::find($sp->id);
        $page->title_fr = $sp->title_fr;
        $page->title_en = $sp->title_en;
        $page->title_en_ben = $sp->title_en_ben;
        $page->title_fr_ben = $sp->title_fr_ben;

        $page->save();

        echo json_encode(array("status" => 1, "message" => "ok"));       

    }

    public function question_isma(){

        $data = file_get_contents('php://input');
        $reponse_question = json_decode($data)->questions;

        $question = Question::find($reponse_question->id);
        $question->question_fr = $reponse_question->question_fr;
        $question->question_en = $reponse_question->question_en;
        $question->save();

        echo json_encode(array("status" => 1, "message" => "ok"));

    }

    public function supp_question(){

        $data = file_get_contents('php://input');
        $question = json_decode($data)->questions;        
        $fb = Type::where('name',"LIKE", "%feedback%")->first();
        if($question->type_id == $fb->id){
            echo json_encode(array("status" => 0, "message" => "error"));
        }else{
            $test_sp = Test::where("template_id",$question->type_id)->count();
            if($test_sp == 0){
                $questionReponsesRep = ReponseRepondant::where("question_id",$question->id)->delete();
                $questionReponses = Reponse::where("question_id",$question->id)->delete();
                $questionQuest = Question::where("id",$question->id)->delete();
                echo json_encode(array("status" => 1, "message" => "ok"));
            } else {
                echo json_encode(array("status" => 0, "message" => "error"));
            }
        }
        // $typeQuestion = Test::where("template_id",$question->type_id)->first();

        // if(!is_null($typeQuestion)){

        //     $test_sp = Test::where("template_id",$question->type_id)->count();

        //     if($test_sp == 0){
        //         $questionReponsesRep = ReponseRepondant::where("question_id",$question->id)->delete();
        //         $questionReponses = Reponse::where("question_id",$question->id)->delete();
        //         $questionQuest = Question::where("id",$question->id)->delete();
        //         echo json_encode(array("status" => 1, "message" => "ok"));
        //     } else {
        //         echo json_encode(array("status" => 0, "message" => "error"));
        //     }
        // } else {
        //     echo json_encode(array("status" => 0, "message" => "error"));
        // }
    }

    public function suppSouspages(){

        $data = file_get_contents('php://input');
        $sp = json_decode($data)->souspages;    

        $fb = Type::where('name',"LIKE", "%feedback%")->first();
        if($sp->type_id == $fb->id){
            echo json_encode(array("status" => 0, "message" => "error"));
        }else{

            $test_sp = Test::where("template_id",$sp->type_id)->count();

            if($test_sp == 0){
                
                $questSps = Question::where("page_id",$sp->id)->get();
                foreach ($questSps as $key => $value) {
                    $delReponseRep = ReponseRepondant::where("question_id",$value->question_id)->delete();
                    $delReponse = Reponse::where("question_id",$value->id)->delete();
                }
                $delQuestSp = Question::where("page_id",$sp->id)->delete();
                //Page::where('id','=',$sp->id)->where("page_id","=",$sp->page_id)->delete();
                $delPage = Page::where("id",$sp->id)->delete();
                
                echo json_encode(array("status" => 1, "message" => "ok"));
            } else {
                echo json_encode(array("status" => 0, "message" => "error"));
            }
        }
        // $typeSp = Test::where("template_id",$sp->type_id)->first();

        // if(!is_null($typeSp)){
        //     $test_sp = Test::where("template_id",$sp->type_id)->count();
        //     if($test_sp == 0){
        //         $questSps = Question::where("page_id",$sp->id)->get();
        //         foreach ($questSps as $key => $value) {
        //             $delReponseRep = ReponseRepondant::where("question_id",$value->question_id)->delete();
        //             $delReponse = Reponse::where("question_id",$value->question_id)->delete();
        //         }
        //         $delQuestSp = Question::where("page_id",$sp->id);
        //         $delSP = Page::find($sp->id);
        //         echo json_encode(array("status" => 1, "message" => "ok"));
        //     } else {
        //         echo json_encode(array("status" => 0, "message" => "error"));
        //     }
        // } else {
        //     echo json_encode(array("status" => 0, "message" => "error"));
        // }

    }

    public function question(){

        $data = file_get_contents('php://input');
        $reponse_question = json_decode($data)->questions;
        $check = json_decode($data)->check;

        $question = Question::find($reponse_question->id);
        $question->question_fr = $reponse_question->question_fr;
        $question->question_en = $reponse_question->question_en;
        $question->question_fr_ben = $reponse_question->question_fr_ben;
        $question->question_en_ben = $reponse_question->question_en_ben;
        if(!is_null($check)){
            $question->negative = $check == true ? 1 : 0;
        }
        $question->save();
        $count = Reponse::where("question_id", "=", $reponse_question->id)->count();
        if($question->type == "radio"){
            $reponses = Reponse::where("question_id", "=", $reponse_question->id)->get()[0];

            if(!is_null($check)){
                if($check == true){
                    for ($i=0; $i < $count-1; $i++) { 
                        Reponse::where("question_id", "=", $reponse_question->id)->where("id", "=", ($reponses->id+$i))->update(
                            ['score' => ($count-1)-$i],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    }
                } else {
                    for ($i=0; $i < $count-1; $i++) { 
                        Reponse::where("question_id", "=", $reponse_question->id)->where("id", "=", ($reponses->id+$i))->update(
                            ['score' => $i+1],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    }
                }
            }
        }
            

        echo json_encode(array("status" => 1, "message" => "ok"));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'question_fr' => 'required',
            'question_en' => 'required',

        ]);



            if($request->active) $typeActives= $request->active;
            else $typeActives='formation';
            $question = Question::find($id);

            $question->question_fr = $request->question_fr;
            $question->question_en = $request->question_en;
            $question->save(); 
            $request->session()->flash('message', 'Question modifiée avec succès!'); 
            $request->session()->flash('active', $typeActives ); 
            return redirect()->route('climage-questionnaire');    

        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

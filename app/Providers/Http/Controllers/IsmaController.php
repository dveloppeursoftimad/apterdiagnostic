<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Test;
use App\Type;
use App\Mail;
use App\ReponseRepondant;
use App\Reponse;
use App\Question;
use App\TestRepondant;
use App\Category;
use App\Category_question;
use App\User;
use App\Groupe;
use App\Page;

use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

use Amenadiel\JpGraph\Graph;
use Amenadiel\JpGraph\Plot;

use PhpOffice\PhpWord\Shared\ZipArchive;

class IsmaController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {

    // $wp_users = \DB::connection('mysql2')->select("select max(id_coache) as ua, `civilite`, `nom`, `prenom`, `age`, `pays`, `phone`, max(date_add), `id_consultant`, `link_generate`, `statut`, `email`, `lang`, `organisation` from wp_surveywp_coache group by nom, prenom order by max(id_coache) desc");

    // //var_dump($wp_users);
    // //die;

    // foreach ($wp_users as $key => $value) {
    //   var_dump($value->ua);
    // }
    // die;

    // $newUsers0 = array();
    // $newUsers = array();
    // foreach ($wp_users as $key => $wp_user) {
    //   $checkons = \DB::connection('mysql2')->table('wp_surveywp_consultant')->where('nom', $wp_user->nom)->where('prenom', $wp_user->prenom)->count();
    //   if($checkons == 0){
    //     array_push($newUsers0,$wp_user);
    //   }
    // }  
    // die;

    if(Auth::user()->role == "consultant" || Auth::user()->role == "partenaire" || Auth::user()->role == "admin_rech_acad"){

        // $tests = Test::with("consultant", "users")->whereHas('type', function ($query) {
        //   $query->where('name', 'like', "%isma%");
        // })->where("consultant_id", "=", Auth::user()->id)->get();   

        $tests = Test::whereHas('type', function ($query) {
          $query->where('name', 'like', "%isma%");
        })->where("consultant_id", "=", Auth::user()->id)->orderBy("created_at","desc")->get(); 

    }

    else if(Auth::user()->role == "admin" || Auth::user()->role == "admin_acad"){
        $tests = Test::whereHas('type', function ($query) {
          $query->where('name', 'like', "%isma%");
        })->orderBy("created_at","desc")->get();          
    }

    $arrayIsma = array();
    foreach ($tests as $key => $test) {
      if(is_null($test->description)){
        array_push($arrayIsma, $test);
      }
    }

    return view('isma/index', ["tests" =>  $arrayIsma]);
  }

  public function rapportIsma($id){
    $isma = Type::where('name', "isma")->first();

    $test = Test::find($id);
    $data_users = [];
    $repondant = $test->users[0];

    $score_user = [];
    $scores = ReponseRepondant::where("test_id","=",$id)->where("repondant_id","=",$repondant->id)->get();
    foreach ($scores as $k => $score) {
      // var_dump($score->reponse_id);
      $repUser = Reponse::where("id",$score->reponse_id)->first();
      $score_user[$repUser["question_id"]] = $repUser["score"];
    }
    array_push($data_users,["nom"=>$repondant->firstname." ".$repondant->lastname, "score"=>$score_user]);
    return view('isma/resultat', ["isma" => $isma, "data_users" => $data_users]);
  }

  public function liste_rapport_isma($id){
    $test = Test::find($id);
    $repondant = $test->users[0];  

    $testUsers = Test::where("name","like","%"."ISMA - ".$repondant->firstname." ".$repondant->lastname."%")->orWhere("name","like","%".$repondant->lastname." ".$repondant->firstname."%")->get();

    foreach ($testUsers as $key => $value) {
      if($value->id !== (int)$id){

        $datesUsers = ReponseRepondant::where("test_id",$value->id)->where("repondant_id",$repondant->id)->get();

      }
    }

    $consultant = $test->consultant;
    $dates = ReponseRepondant::where("test_id",$id)->where("repondant_id",$repondant->id)->get();
    $datePerMonth = array();

    if(isset($datesUsers)){
      foreach ($datesUsers as $key => $date) {
        $d = explode(" ", $date->created_at);
        if(!in_array($d[0],$datePerMonth)){
          array_push($datePerMonth, $d[0]);
        }
      }
    }
    
    foreach ($dates as $key => $date) {
      $d = explode(" ", $date->created_at);
      if(!in_array($d[0],$datePerMonth)){
        array_push($datePerMonth, $d[0]);
      }
    }

    krsort($datePerMonth);
    return view('isma/liste_rapport_isma',["listes"=>$datePerMonth, "id"=>$id,"nom"=>$test->name]);

  }

  public function rapport_court_en($id,$date){

    $test = Test::find($id);
    $repondant = $test->users[0];
    $consultant = $test->consultant;
    $txtRa = "Rapport-Inventaire-".str_replace(" ","-",$repondant->firstname);

    $serieuxTotal = 0;
    $enjouTotal = 0;
    $conformTotal = 0;
    $transagressiTotal = 0;

    $autruiTotal = 0;
    $soiTotal = 0;
    $maitriseTotal = 0;
    $sympathieTotal = 0;

    ////// GET ALL CATEGORIES ////////
    $cats = Category::all();

    ////// GET ALL QUESTIONS POUR CHAQUE CATEGORIE ///////////
    $data_quest = [];
    foreach ($cats as $key => $cat) {
      if($cat->name == "serieux"){
        $data_quest["serieux"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "enjoue"){
        $data_quest["enjoue"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "transgressif"){
        $data_quest["transgressif"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "conforme"){
        $data_quest["conforme"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "autrui"){
        $data_quest["autrui"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "soi"){
        $data_quest["soi"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "maitrise"){
        $data_quest["maitrise"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else {
        $data_quest["sympathie"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      }
    }

    // SCORE SERIEUX
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $serieuxTotal = $serieuxTotal + $score->score;
    }

    if($serieuxTotal >= 0)
      $serieuxTotal = ((($serieuxTotal - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
    
    // SCORE ENJOUE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $enjouTotal = $enjouTotal + $score->score;
    }
    if($enjouTotal >= 0)
      $enjouTotal = ((($enjouTotal - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

    // SCORE CONFORME
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $conformTotal = $conformTotal + $score->score;
    }
    if($conformTotal >= 0)
      $conformTotal = ((($conformTotal - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

    // SCORE TRANSGRESSIF
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $transagressiTotal = $transagressiTotal + $score->score;
    }
    if($transagressiTotal >= 0)
      $transagressiTotal = ((($transagressiTotal - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

    // SCORE AUTRUI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $autruiTotal = $autruiTotal + $score->score;
    }
    if($autruiTotal >= 0){
      $autruiTotal = ($autruiTotal / 2);
      $autruiTotal = ((($autruiTotal - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
    }

    // SCORE SOI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $soiTotal = $soiTotal + $score->score;
    }
    if($soiTotal >= 0){
      $soiTotal = ($soiTotal / 2);
      $soiTotal = ((($soiTotal - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
    }

    // SCORE MAITRISE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $maitriseTotal = $maitriseTotal + $score->score;
    }
    if($maitriseTotal >= 0){
      $maitriseTotal = ($maitriseTotal / 2);
      $maitriseTotal = ((($maitriseTotal - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
    }

    // SCORE SYMPHATIE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $sympathieTotal = $sympathieTotal + $score->score;
    }
    if($sympathieTotal >= 0){
      $sympathieTotal = ($sympathieTotal / 2);
      $sympathieTotal = ((($sympathieTotal - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
    }

    $arrayGraph1DrawY = array(number_format($serieuxTotal),
      number_format($enjouTotal),
      number_format($conformTotal),
      number_format($transagressiTotal),
      number_format($maitriseTotal),
      number_format($sympathieTotal),
      number_format($autruiTotal),
      number_format($soiTotal)
    );

    $pdf = new Fpdi();
    $path = base_path().'/public/isma/';
    $pageCount = $pdf->setSourceFile($path.'templates/isma_rapport_court_en.pdf');

    ///////////////////////// PAGE 1 ///////////////////////////////////////////////////////


    $pdf->addPage();
    $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->setFillColor(255,255,255);
    $pdf->SetXY(50,180);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Report to"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,180);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $repondant->firstname." ".$repondant->lastname),"0",0,'L',0);
    $pdf->SetXY(50,190);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", $repondant->organisation),"0",0,'C',0);
    $pdf->SetFont('RobotoCondensed-Italic','',17);
    $pdf->SetXY(50,200);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", date_format($test->created_at,"d/m/Y")),"0",0,'C',0);
    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->SetXY(50,215);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Debriefed by"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,215);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $consultant->firstname." ".$consultant->lastname),"0",0,'L',0);

    ///////////////////////// END PAGE 1 /////////////////////////////////////////////////////// 

    ///////////////////////// PAGE 2 À 7 ///////////////////////////////////////////////////////
    
    
    for ($i=2; $i < 8; $i++) { 
      $pdf->addPage();
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }


    ///////////////////////// END PAGE 2 À 7 ///////////////////////////////////////////////////////

    ///////////////////////// PAGE 8 ///////////////////////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(8, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);


  $EnjoueSerieux = $arrayGraph1DrawY[0] - $arrayGraph1DrawY[1];
    $ConformeTransgressif = $arrayGraph1DrawY[2] - $arrayGraph1DrawY[3];
    $MaitriseSympathie = $arrayGraph1DrawY[4] - $arrayGraph1DrawY[5];
    $SoiAutrui = $arrayGraph1DrawY[7] - $arrayGraph1DrawY[6];

    $valEnjoueSerieux = $EnjoueSerieux;
    if($valEnjoueSerieux < 0)
      $valEnjoueSerieux = $valEnjoueSerieux * (-1);
    $valConformeTransgressif = $ConformeTransgressif;
    if($valConformeTransgressif < 0)
      $valConformeTransgressif = $valConformeTransgressif * (-1);
    $valMaitriseSympathie = $MaitriseSympathie;
    if($valMaitriseSympathie < 0)
      $valMaitriseSympathie = $valMaitriseSympathie * (-1);
    $valSoiAutrui = $SoiAutrui;
    if($valSoiAutrui < 0)
      $valSoiAutrui = $valSoiAutrui * (-1);
    

    $arrayGraphe2 = array($valEnjoueSerieux,$valConformeTransgressif,$valMaitriseSympathie,$valSoiAutrui);
    $maxValYGraphe2 = max($arrayGraphe2);

  $pdf->SetFont('RobotoCondensed-Regular','',10);
    $BGraph2Y = 40;
    $BGraph2YC = 38;
    $BGraph2YCT = 45;
    $inc = 5;
    if($maxValYGraphe2 >= 161 ){
      $BGraph2YCT = 255;
      $inc = 25;
    }
    else if ($maxValYGraphe2 >= 121 and $maxValYGraphe2 <= 160 ){
      $BGraph2YCT = 180;
      $inc = 20; 
    }
    else if ($maxValYGraphe2 > 80 and $maxValYGraphe2 <= 120){
      $BGraph2YCT = 135;
      $inc = 15;
      
    }
    else if ($maxValYGraphe2 > 40 and $maxValYGraphe2 <= 80){
      $BGraph2YCT = 90;
      $inc = 10; 
    }
    else{
      $BGraph2YCT = 45;
      $inc = 5;
    }
    
    for($IBGraph2X = 0; $IBGraph2X <= 18 ; $IBGraph2X++){
      $pdf->setFillColor(230,230,230);
      $pdf->Rect(29, $BGraph2Y, 161, 0, 'F');
      $pdf->setFillColor(0,0,0);
      $pdf->Rect(27, $BGraph2Y, 2, 0, 'F');
      $pdf->setXY(21,$BGraph2YC);
      $pdf->Cell(6,4,$BGraph2YCT,"0",0,'C',0);
      if($IBGraph2X < 9)
        $BGraph2YCT = $BGraph2YCT - $inc;
      else
        $BGraph2YCT = $BGraph2YCT + $inc;
      $BGraph2Y = $BGraph2Y + 10; 
      $BGraph2YC = $BGraph2YC + 10;
    }

    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(29,40);
    $pdf->Cell(0,180,"","L",0,'C',0);
 
    $arrayGraph2T = array('Serious','Playful','Compliance','Rebellious','Mastery','Sympathy','Self','Other');
    $arrayColor = array(  '1' => array(255,192,0),
                '0' => array(0,153,204),
                '2' => array(255,127,71),
                '3' => array(0,157,137),
                '5' => array(115,185,213),
                '4' => array(178,1,95),
                '7' => array(0,204,102),
                '6' => array(255,0,100)
              );
    $Graph2TXT = 29;
    $iGraph2TA = 0;
    for($iGraph2T = 0 ; $iGraph2T < 4; $iGraph2T++){

       
      $pdf->setXY($Graph2TXT,41);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]); 
      $pdf->setXY($Graph2TXT + 5,50);
      if($iGraph2TA == 2 || $iGraph2TA == 4)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1; 
      $pdf->setXY($Graph2TXT,211);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]);
      $pdf->setXY($Graph2TXT + 5,130);
      if($iGraph2TA == 3 || $iGraph2TA == 5)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1;
      $Graph2TXT = $Graph2TXT + 40;

       
    }
   

  //motifConform
  $yGi2M = 49.86;
  for ($Gi2M=0; $Gi2M < 80 ; $Gi2M++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 19; $Gi2MY++){
      $pdf->Image($path.'img/motifConform.png',$xGi2M, $yGi2M,1.6); 
      $xGi2M += 1.6;
    } 
    $yGi2M += 1.002;
  }

  //motifMaitrise 
  $xGi2T = 49.86;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {  
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 20; $Gi2MY++){
      $pdf->Image($path.'img/motifMaitrise.png',$xGi2M, $xGi2T,1.1); 
      $xGi2M += 1.55;
    } 
    $xGi2T += 1.97;
  } 
 
  //MotifTransgressif 
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 18; $Gi2MY++){
      $pdf->Image($path.'img/MotifTransgressif.png',$xGi2M, $xGi2T,1.7); 
      $xGi2M += 1.7;
    }  
    $xGi2T += 1.956;
  } 
  //motifSympathie
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 80 ; $Gi2T++) { 
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 15; $Gi2MY++){
      $pdf->Image($path.'img/motifSympathie.png',$xGi2M,$xGi2T,2.1);
      $xGi2M += 2.03;
    } 
    $xGi2T += 1;
  } 
 

 

  $pdf->SetFont('RobotoCondensed-Bold','',14);
  $pdf->setFillColor(255,255,255);

  if($EnjoueSerieux > 0){
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ;
    $pdf->setXY(37,130 - $yEnjoueSerieux);
  }
  else{

    $pdf->setXY(37,130); 
    $EnjoueSerieux = $EnjoueSerieux * -1;
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ; 
  }
  if($EnjoueSerieux > 0)
    $pdf->Cell(24,$yEnjoueSerieux,$EnjoueSerieux,"0",0,'C',1);


  if($ConformeTransgressif > 0){
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ;
    $pdf->setXY(77,130 - $yConformeTransgressif); 
  }
  else{
    $pdf->setXY(77,130); 
    $ConformeTransgressif = $ConformeTransgressif * -1;
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ; 
  }
  if($ConformeTransgressif > 0)
    $pdf->Cell(24,$yConformeTransgressif,$ConformeTransgressif,"0",0,'C',1);


  if($MaitriseSympathie > 0){
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
    $pdf->setXY(117,130 - $yMaitriseSympathie);
  }
  else{
    $pdf->setXY(117,130); 
    $MaitriseSympathie = $MaitriseSympathie * -1; 
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
  }
  if($MaitriseSympathie > 0)
    $pdf->Cell(24,$yMaitriseSympathie,$MaitriseSympathie,"0",0,'C',1);


  if($SoiAutrui > 0){
    $ySoiAutrui = ($SoiAutrui * 10) / $inc ; 
    $pdf->setXY(157,130 - $ySoiAutrui);
  }
  else{
    $pdf->setXY(157,130); 
    $SoiAutrui = $SoiAutrui * -1; 
    $ySoiAutrui = $SoiAutrui;
  }
  if($SoiAutrui > 0)
    $pdf->Cell(24,$ySoiAutrui,$SoiAutrui,"0",0,'C',1);

  /////////////////////////END  PAGE 8 ///////////////////////////////////////////////////////  

  ///////////////////////// PAGE 9 ///////////////////////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(9, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);



    //graphe 1 ////////////////////////////////////////
    $pdf->SetY(40);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setFillColor(230,230,230); 

    $BGraph1X = 47;
    for($IBGraph1X = 0; $IBGraph1X < 18 ; $IBGraph1X++){
      $pdf->Rect($BGraph1X, 40, 0, 125, 'F');
      $BGraph1X = $BGraph1X + 9; 
    }

    //  1 
    $pdf->setFillColor(255,255,255); 
    $pdf->setX(37);
    $pdf->Cell(163,125,"","LRB",0,'C',0);

    $Graph1V = 30.2;
    for($IGraph1V = 0; $IGraph1V <= 18 ; $IGraph1V++){
      $pdf->setXY($Graph1V,165);
      if($IGraph1V != 0)
        $pdf->Cell(9.5,6,$IGraph1V."0",0,0,'R',0);
      else
        $pdf->Cell(9.5,6,$IGraph1V,0,0,'R',0);
      $Graph1V = $Graph1V + 9; 
    }

    //text Graphe 1
    $arrayGraph1T = array('Serious','Playful','Compliance','Rebellious','Mastery','Sympathy','Other','Self');
    $Graph1VTY = 44;
    for($IGraph1VTY = 0; $IGraph1VTY < 8; $IGraph1VTY++){
      $pdf->setXY(19,$Graph1VTY);
      $pdf->Cell(17,9,iconv("UTF-8", "windows-1252", $arrayGraph1T[$IGraph1VTY]),"0",0,'R',0);
      $Graph1VTY = $Graph1VTY + 15;
    }

    $Graph1DrawY = 44;
    $arrayColor = array(  '0' => array(0,153,204),
                '1' => array(255,192,0),
                '3' => array(255,127,71),
                '2' => array(0,157,137),
                '4' => array(115,185,213),
                '5' => array(178,1,95),
                '6' => array(0,204,102),
                '7' => array(255,0,100)
            );

    for($IGraph1DrawY = 0; $IGraph1DrawY < 8 ; $IGraph1DrawY++){
      $pdf->setXY(37.2,$Graph1DrawY);
      $pdf->setFillColor($arrayColor[$IGraph1DrawY][0],$arrayColor[$IGraph1DrawY][1],$arrayColor[$IGraph1DrawY][2]); 

      $x = $arrayGraph1DrawY[$IGraph1DrawY]; 
      if($x > 0){
        $x = (($x * 9 ) / 10) + 1 ;
        if(in_array($IGraph1DrawY,array(2,3,4,5)))
          $pdf->Cell($x,9,"","",0,'C',0);
        else
          $pdf->Cell($x,9,"","",0,'C',1);
      }

      $Graph1DrawY = $Graph1DrawY + 15; 
    }
    
    $Graf1CX = 37.2; 
    $xt = $arrayGraph1DrawY[2]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,82.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,82.1,0.8);
      } 
      $Graf1CX += 0.9;  
    }

    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[3]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89.9,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,90.8,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,91.7,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,92.6,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,93.5,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,94.4,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,95.3,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,96.2,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,97.1,0.7);
      $Graf1MX += 0.9;  
    }

    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[4]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104.9,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,105.8,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,106.7,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,107.6,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,108.5,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,109.4,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,110.3,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,111.2,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,112.1,0.7);
      $Graf1MX += 0.9;  
    }

    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[5]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,127.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,127.1,0.8);
      }
      
      $Graf1MX += 0.9;  
    }

    // fix border dashed 
    $pdf->setFillColor(0,0,0);
    $bDashedY = 40;
    for($IbDashed = 0; $IbDashed < 44; $IbDashed++){
      if($IbDashed < 43)
        $pdf->Rect(114.5, $bDashedY, 0, 2, 'F');
      if($IbDashed < 40)
        $pdf->Rect(119, $bDashedY, 0, 2, 'F');
      if($IbDashed < 41)
        $pdf->Rect(128, $bDashedY, 0, 2, 'F');
      $pdf->Rect(141.5, $bDashedY, 0, 2, 'F');
      $bDashedY = $bDashedY + 3.5;
    }

    // text sous graphe 1 
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setFillColor(255,255,255);
    $pdf->setXY(69,175);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores  inférieurs à 90)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-left.png',60,175,11); 
    $pdf->setXY(64,185);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","16% pop. de réf. (Scores inférieurs à 90)"),"0",0,'R',1); 
    $pdf->Image($path.'img/arrow-left.png',56,185,11);  

    $pdf->setXY(129,178.5);
    $pdf->Cell(49,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores supérieurs à 110)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-right.png',178,178.5,11); 
    $pdf->setXY(142,188.9);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","26% pop. de réf. (Scores supérieurs à 115)"),"0",0,'R',1); 
    $pdf->Image($path.'img/arrow-right.png',192,188.9,11); 

    ///////////////////////// END  PAGE 9 ///////////////////////////////////////////////////////

    ///////////////////////// PAGE 10 ///////////////////////////////////////////////////////
    $pdf->AddPage("P", "A4"); 
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Relevé chiffré"),"0",'L',0);
    $pdf->Rect(10, 19, 190, 0, 'F');
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',9);
    $pdf->setFillColor(255,255,255);
    $pdf->Image($path.'img/lunette.png',7,70,195);
    $pdf->Image($path.'img/rond_gris.png',55,135,10);
    $pdf->Image($path.'img/rond_gris.png',144,135,10);
    $maxVal = max($arrayGraph1DrawY);
    //table 1 
    $Sval = ($arrayGraph1DrawY[0] * 38) / $maxVal*0.7;
    $xySval = (38 - $Sval) / 2;
    $Eval = ($arrayGraph1DrawY[1] * 38) / $maxVal*0.7;
    $xyEval = (38 - $Eval) / 2; 
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(12,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);


    $xtrans=12;
    $xxtrans=-24;
    $kxtrans=25;

    $k=10;
    $k2=15;
    $dx=3;
    /*
  Borders of the graphe * and design
    */

    $somatiqueTotal = $serieuxTotal + $conformTotal + $enjouTotal + $transagressiTotal;
    $transactionnelTotal = $sympathieTotal + $soiTotal+ $maitriseTotal +$autruiTotal;

    $pdf->AddFont('analgesics','B','analgesics.php');
    // MILIEU 1
    $pdf->setXY(45, 138);
    $pdf->SetFont('analgesics','B','analgesics.php');

    $pdf->setTextColor(90,90,90); 
    $pdf->MultiCell(30, 5,intval($somatiqueTotal),"0", 'C', 0 );
    // MILIEU 2
    $pdf->setXY(134, 138);
    $pdf->MultiCell(30, 5,intval($transactionnelTotal),"0" ,'C', 0);

    // BUT
    $pdf->setTextColor(0,0,0); 
    $pdf->SetFont('analgesics','B',10);
    $pdf->setXY(40,90);
    $pdf->MultiCell(40,55, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[0]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(42+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Buts / Moyens"), "0", 'C', 0);

    // MOYEN
    $pdf->setXY(40,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[1]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(82+$kxtrans,25, iconv("UTF-8", "windows-1252","Tendu ou Relâché"), "0", 'C', 0);
    $pdf->setXY(22,153);
    $TotalSE = $arrayGraph1DrawY[0] + $arrayGraph1DrawY[1];

    //table 2 
    $Cval = ($arrayGraph1DrawY[2] * 38) / $maxVal*0.7;
    $xyCval = (38 - $Cval) / 2;
    $Tval = ($arrayGraph1DrawY[3] * 38) / $maxVal*0.7;
    $xyTval = (38 - $Tval) / 2;
    $pdf->setXY(57+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);

    // DEVOIR
      // $pdf->Image($path.'img/testmed2.png',61 + $xyCval ,51 + $xyCval ,$Cval);
    $pdf->setXY(20,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[2]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(60+$xtrans,100);
    //$pdf->MultiCell(113+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Règles"), "0", 'L', 0);

    // LIBERTE
    $pdf->setXY(61,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[3]), "0", 'C', 0);
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(67+$xtrans,153);
    $TotalCT = $arrayGraph1DrawY[2] + $arrayGraph1DrawY[3];

    //somme table 1 & table 2
    $pdf->setXY(35+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(42.5+$xtrans,175);
    $pdf->setFillColor(230,230,230); 

    //table 3
    $SYval = ($arrayGraph1DrawY[5] * 38) / $maxVal*0.7;
    $xySYval = (38 - $SYval) / 2;
    $Mval = ($arrayGraph1DrawY[4] * 38) / $maxVal*0.7;
    $xyMval = (38 - $Mval) / 2;
    $pdf->setFillColor(255,255,255);  
    $pdf->setXY(112+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // harmonie
    $pdf->setXY(109,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[5]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(215+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Interactions"), "0", 'C', 0);

    // POUVOIR
    $pdf->setXY(150,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[4]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(247+$kxtrans,25, iconv("UTF-8", "windows-1252","Gagnant ou Perdant"), "0", 'C', 0);
    $pdf->setXY(122+$xtrans,153);
    $TotalSM = $arrayGraph1DrawY[4] + $arrayGraph1DrawY[5];

    //table 4 
    $SOval = ($arrayGraph1DrawY[7] * 38) / $maxVal*0.7;
    $xySOval = (38 - $SOval) / 2;
    $AUval = ($arrayGraph1DrawY[6] * 38) / $maxVal*0.7;
    $xyAUval = (38 - $AUval) / 2;
    $pdf->setXY(157+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // individu
    $pdf->setXY(129,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[7]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(145+$xtrans,100);
   // $pdf->MultiCell(285+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Orientation"), "0", 'L', 0);
    
      //$pdf->Image($path.'img/new_img/block_autrui.png',137 + $xyAUval-$dx+$xtrans,100 + $xyAUval+$k2+$k,$AUval);
    // collectif
    $pdf->setXY(129,113);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[6]), "0", 'C', 0);
    $pdf->setXY(167+$xtrans,153);
    $TotalSA = $arrayGraph1DrawY[7] + $arrayGraph1DrawY[6];

    //somme table 3 & table 4 
    $pdf->setXY(135.5+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(143+$xtrans ,175);
    $pdf->setFillColor(230,230,230);       
    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "10"),"0",0,'R',0);

    
    


    ///////////////////////// END  PAGE 10 ///////////////////////////////////////////////////////

    ///////////////////////// PAGE 11 À 13///////////////////////////////////////////////////////
    for ($i=11; $i < 14; $i++) { 
      $pdf->addPage();
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }
      

    ///////////////////////// END PAGE 11 À 13///////////////////////////////////////////////////////




    ///////////////////////// PAGE 14///////////////////////////////////////////////////////
        $pdf->addPage();
        $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
        $pdf->useImportedPage($pageId);

        $pdf->SetFont('RobotoCondensed-Regular','',10);
        $pdf->setFillColor(230,230,230); 

        $BGraph4X = 25;
        for($IBGraph4X = 0; $IBGraph4X < 16 ; $IBGraph4X++){
          $pdf->Rect($BGraph4X, 77, 0, 88, 'F');
          $BGraph4X = $BGraph4X + 11; 
        }
         
        $pdf->setFillColor(255,255,255); 
        $pdf->setXY(25,77);
        $pdf->Cell(176,88,"","LRB",0,'C',0);

        $Graph4V = 18;
        for($IGraph4V = 0; $IGraph4V <= 16 ; $IGraph4V++){
          $pdf->setXY($Graph4V,165);
          $pdf->Cell(9.5,6,($IGraph4V * 20),0,0,'R',0);
          $Graph4V = $Graph4V + 11; 
        } 
        //text Graphe 4
        $pdf->SetFont('RobotoCondensed-Regular','',8);
        $arrayGraph4T = array("Serious \n Playful","Compliance \n Rebellious","Mastery \n Sympathy","Other \n Self");

        $Graph4VTY = 82;
        for($IGraph4VTY = 0; $IGraph4VTY < 4; $IGraph4VTY++){
          $pdf->setXY(4,$Graph4VTY); 
          $pdf->MultiCell(20,4, iconv("UTF-8", "windows-1252",$arrayGraph4T[$IGraph4VTY]), "0", 'R', 1);
          $Graph4VTY = $Graph4VTY + 20;
        }

        $arrayTotalG4 = array($TotalSE,$TotalCT,$TotalSM,$TotalSA);
        $Graph4DrawY = 80;
        $pdf->SetFont('RobotoCondensed-Bold','',10);
        for($IGraph4DrawY = 0; $IGraph4DrawY < 4 ; $IGraph4DrawY++){
          $pdf->setXY(25.2,$Graph4DrawY);
          $pdf->setFillColor(230,230,230);
          $x = $arrayTotalG4[$IGraph4DrawY];
          $x = ($x * 11) / 20;
          if($x > 0)
            $pdf->Cell($x,12,$arrayTotalG4[$IGraph4DrawY],"",0,'C',1);
          $Graph4DrawY = $Graph4DrawY + 20; 
        }



        $pdf->Ln(35);

    ///////////////////////// END PAGE 14///////////////////////////////////////////////////////


    ///////////////////////// PAGE 15 ///////////////////////////////////////////////////////
        
          $pdf->addPage();
          $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
          $pdf->useImportedPage($pageId);
              

    ///////////////////////// END PAGE 15///////////////////////////////////////////////////////



    ///////////////////////// PAGE 16 ///////////////////////////////////////////////////////

        $pdf->addPage();
        $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
        $pdf->useImportedPage($pageId);


        $pdf->SetFont('RobotoCondensed-Regular','',10);
        $pdf->setFillColor(230,230,230); 

        $BGraph5X = 36;
        for($IBGraph5X = 0; $IBGraph5X < 15 ; $IBGraph5X++){
          $pdf->Rect($BGraph5X, 45, 0, 82, 'F');
          $BGraph5X = $BGraph5X + 11; 
        }
         
        $pdf->setFillColor(255,255,255); 
        $pdf->setXY(36,45);
        $pdf->Cell(154,82,"","LRB",0,'C',0);

        $Graph5V = 29;
        for($IGraph5V = 100; $IGraph5V <= 680 ; $IGraph5V+=40){
          $pdf->setXY($Graph5V,127);
          $pdf->Cell(9.5,6,$IGraph5V,0,0,'R',0);
          $Graph5V = $Graph5V + 11; 
        } 
        //text Graphe 4 
        $arrayGraph5T = array("Serious \n Playful \n Compliance \n Rebellious","Mastery \n Sympathy \n Other \n Self");
        $Graph5VTY = 59;
        for($IGraph5VTY = 0; $IGraph5VTY < 2; $IGraph5VTY++){
          $pdf->setXY(2,$Graph5VTY); 
          $pdf->MultiCell(32,4, iconv("UTF-8", "windows-1252",$arrayGraph5T[$IGraph5VTY]), "0", 'R', 1);
          $Graph5VTY = $Graph5VTY + 35;
        }

        $arrayTotalG5 = array($TotalSE + $TotalCT,$TotalSM + $TotalSA);
        $Graph5DrawY = 55;
        $pdf->SetFont('RobotoCondensed-Bold','',10);
        for($IGraph5DrawY = 0; $IGraph5DrawY < 2 ; $IGraph5DrawY++){
          $pdf->setXY(36.2,$Graph5DrawY);
          $pdf->setFillColor(230,230,230);
          $x = $arrayTotalG5[$IGraph5DrawY];
          $x = (($x * 11) / 40 ) - 28;
          if($x > 0)
            $pdf->Cell($x,25,$arrayTotalG5[$IGraph5DrawY],"",0,'C',1);
          $Graph5DrawY = $Graph5DrawY + 35; 
        }







        $pdf->Ln(50);


    ///////////////////////// END PAGE 16 ///////////////////////////////////////////////////////





    ///////////////////////// PAGE 17 ///////////////////////////////////////////////////////
        $pdf->addPage();
        $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
        $pdf->useImportedPage($pageId);
        $pdf->Image($path.'img/observation-4-en.png',8,48,170);

        $Graph4DrawY = 61;
        $pdf->setFillColor(230,230,230); 

        $obs4 = [
          [0,1], [2,3], [4,5], [6,7]
        ];
        for($i = 0; $i < 4 ; $i++){
          $pdf->setXY(79.1,$Graph4DrawY);
          $pdf->setFillColor(230,230,230);
          $x = $arrayTotalG4[$i];
          $x = ($x * 11) / 20;
          // x = 10 <= 20
                // <= x - 100

          if($x > 0){
            $val = $arrayGraph1DrawY[$obs4[$i][0]] + $arrayGraph1DrawY[$obs4[$i][1]];
            // $val = 120;
            $val_graph = ( ($val-100) * 9.7)/20;
            $pdf->Cell($val_graph,5,$val,"",0,'C',1);
          }
          $Graph4DrawY = $Graph4DrawY + 9 ; 
        }
    ///////////////////////// END PAGE 17 ///////////////////////////////////////////////////////





    ///////////////////////// PAGE 18 ///////////////////////////////////////////////////////
        $pdf->addPage();
        $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
        $pdf->useImportedPage($pageId);
        
        $pdf->Image($path.'img/observation-6-en.png',20,35);

        $obs4 = [
          [0,1], [2,3], [4,5], [7,6]
        ];
        $Graph4DrawY = 43.5;
        $pdf->setFillColor(230,230,230); 
        for($i = 0; $i < 4 ; $i++){
          $pdf->setXY(104.8,$Graph4DrawY);
          $x = $arrayTotalG4[$i];
          $x = ($x * 11) / 20;
          // x = 10 <= 20
                // <= x - 100

          if($x > 0){
            $val = $arrayGraph1DrawY[$obs4[$i][0]] - $arrayGraph1DrawY[$obs4[$i][1]];
            // $val = 20;
            $val_graph = ( ($val) * 10.8)/10;
            $pdf->Cell($val_graph,5.7,$val,"",0,'C',1);
          }
          $Graph4DrawY = $Graph4DrawY + 11.2  ; 
        }
    ///////////////////////// END PAGE 18 ///////////////////////////////////////////////////////


    ///////////////////////// PAGE 19 À 20///////////////////////////////////////////////////////
        for ($i=19; $i < 21; $i++) { 
          $pdf->addPage();
          $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
          $pdf->useImportedPage($pageId);
        }
          

    ///////////////////////// END PAGE 19 À 20///////////////////////////////////////////////////////

    $pdf->Output($txtRa.".pdf","I");

  }

  public function rapport_court_fr_new($id,$date,$genre){

    $test = Test::find($id);
    $repondant = $test->users[0];
    $consultant = $test->consultant;
    $serieuxTotal = 0;
    $enjouTotal = 0;
    $conformTotal = 0;
    $transagressiTotal = 0;

    $autruiTotal = 0;
    $soiTotal = 0;
    $maitriseTotal = 0;
    $sympathieTotal = 0;

    ////// GET ALL CATEGORIES ////////
    $cats = Category::all();

    ////// GET ALL QUESTIONS POUR CHAQUE CATEGORIE ///////////
    $data_quest = [];
    foreach ($cats as $key => $cat) {
      if($cat->name == "serieux"){
        $data_quest["serieux"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "enjoue"){
        $data_quest["enjoue"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "transgressif"){
        $data_quest["transgressif"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "conforme"){
        $data_quest["conforme"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "autrui"){
        $data_quest["autrui"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "soi"){
        $data_quest["soi"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "maitrise"){
        $data_quest["maitrise"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else {
        $data_quest["sympathie"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      }
    }

    // SCORE SERIEUX
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();

    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $serieuxTotal = $serieuxTotal + $score->score;
    }

    if($serieuxTotal >= 0)
      $serieuxTotal = ((($serieuxTotal - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
    
    // SCORE ENJOUE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $enjouTotal = $enjouTotal + $score->score;
    }
    if($enjouTotal >= 0)
      $enjouTotal = ((($enjouTotal - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

    // SCORE CONFORME
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $conformTotal = $conformTotal + $score->score;
    }
    if($conformTotal >= 0)
      $conformTotal = ((($conformTotal - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

    // SCORE TRANSGRESSIF
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $transagressiTotal = $transagressiTotal + $score->score;
    }
    if($transagressiTotal >= 0)
      $transagressiTotal = ((($transagressiTotal - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

    // SCORE AUTRUI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $autruiTotal = $autruiTotal + $score->score;
    }
    if($autruiTotal >= 0){
      $autruiTotal = ($autruiTotal / 2);
      $autruiTotal = ((($autruiTotal - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
    }

    // SCORE SOI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $soiTotal = $soiTotal + $score->score;
    }
    if($soiTotal >= 0){
      $soiTotal = ($soiTotal / 2);
      $soiTotal = ((($soiTotal - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
    }

    // SCORE MAITRISE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $maitriseTotal = $maitriseTotal + $score->score;
    }
    if($maitriseTotal >= 0){
      $maitriseTotal = ($maitriseTotal / 2);
      $maitriseTotal = ((($maitriseTotal - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
    }

    // SCORE SYMPHATIE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $sympathieTotal = $sympathieTotal + $score->score;
    }
    if($sympathieTotal >= 0){
      $sympathieTotal = ($sympathieTotal / 2);
      $sympathieTotal = ((($sympathieTotal - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
    }

    $arrayGraph1DrawY = array(number_format($serieuxTotal),
      number_format($enjouTotal),
      number_format($conformTotal),
      number_format($transagressiTotal),
      number_format($maitriseTotal),
      number_format($sympathieTotal),
      number_format($autruiTotal),
      number_format($soiTotal)
    );

    $pdf = new Fpdi();
    $path = base_path().'/public/templates/';
    //$pageCount = $pdf->setSourceFile($path.'isma/isma_fr_vrai.pdf');
    $pageCount = $pdf->setSourceFile($path.'isma/fr_isma.pdf');
    $tplIdx = $pdf -> importPage(1);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);

    ////////////// PAGE 1 ////////////////

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->AddFont('Lato-Bold_0','','Lato-Bold_0.php'); 
    $pdf->AddFont('GOTHIC','','GOTHIC.php'); 
    $pdf->AddFont('Lato-Regular_0','','Lato-Regular_0.php'); 
    $pdf->AddFont('GOTHICI','','GOTHICI.php');
    $pdf->SetFont('Lato-Bold_0','',14);

    $moistest = explode("-", $date);
    $dateTest = $this->getMonth2($moistest[1],"fr");

    $pdf->SetXY(192.5, 9);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(0,81,94);

    $str = $repondant->username." - ".$moistest[2]." ".$dateTest." ".$moistest[0];

    $pdf->MultiCell(105,5,iconv("UTF-8", "windows-1252",$str),0,1,'L',false);

    $pdf->SetFont('Lato-Bold_0','',12);
    if(strlen($str) > 38){
      $pdf->SetXY(192.5, 20);
    } else {
      $pdf->SetXY(192.5, 15);
    }
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(96,174,189);
    //$str = "V".mb_strtolower("otre consultant: ".$repondant->username, 'UTF-8');
    $str = "Votre consultant: ".$consultant->username;
    $pdf->MultiCell(105,5,iconv("UTF-8", "windows-1252",$str),0,1,'L',false); 

    //////////////////////////////////////////


    /////////////// PAGE 2 - 4 /////////////////

    for ($i=2; $i < 5; $i++) { 

      $tplIdx = $pdf -> importPage($i);
      $size = $pdf->getTemplateSize($tplIdx);
      $pdf->SetAutoPageBreak(false);

      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId); 
    }

    ///////////////////////////////////////////

    ////////////// PAGE 5 /////////////////////
    $pdf->SetTextColor(45,49,49);
    // GENERER LES RADARS POUR LES GRAPHES
    $path2 = base_path().'/public/templates/isma';
    if(!is_dir($path2."/".$test->id."/") == true){
      mkdir($path2."/".$test->id."/",0777,true);
    }

    $somatiqueTotal = $serieuxTotal + $conformTotal + $enjouTotal + $transagressiTotal;
    $transactionnelTotal = $sympathieTotal + $soiTotal+ $maitriseTotal +$autruiTotal;
    $total = $somatiqueTotal + $transactionnelTotal;
    $gauche = ($somatiqueTotal * 100)/$total;
    $droite = ($transactionnelTotal * 100)/$total;

    $tplIdx = $pdf -> importPage(5);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(5, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // RADAR GAUCHE
    $data = array($serieuxTotal,$conformTotal,$enjouTotal,$transagressiTotal);

    $graph = new Graph\RadarGraph(350,250,"auto");
    $graph->SetScale('lin',0,150);
    $plot = new Plot\RadarPlot($data);
    $graph->axis->SetColor('#d4d5d5');
    $plot->SetColor('white');
    $plot->SetFillColor('#d4d5d5',GRAD_VER);
    $graph->HideTickMarks();
    $graph->SetTitles(array("","","",""));
    // $graph->SetFrame(true,'black:0', 1); //but this make labels/letters weird !!!
    // $graph->img->SetTransparent('black');

    $graph->Add($plot);
    @unlink($path2."/".$test->id."/gauche.png");
    $graph->Stroke($path2."/".$test->id."/gauche.png");  

    //1 : x
    //2 : y
    //3 : taille
    $pdf->Image($path2."/".$test->id."/gauche.png",13.8,123.8,85.5);
    $pdf->Image($path2."/images/titre_hor.png",5,111,25);
    $pdf->Image($path2."/images/titre_ver.png",25,123,82);
    $pdf->Image($path2."/images/titre_hor.png",95,111,25);
    $pdf->Image($path2."/images/titre_hor.png",29.5,181,152);
    //$pdf->Image($path2."/images/titre_ver.png",5,191,152);

    // RADAR DROITE
    $data2 = array($soiTotal,$sympathieTotal,$autruiTotal,$maitriseTotal);


    // var_dump($data2);
    // die;
    $graph2 = new Graph\RadarGraph(350,250,"auto");
    $graph2->SetScale('lin',0,150);


    $plot2 = new Plot\RadarPlot($data2);
    $plot2->SetFillColor('#d4d5d5',GRAD_VER);

    $graph2->axis->SetColor('#d4d5d5');
    $plot2->SetColor('white');
    $graph2->HideTickMarks();
    //$graph2->setBox(false);
    $graph2->SetTitles(array("","","",""));
    $graph2->Add($plot2);
    @unlink($path2."/".$test->id."/droite.png");
    $graph2->Stroke($path2."/".$test->id."/droite.png");
    //1 : x
    //2 : y
    //3 : taille
    $pdf->Image($path2."/".$test->id."/droite.png",117.6,123.6,86);
    $pdf->Image($path2."/images/titre_hor.png",113.6,123,22);
    $pdf->Image($path2."/images/titre_ver.png",119.5,123,102);
    $pdf->Image($path2."/images/titre_hor.png",194.6,123,12);
    $pdf->Image($path2."/images/titre_ver.png",119.5,182,122);
  
    $pdf->Image($path2."/images/lunette_fr.png",5,76,210);
    //$pdf->Image($path2."/images/mascotte_15.png",218,98,70);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    if($value == $array["1"]){
      $pdf->SetXY(65, 148);
    } else if($value == $array["2"]){
      $pdf->SetXY(65, 162);
    } else if($value == $array["3"]){
      $pdf->SetXY(36, 162);
    } else {
      $pdf->SetXY(36, 148);
    }
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);

    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(140, 148);
    } else if($value == $array2["2"]){
      $pdf->SetXY(168, 148);
    } else if($value == $array2["3"]){
      $pdf->SetXY(168, 162);
    } else {
      $pdf->SetXY(140, 162);
    }

    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);


    ///////////////////////////////////////////// 

    //////////////// PAGE 6 /////////////////////

    $tplIdx = $pdf -> importPage(6);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(6, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',16);
    $pdf->Image($path2."/".$test->id."/gauche.png",44.5,102.4,98);
    $pdf->Image($path2."/images/titre_hor.png",44,101.5,2.9);
    $pdf->Image($path2."/images/titre_ver.png",48,101.5,100.2);
    $pdf->Image($path2."/images/titre_hor.png",138,103.5,10.2);
    $pdf->Image($path2."/images/titre_ver.png",45,169.5,100.2);
    $pdf->Image($path2."/images/gauche_fr.png",37,50,117);
    //$pdf->Image($path2."/images/petit_carre.png",92,113,10);
    //$pdf->Image($path2."/images/cercles.png",-0.2,71.5,175);
    $pdf->SetXY(91, 115);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($serieuxTotal),0),0,1,'L',0);
    $pdf->SetXY(114, 134);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($transagressiTotal),0),0,1,'L',0);
    $pdf->SetXY(91, 161);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($enjouTotal),0),0,1,'L',0);
    $pdf->SetXY(65, 134);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($conformTotal),0),0,1,'L',0);

    /////////////////////////////////////////////


    //////////////////////// PAGE 7 ///////////////////////

    $tplIdx = $pdf -> importPage(7);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(7, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('Bartender-SemiCondensedSerif','',16);
    $pdf->Image($path2."/".$test->id."/droite.png",43.6,100.2,98);
    
    $pdf->Image($path2."/images/titre_ver.png",43,98.5,98.5);
    $pdf->Image($path2."/images/titre_hor.png",43,98.5,3);
    $pdf->Image($path2."/images/titre_hor.png",135,100.5,10.2);
    $pdf->Image($path2."/images/titre_ver.png",46,167.5,100.2);
    $pdf->Image($path2."/images/droite_fr.png",37,81,113);
    $pdf->SetXY(89, 112);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($soiTotal),0),0,1,'L',0);
    $pdf->SetXY(112, 132);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($maitriseTotal),0),0,1,'L',0);
    $pdf->SetXY(89, 160);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($autruiTotal),0),0,1,'L',0);
    $pdf->SetXY(61, 132);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($sympathieTotal),0),0,1,'L',0);

    //////////////////////////////////////////////////////

    ///////////////////// PAGE 8 ///////////////////////////

    $tplIdx = $pdf -> importPage(8);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(8, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('Bartender-SemiCondensedSerif','',18);
    //$pdf->Image($path2."/images/soi.png",26,158.5,20.8);
    //$pdf->Image($path2."/images/autrui.png",28,168.5,18.8);
    // SERIEUX
    $pdf->SetXY(52, 92.5);
    $pdf->SetFillColor(140,173,219);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($serieuxTotal,0)*17)/20)-2.5),7, number_format($serieuxTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 92.5);
    $pdf->SetFillColor(117,142,172);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($serieuxTotal,0)*17)/20)-2.5),0.5,'',0,1,'R',true);

    // ENJOUE
    $pdf->SetXY(52, 103.5);
    $pdf->SetFillColor(255,201,42);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($enjouTotal,0)*17)/20)-2.5),7, number_format($enjouTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 103.5);
    $pdf->SetFillColor(196,158,56);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($enjouTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true);

    // CONFORME
    $pdf->SetXY(52, 114.5);
    $pdf->SetFillColor(91,95,96);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($conformTotal,0)*17)/20)-2.5),7, number_format($conformTotal,1),0,1,'R',true); 
    $pdf->SetXY(52, 114.5);
    $pdf->SetFillColor(76,81,81);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($conformTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true); 

    // TRANASGRESSIF
    $pdf->SetXY(52, 125.5);
    $pdf->SetFillColor(165,168,169);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*17)/20)-2.5),7, number_format($transagressiTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 125.5);
    $pdf->SetFillColor(133,138,137);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true);

    // SYMPATHIE
    $pdf->SetXY(52, 136.5);
    $pdf->SetFillColor(125,129,130);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($sympathieTotal,0)*17)/20)-2.5),7, number_format($sympathieTotal,1),0,1,'R',true); 
    $pdf->SetXY(52, 136.5);
    $pdf->SetFillColor(104,109,109);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($sympathieTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true); 

    // MAITRISE
    $pdf->SetXY(52, 147.5);
    $pdf->SetFillColor(190,192,192);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*17)/20)-2.5),7, number_format($maitriseTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 147.5);
    $pdf->SetFillColor(150,153,153);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*17)/20)-2.5),0.5,'',0,1,'R',true);

    // SOI
    $pdf->SetXY(52, 158.5);
    $pdf->SetFillColor(246,156,157);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($soiTotal,0)*17)/20)-2.5),7, number_format($soiTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 158.5);
    $pdf->SetFillColor(191,125,129);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($soiTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true);

    // AUTRUI
    $pdf->SetXY(52, 169.5);
    $pdf->SetFillColor(166,212,153);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*17)/20)-2.5),7, number_format($autruiTotal,1),0,1,'R',true);
    $pdf->SetXY(52, 169.5);
    $pdf->SetFillColor(131,169,126);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*17)/20)-2.5),0.5, '',0,1,'R',true);

    ////////////////////////////////////////////////////////  


    /////////////////////////// PAGE 9 ////////////////////

    $tplIdx = $pdf -> importPage(9);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(9, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php'); 
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->SetFont('Bartender-SemiCondensedSerif','',19);
    /*$pdf->Image($path2."/images/serieux.png",25.5,123,24);
    $pdf->Image($path2."/images/enjoue.png",52,123.7,22);
    $pdf->Image($path2."/images/soi.png",52,176,19.8);
    $pdf->Image($path2."/images/bar.png",59,174,4.7);
    $pdf->Image($path2."/images/autrui.png",40.5,175.3,18);*/
    $pdf->Image($path2."/images/carre_repere2.png",69.5,195.3,9);
    $pdf->Image($path2."/images/carre_repere2.png",84.7,195.3,9);
    $pdf->Image($path2."/images/carre_repere2.png",99.7,195.3,9);
    $pdf->Image($path2."/images/carre_repere2.png",114.5,195.3,9);
    // ENJOUE / SERIEUX
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(140,173,219);
    $pdf->SetTextColor(255,255,255);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($serieuxTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(117,142,172);
    $pdf->Cell(-(((number_format($serieuxTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(255,201,42);
    $pdf->SetTextColor(255,255,255);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($enjouTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(196,158,56);
    $pdf->Cell((((number_format($enjouTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    // LE TEXT
    $pdf->SetXY(79, 128);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0,  number_format(($serieuxTotal-$enjouTotal),0) > 0 ? number_format(($serieuxTotal-$enjouTotal),0) : number_format((abs($serieuxTotal-$enjouTotal)),0),0,1,'L',0);

    // CONFORME / TRANSGRESSIF
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(91,95,96);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($conformTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(76,81,81); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($conformTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(167,170,171);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(133,138,137); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 145.5);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($conformTotal-$transagressiTotal),0) > 0 ? number_format(($conformTotal-$transagressiTotal),0) : number_format((abs($conformTotal-$transagressiTotal)),0),0,1,'L',0);

    // SYMPATHIE / MAITRISE
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(125,129,130);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($sympathieTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(104,109,109); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($sympathieTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(190,192,192);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(150,153,153); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 163);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($maitriseTotal-$sympathieTotal),0) > 0 ? number_format(($maitriseTotal-$sympathieTotal),0) : number_format((abs($maitriseTotal-$sympathieTotal)),0),0,1,'L',0);

    // SOI / AUTRUI
    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(246,156,157);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($soiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(191,125,129);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($soiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(166,212,153);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 178);  
    $pdf->SetFillColor(131,169,126);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 180.5);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($soiTotal-$autruiTotal),0) > 0 ? number_format(($soiTotal-$autruiTotal),0) : number_format(abs(($soiTotal-$autruiTotal)),0),0,1,'L',0);    

    ////////////////////////////////////////////////////////////////////////////

    ////////////// PAGE 10 ////////////////////////////////////

    $tplIdx = $pdf -> importPage(10);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(10, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    // SERIEUX / ENJOUE
    $pdf->Image($path2."/images/ser_enjoue.png",174,130,((number_format(($serieuxTotal+$enjouTotal),0)*25)/100),6);
    $pdf->SetXY(178+((number_format(($serieuxTotal+$enjouTotal),0)*25)/100), 133);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($serieuxTotal+$enjouTotal,0),0,1,'L',0);

    // CONFORME / TRANSGRESSIF
    $pdf->Image($path2."/images/conf_trans.png",174,147,((number_format(($conformTotal+$transagressiTotal),0)*25)/100),6);
    $pdf->SetXY(178+((number_format(($conformTotal+$transagressiTotal),0)*25)/100), 150);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($conformTotal+$transagressiTotal,0),0,1,'L',0);

    // SYMPATHIE / MAITRISE
    $pdf->Image($path2."/images/symp_maitri.png",174,164,((number_format(($sympathieTotal+$maitriseTotal),0)*25)/100),6);
    $pdf->SetXY(178+((number_format(($sympathieTotal+$maitriseTotal),0)*25)/100), 167);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($sympathieTotal+$maitriseTotal,0),0,1,'L',0);

    // SOI / AUTRUI 
    $pdf->Image($path2."/images/soi_autr.png",174,181,((number_format(($soiTotal+$autruiTotal),0)*25)/100),6);
    $pdf->SetXY(178+((number_format(($soiTotal+$autruiTotal),0)*25)/100), 184);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($soiTotal+$autruiTotal,0),0,1,'L',0);

    //////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 11 //////////////////////////////////////////
    $tplIdx = $pdf -> importPage(11);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(11, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    /////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////// PAGE 12 //////////////////////////////////////////

    $pdf->setSourceFile($path.'isma/isma_fr_ancien.pdf');
    $tplIdx = $pdf->importPage(12);
    $tplIdx = $pdf -> importPage(12);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId); 

    $alldates = ReponseRepondant::where("test_id",$id)->where("repondant_id",$repondant->id)->get();
    $datePerMonth = array();

    /// TOUS LES TESTS PAR MOIS
    foreach ($alldates as $key => $date) {
      $d = explode(" ", $date->created_at);
      if(!in_array($d[0],$datePerMonth)){
        array_push($datePerMonth, $d[0]);
      }
    }

    $x = 47;
    $y = 115.5;

    $x2 = 37;
    $y2 = 112.7;

    $y3 = 198.5;
    $y4 = 195.5;

    $arrayEnjoue = array();
    $arrayEnjouey = array();

    $arraySerieux = array();
    $arraySerieuxy = array();

    $arrayTrans = array();
    $arrayTransy = array();

    $arrayConform = array();
    $arrayConformy = array();

    $arraySoi = array();
    $arraySoiy = array();

    $arrayAutrui = array();
    $arrayAutruiy = array();

    $arrayMait = array();
    $arrayMaity = array();

    $arraySymp = array();
    $arraySympy = array();

    $months = array();
    $months2 = array();
    
    foreach ($datePerMonth as $key => $val) {
      $mois = explode("-", $val);
      $month = $this->getMonth($mois[1],"fr");
      array_push($months, $month." ".$mois[0]);
      $month2 = $this->getMonth2($mois[1],"fr");
      array_push($months2, $month2." ".$mois[0]);
      
      $serieuxTotal2 = 0;
      $enjouTotal2 = 0;
      $conformTotal2 = 0;
      $transagressiTotal2 = 0;

      $autruiTotal2 = 0;
      $soiTotal2 = 0;
      $maitriseTotal2 = 0;
      $sympathieTotal2 = 0;

      // SCORE SERIEUX
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();

      foreach ($valeurs2 as $key => $value) {
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $serieuxTotal2 = $serieuxTotal2 + $score->score;
      }

      if($serieuxTotal2 >= 0)
        $serieuxTotal2 = ((($serieuxTotal2 - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
      
      // SCORE ENJOUE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $enjouTotal2 = $enjouTotal2 + $score->score;
      }
      if($enjouTotal2 >= 0)
        $enjouTotal2 = ((($enjouTotal2 - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

      // SCORE CONFORME
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $conformTotal2 = $conformTotal2 + $score->score;
      }
      if($conformTotal2 >= 0)
        $conformTotal2 = ((($conformTotal2 - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

      // SCORE TRANSGRESSIF
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $transagressiTotal2 = $transagressiTotal2 + $score->score;
      }
      if($transagressiTotal2 >= 0)
        $transagressiTotal2 = ((($transagressiTotal2 - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

      // SCORE AUTRUI
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $autruiTotal2 = $autruiTotal2 + $score->score;
      }
      if($autruiTotal2 >= 0){
        $autruiTotal2 = ($autruiTotal2 / 2);
        $autruiTotal2 = ((($autruiTotal2 - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
      }

      // SCORE SOI
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $soiTotal2 = $soiTotal2 + $score->score;
      }
      if($soiTotal2 >= 0){
        $soiTotal2 = ($soiTotal2 / 2);
        $soiTotal2 = ((($soiTotal2 - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
      }

      // SCORE MAITRISE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $maitriseTotal2 = $maitriseTotal2 + $score->score;
      }
      if($maitriseTotal2 >= 0){
        $maitriseTotal2 = ($maitriseTotal2 / 2);
        $maitriseTotal2 = ((($maitriseTotal2 - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
      }

      // SCORE SYMPHATIE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $sympathieTotal2 = $sympathieTotal2 + $score->score;
      }
      if($sympathieTotal2 >= 0){
        $sympathieTotal2 = ($sympathieTotal2 / 2);
        $sympathieTotal2 = ((($sympathieTotal2 - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
      }

      $arrayMonths[$month2." ".$mois[0]] = array(number_format($serieuxTotal2),
        number_format($enjouTotal2),
        number_format($conformTotal2),
        number_format($transagressiTotal2),
        number_format($maitriseTotal2),
        number_format($sympathieTotal2),
        number_format($autruiTotal2),
        number_format($soiTotal2)
      );

      // SERIEUX
      $pdf->SetXY(47, $y);
      $pdf->SetFillColor(140,173,219);
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true);
      $pdf->SetXY(47, $y);
      $pdf->SetFillColor(117,142,172);     
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);     

      // ENJOUE
      $pdf->SetXY(47, $y2);
      $pdf->SetFillColor(255,201,42);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y2);
      $pdf->SetFillColor(196,158,56);     
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(38, $y2+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0);

      array_push($arrayEnjoue,(((number_format($enjouTotal2,0)*20)/50))+1.5);
      array_push($arrayEnjouey,number_format($y2,0));

      array_push($arraySerieux,(((number_format($serieuxTotal2,0)*20)/50))+1.5);
      array_push($arraySerieuxy,number_format($y,0));

      // TRANGRESSIF
      $pdf->SetXY(175.5, $y2);
      $pdf->SetFillColor(165,168,169);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y2);
      $pdf->SetFillColor(133,138,137);     
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);  
      
      // CONFORME
      $pdf->SetXY(175.5, $y);
      $pdf->SetFillColor(91,95,96);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true);
      $pdf->SetXY(175.5, $y);
      $pdf->SetFillColor(76,81,81);     
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(166.5, $y2+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0);

      array_push($arrayTrans,(((number_format($transagressiTotal2,0)*20)/50))+1.5);
      array_push($arrayTransy,number_format($y2,0));

      array_push($arrayConform,(((number_format($conformTotal2,0)*20)/50))+1.5);
      array_push($arrayConformy,number_format($y,0));

      // AUTRUI
      $pdf->SetXY(47, $y3);
      $pdf->SetFillColor(166,212,153);
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y3);
      $pdf->SetFillColor(131,169,126);     
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      // SOI
      $pdf->SetXY(47, $y4);
      $pdf->SetFillColor(246,156,157);
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y4);
      $pdf->SetFillColor(191,125,129);     
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true); 

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(38, $y4+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0); 
      
      array_push($arraySoi,(((number_format($soiTotal2,0)*20)/50))+1.5);
      array_push($arraySoiy,number_format($y4,0));

      array_push($arrayAutrui,(((number_format($autruiTotal2,0)*20)/50))+1.5);
      array_push($arrayAutruiy,number_format($y3,0));

      // MAITRISE
      $pdf->SetXY(175.5, $y3);
      $pdf->SetFillColor(190,192,192);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y3);
      $pdf->SetFillColor(150,153,153);     
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      // SYMPATHIE
      $pdf->SetXY(175.5, $y4);
      $pdf->SetFillColor(125,129,130);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y4);
      $pdf->SetFillColor(104,109,109);     
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);      

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(166.5, $y4+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0); 

      array_push($arraySymp,(((number_format($sympathieTotal2,0)*20)/50))+1.5);
      array_push($arraySympy,number_format($y4,0));

      array_push($arrayMait,(((number_format($maitriseTotal2,0)*20)/50))+1.5);
      array_push($arrayMaity,number_format($y3,0));

      // TEXT SERIEUX
      $pdf->SetFont('Bartender-SemiCondensedSerif','',9);
      $pdf->SetXY(55, $y+0.5);
      $pdf->SetTextColor(117,142,172);     
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50)),0.5, number_format($serieuxTotal2,0),0,1,'R',0);

      // TEXT ENJOUE
      $pdf->SetXY(55, $y2+1);
      $pdf->SetTextColor(255,201,42);     
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50)),0.5, number_format($enjouTotal2,0),0,1,'R',0);

      // TEXT TRANSGRESSIF
      $pdf->SetXY(182.5, $y2+1);
      $pdf->SetTextColor(165,168,169);    
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,0.5, number_format($transagressiTotal2,0),0,1,'R',0);

      // TEXT CONFORME
      $pdf->SetXY(182.5, $y+1);
      $pdf->SetTextColor(91,95,96);    
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,0.5, number_format($conformTotal2,0),0,1,'R',0);         
      // TEXT AUTRUI
      $pdf->SetXY(55, $y3+0.5);
      $pdf->SetTextColor(166,212,153);     
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50)),0.5, number_format($autruiTotal2,0),0,1,'R',0);

      // TEXT SOI
      $pdf->SetXY(55, $y4+0.5);
      $pdf->SetTextColor(246,156,157);     
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50)),0.5, number_format($soiTotal2,0),0,1,'R',0);

      // TEXT MAITRISE
      $pdf->SetXY(182.5, $y3+1);
      $pdf->SetTextColor(190,192,192);    
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,0.5, number_format($maitriseTotal2,0),0,1,'R',0);

      // TEXT SYMPATHIE
      $pdf->SetXY(182.5, $y4+1);
      $pdf->SetTextColor(125,129,130);    
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,0.5, number_format($sympathieTotal2,0),0,1,'R',0);

      $y = $y - 6.5;
      $y2 = $y2 - 6.5;

      $y3 = $y3 - 6.5;
      $y4 = $y4 - 6.5;  

    }

    // AFFICHER LE MOIS DE DEBUT ET FIN
    $pdf->SetFont('Lato-Regular_0','',18);
    $pdf->SetXY(170.5, 39.5);
    $pdf->SetTextColor(255,204,51);  
    $str = current($months)." - ".end($months);
    $str = mb_strtoupper($str, 'UTF-8');  
    $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$str.")"),0,1,'R',0);

    // DESSINER LES LIGNES HORIZONTALES

    for ($i=1; $i <= 10; $i++) { 
      if(isset($arrayEnjoue[$i]) && isset($arraySerieux[$i])){
        // ENJOUE
        $pdf->SetDrawColor(196,158,56);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayEnjoue[$i-1],0)+46.5, $arrayEnjouey[$i-1], number_format($arrayEnjoue[$i],0)+46.5, $arrayEnjouey[$i]);

        // SERIEUX
        $pdf->SetDrawColor(117,142,172);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySerieux[$i-1],0)+46.5, $arraySerieuxy[$i-1], number_format($arraySerieux[$i],0)+46.5, $arraySerieuxy[$i]);

        // TRANSGRESSIF
        $pdf->SetDrawColor(133,138,137);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayTrans[$i-1],0)+175, $arrayTransy[$i-1], number_format($arrayTrans[$i],0)+175, $arrayTransy[1]); 

        // CONFORME
        $pdf->SetDrawColor(76,81,81);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayConform[$i-1],0)+175, $arrayConformy[$i-1], number_format($arrayConform[$i],0)+175, $arrayConformy[$i]);

        // AUTRUI
        $pdf->SetDrawColor(166,212,153);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayAutrui[$i-1],0)+46.5, $arrayAutruiy[$i-1], number_format($arrayAutrui[$i],0)+46.5, $arrayAutruiy[$i]);

        // SOI
        $pdf->SetDrawColor(246,156,157);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySoi[$i-1],0)+46.5, $arraySoiy[$i-1], number_format($arraySoi[$i],0)+46.5, $arraySoiy[$i]);

        // MAITRISE
        $pdf->SetDrawColor(150,153,153);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayMait[$i-1],0)+175, $arrayMaity[$i-1], number_format($arrayMait[$i],0)+175, $arrayMaity[$i]);

        // SYMPATHIE
        $pdf->SetDrawColor(104,109,109);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySymp[$i-1],0)+175, $arraySympy[$i-1], number_format($arraySymp[$i],0)+175, $arraySympy[$i]);        
      }      
    }

    /////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////// PAGE 13 ///////////////////////////////////////////////

    $pdf->setSourceFile($path.'isma/isma_fr_ancien.pdf');
    $tplIdx = $pdf->importPage(13);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    
    $cur = 0;
    $x = 26.5;
    $y = 70.5;
    $x3 = 26.5;
    $y3 = 70.5;
    $xmois = 65.5;
    $xmois3 = 65.5;
    foreach ($arrayMonths as $key => $value) {
      if($cur < 3){

        // AFFICHER LE MOIS
        $pdf->SetFont('GOTHICI','',12);
        $pdf->SetXY($xmois, 61.5);
        $pdf->SetTextColor(167,169,171);    
        $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0); 

        // SERIEUX
        $pdf->SetFont('Bartender-SemiCondensedSerif','',9.5);
        $pdf->SetXY($x, $y);
        $pdf->SetFillColor(140,173,219);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y);
        $pdf->SetFillColor(117,142,172);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2,'',0,1,'R',true) : "";

        // ENJOUE
        $pdf->SetXY($x, $y+5);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+5);
        $pdf->SetFillColor(196,158,56);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // CONFORME
        $pdf->SetXY($x, $y+10);
        $pdf->SetFillColor(91,95,96);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+10);
        $pdf->SetFillColor(76,81,81);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // TRANSGRESSIF
        $pdf->SetXY($x, $y+15);
        $pdf->SetFillColor(165,168,169);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+15);
        $pdf->SetFillColor(133,138,137);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SYMPATHIE
        $pdf->SetXY($x, $y+20);
        $pdf->SetFillColor(125,129,130);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+20);
        $pdf->SetFillColor(104,109,109);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // MAITRISE
        $pdf->SetXY($x, $y+25);
        $pdf->SetFillColor(190,192,192);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+25);
        $pdf->SetFillColor(150,153,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SOI
        $pdf->SetXY($x, $y+30.5);
        $pdf->SetFillColor(246,156,157);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+30.5);
        $pdf->SetFillColor(191,125,129);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // AUTRUI
        $pdf->SetXY($x, $y+35.5);
        $pdf->SetFillColor(166,212,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+35.5);
        $pdf->SetFillColor(131,169,126);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";       

        $x = $x+97;
        $xmois = $xmois+96;

      } else {
        $y3 = $y3 + 71;

        // AFFICHER LE MOIS
        $pdf->SetFont('GOTHICI','',12);
        $pdf->SetXY($xmois3, 134.5);
        $pdf->SetTextColor(167,169,171);    
        $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

        // SERIEUX
        $pdf->SetFont('Bartender-SemiCondensedSerif','',9.5);
        $pdf->SetXY($x3, $y3);
        $pdf->SetFillColor(140,173,219);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell((((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // ENJOUE
        $pdf->SetXY($x3, $y3+5);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+5);
        $pdf->SetFillColor(196,158,56);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // CONFORME
        $pdf->SetXY($x3, $y3+10);
        $pdf->SetFillColor(91,95,96);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+10);
        $pdf->SetFillColor(76,81,81);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // TRANSGRESSIF
        $pdf->SetXY($x3, $y3+15);
        $pdf->SetFillColor(165,168,169);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+15);
        $pdf->SetFillColor(133,138,137);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SYMPATHIE
        $pdf->SetXY($x3, $y3+20);
        $pdf->SetFillColor(125,129,130);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+20);
        $pdf->SetFillColor(104,109,109);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // MAITRISE
        $pdf->SetXY($x3, $y3+25);
        $pdf->SetFillColor(190,192,192);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+25);
        $pdf->SetFillColor(150,153,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SOI
        $pdf->SetXY($x3, $y3+30.5);
        $pdf->SetFillColor(246,156,157);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+30.5);
        $pdf->SetFillColor(191,125,129);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // AUTRUI
        $pdf->SetXY($x3, $y3+35.5);
        $pdf->SetFillColor(166,212,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+35.5);
        $pdf->SetFillColor(131,169,126);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";        

        $xmois3 = $xmois3+96;
        $x3 = $x3 + 97;
      }
      $cur=$cur+1;
    }

    //AFFICHER LE MOIS DE DEBUT ET FIN
    $pdf->SetFont('Lato-Regular_0','',18);
    $pdf->SetXY(170.5, 39.5);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(255,204,51);   
    $str = $months[0]." - ".end($months);
    $str = mb_strtoupper($str, 'UTF-8'); 
    $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$str.")"),0,1,'R',0);

    /////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////// PAGE 14 ////////////////////////////////////////////////

    $pdf->setSourceFile($path.'isma/isma_fr_ancien.pdf');
    $tplIdx = $pdf->importPage(14);
    $tplIdx = $pdf -> importPage(14);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    //if(count($arrayMonths)>5){
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
   

      // AFFICHER LE MOIS DE DEBUT ET FIN
      $pdf->SetFont('RobotoCondensed-Bold','',18);
      $pdf->SetXY(170.5, 39.5);
      $pdf->SetTextColor(255,201,42);    
      //$pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$months[0]." - ".end($months).")"),0,1,'R',0);

      $cur = 0;
      $x = 64.8;
      $y = 64.5;
      $x3 = 64.8;
      $xmois = 98.5;
      $xmois3 = 98.5;
      foreach ($arrayMonths as $key => $value) {
        if($cur >=6 && $cur < 8){
          // SERIEUX
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x, $y);
          $pdf->SetFillColor(140,173,219);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x, $y);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // ENJOUE
          $pdf->SetXY($x, $y+5);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+5);
          $pdf->SetFillColor(196,158,56);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // CONFORME
          $pdf->SetXY($x, $y+10.5);
          $pdf->SetFillColor(91,95,96);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+10.5);
          $pdf->SetFillColor(76,81,81);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // TRANSGRESSIF
          $pdf->SetXY($x, $y+16);
          $pdf->SetFillColor(165,168,169);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+16);
          $pdf->SetFillColor(133,138,137);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SYMPATHIE
          $pdf->SetXY($x, $y+21);
          $pdf->SetFillColor(125,129,130);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+21);
          $pdf->SetFillColor(104,109,109);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";
          // MAITRISE
          $pdf->SetXY($x, $y+26);
          $pdf->SetFillColor(190,192,192);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+26);
          $pdf->SetFillColor(150,153,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SOI
          $pdf->SetXY($x, $y+31);
          $pdf->SetFillColor(246,156,157);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+31);
          $pdf->SetFillColor(191,125,129);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AUTRUI
          $pdf->SetXY($x, $y+36);
          $pdf->SetFillColor(166,212,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+36);
          $pdf->SetFillColor(131,169,126);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2,'',0,1,'R',true) : "";

          // AFFICHER LE MOIS
          $pdf->SetFont('RobotoCondensed-Regular','',14);
          $pdf->SetXY($xmois, 58.5);
          $pdf->SetTextColor(167,169,171);    
          $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

          $x = $x+111.5;
          $xmois = $xmois+116;
        } else if($cur >= 8) {
          // SERIEUX
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x3, 126);
          $pdf->SetFillColor(140,173,219);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x3, 126);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // ENJOUE
          $pdf->SetXY($x3, 126+5);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+5);
          $pdf->SetFillColor(196,158,56);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // CONFORME
          $pdf->SetXY($x3, 126+10.5);
          $pdf->SetFillColor(91,95,96);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+10.5);
          $pdf->SetFillColor(76,81,81);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // TRANSGRESSIF
          $pdf->SetXY($x3, 126+16);
          $pdf->SetFillColor(165,168,169);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+16);
          $pdf->SetFillColor(133,138,137);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SYMPATHIE
          $pdf->SetXY($x3, 126+21);
          $pdf->SetFillColor(125,129,130);
          $pdf->SetTextColor(255,255,255);
          //isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+21);
          $pdf->SetFillColor(104,109,109);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // MAITRISE
          $pdf->SetXY($x3, 126+26);
          $pdf->SetFillColor(190,192,192);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+26);
          $pdf->SetFillColor(150,153,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SOI
          $pdf->SetXY($x3, 126+31);
          $pdf->SetFillColor(246,156,157);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+31);
          $pdf->SetFillColor(191,125,129);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AUTRUI
          $pdf->SetXY($x3, 126+36);
          $pdf->SetFillColor(166,212,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+36);
          $pdf->SetFillColor(131,169,126);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AFFICHER LE MOIS
          $pdf->SetFont('RobotoCondensed-Regular','',14);
          $pdf->SetXY($xmois3, 120.5);
          $pdf->SetTextColor(167,169,171);    
          $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

          $xmois3 = $xmois3+116.5;
          $x3 = $x3 + 111.6;
        }
        $cur = $cur+1;    
      }
    //}

    ////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 15 //////////////////////////////////////////

    $pdf->setSourceFile($path.'isma/fr_isma.pdf');
    $tplIdx = $pdf -> importPage(15);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    ///////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 16 //////////////////////////////////////////

    $tplIdx = $pdf -> importPage(16);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->Image($path2."/".$test->id."/gauche.png",53.5,93.5,85);
    $pdf->Image($path2."/images/titre_ver.png",69,150,120);
    $pdf->Image($path2."/".$test->id."/droite.png",156.9,94.3,85);
    $pdf->Image($path2."/images/titre_ver.png",165,151,120);
    $pdf->Image($path2."/images/titre_ver.png",165,94,120);

    $pdf->Image($path2."/images/lunettefr1.png",35,43,225);
    $pdf->Image($path2."/images/petit_carre.png",99,99,10);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    $key = array_search($value, $array);
    if($value == $array["1"]){
      $pdf->SetXY(100, 116);
    } else if($value == $array["2"]){
      $pdf->SetXY(100, 133);
    } else if($value == $array["3"]){
      $pdf->SetXY(70, 133);
    } else {
      $pdf->SetXY(70, 116);
    }
    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);
    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(172, 116);
    } else if($value == $array2["2"]){
      $pdf->SetXY(202, 116);
    } else if($value == $array2["3"]){
      $pdf->SetXY(202, 133);
    } else {
      $pdf->SetXY(172, 133);
    }

    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);

    ///////////////////////////////////////////////////////////////////////////////////


    /////////////////////////// PAGE 17 ////////////////////////////////////////////

    $tplIdx = $pdf -> importPage(17);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->Image($path2."/".$test->id."/gauche.png",53.9,93.9,85);
    $pdf->Image($path2."/images/titre_ver.png",69,150,120);
    $pdf->Image($path2."/".$test->id."/droite.png",155.8,95,85);
    $pdf->Image($path2."/images/titre_ver.png",165,147,120);
    $pdf->Image($path2."/images/titre_ver.png",165,97,120);

    $pdf->Image($path2."/images/lunettefr2.png",35,43,225);
    $pdf->Image($path2."/images/petit_carre.png",99,99,10);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    $key = array_search($value, $array);
    if($value == $array["1"]){
      $pdf->SetXY(100, 116);
    } else if($value == $array["2"]){
      $pdf->SetXY(100, 133);
    } else if($value == $array["3"]){
      $pdf->SetXY(70, 133);
    } else {
      $pdf->SetXY(70, 116);
    }
    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);
    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(172, 116);
    } else if($value == $array2["2"]){
      $pdf->SetXY(202, 116);
    } else if($value == $array2["3"]){
      $pdf->SetXY(202, 133);
    } else {
      $pdf->SetXY(172, 133);
    }

    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);    

    ////////////////////////////////////////////////////////////////////////////////// 


    /////////////////////////////////// PAGE 18 ///////////////////////////////////////
    $tplIdx = $pdf -> importPage(18);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);    

    //////////////////////////////////////////////////////////////////////////////////  

    $txtRa = "Rapport ISMA FR 2019-".iconv('ISO-8859-1', 'UTF-8//IGNORE', $month)."-".str_replace(" ","-",$repondant->username);

    if($genre == "pdf"){
      $pdf->Output($txtRa.".pdf","I");
      $this->deleteFolder($path2."/".$test->id."/");
    } else {
      $path = base_path().'/public/templates/isma/';
      $target_dir = $path."/archive/".$test->id."_".$mois[1];
      if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
      }
      $pdf->Output($path."archive/pptx".$test->id."_".$mois[1].".pdf",'F');

      $pdf = new PDF_Diag();
      $pagecount = $pdf->setSourceFile($path."archive/pptx".$test->id."_".$mois[1].".pdf");

      $target_dir2 = $path."archive/liste_images_".$test->id."_".$mois[1];
      if (!file_exists($target_dir2)) {
        mkdir($target_dir2, 0777, true);
      }

      // Split each page into a new PDF
      for ($i = 1; $i <= $pagecount; $i++) {
        $new_pdf = new PDF_Diag();
        $new_pdf->AddPage();
        $new_pdf->setSourceFile($path."archive/pptx".$test->id."_".$mois[1].".pdf");
        $new_pdf->useTemplate($new_pdf->importPage($i), null, null, $size["width"], $size["height"],TRUE);
        
        try {
          $new_filename = $path."archive/pptx".$test->id."_".$mois[1]."_".$i.".pdf";
          $new_pdf->Output($new_filename, "F");
          
          ini_set('max_execution_time', 400);
          $array=array();
          $count = $i-1;
          // -COLORSPACE SRGB ENLEVE LES FOND NOIR LORS DE LA CONVERSION 
          exec("convert -density 200 -colorspace sRGB ".$new_filename." -quality 60 ".$path."archive/liste_images_".$test->id."_".$mois[1]."/ppt-".$count.".png", $array);

        } catch (Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
      }

      copy($path.'isma_fr2.pptx',$path."archive/".$test->id."_".$mois[1].".zip");
      $filename = $path."archive/".$test->id."_".$mois[1].".zip";
      $zip = new ZipArchive;
      $res = $zip->open($filename);

      if ($res === TRUE) {
        $newpath = $path."archive/".$test->id."_".$mois[1];
        // Extract file
        $zip->extractTo($newpath);
        $zip->close();

        /////////////// ALL - SLIDES  ////////////////////////////////////////////

        $count = 3;
        for ($i=0; $i < 18; $i++) { 
          copy($path."archive/liste_images_".$test->id."_".$mois[1]."/ppt-".$i.".png",$path."archive/".$test->id."_".$mois[1]."/ppt/media/image".$count.".png");
          $count++; 
        }
        //////////////////////////////////////////////////////////////////////

        header('Content-Type: text/html; charset=ISO-8859-1');
        $this->zipFolder($zip,$newpath,$path."archive/".$test->id."_".$mois[1]."/");
        if(!is_dir(base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx") == true){
          mkdir(base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx",0777,true);   
        }
        $file_url = url("/uploads/isma/".$test->id."_".$mois[1]."/pptx");
        $dir = base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");
        copy($path."archive/".$test->id."_".$mois[1].'/file.zip', $dir);

        if(file_exists($dir)){
          $this->deleteFolder($path."archive");
          $this->deleteFolder($path2."/".$test->id);
          copy($path.'backup.pptx',$path.'isma_fr2.pptx');
          $response["generated"] = true;
          $response["doc"] = $file_url."/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");;
          echo json_encode($response);
        }
      }
    }
  }

  public function rapport_court_en_new_pptx(Request $request){
    $this->rapport_court_en_new($request->id,$request->date,$request->genre);
  }

  public function rapport_court_en_new($id,$date,$genre){
    $test = Test::find($id);
    $repondant = $test->users[0];
    $consultant = $test->consultant;

    $serieuxTotal = 0;
    $enjouTotal = 0;
    $conformTotal = 0;
    $transagressiTotal = 0;

    $autruiTotal = 0;
    $soiTotal = 0;
    $maitriseTotal = 0;
    $sympathieTotal = 0;

    ////// GET ALL CATEGORIES ////////
    $cats = Category::all();

    ////// GET ALL QUESTIONS POUR CHAQUE CATEGORIE ///////////
    $data_quest = [];
    foreach ($cats as $key => $cat) {
      if($cat->name == "serieux"){
        $data_quest["serieux"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "enjoue"){
        $data_quest["enjoue"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "transgressif"){
        $data_quest["transgressif"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "conforme"){
        $data_quest["conforme"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "autrui"){
        $data_quest["autrui"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "soi"){
        $data_quest["soi"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "maitrise"){
        $data_quest["maitrise"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else {
        $data_quest["sympathie"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      }
    }

    // SCORE SERIEUX
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();

    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $serieuxTotal = $serieuxTotal + $score->score;
    }

    if($serieuxTotal >= 0)
      $serieuxTotal = ((($serieuxTotal - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
    
    // SCORE ENJOUE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $enjouTotal = $enjouTotal + $score->score;
    }
    if($enjouTotal >= 0)
      $enjouTotal = ((($enjouTotal - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

    // SCORE CONFORME
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $conformTotal = $conformTotal + $score->score;
    }
    if($conformTotal >= 0)
      $conformTotal = ((($conformTotal - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

    // SCORE TRANSGRESSIF
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $transagressiTotal = $transagressiTotal + $score->score;
    }
    if($transagressiTotal >= 0)
      $transagressiTotal = ((($transagressiTotal - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

    // SCORE AUTRUI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $autruiTotal = $autruiTotal + $score->score;
    }
    if($autruiTotal >= 0){
      $autruiTotal = ($autruiTotal / 2);
      $autruiTotal = ((($autruiTotal - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
    }

    // SCORE SOI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $soiTotal = $soiTotal + $score->score;
    }
    if($soiTotal >= 0){
      $soiTotal = ($soiTotal / 2);
      $soiTotal = ((($soiTotal - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
    }

    // SCORE MAITRISE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $maitriseTotal = $maitriseTotal + $score->score;
    }
    if($maitriseTotal >= 0){
      $maitriseTotal = ($maitriseTotal / 2);
      $maitriseTotal = ((($maitriseTotal - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
    }

    // SCORE SYMPHATIE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $sympathieTotal = $sympathieTotal + $score->score;
    }
    if($sympathieTotal >= 0){
      $sympathieTotal = ($sympathieTotal / 2);
      $sympathieTotal = ((($sympathieTotal - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
    }

    $arrayGraph1DrawY = array(number_format($serieuxTotal),
      number_format($enjouTotal),
      number_format($conformTotal),
      number_format($transagressiTotal),
      number_format($maitriseTotal),
      number_format($sympathieTotal),
      number_format($autruiTotal),
      number_format($soiTotal)
    );

    $pdf = new Fpdi();
    $path = base_path().'/public/templates/';
    //$pageCount = $pdf->setSourceFile($path.'isma/isma_en_vrai.pdf');
    $pageCount = $pdf->setSourceFile($path.'isma/en_isma.pdf');
    $tplIdx = $pdf -> importPage(1);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetAutoPageBreak(false);

    ////////////// PAGE 1 ////////////////

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->AddFont('Lato-Bold_0','','Lato-Bold_0.php'); 
    $pdf->AddFont('GOTHIC','','GOTHIC.php'); 
    $pdf->AddFont('Lato-Regular_0','','Lato-Regular_0.php'); 
    $pdf->AddFont('GOTHICI','','GOTHICI.php');
    $pdf->SetFont('Lato-Bold_0','',16);

    $moistest = explode("-", $date);
    $dateTest = $this->getMonth2($moistest[1],"fr");

    $pdf->SetXY(192.5, 9);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(0,81,94);

    $str = $repondant->username." - ".$moistest[2]." ".$dateTest." ".$moistest[0];
    $pdf->MultiCell(105,5,iconv("UTF-8", "windows-1252",$str),0,1,'L',false);

    $pdf->SetFont('Lato-Bold_0','',12);
    if(strlen($str) > 32){
      $pdf->SetXY(192.5, 20);
    } else {
    $pdf->SetXY(192.5, 15);
    }
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(96,174,189);
    $str = "Your coach: ".$consultant->username;
    $pdf->MultiCell(105,5,iconv("UTF-8", "windows-1252",$str),0,1,'L',false); 

    //////////////////////////////////////////

    ////////////// PAGE 2 - 4 ////////////////

    for ($i=2; $i < 5; $i++) { 
      $tplIdx = $pdf -> importPage($i);
      $size = $pdf->getTemplateSize($tplIdx);
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId); 
    }

    //////////////////////////////////////////

    ////////////// PAGE 5 /////////////////////
    $tplIdx = $pdf -> importPage(5);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->SetTextColor(45,49,49);
    // GENERER LES RADARS POUR LES GRAPHES
    $path2 = base_path().'/public/templates/isma';
    if(!is_dir($path2."/".$test->id."/") == true){
      mkdir($path2."/".$test->id."/",0777,true);
    }

    $somatiqueTotal = $serieuxTotal + $conformTotal + $enjouTotal + $transagressiTotal;
    $transactionnelTotal = $sympathieTotal + $soiTotal+ $maitriseTotal +$autruiTotal;
    $total = $somatiqueTotal + $transactionnelTotal;
    $gauche = ($somatiqueTotal * 100)/$total;
    $droite = ($transactionnelTotal * 100)/$total;

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(5, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->AddFont('Lato-Bold_0','','Lato-Bold_0.php'); 
    $pdf->AddFont('GOTHIC','','GOTHIC.php'); 
    $pdf->AddFont('Lato-Regular_0','','Lato-Regular_0.php'); 
    $pdf->AddFont('GOTHICI','','GOTHICI.php'); 
    
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // RADAR GAUCHE
    $data = array($serieuxTotal,$conformTotal,$enjouTotal,$transagressiTotal);
    $graph = new Graph\RadarGraph(350,250,"auto");
    $graph->SetScale('lin',0,150);

    $plot = new Plot\RadarPlot($data);
    $graph->axis->SetColor('#d4d5d5');
    $plot->SetColor('white');
    $plot->SetFillColor('#d4d5d5',GRAD_VER);
    $graph->HideTickMarks();
    $graph->SetTitles(array("","","",""));
    $graph->Add($plot);
    @unlink($path2."/".$test->id."/gauche.png");
    $graph->Stroke($path2."/".$test->id."/gauche.png");  

    //1 : x
    //2 : y
    //3 : taille
    $pdf->Image($path2."/".$test->id."/gauche.png",12.4,123.8,85.5);
    $pdf->Image($path2."/images/titre_hor.png",5,111,25);
    $pdf->Image($path2."/images/titre_ver.png",25,123,82);
    $pdf->Image($path2."/images/titre_hor.png",95,111,25);
    $pdf->Image($path2."/images/titre_hor.png",29.5,181,152);
    //$pdf->Image($path2."/images/titre_ver.png",5,191,152);

    // RADAR DROITE
    $data2 = array($soiTotal,$sympathieTotal,$autruiTotal,$maitriseTotal);
    $graph2 = new Graph\RadarGraph(350,250,"auto");
    $graph2->SetScale('lin',0,150);

    $plot2 = new Plot\RadarPlot($data2);
    $plot2->SetFillColor('#d4d5d5',GRAD_VER);

    $graph2->axis->SetColor('#d4d5d5');
    $plot2->SetColor('white');
    $graph2->HideTickMarks();
    //$graph2->setBox(false);
    $graph2->SetTitles(array("","","",""));
    $graph2->Add($plot2);
    @unlink($path2."/".$test->id."/droite.png");
    $graph2->Stroke($path2."/".$test->id."/droite.png");
    //1 : x
    //2 : y
    //3 : taille
    $pdf->Image($path2."/".$test->id."/droite.png",116.5,123.4,85.5);
    $pdf->Image($path2."/images/titre_hor.png",111.6,121,22);
    $pdf->Image($path2."/images/titre_ver.png",118.5,121,92);
    $pdf->Image($path2."/images/titre_hor.png",190.6,120,12);
    $pdf->Image($path2."/images/titre_ver.png",115.5,181,112);
  
    $pdf->Image($path2."/images/lunette_en.png",3.5,111,210);
    //$pdf->Image($path2."/images/mascotte_15.png",218,98,70);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    if($value == $array["1"]){
      $pdf->SetXY(65, 148);
    } else if($value == $array["2"]){
      $pdf->SetXY(65, 162);
    } else if($value == $array["3"]){
      $pdf->SetXY(36, 162);
    } else {
      $pdf->SetXY(36, 148);
    }
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);

    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(140, 148);
    } else if($value == $array2["2"]){
      $pdf->SetXY(168, 148);
    } else if($value == $array2["3"]){
      $pdf->SetXY(168, 162);
    } else {
      $pdf->SetXY(140, 162);
    }

    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);


    ///////////////////////////////////////////// 

    //////////////// PAGE 6 /////////////////////

    $tplIdx = $pdf -> importPage(6);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(6, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',16);
    $pdf->Image($path2."/".$test->id."/gauche.png",45.5,102.4,98);
    $pdf->Image($path2."/images/titre_hor.png",45,101.5,2.9);
    $pdf->Image($path2."/images/titre_ver.png",48,101.5,100.2);
    $pdf->Image($path2."/images/titre_hor.png",138,103.5,10.2);
    $pdf->Image($path2."/images/titre_ver.png",45,169.5,100.2);
    $pdf->Image($path2."/images/gauche_en.png",37,89.5,117);
    //$pdf->Image($path2."/images/petit_carre.png",92,113,10);
    //$pdf->Image($path2."/images/cercles.png",-0.2,71.5,175);
    $pdf->SetXY(91, 115);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($serieuxTotal),0),0,1,'L',0);
    $pdf->SetXY(114, 134);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($transagressiTotal),0),0,1,'L',0);
    $pdf->SetXY(91, 161);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($enjouTotal),0),0,1,'L',0);
    $pdf->SetXY(65, 134);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($conformTotal),0),0,1,'L',0);

    /////////////////////////////////////////////


    //////////////////////// PAGE 7 ///////////////////////

    $tplIdx = $pdf -> importPage(7);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(7, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('Bartender-SemiCondensedSerif','',16);
    $pdf->Image($path2."/".$test->id."/droite.png",44.2,96.2,98);
    
    $pdf->Image($path2."/images/titre_ver.png",45,95.5,98.2);
    $pdf->Image($path2."/images/titre_hor.png",43,95.5,3);
    $pdf->Image($path2."/images/titre_hor.png",135,97.5,10.2);
    $pdf->Image($path2."/images/titre_ver.png",45,165.5,100.2);
    $pdf->Image($path2."/images/droite_en.png",37,81,113);
    $pdf->SetXY(89, 112);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($soiTotal),0),0,1,'L',0);
    $pdf->SetXY(112, 132);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($maitriseTotal),0),0,1,'L',0);
    $pdf->SetXY(89, 160);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($autruiTotal),0),0,1,'L',0);
    $pdf->SetXY(61, 132);
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($sympathieTotal),0),0,1,'L',0);

    //////////////////////////////////////////////////////

    ///////////////////// PAGE 8 ///////////////////////////

    $tplIdx = $pdf -> importPage(8);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(8, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('Bartender-SemiCondensedSerif','',18);

    // SERIEUX
    $pdf->SetXY(49.1, 83);
    $pdf->SetFillColor(140,173,219);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($serieuxTotal,0)*17)/19.5)-2.5),7, number_format($serieuxTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 83);
    $pdf->SetFillColor(117,142,172);
    $pdf->Cell((((number_format($serieuxTotal,0)*17)/19.5)-2.5),0.5,'',0,1,'R',true);

    // ENJOUE
    $pdf->SetXY(49.1, 94);
    $pdf->SetFillColor(255,201,42);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($enjouTotal,0)*17)/19.5)-2.5),7, number_format($enjouTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 94);
    $pdf->SetFillColor(196,158,56);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($enjouTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true);

    // CONFORME
    $pdf->SetXY(49.1, 105);
    $pdf->SetFillColor(91,95,96);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($conformTotal,0)*17)/19.5)-2.5),7, number_format($conformTotal,1),0,1,'R',true); 
    $pdf->SetXY(49.1, 105);
    $pdf->SetFillColor(76,81,81);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($conformTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true); 

    // TRANASGRESSIF
    $pdf->SetXY(49.1, 116);
    $pdf->SetFillColor(165,168,169);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($transagressiTotal,0)*17)/19.5)-2.5),7, number_format($transagressiTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 116);
    $pdf->SetFillColor(133,138,137);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell((((number_format($transagressiTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true);

    // SYMPATHIE
    $pdf->SetXY(49.1, 127);
    $pdf->SetFillColor(125,129,130);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($sympathieTotal,0)*17)/19.5)-2.5),7, number_format($sympathieTotal,1),0,1,'R',true); 
    $pdf->SetXY(49.1, 127);
    $pdf->SetFillColor(104,109,109);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($sympathieTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true); 

    // MAITRISE
    $pdf->SetXY(49.1, 138);
    $pdf->SetFillColor(190,192,192);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*17)/19.5)-2.5),7, number_format($maitriseTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 138);
    $pdf->SetFillColor(150,153,153);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*17)/19.5)-2.5),0.5,'',0,1,'R',true);

    // SOI
    $pdf->Image($path2."/images/self.png",16.4,147.9,26.9);
    $pdf->SetXY(49.1, 149);
    $pdf->SetFillColor(246,156,157);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($soiTotal,0)*17)/19.5)-2.5),7, number_format($soiTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 149);
    $pdf->SetFillColor(191,125,129);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($soiTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true);

    // AUTRUI
    $pdf->Image($path2."/images/other.png",12.5,159.5,32);
    $pdf->SetXY(49.1, 160);
    $pdf->SetFillColor(166,212,153);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*17)/19.5)-2.5),7, number_format($autruiTotal,1),0,1,'R',true);
    $pdf->SetXY(49.1, 160);
    $pdf->SetFillColor(131,169,126);
    $pdf->SetTextColor(255,255,255);
    //17 (entre 1 à 20 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*17)/19.5)-2.5),0.5, '',0,1,'R',true);

    ////////////////////////////////////////////////////////  


    /////////////////////////// PAGE 9 ////////////////////
    $pdf->setSourceFile($path.'isma/isma_en_vrai.pdf');
    $tplIdx = $pdf -> importPage(9);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(9, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php'); 
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->SetFont('Bartender-SemiCondensedSerif','',19);
    $pdf->Image($path2."/images/carre_repere2.png",77.5,124.5,13.8);
    $pdf->Image($path2."/images/carre_repere2.png",77.5,142.5,13.8);
    $pdf->Image($path2."/images/carre_repere2.png",77.5,159.5,13.8);
    $pdf->Image($path2."/images/carre_repere2.png",77.5,176.5,13.8);
    $pdf->Image($path2."/images/carre_repere2.png",64,194,13.8);
    $pdf->Image($path2."/images/carre_repere2.png",85,196,7.8);
    $pdf->Image($path2."/images/carre_repere2.png",100.5,196,7.8);
    $pdf->Image($path2."/images/carre_repere2.png",115.5,196,7.8);
    // ENJOUE / SERIEUX
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(140,173,219);
    $pdf->SetTextColor(255,255,255);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($serieuxTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(117,142,172);
    $pdf->Cell(-(((number_format($serieuxTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(255,201,42);
    $pdf->SetTextColor(255,255,255);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($enjouTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 124);
    $pdf->SetFillColor(196,158,56);
    $pdf->Cell((((number_format($enjouTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    // LE TEXT
    $pdf->SetXY(79, 128);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0,  number_format(($serieuxTotal-$enjouTotal),0) > 0 ? number_format(($serieuxTotal-$enjouTotal),0) : number_format((abs($serieuxTotal-$enjouTotal)),0),0,1,'L',0);

    // CONFORME / TRANSGRESSIF
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(91,95,96);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($conformTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(76,81,81); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($conformTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(167,170,171);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 142);
    $pdf->SetFillColor(133,138,137); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($transagressiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 145.5);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($conformTotal-$transagressiTotal),0) > 0 ? number_format(($conformTotal-$transagressiTotal),0) : number_format((abs($conformTotal-$transagressiTotal)),0),0,1,'L',0);

    // SYMPATHIE / MAITRISE
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(125,129,130);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($sympathieTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(104,109,109); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($sympathieTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(190,192,192);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 160);
    $pdf->SetFillColor(150,153,153); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($maitriseTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 163);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($maitriseTotal-$sympathieTotal),0) > 0 ? number_format(($maitriseTotal-$sympathieTotal),0) : number_format((abs($maitriseTotal-$sympathieTotal)),0),0,1,'L',0);

    // SOI / AUTRUI
    /*$pdf->Image($path2."/images/self.png",46.5,176.5,24.8);
    $pdf->Image($path2."/images/bar.png",54.8,174.7,5.2);
    $pdf->Image($path2."/images/other.png",22.8,176.7,32);*/
    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(246,156,157);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($soiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(191,125,129);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell(-(((number_format($soiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(166,212,153);
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*15.5)/50)-2.5),7, "",0,1,'R',true);
    $pdf->SetXY(139, 178);
    $pdf->SetFillColor(131,169,126); 
    //15.5 (entre 1 à 50 par bar)
    $pdf->Cell((((number_format($autruiTotal,0)*15.5)/50)-2.5),0.5, "",0,1,'R',true);

    $pdf->SetXY(79, 180.5);
    $pdf->SetTextColor(125,129,130);
    $pdf->Cell(15,0, number_format(($autruiTotal-$soiTotal),0) > 0 ? number_format(($autruiTotal-$soiTotal),0) : number_format(abs(($autruiTotal-$soiTotal)),0),0,1,'L',0);    

    ////////////////////////////////////////////////////////////////////////////

    ////////////// PAGE 10 ////////////////////////////////////
    $pdf->setSourceFile($path.'isma/en_isma.pdf');
    $tplIdx = $pdf -> importPage(10);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(10, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    // SERIEUX / ENJOUE
    $pdf->Image($path2."/images/ser_enjoue.png",174.5,130,((number_format(($serieuxTotal+$enjouTotal),0)*25)/100),6);
    $pdf->SetXY(178.5+((number_format(($serieuxTotal+$enjouTotal),0)*25)/100), 133);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($serieuxTotal+$enjouTotal,0),0,1,'L',0);

    // CONFORME / TRANSGRESSIF
    $pdf->Image($path2."/images/conf_trans.png",174.5,147,((number_format(($conformTotal+$transagressiTotal),0)*25)/100),6);
    $pdf->SetXY(178.5+((number_format(($conformTotal+$transagressiTotal),0)*25)/100), 150);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($conformTotal+$transagressiTotal,0),0,1,'L',0);

    // SYMPATHIE / MAITRISE
    $pdf->Image($path2."/images/symp_maitri.png",174.5,164,((number_format(($sympathieTotal+$maitriseTotal),0)*25)/100),6);
    $pdf->SetXY(178.5+((number_format(($sympathieTotal+$maitriseTotal),0)*25)/100), 167);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($sympathieTotal+$maitriseTotal,0),0,1,'L',0);

    // SOI / AUTRUI 
    $pdf->Image($path2."/images/soi_autr.png",174.5,181,((number_format(($soiTotal+$autruiTotal),0)*25)/100),6);
    $pdf->SetXY(178.5+((number_format(($soiTotal+$autruiTotal),0)*25)/100), 184);
    $pdf->SetTextColor(166,169,169);
    $pdf->Cell(15,0, number_format($soiTotal+$autruiTotal,0),0,1,'L',0);

    //////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 11 //////////////////////////////////////////
    
    $tplIdx = $pdf -> importPage(11);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(11, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    /////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////// PAGE 12 //////////////////////////////////////////
    $pageCount = $pdf->setSourceFile($path.'isma/isma_en_ancien.pdf');
    $tplIdx = $pdf -> importPage(12);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId); 

    $alldates = ReponseRepondant::where("test_id",$id)->where("repondant_id",$repondant->id)->get();
    $datePerMonth = array();

    /// TOUS LES TESTS PAR MOIS
    foreach ($alldates as $key => $date) {
      $d = explode(" ", $date->created_at);
      if(!in_array($d[0],$datePerMonth)){
        array_push($datePerMonth, $d[0]);
      }
    }

    $x = 47;
    $y = 115.5;

    $x2 = 37;
    $y2 = 112.7;

    $y3 = 198.5;
    $y4 = 195.5;

    $arrayEnjoue = array();
    $arrayEnjouey = array();

    $arraySerieux = array();
    $arraySerieuxy = array();

    $arrayTrans = array();
    $arrayTransy = array();

    $arrayConform = array();
    $arrayConformy = array();

    $arraySoi = array();
    $arraySoiy = array();

    $arrayAutrui = array();
    $arrayAutruiy = array();

    $arrayMait = array();
    $arrayMaity = array();

    $arraySymp = array();
    $arraySympy = array();

    $months = array();
    $months2 = array();
    
    foreach ($datePerMonth as $key => $val) {
      $mois = explode("-", $val);
      $month = $this->getMonth($mois[1],"en");
      array_push($months, $month." ".$mois[0]);
      $month2 = $this->getMonth2($mois[1],"en");
      array_push($months2, $month2." ".$mois[0]);
      
      $serieuxTotal2 = 0;
      $enjouTotal2 = 0;
      $conformTotal2 = 0;
      $transagressiTotal2 = 0;

      $autruiTotal2 = 0;
      $soiTotal2 = 0;
      $maitriseTotal2 = 0;
      $sympathieTotal2 = 0;

      // SCORE SERIEUX
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();

      foreach ($valeurs2 as $key => $value) {
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $serieuxTotal2 = $serieuxTotal2 + $score->score;
      }

      if($serieuxTotal2 >= 0)
        $serieuxTotal2 = ((($serieuxTotal2 - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
      
      // SCORE ENJOUE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $enjouTotal2 = $enjouTotal2 + $score->score;
      }
      if($enjouTotal2 >= 0)
        $enjouTotal2 = ((($enjouTotal2 - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

      // SCORE CONFORME
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $conformTotal2 = $conformTotal2 + $score->score;
      }
      if($conformTotal2 >= 0)
        $conformTotal2 = ((($conformTotal2 - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

      // SCORE TRANSGRESSIF
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $transagressiTotal2 = $transagressiTotal2 + $score->score;
      }
      if($transagressiTotal2 >= 0)
        $transagressiTotal2 = ((($transagressiTotal2 - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

      // SCORE AUTRUI
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $autruiTotal2 = $autruiTotal2 + $score->score;
      }
      if($autruiTotal2 >= 0){
        $autruiTotal2 = ($autruiTotal2 / 2);
        $autruiTotal2 = ((($autruiTotal2 - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
      }

      // SCORE SOI
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $soiTotal2 = $soiTotal2 + $score->score;
      }
      if($soiTotal2 >= 0){
        $soiTotal2 = ($soiTotal2 / 2);
        $soiTotal2 = ((($soiTotal2 - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
      }

      // SCORE MAITRISE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $maitriseTotal2 = $maitriseTotal2 + $score->score;
      }
      if($maitriseTotal2 >= 0){
        $maitriseTotal2 = ($maitriseTotal2 / 2);
        $maitriseTotal2 = ((($maitriseTotal2 - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
      }

      // SCORE SYMPHATIE
      $valeurs2 = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$val."%")->get();
      foreach ($valeurs2 as $key => $value) {
        //var_dump($value->reponse_id);
        $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
        $sympathieTotal2 = $sympathieTotal2 + $score->score;
      }
      if($sympathieTotal2 >= 0){
        $sympathieTotal2 = ($sympathieTotal2 / 2);
        $sympathieTotal2 = ((($sympathieTotal2 - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
      }

      $arrayMonths[$month2." ".$mois[0]] = array(number_format($serieuxTotal2),
        number_format($enjouTotal2),
        number_format($conformTotal2),
        number_format($transagressiTotal2),
        number_format($maitriseTotal2),
        number_format($sympathieTotal2),
        number_format($autruiTotal2),
        number_format($soiTotal2)
      );

      // SERIEUX
      $pdf->SetXY(47, $y);
      $pdf->SetFillColor(140,173,219);
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true);
      $pdf->SetXY(47, $y);
      $pdf->SetFillColor(117,142,172);     
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);     

      // ENJOUE
      $pdf->SetXY(47, $y2);
      $pdf->SetFillColor(255,201,42);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y2);
      $pdf->SetFillColor(196,158,56);     
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(38, $y2+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0);

      array_push($arrayEnjoue,(((number_format($enjouTotal2,0)*20)/50))+1.5);
      array_push($arrayEnjouey,number_format($y2,0));

      array_push($arraySerieux,(((number_format($serieuxTotal2,0)*20)/50))+1.5);
      array_push($arraySerieuxy,number_format($y,0));

      // TRANGRESSIF
      $pdf->SetXY(175.5, $y2);
      $pdf->SetFillColor(165,168,169);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y2);
      $pdf->SetFillColor(133,138,137);     
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);  
      
      // CONFORME
      $pdf->SetXY(175.5, $y);
      $pdf->SetFillColor(91,95,96);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true);
      $pdf->SetXY(175.5, $y);
      $pdf->SetFillColor(76,81,81);     
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(166.5, $y2+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0);

      array_push($arrayTrans,(((number_format($transagressiTotal2,0)*20)/50))+1.5);
      array_push($arrayTransy,number_format($y2,0));

      array_push($arrayConform,(((number_format($conformTotal2,0)*20)/50))+1.5);
      array_push($arrayConformy,number_format($y,0));

      // AUTRUI
      $pdf->SetXY(47, $y3);
      $pdf->SetFillColor(166,212,153);
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y3);
      $pdf->SetFillColor(131,169,126);     
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      // SOI
      $pdf->SetXY(47, $y4);
      $pdf->SetFillColor(246,156,157);
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(47, $y4);
      $pdf->SetFillColor(191,125,129);     
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true); 

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(38, $y4+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0); 
      
      array_push($arraySoi,(((number_format($soiTotal2,0)*20)/50))+1.5);
      array_push($arraySoiy,number_format($y4,0));

      array_push($arrayAutrui,(((number_format($autruiTotal2,0)*20)/50))+1.5);
      array_push($arrayAutruiy,number_format($y3,0));

      // MAITRISE
      $pdf->SetXY(175.5, $y3);
      $pdf->SetFillColor(190,192,192);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y3);
      $pdf->SetFillColor(150,153,153);     
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);

      // SYMPATHIE
      $pdf->SetXY(175.5, $y4);
      $pdf->SetFillColor(125,129,130);
      $pdf->SetTextColor(255,255,255);
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,2, "",0,1,'R',true); 
      $pdf->SetXY(175.5, $y4);
      $pdf->SetFillColor(104,109,109);     
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,0.5, "",0,1,'R',true);      

      $pdf->SetFont('Bartender-SemiCondensedSerif','',10);
      $pdf->SetXY(166.5, $y4+2);
      $pdf->SetTextColor(125,128,129);
      $str = mb_strtoupper($month, 'UTF-8');
      $pdf->Cell(10,0,iconv("UTF-8", "windows-1252",$str),0,1,'L',0); 

      array_push($arraySymp,(((number_format($sympathieTotal2,0)*20)/50))+1.5);
      array_push($arraySympy,number_format($y4,0));

      array_push($arrayMait,(((number_format($maitriseTotal2,0)*20)/50))+1.5);
      array_push($arrayMaity,number_format($y3,0));

      // TEXT SERIEUX
      $pdf->SetFont('Bartender-SemiCondensedSerif','',9);
      $pdf->SetXY(55, $y+0.5);
      $pdf->SetTextColor(117,142,172);     
      $pdf->Cell((((number_format($serieuxTotal2,0)*20)/50)),0.5, number_format($serieuxTotal2,0),0,1,'R',0);

      // TEXT ENJOUE
      $pdf->SetXY(55, $y2+1);
      $pdf->SetTextColor(255,201,42);     
      $pdf->Cell((((number_format($enjouTotal2,0)*20)/50)),0.5, number_format($enjouTotal2,0),0,1,'R',0);

      // TEXT TRANSGRESSIF
      $pdf->SetXY(182.5, $y2+1);
      $pdf->SetTextColor(165,168,169);    
      $pdf->Cell((((number_format($transagressiTotal2,0)*20)/50))+1.5,0.5, number_format($transagressiTotal2,0),0,1,'R',0);

      // TEXT CONFORME
      $pdf->SetXY(182.5, $y+1);
      $pdf->SetTextColor(91,95,96);    
      $pdf->Cell((((number_format($conformTotal2,0)*20)/50))+1.5,0.5, number_format($conformTotal2,0),0,1,'R',0);         
      // TEXT AUTRUI
      $pdf->SetXY(55, $y3+0.5);
      $pdf->SetTextColor(166,212,153);     
      $pdf->Cell((((number_format($autruiTotal2,0)*20)/50)),0.5, number_format($autruiTotal2,0),0,1,'R',0);

      // TEXT SOI
      $pdf->SetXY(55, $y4+0.5);
      $pdf->SetTextColor(246,156,157);     
      $pdf->Cell((((number_format($soiTotal2,0)*20)/50)),0.5, number_format($soiTotal2,0),0,1,'R',0);

      // TEXT MAITRISE
      $pdf->SetXY(182.5, $y3+1);
      $pdf->SetTextColor(190,192,192);    
      $pdf->Cell((((number_format($maitriseTotal2,0)*20)/50))+1.5,0.5, number_format($maitriseTotal2,0),0,1,'R',0);

      // TEXT SYMPATHIE
      $pdf->SetXY(182.5, $y4+1);
      $pdf->SetTextColor(125,129,130);    
      $pdf->Cell((((number_format($sympathieTotal2,0)*20)/50))+1.5,0.5, number_format($sympathieTotal2,0),0,1,'R',0);

      $y = $y - 6.5;
      $y2 = $y2 - 6.5;

      $y3 = $y3 - 6.5;
      $y4 = $y4 - 6.5;  

    }

    // AFFICHER LE MOIS DE DEBUT ET FIN
    $pdf->SetFont('Lato-Regular_0','',18);
    $pdf->SetXY(170.5, 39.5);
    $pdf->SetTextColor(255,204,51);  
    $str = current($months)." - ".end($months);
    $str = mb_strtoupper($str, 'UTF-8');  
    $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$str.")"),0,1,'R',0);

    // DESSINER LES LIGNES HORIZONTALES

    for ($i=1; $i <= 10; $i++) { 
      if(isset($arrayEnjoue[$i]) && isset($arraySerieux[$i])){
        // ENJOUE
        $pdf->SetDrawColor(196,158,56);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayEnjoue[$i-1],0)+46.5, $arrayEnjouey[$i-1], number_format($arrayEnjoue[$i],0)+46.5, $arrayEnjouey[$i]);

        // SERIEUX
        $pdf->SetDrawColor(117,142,172);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySerieux[$i-1],0)+46.5, $arraySerieuxy[$i-1], number_format($arraySerieux[$i],0)+46.5, $arraySerieuxy[$i]);

        // TRANSGRESSIF
        $pdf->SetDrawColor(133,138,137);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayTrans[$i-1],0)+175, $arrayTransy[$i-1], number_format($arrayTrans[$i],0)+175, $arrayTransy[1]); 

        // CONFORME
        $pdf->SetDrawColor(76,81,81);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayConform[$i-1],0)+175, $arrayConformy[$i-1], number_format($arrayConform[$i],0)+175, $arrayConformy[$i]);

        // AUTRUI
        $pdf->SetDrawColor(166,212,153);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayAutrui[$i-1],0)+46.5, $arrayAutruiy[$i-1], number_format($arrayAutrui[$i],0)+46.5, $arrayAutruiy[$i]);

        // SOI
        $pdf->SetDrawColor(246,156,157);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySoi[$i-1],0)+46.5, $arraySoiy[$i-1], number_format($arraySoi[$i],0)+46.5, $arraySoiy[$i]);

        // MAITRISE
        $pdf->SetDrawColor(150,153,153);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arrayMait[$i-1],0)+175, $arrayMaity[$i-1], number_format($arrayMait[$i],0)+175, $arrayMaity[$i]);

        // SYMPATHIE
        $pdf->SetDrawColor(104,109,109);
        $pdf->SetLineWidth(0.6);
        $pdf->Line(number_format($arraySymp[$i-1],0)+175, $arraySympy[$i-1], number_format($arraySymp[$i],0)+175, $arraySympy[$i]);        
      }      
    }

    /////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////// PAGE 13 ///////////////////////////////////////////////

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    
    $cur = 0;
    $x = 26.5;
    $y = 70.5;
    $x3 = 26.5;
    $y3 = 70.5;
    $xmois = 65.5;
    $xmois3 = 65.5;
    foreach ($arrayMonths as $key => $value) {
      if($cur < 3){

        // AFFICHER LE MOIS
        $pdf->SetFont('GOTHICI','',12);
        $pdf->SetXY($xmois, 61.5);
        $pdf->SetTextColor(167,169,171);    
        $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0); 

        // SERIEUX
        $pdf->SetFont('Bartender-SemiCondensedSerif','',9.5);
        $pdf->SetXY($x, $y);
        $pdf->SetFillColor(140,173,219);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y);
        $pdf->SetFillColor(117,142,172);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2,'',0,1,'R',true) : "";

        // ENJOUE
        $pdf->SetXY($x, $y+5);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+5);
        $pdf->SetFillColor(196,158,56);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // CONFORME
        $pdf->SetXY($x, $y+10);
        $pdf->SetFillColor(91,95,96);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+10);
        $pdf->SetFillColor(76,81,81);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // TRANSGRESSIF
        $pdf->SetXY($x, $y+15);
        $pdf->SetFillColor(165,168,169);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+15);
        $pdf->SetFillColor(133,138,137);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SYMPATHIE
        $pdf->SetXY($x, $y+20);
        $pdf->SetFillColor(125,129,130);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+20);
        $pdf->SetFillColor(104,109,109);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // MAITRISE
        $pdf->SetXY($x, $y+25);
        $pdf->SetFillColor(190,192,192);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+25);
        $pdf->SetFillColor(150,153,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SOI
        $pdf->SetXY($x, $y+30.5);
        $pdf->SetFillColor(246,156,157);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+30.5);
        $pdf->SetFillColor(191,125,129);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // AUTRUI
        $pdf->SetXY($x, $y+35.5);
        $pdf->SetFillColor(166,212,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
        $pdf->SetXY($x, $y+35.5);
        $pdf->SetFillColor(131,169,126);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";       

        $x = $x+97;
        $xmois = $xmois+96;

      } else {
        $y3 = $y3 + 71;

        // AFFICHER LE MOIS
        $pdf->SetFont('GOTHICI','',12);
        $pdf->SetXY($xmois3, 134.5);
        $pdf->SetTextColor(167,169,171);    
        $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

        // SERIEUX
        $pdf->SetFont('Bartender-SemiCondensedSerif','',9.5);
        $pdf->SetXY($x3, $y3);
        $pdf->SetFillColor(140,173,219);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[0]) ? $pdf->Cell((((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // ENJOUE
        $pdf->SetXY($x3, $y3+5);
        $pdf->SetFillColor(255,201,42);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+5);
        $pdf->SetFillColor(196,158,56);
        $pdf->SetTextColor(255,255,255);
        isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // CONFORME
        $pdf->SetXY($x3, $y3+10);
        $pdf->SetFillColor(91,95,96);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+10);
        $pdf->SetFillColor(76,81,81);
        $pdf->SetTextColor(255,255,255);
        isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // TRANSGRESSIF
        $pdf->SetXY($x3, $y3+15);
        $pdf->SetFillColor(165,168,169);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+15);
        $pdf->SetFillColor(133,138,137);
        $pdf->SetTextColor(255,255,255);
        isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SYMPATHIE
        $pdf->SetXY($x3, $y3+20);
        $pdf->SetFillColor(125,129,130);
        $pdf->SetTextColor(255,255,255);
        isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+20);
        $pdf->SetFillColor(104,109,109);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // MAITRISE
        $pdf->SetXY($x3, $y3+25);
        $pdf->SetFillColor(190,192,192);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+25);
        $pdf->SetFillColor(150,153,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // SOI
        $pdf->SetXY($x3, $y3+30.5);
        $pdf->SetFillColor(246,156,157);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+30.5);
        $pdf->SetFillColor(191,125,129);
        $pdf->SetTextColor(255,255,255);
        isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

        // AUTRUI
        $pdf->SetXY($x3, $y3+35.5);
        $pdf->SetFillColor(166,212,153);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
        $pdf->SetXY($x3, $y3+35.5);
        $pdf->SetFillColor(131,169,126);
        $pdf->SetTextColor(255,255,255);
        isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";        

        $xmois3 = $xmois3+96;
        $x3 = $x3 + 97;
      }
      $cur=$cur+1;
    }

    //AFFICHER LE MOIS DE DEBUT ET FIN
    $pdf->SetFont('Lato-Regular_0','',18);
    $pdf->SetXY(170.5, 39.5);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(255,204,51);   
    $str = $months[0]." - ".end($months);
    $str = mb_strtoupper($str, 'UTF-8'); 
    $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$str.")"),0,1,'R',0);

    /////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////// PAGE 14 ////////////////////////////////////////////////

    $tplIdx = $pdf -> importPage(14);
    $size = $pdf->getTemplateSize($tplIdx);
    //if(count($arrayMonths)>5){
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
   

      // AFFICHER LE MOIS DE DEBUT ET FIN
      $pdf->SetFont('Lato-Regular_0','',18);
      $pdf->SetXY(170.5, 39.5);
      $pdf->SetTextColor(255,201,42);    
      $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", "(".$months[0]." - ".end($months).")"),0,1,'R',0);

      $cur = 0;
      $x = 64.8;
      $y = 64.5;
      $x3 = 64.8;
      $xmois = 98.5;
      $xmois3 = 98.5;
      foreach ($arrayMonths as $key => $value) {
        if($cur >=6 && $cur < 8){
          // SERIEUX
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x, $y);
          $pdf->SetFillColor(140,173,219);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x, $y);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // ENJOUE
          $pdf->SetXY($x, $y+5);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+5);
          $pdf->SetFillColor(196,158,56);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // CONFORME
          $pdf->SetXY($x, $y+10.5);
          $pdf->SetFillColor(91,95,96);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+10.5);
          $pdf->SetFillColor(76,81,81);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // TRANSGRESSIF
          $pdf->SetXY($x, $y+16);
          $pdf->SetFillColor(165,168,169);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+16);
          $pdf->SetFillColor(133,138,137);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SYMPATHIE
          $pdf->SetXY($x, $y+21);
          $pdf->SetFillColor(125,129,130);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+21);
          $pdf->SetFillColor(104,109,109);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";
          // MAITRISE
          $pdf->SetXY($x, $y+26);
          $pdf->SetFillColor(190,192,192);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+26);
          $pdf->SetFillColor(150,153,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SOI
          $pdf->SetXY($x, $y+31);
          $pdf->SetFillColor(246,156,157);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+31);
          $pdf->SetFillColor(191,125,129);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AUTRUI
          $pdf->SetXY($x, $y+36);
          $pdf->SetFillColor(166,212,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
          $pdf->SetXY($x, $y+36);
          $pdf->SetFillColor(131,169,126);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2,'',0,1,'R',true) : "";

          // AFFICHER LE MOIS
          $pdf->SetFont('RobotoCondensed-Regular','',14);
          $pdf->SetXY($xmois, 58.5);
          $pdf->SetTextColor(167,169,171);    
          $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

          $x = $x+111.5;
          $xmois = $xmois+116;
        } else if($cur >= 8) {
          // SERIEUX
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x3, 126);
          $pdf->SetFillColor(140,173,219);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),3, number_format($value[0],0),0,1,'R',true) : "";
          $pdf->SetFont('RobotoCondensed-Bold','',9.5);
          $pdf->SetXY($x3, 126);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[0]) ? $pdf->Cell( (((number_format($value[0],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // ENJOUE
          $pdf->SetXY($x3, 126+5);
          $pdf->SetFillColor(255,201,42);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),3, number_format($value[1],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+5);
          $pdf->SetFillColor(196,158,56);
          $pdf->SetTextColor(255,255,255);
          isset($value[1]) ? $pdf->Cell( (((number_format($value[1],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // CONFORME
          $pdf->SetXY($x3, 126+10.5);
          $pdf->SetFillColor(91,95,96);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),3, number_format($value[2],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+10.5);
          $pdf->SetFillColor(76,81,81);
          $pdf->SetTextColor(255,255,255);
          isset($value[2]) ? $pdf->Cell( (((number_format($value[2],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // TRANSGRESSIF
          $pdf->SetXY($x3, 126+16);
          $pdf->SetFillColor(165,168,169);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),3, number_format($value[3],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+16);
          $pdf->SetFillColor(133,138,137);
          $pdf->SetTextColor(255,255,255);
          isset($value[3]) ? $pdf->Cell( (((number_format($value[3],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SYMPATHIE
          $pdf->SetXY($x3, 126+21);
          $pdf->SetFillColor(125,129,130);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),3, number_format($value[5],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+21);
          $pdf->SetFillColor(104,109,109);
          $pdf->SetTextColor(255,255,255);
          isset($value[5]) ? $pdf->Cell( (((number_format($value[5],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // MAITRISE
          $pdf->SetXY($x3, 126+26);
          $pdf->SetFillColor(190,192,192);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),3, number_format($value[4],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+26);
          $pdf->SetFillColor(150,153,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[4]) ? $pdf->Cell( (((number_format($value[4],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // SOI
          $pdf->SetXY($x3, 126+31);
          $pdf->SetFillColor(246,156,157);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),3, number_format($value[7],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+31);
          $pdf->SetFillColor(191,125,129);
          $pdf->SetTextColor(255,255,255);
          isset($value[7]) ? $pdf->Cell( (((number_format($value[7],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AUTRUI
          $pdf->SetXY($x3, 126+36);
          $pdf->SetFillColor(166,212,153);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),3, number_format($value[6],0),0,1,'R',true) : "";
          $pdf->SetXY($x3, 126+36);
          $pdf->SetFillColor(131,169,126);
          $pdf->SetTextColor(255,255,255);
          isset($value[6]) ? $pdf->Cell( (((number_format($value[6],0)*7.5)/20)+2),0.2, '',0,1,'R',true) : "";

          // AFFICHER LE MOIS
          $pdf->SetFont('RobotoCondensed-Regular','',14);
          $pdf->SetXY($xmois3, 120.5);
          $pdf->SetTextColor(167,169,171);    
          $pdf->Cell(10,2, iconv("UTF-8", "windows-1252", $key),0,1,'R',0);

          $xmois3 = $xmois3+116.5;
          $x3 = $x3 + 111.6;
        }
        $cur = $cur+1;    
      }
    //}

    ////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 15 //////////////////////////////////////////

    $pageCount = $pdf->setSourceFile($path.'isma/isma_en_ancien.pdf');
    $tplIdx = $pdf -> importPage(12);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    ///////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////// PAGE 16 //////////////////////////////////////////
    $tplIdx = $pdf -> importPage(16);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->Image($path2."/".$test->id."/gauche.png",54,94.3,85);
    $pdf->Image($path2."/images/titre_ver.png",69,152,120);
    $pdf->Image($path2."/".$test->id."/droite.png",166.5,101.8,66);
    $pdf->Image($path2."/images/titre_ver.png",165,147,120);
    $pdf->Image($path2."/images/titre_ver.png",165,99,120);

    $pdf->Image($path2."/images/lunetteen1.png",35,43,225);
    $pdf->Image($path2."/images/petit_carre.png",99,99,10);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    $key = array_search($value, $array);
    if($value == $array["1"]){
      $pdf->SetXY(100, 116);
    } else if($value == $array["2"]){
      $pdf->SetXY(100, 133);
    } else if($value == $array["3"]){
      $pdf->SetXY(70, 133);
    } else {
      $pdf->SetXY(70, 116);
    }
    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);
    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(172, 116);
    } else if($value == $array2["2"]){
      $pdf->SetXY(202, 116);
    } else if($value == $array2["3"]){
      $pdf->SetXY(202, 133);
    } else {
      $pdf->SetXY(172, 133);
    }

    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);

    ///////////////////////////////////////////////////////////////////////////////////


    /////////////////////////// PAGE 17 ////////////////////////////////////////////

    $tplIdx = $pdf -> importPage(17);
    $size = $pdf->getTemplateSize($tplIdx);
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->Image($path2."/".$test->id."/gauche.png",54,94.3,85);
    $pdf->Image($path2."/images/titre_ver.png",69,152,120);
    $pdf->Image($path2."/".$test->id."/droite.png",166.5,101.8,66);
    $pdf->Image($path2."/images/titre_ver.png",165,147,120);
    $pdf->Image($path2."/images/titre_ver.png",165,99,120);

    $pdf->Image($path2."/images/lunetteen1.png",35,43,225);
    $pdf->Image($path2."/images/petit_carre.png",99,99,10);

    $pdf->AddFont('Bartender-SemiCondensedSerif','','Bartender-SemiCondensedSerif.php'); 
    $pdf->SetFont('Bartender-SemiCondensedSerif','',26);

    // POUR AFFICHER LE POSISTION DE LA VALEUR SELON LE MAXIMUM
    $array = array();
    $array["1"] = $serieuxTotal+$transagressiTotal;
    $array["2"] = $transagressiTotal+$enjouTotal;
    $array["3"] = $conformTotal+$enjouTotal;
    $array["4"] = $conformTotal+$serieuxTotal;
    $value = max($array);
    $key = array_search($value, $array);
    if($value == $array["1"]){
      $pdf->SetXY(100, 116);
    } else if($value == $array["2"]){
      $pdf->SetXY(100, 133);
    } else if($value == $array["3"]){
      $pdf->SetXY(70, 133);
    } else {
      $pdf->SetXY(70, 116);
    }
    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($gauche),1)."%",0,1,'L',0); 

    $array2 = array();
    $array2["1"] = $soiTotal+$sympathieTotal;
    $array2["2"] = $soiTotal+$maitriseTotal;
    $array2["3"] = $maitriseTotal+$autruiTotal;
    $array2["4"] = $autruiTotal+$sympathieTotal;
    $value = max($array2);
    $key = array_search($value, $array2);
    if($value == $array2["1"]){
      $pdf->SetXY(172, 116);
    } else if($value == $array2["2"]){
      $pdf->SetXY(202, 116);
    } else if($value == $array2["3"]){
      $pdf->SetXY(202, 133);
    } else {
      $pdf->SetXY(172, 133);
    }

    $pdf->SetTextColor(32,33,33);
    $pdf->Cell(15,0,number_format(($droite),1)."%",0,1,'L',0);    

    ////////////////////////////////////////////////////////////////////////////////// 


    /////////////////////////////////// PAGE 18 ///////////////////////////////////////

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);    

    //////////////////////////////////////////////////////////////////////////////////  

    //$pdf->Output($txtRa.".pdf","I"); 

    $txtRa = "ISMA REPORT ENG 2019-".iconv('ISO-8859-1', 'UTF-8//IGNORE', $month."-".str_replace(" ","-",$repondant->username));

    if($genre == "pdf"){
      $pdf->Output($txtRa.".pdf","I");
      $this->deleteFolder($path2."/".$test->id."/");
    } else {
      $path = base_path().'/public/templates/isma/';
      $target_dir = $path."/archive/".$test->id."_".$mois[1];
      if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
      }
      $pdf->Output($path."archive/pptx".$test->id."_".$mois[1].".pdf",'F');

      $pdf = new PDF_Diag();
      $pagecount = $pdf->setSourceFile($path."archive/pptx".$test->id."_".$mois[1].".pdf");

      $target_dir2 = $path."archive/liste_images_".$test->id."_".$mois[1];
      if (!file_exists($target_dir2)) {
        mkdir($target_dir2, 0777, true);
      }

      // Split each page into a new PDF
      for ($i = 1; $i <= $pagecount; $i++) {
        $new_pdf = new PDF_Diag();
        $new_pdf->AddPage();
        $new_pdf->setSourceFile($path."archive/pptx".$test->id."_".$mois[1].".pdf");
        $new_pdf->useTemplate($new_pdf->importPage($i), null, null, $size["width"], $size["height"],TRUE);
        
        try {
          $new_filename = $path."archive/pptx".$test->id."_".$mois[1]."_".$i.".pdf";
          $new_pdf->Output($new_filename, "F");
          
          ini_set('max_execution_time', 400);
          $array=array();
          $count = $i-1;
          exec("convert -density 200  -colorspace sRGB ".$new_filename." -quality 60 ".$path."archive/liste_images_".$test->id."_".$mois[1]."/ppt-".$count.".png", $array);

        } catch (Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
      }

      copy($path.'isma_en2.pptx',$path."archive/".$test->id."_".$mois[1].".zip");
      $filename = $path."archive/".$test->id."_".$mois[1].".zip";
      $zip = new ZipArchive;
      $res = $zip->open($filename);

      if ($res === TRUE) {
        $newpath = $path."archive/".$test->id."_".$mois[1];
        // Extract file
        $zip->extractTo($newpath);
        $zip->close();

        /////////////// ALL - SLIDES  ////////////////////////////////////////////

        $count = 3;
        for ($i=0; $i < 17; $i++) { 
          copy($path."archive/liste_images_".$test->id."_".$mois[1]."/ppt-".$i.".png",$path."archive/".$test->id."_".$mois[1]."/ppt/media/image".$count.".png");
          $count++; 
        }
        //////////////////////////////////////////////////////////////////////

        header('Content-Type: text/html; charset=ISO-8859-1');
        $this->zipFolder($zip,$newpath,$path."archive/".$test->id."_".$mois[1]."/");
        if(!is_dir(base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx") == true){
          mkdir(base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx",0777,true);   
        }
        $file_url = url("/uploads/isma/".$test->id."_".$mois[1]."/pptx");
        $dir = base_path()."/public/uploads/isma/".$test->id."_".$mois[1]."/pptx/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");
        copy($path."archive/".$test->id."_".$mois[1].'/file.zip', $dir);

        if(file_exists($dir)){
          $this->deleteFolder($path."archive");
          $this->deleteFolder($path2."/".$test->id);
          copy($path.'backup.pptx',$path.'isma_en2.pptx');
          $response["generated"] = true;
          $response["doc"] = $file_url."/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");;
          echo json_encode($response);
        }
      }
    }
  }

  public function rapport_court_fr_new_pptx(Request $request){
    $this->rapport_court_fr_new($request->id,$request->date,$request->genre);
  }

  public function deleteFolder($str)
  {
    if (is_file($str)) { 
          
        // If it is file then remove by 
        // using unlink function 
        return unlink($str); 
    } 
      
    // If it is a directory. 
    elseif (is_dir($str)) { 
          
        // Get the list of the files in this 
        // directory 
        $scan = glob(rtrim($str, '/').'/*'); 
          
        // Loop through the list of files 
        foreach($scan as $index=>$path) { 
              
            // Call recursive function 
            $this->deleteFolder($path); 
        } 
          
        // Remove the directory itself 
        return @rmdir($str); 
    }
  }

  public function zipFolder($zip,$path,$path2){
    $rootPath = realpath($path);

    // Initialize archive object
    $zip->open($path2.'file.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($rootPath),
        \RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
  }

  public function getMonth($mois,$lang){
    switch ($mois) {
      case "01":
        # code...
        $value = $lang == "fr" ? "Jan." : "Jan.";
        break;
      case "02":
        # code...
        $value = $lang == "fr" ? "Fév." : "Feb.";
        break;
      case "03":
        # code...
        $value = $lang == "fr" ? "Mars" : "March";
        break;
      case "04":
        # code...
        $value = $lang == "fr" ? "Avril" : "April";
        break;
      case "05":
        # code...
        $value = $lang == "fr" ? "Mai" : "May";
        break;
      case "06":
        # code...
        $value = $lang == "fr" ? "Juin" : "June";
        break;
      case "07":
        # code...
        $value = $lang == "fr" ? "Jui." : "July";
        break;
      case "08":
        # code...
        $value = $lang == "fr" ? "Août" : "Aug.";
        break;
      case "09":
        # code...
        $value = $lang == "fr" ? "Sep." : "Sept.";
        break;
      case "10":
        # code...
        $value = $lang == "fr" ? "Oct." : "Oct.";
        break;
      case "11":
        # code...
        $value = $lang == "fr" ? "Nov." : "Nov.";
        break;
      case "12":
        # code...
        $value = $lang == "fr" ? "Déc." : "Dec.";
        break;
      default:
          # code...
          break;
    }
    return $value;
  }

  public function getMonth2($mois,$lang){
    switch ($mois) {
      case "01":
        # code...
        $value = $lang == "fr" ? "Janvier" : "January";
        break;
      case "02":
        # code...
        $value = $lang == "fr" ? "Février" : "February";
        break;
      case "03":
        # code...
        $value = $lang == "fr" ? "Mars" : "March";
        break;
      case "04":
        # code...
        $value = $lang == "fr" ? "Avril" : "April";
        break;
      case "05":
        # code...
        $value = $lang == "fr" ? "Mai" : "May";
        break;
      case "06":
        # code...
        $value = $lang == "fr" ? "Juin" : "June";
        break;
      case "07":
        # code...
        $value = $lang == "fr" ? "Juillet" : "July";
        break;
      case "08":
        # code...
        $value = $lang == "fr" ? "Août" : "August";
        break;
      case "09":
        # code...
        $value = $lang == "fr" ? "Septembre" : "September";
        break;
      case "10":
        # code...
        $value = $lang == "fr" ? "Octobre" : "October";
        break;
      case "11":
        # code...
        $value = $lang == "fr" ? "Novembre" : "November";
        break;
      case "12":
        # code...
        $value = $lang == "fr" ? "Décembre" : "December";
        break;
      default:
          # code...
          break;
    }
    return $value;
  }

  public function rapport_court_fr($id,$date){

    $test = Test::find($id);
    $repondant = $test->users[0];
    $consultant = $test->consultant;
    $txtRa = "Rapport-Inventaire-".str_replace(" ","-",$repondant->firstname);

    $serieuxTotal = 0;
    $enjouTotal = 0;
    $conformTotal = 0;
    $transagressiTotal = 0;

    $autruiTotal = 0;
    $soiTotal = 0;
    $maitriseTotal = 0;
    $sympathieTotal = 0;

    ////// GET ALL CATEGORIES ////////
    $cats = Category::all();

    ////// GET ALL QUESTIONS POUR CHAQUE CATEGORIE ///////////
    $data_quest = [];
    foreach ($cats as $key => $cat) {
      if($cat->name == "serieux"){
        $data_quest["serieux"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "enjoue"){
        $data_quest["enjoue"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "transgressif"){
        $data_quest["transgressif"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "conforme"){
        $data_quest["conforme"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "autrui"){
        $data_quest["autrui"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "soi"){
        $data_quest["soi"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "maitrise"){
        $data_quest["maitrise"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else {
        $data_quest["sympathie"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      }
    }

    // SCORE SERIEUX
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();

    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $serieuxTotal = $serieuxTotal + $score->score;
    }

    if($serieuxTotal >= 0)
      $serieuxTotal = ((($serieuxTotal - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
    
    // SCORE ENJOUE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $enjouTotal = $enjouTotal + $score->score;
    }
    if($enjouTotal >= 0)
      $enjouTotal = ((($enjouTotal - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

    // SCORE CONFORME
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $conformTotal = $conformTotal + $score->score;
    }
    if($conformTotal >= 0)
      $conformTotal = ((($conformTotal - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

    // SCORE TRANSGRESSIF
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $transagressiTotal = $transagressiTotal + $score->score;
    }
    if($transagressiTotal >= 0)
      $transagressiTotal = ((($transagressiTotal - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

    // SCORE AUTRUI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $autruiTotal = $autruiTotal + $score->score;
    }
    if($autruiTotal >= 0){
      $autruiTotal = ($autruiTotal / 2);
      $autruiTotal = ((($autruiTotal - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
    }

    // SCORE SOI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $soiTotal = $soiTotal + $score->score;
    }
    if($soiTotal >= 0){
      $soiTotal = ($soiTotal / 2);
      $soiTotal = ((($soiTotal - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
    }

    // SCORE MAITRISE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $maitriseTotal = $maitriseTotal + $score->score;
    }
    if($maitriseTotal >= 0){
      $maitriseTotal = ($maitriseTotal / 2);
      $maitriseTotal = ((($maitriseTotal - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
    }

    // SCORE SYMPHATIE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $sympathieTotal = $sympathieTotal + $score->score;
    }
    if($sympathieTotal >= 0){
      $sympathieTotal = ($sympathieTotal / 2);
      $sympathieTotal = ((($sympathieTotal - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
    }

    $arrayGraph1DrawY = array(number_format($serieuxTotal),
      number_format($enjouTotal),
      number_format($conformTotal),
      number_format($transagressiTotal),
      number_format($maitriseTotal),
      number_format($sympathieTotal),
      number_format($autruiTotal),
      number_format($soiTotal)
    );

    $pdf = new Fpdi();
    $path = base_path().'/public/isma/';
    $pageCount = $pdf->setSourceFile($path.'templates/isma_rapport_court.pdf');

    ///////////PAGE 1////////////////

    $pdf->AddPage("P", "A4");
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',30);
    $pdf->setFillColor(255,255,255); 
    $pdf->setTextColor(75,75,75);
    $pdf->setFillColor(198,198,198);
    $pdf->SetXY(0,40);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", " "),"0",0,'C',1);
    $pdf->setFillColor(255,255,255);
    $pdf->SetXY(61,39);
    $pdf->Cell(84,17,iconv("UTF-8", "windows-1252", ""),"0",0,'C',1);
    $pdf->Image($path.'img/apter_solution.jpeg',70,35,70); 
    $pdf->SetFont('RobotoCondensed-Bold','',40);
    $pdf->setTextColor(75,75,75);
    $pdf->SetXY(0,85);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "INVENTAIRE"),"0",0,'C',1);
    $pdf->SetXY(0,105);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "DE STYLES"),"0",0,'C',1);
    $pdf->SetXY(0,125);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "MOTIVATIONNELS"),"0",0,'C',1);
     
    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->setFillColor(243,199,20);
    $pdf->SetXY(50,170);
    $pdf->Cell(110,65,iconv("UTF-8", "windows-1252", " "),"0",0,'C',1);

    $pdf->setFillColor(255,255,255);
    $pdf->SetXY(50,180);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Cahier de"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,180);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $repondant->firstname." ".$repondant->lastname),"0",0,'L',0);
    $pdf->SetXY(50,190);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", $repondant->organisation),"0",0,'C',0);
    $pdf->SetFont('RobotoCondensed-Italic','',17);
    $pdf->SetXY(50,200);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", date_format($test->created_at,"d/m/Y")),"0",0,'C',0);
    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->SetXY(50,215);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Présenté par"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,215);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $consultant->firstname." ".$consultant->lastname),"0",0,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->SetAutoPageBreak(0);
    $pdf->SetXY(0,282);
    $pdf->setTextColor(255,255,255);
    $pdf->setFillColor(198,198,198);
    $pdf->Cell(210,16,iconv("UTF-8", "windows-1252", "© Apter-France | www.methode-apter.com"),"0",0,'C',1);

    ///////////END PAGE 1////////////////

    //////////PAGE 2 :SOMMAIRE///////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(2, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    //////////END PAGE 2 :SOMMAIRE///////////////

    //////////PAGE 3 à 7/////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(3, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(4, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(5, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(6, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(7, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    //////////END PAGE 3 à 7/////////////////////////

    //////////////PAGE 8 ///////////////////////////
    $pdf->AddPage("P", "A4"); 

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Votre diagramme « Profil de dominances » "),"0",'L',0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 19, 190, 0, 'F');

    $EnjoueSerieux = $arrayGraph1DrawY[0] - $arrayGraph1DrawY[1];
    $ConformeTransgressif = $arrayGraph1DrawY[2] - $arrayGraph1DrawY[3];
    $MaitriseSympathie = $arrayGraph1DrawY[5] - $arrayGraph1DrawY[4];
    $SoiAutrui = $arrayGraph1DrawY[7] - $arrayGraph1DrawY[6];

    $valEnjoueSerieux = $EnjoueSerieux;

    if($valEnjoueSerieux < 0)
      $valEnjoueSerieux = $valEnjoueSerieux * (-1);
    $valConformeTransgressif = $ConformeTransgressif;
    if($valConformeTransgressif < 0)
      $valConformeTransgressif = $valConformeTransgressif * (-1);
    $valMaitriseSympathie = $MaitriseSympathie;
    if($valMaitriseSympathie < 0)
      $valMaitriseSympathie = $valMaitriseSympathie * (-1);
    $valSoiAutrui = $SoiAutrui;
    if($valSoiAutrui < 0)
      $valSoiAutrui = $valSoiAutrui * (-1);
    

    $arrayGraphe2 = array($valEnjoueSerieux,$valConformeTransgressif,$valMaitriseSympathie,$valSoiAutrui);
    $maxValYGraphe2 = max($arrayGraphe2);

    $pdf->SetFont('RobotoCondensed-Regular','',10);
    $BGraph2Y = 40;
    $BGraph2YC = 38;
    $BGraph2YCT = 45;
    $inc = 5;
    if($maxValYGraphe2 >= 161 ){
      $BGraph2YCT = 255;
      $inc = 25;
    }
    else if ($maxValYGraphe2 >= 121 and $maxValYGraphe2 <= 160 ){
      $BGraph2YCT = 180;
      $inc = 20; 
    }
    else if ($maxValYGraphe2 > 80 and $maxValYGraphe2 <= 120){
      $BGraph2YCT = 135;
      $inc = 15;
      
    }
    else if ($maxValYGraphe2 > 40 and $maxValYGraphe2 <= 80){
      $BGraph2YCT = 90;
      $inc = 10; 
    }
    else{
      $BGraph2YCT = 45;
      $inc = 5;
    }
    
    for($IBGraph2X = 0; $IBGraph2X <= 18 ; $IBGraph2X++){
      $pdf->setFillColor(230,230,230);
      $pdf->Rect(29, $BGraph2Y, 161, 0, 'F');
      $pdf->setFillColor(0,0,0);
      $pdf->Rect(27, $BGraph2Y, 2, 0, 'F');
      $pdf->setXY(21,$BGraph2YC);
      $pdf->Cell(6,4,$BGraph2YCT,"0",0,'C',0);
      if($IBGraph2X < 9)
        $BGraph2YCT = $BGraph2YCT - $inc;
      else
        $BGraph2YCT = $BGraph2YCT + $inc;
      $BGraph2Y = $BGraph2Y + 10;
      $BGraph2YC = $BGraph2YC + 10;
    }

    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(29,40);
    $pdf->Cell(0,180,"","L",0,'C',0);

    $arrayGraph2T = array('Sérieux','Enjoué','Conforme','Transgressif','Sympathie','Maîtrise','Soi','Autrui');

    $arrayColor = array(  '1' => array(255,192,0),
                '0' => array(0,153,204),
                '2' => array(255,127,71),
                '3' => array(0,157,137),
                '4' => array(115,185,213),
                '5' => array(178,1,95),
                '7' => array(0,204,102),
                '6' => array(255,0,100)
              );
    $Graph2TXT = 29;
    $iGraph2TA = 0;
    for($iGraph2T = 0 ; $iGraph2T < 4; $iGraph2T++){

       
      $pdf->setXY($Graph2TXT,41);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]); 
      $pdf->setXY($Graph2TXT + 5,50);
      if($iGraph2TA == 2 || $iGraph2TA == 4)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1; 
      $pdf->setXY($Graph2TXT,211);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]);
      $pdf->setXY($Graph2TXT + 5,130);
      if($iGraph2TA == 3 || $iGraph2TA == 5)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1;
      $Graph2TXT = $Graph2TXT + 40;

       
    }
   

  //motifConform
  $yGi2M = 49.86;
  for ($Gi2M=0; $Gi2M < 80 ; $Gi2M++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 19; $Gi2MY++){
      $pdf->Image($path.'img/motifConform.png',$xGi2M, $yGi2M,1.6); 
      $xGi2M += 1.6;
    } 
    $yGi2M += 1.002;
  }

  
 
  //MotifTransgressif 
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 18; $Gi2MY++){
      $pdf->Image($path.'img/MotifTransgressif.png',$xGi2M, $xGi2T,1.7);
      $xGi2M += 1.7;
    }  
    $xGi2T += 1.956;
  }
  //motifSympathie
  $xGi2T = 49.86;
  for ($Gi2T=0; $Gi2T < 80 ; $Gi2T++) {
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 15; $Gi2MY++){
      $pdf->Image($path.'img/motifSympathie.png',$xGi2M,$xGi2T,2.1);
      $xGi2M += 2.03;
    }
    $xGi2T += 1;
  }

  //motifMaitrise
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 20; $Gi2MY++){
      $pdf->Image($path.'img/motifMaitrise.png',$xGi2M, $xGi2T,1.1);
      $xGi2M += 1.55;
    }
    $xGi2T += 1.97;
  }


$pdf->SetFont('RobotoCondensed-Bold','',14);
  $pdf->setFillColor(255,255,255);

  if($EnjoueSerieux > 0){
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ;
    $pdf->setXY(37,130 - $yEnjoueSerieux);
  }
  else{

    $pdf->setXY(37,130); 
    $EnjoueSerieux = $EnjoueSerieux * -1;
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ; 
  }
  if($EnjoueSerieux > 0)
    $pdf->Cell(24,$yEnjoueSerieux,$EnjoueSerieux,"0",0,'C',1);


  if($ConformeTransgressif > 0){
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ;
    $pdf->setXY(77,130 - $yConformeTransgressif); 
  }
  else{
    $pdf->setXY(77,130); 
    $ConformeTransgressif = $ConformeTransgressif * -1;
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ; 
  }
  if($ConformeTransgressif > 0)
    $pdf->Cell(24,$yConformeTransgressif,$ConformeTransgressif,"0",0,'C',1);


  if($MaitriseSympathie > 0){
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
    $pdf->setXY(117,130 - $yMaitriseSympathie);
  }
  else{
    $pdf->setXY(117,130); 
    $MaitriseSympathie = $MaitriseSympathie * -1; 
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
  }
  if($MaitriseSympathie > 0)
    $pdf->Cell(24,$yMaitriseSympathie,$MaitriseSympathie,"0",0,'C',1);


  if($SoiAutrui > 0){
    $ySoiAutrui = ($SoiAutrui * 10) / $inc ; 
    $pdf->setXY(157,130 - $ySoiAutrui);
  }
  else{
    $pdf->setXY(157,130); 
    $SoiAutrui = $SoiAutrui * -1; 
    $ySoiAutrui = $SoiAutrui;
  }
  if($SoiAutrui > 0)
    $pdf->Cell(24,$ySoiAutrui,$SoiAutrui,"0",0,'C',1);



    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "8"),"0",0,'R',0);

    //////////////END PAGE 8 ///////////////////////////

    ///////////////PAGE 9//////////////////////////////////////
    $pdf->AddPage("P", "A4"); 

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Votre diagramme de scores motivationnels "),"0",'L',0);
    $pdf->Rect(10, 19, 190, 0, 'F');

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Regular','',11);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Ce graphe indique la fréquence relative à laquelle vous expérimentez chaque état mental (cela au regard du groupe de référence (la moyenne 100) "),"0",'L',0);


    //graphe 1 ////////////////////////////////////////
    $pdf->SetY(40);

    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setFillColor(230,230,230); 

    $BGraph1X = 47;
    for($IBGraph1X = 0; $IBGraph1X < 18 ; $IBGraph1X++){
      $pdf->Rect($BGraph1X, 40, 0, 125, 'F');
      $BGraph1X = $BGraph1X + 9; 
    }

    //  1 
    $pdf->setFillColor(255,255,255); 
    $pdf->setX(37);
    $pdf->Cell(163,125,"","LRB",0,'C',0);

    $Graph1V = 30.2;
    for($IGraph1V = 0; $IGraph1V <= 18 ; $IGraph1V++){
      $pdf->setXY($Graph1V,165);
      if($IGraph1V != 0)
        $pdf->Cell(9.5,6,$IGraph1V."0",0,0,'R',0);
      else
        $pdf->Cell(9.5,6,$IGraph1V,0,0,'R',0);
      $Graph1V = $Graph1V + 9; 
    }

    //text Graphe 1
    $arrayGraph1T = array('Sérieux','Enjoué','Conforme','Transgressif','Maîtrise','Sympathie','Autrui','Soi');
    // position du text
    $Graph1VTY = 44;
    for($IGraph1VTY = 0; $IGraph1VTY < 8; $IGraph1VTY++){
      $pdf->setXY(19,$Graph1VTY);
      $pdf->Cell(17,9,iconv("UTF-8", "windows-1252", $arrayGraph1T[$IGraph1VTY]),"0",0,'R',0);
      $Graph1VTY = $Graph1VTY + 15;
    }


    $Graph1DrawY = 44;
      
    

    $arrayColor = array(  '0' => array(0,153,204),
                '1' => array(255,192,0),
                '3' => array(255,127,71),
                '2' => array(0,157,137),
                '4' => array(115,185,213),
                '5' => array(178,1,95),
                '6' => array(0,204,102),
                '7' => array(255,0,100)
            );

    for($IGraph1DrawY = 0; $IGraph1DrawY < 8 ; $IGraph1DrawY++){
      $pdf->setXY(37.2,$Graph1DrawY);
      $pdf->setFillColor($arrayColor[$IGraph1DrawY][0],$arrayColor[$IGraph1DrawY][1],$arrayColor[$IGraph1DrawY][2]); 

      $x = $arrayGraph1DrawY[$IGraph1DrawY]; 
      if($x > 0){
        $x = (($x * 9 ) / 10) + 1 ;
        if(in_array($IGraph1DrawY,array(2,3,4,5)))
          $pdf->Cell($x,9,"","",0,'C',0);
        else
          $pdf->Cell($x,9,"","",0,'C',1);
      }

      $Graph1DrawY = $Graph1DrawY + 15; 
    }


    
    $Graf1CX = 37.2; 
    $xt = $arrayGraph1DrawY[2]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,82.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,82.1,0.8);
      } 
      $Graf1CX += 0.9;  
    }


    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[3]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89.9,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,90.8,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,91.7,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,92.6,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,93.5,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,94.4,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,95.3,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,96.2,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,97.1,0.7);
      $Graf1MX += 0.9;  
    }


    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[4]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104.9,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,105.8,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,106.7,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,107.6,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,108.5,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,109.4,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,110.3,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,111.2,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,112.1,0.7);
      $Graf1MX += 0.9;  
    }

    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[5]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,127.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,127.1,0.8);
      }
      
      $Graf1MX += 0.9;  
    }



    // fix border dashed 
    $pdf->setFillColor(0,0,0);
    $bDashedY = 40;
    for($IbDashed = 0; $IbDashed < 44; $IbDashed++){
      if($IbDashed < 43)
        $pdf->Rect(114.5, $bDashedY, 0, 2, 'F');
      if($IbDashed < 40)
        $pdf->Rect(119, $bDashedY, 0, 2, 'F');
      if($IbDashed < 41)
        $pdf->Rect(132.5, $bDashedY, 0, 2, 'F');
      $pdf->Rect(141.5, $bDashedY, 0, 2, 'F');
      $bDashedY = $bDashedY + 3.5;
    }


    // text sous graphe 1 
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setFillColor(255,255,255);
    $pdf->setXY(69,175);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores inférieurs à 90)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-left.png',60,175,11); 
    $pdf->setXY(64,185);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","16% pop. de réf. (Scores inférieurs à 85)"),"0",0,'R',1); 
    $pdf->Image($path.'img/arrow-left.png',56,185,11);  

    $pdf->setXY(133.5,178.5);
    $pdf->Cell(49,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores supérieurs à 110)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-right.png',182.5,178.5,11); 
    $pdf->setXY(142,188.9);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","16% pop. de réf. (Scores supérieurs à 115)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-right.png',192,188.9,11);  




    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "9"),"0",0,'R',0);

    ///////////////END PAGE 9//////////////////////////////////////


    /////////////////PAGE 10///////////////////////////////////////

    $pdf->AddPage("P", "A4"); 
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Relevé chiffré"),"0",'L',0);
    $pdf->Rect(10, 19, 190, 0, 'F');
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',9);
    $pdf->setFillColor(255,255,255);
    $pdf->Image($path.'img/lunette.png',7,70,195);
    $pdf->Image($path.'img/rond_gris.png',55,135,10);
    $pdf->Image($path.'img/rond_gris.png',144,135,10);
    $maxVal = max($arrayGraph1DrawY);
    //table 1 
    $Sval = ($arrayGraph1DrawY[0] * 38) / $maxVal*0.7;
    $xySval = (38 - $Sval) / 2;
    $Eval = ($arrayGraph1DrawY[1] * 38) / $maxVal*0.7;
    $xyEval = (38 - $Eval) / 2; 
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(12,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);


    $xtrans=12;
    $xxtrans=-24;
    $kxtrans=25;

    $k=10;
    $k2=15;
    $dx=3;
    /*
  Borders of the graphe * and design
    */

    $somatiqueTotal = $serieuxTotal + $conformTotal + $enjouTotal + $transagressiTotal;
    $transactionnelTotal = $sympathieTotal + $soiTotal+ $maitriseTotal +$autruiTotal;

    $pdf->AddFont('analgesics','B','analgesics.php');
    // MILIEU 1
    $pdf->setXY(45, 138);
    $pdf->SetFont('analgesics','B','analgesics.php');

    $pdf->setTextColor(90,90,90); 
    $pdf->MultiCell(30, 5,intval($somatiqueTotal),"0", 'C', 0 );
    // MILIEU 2
    $pdf->setXY(134, 138);
    $pdf->MultiCell(30, 5,intval($transactionnelTotal),"0" ,'C', 0);

    // BUT
    $pdf->setTextColor(0,0,0); 
    $pdf->SetFont('analgesics','B',10);
    $pdf->setXY(40,90);
    $pdf->MultiCell(40,55, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[0]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(42+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Buts / Moyens"), "0", 'C', 0);

    // MOYEN
    $pdf->setXY(40,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[1]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(82+$kxtrans,25, iconv("UTF-8", "windows-1252","Tendu ou Relâché"), "0", 'C', 0);
    $pdf->setXY(22,153);
    $TotalSE = $arrayGraph1DrawY[0] + $arrayGraph1DrawY[1];

    //table 2 
    $Cval = ($arrayGraph1DrawY[2] * 38) / $maxVal*0.7;
    $xyCval = (38 - $Cval) / 2;
    $Tval = ($arrayGraph1DrawY[3] * 38) / $maxVal*0.7;
    $xyTval = (38 - $Tval) / 2;
    $pdf->setXY(57+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);

    // DEVOIR
      // $pdf->Image($path.'img/testmed2.png',61 + $xyCval ,51 + $xyCval ,$Cval);
    $pdf->setXY(20,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[2]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(60+$xtrans,100);
    //$pdf->MultiCell(113+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Règles"), "0", 'L', 0);

    // LIBERTE
    $pdf->setXY(61,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[3]), "0", 'C', 0);
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(67+$xtrans,153);
    $TotalCT = $arrayGraph1DrawY[2] + $arrayGraph1DrawY[3];

    //somme table 1 & table 2
    $pdf->setXY(35+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(42.5+$xtrans,175);
    $pdf->setFillColor(230,230,230); 

    //table 3
    $SYval = ($arrayGraph1DrawY[5] * 38) / $maxVal*0.7;
    $xySYval = (38 - $SYval) / 2;
    $Mval = ($arrayGraph1DrawY[4] * 38) / $maxVal*0.7;
    $xyMval = (38 - $Mval) / 2;
    $pdf->setFillColor(255,255,255);  
    $pdf->setXY(112+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // harmonie
    $pdf->setXY(109,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[5]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(215+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Interactions"), "0", 'C', 0);

    // POUVOIR
    $pdf->setXY(150,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[4]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(247+$kxtrans,25, iconv("UTF-8", "windows-1252","Gagnant ou Perdant"), "0", 'C', 0);
    $pdf->setXY(122+$xtrans,153);
    $TotalSM = $arrayGraph1DrawY[4] + $arrayGraph1DrawY[5];

    //table 4 
    $SOval = ($arrayGraph1DrawY[7] * 38) / $maxVal*0.7;
    $xySOval = (38 - $SOval) / 2;
    $AUval = ($arrayGraph1DrawY[6] * 38) / $maxVal*0.7;
    $xyAUval = (38 - $AUval) / 2;
    $pdf->setXY(157+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // individu
    $pdf->setXY(129,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[7]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(145+$xtrans,100);
   // $pdf->MultiCell(285+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Orientation"), "0", 'L', 0);
    
      //$pdf->Image($path.'img/new_img/block_autrui.png',137 + $xyAUval-$dx+$xtrans,100 + $xyAUval+$k2+$k,$AUval);
    // collectif
    $pdf->setXY(129,113);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[6]), "0", 'C', 0);
    $pdf->setXY(167+$xtrans,153);
    $TotalSA = $arrayGraph1DrawY[7] + $arrayGraph1DrawY[6];

    //somme table 3 & table 4 
    $pdf->setXY(135.5+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(143+$xtrans ,175);
    $pdf->setFillColor(230,230,230);       
    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "10"),"0",0,'R',0);

    /////////////////END PAGE 10///////////////////////////////////////

    ///////////////////PAGE 11 à 14//////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(11, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    ///////////////////END PAGE 11 à 14//////////////////////////////////

    //////////////////PAGE 15///////////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    // Graphe 5
    $pdf->SetFont('RobotoCondensed-Regular','',10);
    $pdf->setFillColor(230,230,230); 

    $BGraph5X = 36;
    for($IBGraph5X = 0; $IBGraph5X < 15 ; $IBGraph5X++){
      $pdf->Rect($BGraph5X, 70, 0, 82, 'F');
      $BGraph5X = $BGraph5X + 11; 
    }
     
    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(36,70);
    $pdf->Cell(154,82,"","LRB",0,'C',0);

    $Graph5V = 29;
    for($IGraph5V = 100; $IGraph5V <= 680 ; $IGraph5V+=40){
      $pdf->setXY($Graph5V,152);
      $pdf->Cell(9.5,6,$IGraph5V,0,0,'R',0);
      $Graph5V = $Graph5V + 11; 
    } 
    //text Graphe 4
    $arrayGraph5T = array("Sérieux \n Enjoué \n Conforme \n Transgressif","Maîtrise \n Sympathie \n Autrui \n Soi");
    $Graph5VTY = 84;
    for($IGraph5VTY = 0; $IGraph5VTY < 2; $IGraph5VTY++){
      $pdf->setXY(2,$Graph5VTY); 
      $pdf->MultiCell(32,4, iconv("UTF-8", "windows-1252",$arrayGraph5T[$IGraph5VTY]), "0", 'R', 1);
      $Graph5VTY = $Graph5VTY + 35;
    }

    $arrayTotalG5 = array($TotalSE + $TotalCT,$TotalSM + $TotalSA);
    $Graph5DrawY = 80;
    $pdf->SetFont('RobotoCondensed-Bold','',10);
    for($IGraph5DrawY = 0; $IGraph5DrawY < 2 ; $IGraph5DrawY++){
      $pdf->setXY(36.2,$Graph5DrawY);
      $pdf->setFillColor(230,230,230);
      $x = $arrayTotalG5[$IGraph5DrawY];
      $x = (($x * 11) / 40 ) - 28;
      if($x > 0)
        $pdf->Cell($x,25,$arrayTotalG5[$IGraph5DrawY],"",0,'C',1);
      $Graph5DrawY = $Graph5DrawY + 35; 
    }

    //////////////////END PAGE 15///////////////////////////////////////////

    /////////////////PAGE 16////////////////////////////////////////////////////
    
    $pdf->addPage();
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->Image($path.'img/observation-4.png',8,58); 
    $Graph4DrawY = 67.5;
    $pdf->setFillColor(230,230,230); 

    $obs4 = [
      [0,1], [2,3], [4,5], [6,7]
    ];

    $arrayTotalG4 = array($TotalSE,$TotalCT,$TotalSM,$TotalSA);

    for($i = 0; $i < 4 ; $i++){
      $pdf->setXY(83,$Graph4DrawY);
      $pdf->setFillColor(230,230,230);
      $x = $arrayTotalG4[$i];
      $x = ($x * 11) / 20;

      $val = $arrayGraph1DrawY[$obs4[$i][0]] + $arrayGraph1DrawY[$obs4[$i][1]];
      // $val = 120;
      $val_graph = ( ($val-100) * 10)/20;
      $pdf->Cell($val_graph,5,$val,"",0,'C',1);

      $Graph4DrawY = $Graph4DrawY + 9 ; 
    }

    /////////////////END PAGE 16////////////////////////////////////////////////////

    /////////////////PAGE 17////////////////////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->Image($path.'img/observation-6.png',30,58,150);
    $obs4 = [
      [0,1], [2,3], [4,5], [7,6]
    ];
    $Graph4DrawY = 65;
    $pdf->setFillColor(230,230,230); 

    for($i = 0; $i < 4 ; $i++){
      $pdf->setXY(106,$Graph4DrawY);
      $x = $arrayTotalG4[$i];
      $x = ($x * 11) / 20;
      // x = 10 <= 20
            // <= x - 100

      // if($x > 0){
        $val = $arrayGraph1DrawY[$obs4[$i][0]] - $arrayGraph1DrawY[$obs4[$i][1]];
        // $val = 20;
        $val_graph = ( ($val) * 10.8)/10;
        $pdf->Cell($val_graph,5.7,$val,"",0,'C',1);
      // }
      $Graph4DrawY = $Graph4DrawY + 10  ; 
    }


    /////////////////END PAGE 17////////////////////////////////////////////////////


    /////////////////PAGE 18 à 22////////////////////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(19, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(20, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(21, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(22, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    /////////////////END PAGE 18 à 22////////////////////////////////////////////////////

    $pdf->Output($txtRa.".pdf","I");

  }

  public function rapport_long($id,$date){
    $test = Test::find($id);
    $repondant = $test->users[0];
    //coach = consultant
    $consultant = $test->consultant;
    $txtRa = "Rapport-Inventaire-".str_replace(" ","-",$repondant->firstname);
    $pdf = new Fpdi();
    $path = base_path().'/public/isma2/';
    $pageCount = $pdf->setSourceFile($path.'templates/isma_rapport_long.pdf');
    $pdf->AddPage("P", "A4");
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');

    $pdf->SetFont('RobotoCondensed-Bold','',30);
    $pdf->setFillColor(255,255,255); 

    $pdf->setTextColor(75,75,75);
    $pdf->setFillColor(198,198,198);
    $pdf->SetXY(0,40);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", " "),"0",0,'C',1);
    $pdf->setFillColor(255,255,255);
    $pdf->SetXY(61,39);
    $pdf->Cell(84,17,iconv("UTF-8", "windows-1252", ""),"0",0,'C',1);
    $pdf->Image($path.'img/apter_solution.jpeg',70,35,70); 

    

    $pdf->SetFont('RobotoCondensed-Bold','',40);
    $pdf->setTextColor(75,75,75);
    $pdf->SetXY(0,85);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "INVENTAIRE"),"0",0,'C',1);
    $pdf->SetXY(0,105);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "DE STYLES"),"0",0,'C',1);
    $pdf->SetXY(0,125);
    $pdf->Cell(210,15,iconv("UTF-8", "windows-1252", "MOTIVATIONNELS"),"0",0,'C',1);
     
    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->setFillColor(243,199,20);
    $pdf->SetXY(50,170);
    $pdf->Cell(110,65,iconv("UTF-8", "windows-1252", " "),"0",0,'C',1);

    $pdf->setFillColor(255,255,255);
    $pdf->SetXY(50,180);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Cahier de"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,180);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $repondant->firstname." ".$repondant->lastname),"0",0,'L',0);

    $pdf->SetXY(50,190);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", $repondant->organisation),"0",0,'C',0);
    $pdf->SetFont('RobotoCondensed-Italic','',17);
    $pdf->SetXY(50,200);
    $pdf->Cell(110,10,iconv("UTF-8", "windows-1252", date_format($test->created_at,"d/m/Y")),"0",0,'C',0);

    $pdf->SetFont('RobotoCondensed-Regular','',17);
    $pdf->SetXY(50,215);
    $pdf->Cell(45,10,iconv("UTF-8", "windows-1252", "Présenté par"),"0",0,'R',0);
    $pdf->SetFont('RobotoCondensed-Bold','',17);
    $pdf->SetXY(95,215);
    $pdf->Cell(65,10,iconv("UTF-8", "windows-1252", $consultant->firstname." ".$consultant->lastname),"0",0,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->SetAutoPageBreak(0);
    $pdf->SetXY(0,282);
    $pdf->setTextColor(255,255,255);
    $pdf->setFillColor(198,198,198);
    $pdf->Cell(210,16,iconv("UTF-8", "windows-1252", "© Apter-France | www.methode-apter.com"),"0",0,'C',1);

    /////// FIN PAGE 1 ////////

    /////// PAGE 2 - 7 ///////////

    $pdf->addPage();
    $pageId = $pdf->importPage(2, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(3, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(4, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(5, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(6, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(7, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    //////// FIN PAGE 2 - 7 ///////////

    $serieuxTotal = 0;
    $enjouTotal = 0;
    $conformTotal = 0;
    $transagressiTotal = 0;

    $autruiTotal = 0;
    $soiTotal = 0;
    $maitriseTotal = 0;
    $sympathieTotal = 0;

    //$type_id = $test->type->id;

    ////// GET ALL CATEGORIES ////////
    $cats = Category::all();


    ////// GET ALL QUESTIONS POUR CHAQUE CATEGORIE ///////////
    $data_quest = [];
    foreach ($cats as $key => $cat) {
      if($cat->name == "serieux"){
        $data_quest["serieux"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "enjoue"){
        $data_quest["enjoue"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "transgressif"){
        $data_quest["transgressif"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "conforme"){
        $data_quest["conforme"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "autrui"){
        $data_quest["autrui"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "soi"){
        $data_quest["soi"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else if($cat->name == "maitrise"){
        $data_quest["maitrise"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      } else {
        $data_quest["sympathie"] = Category_question::where('category_id','=',$cat->id)->select('question_id')->get();
      }
    }

    // SCORE SERIEUX
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["serieux"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $serieuxTotal = $serieuxTotal + $score->score;
    }

    if($serieuxTotal >= 0)
      $serieuxTotal = ((($serieuxTotal - 23.6105200945627 ) / 3.14411700763939 ) * 15 ) + 100 ;
    
    // SCORE ENJOUE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["enjoue"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $enjouTotal = $enjouTotal + $score->score;
    }
    if($enjouTotal >= 0)
      $enjouTotal = ((($enjouTotal - 17.8221040189125 ) / 3.49684772378874 ) * 15 ) + 100 ;

    // SCORE CONFORME
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["conforme"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $conformTotal = $conformTotal + $score->score;
    }
    if($conformTotal >= 0)
      $conformTotal = ((($conformTotal - 21.6323877068557 ) / 3.83557264004136 ) * 15 ) + 100 ;

    // SCORE TRANSGRESSIF
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["transgressif"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $transagressiTotal = $transagressiTotal + $score->score;
    }
    if($transagressiTotal >= 0)
      $transagressiTotal = ((($transagressiTotal - 12.7056737588652 ) / 3.62178714506202 ) * 15 ) + 100 ;

    // SCORE AUTRUI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["autrui"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $autruiTotal = $autruiTotal + $score->score;
    }
    if($autruiTotal >= 0){
      $autruiTotal = ($autruiTotal / 2);
      $autruiTotal = ((($autruiTotal - 23.669621749409 ) / 3.0465847480089 ) * 15 ) + 100 ;
    }

    // SCORE SOI
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["soi"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $soiTotal = $soiTotal + $score->score;
    }
    if($soiTotal >= 0){
      $soiTotal = ($soiTotal / 2);
      $soiTotal = ((($soiTotal - 20.8720449172577 ) / 2.90588095600065 ) * 15 ) + 100 ;
    }

    // SCORE MAITRISE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["maitrise"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $maitriseTotal = $maitriseTotal + $score->score;
    }
    if($maitriseTotal >= 0){
      $maitriseTotal = ($maitriseTotal / 2);
      $maitriseTotal = ((($maitriseTotal - 23.1146572104019 ) / 2.93032196409131 ) * 15 ) + 100 ;
    }

    // SCORE SYMPHATIE
    $valeurs = ReponseRepondant::whereIn('question_id', $data_quest["sympathie"])->where('repondant_id','=',$repondant->id)->where("created_at","like","%".$date."%")->get();
    foreach ($valeurs as $key => $value) {
      //var_dump($value->reponse_id);
      $score = Reponse::where('id','=',$value->reponse_id)->select('score')->first();
      $sympathieTotal = $sympathieTotal + $score->score;
    }
    if($sympathieTotal >= 0){
      $sympathieTotal = ($sympathieTotal / 2);
      $sympathieTotal = ((($sympathieTotal - 21.4270094562648 ) / 3.05302398469498 ) * 15 ) + 100 ;
    }

    $arrayGraph1DrawY = array(number_format($serieuxTotal),
      number_format($enjouTotal),
      number_format($conformTotal),
      number_format($transagressiTotal),
      number_format($maitriseTotal),
      number_format($sympathieTotal),
      number_format($autruiTotal),
      number_format($soiTotal)
    );

    /////////////PAGE 8////////////////////////////////
    $pdf->AddPage("P", "A4"); 

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Votre diagramme « Profil de dominances » "),"0",'L',0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 19, 190, 0, 'F');
    $EnjoueSerieux = $arrayGraph1DrawY[0] - $arrayGraph1DrawY[1];
    $ConformeTransgressif = $arrayGraph1DrawY[2] - $arrayGraph1DrawY[3];
    $MaitriseSympathie = $arrayGraph1DrawY[5] - $arrayGraph1DrawY[4];
    $SoiAutrui = $arrayGraph1DrawY[7] - $arrayGraph1DrawY[6];

    $valEnjoueSerieux = $EnjoueSerieux;

    if($valEnjoueSerieux < 0)
      $valEnjoueSerieux = $valEnjoueSerieux * (-1);
    $valConformeTransgressif = $ConformeTransgressif;
    if($valConformeTransgressif < 0)
      $valConformeTransgressif = $valConformeTransgressif * (-1);
    $valMaitriseSympathie = $MaitriseSympathie;
    if($valMaitriseSympathie < 0)
      $valMaitriseSympathie = $valMaitriseSympathie * (-1);
    $valSoiAutrui = $SoiAutrui;
    if($valSoiAutrui < 0)
      $valSoiAutrui = $valSoiAutrui * (-1);

    $arrayGraphe2 = array($valEnjoueSerieux,$valConformeTransgressif,$valMaitriseSympathie,$valSoiAutrui);
    $maxValYGraphe2 = max($arrayGraphe2);

    $pdf->SetFont('RobotoCondensed-Regular','',10);

    $BGraph2Y = 40;
    $BGraph2YC = 38;
    $BGraph2YCT = 45;
    $inc = 5;
    if($maxValYGraphe2 >= 161 ){
      $BGraph2YCT = 255;
      $inc = 25;
    }
    else if ($maxValYGraphe2 >= 121 and $maxValYGraphe2 <= 160 ){
      $BGraph2YCT = 180;
      $inc = 20; 
    }
    else if ($maxValYGraphe2 > 80 and $maxValYGraphe2 <= 120){
      $BGraph2YCT = 135;
      $inc = 15;
      
    }
    else if ($maxValYGraphe2 > 40 and $maxValYGraphe2 <= 80){
      $BGraph2YCT = 90;
      $inc = 10; 
    }
    else{
      $BGraph2YCT = 45;
      $inc = 5;
    }

    for($IBGraph2X = 0; $IBGraph2X <= 18 ; $IBGraph2X++){
      $pdf->setFillColor(230,230,230);
      $pdf->Rect(29, $BGraph2Y, 161, 0, 'F');
      $pdf->setFillColor(0,0,0);
      $pdf->Rect(27, $BGraph2Y, 2, 0, 'F');
      $pdf->setXY(21,$BGraph2YC);
      $pdf->Cell(6,4,$BGraph2YCT,"0",0,'C',0);
      if($IBGraph2X < 9)
        $BGraph2YCT = $BGraph2YCT - $inc;
      else
        $BGraph2YCT = $BGraph2YCT + $inc;
      $BGraph2Y = $BGraph2Y + 10;
      $BGraph2YC = $BGraph2YC + 10;
    }

    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(29,40);
    $pdf->Cell(0,180,"","L",0,'C',0);

    $arrayGraph2T = array('Sérieux','Enjoué','Conforme','Transgressif','Sympathie','Maîtrise','Soi','Autrui');

    $arrayColor = array(  '1' => array(255,192,0),
                '0' => array(0,153,204),
                '2' => array(255,127,71),
                '3' => array(0,157,137),
                '4' => array(115,185,213),
                '5' => array(178,1,95),
                '7' => array(0,204,102),
                '6' => array(255,0,100)
              );
    $Graph2TXT = 29;
    $iGraph2TA = 0;
    for($iGraph2T = 0 ; $iGraph2T < 4; $iGraph2T++){

       
      $pdf->setXY($Graph2TXT,41);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]); 
      $pdf->setXY($Graph2TXT + 5,50);
      if($iGraph2TA == 2 || $iGraph2TA == 4)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1; 
      $pdf->setXY($Graph2TXT,211);
      $pdf->Cell(40,8,iconv("UTF-8", "windows-1252",$arrayGraph2T[$iGraph2TA]),"0",0,'C',0);

      $pdf->setFillColor($arrayColor[$iGraph2TA][0],$arrayColor[$iGraph2TA][1],$arrayColor[$iGraph2TA][2]);
      $pdf->setXY($Graph2TXT + 5,130);
      if($iGraph2TA == 3 || $iGraph2TA == 5)
        $pdf->Cell(30,80,"",0,0,'C',0);
      else
        $pdf->Cell(30,80,"",0,0,'C',1);

      $iGraph2TA = $iGraph2TA + 1;
      $Graph2TXT = $Graph2TXT + 40;

       
    }

  //motifConform
  $yGi2M = 49.86;
  for ($Gi2M=0; $Gi2M < 80 ; $Gi2M++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 19; $Gi2MY++){
      $pdf->Image($path.'img/motifConform.png',$xGi2M, $yGi2M,1.6); 
      $xGi2M += 1.6;
    } 
    $yGi2M += 1.002;
  }
 
  //MotifTransgressif 
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {  
    $xGi2M = 74;
    for($Gi2MY= 0; $Gi2MY < 18; $Gi2MY++){
      $pdf->Image($path.'img/MotifTransgressif.png',$xGi2M, $xGi2T,1.7);
      $xGi2M += 1.7;
    }  
    $xGi2T += 1.956;
  }
  //motifSympathie
  $xGi2T = 49.86;
  for ($Gi2T=0; $Gi2T < 80 ; $Gi2T++) {
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 15; $Gi2MY++){
      $pdf->Image($path.'img/motifSympathie.png',$xGi2M,$xGi2T,2.1);
      $xGi2M += 2.03;
    }
    $xGi2T += 1;
  }

  //motifMaitrise
  $xGi2T = 130;
  for ($Gi2T=0; $Gi2T < 41 ; $Gi2T++) {
    $xGi2M = 114;
    for($Gi2MY= 0; $Gi2MY < 20; $Gi2MY++){
      $pdf->Image($path.'img/motifMaitrise.png',$xGi2M, $xGi2T,1.1);
      $xGi2M += 1.55;
    }
    $xGi2T += 1.97;
  }

$pdf->SetFont('RobotoCondensed-Bold','',14);
  $pdf->setFillColor(255,255,255);

  if($EnjoueSerieux > 0){
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ;
    $pdf->setXY(37,130 - $yEnjoueSerieux);
  }
  else{

    $pdf->setXY(37,130); 
    $EnjoueSerieux = $EnjoueSerieux * -1;
    $yEnjoueSerieux = ($EnjoueSerieux * 10 ) / $inc ; 
  }
  if($EnjoueSerieux > 0)
    $pdf->Cell(24,$yEnjoueSerieux,$EnjoueSerieux,"0",0,'C',1);


  if($ConformeTransgressif > 0){
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ;
    $pdf->setXY(77,130 - $yConformeTransgressif); 
  }
  else{
    $pdf->setXY(77,130); 
    $ConformeTransgressif = $ConformeTransgressif * -1;
    $yConformeTransgressif = ($ConformeTransgressif * 10 ) / $inc ; 
  }
  if($ConformeTransgressif > 0)
    $pdf->Cell(24,$yConformeTransgressif,$ConformeTransgressif,"0",0,'C',1);


  if($MaitriseSympathie > 0){
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
    $pdf->setXY(117,130 - $yMaitriseSympathie);
  }
  else{
    $pdf->setXY(117,130); 
    $MaitriseSympathie = $MaitriseSympathie * -1; 
    $yMaitriseSympathie = ($MaitriseSympathie * 10) / $inc ; 
  }
  if($MaitriseSympathie > 0)
    $pdf->Cell(24,$yMaitriseSympathie,$MaitriseSympathie,"0",0,'C',1);


  if($SoiAutrui > 0){
    $ySoiAutrui = ($SoiAutrui * 10) / $inc ; 
    $pdf->setXY(157,130 - $ySoiAutrui);
  }
  else{
    $pdf->setXY(157,130); 
    $SoiAutrui = $SoiAutrui * -1; 
    $ySoiAutrui = $SoiAutrui;
  }
  if($SoiAutrui > 0)
    $pdf->Cell(24,$ySoiAutrui,$SoiAutrui,"0",0,'C',1);



    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "8"),"0",0,'R',0);

    /////////////END PAGE 8//////////////////////////////// 

    ///////////////PAGE 9//////////////////////////////////////

    $pdf->AddPage("P", "A4"); 

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Votre diagramme de scores motivationnels "),"0",'L',0);
    $pdf->Rect(10, 19, 190, 0, 'F');

    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Regular','',11);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Ce graphe indique la fréquence relative à laquelle vous expérimentez chaque état mental (cela au regard du groupe de référence (la moyenne 100) "),"0",'L',0);


    /////////////////// graphe 1 ////////////////////////////////////////
    $pdf->SetY(40);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setFillColor(230,230,230); 

    $BGraph1X = 47;
    for($IBGraph1X = 0; $IBGraph1X < 18 ; $IBGraph1X++){
      $pdf->Rect($BGraph1X, 40, 0, 125, 'F');
      $BGraph1X = $BGraph1X + 9; 
    }

    //  1 
    $pdf->setFillColor(255,255,255); 
    $pdf->setX(37);
    $pdf->Cell(163,125,"","LRB",0,'C',0);

    $Graph1V = 30.2;
    for($IGraph1V = 0; $IGraph1V <= 18 ; $IGraph1V++){
      $pdf->setXY($Graph1V,165);
      if($IGraph1V != 0)
        $pdf->Cell(9.5,6,$IGraph1V."0",0,0,'R',0);
      else
        $pdf->Cell(9.5,6,$IGraph1V,0,0,'R',0);
      $Graph1V = $Graph1V + 9; 
    }

    //text Graphe 1
    $arrayGraph1T = array('Sérieux','Enjoué','Conforme','Transgressif','Maîtrise','Sympathie','Autrui','Soi');
    $Graph1VTY = 44;
    for($IGraph1VTY = 0; $IGraph1VTY < 8; $IGraph1VTY++){
      $pdf->setXY(19,$Graph1VTY);
      $pdf->Cell(17,9,iconv("UTF-8", "windows-1252", $arrayGraph1T[$IGraph1VTY]),"0",0,'R',0);
      $Graph1VTY = $Graph1VTY + 15;
    }

    $Graph1DrawY = 44;     
  
    $arrayColor = array(  '0' => array(0,153,204),
                '1' => array(255,192,0),
                '3' => array(255,127,71),
                '2' => array(0,157,137),
                '4' => array(115,185,213),
                '5' => array(178,1,95),
                '6' => array(0,204,102),
                '7' => array(255,0,100)
            );

    for($IGraph1DrawY = 0; $IGraph1DrawY < 8 ; $IGraph1DrawY++){
      $pdf->setXY(37.2,$Graph1DrawY);
      $pdf->setFillColor($arrayColor[$IGraph1DrawY][0],$arrayColor[$IGraph1DrawY][1],$arrayColor[$IGraph1DrawY][2]); 

      $x = $arrayGraph1DrawY[$IGraph1DrawY]; 
      if($x > 0){
        $x = (($x * 9 ) / 10) + 1 ;
        if(in_array($IGraph1DrawY,array(2,3,4,5)))
          $pdf->Cell($x,9,"","",0,'C',0);
        else
          $pdf->Cell($x,9,"","",0,'C',1);
      }

      $Graph1DrawY = $Graph1DrawY + 15; 
    }


    
    $Graf1CX = 37.2; 
    $xt = $arrayGraph1DrawY[2]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformL.png',$Graf1CX,82.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,74.9,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,75.8,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,76.7,0.8);
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,77.6,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,78.5,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,79.4,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,80.3,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,81.2,0.8); 
        $pdf->Image($path.'img/motifConformR.png',$Graf1CX,82.1,0.8);
      } 
      $Graf1CX += 0.9;  
    }


    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[3]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,89.9,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,90.8,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,91.7,0.7);
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,92.6,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,93.5,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,94.4,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,95.3,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,96.2,0.7); 
      $pdf->Image($path.'img/MotifTransgressif.png',$Graf1MX,97.1,0.7);
      $Graf1MX += 0.9;  
    }


    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[4]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,104.9,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,105.8,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,106.7,0.7);
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,107.6,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,108.5,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,109.4,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,110.3,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,111.2,0.7); 
      $pdf->Image($path.'img/motifMaitrise.png',$Graf1MX,112.1,0.7);
      $Graf1MX += 0.9;  
    }

    $Graf1MX = 37.2; 
    $xt = $arrayGraph1DrawY[5]; 
    $ixT = (($xt * 10 ) / 9);
    $dxt = ($xt / 10 );
    if($dxt >= 10 )
      $dxt += 1;
    $ixT = $ixT - $dxt; 
    for($ml = 0; $ml < $ixT; $ml++){ 

      if($ml % 2 == 0){
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieL.png',$Graf1MX,127.1,0.8);
      }
      else{
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,119.9,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,120.8,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,121.7,0.8);
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,122.6,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,123.5,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,124.4,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,125.3,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,126.2,0.8); 
        $pdf->Image($path.'img/motifSympathieR.png',$Graf1MX,127.1,0.8);
      }
      
      $Graf1MX += 0.9;  
    }



    // fix border dashed 
    $pdf->setFillColor(0,0,0);
    $bDashedY = 40;
    for($IbDashed = 0; $IbDashed < 44; $IbDashed++){
      if($IbDashed < 43)
        $pdf->Rect(114.5, $bDashedY, 0, 2, 'F');
      if($IbDashed < 40)
        $pdf->Rect(119, $bDashedY, 0, 2, 'F');
      if($IbDashed < 41)
        $pdf->Rect(132.5, $bDashedY, 0, 2, 'F');
      $pdf->Rect(141.5, $bDashedY, 0, 2, 'F');
      $bDashedY = $bDashedY + 3.5;
    }


    // text sous graphe 1 
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setFillColor(255,255,255);
    $pdf->setXY(69,175);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores inférieurs à 90)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-left.png',60,175,11); 
    $pdf->setXY(64,185);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","16% pop. de réf. (Scores inférieurs à 85)"),"0",0,'R',1); 
    $pdf->Image($path.'img/arrow-left.png',56,185,11);  

    $pdf->setXY(133.5,178.5);
    $pdf->Cell(49,5,iconv("UTF-8", "windows-1252","25% pop. de réf. (Scores supérieurs à 110)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-right.png',182.5,178.5,11); 
    $pdf->setXY(142,188.9);
    $pdf->Cell(50,5,iconv("UTF-8", "windows-1252","16% pop. de réf. (Scores supérieurs à 115)"),"0",0,'R',1);
    $pdf->Image($path.'img/arrow-right.png',192,188.9,11);  
    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "9"),"0",0,'R',0);

    ///////////////END PAGE 9//////////////////////////////////////



    /////////////////PAGE 10///////////////////////////////////////
    $pdf->AddPage("P", "A4"); 
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->MultiCell(190,5,iconv("UTF-8", "windows-1252", "Relevé chiffré"),"0",'L',0);
    $pdf->Rect(10, 19, 190, 0, 'F');
    $pdf->setTextColor(0,0,0);
    $pdf->Ln(4);
    $pdf->SetFont('RobotoCondensed-Bold','',9);
    $pdf->setFillColor(255,255,255);
    $pdf->Image($path.'img/lunette.png',7,70,195);
    $pdf->Image($path.'img/rond_gris.png',55,135,10);
    $pdf->Image($path.'img/rond_gris.png',144,135,10);
    $maxVal = max($arrayGraph1DrawY);
    //table 1 
    $Sval = ($arrayGraph1DrawY[0] * 38) / $maxVal*0.7;
    $xySval = (38 - $Sval) / 2;
    $Eval = ($arrayGraph1DrawY[1] * 38) / $maxVal*0.7;
    $xyEval = (38 - $Eval) / 2; 
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(12,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);


    $xtrans=12;
    $xxtrans=-24;
    $kxtrans=25;

    $k=10;
    $k2=15;
    $dx=3;
    /*
  Borders of the graphe * and design
    */

    $somatiqueTotal = $serieuxTotal + $conformTotal + $enjouTotal + $transagressiTotal;
    $transactionnelTotal = $sympathieTotal + $soiTotal+ $maitriseTotal +$autruiTotal;

   $pdf->AddFont('analgesics','B','analgesics.php');
    // MILIEU 1
    $pdf->setXY(45, 138);
    $pdf->SetFont('analgesics','B','analgesics.php');

    $pdf->setTextColor(90,90,90); 
    $pdf->MultiCell(30, 5,intval($somatiqueTotal),"0", 'C', 0 );
    // MILIEU 2
    $pdf->setXY(134, 138);
    $pdf->MultiCell(30, 5,intval($transactionnelTotal),"0" ,'C', 0);

    // BUT
    $pdf->setTextColor(0,0,0); 
    $pdf->SetFont('analgesics','B',10);
    $pdf->setXY(40,90);
    $pdf->MultiCell(40,55, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[0]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(42+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Buts / Moyens"), "0", 'C', 0);

    // MOYEN
    $pdf->setXY(40,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[1]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(82+$kxtrans,25, iconv("UTF-8", "windows-1252","Tendu ou Relâché"), "0", 'C', 0);
    $pdf->setXY(22,153);
    $TotalSE = $arrayGraph1DrawY[0] + $arrayGraph1DrawY[1];

    //table 2 
    $Cval = ($arrayGraph1DrawY[2] * 38) / $maxVal*0.7;
    $xyCval = (38 - $Cval) / 2;
    $Tval = ($arrayGraph1DrawY[3] * 38) / $maxVal*0.7;
    $xyTval = (38 - $Tval) / 2;
    $pdf->setXY(57+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);

    // DEVOIR
      // $pdf->Image($path.'img/testmed2.png',61 + $xyCval ,51 + $xyCval ,$Cval);
    $pdf->setXY(20,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[2]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(60+$xtrans,100);
    //$pdf->MultiCell(113+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Règles"), "0", 'L', 0);

    // LIBERTE
    $pdf->setXY(61,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[3]), "0", 'C', 0);
    $pdf->SetTextColor(0,0,0);
    $pdf->setXY(67+$xtrans,153);
    $TotalCT = $arrayGraph1DrawY[2] + $arrayGraph1DrawY[3];

    //somme table 1 & table 2
    $pdf->setXY(35+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(42.5+$xtrans,175);
    $pdf->setFillColor(230,230,230); 

    //table 3
    $SYval = ($arrayGraph1DrawY[5] * 38) / $maxVal*0.7;
    $xySYval = (38 - $SYval) / 2;
    $Mval = ($arrayGraph1DrawY[4] * 38) / $maxVal*0.7;
    $xyMval = (38 - $Mval) / 2;
    $pdf->setFillColor(255,255,255);  
    $pdf->setXY(112+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // harmonie
    $pdf->setXY(109,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[5]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(215+$kxtrans,15, iconv("UTF-8", "windows-1252","Domaine Interactions"), "0", 'C', 0);

    // POUVOIR
    $pdf->setXY(150,133);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[4]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    //$pdf->MultiCell(247+$kxtrans,25, iconv("UTF-8", "windows-1252","Gagnant ou Perdant"), "0", 'C', 0);
    $pdf->setXY(122+$xtrans,153);
    $TotalSM = $arrayGraph1DrawY[4] + $arrayGraph1DrawY[5];

    //table 4 
    $SOval = ($arrayGraph1DrawY[7] * 38) / $maxVal*0.7;
    $xySOval = (38 - $SOval) / 2;
    $AUval = ($arrayGraph1DrawY[6] * 38) / $maxVal*0.7;
    $xyAUval = (38 - $AUval) / 2;
    $pdf->setXY(157+$xtrans,50); 
    $pdf->Cell(40,110,"","0",0,'C',0);
    
    // individu
    $pdf->setXY(129,162);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[7]), "0", 'C', 0);
    $pdf->SetFont('RobotoCondensed-Regular','',9);
    $pdf->setXY(145+$xtrans,100);
   // $pdf->MultiCell(285+$kxtrans,55, iconv("UTF-8", "windows-1252","Domaine Orientation"), "0", 'L', 0);
    
      //$pdf->Image($path.'img/new_img/block_autrui.png',137 + $xyAUval-$dx+$xtrans,100 + $xyAUval+$k2+$k,$AUval);
    // collectif
    $pdf->setXY(129,113);
    $pdf->SetFont('analgesics','B',10);
    $pdf->MultiCell(40,5, iconv("UTF-8", "windows-1252",$arrayGraph1DrawY[6]), "0", 'C', 0);
    $pdf->setXY(167+$xtrans,153);
    $TotalSA = $arrayGraph1DrawY[7] + $arrayGraph1DrawY[6];

    //somme table 3 & table 4 
    $pdf->setXY(135.5+$xtrans,166.5);
    //$pdf->Cell(45,16,"","LRB",0,'C',0);
    $pdf->setXY(143+$xtrans ,175);
    $pdf->setFillColor(230,230,230);       
    $pdf->SetAutoPageBreak(0);
    $pdf->setFillColor(255,193,46);
    $pdf->Rect(10, 285, 190, 0, 'F');
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $pdf->setTextColor(128,128,128); 
    $pdf->SetY(285);
    $pdf->Cell(100,4,iconv("UTF-8", "windows-1252", "Apter France – Inventaire de Styles Motivationnels"),"0",0,'L',0);
    $pdf->Cell(90,4,iconv("UTF-8", "windows-1252", "10"),"0",0,'R',0);

    /////////////////END PAGE 10///////////////////////////////////////

    ///////////////Page 11 à 13 ////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(11, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    ///////////////END Page 11 à 13 ////////////////////////////////



    ///////////////Page 14 ////////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',10);
    $pdf->setFillColor(230,230,230); 

    $BGraph4X = 25;
    for($IBGraph4X = 0; $IBGraph4X < 16 ; $IBGraph4X++){
      $pdf->Rect($BGraph4X, 77, 0, 88, 'F');
      $BGraph4X = $BGraph4X + 11; 
    }
     
    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(25,77);
    $pdf->Cell(176,88,"","LRB",0,'C',0);

    $Graph4V = 18;
    for($IGraph4V = 0; $IGraph4V <= 16 ; $IGraph4V++){
      $pdf->setXY($Graph4V,165);
      $pdf->Cell(9.5,6,($IGraph4V * 20),0,0,'R',0);
      $Graph4V = $Graph4V + 11; 
    } 
    //text Graphe 4
    $pdf->SetFont('RobotoCondensed-Regular','',8);
    $arrayGraph4T = array("Sérieux \n Enjoué","Conforme \n Transgressif","Maîtrise \n Sympathie","Autrui \n Soi");
    $Graph4VTY = 82;
    for($IGraph4VTY = 0; $IGraph4VTY < 4; $IGraph4VTY++){
      $pdf->setXY(5,$Graph4VTY); 
      $pdf->MultiCell(19,4, iconv("UTF-8", "windows-1252",$arrayGraph4T[$IGraph4VTY]), " ", 'R', 1);
      $Graph4VTY = $Graph4VTY + 20;
    }

    $arrayTotalG4 = array($TotalSE,$TotalCT,$TotalSM,$TotalSA);
    $Graph4DrawY = 80;
    $pdf->SetFont('RobotoCondensed-Bold','',10);
    for($IGraph4DrawY = 0; $IGraph4DrawY < 4 ; $IGraph4DrawY++){
      $pdf->setXY(25.2,$Graph4DrawY);
      $pdf->setFillColor(230,230,230);
      $x = $arrayTotalG4[$IGraph4DrawY];
      $x = ($x * 11) / 20;
      if($x > 0)
        $pdf->Cell($x,12,$arrayTotalG4[$IGraph4DrawY],"",0,'C',1);
      $Graph4DrawY = $Graph4DrawY + 20; 
    }

    /////////////// End Page 14 ////////////////////////////////////


    ///////////////Page 15 à 17 ////////////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    ///////////////END Page 15 à 17 ////////////////////////////////


    ///////////////Page 18 ///////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',10);
    $pdf->setFillColor(230,230,230); 

    $BGraph5X = 36;
    for($IBGraph5X = 0; $IBGraph5X < 15 ; $IBGraph5X++){
      $pdf->Rect($BGraph5X, 60, 0, 82, 'F');
      $BGraph5X = $BGraph5X + 11; 
    }
     
    $pdf->setFillColor(255,255,255); 
    $pdf->setXY(36,60);
    $pdf->Cell(154,82,"","LRB",0,'C',0);

    $Graph5V = 29;
    for($IGraph5V = 100; $IGraph5V <= 680 ; $IGraph5V+=40){
      $pdf->setXY($Graph5V,142);
      $pdf->Cell(9.5,6,$IGraph5V,0,0,'R',0);
      $Graph5V = $Graph5V + 11; 
    } 
    //text Graphe 4
    $arrayGraph5T = array("Sérieux \n Enjoué \n Conforme \n Transgressif","Maîtrise \n Sympathie \n Autrui \n Soi");
    $Graph5VTY = 74;
    for($IGraph5VTY = 0; $IGraph5VTY < 2; $IGraph5VTY++){
      $pdf->setXY(2,$Graph5VTY); 
      $pdf->MultiCell(32,4, iconv("UTF-8", "windows-1252",$arrayGraph5T[$IGraph5VTY]), "0", 'R', 1);
      $Graph5VTY = $Graph5VTY + 35;
    }

    $arrayTotalG5 = array($TotalSE + $TotalCT,$TotalSM + $TotalSA);
    $Graph5DrawY = 70;
    $pdf->SetFont('RobotoCondensed-Bold','',10);
    for($IGraph5DrawY = 0; $IGraph5DrawY < 2 ; $IGraph5DrawY++){
      $pdf->setXY(36.2,$Graph5DrawY);
      $pdf->setFillColor(230,230,230);
      $x = $arrayTotalG5[$IGraph5DrawY];
      $x = (($x * 11) / 40 ) - 28;
      if($x > 0)
        $pdf->Cell($x,25,$arrayTotalG5[$IGraph5DrawY],"",0,'C',1);
      $Graph5DrawY = $Graph5DrawY + 35; 
    }

    ///////////////END Page 18 ///////////////////////////




    /////////////PAGE 19 à 21 /////////////////////////
    $pdf->addPage();
    $pageId = $pdf->importPage(19, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(20, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(21, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->Image($path.'img/observation-4.png',8,48); 
    $Graph4DrawY = 57.5;
    $pdf->setFillColor(230,230,230); 
    // $arrayGraph1DrawY = array(number_format($serieuxTotal),
    //        number_format($enjouTotal),
    //        number_format($conformTotal),
    //        number_format($transagressiTotal),
    //        number_format($maitriseTotal),
      //      number_format($sympathieTotal),
      //      number_format($autruiTotal),
      //      number_format($soiTotal)
    //      );

    $obs4 = [
      [0,1], [2,3], [4,5], [6,7]
    ];
    for($i = 0; $i < 4 ; $i++){
      $pdf->setXY(83,$Graph4DrawY);
      $pdf->setFillColor(230,230,230);
      $x = $arrayTotalG4[$i];
      $x = ($x * 11) / 20;
      // x = 10 <= 20
            // <= x - 100

      if($x > 0){
        $val = $arrayGraph1DrawY[$obs4[$i][0]] + $arrayGraph1DrawY[$obs4[$i][1]];
        // $val = 120;
        $val_graph = ( ($val-100) * 10)/20;
        $pdf->Cell($val_graph,5,$val,"",0,'C',1);
      }
      $Graph4DrawY = $Graph4DrawY + 9 ; 
    }

    $pdf->addPage();
    $pageId = $pdf->importPage(22, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(23, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(24, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(25, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->addPage();
    $pageId = $pdf->importPage(26, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->Image($path.'img/observation-6.png',30,38,150);
    $obs4 = [
      [0,1], [2,3], [4,5], [7,6]
    ];
    $Graph4DrawY = 45;
    $pdf->setFillColor(230,230,230); 

    for($i = 0; $i < 4 ; $i++){
      $pdf->setXY(106,$Graph4DrawY);
      $x = $arrayTotalG4[$i];
      $x = ($x * 11) / 20;
      // x = 10 <= 20
            // <= x - 100

      if($x > 0){
        $val = $arrayGraph1DrawY[$obs4[$i][0]] - $arrayGraph1DrawY[$obs4[$i][1]];
        // $val = 20;
        $val_graph = ( ($val) * 10.8)/10;
        $pdf->Cell($val_graph,5.7,$val,"",0,'C',1);
      }
      $Graph4DrawY = $Graph4DrawY + 10  ; 
    }

    for ($i=27; $i <36 ; $i++) { 
      $pdf->addPage();
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }


    /////////////END PAGE 19 à 21 /////////////////////////

    $pdf->Output($txtRa.".pdf","I");
  }

   /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      
      ReponseRepondant::where("test_id",$id)->delete();
      TestRepondant::where("test_id",$id)->delete();
      $users = User::where("test_id", $id)->get();
      foreach ($users as $key => $user) {
        $user->test_id = NULL;
        $user->save();
      }
      Test::destroy($id);
      return redirect()->route('isma.index');
  }

  public function coache(){
    // $reps = TestRepondant::paginate(10);
    // return view('isma/coache', ["reps" =>  $reps]);
  }

  public function export(){
    /*$excel = "";
    $excel .= "Consultant\tCoaché\tDate\n";

    $reps = TestRepondant::all();

    foreach($reps as $row) {
      $excel .= "$row->id\t$row->repondant_id\t$row->question_id\t$row->test_id\n";
    }

    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: attachment; filename=Module survey.xls");

    print mb_convert_encoding($excel, 'UTF-16LE', 'UTF-8');;
    exit;*/
  }

}

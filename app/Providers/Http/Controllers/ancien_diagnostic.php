<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Test;
use App\Type;
use App\TestRepondant;
use App\ReponseRepondant;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class DiagnosticController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      return view("diagnostic/home");
    }

    public function inscription(){
      $pays = ["France","Madagascar"];
      return view("diagnostic/inscription",['pays' => $pays]);
    }

    public function connexion(){
      // if(isset(Auth::user()->id)){
      //   return view('diagnostic/home');
      // }
      
      return view("diagnostic/home");
    }

    public function connexion_test(){
      return view("diagnostic/connexion"); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
      if($_POST){

        $user = new User();

        $this->validate($request, [
          'email' => 'required',
          'mdp' => 'required',
        ]);

        $user->email = $request->input('email');
        $user->password = $request->input('mdp');
        
        $count = User::where('email', '=' ,$user->email)->where('code', '=' ,$user->code)->count(); 
        if($count == 1){
          return view("diagnostic/home");
        } else {
          redirect('/diagnostic/connexion');
        }
      }
    }

    public function home(){
      return view("diagnostic/home"); 
    }


    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {

      if(isset($request->redirect)){
        \Session::put('isma',$request->redirect);
      }

      $credentials = $request->only('email', 'password');

      if (Auth::attempt($credentials)) {

        // test si repondant existe sans un test
        $test_rep = TestRepondant::where('repondant_id', '=' , Auth::user()->id)->count();


        if($test_rep == 0){
          return redirect('diagnostics');
        } else {

          // select tous les test_id pour le repondant_id
          $test_idrep = TestRepondant::where('repondant_id', '=' , Auth::user()->id)->select('test_id')->get();

          foreach ($test_idrep as $key => $value) {

            // verifier si la reponse du test pour l'user est vide ou non
            $rep_user = ReponseRepondant::where('repondant_id', '=' , Auth::user()->id)/*->where('test_id', '=' , $value->test_id)*/->count();

            // pour avoir le feedback le plus recent


            if($rep_user == 0){

              // get type_id dans table test pour savoir si test == climage ou feedback
              $type_id = Test::where('id', '=', $value->test_id)->select('type_id')->first();

              // get type test si climage ou feedback par type_id
              $test_type = Type::where('id', '=', $type_id->type_id)->select("name")->first();
                
              $verif_test = explode("-", $test_type->name);

              // si test == climage
              if(strtolower($verif_test[0]) == "climage"){

                $codes = Test::where('type_id', '=' ,$type_id->type_id)->select('id','code')->first();
                if(isset($codes)){
                  return redirect()->route('climage-front-personal', ['id' => $codes->id, "code" => $codes->code]);
                }

              } else if(strtolower($verif_test[0]) == "feedback"){
              // si test == feedback 
                $codes = Test::where('type_id', '=' ,$type_id->type_id)->select('id','code')->first();
                if(isset($codes)){
                  return redirect()->route('feedback-front-questions', ['id' => $codes->id, "code" => $codes->code]);
                }
              }
            } 
          }
          return view("diagnostic/home", ["dataIsma" => "isma"]);
        }
      }
      return redirect()->back()->withInput()->with('error_log', 'Email ou mot de passe incorrect');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      if($_POST){

        if(!empty($request->nom) && !empty($request->prenom) && !empty($request->email) && !empty($request->password) && !empty($request->confirm_mdp)){

          if(strlen($request->password)>=6){
            if($_POST["password"] == $_POST["confirm_mdp"]){

              $this->validate($request, [
                'email' => 'email',
              ]);

              $user = new User();

              $user->username = $request->input('prenom')." ".$request->input('nom');
              $user->firstname = $request->input('nom');
              $user->lastname = $request->input('prenom');
              $user->email = $request->input('email');
              $user->civility = $request->input('civilite');
              $user->password = bcrypt($request->input('password'));
              $user->organisation = $request->input('organisation');
              $user->age = $request->input('age');
              $user->language = $request->input('pays');
              $user->pays = $request->input('pays');
              $user->role = "user";
              $user->code = $request->input('password');
              //$user->code = bcrypt($request->input('mdp'));

              $count = user::where('email', '=' ,$user->email)->count();
              if($count == 0) {

                $user -> save();

                $credentials = $request->only('email', 'password');

                if (Auth::attempt($credentials)) {
                    // Authentication passed...
                    return redirect()->route('diagnostics')->with('success', 'Données enregistrées');
                }

                //$user->save();
                //var_dump("okkk");
                //return redirect('diagnostic')->with('success', 'Données enregistrées'); 


              } else {
                return redirect('/diagnostics/inscription')->with('error_email', 'Email existante,veuillez entrer un autre');  
              }
            } else {
              return redirect('/diagnostics/inscription')->with('error_mdp', 'Mot de passe incorrecte');
            }   
          } else {
            return redirect('/diagnostics/inscription')->with('error_court', 'Mot de passe trop court');
          }

        } else {
          return redirect('/diagnostics/inscription')->with('error_vide', 'Champs obligatoire');
        }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deconnexion(){
      Auth::logout();
      return view("diagnostic/home");
    }

  }

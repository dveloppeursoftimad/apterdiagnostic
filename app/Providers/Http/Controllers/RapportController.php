<?php 
namespace App\Http\Controllers;
//require_once __DIR__.'\..\..\../vendor/autoload.php';
// http://www.chartphp.com/demo/
use Illuminate\Http\Request;
use App\User;
use App\Test;
use App\Type;
use App\Groupe;
use App\TestRepondant;
use App\Question;
use App\ReponseRepondant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Hisune\EchartsPHP\ECharts;
use \Hisune\EchartsPHP\Config;
use Hisune\EchartsPHP\Doc\IDE\XAxis;

class RapportController extends Controller
{
	private $groupeArray;

	 public function __construct()
    {
        $this->middleware('auth');
    }
	public function index($value='')
	{
		# code...
		echo 'rapport';
	}
	public function getBeneficiaire($idTest){
		$testData = DB::table('test_repondant')->where(['test_id'=>$idTest]);
		if($testData->count()>0){
			for($i=0;$i<$testData->count();$i++){
				$repondant = DB::table('users')->where(['id'=>$testData->get()[$i]->repondant_id]);
				if($repondant->get()[0]->role == 'beneficiaire'){
					$beneficiaire = $repondant->get()[0];
					break;
				}
			}
			return $beneficiaire;
		}
		else{
			return ['id'=>-1];
		}
	}
	public function taux($numerateur,$denominateur)
	{
		# code...
		$retour = ($numerateur/$denominateur)*100;
		return $retour;
	}
	
	public function getNbrRepondantArepondu($idTest){
		$string = $this->setGroupeArrayString($idTest);
		$groupe = $this->groupeArray;
		$retour =0;
		foreach ($groupe as $key => $value) {
			# code...
			$retour = $retour + $value["nbrReponse"];
		}
		return $retour;
	}
	public function setGroupeArrayString($idTest)
	{
		# code...
		$test = DB:: table('test_repondant')->where(['test_id'=>$idTest]);
		$groupe_id=-1;
		$number=1;
		$retour=[];
		for($i=0;$i<$test->count();$i++){
			$toPush = DB::table('users')->where(['id'=>$test->get()[$i]->repondant_id])->get()[0]->groupe_id;
			if($groupe_id != $toPush){
				array_push($retour,['abbr'=>Groupe::find($toPush)->abbreviation,'name'=>Groupe::find($toPush)->name,'id'=> $toPush,'nbrMembre'=>$this->nombreMembre($toPush,$idTest),'nbrReponse'=>$this->reponseParMembre($toPush,$idTest)]);
				$groupe_id =$toPush;
			}
		}
		$this->groupeArray = $retour;
		$groupe_string ='';
		foreach ($retour as $key => $value) {
	    		$groupe_string.=$value["name"]." (".$value["abbr"].") : ".$value["nbrReponse"]." / ".$value["nbrMembre"]."</w:t><w:br/><w:t xml:space='preserve'>";
	    	}
		return $groupe_string;
	}
	public function nombreMembre($groupe_id,$idTest)
	{
		# code...
		$test = DB::table('test_repondant')->where(['test_id'=>$idTest]);
		$nombre=0;
		for($i=0;$i<$test->count();$i++){
			if(User::find($test->get()[$i]->repondant_id)->groupe_id==$groupe_id){
				$nombre++;
			}
		}
		return $nombre;
	}
	public function checkTerminerQuestion($idTest,$repondant_id)
	{
		# code...
		$retour = false;
		$reponse= DB:: table('reponse_repondant')->where(['test_id'=>$idTest,'repondant_id'=>$repondant_id]);
		for($i=0;$i<$reponse->count();$i++){
			if($reponse->get()[$i]->reponse_text !=NULL){
				$retour= true;
				break;
			}
		}
		return $retour;
	}
	public function reponseParMembre($groupe_id,$idTest)
	{
		# code...
		$user = User::all();
		$nombre =0;
		for($i=0;$i<$user->count();$i++){
			if($this->checkTerminerQuestion($idTest,$user[$i]->id)){
				if($user[$i]->groupe_id == $groupe_id){
					$nombre++;
				}
			}		
		}
		return $nombre;
	}
	public function getRepondantsFrom($idTest='')
	{
		# code...
		$users = DB::table('test_repondant')->where(['test_id'=>$idTest]);
		return $users;
	}
	public function tracerBarHorizontale($group,$val,$question="Question lava be?")
	{
		# code...
		$chart = new ECharts();
	    $chart->title = [
	        "text" => ""
	    ];
	    $chart->tooltip->trigger = 'axis';

	    $chart->tooltip->show = true;
	    $chart->type = "bar";
	    $chart->width = 500;
	    $chart->backgroundColor = "#fff";
	    $chart->color = ["#2EC7C9", "#5AB1EF", "#B6A2DE", "#FFB980", "#D87A80", "#8D98B3"];
	    $chart->itemStyle->normal->barBorderRadius = 50;

	    $chart->tooltip->trigger = 'axis';
	    $chart->legend->data = ['VOUS','HIER', "FONCT", "PAIR"];
	    $chart->yAxis[] = array(
	        'type' => 'category',
	        'data' => array($question)
	    );
	    $xAxis = new XAxis();
	    $xAxis->type = 'value';
	    $xAxis->max = 4;
	    $xAxis->min = 0;
	    $chart->addXAxis($xAxis);
	    for($i=0; $i<count($group); $i++){
	        if($i == 0){
	                $chart->series[] = array(
	                'name' => $group[$i],
	                'type' => 'bar',
	                'data' => array($val[$i]),
	                "itemStyle" => [
	                    "normal"=> [
	                        "barBorderRadius" => 15
	                    ]
	                ],
	                "label" => [
	                    "normal"=> [
	                        "show" => true,
	                        "formatter" => '{a} : {c}'
	                    ]
	                ],
	                "markLine" => [
	                    "data" => [
	                        ["name" => 'Poitn de bascule', "xAxis" => 2.5, "itemStyle" => ["normal" =>["color" => '#dc143c',"label" => ["position" =>'top'] ] ] ]
	                    ]
	                ]
	                );
	        }else{

	                $chart->series[] = array(
	                'name' => $group[$i],
	                'type' => 'bar',
	                'data' => array($val[$i]),
	                "itemStyle" => [
	                    "normal"=> [
	                        "barBorderRadius" => 15
	                    ]
	                ],
	                "label" => [
	                    "normal"=> [
	                        "show" => true,
	                        "formatter" => '{a} : {c}'
	                    ]
	                ],
	            );
	        }
	    }
	    return  $chart->render('simple-custom-id');
	}
	public function rapport($id) // id = id du test
	{
		# code...  
        
        $group = ['VOUS','HIER', "FONCT", "PAIR"];
		$val = [3,3.6, 2.7, 3.4];
		//$val = [2,4, 3.7, 3.4];
		// return view('rapport/rapport',['img'=>$this->tracerBarHorizontale($group,$val,'question nataoko no eto'),'id'=>$id]);
        echo $this->tracerBarHorizontale($group,$val,'question nataoko no eto');
        die();
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->getCompatibility()->setOoxmlVersion(15);
        $template = new \PhpOffice\PhpWord\TemplateProcessor(__DIR__.'/templateFolder/Template360FBML.docx');
        $target_dir =url('/public/img/uploads/');
    		if (!file_exists($target_dir.$id."/")) {
			    mkdir($target_dir.$id."/", 0777, true);
			}
			$target_dir = $target_dir.$id."/";

			$template_dir = __DIR__.'/templateFolder/';

    		
    		$template = new \PhpOffice\PhpWord\TemplateProcessor($template_dir.'Template360FBML.docx');

			$template->setValue('ben_initiales', $this->getBeneficiaire($id)->initiale);
			$template->setValue('ben_societe', $this->getBeneficiaire($id)->organisation);
			// $template->setValue('cons_firstname', $consultant->user_firstname);
			// $template->setValue('cons_lastname', $consultant->user_lastname);
			$template->setValue('cons_firstname', "Laurence");
			$template->setValue('cons_lastname', "VANDEVENTER");

			setlocale (LC_TIME, 'fr_FR.utf8','fra');
			
			$template->setValue('date', strftime("%A %d %B %Y."));
			//strftime("%A %d %B %Y.")   date("D M d, Y")
			$template->setValue('nb_reponses', $this->getNbrRepondantArepondu($id));
			$template->setValue('nb_repondants', $this->getRepondantsFrom($id)->count());
			$template->setValue('taux_reponse', round($this->taux($this->getNbrRepondantArepondu($id),$this->getRepondantsFrom($id)->count())));
			$template->setValue('ben_civilite', $this->getBeneficiaire($id)->civility);
			$template->setValue('ben_firstname', $this->getBeneficiaire($id)->username);
			$template->setValue('ben_lastname', $this->getBeneficiaire($id)->lastname);
			$template->setValue('quest_type', Test::find($id)->name/*htmlspecialchars($quest->type)*/ );
			$template->setValue('groupesArray', $this->setGroupeArrayString($id));
			if(file_exists(__DIR__."/templateFolder/Rapport.docx")){
				unlink(__DIR__."/templateFolder/Rapport.docx");
			}
       		$template->saveAs(__DIR__."/templateFolder/Rapport.docx");

       		/*$chart = new ECharts();
			$chart->tooltip->show = true;
			$chart->legend->data[] = 'legende';
			$chart->xAxis[] = array(
			    'type' => 'value'
			);
			$chart->yAxis[] = array(
				'type' => 'category',
			    'data' => array("data 1","data 2","data 3","data 4","data 5","data 6")
			    
			);
			$chart->series[] = array(
			    'name' => 'legende',
			    //'type' => 'radar',
			    'type' => 'bar',
			    'data' => array(5, 20, 35, 10, 10, 20)
			);
			echo $chart->render('simple-custom-id');
       		die();*/
			$template->cloneRow('question_img_array', count($datas['data_question_img']));
			
    		
			//save questimage
			$i = 1;
			foreach ($datas['data_question_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."question_img_".$key.".png", $unencodedData);

	    		$template->setImg('question_img_array#'.($i),array('src' => $target_dir."question_img_".$key.".png",'swh'=>'580'));
	    		$i++;
			}

			
			$template->cloneRow('detail_img_array', count($datas['data_detail_img']));
			
			$i = 1;
			//save datail img
			foreach ($datas['data_detail_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."detail_img_".$key.".png", $unencodedData);
	    		$template->setImg('detail_img_array#'.($i),array('src' => $target_dir."detail_img_".$key.".png",'swh'=>'550'));
	    		
	    		$i++;
			}

			$template->cloneRow('principal_img_array', count($datas['data_principal_img']));
			$i=1;
			//save principal img
			foreach ($datas['data_principal_img'] as $key => $value) {
				$imageData = $value;
				$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    		$unencodedData=base64_decode($filteredData);
	    		
	    		file_put_contents($target_dir."principal_img_".$key.".png", $unencodedData);
	    		$template->setImg('principal_img_array#'.($i),array('src' => $target_dir."principal_img_".$key.".png",'swh'=>'550'));
	    		$i++;
			}

			//save question ouverte img
			///GET question ouverte

			// $template->cloneRow('question_ouverte_img_array', count($datas['data_question_ouverte_img']));

			// $i=1;
			// foreach ($datas['data_question_ouverte_img'] as $key => $value) {
			// 	$imageData = $value;
			// 	$filteredData=substr($imageData, strpos($imageData, ",")+1);
	  //   		$unencodedData=base64_decode($filteredData);
	    		
	  //   		file_put_contents($target_dir."question_ouverte_img_".$key.".png", $unencodedData);
	  //   		$template->setImg('question_ouverte_img_array#'.($i),array('src' => $target_dir."question_ouverte_img_".$key.".png",'swh'=>'580'));
	  //   		$i++;
			// }
			////QUESTIONS OUVERTES VERBATIN
			//Lister les groupes
			$repondants = $repondantModel->find([
	    			'includes' => ['ApterGroupe' => ['selects'=> 'ApterGroupe.*']],
	    			'conditions' => [
	    				
	    				'ApterRepondant.apter_questionnaire_id' => $this->params['data']['id']
	    			]	
	    		]);

			
			$groupe= [];
			foreach ($repondants as $key => $value) {
				
				if(!array_search($value->apter_groupe->abreviation, $groupe) && $value->apter_groupe->abreviation !== "BEN"){
					$groupe[] = $value->apter_groupe->abreviation;
				}
				
			}

			//Grouper les répondants
			$repondants_groupe = [];
			foreach ($repondants as $key => $value) {
				if($value->apter_groupe->abreviation !== "BEN" && $value->reponse != NULL && $value->reponse != ""){
					if(!array_key_exists($value->apter_groupe->abreviation, $repondants_groupe)){
						$repondants_groupe[$value->apter_groupe->abreviation] = [];
						$repondants_groupe[$value->apter_groupe->abreviation][] = $value;
					}else{
						$repondants_groupe[$value->apter_groupe->abreviation][] = $value;
					}
				}
					
				
			}

			// var_dump($repondants_groupe);
			
		

			//Lister les questions ouvertes
			$questions_ouvertes = [];
			foreach ($questionnaire->apter_pages as $key => $page) {
				foreach ($page->apter_questions as $key => $question) {
					if($question->type == "text"){
						$questions_ouvertes[] = $question;
					}
				}
			}
			// var_dump($repondants_groupe);


			//Afficher les réponses pour chaque question ouverte
			$text  = "";
			foreach ($questions_ouvertes as $key => $value) {
				$text .= " - ".$value->question."</w:t><w:br/><w:t xml:space='preserve'>";
				foreach ($repondants_groupe as $key2 => $groupe) {
					$text .= $key2."</w:t><w:br/><w:t xml:space='preserve'>";
					foreach ($groupe as $key3 => $rep) {
						
						$reponse_cur = reset(array_filter(json_decode($rep->reponse), function($elt) use ($value){
							
							return ((int)$elt->question_id == $value->id) || ((int)$elt->question_id == (int)$value->id +339) || ($elt->question_id == (int)$value->id -339);
						})) ;
						
						$text .= htmlspecialchars($reponse_cur->text, ENT_COMPAT, 'UTF-8').", ";
						
					}
					$text .= "</w:t><w:br/><w:t xml:space='preserve'>";
				}
				$text .= "</w:t><w:br/><w:t xml:space='preserve'></w:t><w:br/><w:t xml:space='preserve'>";

			}

			$template->setValue('question_ouverte_img_array', $text);
			


			//save radar img
			$imageData = $datas['img_radar'];
			$filteredData=substr($imageData, strpos($imageData, ",")+1);
	    	$unencodedData=base64_decode($filteredData);
	    		
	    	file_put_contents($target_dir."radar_img.png", $unencodedData);
	    	$template->setImg('radar_img',array('src' => $target_dir."radar_img.png",'swh'=>'550'));


    		
	    	
			
			/*$i = 1;
			foreach ($datas['data_question_img'] as $key => $value) {
				$template->setImg('question_img_array#'.($i),array('src' => $target_dir."question_img_".$key.".png",'swh'=>'550'));
				$i = $i+1;
			}*/
			if(file_exists($target_dir."Rapport_".$quest->type.".docx")){
				unlink($target_dir."Rapport_".$quest->type.".docx");

			}
			$template->saveAs($target_dir."Rapport_".$quest->type.".docx");
			if(file_exists($target_dir."Rapport_".$quest->type.".docx")){
				$reponse["generated"] = true;
			}
        //$template->applyXslStyleSheet();
       /* if(file_exists(__DIR__."/templateFolder/Rapport.docx")){
				unlink(__DIR__."/templateFolder/Rapport.docx");

			}
        $template->saveAs(__DIR__."/templateFolder/Rapport.docx");
       */
	}
	public function allQuestion($idTest='')
	{
		# code...
		$toJson = DB::table('question')->get();
		echo json_encode($toJson);
	}
	public function getReponseRepondant($idTest ='')
	{
		# code...
		$toJson = DB::table('reponse_repondant')->leftJoin('reponse', 'reponse_repondant.reponse_id', '=', 'reponse.id')->leftJoin('users','reponse_repondant.repondant_id','=','users.id')
            ->get();
		echo json_encode($toJson);
	}

}
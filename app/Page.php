<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $page_id
 * @property int $type_id
 * @property int $sequence
 * @property string $title_ben
 * @property string $subtitle_ben
 * @property string $title_fr
 * @property string $subtitle_fr
 * @property string $title_ben_fr
 * @property string $subtitle_ben_fr
 * @property string $title_en
 * @property string $subtitle_en
 * @property string $title_ben_en
 * @property string $subtitle_ben_en
 * @property Page $page
 * @property Type $type
 * @property Question[] $questions
 */
class Page extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'page';

    /**
     * @var array
     */
    protected $fillable = ['page_id', 'type_id', 'sequence', 'title_ben', 'subtitle_ben', 'title_fr', 'subtitle_fr', 'title_ben_fr', 'subtitle_ben_fr', 'title_en', 'subtitle_en', 'title_ben_en', 'subtitle_ben_en'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}

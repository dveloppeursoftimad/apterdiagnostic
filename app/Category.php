<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $category_id
 * @property string $name
 */
class Category extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'category';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    //public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['name','type_id'];

    public function categorieQuestion()
    {
        return $this->belongsToMany('App\Question', 'category_question','category_id','question_id');
    }

}

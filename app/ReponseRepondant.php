<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property int $test_id
 * @property int $reponse_id
 * @property string $reponse_text
 * @property Question $question
 * @property Reponse $reponse
 * @property Test $test
 * @property User $user
 * @property timestamp $created_at
 */
class ReponseRepondant extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'reponse_repondant';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'question_id', 'test_id', 'reponse_id', 'reponse_text','created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reponse()
    {
        return $this->belongsTo('App\Reponse', 'reponse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', "repondant_id");
    }
}

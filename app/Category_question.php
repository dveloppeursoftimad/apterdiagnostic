<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_question extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'category_question';

    /**
     * @var array
     */

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = ['category_id', 'question_id'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }
}

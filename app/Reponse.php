<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $question_id
 * @property string $reponse_fr
 * @property string $reponse_en
 * @property string $score
 * @property Question $question
 * @property ReponseRepondant[] $reponseRepondants
 */
class Reponse extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'reponse';

    /**
     * @var array
     */
    protected $fillable = ['question_id', 'reponse_fr', 'reponse_en', 'score'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponseRepondants()
    {
        return $this->hasMany('App\ReponseRepondant');
    }
}

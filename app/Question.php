<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/** 
 * @property int $id
 * @property int $page_id
 * @property int $type_id
 * @property string $question_fr_ben
 * @property string $question_fr
 * @property string $question_en_ben
 * @property string $question_en
 * @property string $type
 * @property boolean $require
 * @property boolean $negative
 * @property int $sequence
 * @property string $titre
 * @property Page $page
 * @property Type $type
 * @property Reponse[] $reponses
 * @property ReponseRepondant[] $reponseRepondants
 */
class Question extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'question';

    /**
     * @var array
     */
    protected $fillable = ['page_id', 'type_id', 'question_fr_ben', 'question_fr', 'question_en_ben', 'question_en', 'type', 'require', 'negative', 'sequence', 'titre'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponses()
    {
        return $this->hasMany('App\Reponse');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponseRepondants()
    {
        return $this->hasMany('App\ReponseRepondant');
    }
}

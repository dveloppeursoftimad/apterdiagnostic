<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $num_page
 * @property int $progreesion
 * @property string $page_id
 * @property string $repondant_id
 */

class Terminer extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'terminer';

    /**
     * @var array
     */
    protected $fillable = ['id','num_page', 'progression', 'page_id','repondant_id','test_id'];
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * @var array
     */
    protected $fillable = ['groupe_id', 'test_id', 'username', 'email', 'password', 'create_time', 'firstname', 'lastname', 'phone', 'organisation', 'civility', 'language', 'role', 'avatar_url', 'service', 'function', 'manager', 'age', 'pays', 'email_verified_at', 'remember_token', 'created_at', 'updated_at', 'code','type','mail_send','initiale'];

    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];


    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

   /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function groupe()
    {
        return $this->belongsTo('App\Groupe');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function consultantAccesses()
    {
        return $this->hasMany('App\ConsultantAccess', 'consultant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponseRepondants()
    {
        return $this->hasMany('App\ReponseRepondant', 'repondant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tests()
    {
        return $this->hasMany('App\Test', 'consultant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testRepondants()
    {
        return $this->hasMany('App\TestRepondant', 'repondant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testAccess()
    {
        return $this->belongsToMany('App\Type', 'consultant_access','consultant_id','type_id');
    }

    public function mail()
    {
        return $this->hasMany('App\Mail', 'consultant_id');
    }


}

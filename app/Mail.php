<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $type_id
 * @property string $type
 * @property string $mail
 * @property string $subject
 * @property Type $type
 */
class Mail extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mail';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['type_id', 'type', 'mail', 'subject','language','active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Type;
use Closure;

class ActiAcess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cons_access = Auth::user()->TestAccess()->get();

        if(Auth::user()->role == "admin"){
            return $next($request);
        }

        if(!is_null($cons_access)){
            foreach ($cons_access as $key => $value) {

                $test = $value->name;
                

                if($test == "acti"){

                    return $next($request);
                }
            }

            return redirect()->route('403');
        }
    }
}

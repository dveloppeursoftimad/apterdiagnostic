<?php

namespace App\Http\Middleware;

use Closure;

class RepondantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user()) {
            // die();
            return redirect()->route('connexion');
        }
        return $next($request);
    }
}

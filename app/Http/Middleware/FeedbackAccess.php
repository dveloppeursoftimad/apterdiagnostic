<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Type;
use Closure;

class FeedbackAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $cons_access = Auth::user()->TestAccess()->get();

        if(!is_null($cons_access)){
            foreach ($cons_access as $key => $value) {

                $test = explode("-", $value->name);
                

                if($test[0] == "feedback"){

                    return $next($request);
                }
            }

            return redirect()->route('403');
        }
    }
}

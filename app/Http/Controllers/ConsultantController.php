<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Type;
use App\Groupe;
use App\Test;
use App\Mail;
use App\TestRepondant;
use App\ReponseRepondant;
use App\ConsultantAccess;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Auth;
use Image;
class ConsultantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        //
        $users = User::where("role", 'admin')->orWhere("role", "consultant")->orWhere("role", "partenaire")->orWhere("role", "admin_acad")->orWhere("role", "admin_rech_acad")->orderBy("created_at","desc")->get();

        foreach ($users as $key => $user) {
          $user->typeinv = DB::table('consultants')->where(['email'=>$user->email, 'username'=>$user->username])->select("renvoye_mail")->first();

          // recuperer les anciens consultants
          if($user->created_at <= '2019-07-01 00:00:00'){
            $checkCons = DB::table('consultants')->where(['email'=>$user->email, 'username'=>$user->username]);
            if($checkCons->count()>0){
              \DB::table('consultants')->where(["email"=>$user->email, 'username'=>$user->username])->update(
                ['renvoye_mail' => 1]);
            } else {
              DB::table('consultants')->insert(['email' => $user->email, 'username'=>$user->username]);
            }
          }
        }

        $types = Type::where("name", "NOT LIKE", "%climage%")->get();
        $climage = Type::where("name", "LIKE", "%climage%")->first();
        $climage->description = "Climage";
        $climage->name= "Climage";
        $types[] = $climage;
        $repondant=[];
        foreach ($users as $key1 => $user) {

          //foreach ($user->tests as $key2 => $test) {
            // CLIMAGE
            $climagesUser = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%climage%");
            })->where("consultant_id", "=", $user->id)->get();

            $cli_user[$user->id] = [];            
            foreach ($climagesUser as $key2 => $climageUser) {
              array_push($cli_user[$user->id], $climageUser->name." - ".date('Y-m-d', strtotime($climageUser->created_at)));
              //$cli_user[5] = $climageUser->name." - ".date('Y-m-d', strtotime($climageUser->created_at));
            }

            $imp_cliUser[$user->id] = [];
            if(array_key_exists($user->id,$cli_user)){
              $imp_cliUser[$user->id] = implode(", ",(array)$cli_user[$user->id]);
            }

            // FEEDBACK
            $fb_user[$user->id] = [];
            $fbsUser = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%feedback%");
            })->where("consultant_id", "=", $user->id)->distinct()->get();
            foreach ($fbsUser as $key2 => $fbUser) {
              array_push($fb_user[$user->id], $fbUser->name." - ".date('Y-m-d', strtotime($fbUser->created_at)));
            }

            $imp_fbUser[$user->id] = [];
            if(array_key_exists($user->id, $fb_user)){
              $imp_fbUser[$user->id] = implode(", ",(array)$fb_user[$user->id]);
            }

            // ISMA
            $isma_user[$user->id] = [];
            $ismasUser = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%isma%");
            })->where("consultant_id", "=", $user->id)->distinct()->get();
            foreach ($ismasUser as $key2 => $ismaUser) {
              array_push($isma_user[$user->id], $ismaUser->name." - ".date('Y-m-d', strtotime($ismaUser->created_at)));
            }

            $imp_ismaUser[$user->id] = [];
            if(array_key_exists($user->id, $isma_user)){
              $imp_ismaUser[$user->id] = implode(", ",(array)$isma_user[$user->id]);
            }

            // ACTI
            $acti_user[$user->id] = [];
            $actisUser = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%acti%");
            })->where("consultant_id", "=", $user->id)->distinct()->get();
            foreach ($actisUser as $key2 => $actiUser) {
              array_push($acti_user[$user->id], $actiUser->name." - ".date('Y-m-d', strtotime($actiUser->created_at)));
            }

            $imp_actiUser[$user->id] = [];
            if(array_key_exists($user->id, $acti_user)){
              $imp_actiUser[$user->id] = implode(", ",(array)$acti_user[$user->id]);
            }
          //}
          
        }


        //return view('consultant/index',['users' =>$users , 'types' => $types, "userClimages"=>$climagesUser, "imp_cliUser"=>$imp_cliUser, "imp_fbUser"=>$imp_fbUser, "imp_ismaUser"=>$imp_ismaUser, "imp_actiUser"=>$imp_cliUser, "imp_actiUser"=>$imp_actiUser,"mails"=>$mails,"mail"=>$mail]);

        
        $mails = Mail::where("type", "consultant" )->where("active", "=", 1)->where("language", \App::getLocale())->get();

        foreach ($mails as $key => $m) {
          if(!is_null($m->consultant_id)){
            $m->user = User::find($m->consultant_id)->username;
          }
        }

        $mail = $mails[0];

        // $blo=User::whereDoesntHave('test',function($query){
        //   $query->where('id',6);
        // })->get(); 
        $types2=Type::all();
       //echo '<pre>';
        //var_dump($types2);
        $tests=[];
        
        foreach ($types2 as $type) {
          $a=Test::where('type_id',$type['id'])->join("users", "test.consultant_id", "=", "users.id")->select('test.id AS id', 'test.name AS testName','test.updated_at as ua', 'users.firstname AS firstname', 'users.lastname AS lastname', 'users.organisation AS organisation', 'users.role as role')->get();
          $tests[$type->id]=$a;
           //echo $type['name'].' <br>';
        }
       
        /*foreach($tests as $test){
         * echo $test['organisation'];
        }*/
        return view('consultant/index',['users' =>$users , 'types' => $types, "repondants"=>$repondant,"mails"=>$mails,"mail"=>$mail, "types2"=>$types2 , "tests"=> $tests]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'lastname' => 'required',
            'firstname' => 'required',
            'email' => 'required'

        ]);
        
        $count = user::where('email', '=' ,$request->email)->where("role", "consultant")->count();
        if($count < 1){
            $user = User::updateOrCreate(
              ["email" => $request->email],
              [
                "username" => $request->lastname.' '.$request->firstname,
                "password" => Hash::make($request->password),
                "code" => $request->password,
                "firstname" => $request->firstname,
                "lastname" => $request->lastname,
                "phone" => $request->phone,
                "organisation" => $request->organisation,
                "civility" => $request->civility,
                "language" => $request->language,
                "role" => "consultant"
              ]
            );
            if($request->file('avatar')){
                //on efface ce qui est avant ou ce qui a le meme nom
                $path =  $request->file('avatar')->storeAs(
                    'public/consultant',$user->id //$user->id est le nom du fichier a partir de maintenant
                );
                $pathExplode = explode('/', $path);
                $path =$pathExplode[1].'/'.$pathExplode[2];
                $urlPath = url('/storage/'.$path);
                $user = user::find($user->id);
                $user->avatar_url = $urlPath;
                $user->save();
            }
            return redirect('consultant');
        }else{
            return redirect('consultant')->with('error_email', 'Adresse email existante,veuillez entrer une autre');  
        }
            
        // if(!$request->language){
        //     $request->language = 'fr';
        // }
        // $count = user::where('email', '=' ,$request->email)->count();
        // if($count ==0){
        //     $role = $request->get('role') ? $request->get('role') : 'consultant';
        //         //$path = Storage::putFile('avatars', $request->file('avatar'));
        //         $dataURL = null;

        //         $userData = [
        //             'username' =>$request->get('lastname').' '.$request->firstname, 
        //             'email' =>$request->get('email'), 
        //             'password' =>Hash::make ($request->get('password')), 
        //             'code' =>$request->get('password'), 
        //             'firstname'=>$request->firstname, 
        //             'lastname'=>$request->get('lastname'), 
        //             'phone'=>$request->get('phone'), 
        //             'organisation'=>$request->get('organisation'), 
        //             'civility'=>$request->get('civility'), 
        //             'language'=>$request->get('language')? $request->get('language'):'fr', 
        //             'role'=> $role, 

        //         ];
        //         if($request->file('avatar')){
        //              $path = $request->file('avatar')->store('public/img');
        //             $types = explode(".", $path); 
        //             $img = file_get_contents(__DIR__.'/../../../storage/app/'.$path);
        //             $data = base64_encode($img);
        //             $dataURL = 'data:image/'.$types[count($types)-1].';base64,'.$data;
        //             Storage::deleteDirectory('public/img');

        //             $userData =[
        //                 'username' =>$request->get('lastname').' '.$request->firstname, 
        //                 'lastname' =>$request->get('lastname'), 
        //                 'firstname' =>$request->firstname, 
        //                 'email' =>$request->get('email'), 
        //                 'password' =>Hash::make ($request->get('password')), 
        //                 'code' =>$request->get('password'), 
        //                 'phone'=>$request->get('phone'), 
        //                 'organisation'=>$request->get('organisation'), 
        //                 'civility'=>$request->get('civility'), 
        //                 'language'=>$request->get('language')? $request->get('language'):'fr', 
        //                 'role'=> $role, 
        //                 'avatar_url'=> $dataURL      
        //             ];
        //         } 

        //         $user= new User($userData);
        //         $user->save();
        //         return redirect('consultant');
        // }
        // else{
        //   return redirect('consultant')->with('error_email', 'Email existante,veuillez entrer un autre');     
        // }
                

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
        'lastname'=>'required',
        'firstname'=>'required',
        'email'=> 'required'
      ]);
      
      $user = User::find($id);
      if($request->avatar){
        $file=$request->avatar;
        $img=Image::make($file)->resize(250,250);
        }else if($user->avatar_url){
            // $img=Image::make(asset('/storage/climage/1'))->resize(250,250);
            $img=Image::make(public_path('/storage/climage/'.$user->id) )->resize(250,250);
            //var_dump($img);
        }

        // $img->save('public/storage/climage/antsa');
        if(isset($img)){
          if($request->input('rotate')!='')
          {
              $img->rotate(-$request->input('rotate'));
          }
          if (($request->input('w')!='' && $request->input('h')!='') && ($request->input('w')!='0' && $request->input('h')!='0')) {
              $img->crop($request->input('w'), $request->input('h'), $request->input('x1'), $request->input('y1'));
          }
          $img->encode('jpg', 90);
          
          Storage::delete('public/climage/'.$user->id);
          Storage::put( $user->id. '.jpg', $img);
          Storage::move($user->id.'.jpg', 'public/climage/'.$user->id );
          // $img->crop(0, 2, 3, 1);
          // $img->save('public/storage/climage/','Antsa');
             
          
          $user->avatar_url=url('/storage/climage/'.$user->id);
        }
     
      if($request->get('password')) $user->password =Hash::make ($request->get('password'));
      if($request->get('password')) $user->code =$request->get('password');
      if($request->get('lastname')) $user->lastname =$request->get('lastname');
      if($request->get('firstname')) $user->firstname =$request->get('firstname');
      if($request->get('username')) $user->username =$request->get('lastname').' '.$request->get('firstname');
      if($request->get('email')) $user->email= $request->get('email');
      if($request->get('phone')) $user->phone =$request->get('phone');
      if($request->get('organisation')) $user->organisation=$request->get('organisation');
      if($request->get('civility')) $user->civility =$request->get('civility');
      if($request->get('language')) $user->language = $request->get('language');
      $user->save();
      return redirect('consultant');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::find($id);
        $user->delete();
        return redirect('consultant');
        /*return view('consultant/index',['users'=>$users]);*/
    }
    public function getData($id){
        $user = User::find($id);
        echo json_encode($user);
    }
    public function getConsultantAccess($id='')
    {
        # code...
        $consultantAccess = DB::table('consultant_access')->where('consultant_id', $id)->get();
        echo json_encode($consultantAccess);
    }
    
    public function setConsultantAccess(Request $request){
       /*var_dump ($request->type);
       var_dump ($request->description);
       var_dump($request->consultant_id);*/
      /* var_dump($request->admin);*/
      // var_dump($request->type);
      // die();
    
       if($request->type){
        foreach ($request->type as $key => $value) {
               # code...
               /* var_dump($key);
                var_dump($value);*/
            

            if($value == "true"){
                    # code...
                    $type = Type::find($key);
                    

                    if(strpos($type->name, 'climage') !== false){
                      // echo 'CLIMAGE';
                      $climages = Type::where("name", "like", "%climage%")->get();
                      foreach ($climages as $k => $climage) {
                            $consultantAccess = DB::table('consultant_access')->where(['consultant_id'=>$request->consultant_id,'type_id'=>$climage->id]);
                            if(count($consultantAccess->get()) == 0){
                                $consultantAccess = new ConsultantAccess([
                                    'consultant_id' => $request->consultant_id,
                                    'type_id' => $climage->id,
                                ]);
                                $consultantAccess->save();
                            }   
                      }
                     

                    }else{
                        $consultantAccess = DB::table('consultant_access')->where(['consultant_id'=>$request->consultant_id,'type_id'=>$key]);
                        if(count($consultantAccess->get()) == 0){
                            $consultantAccess = new ConsultantAccess([
                                'consultant_id' => $request->consultant_id,
                                'type_id' => $key,
                            ]);
                            $consultantAccess->save();
                        }   
                    }
                        
            }
            else if ($value == "false" || $value == NULL) {
                  $type = Type::find($key);
                 

                    if(strpos($type->name, 'climage') !== false){
                      
                      
                      $climages = Type::where("name", "like", "%climage%")->get();
                      foreach ($climages as $k => $climage) {
                            $consultantAccess = DB::table('consultant_access')->where(['consultant_id'=>$request->consultant_id,'type_id'=>$climage->id]);
                            if( $consultantAccess) {
                             $consultantAccess->delete();
                            }
                      }
                     

                    }else{
                        $consultantAccess = DB::table('consultant_access')->where(['consultant_id'=>$request->consultant_id,'type_id'=>$key]);
                        if( $consultantAccess) {
                         $consultantAccess->delete();
                        } 
                    }
                    
                }
            }
       }
        // die();
       
      
         $user = User::find($request->consultant_id);
         $user->role= $request->role;
         $user->save();
      
       return redirect('consultant');
    }

    public function createrepondantActi(Request $request,$id='')
    {
        // delete all repondant
        ReponseRepondant::where("test_id",$id)->delete();
        TestRepondant::where("test_id",$id)->delete();
        $users = User::where("test_id", $id)->get();
        foreach ($users as $key => $user) {
          $user->test_id = NULL;
          $user->save();
        }

        //Add beneficiaire
        $password=$this->randomPassword();
        $beneficiaire = User::firstOrCreate(
          ['email' => $request->beneficiaire[5]],
          [
                    'username' =>$request->beneficiaire[1].' '.$request->beneficiaire[2], 
                    'email' =>$request->beneficiaire[5], 
                    'password' =>bcrypt($password), 
                    'phone'=>$request->beneficiaire[7], 
                    'organisation'=>$request->beneficiaire[3], 
                    'civility'=>$request->beneficiaire[0], 
                    'language'=>strpos((strtolower($request->beneficiaire[6])),'fr') === false ? 'en':'fr',
                    'initiale'=>$request->beneficiaire[4],
                    'lastname'=>$request->beneficiaire[2],
                    'firstname'=>$request->beneficiaire[1],
                    'code' => $password,
                    'role'=> "user",

          ]
        );

        $beneficiaire->organisation = $request->beneficiaire[3];
        $beneficiaire->phone = $request->beneficiaire[7];
        $beneficiaire->initiale = $request->beneficiaire[4];
        $beneficiaire->save();

        //Add beneficiaire in the test
        $testben = TestRepondant::firstOrCreate(
          [
            "repondant_id" => $beneficiaire->id,
            "test_id" => $request->idTest,
            "role" => "beneficiaire",
          ],
          [
            
            "groupe_id" => $this->setGroupe('Bénéficiaire','VOUS')
          ]
        );


        $test = Test::find($request->idTest);
        $test->beneficiaire_id = $beneficiaire->id; 
        $test->save();

        //add Repondants

        if(count($request->abreviationRepondant) > 0){
          for($i=0; $i< count($request->abreviationRepondant); $i++){
            //add repondant
            $password=$this->randomPassword();
            $repondant = User::firstOrCreate(
              [
                'email' =>$request->emailRepondant[$i], 
              ],
              [
                          'username' =>$request->prenomRepondant[$i].' '.$request->nomRepondant[$i], 
                          'lastname' =>$request->nomRepondant[$i], 
                          'firstname' =>$request->prenomRepondant[$i], 
                          'email' =>$request->emailRepondant[$i], 
                          'password' =>bcrypt ($password), 
                          'organisation'=> $beneficiaire->organisation, 
                          'civility'=>$request->civiliteRepondant[$i], 
                          'language'=> strpos((strtolower($request->langageRepondant[$i])),'fr') === false ? 'en':'fr',
                          'role'=> "user",
                          'code' => $password
              ]
            );
            $repondant->language = strpos((strtolower($request->langageRepondant[$i])),'fr') === false ? 'en':'fr';
            $repondant->organisation = $beneficiaire->organisation;
            $repondant->save();
            //add repondant to test
            $testBen = TestRepondant::where([
                "repondant_id" => $repondant->id,
                "test_id" => $request->idTest,
                "role" => "beneficiaire"
              ])->count();

            if(!$testBen){
              $t = TestRepondant::firstOrNew(
                [
                  "repondant_id" => $repondant->id,
                  "test_id" => $request->idTest
                ]
              );
              $t->groupe_id = $this->setGroupe($request->groupeRepondant[$i],$request->abreviationRepondant[$i]);
              $t->role = "repondant";
              $t->save();
            }
              
            

          }
        }
        // echo "<pre>";
        //     var_dump($beneficiaire->email);
        //     echo "</pre>";
        // die();

      return  redirect ('/acti/repondantActi/'.$request->idTest);
    }

    public function createrepondant(Request $request,$id='')
    {
        // delete all repondant
        ReponseRepondant::where("test_id",$request->idTest)->delete();
        TestRepondant::where("test_id",$request->idTest)->delete();
        $users = User::where("test_id", $request->idTest)->get();
        foreach ($users as $key => $user) {
          $user->test_id = NULL;
          $user->save();
        }

        //Add beneficiaire
        $password=$this->randomPassword();
        $beneficiaire = User::firstOrCreate(
          ['email' => $request->beneficiaire[5]],
          [
                    'username' =>$request->beneficiaire[1].' '.$request->beneficiaire[2], 
                    'email' =>$request->beneficiaire[5], 
                    'password' =>bcrypt($password), 
                    'phone'=>$request->beneficiaire[7], 
                    'organisation'=>$request->beneficiaire[3], 
                    'civility'=>$request->beneficiaire[0], 
                    'language'=>strpos((strtolower($request->beneficiaire[6])),'fr') === false ? 'en':'fr',
                    'initiale'=>$request->beneficiaire[4],
                    'lastname'=>$request->beneficiaire[2],
                    'firstname'=>$request->beneficiaire[1],
                    'code' => $password,
                    'role'=> "user",

          ]
        );

        $beneficiaire->organisation = $request->beneficiaire[3];
        $beneficiaire->phone = $request->beneficiaire[7];
        $beneficiaire->initiale = $request->beneficiaire[4];
        $beneficiaire->save();

        //Add beneficiaire in the test
        $testben = TestRepondant::firstOrCreate(
          [
            "repondant_id" => $beneficiaire->id,
            "test_id" => $request->idTest,
            "role" => "beneficiaire",
          ],
          [
            
            "groupe_id" => $this->setGroupe('Bénéficiaire','VOUS')
          ]
        );


        $test = Test::find($request->idTest);
        $test->beneficiaire_id = $beneficiaire->id; 
        $test->save();

        //add Repondants

        if(count($request->abreviationRepondant) > 0){
          for($i=0; $i< count($request->abreviationRepondant); $i++){
            //add repondant
            $password=$this->randomPassword();
            $repondant = User::firstOrCreate(
              [
                'email' =>$request->emailRepondant[$i], 
              ],
              [
                          'username' =>$request->prenomRepondant[$i].' '.$request->nomRepondant[$i], 
                          'lastname' =>$request->nomRepondant[$i], 
                          'firstname' =>$request->prenomRepondant[$i], 
                          'email' =>$request->emailRepondant[$i], 
                          'password' =>bcrypt ($password), 
                          'organisation'=> $beneficiaire->organisation, 
                          'civility'=>$request->civiliteRepondant[$i], 
                          'language'=> strpos((strtolower($request->langageRepondant[$i])),'fr') === false ? 'en':'fr',
                          'role'=> "user",
                          'code' => $password
              ]
            );
            $repondant->language = strpos((strtolower($request->langageRepondant[$i])),'fr') === false ? 'en':'fr';
            $repondant->organisation = $beneficiaire->organisation;
            $repondant->save();
            //add repondant to test
            $testBen = TestRepondant::where([
                "repondant_id" => $repondant->id,
                "test_id" => $request->idTest,
                "role" => "beneficiaire"
              ])->count();

      

            if(!$testBen){
              $t = TestRepondant::firstOrNew(
                [
                  "repondant_id" => $repondant->id,
                  "test_id" => $request->idTest
                ]
              );
              $t->groupe_id = $this->setGroupe($request->groupeRepondant[$i],$request->abreviationRepondant[$i]);
              $t->role = "repondant";
              $t->save();
            }
              
            

          }
        }

        
        
        return  redirect ('/feedback/repondant/'.$request->idTest);
    }
    public function setGroupe($name,$abbreviation){
        //$name = $request->groupeRepondant[$i];
        //$request->abreviationRepondant[$i]]
        $groupe = DB::table('groupe')->where(['name'=>$name,'abbreviation'=>$abbreviation]);
        if(count($groupe->get())==0){
            $gp = new Groupe(['name'=>$name,'abbreviation'=>$abbreviation]);
            $gp->save();
        }
        return DB::table('groupe')->where(['name'=>$name,'abbreviation'=>$abbreviation])->get()[0]->id;
    }

    public function updateRepondantActi(Request $request,$id=''){
      
      $user= User::find($id);

      if($request->file('editavatar')) {     
          $user->avatar_url= $request->avatar_url;   
      }
      if($request->post('password')) $user->password =bcrypt ($request->post('password'));
      if($request->post('password')) $user->code =$request->post('password');
      if($request->post('username')) $user->username =$request->post('username');
      if($request->post('firstname')) $user->firstname =$request->post('firstname');
      if($request->post('lastname')) $user->lastname =$request->post('lastname');
      if($request->lastname && $request->firstname) $user->username = $request->lastname .' '. $request->firstname;
      if($request->post('email')) $user->email= $request->post('email');
      if($request->post('phone')) $user->phone =$request->post('phone');
      if($request->post('organisation')) $user->organisation=$request->post('organisation');
      
      if($request->post('civility')) $user->civility =$request->post('civility');
      if($request->post('language')) $user->language = strpos((strtolower($request->post('language'))),'fr') === false ? 'en':'fr';

      if($request->post('groupe_id')){
        $testRep = TestRepondant::where("test_id",$request->idTest)->where("repondant_id",$id)->first();
        $testRep->groupe_id = $request->post('groupe_id');
        $testRep->save();
      } 

      $user->save();
      return redirect('/acti/repondantActi/'.$request->idTest);
    }

    public function updateRepondant(Request $request,$id='')
    {
      $user= User::find($id);

      if($request->file('editavatar')) {     
          $user->avatar_url= $request->avatar_url;   
      }
      if($request->post('password')) $user->password =bcrypt ($request->post('password'));
      if($request->post('password')) $user->code =$request->post('password');
      if($request->post('username')) $user->username =$request->post('username');
      if($request->post('firstname')) $user->firstname =$request->post('firstname');
      if($request->post('lastname')) $user->lastname =$request->post('lastname');
      if($request->lastname && $request->firstname) $user->username = $request->lastname .' '. $request->firstname;
      if($request->post('email')) $user->email= $request->post('email');
      if($request->post('phone')) $user->phone =$request->post('phone');
      if($request->post('organisation')) $user->organisation=$request->post('organisation');
      
      if($request->post('civility')) $user->civility =$request->post('civility');
      if($request->post('language')) $user->language = strpos((strtolower($request->post('language'))),'fr') === false ? 'en':'fr';

      if($request->post('groupe_id')){
        $testRep = TestRepondant::where("test_id",$request->idTest)->where("repondant_id",$id)->first();
        $testRep->groupe_id = $request->post('groupe_id');
        $testRep->save();
      } 

      $user->save();
      return redirect('/feedback/repondant/'.$request->idTest);
    }

    public function randomPassword($value='')
    {
        # code... abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 9; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function addRepondantToActi(Request $request){
      # code...
      $role = $request->role ? $request->role : 'repondant';
      $dataURL = null;
            $test = Test::find($request->idTest);
            $beneficiaire = $test->beneficiaire;
            $password=$this->randomPassword();
            $repondant = User::firstOrCreate(
              [
                'email' =>$request->email, 
              ],
              [
                  'username' =>$request->lastname.' '.$request->firstname, 
                  'email' =>$request->email, 
                  'password' =>Hash::make ($request->password), 
                  'code' =>$request->password, 
                  'firstname'=>$request->firstname, 
                  'lastname'=>$request->lastname, 
                  
                  'phone'=>$request->phone, 
                  'organisation'=>$request->organisation, 
                 
                  'civility'=>$request->civility, 
                  'language'=>strpos((strtolower($request->language)),'fr') === false ? 'en':'fr', 
                  'role'=> 'user' , 
              ]
            );
            $repondant->language = strpos((strtolower($request->language)),'fr') === false ? 'en':'fr';
            $repondant->organisation = $beneficiaire->organisation;
            $repondant->save();
            //add repondant to test
            $testBen = TestRepondant::where([
                "repondant_id" => $repondant->id,
                "test_id" => $request->idTest,
                "role" => "beneficiaire"
              ])->count();

            if(!$testBen){
              $t = TestRepondant::firstOrNew(
                [
                  "repondant_id" => $repondant->id,
                  "test_id" => $request->idTest
                ]
              );
              $t->groupe_id = $request->groupe_id ;
              $t->role = "repondant";
              $t->save();
            }

          return redirect('/acti/repondantActi/'.$request->idTest);
       
    }



    public function addRepondantTo(Request $request)
    {
         $role = $request->role ? $request->role : 'repondant';
          $dataURL = null;
            $test = Test::find($request->idTest);
            $beneficiaire = $test->beneficiaire;
            $password=$this->randomPassword();
            $repondant = User::firstOrCreate(
              [
                'email' =>$request->email, 
              ],
              [
                  'username' =>$request->lastname.' '.$request->firstname, 
                  'email' =>$request->email, 
                  'password' =>Hash::make ($request->password), 
                  'code' =>$request->password, 
                  'firstname'=>$request->firstname, 
                  'lastname'=>$request->lastname, 
                  
                  'phone'=>$request->phone, 
                  'organisation'=>$request->organisation, 
                 
                  'civility'=>$request->civility, 
                  'language'=>strpos((strtolower($request->language)),'fr') === false ? 'en':'fr', 
                  'role'=> 'user' , 
              ]
            );
            $repondant->language = strpos((strtolower($request->language)),'fr') === false ? 'en':'fr';
            $repondant->organisation = $beneficiaire->organisation;
            $repondant->save();
            //add repondant to test
            $testBen = TestRepondant::where([
                "repondant_id" => $repondant->id,
                "test_id" => $request->idTest,
                "role" => "beneficiaire"
              ])->count();

            if(!$testBen){
              $t = TestRepondant::firstOrNew(
                [
                  "repondant_id" => $repondant->id,
                  "test_id" => $request->idTest
                ]
              );
              $t->groupe_id = $request->groupe_id ;
              $t->role = "repondant";
              $t->save();
            }

        return redirect('/feedback/repondant/'.$request->idTest);
               
    }
    public function sendMail(Request $request)
    {

      $email = User::find($request->consultant_id);

      $checkCons = DB::table('consultants')->where(['email'=>$email->email, 'username'=>$email->username]);

      if($checkCons->count()>0){
        \DB::table('consultants')->where(["email"=>$email->email, 'username'=>$email->username])->update(
          ['renvoye_mail' => 1]);
      } else {
        DB::table('consultants')->insert(['email' => $email->email, 'username'=>$email->username]);
      }

      $mail = Mail::find($request->id);
      
      // set other inactive
      $mails = Mail::where("type", $mail->type)->where("type_id", $mail->type_id)->where("language", $mail->language)->get();
      foreach ($mails as $key => $m) {
          if($m->id != $mail->id){
              $m->active = 0;
              $m->save();
          }

      }
      $mailNew = Mail::find($request->id);
      $mailNew->subject = $request->subject;
      $mailNew->mail = $request->mail1;
      $mailNew->active = 1;
      $mailNew->consultant_id = Auth::user()->id;

      $mailNew->save();

      $allProduits = array();
      $access = ConsultantAccess::where('consultant_id','=',$request->consultant_id)->get();
      foreach ($access as $key => $value) {
        $produits = explode("-",$value->type->name);
        if(!in_array($produits[0], $allProduits)){
          array_push($allProduits,$produits[0]);
        }
      }
      $Liste = "<ul>";
      if(count($allProduits)){
        foreach ($allProduits as $key => $val) {
          $allCli = array();
          if($val == "climage"){
            $listes = Type::where("name","like","%".$val."%")->select("description")->get();
            foreach ($listes as $key => $value) {
              array_push($allCli, $value->description); 
            }
            $listeCli = implode(', ', $allCli);
            $Liste .="<li>Climages (".$listeCli.")</li>";
          } else {
            $listes = Type::where("name","like","%".$val."%")->first();
            //var_dump($listes->description);
            $Liste .="<li>".$listes->description."</li>";
          }
        }
      } else {
        $Liste .="<li>Aucune</li>";
      }
      $Liste.="</ul>";

      $consultant= User::find($request->consultant_id);
      $language = strpos(strtolower($consultant->language), 'fr')===false ? 'en' :'fr';
      $mails = DB::table('mail')->where(['type'=>'consultant','language'=>$language,'active'=>1]);
      if($mails->count()>0){
         //$body = $mails->get()[0]->mail;
         //$subject = $mails->get()[0]->subject;
        $body = $request->mail1;
        $subject = $request->subject;
      }
      else{
        $subject ='Apter Consultant';
        $body = "
        <p>
        Bonjour et bienvenue dans la nouvelle interface web Apter Solutions dédiée à nos consultants partenaires 
        </p>
        <br> 
        <p>
        Vous trouverez dans ce mail, votre identifiant et votre mot de passe qui vous permettront de vous connecter à l’adresse web suivante : <a style='cursor:pointer;color:#0f69ff;' href='www.aptersolutions.org'>www.aptersolutions.org</a>
        </p>
        <p>
        En cliquant en haut à gauche sur le bouton Espace Consultant, vous pourrez ainsi accéder à votre espace et aux produits auxquels vous avez choisi de souscrire.
        </p>

        ".$Liste."

        <p>
        Si l’interface est très intuitive, nous sommes à votre entière disposition pour voir avec vous les processus pour créer un outil (test, Climage, 360) et le diffuser à vos clients.
        </p>


        <p>Toute l’équipe Apter Solutions vous remercie de votre confiance et reste à votre écoute</p>
        <br>
        <p>Laurence Vandeventer</p>

        <p>+33 (0) 6 15 57 18 70</p>

        <p>+212 (0) 6 96 91 71 45</p>

        <p><a style='cursor:pointer;color:#0f69ff;' href = 'www.aptersolutions.com'>www.aptersolutions.com</a></p

        <p><a style='cursor:pointer;color:#0f69ff;' href = 'laurence@apter-france.com'>laurence@apter-france.com</a></p>

        <p><a style='cursor:pointer;color:#0f69ff;' href='laurence@apter-maroc.com'>laurence@apter-maroc.com</a></p>";

      }

      $site_lien = "<a href='".url('/')."'>".url('/')."</a>";
      $mail_lien = "<a href='mailto:".Auth::user()->email."'>".Auth::user()->email."</a>";
      $body = str_replace('%LISTE_PRODUIT%', $Liste, $body);
      $body = str_replace('%LOGIN%', $consultant->email, $body);
      $body = str_replace('%MOT_DE_PASSE%', $consultant->code, $body);
      $body = str_replace('%CONST_LASTNAME%', Auth::user()->lastname, $body);
      $body = str_replace('%CONST_FIRSTNAME%', Auth::user()->firstname, $body);
      $body = str_replace('%NUMBER_PHONE%', Auth::user()->phone, $body);
      $body = str_replace('%SITE_LIEN%',$site_lien, $body);
      $body = str_replace('%CONST_MAIL%',$mail_lien, $body);

      // echo $body;

      return $this->moduleMail($body,$subject,$request->consultant_id);
    }

    public function moduleMail($body,$subject,$idConsultant)
    {
      # code...

      $address = user::find($idConsultant)->email;
   
      $mail = new PHPMailer(true);                              // Passing `true` enables exceptions

        try {
            //Server settings
            $mail->SMTPDebug = 2; 
            $mail->CharSet = 'UTF-8';                                // Enable verbose debug output

           // $mail->isSMTP();                                      // Set mailer to use SMTP

            $mail->Host = 'smtp.gmail.com';                        // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'contact@aptersolutions.org';                 // SMTP username
            $mail->Password = '@pterSolutions2019';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
            //Recipients
            $mail->setFrom('contact@aptersolutions.org', 'Apter Solutions');
            $mail->addAddress($address);     // Add a recipient
            /*$mail->addAddress('ellen@example.com'); */              // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            /*$mail->addCC('cc@example.com');
            $mail->addBCC('bcc@example.com');*/

            //Attachments
            /*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name*/

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
           
            //$consultant = user ::find($idConsultant);
            //$consultant->mail_send = true;
            //$consultant->save();
            /*var_dump('test 1');
            var_dump($body);*/
            return redirect('consultant');
           
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            return redirect('consultant');
        }
    }
}


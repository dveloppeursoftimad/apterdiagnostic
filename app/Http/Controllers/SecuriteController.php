<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PHPMailer\PHPMailer\PHPMailer;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class SecuriteController extends Controller
{
    public function oubliee(){
    	return view("securite/oublie");
    }

    public function sendEmail(Request $request){
    	$token = User::where("email",$request->email)->first();
    	$address = $request->email;
    	//$mdp = bcrypt("123123");

    	// if(isset($token)){
    	// 	User::where("id",$token->id)->update(["password"=>$mdp,"code"=>"123123"]);
    	// }

    	//var_dump(url("securite")."/changeMdp/".$token->remember_token."/".$token->id);

            // 
      	if(isset($token)){

            $subject ='Apter Solutions';

            $tokenUser = isset($token->remember_token) ? $token->remember_token : str_random(50);

            if(is_null($token->remember_token)){
                User::where("id",$token->id)->update(["remember_token"=>$tokenUser]);
            }          

            if($token->language == 'fr'){
                $body = "
                    <div style='text-align:center;width:400px; height:400px; position: absolute;margin-left: 25%;'>
                        <br/>
                        <h1 style='font-weight:700;color:#444; text-align:center; background-color:#f2f4f6; margin-bottom:40px;'>Bonjour ".(isset($token->civility) ? ($token->civility == 'madame' ? 'Mde' : 'Mr') : '')." ".$token->username."!</h1>
                        <h3 style='color:#979b9f;  text-align:center;'>Afin de changer votre mot de passe!</h3>
                        <h3 style='color:#979b9f;  text-align:center;margin-bottom:25px;'>Veuillez cliquer sur le bouton ci-dessous.</h3> 
                        <div style='text-align:center;'><a style='border: none;color: white;padding: 12px 40px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;border-radius:10px;background-color:#3869d4;' href=".url('securite').'/changeMdp/'.$tokenUser.'/'.$token->id." >Changer mot de passe</a>
                        </div>
                    </div>";
            } else {
                $body = "
                    <div style='text-align:center;width:400px; height:400px; position: absolute;margin-left: 25%;'>
                        <br/>
                        <h1 style='font-weight:700;color:#444; text-align:center; background-color:#f2f4f6; margin-bottom:40px;'>Hello ".(isset($token->civility) ? ($token->civility == 'madame' ? 'Mrs' : 'Mr') : '')." ".$token->username."!</h1>
                        <h3 style='color:#979b9f;  text-align:center;'>In order to change your password!</h3>
                        <h3 style='color:#979b9f;  text-align:center;margin-bottom:25px;'>Please click on the button below.</h3> 
                        <div style='text-align:center;'><a style='border: none;color: white;padding: 12px 40px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer;border-radius:10px;background-color:#3869d4;' href=".url('securite').'/changeMdp/'.$tokenUser.'/'.$token->id." >Change password</a>
                        </div>
                    </div>";
            }

      		$mail = new PHPMailer(true);	
	      	try {
	            //Server settings
	            $mail->SMTPDebug = 2; 
	            $mail->CharSet = 'UTF-8';                                // Enable verbose debug output
	           // $mail->isSMTP();                                      // Set mailer to use SMTP

	            $mail->Host = 'smtp.gmail.com';                        // Specify main and backup SMTP servers
	            $mail->SMTPAuth = true;                               // Enable SMTP authentication
	            $mail->Username = 'contact@aptersolutions.org';                 // SMTP username
	            $mail->Password = '@pterSolutions2019';                           // SMTP password
	            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	            $mail->Port = 465;                                    // TCP port to connect to
	            //Recipients
	            $mail->setFrom('contact@aptersolutions.org', 'Apter Solutions');
	            $mail->addAddress($address);     // Add a recipient

	            //Content
	            $mail->isHTML(true);                                  // Set email format to HTML
	            $mail->Subject = $subject;
	            $mail->Body = $body;
	            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	            //$mail->send();
	          	if($mail->send()){
	          		return redirect('securite/oubliee')->with('envoye_success',"mail envoyé, consulter votre email s'il vous plait");
	          	} else {
	          		return redirect('securite/oubliee')->with('envoye_echoue','non envoyé');
	          	}
	           
	        } catch (Exception $e) {
	            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	            return redirect('securite/oubliee')->with('envoye_success',"mail envoyé, consulter votre email s'il vous plait");
	        }
      	} else {
      		return redirect('securite/oubliee')->with('envoye_success',"mail envoyé, consulter votre email s'il vous plait");
      	}
      	
    	//$data = array();
    	//$data["token"] = $token->remember_token;
    	//$data["email"] = $request->email;
    	//return view("changerPassword",["data"=>$data]);
    }


    public function changeMdp($token,$id){
    	//var_dump($token);
    	//var_dump($id);
    	$checkToken = User::where("remember_token",$token)->where("id",$id)->count();
    	if($checkToken == 1){
    		return view("securite/changerMdp");
    	} else {
    		return redirect('securite/oubliee');
    	}	
    }

    public function confirmChangeMdp(Request $request){
    	$newMdp = User::where("email",$request->email)->first();
    	$mdp = bcrypt((string)$request->password);
    	if(isset($newMdp)){
    		if($request->password == $request->password_confirmation){
    			User::where("email",$request->email)->update(["password"=>$mdp,"code"=>$request->password]);
    			return redirect()->route('connexion')->with("mdp_change","Mot de passe changé");
    		}
    	}
    	

    	// if(isset($token)){
    	// 	User::where("id",$token->id)->update(["password"=>$mdp,"code"=>"123123"]);
    }
}

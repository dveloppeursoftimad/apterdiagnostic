<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Type;
use App\Question;
use App\User;
use App\Page;
use App\ReponseRepondant;
use App\TestRepondant;
use App\Terminer;
use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;



class FeedbackFrontController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question["feedback"] = Type::where('name', "feedback")->first();
        $feedbacks = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%feedback%");
        })->get();

        return view("feedback/front/index", ["feedbacks" => $feedbacks, "questions" => $question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request->reponse_question);
        die;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function questions($id, $code, $idpage=null, $num=null, $idUser = null){
        // var_dump($idUser);
        

        $credentials =[];
        if($idUser){
            $repondant = User::find($idUser);
            if($repondant && $repondant->code){

                $credentials["email"] = $repondant->email;
                $credentials["password"] = $repondant->code;
               

                $auth = Auth::attempt($credentials);
                if(!$auth){
                    return redirect()->route("feedback-front");
                }
               
            }else{
                return redirect()->route("feedback-front");
            }
        }
        // var_dump(Auth::user()->email);
        // die();
        

        $feedback = Test::find($id);
        $fb = Type::where('name', "feedback")->first();
        $type = Type::find($feedback->type_id);

        $count = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->count();
        //$count = \DB::table('terminer')->select('id','num_page','progression','page_id','repondant_id')->count();
        $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->select('id','num_page','progression','page_id','repondant_id')->first();


        $lang = (strpos(Auth::user()->language, "fr") === false) ? "en" : "fr";

        
        $firstpage = false;
        if($code == $feedback->code){

            if(empty($count)){

                $numero = 1;

                $page = Type::where('name', $type->name)->first()->pages[0];
                if($feedback->template_id){
                    $page = Type::find($feedback->template_id)->pages[0];
                }
                $data = ["id" => $id, "code" => $code, "idpage" => $page->id+1];
                $style = "none";
                $col = "col-md-12";
                $firstpage = true;

            } else {

                $data = ["id" => $id, "code" => $code, "idpage" => ($terminer->page_id+1)];
                $page = Page::find($terminer->page_id);
                $numero = $terminer->num_page;
                $style = "block";
                $col = "col-md-6";
            }

            return view("feedback/front/questions", ["feedback" => $fb, "pages" => $page, "lang" =>  $lang , "data" => $data, "numero" => $numero, "terminer_id"=> empty($terminer->id) ? 0 : $terminer->id, "progression" => empty($terminer->progression) ? 0 : $terminer->progression, "style" => $style, "col"=> $col, "test" => $feedback, "firstpage" => $firstpage]);

        }
        return redirect()->route("feedback-front");

    }

    public function questionnaire($id, $code, $idpage=null, $num=null){
        
        $feedback = Test::find($id);
        $fb = Type::where('name', "feedback")->first();
        $type = Type::find($feedback->type_id);

        $count = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->count();
        //$count = \DB::table('terminer')->select('id','num_page','progression','page_id','repondant_id')->count();
        $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->select('id','num_page','progression','page_id','repondant_id')->first();

        $firstpage = false;
        if($code == $feedback->code){

            if(empty($count)){

                $numero = 1;
                $page = Type::where('name', $type->name)->first()->pages[0];
                $data = ["id" => $id, "code" => $code, "idpage" => $page->id+1];
                $style = "none";
                $col = "col-md-12";
                $firstpage = true;

            } else {

                $data = ["id" => $id, "code" => $code, "idpage" => ($terminer->page_id+1)];
                $page = Page::find($terminer->page_id);
                $numero = $terminer->num_page;
                $style = "block";
                $col = "col-md-6";
            }

            return view("feedback/front/questions", ["feedback" => $fb, "pages" => $page, "lang" => \App::getLocale(), "data" => $data, "numero" => $numero, "terminer_id"=> empty($terminer->id) ? 0 : $terminer->id, "progression" => empty($terminer->progression) ? 0 : $terminer->progression, "style" => $style, "col"=> $col, "test" => $feedback, "firstpage" => $firstpage]);

        }
        return redirect()->route("feedback-front");

    }

    public function saveReponse(Request $request){

        $feedback = Test::find($request->id);
        $type = Type::find($feedback->type_id);
        
        if(isset($_POST["next"])){
            $this->validate($request, [
                'reponse_question' => "required|array|min:".$request->nbQuest,
                'reponse_question.*' => "required"
            ]);

            // save personal
            \DB::table('test_repondant')->updateOrInsert(
                ['test_id' => $request->id, 'repondant_id' => Auth::user()->id],
                ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
            );

            foreach ($request->reponse_question as $key => $reponse) {
                //foreach ($reponse as $reponse_id => $value) {
                    
                    $count = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id", $request->id)->count();
                    
                    if($count == 0){
                        $repondant_question_id = \DB::table('reponse_repondant')->insert(
                            ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $feedback->id, "reponse_id" => $reponse],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    } else {
                        $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id", $request->id)->update(
                            ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $feedback->id, "reponse_id" => $reponse],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    }
                //}
            }
            $count = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->count();
            //$count = \DB::table('terminer')->select('id','num_page','progression','page_id')->count();
            if($count == 0){
                $progress = 100/8;
                \DB::table('terminer')->insert(
                    ['num_page' => $request->numero+1, 'progression' => $progress, 'page_id' => $request->idpage, "repondant_id" => Auth::user()->id, 'test_id' => $feedback->id]
                );
            } else {
                $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->select('progression')->first();
                //$prog = $request->progression == 0 ? (100/8) : $request->progression; 
                $progress = ($terminer->progression + (100/8));
                Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->update(
                    ['num_page' => $request->numero+1, 'progression' => $progress, 'page_id' => $request->idpage,'test_id'=> $feedback->id]
                );
            }

            return redirect()->route('feedback-front-questions', ['id' => $request->id, "code" => $request->code, "idpage" => $request->idpage, 'numero' => ($request->numero+1)]);

        } else if(isset($_POST["previous"])) {
            
            $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->select('progression')->first();
            $progress = ($terminer->progression - $request->progression);
            Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$feedback->id)->update(
                ['num_page' => $request->numero-1, 'progression' => $progress, 'page_id' => ($request->idpage-2), 'test_id' => $feedback->id]
            );
            return redirect()->route('feedback-front-questions', ['id' => $request->id, "code" => $request->code, "idpage" => ($request->idpage-2), 'numero' => ($request->numero-1)]);

        } else {

            foreach ($request->reponse_question as $key => $reponse) {
                //foreach ($reponse as $reponse_id => $value) {
                    
                $count = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id", $request->id)->count();
                
                if($count == 0){
                    $repondant_question_id = \DB::table('reponse_repondant')->updateOrInsert(
                        ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $feedback->id, "reponse_text" => $reponse],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );
                } else {
                    $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id", $request->id)->update(
                        ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $feedback->id, "reponse_text" => $reponse],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );
                }
                //}
            }

            Terminer::where([
                    [ "repondant_id", "=", Auth::user()->id],
                ])->where('test_id','=',$feedback->id)->delete();
            TestRepondant::where([
                    [ "repondant_id", "=", Auth::user()->id],
                ])->where('test_id','=',$feedback->id)->update(['reponse'=> 1]);

            return redirect()->route('diagnostics')->with('success', 'Vous avez terminé le test '.$feedback->name.', vos réponses ont été enregistrées correctement!');           

        }
        
    }

    public function saveUsers(Request $request){

        $this->validate($request, [
          'code' => 'required',
        ]);

        $feedbacks = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%feedback%");
        })->get();

        foreach ($feedbacks as $key => $feedback) {
            
            $count_type = Test::where('type_id', '=' ,$feedback->type_id)->count();
            if(!empty($count_type)){

                $all_codes = Test::where('type_id', '=' ,$feedback->type_id)->select('id','code')->get();

                foreach ($all_codes as $key => $value) {

                    if($value->code == $request->code){

                        $test_rep = TestRepondant::where('repondant_id', '=', Auth::user()->id)->where('test_id', '=', $value->id)->count();
                        if($test_rep > 0){
                            return redirect()->route('feedback-front-questions', ['id' => $value->id, "code" => $request->code]);
                            break;
                        } else {
                            return redirect('/diagnostics/')->with('error_testcode', 'vous n\'êtes pas encore inscrit à ce test!');
                        }

                    }/* else {
                        return redirect('/diagnostics/')->with('error_code', 'Auncune test feedback trouvée!');  
                    } */

                }

                return redirect('/diagnostics/')->with('error_code', 'Auncune test trouvée!');

            } else {


                return redirect('/diagnostics/')->with('error_code', 'Auncune test feedback trouvée!');


            }
        }

        // foreach ($feedbacks as $key => $feedback) {
        //     if($feedback->code == $request->code){
        //         return redirect()->route('feedback-front-questions', ['id' => $feedback->id, "code" => $request->code]);
        //         break;
        //     } else {
        //         return redirect('/diagnostics/')->with('error_code', 'Code du Feedback incorrect');
        //     }
        // }

    }
    public function setRapport(){
        echo 'rapport is herre';
        die();
    } 
}

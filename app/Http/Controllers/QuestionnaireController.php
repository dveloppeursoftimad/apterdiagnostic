<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Type;
use App\Question;
use App\User;
use App\Test;
use App\TestRepondant;
use App\Mail\SendEmail;
use App\Groupe;
use App\ReponseRepondant;
use App\Mail;
use Illuminate\Support\Facades\Auth;
    
/*require url('vendor/autoload.php');*/
class QuestionnaireController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function climage()
    {
        $formation = Type::where('name', "climage-formation")->first();
        $reunion = Type::where('name', "climage-reunion")->first();
        $transformation = Type::where('name', "climage-transformation")->first();
        $negociation = Type::where('name', "climage-interlocuteur")->first();
        $question = null;
      
        $data = ["formation" => $formation,"reunion" => $reunion, "transformation" => $transformation, "curQuestion" => $question,"negociation"=>$negociation];

        // var_dump($question);
        // die();
    

        return view('climage/questionnaire/index', $data);
    }

    public function deleteFromTestActi(Request $request){
        $testRepondant = DB::table('test_repondant')->where(['repondant_id'=>$request->idDelete, 'test_id'=>$request->idTest]);
        $test = TestRepondant::find($testRepondant->get()[0]->id);
        $test->delete();
        return  redirect ('/acti/repondantActi/'.$request->idTest);
    }

    public function deleteFromTest(Request $request){
        $testRepondant = DB::table('test_repondant')->where(['repondant_id'=>$request->idDelete, 'test_id'=>$request->idTest]);
        $test = TestRepondant::find($testRepondant->get()[0]->id);
        $test->delete();
        return  redirect ('/feedback/repondant/'.$request->idTest);
    }

    public function acti($idQuestion = null){

        $acti = Type::where('name', "acti")->first();


        $question = null;
        if($idQuestion){
            $question = Question::find($idQuestion);
        }
        $data = ["acti" => $acti, "curQuestion" => $question];
        
        return view('acti/questionnaire/index', $data);        
    }

    public function feedback($idQuestion = null, $active = null){

        $fb = Type::where('name', "feedback")->first();
        $questionnaires = Type::where('name',"LIKE", "%feedback%")->get();
        foreach ($questionnaires as $key => $quest) {
            // $quest->pages = $quest->pages()->where('page_id',NULL)->get();
            // foreach ($quest->pages as $key2 => $page) {
            //     $page->pages = $page->pages;
            //     foreach ($page->pages as $key3 => $section) {
            //         foreach ($section->questions as $key4 => $question) {
            //             $question->reponses = $question->reponses;
                        
            //         }
            //     }
                
            // }
                
        }

        $question = null;
        if($idQuestion){
            $question = Question::find($idQuestion);
        }
        if(!$active){
            $active = $fb->id;
        }
        $data = [ "curQuestion" => $question, "questionnaires" => $questionnaires, "active" => $active, "fb"=>$fb->id];

        return view('feedback/questionnaire/index', $data);        
    }

    public function getUserMail($mails){
        foreach ($mails as $key => $m) {
            if(!is_null($m->consultant_id)){
              $m->user = User::find($m->consultant_id)->username;
            }
        }
    }

    public function repondant($id='')
    {
        # code...

        $test = Test::find($id);
        $users = Test::find($id)->users;
        
        /*foreach ($users as $key => $value) {
            # code...
            echo $value->user->email;
        }
        die();*/

        $fb = Type::where('name',"LIKE", "%feedback%")->first();
        if($test->template_id){
          $fb =  Type::find($test->template_id);
        }
        $cur = 0;
        foreach ($fb->pages as $key1 => $page) {
        
            $cur+=1;
            if($cur < 7){
                continue;            
            }
            $sectionNum = 1;
            foreach ($page->pages as $key2 => $section) {

                foreach ($section->questions as $key3 => $question) {

                    

                    
                        
                        $reponses = ReponseRepondant::whereHas('test', function ($query) use ($id) {
                          $query->where('id', $id);
                        })->whereHas('question', function($query) use ($question) {
                          $query->where("id", $question->id);
                        })->get();
                        $data = [];
                        $legendData = [];

                        foreach ($reponses as $key4 => $rep) {
                          if($rep->user){

                            // var_dump($question->question_fr);
                            // var_dump($rep->user->email);
                            $test = TestRepondant::where("test_id", $id)->where("repondant_id", $rep->user->id)->first();
                            $test->reponse = 1;
                            $test->save();
                            
                          }
                        }
                    
                }
            }
        }
        // die;



        $test = Test::find($id);
     
        $repondants = $test->users;
        $users = $test->users;
        foreach ($repondants as $key => $rep) {
            $repTest = TestRepondant::where("test_id", $id)->where("repondant_id",$rep->id)->first();
      
            if(!$repTest->groupe_id){
                
                $repTest->role = $rep->type;
                $repTest->groupe_id = $rep->groupe_id;
                $repTest->save();
            }
            $rep->groupe = $rep->testRepondants()->where("test_id", $id)->first() ? $rep->testRepondants()->where("test_id", $id)->first()->groupe : null;
            $rep->url = url('diagnostics')."/feedback/".$id."/". $test->code."/questions/1/0/".$rep->id;

            

        }


        foreach ($repondants as $key => $rep) {
            $typeMail = TestRepondant::where("repondant_id","=", $rep->id)->first();
            if($typeMail->mail_send == 0 && $typeMail->thanks_mail_send == 0){
                $rep->mail = 'invitation';
            } else if($typeMail->mail_send == 1 && $typeMail->thanks_mail_send == 0){
                $rep->mail = 'relance';
            } else if($typeMail->thanks_mail_send == 1) {
                $rep->mail = 'remerciement';
            }
        }

        

        // echo "<pre>";
        // var_dump($repondants);
        // echo "</pre>";
        // die;

        // $allUsers = User::all();
        // foreach ($allUsers as $key => $user) {
        //     $lang = $user->language ? ((strpos(strtolower($user->language), "fr") === false) ? "en" :"fr") : "fr";
        //     $user->language = $lang;
        //     $user->save();
        // }

        
        if(!$test->beneficiaire){
            foreach ($repondants as $key => $rep) {
           
               if($rep->type == "beneficiaire"){
                    $benTestRep = TestRepondant::where("test_id", $id)->where("repondant_id",$rep->id)->first();
                    $benTestRep->role = "beneficiaire";
                    $benTestRep->save();
                    $test->beneficiaire_id = $rep->id;
                    $test->save();
                    
                    $test = Test::find($id);

                    break;
               }
            }

        }
        
        $beneficiaire = $test->beneficiaire;
        // var_dump($beneficiaire);
        // die;
        $users = $test->users;

        $groupes = [];
        foreach ($repondants as $key => $rep) {

            if(!$rep->groupe){
                continue;
            }
            if(!array_key_exists($rep->groupe->id, $groupes) && strtolower($rep->groupe->abbreviation) != "vous"){
              $groupes[$rep->groupe->id] = $rep->groupe;
            }
        }

        $testrepondant = TestRepondant::all();
        $groupe = Groupe::all();
        $allUsers = User::all();
        foreach ($allUsers as $key => $user) {
            $lang = $user->language ? ((strpos(strtolower($user->language), "fr") === false) ? "en" :"fr") : "fr";
            $user->language = $lang;
            $user->save();
        }

        $type = Type::where('name', 'like', "%feedback%")->first();
        $all_mails = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", \App::getLocale() )->orderBy('type', 'asc')->get();

        foreach ($all_mails as $key => $mail) {

            // invitation beneficiaire
            $mails_inv_benef = Mail::where("type", "invitation beneficiaire")->where("language", $mail->language)->get();

            $mail_inv_benef = Mail::find($mails_inv_benef[0]->id);

            // invitation repondant
            $mails_inv_rep = Mail::where("type", "invitation repondant")->where("language", $mail->language)->get();

            $mail_inv_rep = Mail::find($mails_inv_rep[0]->id);

            // relance benef
            $mails_rel_benef = Mail::where("type", "relance beneficiaire")->where("active", "=", 1)->where("language", $mail->language)->get();

            $mail_rel_benef = Mail::find($mails_rel_benef[0]->id);

            // relance repondant
            $mails_rel_rep = Mail::where("type","like","%relance repondant%")->where("active", "=", 1)->where("language", $mail->language)->get();

            $mail_rel_rep = Mail::find($mails_rel_rep[0]->id);

            // thanks benef
            $mails_benef = Mail::where("type", "remerciement beneficiaire")->where("language", $mail->language)->get();

            $mail_benef = Mail::find($mails_benef[0]->id);

            // thanks repondant
            $mails_rep = Mail::where("type", "remerciement repondant")->where("language", $mail->language)->get();

            $mail_rep = Mail::find($mails_rep[0]->id);

        }

        $this->getUserMail($mails_inv_benef);
        $this->getUserMail($mails_rel_benef);
        $this->getUserMail($mails_benef);
        $this->getUserMail($mails_inv_rep);
        $this->getUserMail($mails_rel_rep);
        $this->getUserMail($mails_rep);

        return view('/feedback/repondant/index',['idtest'=>$id,'testrepondant'=>$testrepondant,'test'=>$test,'groupe'=>$groupe, "repondants" => $repondants, "groupes" => $groupes, "beneficiaire"=>$beneficiaire, 'mails_benef' => $mails_benef, "mail_benef" => $mail_benef, 'mails_rep' => $mails_rep, "mail_rep" => $mail_rep, 'mails_inv_benef' => $mails_inv_benef, 'mail_inv_benef' => $mail_inv_benef, 'mails_inv_rep' => $mails_inv_rep, 'mail_inv_rep' => $mail_inv_rep, 'mails_rel_rep' => $mails_rel_rep, 'mail_rel_rep' => $mail_rel_rep, 'mail_rel_benef' => $mail_rel_benef, 'mails_rel_benef' => $mails_rel_benef]);
    }
  
    public function randomPassword($value='')
    {
        # code... abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 9; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function removeTestRepondant(Request $request)
    {
        # code...
         $repondant = DB::table('test_repondant')->where(['repondant_id'=>$request->idDelete, 'test_id'=>$request->idTest]);
        
        $user = TestRepondant::find($repondant->get()[0]->id);
        $user->delete();
        return redirect('/feedback/repondant/'.$request->idTest);
    }
    public function getTestRepondant(){
        $test_repondant = TestRepondant::all();
        echo json_encode($test_repondant);
    }
    
    public function sendMailRelanceActi(Request $request){
        $this->internalSendmailActi($request->idRepondant,$request->idTest,$request->motif);
        return redirect('acti/repondantActi/'.$request->idTest);
    }

    public function sendMailRelance(Request $request){
        $this->internalSendmail($request->idRepondant,$request->idTest,$request->motif);
        return redirect('feedback/repondant/'.$request->idTest);
    }

    public function sendMailGlobalyActi(Request $request){
        # code...
        $beneficiaire = $this->getBeneficiaireFrom($request->idTest);
        $repondants= $this->getRepondantFrom($request->idTest);

        if(isset($beneficiaire->id)){
            foreach ($repondants as $repondant){
                # code...
                $testRepondant = DB::table('test_repondant')->where(
                    [
                        'test_id'=>$request->idTest,
                        'repondant_id'=>$repondant->id
                    ])->get();
               
                if(count($testRepondant)>0){

                    if($testRepondant[0]->mail_send == 0){
                        //echo 'invitation';
                        $this->internalSendmailActi($repondant->id,$request->idTest,'invitation');
                    }
                    if($testRepondant[0]->mail_send == 1 && $testRepondant[0]->thanks_mail_send == 0){
                        //echo 'relance';
                        $this->internalSendmailActi($repondant->id,$request->idTest,'relance');
                    }
                    if($testRepondant[0]->thanks_mail_send == 1){
                        //echo 'remerciement';
                        $this->internalSendmailActi($repondant->id,$request->idTest,'remerciement');
                    }
                }                  
            }

            $testRepondant = DB::table('test_repondant')->where(
                    [
                        'test_id'=>$request->idTest,
                        'repondant_id'=>$beneficiaire->id
                    ])->get();
            
            if(count($testRepondant)>0){
                if($testRepondant[0]->mail_send == 0){
                    $this->internalSendmailActi($beneficiaire->id,$request->idTest,'invitation');
                }
                if($testRepondant[0]->mail_send == 1 && $testRepondant[0]->thanks_mail_send == 0){
                    $this->internalSendmailActi($beneficiaire->id,$request->idTest,'relance');
                }
                if($testRepondant[0]->thanks_mail_send == 1){
                    $this->internalSendmailActi($beneficiaire->id,$request->idTest,'remerciement');
                }
            }      
        }
        return redirect('acti/repondantActi/'.$request->idTest);   
    }

    public function checkMailsActi(Request $request){

        $arrayMailsBody = array();
        $arrayMailsSubject = array();

        $this->updateMail($request->idinvbenef,$request->mail7, $request->subjectinvbenef);
        $this->updateMail($request->idrelbenef,$request->mail8, $request->subjectrelbenef);
        $this->updateMail($request->idrembenef,$request->mail9, $request->subjectrembenef);
        $this->updateMail($request->idinvrep,$request->mail10, $request->subjectinvrep);
        $this->updateMail($request->idrelrep,$request->mail11, $request->subjectrelrep);
        $this->updateMail($request->idremrep,$request->mail12, $request->subjectremrep);

        foreach ($request->listeUser as $key => $user) {
            $typeMail = TestRepondant::where("repondant_id","=", $user)->first();
            if($typeMail->mail_send == 0 && $typeMail->thanks_mail_send == 0){
                $mail = 'invitation';
            } else if($typeMail->mail_send == 1 && $typeMail->thanks_mail_send == 0){
                $mail = 'relance';
            } else if($typeMail->thanks_mail_send == 1) {
                $mail = 'remerciement';
            }

            //$this->internalSendmail($user,$request->idTest,$mail);
            $arrayMailsBody["idinvbenef"] = $request->mail7;
            $arrayMailsSubject["idinvbenef"] = $request->subjectinvbenef;
            $arrayMailsBody["idrelbenef"] = $request->mail8;
            $arrayMailsSubject["idrelbenef"] = $request->subjectrelbenef;
            $arrayMailsBody["idrembenef"] = $request->mail9;
            $arrayMailsSubject["idrembenef"] = $request->subjectrembenef;

            $arrayMailsBody["idinvrep"] = $request->mail10;
            $arrayMailsSubject["idinvrep"] = $request->subjectinvrep;
            $arrayMailsBody["idrelrep"] = $request->mail11;
            $arrayMailsSubject["idrelrep"] = $request->subjectrelrep;
            $arrayMailsBody["idremrep"] = $request->mail12;
            $arrayMailsSubject["idremrep"] = $request->subjectremrep;

            $this->internalSendmail3($user,$request->idTest,$mail,$arrayMailsBody,$arrayMailsSubject);


        }
        
        $response = [];
        $response["generated"] = true;

        return response()->json($response);
        
    }

    public function checkMailActi(Request $request){

        $mailNew = Mail::find($request->id);
        $mails = Mail::where("type", $mailNew->type)->where("type_id", $mailNew->type_id)->where("language", $mailNew->language)->get();
        foreach ($mails as $key => $m) {
            if($m->id != $mailNew->id){
                $m->active = 0;
                $m->save();
            }

        }

        //$mailNew = Mail::find($request->id);
        $mailNew->subject = $request->subject;
        if(isset($request->mail1)){
            $mailNew->mail = $request->mail1;
        }
        if(isset($request->mail2)){
            $mailNew->mail = $request->mail2;
        }
        if(isset($request->mail3)){
            $mailNew->mail = $request->mail3;
        }
        if(isset($request->mail4)){
            $mailNew->mail = $request->mail4;
        }
        if(isset($request->mail5)){
            $mailNew->mail = $request->mail5;
        }

        $bodyMail = $mailNew->mail;
        $mailNew->active = 1;
        $mailNew->consultant_id = Auth::user()->id;

        $mailNew->save();

        //$this->internalSendmail($request->idRepondant,$request->idTest,$request->motif);        
        $this->internalSendmail2($request->idRepondant,$request->idTest,$request->motif,$bodyMail,$request->subject);         

        return redirect('acti/repondantActi/'.$request->idTest);
    }

    public function checkMails(Request $request){

        $arrayMailsBody = array();
        $arrayMailsSubject = array();

        $this->updateMail($request->idinvbenef,$request->mail7, $request->subjectinvbenef);
        $this->updateMail($request->idrelbenef,$request->mail8, $request->subjectrelbenef);
        $this->updateMail($request->idrembenef,$request->mail9, $request->subjectrembenef);
        $this->updateMail($request->idinvrep,$request->mail10, $request->subjectinvrep);
        $this->updateMail($request->idrelrep,$request->mail11, $request->subjectrelrep);
        $this->updateMail($request->idremrep,$request->mail12, $request->subjectremrep);

        foreach ($request->listeUser as $key => $user) {
            $typeMail = TestRepondant::where("repondant_id","=", $user)->first();
            if($typeMail->mail_send == 0 && $typeMail->thanks_mail_send == 0){
                $mail = 'invitation';
            } else if($typeMail->mail_send == 1 && $typeMail->thanks_mail_send == 0){
                $mail = 'relance';
            } else if($typeMail->thanks_mail_send == 1) {
                $mail = 'remerciement';
            }
            //$this->internalSendmail($user,$request->idTest,$mail);
            $arrayMailsBody["idinvbenef"] = $request->mail7;
            $arrayMailsSubject["idinvbenef"] = $request->subjectinvbenef;
            $arrayMailsBody["idrelbenef"] = $request->mail8;
            $arrayMailsSubject["idrelbenef"] = $request->subjectrelbenef;
            $arrayMailsBody["idrembenef"] = $request->mail9;
            $arrayMailsSubject["idrembenef"] = $request->subjectrembenef;

            $arrayMailsBody["idinvrep"] = $request->mail10;
            $arrayMailsSubject["idinvrep"] = $request->subjectinvrep;
            $arrayMailsBody["idrelrep"] = $request->mail11;
            $arrayMailsSubject["idrelrep"] = $request->subjectrelrep;
            $arrayMailsBody["idremrep"] = $request->mail12;
            $arrayMailsSubject["idremrep"] = $request->subjectremrep;

            $this->internalSendmail3($user,$request->idTest,$mail,$arrayMailsBody,$arrayMailsSubject);

        }
        $response = [];
        $response["generated"] = true;

        return response()->json($response);

    }

    public function updateMail($id, $typeMail, $subject){
        $mailNew = Mail::find($id);
        $mails = Mail::where("type", $mailNew->type)->where("type_id", $mailNew->type_id)->where("language", $mailNew->language)->get();
        foreach ($mails as $key => $m) {
            if($m->id != $mailNew->id){
                $m->active = 0;
                $m->save();
            }
        }

        $mailNew->subject = $subject;
        $mailNew->mail = $typeMail;
        $mailNew->active = 1;
        $mailNew->consultant_id = Auth::user()->id;
        $mailNew->save();
    }

    public function checkMail(Request $request){

        // if(isset($request->typeMail)){
        //     foreach ($request->typeMail as $user => $type_mail) {
        //         foreach ($type_mail as $mail => $value) {
        //             $testRepondant = DB::table('test_repondant')->where(
        //             [
        //                 'test_id'=>$request->idTest,
        //                 'repondant_id'=>$user
        //             ])->first(); 
        //             $this->internalSendmail($user,$request->idTest,$mail);
        //         }
        //     }
        // }

        //var_dump($request->idRepondant);
        //var_dump($request->idTest);
        //var_dump($request->motif);

        $mailNew = Mail::find($request->id);
        $mails = Mail::where("type", $mailNew->type)->where("type_id", $mailNew->type_id)->where("language", $mailNew->language)->get();
        foreach ($mails as $key => $m) {
            if($m->id != $mailNew->id){
                $m->active = 0;
                $m->save();
            }

        }

        //$mailNew = Mail::find($request->id);
        $mailNew->subject = $request->subject;
        if(isset($request->mail1)){
            $mailNew->mail = $request->mail1;
        }
        if(isset($request->mail2)){
            $mailNew->mail = $request->mail2;
        }
        if(isset($request->mail3)){
            $mailNew->mail = $request->mail3;
        }
        if(isset($request->mail4)){
            $mailNew->mail = $request->mail4;
        }
        if(isset($request->mail5)){
            $mailNew->mail = $request->mail5;
        }
        $bodyMail = $mailNew->mail;
        $mailNew->active = 1;
        $mailNew->consultant_id = Auth::user()->id;

        $mailNew->save();

        //$this->internalSendmail($request->idRepondant,$request->idTest,$request->motif);        
        $this->internalSendmail2($request->idRepondant,$request->idTest,$request->motif,$bodyMail,$request->subject);        

        return redirect('feedback/repondant/'.$request->idTest);
    }

    public function sendMailGlobaly(Request $request)
    {
        # code...
        $beneficiaire = $this->getBeneficiaireFrom($request->idTest);
        $repondants= $this->getRepondantFrom($request->idTest);

        if(isset($beneficiaire->id)){
            foreach ($repondants as $repondant){
                # code...
                $testRepondant = DB::table('test_repondant')->where(
                    [
                        'test_id'=>$request->idTest,
                        'repondant_id'=>$repondant->id
                    ])->get();
               
                if(count($testRepondant)>0){

                    if($testRepondant[0]->mail_send == 0){
                        //echo 'invitation';
                        $this->internalSendmail($repondant->id,$request->idTest,'invitation');
                    }
                    if($testRepondant[0]->mail_send == 1 && $testRepondant[0]->thanks_mail_send == 0){
                        //echo 'relance';
                        $this->internalSendmail($repondant->id,$request->idTest,'relance');
                    }
                    if($testRepondant[0]->thanks_mail_send == 1){
                        //echo 'remerciement';
                        $this->internalSendmail($repondant->id,$request->idTest,'remerciement');
                    }
                }                  
            }

            $testRepondant = DB::table('test_repondant')->where(
                    [
                        'test_id'=>$request->idTest,
                        'repondant_id'=>$beneficiaire->id
                    ])->get();
            
            if(count($testRepondant)>0){
                if($testRepondant[0]->mail_send == 0){
                    $this->internalSendmail($beneficiaire->id,$request->idTest,'invitation');
                }
                if($testRepondant[0]->mail_send == 1 && $testRepondant[0]->thanks_mail_send == 0){
                    $this->internalSendmail($beneficiaire->id,$request->idTest,'relance');
                }
                if($testRepondant[0]->thanks_mail_send == 1){
                    $this->internalSendmail($beneficiaire->id,$request->idTest,'remerciement');
                }
            }      
        }
        return redirect('feedback/repondant/'.$request->idTest); 
    }
    public function checkGlobaly(Request $request){ 
       foreach ($request->checkrelance as $key => $value) {
           # code...
           if($value =='true'){
             $this->internalSendmail($key,$request->idTest,'relance');
           }
       }
       return redirect('feedback/repondant/'.$request->idTest);
    }

    public function checkGlobalyActi(Request $request){ 
       foreach ($request->checkrelance as $key => $value) {
           # code...
           if($value =='true'){
             $this->internalSendmailActi($key,$request->idTest,'relance');
           }
       }
       return redirect('acti/repondantActi/'.$request->idTest);
    }

    public function internalSendmailActi($idRepondant,$idTest,$type){
        # code...
        $repondant='test';
        $mails = 'mailtest';
        $testCode = Test::find($idTest)->code;
        $repondant = User:: find($idRepondant);
        $test = Test::find($idTest);

        $language = 'fr';
        if($repondant->language){
            $language = strpos($repondant->language, 'fr') === false ? 'en' : "fr";

        }

        
        $repondant->type = TestRepondant::where(["test_id" => $idTest, "repondant_id" =>$idRepondant])->first()->role; 

        switch ($type) {
            case 'invitation':
                /*$repondant = User:: find($idRepondant);*/
                if($repondant->type == 'repondant'){

                    // $lang = $repondant->language ? $repondant->language : 'fr'
 
                    $mails = DB::table('mail')->where(['type'=>'invitation repondant','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                    
                }
                if($repondant->type == 'beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'invitation beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                     $subject = $mails[0]->subject;
                }  
                break;
            case 'remerciement':
                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement repondant','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }   
                break;
            case 'relance':
                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'relance repondant','active'=>1,'language'=>$language])->get();
                    
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'relance beneficiaire','active'=>1,'language'=>$language])->get();
                  
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }  
                break;
            default:
        }
        
        if($bodytag == NULL){
            $bodytag = $type;
        }

        /*body*/
        $bodytag = str_replace("%REP_CIVILITE%", $repondant->civility, $bodytag);
        $bodytag = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $bodytag);
        $bodytag = str_replace("%REP_LASTNAME%", $repondant->lastname, $bodytag);
        $bodytag = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $bodytag);
        $bodytag = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $bodytag);
        $bodytag = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $bodytag);
        $bodytag = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $bodytag);


        $bodytag = str_replace("%SITE_LIEN%", url('diagnostics')."/acti/".$idTest."/". $test->code."/questions_rep/1/0/".$repondant->id, $bodytag);


        $bodytag = str_replace("%SITE_CODE%", $repondant->code , $bodytag);
        $bodytag = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $bodytag);
        $bodytag = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $bodytag);
        $bodytag = str_replace("%CONS_EMAIL%", Auth::user()->email, $bodytag);


        /*subject*/
        $subject = str_replace("%REP_CIVILITE%", $repondant->civility, $subject);
        $subject = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $subject);
        $subject = str_replace("%REP_LASTNAME%", $repondant->lastname, $subject);
        $subject = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $subject);
        $subject = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $subject);
        $subject = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $subject);
        $subject = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $subject);


        $subject = str_replace("%SITE_LIEN%", url('diagnostics')."/acti/".$idTest."/". $test->code."/questions_rep/1/0/".$repondant->id, $subject);


        $subject = str_replace("%SITE_CODE%", $repondant->code , $subject);
        $subject = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $subject);
        $subject = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $subject);
        $subject = str_replace("%CONS_EMAIL%", Auth::user()->email, $subject);

        $this->sendMailDirectly($repondant->email,$bodytag,$idTest,$idRepondant,$subject,$type);
    }

    public function internalSendmail2($idRepondant,$idTest,$type, $bodyMail, $subject)
    {
        # code...
        $repondant='test';
        $mails = 'mailtest';
        $testCode = Test::find($idTest)->code;
        $repondant = User:: find($idRepondant);
        $test = Test::find($idTest);

        $language = 'fr';
        if($repondant->language){
            $language = strpos($repondant->language, 'fr') === false ? 'en' : "fr";

        }

        $repondant->type = TestRepondant::where("test_id", $idTest)->where("repondant_id", $repondant->id)->first()->role;

        $bodytag = $bodyMail;
        $subject = $subject;

        
        if($bodytag == NULL){
            $bodytag = $type;
        }

        /*body*/
        $bodytag = str_replace("%REP_CIVILITE%", $repondant->civility, $bodytag);
        $bodytag = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $bodytag);
        $bodytag = str_replace("%REP_LASTNAME%", $repondant->lastname, $bodytag);
        $bodytag = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $bodytag);
        $bodytag = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $bodytag);
        $bodytag = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $bodytag);
        $bodytag = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $bodytag);


        $bodytag = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $bodytag);


        $bodytag = str_replace("%SITE_CODE%", $repondant->code , $bodytag);
        // $bodytag = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $bodytag);
        // $bodytag = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $bodytag);
        $bodytag = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $bodytag);
        $bodytag = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $bodytag);
        $bodytag = str_replace("%CONS_EMAIL%", Auth::user()->email, $bodytag);


        /*subject*/
        $subject = str_replace("%REP_CIVILITE%", $repondant->civility, $subject);
        $subject = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $subject);
        $subject = str_replace("%REP_LASTNAME%", $repondant->lastname, $subject);
        $subject = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $subject);
        $subject = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $subject);
        $subject = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $subject);
        $subject = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $subject);


        $subject = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $subject);


        $subject = str_replace("%SITE_CODE%", $repondant->code , $subject);
        // $subject = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $subject);
        // $subject = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $subject);
        $subject = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $subject);
        $subject = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $subject);
        $subject = str_replace("%CONS_EMAIL%", Auth::user()->email, $subject);

        $this->sendMailDirectly($repondant->email,$bodytag,$idTest,$idRepondant,$subject,$type);

    }

    public function internalSendmail($idRepondant,$idTest,$type)
    {
        # code...
        $repondant='test';
        $mails = 'mailtest';
        $testCode = Test::find($idTest)->code;
        $repondant = User:: find($idRepondant);
        $test = Test::find($idTest);

        // if(substr(strtolower($repondant->language),0,2)=='an' || substr(strtolower($repondant->language),0,2)=='en'){
        //     $language = 'en';
        // }
        // else
        // {
        //     $language = 'fr';
        // }
        $language = 'fr';
        if($repondant->language){
            $language = strpos($repondant->language, 'fr') === false ? 'en' : "fr";

        }

        $repondant->type = TestRepondant::where("test_id", $idTest)->where("repondant_id", $repondant->id)->first()->role;

       
        switch ($type) {
            case 'invitation':
                /*$repondant = User:: find($idRepondant);*/
                if($repondant->type == 'repondant'){

                    // $lang = $repondant->language ? $repondant->language : 'fr'
 
                    $mails = DB::table('mail')->where(['type'=>'invitation repondant','active'=>1,'language'=>$language])->get();
                 
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                    
                }
                if($repondant->type == 'beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'invitation beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                     $subject = $mails[0]->subject;
                }  
                break;
            case 'remerciement':
                /*$repondant = User:: find($idRepondant);*/
                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement repondant','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }   
                break;
            case 'relance':
                /*$repondant = User:: find($idRepondant);*/

                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'relance repondant','active'=>1,'language'=>$language])->get();
                    
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'relance beneficiaire','active'=>1,'language'=>$language])->get();
                  
                    $bodytag = $mails[0]->mail;
                    $subject = $mails[0]->subject;
                }  
                // $mails = DB::table('mail')->where(['type'=>'relance','active'=>1,'language'=>$language])->get();
                // $bodytag = $mails[0]->mail;
                // $subject = $mails[0]->subject;
                //var_dump($mails[0]->mail);
                break;
            default:
                //
        }
        /*if(!isset($repondant->groupe_id==NULL){
            var_dump($repondant->id);
            die();
        };*/
        
        if($bodytag == NULL){
            $bodytag = $type;
        }

        /*body*/
        $bodytag = str_replace("%REP_CIVILITE%", $repondant->civility, $bodytag);
        $bodytag = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $bodytag);
        $bodytag = str_replace("%REP_LASTNAME%", $repondant->lastname, $bodytag);
        $bodytag = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $bodytag);
        $bodytag = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $bodytag);
        $bodytag = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $bodytag);
        $bodytag = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $bodytag);


        $bodytag = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $bodytag);


        $bodytag = str_replace("%SITE_CODE%", $repondant->code , $bodytag);
        // $bodytag = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $bodytag);
        // $bodytag = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $bodytag);
        $bodytag = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $bodytag);
        $bodytag = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $bodytag);
        $bodytag = str_replace("%CONS_EMAIL%", Auth::user()->email, $bodytag);


        /*subject*/
        $subject = str_replace("%REP_CIVILITE%", $repondant->civility, $subject);
        $subject = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $subject);
        $subject = str_replace("%REP_LASTNAME%", $repondant->lastname, $subject);
        $subject = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $subject);
        $subject = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $subject);
        $subject = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $subject);
        $subject = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $subject);


        $subject = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $subject);


        $subject = str_replace("%SITE_CODE%", $repondant->code , $subject);
        // $subject = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $subject);
        // $subject = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $subject);
        $subject = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $subject);
        $subject = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $subject);
        $subject = str_replace("%CONS_EMAIL%", Auth::user()->email, $subject);

        $this->sendMailDirectly($repondant->email,$bodytag,$idTest,$idRepondant,$subject,$type);

    }

    public function internalSendmail3($idRepondant,$idTest,$type,$body,$subject)
    {
        # code...
        $repondant='test';
        $mails = 'mailtest';
        $testCode = Test::find($idTest)->code;
        $repondant = User:: find($idRepondant);
        $test = Test::find($idTest);

        // if(substr(strtolower($repondant->language),0,2)=='an' || substr(strtolower($repondant->language),0,2)=='en'){
        //     $language = 'en';
        // }
        // else
        // {
        //     $language = 'fr';
        // }
        $language = 'fr';
        if($repondant->language){
            $language = strpos($repondant->language, 'fr') === false ? 'en' : "fr";

        }

        $repondant->type = TestRepondant::where("test_id", $idTest)->where("repondant_id", $repondant->id)->first()->role;

       
        switch ($type) {
            case 'invitation':
                /*$repondant = User:: find($idRepondant);*/
                if($repondant->type == 'repondant'){

                    // $lang = $repondant->language ? $repondant->language : 'fr'
 
                    $mails = DB::table('mail')->where(['type'=>'invitation repondant','active'=>1,'language'=>$language])->get();
                 
                    $bodytag = $body["idinvrep"];
                    $subject = $subject["idinvrep"];
                    
                }
                if($repondant->type == 'beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'invitation beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $body["idinvbenef"];
                    $subject = $subject["idinvbenef"];
                }  
                break;
            case 'remerciement':
                /*$repondant = User:: find($idRepondant);*/
                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement repondant','active'=>1,'language'=>$language])->get();
                    $bodytag = $body["idremrep"];
                    $subject = $subject["idremrep"];
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'remerciement beneficiaire','active'=>1,'language'=>$language])->get();
                    $bodytag = $body["idrembenef"];
                    $subject = $subject["idrembenef"];
                }   
                break;
            case 'relance':
                /*$repondant = User:: find($idRepondant);*/

                if($repondant->type =='repondant'){
                    $mails = DB::table('mail')->where(['type'=>'relance repondant','active'=>1,'language'=>$language])->get();
                    
                    $bodytag = $body["idrelrep"];
                    $subject = $subject["idrelrep"];
                }
                if($repondant->type =='beneficiaire'){
                    $mails = DB::table('mail')->where(['type'=>'relance beneficiaire','active'=>1,'language'=>$language])->get();
                  
                    $bodytag = $body["idrelbenef"];
                    $subject = $subject["idrelbenef"];
                }  

                break;
            default:
                //
        }
        
        if($bodytag == NULL){
            $bodytag = $type;
        }

        /*body*/
        $bodytag = str_replace("%REP_CIVILITE%", $repondant->civility, $bodytag);
        $bodytag = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $bodytag);
        $bodytag = str_replace("%REP_LASTNAME%", $repondant->lastname, $bodytag);
        $bodytag = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $bodytag);
        $bodytag = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $bodytag);
        $bodytag = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $bodytag);
        $bodytag = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $bodytag);


        $bodytag = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $bodytag);


        $bodytag = str_replace("%SITE_CODE%", $repondant->code , $bodytag);
        // $bodytag = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $bodytag);
        // $bodytag = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $bodytag);
        $bodytag = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $bodytag);
        $bodytag = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $bodytag);
        $bodytag = str_replace("%CONS_EMAIL%", Auth::user()->email, $bodytag);


        /*subject*/
        $subject = str_replace("%REP_CIVILITE%", $repondant->civility, $subject);
        $subject = str_replace("%REP_FIRSTNAME%", $repondant->firstname, $subject);
        $subject = str_replace("%REP_LASTNAME%", $repondant->lastname, $subject);
        $subject = str_replace("%BEN_CIVILITE%", $this->getBeneficiaireFrom($idTest)->civility, $subject);
        $subject = str_replace("%BEN_FIRSTNAME%", $this->getBeneficiaireFrom($idTest)->firstname, $subject);
        $subject = str_replace("%BEN_LASTNAME%", $this->getBeneficiaireFrom($idTest)->lastname, $subject);
        $subject = str_replace("%BEN_SOCIETE%", $this->getBeneficiaireFrom($idTest)->organisation, $subject);


        $subject = str_replace("%SITE_LIEN%", url('diagnostics')."/feedback/".$idTest."/". $test->code."/questions/1/0/".$repondant->id, $subject);


        $subject = str_replace("%SITE_CODE%", $repondant->code , $subject);
        // $subject = str_replace("%GROUPE%", $this->setGroupe($repondant->groupe_id)->name, $subject);
        // $subject = str_replace("%GROUPE_ABREV%", $this->setGroupe($repondant->groupe_id)->abbreviation, $subject);
        $subject = str_replace("%CONS_FIRSTNAME%", Auth::user()->firstname, $subject);
        $subject = str_replace("%CONS_LASTNAME%", Auth::user()->lastname, $subject);
        $subject = str_replace("%CONS_EMAIL%", Auth::user()->email, $subject);

        $this->sendMailDirectly($repondant->email,$bodytag,$idTest,$idRepondant,$subject,$type);

    }
    public function setGroupe($id='')
    {
        # code...
        $retour = array('name' =>'','abbreviation'=>'');
        if ($id !=NULL){
            $groupe = DB::table('groupe')->where(['id'=>$id])->get();
            if(count($groupe)>0){
                $retour =$groupe[0];
            }
            return $retour;
        }
        else{
            return $retour;
        }   
        
    }
    public function sendMailDirectly($address,$body,$idTest,$idRepondant,$subject,$type)
    {
        # code...
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 2; 
            $mail->CharSet = 'UTF-8';                                // Enable verbose debug output

            //$mail->isSMTP();                                      // Set mailer to use SMTP

            $mail->Host = 'smtp.gmail.com';                        // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'contact@aptersolutions.org';                 // SMTP username
            $mail->Password = '@pterSolutions2019';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to
            //Recipients
            $mail->setFrom('contact@aptersolutions.org', 'Apter Solutions');
            $mail->addAddress($address/*, $repondant->username*/);     // Add a recipient
            /*$mail->addAddress('ellen@example.com'); */              // Name is optional
            $mail->addReplyTo('info@example.com', 'Information');
            /*$mail->addCC('cc@example.com');
            $mail->addBCC('bcc@example.com');*/

            //Attachments
            /*$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name*/

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $body;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            $mail->send();
            
            $testRepondants = DB::table('test_repondant')->where(['test_id'=>$idTest,'repondant_id'=>$idRepondant])->get();
            $testR = TestRepondant::find($testRepondants[0]->id);
            if($type !='remerciement'){
                $testR->mail_send = true;
            }else{
                $testR->thanks_mail_send = true;
            }
            $testR->save();
            /*var_dump('test 1');
            var_dump($body);*/
           
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
        }

    }

    public function sendMailSpecActi(Request $request)
    {
     
        $this->internalSendmailActi($request->idRepondant,$request->idTest,$request->motif);
        return redirect('acti/repondantActi/'.$request->idTest);     
    }    

    public function sendMailSpec(Request $request)
    {
        # code...
     
        $this->internalSendmail($request->idRepondant,$request->idTest,$request->motif);
        return redirect('feedback/repondant/'.$request->idTest);     
    }
    
    public function getBeneficiaireFrom($testId='')
    {
        # code...
        $beneficiaire='';
        $testRepondants = DB::table('test_repondant')->where(['test_id'=>$testId])->get();
        for($i=0;$i<count($testRepondants);$i++){
            if($testRepondants[$i]->role == "beneficiaire"){
                $beneficiaire = User::find($testRepondants[$i]->repondant_id);
                break;
            }
        }
        return $beneficiaire;
    }
    public function getRepondantFrom($testId='')
    {
        # code...
        $repondant=[];
        $testRepondants = DB::table('test_repondant')->where(['test_id'=>$testId])->get();
        for($i=0;$i<count($testRepondants);$i++){
            
            $repondants = User::find($testRepondants[$i]->repondant_id);
            if($repondants->type =="repondant"){
               $repondant[$i]= $repondants;
            }
        }
        return $repondant;
    }

    public function isma($idQuestion = null){
        $isma = Type::where('name', "isma")->first();

        $question = null;
        if($idQuestion){
            $question = Question::find($idQuestion);
        }
        $data = ["isma" => $isma, "curQuestion" => $question];

        // var_dump($isma->pages);
        // die;
        
        return view('isma/questionnaire/index', $data);        
    }

    public function isma_ajax($idQuestion = null){
        $isma = Type::where('name', "isma")->first();
        $data = ["isma" => $isma];

        echo json_encode($data);
        
    } 
}

<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // if(!\App::getLocale()){
        //     \App::setLocale('fr'); 
        // }
        if(\Session::has('locale')){
            \App::setLocale(\Session::get('locale'));
        } else {
            if(Auth::user()){
                strtolower(Auth::user()->language) == 'fr' ? \App::setLocale('fr') : \App::setLocale('en');
            }
        } 
  
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Type;
use Closure;

class ClimageAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // var_dump($request->user()->role);
        // die();
        $cons_access = Auth::user()->TestAccess()->get();
        if(!is_null($cons_access)){
            foreach ($cons_access as $key => $value) {
                // $test_type = Type::where("id", "=", $value->type_id)->select("name")->first();
                $test = explode("-", $value->name);
                if($test[0] == "climage"){
                    return $next($request);
                }
            }
            return redirect()->route('403');
        }
    }
}

<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Type;
use Closure;

class ConsultantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // var_dump($request->user()->role);
        // die();
        
        if ($request->user() && ($request->user()->role != 'consultant' && $request->user()->role != 'admin'  && $request->user()->role != 'partenaire' && $request->user()->role != 'admin_acad' && $request->user()->role != 'admin_rech_acad')) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}

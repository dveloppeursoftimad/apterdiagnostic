<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Test;
use App\Page;
use App\Type;
use App\Mail;
use App\Reponse;
use App\ReponseRepondant;
use App\Question;
use App\TestRepondant;
use App\User;

use App\Groupe;
//use ImalH\PDFLib;
use PhpOffice\PhpWord\Shared\ZipArchive;


use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfReader;

class ActiController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function mydump($var){
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
  }
  public function index()
  {
    //$tests = Test::whereHas('type', function ($query) {
      //$query->where('name', 'like', "%acti%");
    //})->get();

    $tests = [];
    if(Auth::user()->role == "consultant" || Auth::user()->role == "partenaire" || Auth::user()->role == "admin_rech_acad"){

      $tests = Test::whereHas('type', function ($query) {
        $query->where('name', 'like', "%acti%");
      })->where("consultant_id", "=", Auth::user()->id)->orderBy("created_at","desc")->paginate(10);

    } else if(Auth::user()->role == "admin" || Auth::user()->role == "admin_acad"){
      $tests = Test::whereHas('type', function ($query) {
        $query->where('name', 'like', "%acti%");
      })->orderBy("created_at","desc")->paginate(10);
    }

    foreach ($tests as $key => $test) {
      $test->nbReponses = count($test->testRepondants()->where("reponse", 1)->get());
    }


    return view("acti/index", ["tests" =>  $tests]);
  }

  public function repondants($id=''){
    $users = Test::find($id)->users;
    $testrepondant = TestRepondant::where('test_id', $id)->get();
    $test = Test::find($id);

    $repondants = $test->users;
    $groupes = [];
    



    foreach ($repondants as $key => $rep) {

      $rep->groupe = $rep->testRepondants()->where("test_id", $id)->first() ? $rep->testRepondants()->where("test_id", $id)->first()->groupe : null;
      $rep->url = url('diagnostic')."/acti/".$id."/". $test->code."/questions_rep/1/0/".$rep->id;
    }

    foreach ($repondants as $key => $rep) {
      $typeMail = TestRepondant::where("repondant_id","=", $rep->id)->first();
      if($typeMail->mail_send == 0 && $typeMail->thanks_mail_send == 0){
          $rep->mail = 'invitation';
      } else if($typeMail->mail_send == 1 && $typeMail->thanks_mail_send == 0){
          $rep->mail = 'relance';
      } else if($typeMail->thanks_mail_send == 1) {
          $rep->mail = 'remerciement';
      }
    }

    foreach ($repondants as $key => $rep) {

      if(!array_key_exists($rep->groupe->id, $groupes) && strtolower($rep->groupe->abbreviation) != "vous"){
        $groupes[$rep->groupe->id] = $rep->groupe;
      }
    }

    $beneficiaire = $test->beneficiaire;
    $users = $test->users;

    $type = Type::where('name', 'like', "%acti%")->first();
    $all_mails = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", \App::getLocale() )->orderBy('type', 'asc')->get();
    //$mails_en = Mail::where("type", "consultant" )->where("active", "=", 1)->where("language", "en" )->get();



    foreach ($all_mails as $key => $mail) {

      // invitation beneficiaire
      $mails_inv_benef = Mail::where("type", "invitation beneficiaire")->where("language", $mail->language)->get();

      $mail_inv_benef = Mail::find($mails_inv_benef[0]->id);

      // invitation repondant
      $mails_inv_rep = Mail::where("type", "invitation repondant")->where("language", $mail->language)->get();

      $mail_inv_rep = Mail::find($mails_inv_rep[0]->id);

      // relance benef
      $mails_rel_benef = Mail::where("type", "relance beneficiaire")->where("language", $mail->language)->get();

      $mail_rel_benef = Mail::find($mails_rel_benef[0]->id);

      // relance repondant
      $mails_rel_rep = Mail::where("type", "relance repondant")->where("language", $mail->language)->get();

      $mail_rel_rep = Mail::find($mails_rel_rep[0]->id);

      // thanks benef
      $mails_benef = Mail::where("type", "remerciement beneficiaire")->where("language", $mail->language)->get();

      $mail_benef = Mail::find($mails_benef[0]->id);

      // thanks repondant
      $mails_rep = Mail::where("type", "remerciement repondant")->where("language", $mail->language)->get();

      $mail_rep = Mail::find($mails_rep[0]->id);

    }

    $this->getUserMail($mails_inv_benef);
    $this->getUserMail($mails_rel_benef);
    $this->getUserMail($mails_benef);
    $this->getUserMail($mails_inv_rep);
    $this->getUserMail($mails_rel_rep);
    $this->getUserMail($mails_rep);

    return view('/acti/repondant/index',['beneficiaire' =>$beneficiaire,'idtest'=>$id,'testrepondant'=>$testrepondant,'test'=>$test, "repondants" => $repondants, "groupes" => $groupes, 'mails_benef' => $mails_benef, "mail_benef" => $mail_benef, 'mails_rep' => $mails_rep, "mail_rep" => $mail_rep, 'mails_inv_benef' => $mails_inv_benef, 'mail_inv_benef' => $mail_inv_benef, 'mails_inv_rep' => $mails_inv_rep, 'mail_inv_rep' => $mail_inv_rep, 'mails_rel_rep' => $mails_rel_rep, 'mail_rel_rep' => $mail_rel_rep, 'mail_rel_benef' => $mail_rel_benef, 'mails_rel_benef' => $mails_rel_benef]);
  }

   public function getUserMail($mails){
      foreach ($mails as $key => $m) {
          if(!is_null($m->consultant_id)){
            $m->user = User::find($m->consultant_id)->username;
          }
      }
  }

  public function mail(){

    $type = Type::where('name', 'like', "%acti%")->first();
    $mails_fr = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", "fr" )->orderBy('type', 'asc')->get();
    $mails_en = Mail::where("type_id", $type->id )->where("active", "=", 1)->where("language", "en" )->orderBy('type', 'asc')->get();
    return view('acti/mail',['mails_fr'=>$mails_fr, 'mails_en'=>$mails_en]);

  }

  public function mailEditActi($id){
    $mail = Mail::find($id);

    $mails = Mail::where("type", $mail->type)->where("language", $mail->language)->get();
    foreach ($mails as $key => $m) {
      if(!is_null($m->consultant_id)){
        $m->user = User::find($m->consultant_id)->username;
      }
    }

    return view('mail/edit',['mail'=>$mail, 'mails'=>$mails]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required',
      'code' => 'required'
    ]);
    $count = Test::where('name', '=' ,$request->name)->where('code', '=' ,$request->code)->count();
    if($count == 0){
      $acti = new Test;
      $acti->name = $request->name;
      $acti->type_id = Type::firstOrCreate(array('name' => 'acti'))->id;
      $acti->code = $request->code;
      $acti->consultant_id = Auth::user()->id;
      $acti->save();
      return redirect()->route('acti.index');
    } else {
      return redirect()->route('acti.index')->with('error_code', 'Code d\'accès déjà pris pour ce test 360° Performance et Motivations! Veuillez saisir un autre.');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $acti = Test::find($id);
    $acti->name = $request->name;
    $acti->code = $request->code;
    $acti->save();
    return redirect()->route('acti.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    Test::destroy($id);
    return redirect()->route('acti.index');
  }

  public function codes(){ 
    $count = \DB::table("test")->get();
    echo json_encode($count);
  }

  public function resultatActi($id){
    $acti = Type::where('name', "acti")->first();

    $test = Test::find($id);

    $data_users = [];
    foreach ($test->users as $key => $user) {
      $score_user = [];
      $user->groupe = $user->testRepondants()->where("test_id", $id)->first() ? $user->testRepondants()->where("test_id", $id)->first()->groupe : null;
      $scores = ReponseRepondant::where("test_id","=",$id)->where("repondant_id","=",$user->id)->get();
      foreach ($scores as $k => $score) {
        $repUser = Reponse::where("id",$score->reponse_id)->first();

        $score_user[$repUser["question_id"]] = $repUser["score"];
      }
    


      array_push($data_users,["groupe"=>isset($user->groupe->abbreviation)?$user->groupe->abbreviation:"AUCUNE","nom"=>$user->firstname." ".$user->lastname, "score"=>$score_user]);
    }
    
    return view('acti/resultat', ["acti" => $acti, "data_users" => $data_users]);
  }

  public function rapportActiPPTX(Request $request){
    $this->rapportActi($request->id, $request->genre);
  }

  public function rapportActi($id,$genre){
    
    $test = Test::find($id);
    $ben = $test->beneficiaire;
    $sq2 = [];
    $sqAutres = [];
    $sqPairs = [];
    $sqVous = [];
    $sqDirect = [];
    //$count_rep = count($test->reponseRepondants);
    $count_rep = 0;
    $count_repUser = 0;

    if(\App::getLocale() == "fr"){
      $arrayReponses = [
        "-1" => "",
        "0" => 'Jamais',
        "1" => 'T. Rarement',
        "2" => 'Rarement',
        "3" => 'Parfois',
        "4" => 'Souvent',
        "5" => 'T. Souvent',
        "6" => 'Toujours',

      ];
    } else {
      $arrayReponses = [
        "-1" => "",
        "0" => 'Never',
        "1" => 'V. Rarely',
        "2" => 'Rarely',
        "3" => 'Sometimes',
        "4" => 'Often',
        "5" => 'V. Often',
        "6" => 'Always',

      ];
    }

    foreach ($test->reponseRepondants as $key => $value) {
      $checkBenef = TestRepondant::where("repondant_id","=",$value->repondant_id)->distinct()->where("test_id","=",$id)->select("*")->first();
        if(isset($checkBenef->role)){
          $val = Reponse::where('id','=',$value->reponse_id)->where('question_id', '=',$value->question_id  )->where("score",">",3)->orderBy('score','desc')->count();
          $count_rep = $val + $count_rep;
        }
    }

    $repondant = $test->users[0];
    $consultant = $test->consultant;
    if(\App::getLocale() == "fr"){
      $txtRa = iconv("UTF-8", "windows-1252","Rapport-360°-Performance et Motivations").str_replace(" ","-",$repondant->firstname);
    } else {
      $txtRa = iconv("UTF-8", "windows-1252","Report-360-Performance and Motivations").str_replace(" ","-",$repondant->firstname);
    }


    $pdf = new PDF_Diag();
    $path = base_path().'/public/templates/acti/';
    if(\App::getLocale() == "fr"){
      $pageCount = $pdf->setSourceFile($path.'templates/Maquette_360_FB_FINAL.pdf');
    } else {
      $pageCount = $pdf->setSourceFile($path.'templates/actiEN.pdf');
    }

    $tplIdx = $pdf -> importPage(1);
    $size = $pdf->getTemplateSize($tplIdx);

    $type = Type::find($test->type_id);
    $NRC = ReponseRepondant::where("test_id","=",$id)->select("repondant_id")->distinct()->get();
  
    $nrcGroupe = [];
    foreach ($test->testRepondants as $key => $rpd) {
      $grp = strtoupper(trim($rpd->groupe->abbreviation));
      if($rpd->reponse){
        if(!array_key_exists($grp, $nrcGroupe)){
          $nrcGroupe[$grp] = 1;
        }else{
          $nrcGroupe[$grp] += 1;
        }
      }
        

    }

    // $this->mydump($nrcGroupe);
    // die;
 
    $pdf->SetAutoPageBreak(false);

    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
   

    $ouvertIter =0;

    $sq_totPairs = [];
    $sq_totDirect = [];
    $sq_totAutres = [];
    $sq_totVous = [];
    $sq_tot = [];
    foreach ($type->pages()->where('page_id','!=',NULL)->get() as $p => $pages) {
      $pp = $p+1;
      $data2 = [];
      $sq2[$pp] =[];
      $sq_totPairs[$pp] = [];
      $sq_totDirect[$pp] = [];
      $sq_totAutres[$pp] = [];
      $sq_totVous[$pp] = [];
      $sq_tot[$pp] = [];

      $sq[$pp] = [];
      $sqAutres[$pp] = [];
      $sqPairs[$pp] = [];
      $sqDirect[$pp] = [];
      $sqVous[$pp] = [];

      //foreach ($pages->pages as $s => $section) {
      foreach ($pages->questions as $q => $question) {

        $idquestion = Question::find($question->id);

        if($question->type == "text"){
          $ouvertIter += 1;
        }


        $qq = $q+1;
        $sq[$pp][$qq] = [];
        $sqAutres[$pp][$qq] = [];
        $sqPairs[$pp][$qq] = [];
        $sqDirect[$pp][$qq] = [];
        $sqVous[$pp][$qq] = [];

        $sq_totPairs[$pp][$qq] = array(
                        "pourcentage" => 0,
                        "frequence" => 0,
                        "reponse_fr" => NULL,
                        "question_id" => $question->id
                    );

        $sq_totDirect[$pp][$qq] = array(
                        "pourcentage" => 0,
                        "frequence" => 0,
                        "reponse_fr" => NULL,
                        "question_id" => $question->id
                    );

        $sq_totAutres[$pp][$qq] = array(
                        "pourcentage" => 0,
                        "frequence" => 0,
                        "reponse_fr" => NULL,
                        "question_id" => $question->id
                    );
        $sq_totVous[$pp][$qq] = array(
                        "pourcentage" => 0,
                        "frequence" => 0,
                        "reponse_fr" => NULL,
                        "question_id" => $question->id
                    );
        $sq_tot[$pp][$qq] = array(
                        "pourcentage" => 0,
                        "frequence" => 0,
                        "reponse_fr" => NULL,
                        "question_id" => $question->id
                    );

        //if($q<4){
        $pourcentage = 0;

        $perVous = 0;
        $perPairs = 0;
        $perAutres = 0;
        $perDirect = 0;

        $countrep = 0;
        $countrepBenef = 0;
        $countrepAutres = 0;
        $countrepPairs = 0;
        $countrepDirect = 0;
        $countrepVous = 0;
        $allrep = 0;
        if($pp == 9 && $qq == 1){
          $reponseText1 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
        }
        if($pp == 9 && $qq == 2){
          $reponseText2 = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
        }
        if($pp == 9 && $qq == 3){
          $reponseText = ReponseRepondant::where("question_id",$question->id )->where("test_id", $test->id)->get();
        }
        foreach ($idquestion->reponseRepondants as $key => $value){
          $frequence = 0;
          $repondant = $value->user;
          $repondant->groupe = $repondant->testRepondants()->where("test_id", $id)->first() ? $repondant->testRepondants()->where("test_id", $id)->first()->groupe : null;
          if($ouvertIter >0){
            $reponse = ReponseRepondant::where("repondant_id", $repondant->id)->where("question_id",$idquestion->id )->where("test_id", $test->id)->first();
          
          }

          $checkBenef = TestRepondant::where("repondant_id","=",$value->repondant_id)->distinct()->where("test_id","=",$id)->select("*")->first();

          if($checkBenef->reponse == 1){
            if(isset($checkBenef->role)){
              $val = Reponse::where('id','=',$value->reponse_id)->where('question_id', '=',$value->question_id)->where("score",">",3)->orderBy('score','desc')->select("*")->first();
              if((int)$val["score"] <3){
                continue;
              }

              
              // ENSEMBLE DES SCORE SUP A 3
              if(!array_key_exists($value->reponse_id,$sq[$pp][$qq])){    
               //$sq[$pp][$qq][$val["score"]] = $val["score"];   

               $sq[$pp][$qq][$val["score"]] = 1;   
              }else{ 
               $sq[$pp][$qq][$val["score"]] +=  1;
              }

              $pourcentage = $pourcentage + $sq[$pp][$qq][$val["score"]];

              // NOMBRE DE REPONSE SUP A 3
              $val2 = Reponse::where('id','=',$value->reponse_id)->where('question_id', '=',$value->question_id)->where("score",">",3)->count();  
              $countrep = $countrep + $val2;

              if($repondant->groupe){
                $repondant->groupe->abbreviation = strtoupper(trim($repondant->groupe->abbreviation));
                $abbrev = $repondant->groupe->abbreviation;
                if($abbrev == "HIER" ){

                  if(!array_key_exists($value->reponse_id,$sqAutres[$pp][$qq])){    
                   //$sq[$pp][$qq][$val["score"]] = $val["score"];   

                   $sqAutres[$pp][$qq][$val["score"]] = 1;   
                  }else{ 
                   $sqAutres[$pp][$qq][$val["score"]] +=  1;
                  }                  

                  $countrepAutres += $val2;
                  $perAutres = $perAutres + $sqAutres[$pp][$qq][$val["score"]];
                }
                if($abbrev == "DIRECT" ){

                  if(!array_key_exists($value->reponse_id,$sqDirect[$pp][$qq])){    
                   //$sq[$pp][$qq][$val["score"]] = $val["score"];   

                   $sqDirect[$pp][$qq][$val["score"]] = 1;   
                  }else{ 
                   $sqDirect[$pp][$qq][$val["score"]] +=  1;
                  }

                  $countrepDirect += $val2;
                  $perDirect = $perDirect + $sqDirect[$pp][$qq][$val["score"]];
                }
                if($abbrev == "PAIRS"){

                  if(!array_key_exists($value->reponse_id,$sqPairs[$pp][$qq])){    
                   //$sq[$pp][$qq][$val["score"]] = $val["score"];   

                   $sqPairs[$pp][$qq][$val["score"]] = 1;   
                  }else{ 
                   $sqPairs[$pp][$qq][$val["score"]] +=  1;
                  }

                  $countrepPairs += $val2;
                  $perPairs = $perPairs + $sqPairs[$pp][$qq][$val["score"]]; 
                }

                if($abbrev == "VOUS"){

                  if(!array_key_exists($value->reponse_id,$sqVous[$pp][$qq])){    
                   //$sq[$pp][$qq][$val["score"]] = $val["score"];   

                   $sqVous[$pp][$qq][$val["score"]] = 1;   
                  }else{ 
                   $sqVous[$pp][$qq][$val["score"]] +=  1;
                  }

                  $countrepVous += $val2;
                  $perVous = $perVous + $sqVous[$pp][$qq][$val["score"]];
                }
              }
              
              // NOMBRE DE REPONSE AU TOTAL
              $val3 = Reponse::where('id','=',$value->reponse_id)->where('question_id', '=',$value->question_id)->count();  
              $allrep = $allrep + $val3;

            }
          }
        }
        if(count($sq[$pp][$qq]) <1){
          $sq[$pp][$qq] = ["-1"];
        }

        // AUTRES
        $perAutres = isset($nrcGroupe["HIER"]) ? round(($perAutres * 100)/$nrcGroupe["HIER"]) : 0;
        $frequenceAutres = $this->maxIndex($sqAutres[$pp][$qq]);    
        $getQuestAutres = Reponse::where("question_id","=",$question->id)->where("score","=",$frequenceAutres)->select("reponse_fr")->first();  
        $sq_totAutres[$pp][$qq] = array(
            "pourcentage" => $perAutres,
            "frequence" => $frequenceAutres,
            "reponse_fr" => $arrayReponses[$frequenceAutres],
            "question_id" => $question->id
        );

        // PAIRS
        $perPairs = isset($nrcGroupe["PAIRS"]) ? round(($perPairs * 100)/$nrcGroupe["PAIRS"]) : 0;
        $frequencePairs = $this->maxIndex($sqPairs[$pp][$qq]);
        $getQuestPairs = Reponse::where("question_id","=",$question->id)->where("score","=",$frequencePairs)->select("reponse_fr")->first();    
        $sq_totPairs[$pp][$qq] = array(
            "pourcentage" => $perPairs,
            "frequence" => $frequencePairs,
            "reponse_fr" => $arrayReponses[$frequencePairs],
            "question_id" => $question->id
        );

        // DIRECT
        $perDirect = isset($nrcGroupe["DIRECT"]) ? round(($perDirect * 100)/$nrcGroupe["DIRECT"]) : 0;
        $frequenceDirect = $this->maxIndex($sqDirect[$pp][$qq]);
        $getQuestDirect = Reponse::where("question_id","=",$question->id)->where("score","=",$frequenceDirect)->select("reponse_fr")->first();    
        $sq_totDirect[$pp][$qq] = array(
            "pourcentage" => $perDirect,
            "frequence" => $frequenceDirect,
            "reponse_fr" => $arrayReponses[$frequenceDirect],
            "question_id" => $question->id
        );

        //VOUS
        $perVous = isset($nrcGroupe["VOUS"]) ? round(($perVous * 100)/$nrcGroupe["VOUS"]) : 0;
        $frequenceVous = $this->maxIndex($sqVous[$pp][$qq]);
        $getQuestVous = Reponse::where("question_id","=",$question->id)->where("score","=",$frequenceVous)->select("reponse_fr")->first(); 
        $sq_totVous[$pp][$qq] = array(
            "pourcentage" => $perVous,
            "frequence" => $frequenceVous,
            "reponse_fr" => $arrayReponses[$frequenceVous],
            "question_id" => $question->id
        );
        // TOTALS REPONDANTS
        $pourcentage = round(($pourcentage*100)/count($NRC));

        //$pourcentage = $nrcGroupe["DIRECT"] > 0 && $pourcentage > 0 ? round(($nrcGroupe["DIRECT"]*100)/$pourcentage) : 0;
        //$pourcentage = round(($pourcentage)/$nrcGroupe["DIRECT"]); 
        $frequence = $this->maxIndex($sq[$pp][$qq]);
       
        $getQuest = Reponse::where("question_id","=",$question->id)->where("score","=",$frequence)->select("reponse_fr")->first();    
        $sq_tot[$pp][$qq] = array(
          "pourcentage" => $pourcentage,
          "frequence" => $frequence,
          "reponse_fr" => $arrayReponses[$frequence],
          "question_id" => $question->id
        );

        //$sq2[$pp][$qq] = $countrep + $countrepBenef;

        // TOTAL NOMBRE REPONSE SUP A 3 
        $sq2[$pp][$qq] = $countrep;
        $sqAutres[$pp][$qq] = $countrepAutres;
        $sqPairs[$pp][$qq] = $countrepPairs;
        $sqDirect[$pp][$qq] = $countrepDirect;
        $sqVous[$pp][$qq] = $countrepVous;

        // TOTAL NOMBRE REPONSE
        $sqtot[$pp][$qq] = $allrep;            

        // avoir tous les reponses pour chaque questions dans reponses repondants 
        $reqs_q = ReponseRepondant::where("question_id","=",$question->id)->where("test_id","=",$id)->get();
        $scoreTot = 0;
        $maxVal = 0;
        foreach ($reqs_q as $key => $rep) {
          $score = Reponse::where('id','=',$rep->reponse_id)->select("*")->first();
          $scoreTot = $score["score"] + $scoreTot; 
        }
        ${"S".$pp."FQ".$qq} = $scoreTot / count($NRC);
      }
    }

    $max1 = 0;
    $max2 = 0;
    $max3 = 0;
    $max4 = 0;
    $max5 = 0;
    $max6 = 0;
    
    // MAX 1
    for ($i=1; $i < 7; $i++) { 
      for ($j=1; $j < 5; $j++) {
        if($max1<$sq_totDirect[$i][$j]["frequence"]){
          $max1 = $sq_totDirect[$i][$j]["frequence"];
          $questMax1 = $sq_totDirect[$i][$j];
          $index2 = $j;
          $index1 = $i;
        }    
      }
    }
    $q1 = Question::find($questMax1["question_id"]);

    // MAX 2
    for ($i=1; $i < 7; $i++) { 
      for ($j=1; $j < 5; $j++) {
        if($index1 != $i && $index2 != $j){
          if($max2<$sq_totDirect[$i][$j]["frequence"]){
            $max2 = $sq_totDirect[$i][$j]["frequence"];
            $questMax2 = $sq_totDirect[$i][$j];
            $index4 = $j;
            $index3 = $i;
          }
        }  
      }
    }
    $q2 = Question::find($questMax2["question_id"]);

    // MAX 3
    for ($i=1; $i < 7; $i++) { 
      for ($j=1; $j < 5; $j++) {
        if($index3 != $i && $index4 != $j && $index1 != $i && $index2 != $j){
          if($max3<$sq_totDirect[$i][$j]["frequence"]){
            $max3 = $sq_totDirect[$i][$j]["frequence"];
            $questMax3 = $sq_totDirect[$i][$j];
            $index6 = $j;
            $index5 = $i;
          }
        }  
      }
    }
    $q3 = Question::find($questMax3["question_id"]);

    // MAX 4
    for ($i=1; $i < 7; $i++) { 
      for ($j=5; $j < 9; $j++) { 
        if($max4<$sq_totDirect[$i][$j]["frequence"]){
          $max4 = $sq_totDirect[$i][$j]["frequence"];
          $questMax4 = $sq_totDirect[$i][$j];
          $index8 = $j;
          $index7 = $i;
        }
      }
    }
    $q4 = Question::find($questMax4["question_id"]);

    // MAX 5
    for ($i=1; $i < 7; $i++) { 
      for ($j=5; $j < 9; $j++) {
        if($index7 != $i && $index8 != $j){
          if($max5<$sq_totDirect[$i][$j]["frequence"]){
            $max5 = $sq_totDirect[$i][$j]["frequence"];
            $questMax5 = $sq_totDirect[$i][$j];
            $index10 = $j;
            $index9 = $i;
          }
        }  
      }
    }
    $q5 = Question::find($questMax5["question_id"]);

    // MAX 6
    for ($i=1; $i < 7; $i++) { 
      for ($j=5; $j < 9; $j++) {
        if($index9 != $i && $index10 != $j && $index7 != $i && $index8 != $j){
          if($max6<$sq_totDirect[$i][$j]["frequence"]){
            $max6 = $sq_totDirect[$i][$j]["frequence"];
            $questMax6 = $sq_totDirect[$i][$j];
            $index6 = $j;
            $index5 = $i;
          }
        }  
      }
    }
    $q6 = Question::find($questMax6["question_id"]);

    for ($i=1; $i < 9; $i++) {
      ${"TS".$i."NQ"} = 0;
      for ($j=1; $j < 9; $j++) { 
        ${"TS".$i."NQ"} = ${"TS".$i."NQ"} + (isset($sqDirect[$i][$j]) ? $sqDirect[$i][$j] : 0);  
      } 
    }

    $TTS = 0;
    foreach ($sq2 as $key => $value) {
      foreach ($value as $key2 => $value2) {
        $TTS +=$value2;
      }
    }

    // GRAPHE 4
    // $perS1N = array("titre"=>"Communiquer sur mes valeurs", "percent"=>round(($TS1NQ*100)/$TTS));
    // $perS2N = array('titre'=>"Exercice de l'Autorité","percent"=>round(($TS2NQ*100)/$TTS));
    // $perS3N = array("titre"=>"Pilotage de mes activité","percent"=>round(($TS3NQ*100)/$TTS));
    // $perS4N = array("titre"=>"Pilotage des ressources humaines","percent"=>round(($TS4NQ*100)/$TTS));
    // $perS5N = array("titre"=>"Comportements sous pression","percent"=>round(($TS5NQ*100)/$TTS));
    // $perS6N = array("titre"=>"Information et communication","percent"=>round(($TS6NQ*100)/$TTS));

    ///////////////// 2 ///////////////////////
    $TQ1 = (isset($sq2[1][1]) ? $sq2[1][1] : 0) + (isset($sq2[2][1]) ? $sq2[2][1] : 0) + (isset($sq2[3][1]) ? $sq2[3][1] : 0) + (isset($sq2[3][5]) ? $sq2[3][5] : 0) + (isset($sq2[5][1]) ? $sq2[5][1] : 0) + (isset($sq2[6][1]) ? $sq2[6][1] : 0);

    $TQ2 = (isset($sq2[1][2]) ? $sq2[1][2] : 0) + (isset($sq2[2][2]) ? $sq2[2][2] : 0) + (isset($sq2[3][2]) ? $sq2[3][2] : 0) + (isset($sq2[3][6]) ? $sq2[3][6] : 0) + (isset($sq2[5][2]) ? $sq2[5][2] : 0) + (isset($sq2[6][2]) ? $sq2[6][2] : 0);
    $TQ3 = (isset($sq2[1][3]) ? $sq2[1][3] : 0) + (isset($sq2[2][3]) ? $sq2[2][3] : 0) + (isset($sq2[3][3]) ? $sq2[3][3] : 0) + (isset($sq2[3][7]) ? $sq2[3][7] : 0) + (isset($sq2[5][3]) ? $sq2[5][3] : 0) + (isset($sq2[6][3]) ? $sq2[6][3] : 0);
    $TQ4 = (isset($sq2[1][4]) ? $sq2[1][4] : 0) + (isset($sq2[2][4]) ? $sq2[2][4] : 0) + (isset($sq2[3][4]) ? $sq2[3][4] : 0) + (isset($sq2[3][8]) ? $sq2[3][8] : 0) + (isset($sq2[5][4]) ? $sq2[5][4] : 0) + (isset($sq2[6][4]) ? $sq2[6][4] : 0);
    $TQ5 = (isset($sq2[1][5]) ? $sq2[1][5] : 0) + (isset($sq2[2][5]) ? $sq2[2][5] : 0) + (isset($sq2[4][1]) ? $sq2[4][1] : 0) + (isset($sq2[4][5]) ? $sq2[4][5] : 0) + (isset($sq2[5][5]) ? $sq2[5][5] : 0) + (isset($sq2[6][5]) ? $sq2[6][6] : 0);
    $TQ6 = (isset($sq2[1][6]) ? $sq2[1][6] : 0) + (isset($sq2[2][6]) ? $sq2[2][6] : 0) + (isset($sq2[4][2]) ? $sq2[4][2] : 0) + (isset($sq2[4][6]) ? $sq2[4][6] : 0) + (isset($sq2[5][1]) ? $sq2[5][1] : 0) + (isset($sq2[6][1]) ? $sq2[6][1] : 0);
    $TQ7 = (isset($sq2[1][7]) ? $sq2[1][7] : 0) + (isset($sq2[2][7]) ? $sq2[2][7] : 0) + (isset($sq2[4][3]) ? $sq2[4][3] : 0) + (isset($sq2[4][7]) ? $sq2[4][7] : 0) + (isset($sq2[5][1]) ? $sq2[5][1] : 0) + (isset($sq2[6][1]) ? $sq2[6][1] : 0);
    $TQ8 = (isset($sq2[1][8]) ? $sq2[1][8] : 0) + (isset($sq2[2][8]) ? $sq2[2][8] : 0) + (isset($sq2[4][4]) ? $sq2[4][4] : 0) + (isset($sq2[4][8]) ? $sq2[4][8] : 0) + (isset($sq2[5][1]) ? $sq2[5][1] : 0) + (isset($sq2[6][1]) ? $sq2[6][1] : 0);

    $TQ1Vous = (isset($sqVous[1][1]) ? $sqVous[1][1] : 0) + (isset($sqVous[2][1]) ? $sqVous[2][1] : 0) + (isset($sqVous[3][1]) ? $sqVous[3][1] : 0) + (isset($sqVous[3][5]) ? $sqVous[3][5] : 0) + (isset($sqVous[5][1]) ? $sqVous[5][1] : 0) + (isset($sqVous[6][1]) ? $sqVous[6][1] : 0);
    $TQ2Vous = (isset($sqVous[1][2]) ? $sqVous[1][2] : 0) + (isset($sqVous[2][2]) ? $sqVous[2][2] : 0) + (isset($sqVous[3][2]) ? $sqVous[3][2] : 0) + (isset($sqVous[3][6]) ? $sqVous[3][6] : 0) + (isset($sqVous[5][2]) ? $sqVous[5][2] : 0) + (isset($sqVous[6][2]) ? $sqVous[6][2] : 0);
    $TQ3Vous = (isset($sqVous[1][3]) ? $sqVous[1][3] : 0) + (isset($sqVous[2][3]) ? $sqVous[2][3] : 0) + (isset($sqVous[3][3]) ? $sqVous[3][3] : 0) + (isset($sqVous[3][7]) ? $sqVous[3][7] : 0) + (isset($sqVous[5][3]) ? $sqVous[5][3] : 0) + (isset($sqVous[6][3]) ? $sqVous[6][3] : 0);
    $TQ4Vous = (isset($sqVous[1][4]) ? $sqVous[1][4] : 0) + (isset($sqVous[2][4]) ? $sqVous[2][4] : 0) + (isset($sqVous[3][4]) ? $sqVous[3][4] : 0) + (isset($sqVous[3][8]) ? $sqVous[3][8] : 0) + (isset($sqVous[5][4]) ? $sqVous[5][4] : 0) + (isset($sqVous[6][4]) ? $sqVous[6][4] : 0);
    $TQ5Vous = (isset($sqVous[1][5]) ? $sqVous[1][5] : 0) + (isset($sqVous[2][5]) ? $sqVous[2][5] : 0) + (isset($sqVous[4][1]) ? $sqVous[4][1] : 0) + (isset($sqVous[4][5]) ? $sqVous[4][5] : 0) + (isset($sqVous[5][5]) ? $sqVous[5][5] : 0) + (isset($sqVous[6][5]) ? $sqVous[6][6] : 0);
    $TQ6Vous = (isset($sqVous[1][6]) ? $sqVous[1][6] : 0) + (isset($sqVous[2][6]) ? $sqVous[2][6] : 0) + (isset($sqVous[4][2]) ? $sqVous[4][2] : 0) + (isset($sqVous[4][6]) ? $sqVous[4][6] : 0) + (isset($sqVous[5][1]) ? $sqVous[5][1] : 0) + (isset($sqVous[6][1]) ? $sqVous[6][1] : 0);
    $TQ7Vous = (isset($sqVous[1][7]) ? $sqVous[1][7] : 0) + (isset($sqVous[2][7]) ? $sqVous[2][7] : 0) + (isset($sqVous[4][3]) ? $sqVous[4][3] : 0) + (isset($sqVous[4][7]) ? $sqVous[4][7] : 0) + (isset($sqVous[5][1]) ? $sqVous[5][1] : 0) + (isset($sqVous[6][1]) ? $sqVous[6][1] : 0);
    $TQ8Vous = (isset($sqVous[1][8]) ? $sqVous[1][8] : 0) + (isset($sqVous[2][8]) ? $sqVous[2][8] : 0) + (isset($sqVous[4][4]) ? $sqVous[4][4] : 0) + (isset($sqVous[4][8]) ? $sqVous[4][8] : 0) + (isset($sqVous[5][1]) ? $sqVous[5][1] : 0) + (isset($sqVous[6][1]) ? $sqVous[6][1] : 0);

    $TQ1Pairs = (isset($sqPairs[1][1]) ? $sqPairs[1][1] : 0) + (isset($sqPairs[2][1]) ? $sqPairs[2][1] : 0) + (isset($sqPairs[3][1]) ? $sqPairs[3][1] : 0) + (isset($sqPairs[3][5]) ? $sqPairs[3][5] : 0) + (isset($sqPairs[5][1]) ? $sqPairs[5][1] : 0) + (isset($sqPairs[6][1]) ? $sqPairs[6][1] : 0);
    $TQ2Pairs = (isset($sqPairs[1][2]) ? $sqPairs[1][2] : 0) + (isset($sqPairs[2][2]) ? $sqPairs[2][2] : 0) + (isset($sqPairs[3][2]) ? $sqPairs[3][2] : 0) + (isset($sqPairs[3][6]) ? $sqPairs[3][6] : 0) + (isset($sqPairs[5][2]) ? $sqPairs[5][2] : 0) + (isset($sqPairs[6][2]) ? $sqPairs[6][2] : 0);
    $TQ3Pairs = (isset($sqPairs[1][3]) ? $sqPairs[1][3] : 0) + (isset($sqPairs[2][3]) ? $sqPairs[2][3] : 0) + (isset($sqPairs[3][3]) ? $sqPairs[3][3] : 0) + (isset($sqPairs[3][7]) ? $sqPairs[3][7] : 0) + (isset($sqPairs[5][3]) ? $sqPairs[5][3] : 0) + (isset($sqPairs[6][3]) ? $sqPairs[6][3] : 0);
    $TQ4Pairs = (isset($sqPairs[1][4]) ? $sqPairs[1][4] : 0) + (isset($sqPairs[2][4]) ? $sqPairs[2][4] : 0) + (isset($sqPairs[3][4]) ? $sqPairs[3][4] : 0) + (isset($sqPairs[3][8]) ? $sqPairs[3][8] : 0) + (isset($sqPairs[5][4]) ? $sqPairs[5][4] : 0) + (isset($sqPairs[6][4]) ? $sqPairs[6][4] : 0);
    $TQ5Pairs = (isset($sqPairs[1][5]) ? $sqPairs[1][5] : 0) + (isset($sqPairs[2][5]) ? $sqPairs[2][5] : 0) + (isset($sqPairs[4][1]) ? $sqPairs[4][1] : 0) + (isset($sqPairs[4][5]) ? $sqPairs[4][5] : 0) + (isset($sqPairs[5][5]) ? $sqPairs[5][5] : 0) + (isset($sqPairs[6][5]) ? $sqPairs[6][6] : 0);
    $TQ6Pairs = (isset($sqPairs[1][6]) ? $sqPairs[1][6] : 0) + (isset($sqPairs[2][6]) ? $sqPairs[2][6] : 0) + (isset($sqPairs[4][2]) ? $sqPairs[4][2] : 0) + (isset($sqPairs[4][6]) ? $sqPairs[4][6] : 0) + (isset($sqPairs[5][1]) ? $sqPairs[5][1] : 0) + (isset($sqPairs[6][1]) ? $sqPairs[6][1] : 0);
    $TQ7Pairs = (isset($sqPairs[1][7]) ? $sqPairs[1][7] : 0) + (isset($sqPairs[2][7]) ? $sqPairs[2][7] : 0) + (isset($sqPairs[4][3]) ? $sqPairs[4][3] : 0) + (isset($sqPairs[4][7]) ? $sqPairs[4][7] : 0) + (isset($sqPairs[5][1]) ? $sqPairs[5][1] : 0) + (isset($sqPairs[6][1]) ? $sqPairs[6][1] : 0);
    $TQ8Pairs = (isset($sqPairs[1][8]) ? $sqPairs[1][8] : 0) + (isset($sqPairs[2][8]) ? $sqPairs[2][8] : 0) + (isset($sqPairs[4][4]) ? $sqPairs[4][4] : 0) + (isset($sqPairs[4][8]) ? $sqPairs[4][8] : 0) + (isset($sqPairs[5][1]) ? $sqPairs[5][1] : 0) + (isset($sqPairs[6][1]) ? $sqPairs[6][1] : 0);

    $TQ1Direct = (isset($sqDirect[1][1]) ? $sqDirect[1][1] : 0) + (isset($sqDirect[2][1]) ? $sqDirect[2][1] : 0) + (isset($sqDirect[3][1]) ? $sqDirect[3][1] : 0) + (isset($sqDirect[3][5]) ? $sqDirect[3][5] : 0) + (isset($sqDirect[5][1]) ? $sqDirect[5][1] : 0) + (isset($sqDirect[6][1]) ? $sqDirect[6][1] : 0);
    // var_dump($sqDirect[1][1]);
    // var_dump($sqDirect[2][1]);
    // var_dump($sqDirect[3][1]);
    // var_dump($sqDirect[3][5]);
    // var_dump($sqDirect[5][1]);
  
    $TQ2Direct = (isset($sqDirect[1][2]) ? $sqDirect[1][2] : 0) + (isset($sqDirect[2][2]) ? $sqDirect[2][2] : 0) + (isset($sqDirect[3][2]) ? $sqDirect[3][2] : 0) + (isset($sqDirect[3][6]) ? $sqDirect[3][6] : 0) + (isset($sqDirect[5][2]) ? $sqDirect[5][2] : 0) + (isset($sqDirect[6][2]) ? $sqDirect[6][2] : 0);
    $TQ3Direct = (isset($sqDirect[1][3]) ? $sqDirect[1][3] : 0) + (isset($sqDirect[2][3]) ? $sqDirect[2][3] : 0) + (isset($sqDirect[3][3]) ? $sqDirect[3][3] : 0) + (isset($sqDirect[3][7]) ? $sqDirect[3][7] : 0) + (isset($sqDirect[5][3]) ? $sqDirect[5][3] : 0) + (isset($sqDirect[6][3]) ? $sqDirect[6][3] : 0);
    $TQ4Direct = (isset($sqDirect[1][4]) ? $sqDirect[1][4] : 0) + (isset($sqDirect[2][4]) ? $sqDirect[2][4] : 0) + (isset($sqDirect[3][4]) ? $sqDirect[3][4] : 0) + (isset($sqDirect[3][8]) ? $sqDirect[3][8] : 0) + (isset($sqDirect[5][4]) ? $sqDirect[5][4] : 0) + (isset($sqDirect[6][4]) ? $sqDirect[6][4] : 0);
    $TQ5Direct = (isset($sqDirect[1][5]) ? $sqDirect[1][5] : 0) + (isset($sqDirect[2][5]) ? $sqDirect[2][5] : 0) + (isset($sqDirect[4][1]) ? $sqDirect[4][1] : 0) + (isset($sqDirect[4][5]) ? $sqDirect[4][5] : 0) + (isset($sqDirect[5][5]) ? $sqDirect[5][5] : 0) + (isset($sqDirect[6][5]) ? $sqDirect[6][6] : 0);
    $TQ6Direct = (isset($sqDirect[1][6]) ? $sqDirect[1][6] : 0) + (isset($sqDirect[2][6]) ? $sqDirect[2][6] : 0) + (isset($sqDirect[4][2]) ? $sqDirect[4][2] : 0) + (isset($sqDirect[4][6]) ? $sqDirect[4][6] : 0) + (isset($sqDirect[5][6]) ? $sqDirect[5][6] : 0) + (isset($sqDirect[6][6]) ? $sqDirect[6][6] : 0);
    $TQ7Direct = (isset($sqDirect[1][7]) ? $sqDirect[1][7] : 0) + (isset($sqDirect[2][7]) ? $sqDirect[2][7] : 0) + (isset($sqDirect[4][3]) ? $sqDirect[4][3] : 0) + (isset($sqDirect[4][7]) ? $sqDirect[4][7] : 0) + (isset($sqDirect[5][7]) ? $sqDirect[5][7] : 0) + (isset($sqDirect[6][7]) ? $sqDirect[6][7] : 0);
    $TQ8Direct = (isset($sqDirect[1][8]) ? $sqDirect[1][8] : 0) + (isset($sqDirect[2][8]) ? $sqDirect[2][8] : 0) + (isset($sqDirect[4][4]) ? $sqDirect[4][4] : 0) + (isset($sqDirect[4][8]) ? $sqDirect[4][8] : 0) + (isset($sqDirect[5][8]) ? $sqDirect[5][8] : 0) + (isset($sqDirect[6][8]) ? $sqDirect[6][8] : 0);


    $TQ1Autres = (isset($sqAutres[1][1]) ? $sqAutres[1][1] : 0) + (isset($sqAutres[2][1]) ? $sqAutres[2][1] : 0) + (isset($sqAutres[3][1]) ? $sqAutres[3][1] : 0) + (isset($sqAutres[3][5]) ? $sqAutres[3][5] : 0) + (isset($sqAutres[5][1]) ? $sqAutres[5][1] : 0) + (isset($sqAutres[6][1]) ? $sqAutres[6][1] : 0);
    $TQ2Autres = (isset($sqAutres[1][2]) ? $sqAutres[1][2] : 0) + (isset($sqAutres[2][2]) ? $sqAutres[2][2] : 0) + (isset($sqAutres[3][2]) ? $sqAutres[3][2] : 0) + (isset($sqAutres[3][6]) ? $sqAutres[3][6] : 0) + (isset($sqAutres[5][2]) ? $sqAutres[5][2] : 0) + (isset($sqAutres[6][2]) ? $sqAutres[6][2] : 0);
    $TQ3Autres = (isset($sqAutres[1][3]) ? $sqAutres[1][3] : 0) + (isset($sqAutres[2][3]) ? $sqAutres[2][3] : 0) + (isset($sqAutres[3][3]) ? $sqAutres[3][3] : 0) + (isset($sqAutres[3][7]) ? $sqAutres[3][7] : 0) + (isset($sqAutres[5][3]) ? $sqAutres[5][3] : 0) + (isset($sqAutres[6][3]) ? $sqAutres[6][3] : 0);
    $TQ4Autres = (isset($sqAutres[1][4]) ? $sqAutres[1][4] : 0) + (isset($sqAutres[2][4]) ? $sqAutres[2][4] : 0) + (isset($sqAutres[3][4]) ? $sqAutres[3][4] : 0) + (isset($sqAutres[3][8]) ? $sqAutres[3][8] : 0) + (isset($sqAutres[5][4]) ? $sqAutres[5][4] : 0) + (isset($sqAutres[6][4]) ? $sqAutres[6][4] : 0);
    $TQ5Autres = (isset($sqAutres[1][5]) ? $sqAutres[1][5] : 0) + (isset($sqAutres[2][5]) ? $sqAutres[2][5] : 0) + (isset($sqAutres[4][1]) ? $sqAutres[4][1] : 0) + (isset($sqAutres[4][5]) ? $sqAutres[4][5] : 0) + (isset($sqAutres[5][5]) ? $sqAutres[5][5] : 0) + (isset($sqAutres[6][5]) ? $sqAutres[6][6] : 0);
    $TQ6Autres = (isset($sqAutres[1][6]) ? $sqAutres[1][6] : 0) + (isset($sqAutres[2][6]) ? $sqAutres[2][6] : 0) + (isset($sqAutres[4][2]) ? $sqAutres[4][2] : 0) + (isset($sqAutres[4][6]) ? $sqAutres[4][6] : 0) + (isset($sqAutres[5][1]) ? $sqAutres[5][1] : 0) + (isset($sqAutres[6][1]) ? $sqAutres[6][1] : 0);
    $TQ7Autres = (isset($sqAutres[1][7]) ? $sqAutres[1][7] : 0) + (isset($sqAutres[2][7]) ? $sqAutres[2][7] : 0) + (isset($sqAutres[4][3]) ? $sqAutres[4][3] : 0) + (isset($sqAutres[4][7]) ? $sqAutres[4][7] : 0) + (isset($sqAutres[5][1]) ? $sqAutres[5][1] : 0) + (isset($sqAutres[6][1]) ? $sqAutres[6][1] : 0);
    $TQ8Autres = (isset($sqAutres[1][8]) ? $sqAutres[1][8] : 0) + (isset($sqAutres[2][8]) ? $sqAutres[2][8] : 0) + (isset($sqAutres[4][4]) ? $sqAutres[4][4] : 0) + (isset($sqAutres[4][8]) ? $sqAutres[4][8] : 0) + (isset($sqAutres[5][1]) ? $sqAutres[5][1] : 0) + (isset($sqAutres[6][1]) ? $sqAutres[6][1] : 0);

    $TTS2 = $TQ1+$TQ2+$TQ3+$TQ4+$TQ5+$TQ6+$TQ7+$TQ8 ;
    
    $TTSDirect = $TQ1Direct+$TQ2Direct+$TQ3Direct+$TQ4Direct+$TQ5Direct+$TQ6Direct+$TQ7Direct+$TQ8Direct;

    // SCHEMA 5
    $perTQ1 = round(($TQ1*100)/$TTS2);
    $perTQ2 = round(($TQ2*100)/$TTS2); 
    $perTQ3 = round(($TQ3*100)/$TTS2);
    $perTQ4 = round(($TQ4*100)/$TTS2);
    $perTQ5 = round(($TQ5*100)/$TTS2);
    $perTQ6 = round(($TQ6*100)/$TTS2);
    $perTQ7 = round(($TQ7*100)/$TTS2);
    $perTQ8 = round(($TQ8*100)/$TTS2);

    $TTSH = $TS1NQ + $TS2NQ + $TS3NQ + $TS4NQ + $TS5NQ + $TS6NQ;
    $perS1N = array("titre"=>"Communiquer sur mes valeurs", "percent"=>number_format((($TS1NQ*100)/$TTSH),1));
    $perS2N = array('titre'=>"Exercice de l'Autorité","percent"=>number_format((($TS2NQ*100)/$TTSH),1));
    $perS3N = array("titre"=>"Pilotage de mes activité","percent"=>number_format((($TS3NQ*100)/$TTSH),1));
    $perS4N = array("titre"=>"Pilotage des ressources humaines","percent"=>number_format((($TS4NQ*100)/$TTSH),1));
    $perS5N = array("titre"=>"Comportements sous pression","percent"=>number_format((($TS5NQ*100)/$TTSH),1));
    $perS6N = array("titre"=>"Information et communication","percent"=>number_format((($TS6NQ*100)/$TTSH),1));

    $perTQ1Vous = isset($nrcGroupe['VOUS']) ? round(($TQ1Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ2Vous = isset($nrcGroupe['VOUS']) ? round(($TQ2Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0; 
    $perTQ3Vous = isset($nrcGroupe['VOUS']) ? round(($TQ3Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ4Vous = isset($nrcGroupe['VOUS']) ? round(($TQ4Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ5Vous = isset($nrcGroupe['VOUS']) ? round(($TQ5Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ6Vous = isset($nrcGroupe['VOUS']) ? round(($TQ6Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ7Vous = isset($nrcGroupe['VOUS']) ? round(($TQ7Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;
    $perTQ8Vous = isset($nrcGroupe['VOUS']) ? round(($TQ8Vous*100)/ ($nrcGroupe['VOUS'] *6) ) : 0;

    $perTQ1Autres = isset($nrcGroupe['HIER']) ? round(($TQ1Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ2Autres = isset($nrcGroupe['HIER']) ? round(($TQ2Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0; 
    $perTQ3Autres = isset($nrcGroupe['HIER']) ? round(($TQ3Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ4Autres = isset($nrcGroupe['HIER']) ? round(($TQ4Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ5Autres = isset($nrcGroupe['HIER']) ? round(($TQ5Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ6Autres = isset($nrcGroupe['HIER']) ? round(($TQ6Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ7Autres = isset($nrcGroupe['HIER']) ? round(($TQ7Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;
    $perTQ8Autres = isset($nrcGroupe['HIER']) ? round(($TQ8Autres*100)/ ($nrcGroupe['HIER'] *6) ) : 0;

    $pertq1Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ1Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ2Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ2Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0; 
    $perTQ3Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ3Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ4Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ4Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ5Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ5Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ6Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ6Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ7Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ7Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;
    $perTQ8Pairs = isset($nrcGroupe['PAIR']) ? round(($TQ8Pairs*100)/ ($nrcGroupe['PAIR'] *6) ) : 0;


    $pertq1Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ1Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ2Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ2Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0; 
    $perTQ3Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ3Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ4Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ4Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ5Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ5Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ6Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ6Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ7Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ7Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;
    $perTQ8Direct = isset($nrcGroupe['DIRECT']) ? round(($TQ8Direct*100)/ ($nrcGroupe['DIRECT'] *6) ) : 0;



    //$perMax = array($perTQ1,$perTQ2,$perTQ3,$perTQ4,$perTQ5,$perTQ6,$perTQ7,$perTQ8);
    //sort($perMax);

    $perMax = array(number_format((($TQ1Direct/$TTSDirect)*100),1),number_format((($TQ2Direct/$TTSDirect)*100),1),number_format((($TQ3Direct/$TTSDirect)*100),1),number_format((($TQ4Direct/$TTSDirect)*100),1),number_format((($TQ5Direct/$TTSDirect)*100),1),number_format((($TQ6Direct/$TTSDirect)*100),1),number_format((($TQ7Direct/$TTSDirect)*100),1),number_format((($TQ8Direct/$TTSDirect)*100),1));
    sort($perMax);

    // SCHEMA 1
    // $TQ1TQ4 = ($TQ1+$TQ2+$TQ3+$TQ4);

    // $TQ5TQ8 = ($TQ5+$TQ6+$TQ7+$TQ8);
    // $perTQ1TQ4 = ($TQ1TQ4/$TTS2)*100;
    // $perTQ5TQ8 = ($TQ5TQ8/$TTS2)*100;

    $TQ1TQ4 = ($TQ1Direct+$TQ2Direct+$TQ3Direct+$TQ4Direct);

    $TQ5TQ8 = ($TQ5Direct+$TQ6Direct+$TQ7Direct+$TQ8Direct);
    $perTQ1TQ4 = ($TQ1TQ4/$TTSDirect)*100;
    $perTQ5TQ8 = ($TQ5TQ8/$TTSDirect)*100;

    ////////////////////// 3 //////////////////////////////
    
    $som_fq = 0;
    for($i=1;$i<7;$i++){
      for ($j=1; $j < 9; $j++) { 
        ${"perS".$i."Q".$j} = round(isset($sq2[$i][$j]) ? $sq2[$i][$j] / count($NRC) : 0);
        $som_fq = ${"S".$i."FQ".$j} + $som_fq;
        ${"FMS".$i} = round($som_fq);
      }
    }

    $FMA1 = round($S1FQ1+$S2FQ1+$S3FQ1+$S3FQ5+$S5FQ1+$S6FQ1)/6;
    $FMA2 = round($S1FQ2+$S2FQ2+$S3FQ2+$S3FQ6+$S5FQ2+$S6FQ2)/6;
    $FMA3 = round($S1FQ3+$S2FQ3+$S3FQ3+$S3FQ7+$S5FQ3+$S6FQ3)/6;
    $FMA4 = round($S1FQ4+$S2FQ4+$S3FQ4+$S3FQ8+$S5FQ4+$S6FQ4)/6;
    $FMA5 = round($S1FQ5+$S2FQ5+$S4FQ1+$S4FQ5+$S5FQ5+$S6FQ5)/6;
    $FMA6 = round($S1FQ6+$S2FQ6+$S4FQ2+$S4FQ6+$S5FQ6+$S6FQ6)/6;
    $FMA7 = round($S1FQ7+$S2FQ7+$S4FQ3+$S4FQ7+$S5FQ7+$S6FQ7)/6;
    $FMA8 = round($S1FQ8+$S2FQ8+$S4FQ4+$S4FQ8+$S5FQ8+$S6FQ8)/6;

    for($i=1;$i<=8;$i++){
      ${"fqFMA".$i} = $this->frequence(${"FMA".$i});
    }

    $maxperf = 0;
    $maxperf2 = 0;
    // MAX FREQUENCE 1
    for ($i=1; $i < 7; $i++) { 
      if($maxperf < ${"FMA".$i}){
        $maxperf = ${"FMA".$i};
        $indexperf = $i; 
      }
    }

    // MAX FREQUENCE 2
    for ($i=1; $i < 7; $i++) { 
      if($indexperf != $i){
        if($maxperf2 < ${"FMA".$i}){
          $maxperf2 = ${"FMA".$i};
          $indexperf2 = $i; 
        }   
      }
    }

    if(\App::getLocale() == "fr"){
      $text1 = ($indexperf == 1 ? "L'Organisateur" : ( $indexperf == 2 ? "Le Determiné" : ( $indexperf == 3 ? "L'Aimable" : ( $indexperf == 4 ? "Le Comptetiteur" : ( $indexperf == 5 ? "Le Passionné" : ( $indexperf == 6 ? "L'Original" : ( $indexperf == 7 ? "Le Bienveillant" : ( $indexperf == 8 ? "L'Entraineur" : ""))))))));
      $text2 = ($indexperf2 == 1 ? "L'Organisateur" : ( $indexperf2 == 2 ? "Le Determiné" : ( $indexperf2 == 3 ? "L'Aimable" : ( $indexperf2 == 4 ? "Le Comptetiteur" : ( $indexperf2 == 5 ? "Le Passionné" : ( $indexperf2 == 6 ? "L'Original" : ( $indexperf2 == 7 ? "Le Bienveillant" : ( $indexperf2 == 8 ? "L'Entraineur" : ""))))))));

      $textH1 = ($indexperf == 1 ? "Manager du projet" : ( $indexperf == 2 ? "Leader du changement" : ( $indexperf == 3 ? "Leader amical" : ( $indexperf == 4 ? "Leader Compétiteur" : ( $indexperf == 5 ? "Manager de la qualité" : ( $indexperf == 6 ? "Leader entreprenant" : ( $indexperf == 7 ? "Manager animateur" : ( $indexperf == 8 ? "Manager coach" : ""))))))));

      $textH2 = ($indexperf2 == 1 ? "Manager du projet" : ( $indexperf2 == 2 ? "Leader du changement" : ( $indexperf2 == 3 ? "Leader amical" : ( $indexperf2 == 4 ? "Leader Compétiteur" : ( $indexperf2 == 5 ? "Manager de la qualité" : ( $indexperf2 == 6 ? "Leader entreprenant" : ( $indexperf2 == 7 ? "Manager animateur" : ( $indexperf2 == 8 ? "Manager coach" : ""))))))));

      for ($i=1; $i <7 ; $i++) { 

        ${"FM".$i} = round(${"FMS".$i}/${"FMA".$i});
        ${"FMR".$i} = (( ${"FM".$i} < 2 ? "Très rarement" : ( ${"FM".$i} >=2 && ${"FM".$i} < 3 ? "Rarement" : ( ${"FM".$i} >= 3 && ${"FM".$i} < 4 ? "Parfois" : ( ${"FM".$i} >= 4 && ${"FM".$i} < 5 ? "Souvent" : ( ${"FM".$i} >= 5 ? "Très souvent" : "" ))))));
      }

    } else {

      $text1 = ($indexperf == 1 ? "The Organizer" : ( $indexperf == 2 ? "The Challenger" : ( $indexperf == 3 ? "The Amiable" : ( $indexperf == 4 ? "The Competitor" : ( $indexperf == 5 ? "The Enthusiast" : ( $indexperf == 6 ? "The Maverick" : ( $indexperf == 7 ? "The Benevolent" : ( $indexperf == 8 ? "The Helper" : ""))))))));
      $text2 = ($indexperf2 == 1 ? "The Organizer" : ( $indexperf2 == 2 ? "The Challenger" : ( $indexperf2 == 3 ? "The Amiable" : ( $indexperf2 == 4 ? "The Competitor" : ( $indexperf2 == 5 ? "The Enthusiast" : ( $indexperf2 == 6 ? "The Maverick" : ( $indexperf2 == 7 ? "The Benevolent" : ( $indexperf2 == 8 ? "The Helper" : ""))))))));

      $textH1 = ($indexperf == 1 ? "Project manager" : ( $indexperf == 2 ? "Change leader" : ( $indexperf == 3 ? "Pleaser-leader" : ( $indexperf == 4 ? "Competitive leader" : ( $indexperf == 5 ? "Quality manager" : ( $indexperf == 6 ? "Entrepreneurial leader" : ( $indexperf == 7 ? "Caring manager" : ( $indexperf == 8 ? "Coaching manager" : ""))))))));

      $textH2 = ($indexperf2 == 1 ? "Project manager" : ( $indexperf2 == 2 ? "Change leader" : ( $indexperf2 == 3 ? "Pleaser-leader" : ( $indexperf2 == 4 ? "Competitive leader" : ( $indexperf2 == 5 ? "Quality manager" : ( $indexperf2 == 6 ? "Entrepreneurial leader" : ( $indexperf2 == 7 ? "Caring manager" : ( $indexperf2 == 8 ? "Coaching manager" : ""))))))));

      for ($i=1; $i <7 ; $i++) { 

        ${"FM".$i} = round(${"FMS".$i}/${"FMA".$i});
        ${"FMR".$i} = (( ${"FM".$i} < 2 ? "Very Rarely" : ( ${"FM".$i} >=2 && ${"FM".$i} < 3 ? "Rarely" : ( ${"FM".$i} >= 3 && ${"FM".$i} < 4 ? "Sometimes" : ( ${"FM".$i} >= 4 && ${"FM".$i} < 5 ? "Often" : ( ${"FM".$i} >= 5 ? "Very often" : "" ))))));
      }

    }

    // for ($i=1; $i <7 ; $i++) { 

    //   ${"FM".$i} = round(${"FMS".$i}/${"FMA".$i});
    //   ${"FMR".$i} = (( ${"FM".$i} < 2 ? "Très rarement" : ( ${"FM".$i} >=2 && ${"FM".$i} < 3 ? "Rarement" : ( ${"FM".$i} >= 3 && ${"FM".$i} < 4 ? "Parfois" : ( ${"FM".$i} >= 4 && ${"FM".$i} < 5 ? "Souvent" : ( ${"FM".$i} >= 5 ? "Très souvent" : "" ))))));
    // }

    // for ($i=2; $i < 12; $i++) { 
    //   $pdf->addPage();
    //   $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    //   $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
    //   $pdf->useImportedPage($pageId);
    // }

    ///////////////// LES DONNEES POUR LA SERIE 7 AU PLUS ///////////////////

    // nombre sup a 2 pour la serie 7
    $NTS7 = 0;
    // nombre reponse au total pour la serie 7
    $NTTS7 = 0;
    // nombre sup a 2 pour la serie 8
    $NTS8 = 0;
    // nombre reponse au total pour la serie 8
    $NTTS8 = 0;

    for ($i=1; $i < 17; $i++){ 
      $NTS7 = $NTS7 + (isset($sq2[7][$i]) ? isset($sq2[7][$i]) : 0);
      $NTTS7 = $NTTS7 + (isset($sqtot[7][$i]) ? isset($sqtot[7][$i]) : 0);

      $NTS8 = $NTS8 + (isset($sq2[8][$i]) ? isset($sq2[8][$i]) : 0);
      $NTTS8 = $NTTS8 + (isset($sq2[8][$i]) ? isset($sq2[8][$i]) : 0);

    }     

    $S7A = ((isset($sq2[7][1]) ? $sq2[7][1] : 0)+(isset($sq2[7][3]) ? $sq2[7][3] : 0)+(isset($sq2[7][5]) ? $sq2[7][5] : 0)+(isset($sq2[7][7]) ? $sq2[7][7] : 0)+(isset($sq2[7][9]) ? $sq2[7][9] : 0)+(isset($sq2[7][11]) ? $sq2[7][11] : 0)+(isset($sq2[7][13]) ? $sq2[7][13] : 0)+(isset($sq2[7][15]) ? $sq2[7][15] : 0))/$NTS7;
    $S7B = ((isset($sq2[7][2]) ? $sq2[7][2] : 0)+(isset($sq2[7][4]) ? $sq2[7][4] : 0)+(isset($sq2[7][6]) ? $sq2[7][6] : 0)+(isset($sq2[7][8]) ? $sq2[7][8] : 0)+(isset($sq2[7][10]) ? $sq2[7][10] : 0)+(isset($sq2[7][12]) ? $sq2[7][12] : 0)+(isset($sq2[7][14]) ? $sq2[7][14] : 0)+(isset($sq2[7][16]) ? $sq2[7][16] : 0))/$NTS7;
    $R7SUP2 = $NTS7/$NTTS7;

    $S8A = ((isset($sq2[8][1]) ? $sq2[8][1] : 0)+(isset($sq2[8][3]) ? $sq2[8][3] : 0)+(isset($sq2[8][5]) ? $sq2[8][5] : 0)+(isset($sq2[8][7]) ? $sq2[8][7] : 0)+(isset($sq2[8][9]) ? $sq2[8][9] : 0)+(isset($sq2[8][11]) ? $sq2[8][11] : 0)+(isset($sq2[8][13]) ? $sq2[8][13] : 0)+(isset($sq2[8][15]) ? $sq2[8][15] : 0))/$NTS8;
    $S8B = ((isset($sq2[8][2]) ? $sq2[8][2] : 0)+(isset($sq2[8][4]) ? $sq2[8][4] : 0)+(isset($sq2[8][6]) ? $sq2[8][6] : 0)+(isset($sq2[8][8]) ? $sq2[8][8] : 0)+(isset($sq2[8][10]) ? $sq2[8][10] : 0)+(isset($sq2[8][12]) ? $sq2[8][12] : 0)+(isset($sq2[8][14]) ? $sq2[8][14] : 0)+(isset($sq2[8][16]) ? $sq2[8][16] : 0))/$NTS8;
    $R8SUP2 = $NTS8/$NTTS8;

    $NTS7S8 = $NTS7 + $NTS8;

    $NCCS7S8 = $NTTS7 + $NTTS8;

    // CALCUL CLIMAT
    $NTC1 = (isset($sq2[7][1]) ? $sq2[7][1] : 0) + (isset($sq2[7][2]) ? $sq2[7][2] : 0)+(isset($sq2[8][1]) ? $sq2[8][1] : 0)+(isset($sq2[8][2])?$sq2[8][2]:0);
    $NTC2 = (isset($sq2[7][3])?$sq2[7][3]:0)+(isset($sq2[7][4])?$sq2[7][4]:0)+(isset($sq2[8][3])?$sq2[8][3]:0)+(isset($sq2[8][4])?$sq2[8][4]:0);
    $NTC3 = (isset($sq2[7][5])?$sq2[7][5]:0)+(isset($sq2[7][6])?$sq2[7][6]:0)+(isset($sq2[8][5])?$sq2[8][5]:0)+(isset($sq2[8][6])?$sq2[8][6]:0);
    $NTC4 = (isset($sq2[7][7])?$sq2[7][7]:0)+(isset($sq2[7][8])?$sq2[7][8]:0)+(isset($sq2[8][7])?$sq2[8][7]:0)+(isset($sq2[8][8])?$sq2[8][8]:0);
    $NTC5 = (isset($sq2[7][9])?$sq2[7][9]:0)+(isset($sq2[7][10])?$sq2[7][10]:0)+(isset($sq2[8][9])?$sq2[8][9]:0)+(isset($sq2[8][10])?$sq2[8][10]:0);
    $NTC6 = (isset($sq2[7][11])?$sq2[7][11]:0)+(isset($sq2[7][12])?$sq2[7][12]:0)+(($sq2[8][11])?$sq2[8][11]:0)+(isset($sq2[8][12])?$sq2[8][12]:0);
    $NTC7 = (isset($sq2[7][13])?$sq2[7][13]:0)+(isset($sq2[7][14])?$sq2[7][14]:0)+(isset($sq2[8][13])?$sq2[8][13]:0)+(isset($sq2[8][14])?$sq2[8][14]:0);
    $NTC8 = (isset($sq2[7][15])?$sq2[7][15]:0)+(isset($sq2[7][16])?$sq2[7][16]:0)+(isset($sq2[8][15])?$sq2[8][15]:0)+(isset($sq2[8][16])?$sq2[8][16]:0);

    // POURCENTAGE CLIMAT
    $perNTC1 = round(($NTC1*100) / $NTS7S8);
    $perNTC2 = round(($NTC2*100) / $NTS7S8);
    $perNTC3 = round(($NTC3*100) / $NTS7S8);
    $perNTC4 = round(($NTC4*100) / $NTS7S8);
    $perNTC5 = round(($NTC5*100) / $NTS7S8);
    $perNTC6 = round(($NTC6*100) / $NTS7S8);
    $perNTC7 = round(($NTC7*100) / $NTS7S8);
    $perNTC8 = round(($NTC8*100) / $NTS7S8);

    // ROND CENTRE CLIMAT
    $somme_gauche = $perNTC1+$perNTC2+$perNTC3+$perNTC4; 
    $somme_droite = $perNTC5+$perNTC6+$perNTC7+$perNTC8; 

    // POURCENTAGE SERIE TOTAL REP SUP A 2 
    $perR2S7S8 = $NTS7S8 / $NCCS7S8;

    // CALCUL PERCU PLUS PAR CLIMAT
    $PP1 = (isset($sq2[7][1])?$sq2[7][1]:0) + (isset($sq2[8][1])?$sq2[8][1]:0); 
    $PP2 = (isset($sq2[7][3])?$sq2[7][3]:0) + (isset($sq2[8][3])?$sq2[8][3]:0); 
    $PP3 = (isset($sq2[7][5])?$sq2[7][5]:0) + (isset($sq2[8][5])?$sq2[8][5]:0); 
    $PP4 = (isset($sq2[7][7])?$sq2[7][7]:0) + (isset($sq2[8][7])?$sq2[8][7]:0); 
    $PP5 = (isset($sq2[7][9])?$sq2[7][9]:0) + (isset($sq2[8][9])?$sq2[8][9]:0); 
    $PP6 = (isset($sq2[7][11])?$sq2[7][11]:0) + (isset($sq2[8][11])?$sq2[8][11]:0); 
    $PP7 = (isset($sq2[7][13])?$sq2[7][13]:0) + (isset($sq2[8][13])?$sq2[8][13]:0); 
    $PP8 = (isset($sq2[7][15])?$sq2[7][15]:0) + (isset($sq2[8][15])?$sq2[8][15]:0);

    $PP1Vous = (isset($sqVous[7][1])?$sqVous[7][1]:0) + (isset($sqVous[8][1])?$sqVous[8][1]:0); 
    $PP2Vous = (isset($sqVous[7][3])?$sqVous[7][3]:0) + (isset($sqVous[8][3])?$sqVous[8][3]:0); 
    $PP3Vous = (isset($sqVous[7][5])?$sqVous[7][5]:0) + (isset($sqVous[8][5])?$sqVous[8][5]:0); 
    $PP4Vous = (isset($sqVous[7][7])?$sqVous[7][7]:0) + (isset($sqVous[8][7])?$sqVous[8][7]:0); 
    $PP5Vous = (isset($sqVous[7][9])?$sqVous[7][9]:0) + (isset($sqVous[8][9])?$sqVous[8][9]:0); 
    $PP6Vous = (isset($sqVous[7][11])?$sqVous[7][11]:0) + (isset($sqVous[8][11])?$sqVous[8][11]:0); 
    $PP7Vous = (isset($sqVous[7][13])?$sqVous[7][13]:0) + (isset($sqVous[8][13])?$sqVous[8][13]:0); 
    $PP8Vous = (isset($sqVous[7][15])?$sqVous[7][15]:0) + (isset($sqVous[8][15])?$sqVous[8][15]:0);

    $PP1Autres = (isset($sqAutres[7][1])?$sqAutres[7][1]:0) + (isset($sqAutres[8][1])?$sqAutres[8][1]:0); 
    $PP2Autres = (isset($sqAutres[7][3])?$sqAutres[7][3]:0) + (isset($sqAutres[8][3])?$sqAutres[8][3]:0); 
    $PP3Autres = (isset($sqAutres[7][5])?$sqAutres[7][5]:0) + (isset($sqAutres[8][5])?$sqAutres[8][5]:0); 
    $PP4Autres = (isset($sqAutres[7][7])?$sqAutres[7][7]:0) + (isset($sqAutres[8][7])?$sqAutres[8][7]:0); 
    $PP5Autres = (isset($sqAutres[7][9])?$sqAutres[7][9]:0) + (isset($sqAutres[8][9])?$sqAutres[8][9]:0); 
    $PP6Autres = (isset($sqAutres[7][11])?$sqAutres[7][11]:0) + (isset($sqAutres[8][11])?$sqAutres[8][11]:0); 
    $PP7Autres = (isset($sqAutres[7][13])?$sqAutres[7][13]:0) + (isset($sqAutres[8][13])?$sqAutres[8][13]:0); 
    $PP8Autres = (isset($sqAutres[7][15])?$sqAutres[7][15]:0) + (isset($sqAutres[8][15])?$sqAutres[8][15]:0);

    $PP1Pairs = (isset($sqPairs[7][1])?$sqPairs[7][1]:0) + (isset($sqPairs[8][1])?$sqPairs[8][1]:0); 
    $PP2Pairs = (isset($sqPairs[7][3])?$sqPairs[7][3]:0) + (isset($sqPairs[8][3])?$sqPairs[8][3]:0); 
    $PP3Pairs = (isset($sqPairs[7][5])?$sqPairs[7][5]:0) + (isset($sqPairs[8][5])?$sqPairs[8][5]:0); 
    $PP4Pairs = (isset($sqPairs[7][7])?$sqPairs[7][7]:0) + (isset($sqPairs[8][7])?$sqPairs[8][7]:0); 
    $PP5Pairs = (isset($sqPairs[7][9])?$sqPairs[7][9]:0) + (isset($sqPairs[8][9])?$sqPairs[8][9]:0); 
    $PP6Pairs = (isset($sqPairs[7][11])?$sqPairs[7][11]:0) + (isset($sqPairs[8][11])?$sqPairs[8][11]:0); 
    $PP7Pairs = (isset($sqPairs[7][13])?$sqPairs[7][13]:0) + (isset($sqPairs[8][13])?$sqPairs[8][13]:0); 
    $PP8Pairs = (isset($sqPairs[7][15])?$sqPairs[7][15]:0) + (isset($sqPairs[8][15])?$sqPairs[8][15]:0);

    $PP1Direct = (isset($sqDirect[7][1])?$sqDirect[7][1]:0) + (isset($sqDirect[8][1])?$sqDirect[8][1]:0); 
    $PP2Direct = (isset($sqDirect[7][3])?$sqDirect[7][3]:0) + (isset($sqDirect[8][3])?$sqDirect[8][3]:0); 
    $PP3Direct = (isset($sqDirect[7][5])?$sqDirect[7][5]:0) + (isset($sqDirect[8][5])?$sqDirect[8][5]:0); 
    $PP4Direct = (isset($sqDirect[7][7])?$sqDirect[7][7]:0) + (isset($sqDirect[8][7])?$sqDirect[8][7]:0); 
    $PP5Direct = (isset($sqDirect[7][9])?$sqDirect[7][9]:0) + (isset($sqDirect[8][9])?$sqDirect[8][9]:0); 
    $PP6Direct = (isset($sqDirect[7][11])?$sqDirect[7][11]:0) + (isset($sqDirect[8][11])?$sqDirect[8][11]:0); 
    $PP7Direct = (isset($sqDirect[7][13])?$sqDirect[7][13]:0) + (isset($sqDirect[8][13])?$sqDirect[8][13]:0); 
    $PP8Direct = (isset($sqDirect[7][15])?$sqDirect[7][15]:0) + (isset($sqDirect[8][15])?$sqDirect[8][15]:0);


    // CALCUL PERCU MOIN PAR CLIMAT
    $PM1 = (isset($sq2[7][2])?$sq2[7][2]:0) + (isset($sq2[8][2])?$sq2[8][2]:0); 
    $PM2 = (isset($sq2[7][4])?$sq2[7][4]:0) + (isset($sq2[8][4])?$sq2[8][4]:0); 
    $PM3 = (isset($sq2[7][6])?$sq2[7][6]:0) + (isset($sq2[8][6])?$sq2[8][6]:0); 
    $PM4 = (isset($sq2[7][8])?$sq2[7][8]:0) + (isset($sq2[8][8])?$sq2[8][8]:0); 
    $PM5 = (isset($sq2[7][10])?$sq2[7][10]:0) + (isset($sq2[8][10])?$sq2[8][10]:0); 
    $PM6 = (isset($sq2[7][12])?$sq2[7][12]:0) + (isset($sq2[8][12])?$sq2[8][12]:0); 
    $PM7 = (isset($sq2[7][14])?$sq2[7][14]:0) + (isset($sq2[8][14])?$sq2[8][14]:0); 
    $PM8 = (isset($sq2[7][16])?$sq2[7][16]:0) + (isset($sq2[8][16])?$sq2[8][16]:0); 

    $PM1Vous = (isset($sqVous[7][2])?$sqVous[7][2]:0) + (isset($sqVous[8][2])?$sqVous[8][2]:0); 
    $PM2Vous = (isset($sqVous[7][4])?$sqVous[7][4]:0) + (isset($sqVous[8][4])?$sqVous[8][4]:0); 
    $PM3Vous = (isset($sqVous[7][6])?$sqVous[7][6]:0) + (isset($sqVous[8][6])?$sqVous[8][6]:0); 
    $PM4Vous = (isset($sqVous[7][8])?$sqVous[7][8]:0) + (isset($sqVous[8][8])?$sqVous[8][8]:0); 
    $PM5Vous = (isset($sqVous[7][10])?$sqVous[7][10]:0) + (isset($sqVous[8][10])?$sqVous[8][10]:0); 
    $PM6Vous = (isset($sqVous[7][12])?$sqVous[7][12]:0) + (isset($sqVous[8][12])?$sqVous[8][12]:0); 
    $PM7Vous = (isset($sqVous[7][14])?$sqVous[7][14]:0) + (isset($sqVous[8][14])?$sqVous[8][14]:0); 
    $PM8Vous = (isset($sqVous[7][16])?$sqVous[7][16]:0) + (isset($sqVous[8][16])?$sqVous[8][16]:0); 

    $PM1Autres = (isset($sqAutres[7][2])?$sqAutres[7][2]:0) + (isset($sqAutres[8][2])?$sqAutres[8][2]:0); 
    $PM2Autres = (isset($sqAutres[7][4])?$sqAutres[7][4]:0) + (isset($sqAutres[8][4])?$sqAutres[8][4]:0); 
    $PM3Autres = (isset($sqAutres[7][6])?$sqAutres[7][6]:0) + (isset($sqAutres[8][6])?$sqAutres[8][6]:0); 
    $PM4Autres = (isset($sqAutres[7][8])?$sqAutres[7][8]:0) + (isset($sqAutres[8][8])?$sqAutres[8][8]:0); 
    $PM5Autres = (isset($sqAutres[7][10])?$sqAutres[7][10]:0) + (isset($sqAutres[8][10])?$sqAutres[8][10]:0); 
    $PM6Autres = (isset($sqAutres[7][12])?$sqAutres[7][12]:0) + (isset($sqAutres[8][12])?$sqAutres[8][12]:0); 
    $PM7Autres = (isset($sqAutres[7][14])?$sqAutres[7][14]:0) + (isset($sqAutres[8][14])?$sqAutres[8][14]:0); 
    $PM8Autres = (isset($sqAutres[7][16])?$sqAutres[7][16]:0) + (isset($sqAutres[8][16])?$sqAutres[8][16]:0);  

    $PM1Pairs = (isset($sqPairs[7][2])?$sqPairs[7][2]:0) + (isset($sqPairs[8][2])?$sqPairs[8][2]:0); 
    $PM2Pairs = (isset($sqPairs[7][4])?$sqPairs[7][4]:0) + (isset($sqPairs[8][4])?$sqPairs[8][4]:0); 
    $PM3Pairs = (isset($sqPairs[7][6])?$sqPairs[7][6]:0) + (isset($sqPairs[8][6])?$sqPairs[8][6]:0); 
    $PM4Pairs = (isset($sqPairs[7][8])?$sqPairs[7][8]:0) + (isset($sqPairs[8][8])?$sqPairs[8][8]:0); 
    $PM5Pairs = (isset($sqPairs[7][10])?$sqPairs[7][10]:0) + (isset($sqPairs[8][10])?$sqPairs[8][10]:0); 
    $PM6Pairs = (isset($sqPairs[7][12])?$sqPairs[7][12]:0) + (isset($sqPairs[8][12])?$sqPairs[8][12]:0); 
    $PM7Pairs = (isset($sqPairs[7][14])?$sqPairs[7][14]:0) + (isset($sqPairs[8][14])?$sqPairs[8][14]:0); 
    $PM8Pairs = (isset($sqPairs[7][16])?$sqPairs[7][16]:0) + (isset($sqPairs[8][16])?$sqPairs[8][16]:0);    

    $PM1Direct = (isset($sqDirect[7][2])?$sqDirect[7][2]:0) + (isset($sqDirect[8][2])?$sqDirect[8][2]:0); 
    $PM2Direct = (isset($sqDirect[7][4])?$sqDirect[7][4]:0) + (isset($sqDirect[8][4])?$sqDirect[8][4]:0); 
    $PM3Direct = (isset($sqDirect[7][6])?$sqDirect[7][6]:0) + (isset($sqDirect[8][6])?$sqDirect[8][6]:0); 
    $PM4Direct = (isset($sqDirect[7][8])?$sqDirect[7][8]:0) + (isset($sqDirect[8][8])?$sqDirect[8][8]:0); 
    $PM5Direct = (isset($sqDirect[7][10])?$sqDirect[7][10]:0) + (isset($sqDirect[8][10])?$sqDirect[8][10]:0); 
    $PM6Direct = (isset($sqDirect[7][12])?$sqDirect[7][12]:0) + (isset($sqDirect[8][12])?$sqDirect[8][12]:0); 
    $PM7Direct = (isset($sqDirect[7][14])?$sqDirect[7][14]:0) + (isset($sqDirect[8][14])?$sqDirect[8][14]:0); 
    $PM8Direct = (isset($sqDirect[7][16])?$sqDirect[7][16]:0) + (isset($sqDirect[8][16])?$sqDirect[8][16]:0);    

    // POURCENTAGE DE PERCU PLUS

    $NTS7S8 = $PP1+ $PP2+ $PP3 +$PP4+ $PP5+$PP6+ $PP7+$PP8 + $PM1+ $PM2+ $PM3 +$PM4+ $PM5+$PM6+ $PM7+$PM8;
    $NTS7S8Direct = $PP1Direct+ $PP2Direct+ $PP3Direct +$PP4Direct+ $PP5Direct+$PP6Direct+ $PP7Direct+$PP8Direct + $PM1Direct+ $PM2Direct+ $PM3Direct +$PM4Direct+ $PM5Direct+$PM6Direct+ $PM7Direct+$PM8Direct;
    
    $perPP1 = floatval($PP1/$NTS7S8)*100;
    $perPP2 = floatval($PP2/$NTS7S8)*100;
    $perPP3 = floatval($PP3/$NTS7S8)*100;
    $perPP4 = floatval($PP4/$NTS7S8)*100;
    $perPP5 = floatval($PP5/$NTS7S8)*100;
    $perPP6 = floatval($PP6/$NTS7S8)*100;
    $perPP7 = floatval($PP7/$NTS7S8)*100;
    $perPP8 = floatval($PP8/$NTS7S8)*100;

    // MAX PERCU PLUS 1
    $indplus1 = 0;
    $maxplus1 = 0;
    for ($i=1; $i < 9; $i++) { 
      if($maxplus1 < ${"perPP".$i}){
        $maxplus1 = ${"perPP".$i};
        $indplus1 = $i;
      }    
    }

    $qplus1 = ($indplus1 == 1 ? 1 : ( $indplus1 == 2 ? 3 : ( $indplus1 == 3 ? 5 : ( $indplus1 == 4 ? 7 : ( $indplus1 == 5 ? 9 : ( $indplus1 == 6 ? 11 : ( $indplus1 == 7 ? 13 : ( $indplus1 == 8 ? 15 : 0))))))));

    $perPP1Vous = floatval($PP1Vous/$NTS7S8)*100;
    $perPP2Vous = floatval($PP2Vous/$NTS7S8)*100;
    $perPP3Vous = floatval($PP3Vous/$NTS7S8)*100;
    $perPP4Vous = floatval($PP4Vous/$NTS7S8)*100;
    $perPP5Vous = floatval($PP5Vous/$NTS7S8)*100;
    $perPP6Vous = floatval($PP6Vous/$NTS7S8)*100;
    $perPP7Vous = floatval($PP7Vous/$NTS7S8)*100;
    $perPP8Vous = floatval($PP8Vous/$NTS7S8)*100;

    $perPP1Pairs = floatval($PP1Pairs/$NTS7S8)*100;
    $perPP2Pairs = floatval($PP2Pairs/$NTS7S8)*100;
    $perPP3Pairs = floatval($PP3Pairs/$NTS7S8)*100;
    $perPP4Pairs = floatval($PP4Pairs/$NTS7S8)*100;
    $perPP5Pairs = floatval($PP5Pairs/$NTS7S8)*100;
    $perPP6Pairs = floatval($PP6Pairs/$NTS7S8)*100;
    $perPP7Pairs = floatval($PP7Pairs/$NTS7S8)*100;
    $perPP8Pairs = floatval($PP8Pairs/$NTS7S8)*100;

    $perPP1Autres = floatval($PP1Autres/$NTS7S8)*100;
    $perPP2Autres = floatval($PP2Autres/$NTS7S8)*100;
    $perPP3Autres = floatval($PP3Autres/$NTS7S8)*100;
    $perPP4Autres = floatval($PP4Autres/$NTS7S8)*100;
    $perPP5Autres = floatval($PP5Autres/$NTS7S8)*100;
    $perPP6Autres = floatval($PP6Autres/$NTS7S8)*100;
    $perPP7Autres = floatval($PP7Autres/$NTS7S8)*100;
    $perPP8Autres = floatval($PP8Autres/$NTS7S8)*100;

    $perPP1Direct = floatval($PP1Direct/$NTS7S8Direct)*100;
    $perPP2Direct = floatval($PP2Direct/$NTS7S8Direct)*100;
    $perPP3Direct = floatval($PP3Direct/$NTS7S8Direct)*100;
    $perPP4Direct = floatval($PP4Direct/$NTS7S8Direct)*100;
    $perPP5Direct = floatval($PP5Direct/$NTS7S8Direct)*100;
    $perPP6Direct = floatval($PP6Direct/$NTS7S8Direct)*100;
    $perPP7Direct = floatval($PP7Direct/$NTS7S8Direct)*100;
    $perPP8Direct = floatval($PP8Direct/$NTS7S8Direct)*100;
    
    // TOTAL POURCENTAGE PERCU PLUS
    $perTPPS7S8 = $perPP1+$perPP2+$perPP3+$perPP4+$perPP5+$perPP6+$perPP7+$perPP8;

    $perTPPS7S8Direct = $perPP1Direct+$perPP2Direct+$perPP3Direct+$perPP4Direct+$perPP5Direct+$perPP6Direct+$perPP7Direct+$perPP8Direct;
    
    // POURCENTAGE DE PERCU MOIN

    $perPM1 = floatval($PM1/$NTS7S8)*100;
    $perPM2 = floatval($PM2/$NTS7S8)*100;
    $perPM3 = floatval($PM3/$NTS7S8)*100;
    $perPM4 = floatval($PM4/$NTS7S8)*100;
    $perPM5 = floatval($PM5/$NTS7S8)*100;
    $perPM6 = floatval($PM6/$NTS7S8)*100;
    $perPM7 = floatval($PM7/$NTS7S8)*100;
    $perPM8 = floatval($PM8/$NTS7S8)*100;

    // MAX PERCU MOIN 1
    $indplus2 = 0;
    $maxplus2 = 0;
    for ($i=1; $i < 9; $i++) { 
      if($indplus1 != $i){
        if($maxplus2 < ${"perPM".$i}){
          $maxplus2 = ${"perPM".$i};
          $indplus2 = $i;
        }
      }
    }

    $qplus2 = ($indplus2 == 1 ? 2 : ( $indplus2 == 2 ? 4 : ( $indplus2 == 3 ? 6 : ( $indplus2 == 4 ? 8 : ( $indplus2 == 5 ? 10 : ( $indplus2 == 6 ? 12 : ( $indplus2 == 7 ? 14 : ( $indplus2 == 8 ? 16 : 0))))))));

    $perPM1Vous = floatval($PM1Vous/$NTS7S8)*100;
    $perPM2Vous = floatval($PM2Vous/$NTS7S8)*100;
    $perPM3Vous = floatval($PM3Vous/$NTS7S8)*100;
    $perPM4Vous = floatval($PM4Vous/$NTS7S8)*100;
    $perPM5Vous = floatval($PM5Vous/$NTS7S8)*100;
    $perPM6Vous = floatval($PM6Vous/$NTS7S8)*100;
    $perPM7Vous = floatval($PM7Vous/$NTS7S8)*100;
    $perPM8Vous = floatval($PM8Vous/$NTS7S8)*100;

    $perPM1Pairs = floatval($PM1Pairs/$NTS7S8)*100;
    $perPM2Pairs = floatval($PM2Pairs/$NTS7S8)*100;
    $perPM3Pairs = floatval($PM3Pairs/$NTS7S8)*100;
    $perPM4Pairs = floatval($PM4Pairs/$NTS7S8)*100;
    $perPM5Pairs = floatval($PM5Pairs/$NTS7S8)*100;
    $perPM6Pairs = floatval($PM6Pairs/$NTS7S8)*100;
    $perPM7Pairs = floatval($PM7Pairs/$NTS7S8)*100;
    $perPM8Pairs = floatval($PM8Pairs/$NTS7S8)*100;

    $perPM1Direct = floatval($PM1Direct/$NTS7S8Direct)*100;
    $perPM2Direct = floatval($PM2Direct/$NTS7S8Direct)*100;
    $perPM3Direct = floatval($PM3Direct/$NTS7S8Direct)*100;
    $perPM4Direct = floatval($PM4Direct/$NTS7S8Direct)*100;
    $perPM5Direct = floatval($PM5Direct/$NTS7S8Direct)*100;
    $perPM6Direct = floatval($PM6Direct/$NTS7S8Direct)*100;
    $perPM7Direct = floatval($PM7Direct/$NTS7S8Direct)*100;
    $perPM8Direct = floatval($PM8Direct/$NTS7S8Direct)*100;

    $perPM1Autres = floatval($PM1Autres/$NTS7S8)*100;
    $perPM2Autres = floatval($PM2Autres/$NTS7S8)*100;
    $perPM3Autres = floatval($PM3Autres/$NTS7S8)*100;
    $perPM4Autres = floatval($PM4Autres/$NTS7S8)*100;
    $perPM5Autres = floatval($PM5Autres/$NTS7S8)*100;
    $perPM6Autres = floatval($PM6Autres/$NTS7S8)*100;
    $perPM7Autres = floatval($PM7Autres/$NTS7S8)*100;
    $perPM8Autres = floatval($PM8Autres/$NTS7S8)*100;


    

    // TOTAL POURCENTAGE PERCU MOIN
    $perTPMS7S8 = $perPM1+$perPM2+$perPM3+$perPM4+$perPM5+$perPM6+$perPM7+$perPM8;
    $perTPMS7S8Direct = $perPM1Direct+$perPM2Direct+$perPM3Direct+$perPM4Direct+$perPM5Direct+$perPM6Direct+$perPM7Direct+$perPM8Direct;

    // POURCENTAGE DES SCORE DE CHAQUE QUESTIONS
    $perPPS8Q1 = floatval($sq2[8][1]/$NTS7S8)*100;
    $perPPS8Q2 = floatval($sq2[8][2]/$NTS7S8)*100;
    $perPPS8Q3 = floatval($sq2[8][3]/$NTS7S8)*100;
    $perPPS8Q4 = floatval($sq2[8][4]/$NTS7S8)*100;
    $perPPS8Q5 = floatval($sq2[8][5]/$NTS7S8)*100;
    $perPPS8Q6 = floatval($sq2[8][6]/$NTS7S8)*100;
    $perPPS8Q7 = floatval($sq2[8][7]/$NTS7S8)*100;
    $perPPS8Q8 = floatval($sq2[8][8]/$NTS7S8)*100;
    $perPPS8Q9 = floatval($sq2[8][9]/$NTS7S8)*100;
    $perPPS8Q10 = floatval($sq2[8][10]/$NTS7S8)*100;
    $perPPS8Q11 = floatval($sq2[8][11]/$NTS7S8)*100;
    $perPPS8Q12 = floatval($sq2[8][12]/$NTS7S8)*100;
    $perPPS8Q13 = floatval($sq2[8][13]/$NTS7S8)*100;
    $perPPS8Q14 = floatval($sq2[8][14]/$NTS7S8)*100;
    $perPPS8Q15 = floatval($sq2[8][15]/$NTS7S8)*100;
    $perPPS8Q16 = floatval($sq2[8][16]/$NTS7S8)*100;

    $perTPPS8 = 0;
    $perTPMS8 = 0;
    for ($i=1; $i < 17; $i++) { 
      if(($i%2) != 0){
        $perTPPS8 += ${"perPPS8Q".$i};
      }
      if(($i%2) == 0){
        $perTPMS8 += ${"perPPS8Q".$i};
      }
      
    }

    $maxSer8 = 0;
    $indSer8 = 0;
    $maxMSer8 = 0;
    $indMSer8 = 0;
    $ppF = [];
    $ppFVous = [];
    $ppFAutres = [];
    $ppFPairs = [];
    $ppFDirect = [];

    $pmF = [];
    $pmFVous = [];
    $pmFAutres = [];
    $pmFPairs = [];
    $pmFDirect = [];
   
    for ($i=1; $i < 17; $i++) { 
   
      if(($i%2) != 0){
        $ppF[$i] = $sq_tot[8][$i]["frequence"] ? $sq_tot[8][$i]["frequence"] : "-1" ;
        $ppFVous[$i] = $sq_totVous[8][$i]["frequence"] ? $sq_totVous[8][$i]["frequence"] : "-1" ;
        $ppFPairs[$i] = $sq_totPairs[8][$i]["frequence"] ? $sq_totPairs[8][$i]["frequence"] : "-1" ;
        $ppFAutres[$i] = $sq_totAutres[8][$i]["frequence"] ? $sq_totAutres[8][$i]["frequence"] : "-1" ;
        $ppFDirect[$i] = $sq_totDirect[8][$i]["frequence"] ? $sq_totDirect[8][$i]["frequence"] : "-1" ;
      }
      if(($i%2) == 0){
        $pmF[$i] = $sq_tot[8][$i]["frequence"] ? $sq_tot[8][$i]["frequence"] : "-1" ;
        $pmFVous[$i] = $sq_totVous[8][$i]["frequence"] ? $sq_totVous[8][$i]["frequence"] : "-1" ;
        $pmFPairs[$i] = $sq_totPairs[8][$i]["frequence"] ? $sq_totPairs[8][$i]["frequence"] : "-1" ;
        $pmFAutres[$i] = $sq_totAutres[8][$i]["frequence"] ? $sq_totAutres[8][$i]["frequence"] : "-1" ;
        $pmFDirect[$i] = $sq_totDirect[8][$i]["frequence"] ? $sq_totDirect[8][$i]["frequence"] : "-1" ;


      } 
    }   






    //////////////////Page 1//////////////////////////////////
    $pdf->addPage("L");

    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);    

    // var_dump($ben->firstname);
    // die();
    if(\App::getLocale() == "fr"){
      $x = 240;
      $y = 20;
      $pdf->SetFont('RobotoCondensed-Regular','',18);
      $pdf->SetTextColor(255,255,255);
      $pdf->SetXY($x, $y); 
      $pdf->MultiCell(100,8, iconv("UTF-8", "windows-1252","Rapport de : ".$ben->firstname." ".$ben->lastname." - ".$ben->organisation." ".$ben->initiale),0,'R',0);
    } else {
      $x = 240;
      $y = 240;
      $pdf->SetFont('RobotoCondensed-Regular','',18);
      $pdf->SetTextColor(128,128,128);
      $pdf->SetXY($x, $y);
      $pdf->MultiCell(100,8, iconv("UTF-8", "windows-1252","Report of : ".$ben->firstname." ".$ben->lastname." - ".$ben->organisation." ".$ben->initiale),0,'R',0);
    }
   
    date_default_timezone_set('Europe/Paris');
    // --- La setlocale() fonctionnne pour strftime mais pas pour DateTime->format()
    setlocale(LC_TIME, 'fr_FR.utf8','fra');// OK
    // strftime("jourEnLettres jour moisEnLettres annee") de la date courante
    $date = date("Y-m-d H:i:s");
    if(\App::getLocale() == "fr"){
      $month = array(
       1 => 'Janvier',
       2 => 'Février',
       3 => 'Mars',
       4 => 'Avril',
       5 => 'Mai',
       6 => 'Juin',
       7 => 'Juillet',
       8 => 'Août',
       9=> 'Septembre',
        10=> 'Octobre',
        11 => 'Novembre',
        12 => 'Décembre'
      );
    } else {
      $month = array(
       1 => 'January',
       2 => 'February',
       3 => 'March',
       4 => 'April',
       5 => 'May',
       6 => 'June',
       7 => 'July',
       8 => 'August',
       9=> 'September',
        10=> 'October',
        11 => 'November',
        12 => 'Décember'
      );
    }
    
    $date =  date("d")." ".$month[round(date("m"))]." ".date("Y") ;

    
    $pdf->SetFont('RobotoCondensed-Bold','',18);
    $pdf->SetTextColor(128,128,128);
    $pdf->SetXY($x, $y+15); 
    $pdf->Cell(100,10, iconv("UTF-8", "windows-1252","Le : ".$date),0,1,'R',0);
    $target_dir2 = $path."templates/archive/liste_images_".$i;
    if (!file_exists($target_dir2)) {
      mkdir($target_dir2, 0777, true);
    }
    //$pdf->Output($path."templates/archive/pptx.pdf",'F');

    $array=array();
    $actuel_path = $path."/archive/pptx.pdf";
    // exec("convert -density 200 ".$path."templates/archive/pptx.pdf -quality 60 ".$path."templates/archive/liste_images/ppt.jpg", $array);
    // die;
    //////////////////Page 2//////////////////////////////////
    $pdf->addPage("L");

    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(2, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId); 

    $x = 140;
    $y = 50;   
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetTextColor(13, 13, 13);
    $pdf->SetXY($x, $y+10);

    $date_debut = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "asc")->first()->updated_at;
    $date_fin = TestRepondant::where("test_id", $test->id)->orderBy("updated_at", "desc")->first()->updated_at;
  
    $date_debut = strtotime($date_debut);
    $date_fin = strtotime($date_fin);

    $date_debut_text =  date("d", $date_debut)." ".$month[round(date("m", $date_debut))]." ".date("Y", $date_debut) ;
    $date_fin_text =  date("d", $date_fin)." ".$month[round(date("m", $date_fin))]." ".date("Y", $date_fin) ;


    $repondants = $test->users;
    $groups =[];
    foreach ($repondants as $key => $rep) {
      $rep->groupe = $rep->testRepondants()->where("test_id", $id)->first() ? $rep->testRepondants()->where("test_id", $id)->first()->groupe : null;
      if(!$rep->groupe){
        continue;
      }
      $group = strtoupper($rep->groupe->abbreviation);
      $reponse = TestRepondant::where("repondant_id", $rep->id)->where("test_id", $test->id)->where("reponse", 1)->count();

      if(!array_key_exists($group, $groups)){
        $groups[$group]['repondants'] =1;
        $groups[$group]['reponses'] =0;
        if($reponse > 0){
          $groups[$group]['reponses'] =1;
        }
      }else{
        $groups[$group]['repondants'] +=1;
        if($reponse > 0){
          $groups[$group]['reponses'] +=1;
        }
      }
    }

    
    $nbPairs = (array_key_exists("PAIRS", $groups) && array_key_exists("reponses", $groups["PAIRS"])) ? $groups["PAIRS"]["reponses"] : 0;

    $nbCo = 0;
    foreach ($groups as $key => $value) {
      if($key != "VOUS" && $key != "PAIRS"){
        $nbCo += isset($value["reponses"]) ? $value["reponses"] : 0;
      }
    }
    // if(\App::getLocale() == "fr"){
    //   $pdf->MultiCell(190,6, iconv("UTF-8", "windows-1252","Le questionnaire a été réalisé entre le ".$date_debut_text." et le ".$date_fin_text."\nOutre le bénéficiaire ".$ben->civility." ".$ben->firstname." ".$ben->lastname." et son responsable, \n".$nbCo." collaborateur(s)  et ".$nbPairs." pair(s) y ont répondu\nDans la première partie (synthèse de vos scores), seules les réponses des collaborateurs ont été prises en compte."),0,'L',0);
    // } else {
    //   $pdf->MultiCell(190,6, iconv("UTF-8", "windows-1252","The questionnaire was conducted between ".$date_debut_text." and the ".$date_fin_text."\nIn addition to the beneficiary ".$ben->civility." ".$ben->firstname." ".$ben->lastname." and his manager, \n".$nbCo." collaborater(s)  et ".$nbPairs." peer(s) answered\nIn the first part (summary of your scores), only the responses of employees have been taken into account."),0,'L',0);
    // }

  //////////////////Page 3 a 11//////////////////////////////////
    for ($i=3; $i < 12; $i++) { 
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage($i, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
    }

    /////////////// PAGE 12 //////////////////////
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(12, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
      $pdf->SetFont('RobotoCondensed-Regular','',15);
      $pdf->SetFillColor(255, 255, 255);
      $y = 62;
      foreach ($reponseText as $key => $repText) {
        //$pdf->SetXY(33, $y);
        //$pdf->Cell(15,0,"- ".iconv("UTF-8", "windows-1252",mb_convert_encoding($repText->reponse_text, "UTF-8", "UTF-8")),0,1,'L',1);
        if(!is_null($repText->reponse_text)){
          $pdf->SetXY(33, $y);
          $pdf->Multicell(325,10,"- ".iconv("UTF-8", "windows-1252",mb_convert_encoding($repText->reponse_text, "UTF-8", "UTF-8")),0,1,'L',1);
          $y = $y + 10;
        }
      }

    ///////////// PAGE 13 /////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(13, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',17);

    // GRAPHE
    $pdf->SetXY(99, 125); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(229, 229, 229);
    // cell(width,height)
    $pdf->Cell(15,0,number_format(($perTQ1TQ4),1)."%",0,1,'L',0);
    $pdf->SetXY(242, 125); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(229, 229, 229);
    $pdf->Cell(15,0,number_format(($perTQ5TQ8),1)."%",0,1,'L',0);

    // TABLEAU 2 - 3
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(21, 193); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q1->question_fr_ben)>60 ? substr($q1->question_fr_ben, 0, 60).'...' : $q1->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q1->question_en_ben)>60 ? substr($q1->question_en_ben, 0, 60).'...' : $q1->question_en_ben),0,1,'L',1);
    }

    // FREQUENCE
    $pdf->SetXY(136, 193); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax1["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(155, 193); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax1["reponse_fr"]),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(21, 207); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q2->question_fr_ben)>60 ? substr($q2->question_fr_ben, 0, 60).'...' : $q2->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q2->question_en_ben)>60 ? substr($q2->question_en_ben, 0, 60).'...' : $q2->question_en_ben),0,1,'L',1);
    }

    // FREQUENCE
    $pdf->SetXY(136, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax2["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(155, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax2["reponse_fr"]),0,1,'L',1);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(21, 221); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q3->question_fr_ben)>60 ? substr($q3->question_fr_ben, 0, 60).'...' : $q3->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q3->question_en_ben)>60 ? substr($q3->question_en_ben, 0, 60).'...' : $q3->question_en_ben),0,1,'L',1);
    }
  
    // FREQUENCE
    $pdf->SetXY(136, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax3["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(155, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax3["reponse_fr"]),0,1,'L',1);

    ////////////////////////
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(181, 193); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q4->question_fr_ben)>60 ? substr($q4->question_fr_ben, 0, 60).'...' : $q4->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q4->question_en_ben)>60 ? substr($q4->question_en_ben, 0, 60).'...' : $q4->question_en_ben),0,1,'L',1);
    }
    
    // FREQUENCE
    $pdf->SetXY(296, 193); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax4["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(315, 193); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax4["reponse_fr"]),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(181, 207); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q5->question_fr_ben)>60 ? substr($q5->question_fr_ben, 0, 60).'...' : $q5->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q5->question_en_ben)>60 ? substr($q5->question_en_ben, 0, 60).'...' : $q5->question_en_ben),0,1,'L',1);
    }

    // FREQUENCE
    $pdf->SetXY(296, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax5["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(315, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax5["reponse_fr"]),0,1,'L',1);
    
    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(181, 221); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q6->question_fr_ben)>60 ? substr($q6->question_fr_ben, 0, 60).'...' : $q6->question_fr_ben),0,1,'L',1);
    } else {
      $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",strlen($q6->question_en_ben)>60 ? substr($q6->question_en_ben, 0, 60).'...' : $q6->question_en_ben),0,1,'L',1);
    }
    // FREQUENCE
    $pdf->SetXY(296, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax6["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(315, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$questMax6["reponse_fr"]),0,1,'L',1);

    ////////////////////// PAGE 14 ////////////////////////////
    $pdf->addPage();
    //$pdf ->useTemplate($tplIdx, null, null, 415.6, 379.9,TRUE);
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(14, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',17);

    // BAR NUMERO 1
    $pdf->SetXY(80.7, 59); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS1N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS1N["percent"]*10)/5)/2) > 0 && ((round($perS1N["percent"]*10)/5)/2) < 85 ) ? ((round($perS1N["percent"]*10)/5)/2) :( ((round($perS1N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS1N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS1N["percent"] + 85), 59); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS1N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // BAR NUMERO 2
    $pdf->SetXY(80.7, 84.5);
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS2N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS2N["percent"]*10)/5)/2) > 0 && ((round($perS2N["percent"]*10)/5)/2) < 85 ) ? ((round($perS2N["percent"]*10)/5)/2) :( ((round($perS2N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS2N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS2N["percent"]+ 85), 84.5); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS2N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // BAR NUMERO 3
    $pdf->SetXY(80.7, 109.5);
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS3N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS3N["percent"]*10)/5)/2) > 0 && ((round($perS3N["percent"]*10)/5)/2) < 85 ) ? ((round($perS3N["percent"]*10)/5)/2) :( ((round($perS3N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS3N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS3N["percent"] + 85), 109.5); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS3N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // BAR NUMERO 4
    $pdf->SetXY(80.7, 134.5);
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS4N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS4N["percent"]*10)/5)/2) > 0 && ((round($perS4N["percent"]*10)/5)/2) < 85 ) ? ((round($perS4N["percent"]*10)/5)/2) :( ((round($perS4N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS4N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS4N["percent"] + 85), 135.5); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS4N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // BAR NUMERO 5
    $pdf->SetXY(80.7, 159.5);
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS5N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS5N["percent"]*10)/5)/2) > 0 && ((round($perS5N["percent"]*10)/5)/2) < 85 ) ? ((round($perS5N["percent"]*10)/5)/2) :( ((round($perS5N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS5N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS5N["percent"] + 85), 161.5); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS5N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    // BAR NUMERO 6
    $pdf->SetXY(80.7, 184.5);
    $pdf->SetFillColor(252,209,46);
    $pdf->Cell(((((round($perS6N["percent"]*10)/5)/2) == 0 ? 1 :( (((round($perS6N["percent"]*10)/5)/2) > 0 && ((round($perS6N["percent"]*10)/5)/2) < 85 ) ? ((round($perS6N["percent"]*10)/5)/2) :( ((round($perS6N["percent"]*10)/5)/2) > 85 ? 85 : ((round($perS6N["percent"]*10)/5)/2))))),10,"",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);    

    $pdf->SetXY(($perS6N["percent"] + 85), 186.5); // position of text1, numerical, of course, not x1 and y1
    $pdf->SetFillColor(255, 255, 225);
    $pdf->Cell(10,10, $perS6N["percent"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // TABLEAU DROITE
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(181, 76); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",mb_convert_encoding(strlen($q1->question_fr_ben)>75 ? substr($q1->question_fr_ben, 0, 75) : $q1->question_fr_ben, "UTF-8", "UTF-8")),0,1,'L',1);
      $pdf->SetXY(181, 83);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q1->question_fr_ben)>75 ? substr($q1->question_fr_ben, 75) : ""),0,1,'L',1);
    } else {
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",mb_convert_encoding(strlen($q1->question_en_ben)>75 ? substr($q1->question_en_ben, 0, 75) : $q1->question_en_ben, "UTF-8", "UTF-8")),0,1,'L',1);
      $pdf->SetXY(181, 83);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q1->question_en_ben)>75 ? substr($q1->question_en_ben, 75) : ""),0,1,'L',1);
    }
    
    // FREQUENCE
    $pdf->SetXY(310, 76); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $FMS1."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 76); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA1),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(181, 93); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "en"){
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q2->question_en_ben)>75 ? substr($q1->question_en_ben, 0, 75) : $q2->question_en_ben),0,1,'L',1);
      $pdf->SetXY(181, 100);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q2->question_en_ben)>75 ? substr($q2->question_en_ben, 75) : ""),0,1,'L',1);
    } else {
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q2->question_fr_ben)>75 ? substr($q1->question_fr_ben, 0, 75) : $q2->question_fr_ben),0,1,'L',1);
      $pdf->SetXY(181, 100);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q2->question_fr_ben)>75 ? substr($q2->question_fr_ben, 75) : ""),0,1,'L',1);
    }
    
    // FREQUENCE
    $pdf->SetXY(310, 93); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $FMS2."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 93); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA2),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(254,245,214);
    $pdf->SetXY(181, 117); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q3->question_fr_ben)>75 ? substr($q3->question_fr_ben, 0, 75) : $q3->question_fr_ben),0,1,'L',1);
    $pdf->SetXY(181, 124);
    $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q3->question_fr_ben)>75 ? substr($q3->question_fr_ben, 75) : ""),0,1,'L',1);
    
    // FREQUENCE
    $pdf->SetXY(310, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax3["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA3),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(181, 137); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q4->question_fr_ben)>75 ? substr($q4->question_fr_ben, 0, 75) : $q4->question_fr_ben),0,1,'L',1);
      $pdf->SetXY(181, 143);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q4->question_fr_ben)>75 ? substr($q4->question_fr_ben, 75) : ""),0,1,'L',1);
    } else {
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q4->question_en_ben)>75 ? substr($q4->question_en_ben, 0, 75) : $q4->question_en_ben),0,1,'L',1);
      $pdf->SetXY(181, 143);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q4->question_en_ben)>75 ? substr($q4->question_en_ben, 75) : ""),0,1,'L',1);
    } 
    
    // FREQUENCE
    $pdf->SetXY(310, 136); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax4["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 136); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA4),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(254,245,214);
    if(\App::getLocale() == "fr"){
      $pdf->SetXY(181, 156); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q5->question_fr_ben)>75 ? substr($q5->question_fr_ben, 0, 75) : $q5->question_fr_ben),0,1,'L',1);
      $pdf->SetXY(181, 162);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q5->question_fr_ben)>75 ? substr($q5->question_fr_ben, 75) : ""),0,1,'L',1);
    } else {
      $pdf->SetXY(181, 156); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q5->question_en_ben)>75 ? substr($q5->question_en_ben, 0, 75) : $q5->question_en_ben),0,1,'L',1);
      $pdf->SetXY(181, 162);
      $pdf->Cell(10,7, iconv("UTF-8", "windows-1252",strlen($q5->question_en_ben)>75 ? substr($q5->question_en_ben, 75) : ""),0,1,'L',1);
    }
    
    // FREQUENCE
    $pdf->SetXY(310, 156); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax5["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 156); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA5),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(181, 178); // position of text1, numerical, of course, not x1 and y1
    if(\App::getLocale() == "fr"){
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($q6->question_fr_ben)>75 ? substr($q6->question_fr_ben, 0, 75) : $q6->question_fr_ben),0,1,'L',1);
      $pdf->SetXY(181, 184);
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($q6->question_fr_ben)>75 ? substr($q6->question_fr_ben, 75) : ""),0,1,'L',1);
    } else {
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($q6->question_en_ben)>75 ? substr($q6->question_en_ben, 0, 75) : $q6->question_en_ben),0,1,'L',1);
      $pdf->SetXY(181, 184);
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($q6->question_en_ben)>75 ? substr($q6->question_en_ben, 75) : ""),0,1,'L',1);
    }
    
    // FREQUENCE
    $pdf->SetXY(310, 173); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $questMax6["pourcentage"]."%",0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    // REPONSE
    $pdf->SetXY(325, 173); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$fqFMA6),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Bold','',12);

    ///////////////////////////// PAGE 15 //////////////////////////////
    $pdf->addPage();
    //$pdf ->useTemplate($tplIdx, null, null, 415.6, 379.9,TRUE);
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(15, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',23);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(57, 97); // position of text1, numerical, of course, not x1 and y1
 

    $pdf->Cell(10,0, number_format((($TQ1Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(156, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ2Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(230, 68); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ7Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(287, 75); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ5Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(279, 178); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ6Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(217, 199); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ8Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(133, 187); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ3Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    $pdf->SetXY(82, 180); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, number_format((($TQ4Direct/$TTSDirect)*100),1)."%",0,1,'L',1);

    //////////////////// PAGE 16 /////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(16, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('freestyle','','freestyle.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(252,209,46);
    $pdf->SetXY(148, 47); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $perMax[7]."%",0,1,'L',1);

    $image1 = $path.'templates/img/'.$indexperf.".png";
    $pdf->Image($image1, 40, ($pdf->GetY()+39), 33.78);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetFont('freestyle','',29); 

    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(40, 222); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$text1),0,1,'L',0);

    // text mileu haut
    $pdf->SetFont('RobotoCondensed-Bold','',19); 
    $pdf->SetTextColor(34,34,34);
    $pdf->SetXY(227, 56.8); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$textH1),0,1,'L',0);

    // text haut
    $pdf->SetTextColor(66,136,206);
    $pdf->SetFont('freestyle','',35); 
    $pdf->SetXY(34, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","Le ".iconv("UTF-8", "windows-1252",strlen($textH1)>7 ? substr($textH1,0 ,7) : $textH1)),0,1,'L',0);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(42, 75);
    $pdf->SetFont('freestyle','',35);
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($textH1)>7 ? substr($textH1, 7) : ""),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);

    if($indexperf == 1 || $indexperf == 2 || $indexperf == 3 || $indexperf == 4){
      $ysuiv = 0;
      //for ($i=1; $i < 5; $i++) { 
        for ($j=1; $j < 4; $j++) {
          $quest = Question::find($sq_totDirect[$j][$indexperf]["question_id"]); 
          $next = 129 + $ysuiv;
          $pdf->SetXY(139, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
          $pdf->SetXY(270, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, $sq_totDirect[$j][$indexperf]["pourcentage"]."%",0,1,'L',1);
          $pdf->SetXY(291, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$j][$indexperf]["reponse_fr"]),0,1,'L',1);
          $ysuiv = $ysuiv + 17;    
        }
      //}
      if($indexperf == 1) {
        $quest = Question::find($sq_totDirect[3][5]["question_id"]); 
        $pdf->SetXY(139, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][5]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(291, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][5]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf == 2) {
        $quest = Question::find($sq_totDirect[3][6]["question_id"]); 
        $pdf->SetXY(139, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][6]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(291, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][6]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf == 3) {
        $quest = Question::find($sq_totDirect[3][7]["question_id"]); 
        $pdf->SetXY(139, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][7]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(291, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][7]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf == 4) {
        $quest = Question::find($sq_totDirect[3][8]["question_id"]); 
        $pdf->SetXY(139, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][8]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(291, 180); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][8]["reponse_fr"]),0,1,'L',1);
      }

      $ysuiv = 0;
      for ($j=5; $j < 7; $j++) {
        $quest = Question::find($sq_totDirect[$j][$indexperf]["question_id"]); 
        $next = 195 + $ysuiv;
        $pdf->SetXY(139, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[$j][$indexperf]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(291, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$j][$indexperf]["reponse_fr"]),0,1,'L',1);
        $ysuiv = $ysuiv + 17;    
      }

    } else if($indexperf == 5 || $indexperf == 6 || $indexperf == 7 || $indexperf == 8) {
      $ysuiv = 0;
      for ($j=1; $j < 3; $j++) {
        $quest = Question::find($sq_totDirect[$j][$indexperf]["question_id"]); 
        $next = 133 + $ysuiv;
        $pdf->SetXY(139, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[$j][$indexperf]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$j][$indexperf]["reponse_fr"]),0,1,'L',1);
        $ysuiv = $ysuiv + 16;    
      }

      if($indexperf == 5){
        $quest = Question::find($sq_totDirect[4][1]["question_id"]);
        $pdf->SetXY(139, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][1]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][1]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][5]["question_id"]);
        $pdf->SetXY(139, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][5]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][5]["reponse_fr"]),0,1,'L',1);

      } else if($indexperf == 6){
        $quest = Question::find($sq_totDirect[4][2]["question_id"]);
        $pdf->SetXY(139, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][2]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][2]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][6]["question_id"]);
        $pdf->SetXY(139, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][6]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][6]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf == 7){
        $quest = Question::find($sq_totDirect[4][3]["question_id"]);
        $pdf->SetXY(139, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][3]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][3]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][7]["question_id"]);
        $pdf->SetXY(139, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(275, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][7]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][7]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf == 8){
        $quest = Question::find($sq_totDirect[4][4]["question_id"]);
        $pdf->SetXY(139, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][4]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 165); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][4]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][8]["question_id"]);
        $pdf->SetXY(139, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][8]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, 182); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][8]["reponse_fr"]),0,1,'L',1);
      }
      $ysuiv = 0;
      for ($i=5; $i < 7; $i++) { 
        $quest = Question::find($sq_totDirect[$i][$indexperf]["question_id"]); 
        $next = 197 + $ysuiv;
        $pdf->SetXY(139, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(270, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[$i][$indexperf]["frequence"]."%",0,1,'L',1);
        $pdf->SetXY(291, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$i][$indexperf]["reponse_fr"]),0,1,'L',1);
        $ysuiv = $ysuiv + 17;
      } 

    }

    // $ysuiv = 0;
    // for ($i=1; $i < 7; $i++) { 
    //   $next = 129 + $ysuiv;
    //   $pdf->SetXY(270, $next); // position of text1, numerical, of course, not x1 and y1
    //   $pdf->Cell(10,0, ${"FMA".$i}."%",0,1,'L',1);
    //   $pdf->SetXY(291, $next); // position of text1, numerical, of course, not x1 and y1
    //   $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",${"FMR".$i}),0,1,'L',1);
    //   $ysuiv = $ysuiv + 17;

    // }

    ///////////////////////////////// PAGE 17 //////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(17, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(252,209,46);
    $pdf->SetXY(46, 49); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, $perMax[6]."%",0,1,'L',1);

    // text mileu haut
    if(\App::getLocale() == "fr"){
      $pdf->SetFont('RobotoCondensed-Bold','',19); 
      $pdf->SetFillColor(254,245,214);
      $pdf->SetTextColor(34,34,34);
      $pdf->SetXY(81, 56.8); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$textH2),0,1,'L',1);
    } else {
      $pdf->SetFont('RobotoCondensed-Bold','',19); 
      $pdf->SetFillColor(254,245,214);
      $pdf->SetTextColor(34,34,34);
      $pdf->SetXY(110, 50.8); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$textH2),0,1,'L',1);
    } 

    $pdf->SetFont('freestyle','',25);
    $pdf->SetFillColor(255,255,255);
    $image1 = $path.'templates/img/'.$indexperf2.".png";
    $pdf->Image($image1, 250, ($pdf->GetY()+40), 45.78);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(260, 212); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$text2),0,1,'L',1);

    // text haut

    $pdf->SetFont('freestyle','',35); 
    $pdf->SetXY(255, 72); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","Le ".(strlen($textH2) > 7 ? substr($textH2,0 ,7) : $textH2)),0,1,'L',0);
    $pdf->SetXY(266, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",(strlen($textH2) > 7 ? substr($textH2,7) : $textH2)),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);

    if($indexperf2 == 1 || $indexperf2 == 2 || $indexperf2 == 3 || $indexperf2 == 4){
      $ysuiv = 0;
      //for ($i=1; $i < 5; $i++) { 
        for ($j=1; $j < 4; $j++) {
          $quest = Question::find($sq_totDirect[$j][$indexperf2]["question_id"]); 
          $next = 122 + $ysuiv;
          $pdf->SetXY(18, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
          $pdf->SetXY(166, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, $sq_totDirect[$j][$indexperf]["pourcentage"]."%",0,1,'L',1);
          $pdf->SetXY(182, $next); // position of text1, numerical, of course, not x1 and y1
          $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$j][$indexperf2]["reponse_fr"]),0,1,'L',1);
          $ysuiv = $ysuiv + 13;    
        }
      //}
      if($indexperf2 == 1) {
        $quest = Question::find($sq_totDirect[3][5]["question_id"]); 
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][5]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][5]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf2 == 2) {
        $quest = Question::find($sq_totDirect[3][6]["question_id"]); 
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][6]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][6]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf2 == 3) {
        $quest = Question::find($sq_totDirect[3][7]["question_id"]); 
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][7]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][7]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf2 == 4) {
        $quest = Question::find($sq_totDirect[3][8]["question_id"]); 
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[3][8]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[3][8]["reponse_fr"]),0,1,'L',1);
      }

        $quest = Question::find($sq_totDirect[5][$indexperf2]["question_id"]); 
        $pdf->SetXY(18, 174); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',0);
        $pdf->SetXY(166, 174); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[5][$indexperf2]["pourcentage"]."%",0,1,'L',0);
        $pdf->SetXY(182, 174); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[5][$indexperf2]["reponse_fr"]),0,1,'L',0);
       
        $quest = Question::find($sq_totDirect[6][$indexperf2]["question_id"]); 
        $pdf->SetXY(18, 186); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',0);
        $pdf->SetXY(166, 186); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[6][$indexperf2]["pourcentage"]."%",0,1,'L',0);
        $pdf->SetXY(182, 186); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[6][$indexperf2]["reponse_fr"]),0,1,'L',0);
       
    } else if($indexperf2 == 5 || $indexperf2 == 6 || $indexperf2 == 7 || $indexperf2 == 8) {
      $ysuiv = 0;
      for ($j=1; $j < 3; $j++) {
        $quest = Question::find($sq_totDirect[$j][$indexperf2]["question_id"]); 
        $next = 122 + $ysuiv;
        $pdf->SetXY(18, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[$j][$indexperf2]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$j][$indexperf2]["reponse_fr"]),0,1,'L',1);
        $ysuiv = $ysuiv + 13;    
      }

      if($indexperf2 == 5){
        $quest = Question::find($sq_totDirect[4][1]["question_id"]);
        $pdf->SetXY(18, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][1]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][1]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][5]["question_id"]);
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][5]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][5]["reponse_fr"]),0,1,'L',1);

      } else if($indexperf2 == 6){
        $quest = Question::find($sq_totDirect[4][2]["question_id"]);
        $pdf->SetXY(18, 148); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 148); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][2]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 148); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][2]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][6]["question_id"]);
        $pdf->SetXY(18, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][6]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 162); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][6]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf2 == 7){
        $quest = Question::find($sq_totDirect[4][3]["question_id"]);
        $pdf->SetXY(18, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][3]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][3]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][7]["question_id"]);
        $pdf->SetXY(18, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][7]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][7]["reponse_fr"]),0,1,'L',1);
      } else if($indexperf2 == 8){
        $quest = Question::find($sq_totDirect[4][4]["question_id"]);
        $pdf->SetXY(18, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][4]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][4]["reponse_fr"]),0,1,'L',1);

        $quest = Question::find($sq_totDirect[4][8]["question_id"]);
        $pdf->SetXY(18, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[4][8]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, 149); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[4][8]["reponse_fr"]),0,1,'L',1);
      }
      $ysuiv = 0;
      for ($i=5; $i < 7; $i++) { 
        $quest = Question::find($sq_totDirect[$i][$indexperf2]["question_id"]); 
        $next = 175 + $ysuiv;
        $pdf->SetXY(18, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",strlen($quest->question_fr_ben)>75 ? substr($quest->question_fr_ben,0 ,75)."..." : $quest->question_fr_ben),0,1,'L',1);
        $pdf->SetXY(166, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, $sq_totDirect[$i][$indexperf2]["pourcentage"]."%",0,1,'L',1);
        $pdf->SetXY(182, $next); // position of text1, numerical, of course, not x1 and y1
        $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$sq_totDirect[$i][$indexperf2]["reponse_fr"]),0,1,'L',1);
        $ysuiv = $ysuiv + 12;
      } 

    } 


    /////////////////// PAGE 18 ///////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(18, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    $pdf->SetXY(40, 65);
    $data = [];
    //graphe 1
    ////vigilence
    $val1 = ($perPP1Direct*180)/100;
    $val2 = ($perPM1Direct*180)/100;

    $data['pp1'] = $val1;
    $data['pm1'] = $val2;
    $data['vide1'] = 90 - $val1 - $val2;


    ////depassement
    $val1 = ($perPP2Direct*180)/100;
    $val2 = ($perPM2Direct*180)/100;

    $data['vide2'] = 90 - $val1 - $val2;
    $data['pm2'] = $val2;
    $data['pp2'] = $val1;
    


    ////creativite
    $val1 = ($perPP3Direct*180)/100;
    $val2 = ($perPM3Direct*180)/100;

    $data['pp3'] = $val1;
    $data['pm3'] = $val2;
    $data['vide3'] = 90 - $val1 - $val2;


    ////implication
    $val1 = ($perPP4Direct*180)/100;
    $val2 = ($perPM4Direct*180)/100;

    $data['vide4'] = 90 - $val1 - $val2;
    $data['pm4'] = $val2;
    $data['pp4'] = $val1;
    


    //graphe 2
    ////reconnaissance  
    $val1 = ($perPP5Direct*180)/100;
    $val2 = ($perPM5Direct*180)/100;

    $data2['pp5'] = $val1;
    $data2['pm5'] = $val2;
    $data2['vide5'] = 90 - $val1 - $val2;


    ////combativite
    $val1 = ($perPP6Direct*180)/100;
    $val2 = ($perPM6Direct*180)/100;

    $data2['vide6'] = 90 - $val1 - $val2;
    $data2['pm6'] = $val2;
    $data2['pp6'] = $val1;
    


    ////collaboration
    $val1 = ($perPP7Direct*180)/100;
    $val2 = ($perPM7Direct*180)/100;

    $data2['pp7'] = $val1;
    $data2['pm7'] = $val2;
    $data2['vide7'] = 90 - $val1 - $val2;


    ////convivialite
    $val1 = ($perPP8Direct*180)/100;
    $val2 = ($perPM8Direct*180)/100;

    $data2['vide8'] = 90 - $val1 - $val2;
    $data2['pm8'] = $val2;
    $data2['pp8'] = $val1;
    
   

    // var_dump($data);
    // die;
    // $data = array('Men' => 1510, 'Women' => 1610, 'Children' => 1400);
    $color= [];
    $color[]=array(77, 182, 172);
    $color[]=array(178, 223, 219);
    $color[]=array(242, 242, 242);
    $color[]=array(242, 242, 242);
    $color[]=array(178, 223, 219);
    $color[]=array(77, 182, 172);

    $color[]=array(255, 213, 79);
    $color[]=array(255, 236, 179);
    $color[]=array(242, 242, 242);
    $color[]=array(242, 242, 242);
    $color[]=array(255, 236, 179);
    $color[]=array(255, 213, 79);

    $color2= [];
    $color2[] = array(255, 138, 128);
    $color2[] = array(255, 205, 210);
    $color2[] = array(242, 242, 242);
    $color2[] = array(242, 242, 242);
    $color2[] = array(255, 205, 210);
    $color2[] = array(255, 138, 128);

    $color2[] = array(129, 199, 132);
    $color2[] = array(200, 230, 201);
    $color2[] = array(242, 242, 242);
    $color2[] = array(242, 242, 242);
    $color2[] = array(200, 230, 201);
    $color2[] = array(129, 199, 132);


   
    $pdf->PieChart(1000, 110, $data, '%l (%p)', $color);

    $pdf->SetXY(190, 65);

    $pdf->PieChart(1000, 110, $data2, '%l (%p)', $color2);

    $pdf->Image($path.'/templates/img/circle.png',58,82, 75);
    $pdf->Image($path.'/templates/img/circle.png',208,82, 75);


    // text percu
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);

    $x = 20;
    $y = 110;
    $pdf->SetXY($x, $y-10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." +"),0,1,'L',0);
    $pdf->SetXY($x, $y-10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP1Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x+5+10, $y-35); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x+5+10, $y-35+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM1Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x+5+127, $y-10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x+5+127, $y-10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP2Direct)."%"),0,1,'L',0);

    $pdf->SetXY($x+127-10, $y-35); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x+127-10, $y-35 +5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM2Direct)."%"),0,1,'L',0);


    $x = 20;
    $y = 160;
    $pdf->SetXY($x+10+5, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." -"),0,1,'L',0);
    $pdf->SetXY($x+10+5, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM4Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x, $y-35+10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x, $y-35+10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP4Direct)."%"),0,1,'L',0);



    $pdf->SetXY($x-10+127, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x-10+127, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM3Direct)."%"),0,1,'L',0);

    $pdf->SetXY($x+5+127, $y-35+10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x+5+127, $y-35+10 +5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP3Direct)."%"),0,1,'L',0);





    $x = 175;
    $y = 110;
    $pdf->SetXY($x, $y-10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." +"),0,1,'L',0);
    $pdf->SetXY($x, $y-10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP5Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x+5+10, $y-35); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x+5+10, $y-35+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM5Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x+5+127, $y-10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x+5+127, $y-10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP6Direct)."%"),0,1,'L',0);

    $pdf->SetXY($x+127-10, $y-35); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x+127-10, $y-35 +5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM6Direct)."%"),0,1,'L',0);


    $x = 175;
    $y = 160;
    $pdf->SetXY($x+10+5, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." -"),0,1,'L',0);
    $pdf->SetXY($x+10+5, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM8Direct)."%"),0,1,'L',0);


    $pdf->SetXY($x, $y-35+10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x, $y-35+10+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP8Direct)."%"),0,1,'L',0);



    $pdf->SetXY($x-10+127, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." - "),0,1,'L',0);
    $pdf->SetXY($x-10+127, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPM7Direct)."%"),0,1,'L',0);

    $pdf->SetXY($x+5+127, $y-35+10); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? 'Perçu' : 'Perceived'." + "),0,1,'L',0);
    $pdf->SetXY($x+5+127, $y-35+10 +5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP7Direct)."%"),0,1,'L',0);

    //text interieur
    $x = 65;
    $y = 100;
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Vigilence" : "Vigilance"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP1Direct + $perPM1Direct)."%"),0,1,'L',0);

    $x += 35;
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Dépassement" : "Overtaking"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP2Direct + $perPM2Direct)."%"),0,1,'L',0);

    $x = 65;
    $y = 135;
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Implication" : 'Involvement'),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP3Direct + $perPM3Direct)."%"),0,1,'L',0);

    $x += 35;
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Créativité" : "Creativity"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP4Direct + $perPM4Direct)."%"),0,1,'L',0);



    $x = 215;
    $y = 100;
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Reconnaissance" : "Recognition"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP6Direct + $perPM6Direct)."%"),0,1,'L',0);

    $x += 35;
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Combativité" : "Fighting spirit"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP5Direct + $perPM5Direct)."%"),0,1,'L',0);

    $x = 215;
    $y = 135;
    $pdf->SetFont('RobotoCondensed-Bold','',12);
    $pdf->SetFillColor(254,245,214);
    $pdf->SetTextColor(34,34,34);
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Convivialité" : "Usability"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP8Direct + $perPM8Direct)."%"),0,1,'L',0);

    $x += 35;
    $pdf->SetXY($x, $y); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",\App::getLocale() == "fr" ? "Collaboration" : "Collaboration"),0,1,'L',0);
    $pdf->SetXY($x, $y+5); 
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","".round($perPP7Direct + $perPM7Direct)."%"),0,1,'L',0);


    //cilcle  percentage
    $x = 88;
    $y = 115;
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    
    $pdf->SetXY($x, $y); 
    $pdf->SetFillColor(191, 191, 191);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format(($perPP1Direct + $perPM1Direct +$perPP2Direct + $perPM2Direct +$perPP3Direct + $perPM3Direct +$perPP4Direct + $perPM4Direct),1)."%"),0,1,'C',true);

    $x = 238;
    $y = 115;
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    
    $pdf->SetXY($x, $y); 
    $pdf->SetFillColor(191, 191, 191);
    $pdf->SetTextColor(255,255,255);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format(($perPP5Direct + $perPM5Direct +$perPP6Direct + $perPM6Direct +$perPP7Direct + $perPM7Direct +$perPP8Direct + $perPM8Direct),1)."%"),0,1,'C',true);


    //tableau 11
    // general
    $x = 72;
    $y = 233;
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    
    $pdf->SetXY($x, $y); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format($perTPPS7S8Direct,1)."%"),0,1,'C',0);

    $pdf->SetXY($x, $y+12); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format($perTPMS7S8Direct,1)."%"),0,1,'C',0);

    //situation normal
    $x = 102;
    $y = 233;
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    
    $pdf->SetXY($x, $y); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format(($perTPPS7S8Direct - $perTPPS7S8Direct),1)."%"),0,1,'C',0);

    $pdf->SetXY($x, $y+12); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format(($perTPMS7S8Direct - $perTPMS7S8Direct),1)."%"),0,1,'C',0);
  
    
    //situation sous stress
    $x = 128;
    $y = 233;
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    
    $pdf->SetXY($x, $y); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format($perTPPS7S8Direct,1)."%"),0,1,'C',0);

    $pdf->SetXY($x, $y+12); 
    $pdf->SetTextColor(34,34,34);
    $pdf->Cell(15,10, iconv("UTF-8", "windows-1252","".number_format($perTPMS7S8Direct,1)."%"),0,1,'C',0);


    //////////////////////// PAGE 19 ////////////////////
      $pdf->addPage("L");

      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(19, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);

    //////////////////////// PAGE 20 ////////////////////
      $pdf->addPage("L");

      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(20, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
      $pdf->SetFont('RobotoCondensed-Regular','',15);
      $pdf->SetFillColor(255, 255, 255);
      $y = 62;
      foreach ($reponseText2 as $key => $repText) {
        $pdf->SetXY(33, $y);
        if(!is_null($repText->reponse_text)){
          $pdf->Multicell(325,10,"- ".iconv("UTF-8", "windows-1252",mb_convert_encoding($repText->reponse_text, "UTF-8", "UTF-8")),0,1,'L',1);
          $y = $y + 10;
        }
      }

    //////////////////////// PAGE 21 ////////////////////
      $pdf->addPage("L");
      $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
      $pageId = $pdf->importPage(21, PdfReader\PageBoundaries::MEDIA_BOX);
      $pdf->useImportedPage($pageId);
  
    /////////////////// PAGE 22 ///////////////////////////////////////

    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(22, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$pertq1Direct."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP1Direct+$perPM1Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP1Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM1Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 44); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ1Vous."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 51); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$pertq1Pairs."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 58); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ1Autres."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Vous+$perPM1Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 89); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Pairs+$perPM1Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 96); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Autres+$perPM1Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 120); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 134); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 158); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM1Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 165); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM1Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Autres)."%"),0,1,'L',0);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(54, 243); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1+$perPM1)."%"),0,1,'L',0);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(26, 231); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP1Direct)."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(36, 217); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM1Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][1]["pourcentage"])."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][2]["pourcentage"])."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][1]["reponse_fr"])),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][2]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][1]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][2]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][1]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][2]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][1]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][2]["reponse_fr"])),0,1,'L',1);

    //////////////////////////// PAGE 23 ///////////////////////////////////////

    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(23, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(174, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][1]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][1]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 65); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][1]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][1]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][1]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][1]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 99); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][5]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 99); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][5]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 99); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][5]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 99); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][5]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 99); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][5]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][1]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][1]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 116); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][1]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][1]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][1]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][1]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 150); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][1]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 150); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 150); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][1]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 150); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][1]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 150); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][1]["reponse_fr"])),0,1,'L',0);


    ///////////////////////// PAGE 24 ////////////////////////////////////////

    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(24, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 43); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ2Direct."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 83); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP2Direct+$perPM2Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 120); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP2Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(66,136,206);
    $pdf->SetXY(24, 156); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM2Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 42); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ2Vous."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 48); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ2Pairs."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 56); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ2Autres."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Vous+$perPM2Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 86); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Pairs+$perPM2Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 93); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Autres+$perPM2Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 156); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM2Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(66,136,206);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM2Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 169); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Autres)."%"),0,1,'L',0);

    // LEGEND SUR LE BAS DROITE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(39, 233); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Direct+$perPM2Direct)."%"),0,1,'L',0);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(74, 231); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP2Direct)."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(71, 220); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM2Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][3]["pourcentage"])."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][4]["pourcentage"])."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][4]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][4]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][4]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][4]["reponse_fr"])),0,1,'L',0);

    ///////////////////////////////// PAGE 25 ////////////////////////////

    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(25, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(174, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 106); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 106); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 106); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 106); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 106); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 124); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][2]["reponse_fr"])),0,1,'L',0);

    //////////////////// PAGE 26 /////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(26, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ3Direct."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP3Direct+$perPM3Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP3Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM3Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(202,140,33);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 43); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ3Vous."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 50); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ3Pairs."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 57); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ3Autres."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 81); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Vous+$perPM3Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Pairs+$perPM3Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 95); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Autres+$perPM3Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 119); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 157); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM3Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 164); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM3Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 171); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Autres)."%"),0,1,'L',1);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(54, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Direct+$perPM3Direct)."%"),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(33, 227); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP3Direct)."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(27, 214); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM3Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][7]["pourcentage"])."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][8]["pourcentage"])."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][7]["reponse_fr"])),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][8]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][7]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][8]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][7]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][8]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][7]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][8]["reponse_fr"])),0,1,'L',1);

    //////////////////////// PAGE 27 ///////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(27, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(174, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 74); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 92); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 92); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 92); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 92); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 92); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 114); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 114); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 114); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 114); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 114); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 141); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 141); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 141); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 141); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 141); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 161); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 179); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][2]["reponse_fr"])),0,1,'L',0);


    ///////////////////// PAGE 28 ////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(28, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ4Direct."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP4Direct+$perPM4Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP4Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(202,140,33);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM4Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(202,140,33);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 43); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ4Vous."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 50); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ4Pairs."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 57); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ4Autres."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 81); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Vous+$perPM4Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Pairs+$perPM4Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 95); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Autres+$perPM4Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 119); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 157); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM4Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 164); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM4Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 171); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Autres)."%"),0,1,'L',1);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(40, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Direct+$perPM4Direct)."%"),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(69, 207); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP4Direct)."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(66, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM4Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][5]["pourcentage"])."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][6]["pourcentage"])."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][5]["reponse_fr"])),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][6]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][5]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][6]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][5]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][6]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][5]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][6]["reponse_fr"])),0,1,'L',1); 

    /////////////////////// PAGE 29 ///////////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(29, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(174, 64); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][4]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 64); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][4]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 64); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][4]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 64); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][4]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 64); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][4]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 104); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 104); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 104); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 104); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 104); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 131); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 151); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 151); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 151); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 151); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 151); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(198, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 172); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][3]["reponse_fr"])),0,1,'L',0);

    /////////////////////// PAGE 30 /////////////////////
    $pdf->addPage("L");

    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(30, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);

    ////////////////////////// PAGE 31 ////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(31, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ5Direct."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP5Direct+$perPM5Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP5Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM5Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(60,147,88);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 43); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ5Vous."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 50); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ5Pairs."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 57); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ5Autres."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 81); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Vous+$perPM5Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Pairs+$perPM5Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 95); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Autres+$perPM5Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 119); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 157); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM5Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 164); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM5Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 171); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Autres)."%"),0,1,'L',0);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(54, 194); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Direct+$perPM5Direct)."%"),0,1,'L',0);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(33, 217); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP5Direct)."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(27, 203); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM5Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][15]["pourcentage"])."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][16]["pourcentage"])."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][15]["reponse_fr"])),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][16]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][15]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][16]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][15]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][16]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][15]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][16]["reponse_fr"])),0,1,'L',0);

    ////////////////////////////////// PAGE 32 ///////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(32, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(174, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][4]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][4]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][4]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][4]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 79); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][4]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 97); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 97); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[3][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 97); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 97); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 97); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 118); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 143); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(174, 184); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 184); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(242, 184); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(283, 184); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(313, 184); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][3]["reponse_fr"])),0,1,'L',0);

    /////////////////////////// PAGE 33 //////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(33, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',29);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ6Direct."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP6Direct+$perPM6Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP6Direct)."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(60,147,88);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM6Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(60,147,88);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 43); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ6Vous."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 50); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ6Pairs."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 57); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ6Autres."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 81); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Vous+$perPM6Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 88); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Pairs+$perPM6Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 95); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Autres+$perPM6Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 119); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 133); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Autres)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 157); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM6Vous)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 164); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM6Pairs)."%"),0,1,'L',1);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 171); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Autres)."%"),0,1,'L',1);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(39, 213); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Direct+$perPM6Direct)."%"),0,1,'L',1);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(76, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP6Direct)."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(73, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM6Direct)."%"),0,1,'L',1);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][15]["pourcentage"])."%"),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][16]["pourcentage"])."%"),0,1,'L',1);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][13]["reponse_fr"])),0,1,'L',1);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][14]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][13]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][14]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][13]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][14]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][13]["reponse_fr"])),0,1,'L',1);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][14]["reponse_fr"])),0,1,'L',1);

    ////////////////////////////// PAGE 34 ////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(34, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(178, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][7]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][7]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][7]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(178, 105); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][7]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 105); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 105); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][7]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 105); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 105); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][7]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(178, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][3]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][3]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][3]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(178, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][7]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][7]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][7]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(178, 170); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][7]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 170); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 170); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][7]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 170); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 170); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][7]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(178, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][7]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(210, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(238, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][7]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(275, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][7]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][7]["reponse_fr"])),0,1,'L',1);        

    ///////////////////// PAGE 35 /////////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(35, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',35);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ7Direct."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP7Direct+$perPM7Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP7Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM7Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(202,140,33);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 44); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ7Vous."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 51); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ7Pairs."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 58); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ7Autres."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Vous+$perPM7Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 89); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Pairs+$perPM7Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 96); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Autres+$perPM7Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 120); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 134); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 155); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM7Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM7Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 169); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Autres)."%"),0,1,'L',0);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(52, 224); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Direct+$perPM7Direct)."%"),0,1,'L',0);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(30, 213); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP7Direct)."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(29, 221); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM7Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][15]["pourcentage"])."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][16]["pourcentage"])."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][11]["reponse_fr"])),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][12]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][11]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][12]["reponse_fr"])),0,1,'L',1);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][11]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][12]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][11]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][12]["reponse_fr"])),0,1,'L',0);


    ///////////////////////////////////////////// PAGE 36 /////////////////////////    
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(36, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(177, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(202, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(236, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][6]["reponse_fr"])),0,1,'L',0);    

    ////////////////////////// PAGE 37 //////////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(37, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->AddFont('RobotoCondensed-Bold','','RobotoCondensed-Bold.php');
    $pdf->AddFont('RobotoCondensed-Regular','','RobotoCondensed-Regular.php');
    $pdf->AddFont('RobotoCondensed-BoldItalic','','RobotoCondensed-BoldItalic.php');
    $pdf->AddFont('RobotoCondensed-Italic','','RobotoCondensed-Italic.php');
    $pdf->SetFont('RobotoCondensed-Bold','',35);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 45); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",$perTQ8Direct."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 84); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP8Direct+$perPM8Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 123); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPP8Direct)."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(236,111,116);
    $pdf->SetXY(24, 159); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,10, iconv("UTF-8", "windows-1252",round($perPM8Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetFillColor(202,140,33);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 44); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ8Vous."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 51); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ8Pairs."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 58); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$perTQ8Autres."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 82); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Vous+$perPM8Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 89); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Pairs+$perPM8Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 96); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Autres+$perPM8Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 120); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 127); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 134); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Autres)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(236, 155); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM8Vous)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',18);
    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(253, 162); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM8Pairs)."%"),0,1,'L',0);

    $pdf->SetTextColor(255,255,255);
    $pdf->SetXY(257, 169); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Autres)."%"),0,1,'L',0);

    // LEGEND SUR LE BAS GAUCHE
    $pdf->SetTextColor(34,34,34);
    $pdf->SetFont('RobotoCondensed-Bold','',14);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(44, 225); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Direct+$perPM8Direct)."%"),0,1,'L',0);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(76, 217); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPP8Direct)."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(74, 228); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($perPM8Direct)."%"),0,1,'L',0);

    $pdf->SetFont('RobotoCondensed-Regular','',13);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][15]["pourcentage"])."%"),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(159, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",round($sq_totDirect[8][16]["pourcentage"])."%"),0,1,'L',0);

    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][9]["reponse_fr"])),0,1,'L',0);
    $pdf->SetFillColor(255,255,255);
    $pdf->SetXY(179, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[8][10]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(209, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][9]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(209, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[8][10]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(244, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][9]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(244, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[8][10]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(275, 216); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][9]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(275, 232); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[8][10]["reponse_fr"])),0,1,'L',0);

    /////////////////////////////// PAGE 38 ///////////////////////////////////////
    $pdf->addPage();
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(38, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->SetFillColor(210,223,240);
    $pdf->SetXY(177, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[1][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 87); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 103); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][2]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[4][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][2]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][2]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 126); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][2]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[2][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 149); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[5][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 167); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][6]["reponse_fr"])),0,1,'L',0);

    $pdf->SetXY(177, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][6]["pourcentage"]."%")),0,1,'L',0);
    $pdf->SetXY(200, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totDirect[6][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(235, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][6]["reponse_fr"])),0,1,'L',0);    
    $pdf->SetXY(274, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][6]["reponse_fr"])),0,1,'L',0);
    $pdf->SetXY(310, 191); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][6]["reponse_fr"])),0,1,'L',0);

    
    /////////////////////// PAGE 39 //////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(39, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);
    $pdf->SetFont('RobotoCondensed-Regular','',15);
    $pdf->SetFillColor(255, 255, 255);
    $y = 62;
    foreach ($reponseText1 as $key => $repText) {
      if(!is_null($repText->reponse_text)){
        $pdf->SetXY(33, $y);
        $pdf->Multicell(325,10,"- ".iconv("UTF-8", "windows-1252",mb_convert_encoding($repText->reponse_text, "UTF-8", "UTF-8")),0,1,'L',1);
        $y = $y + 10;
      }
    }


    ///////////////////// PAGE 40 //////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(40, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->useImportedPage($pageId);


    ///////////////////// PAGE 41 //////////////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(41, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->useImportedPage($pageId);

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[1][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[1][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 20; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[1][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 20; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[1][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 20; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[1][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 20; 
    }


    /////////////////////////////////// PAGE 42 ///////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(42, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->useImportedPage($pageId);
    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[2][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[2][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[2][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[2][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[2][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    ///////////////////// PAGE 43 ///////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(43, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->useImportedPage($pageId);
    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[3][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[3][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[3][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[3][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[3][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }   


    ////////////////////////////// PAGE 44 ///////////////////////////////////

    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(44, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->useImportedPage($pageId);
    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[4][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[4][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[4][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[4][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 82;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[4][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }


    /////////////////////////////// PAGE 45 ////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(45, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',12);
    $pdf->useImportedPage($pageId);
    $y = 59;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[5][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[5][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 14; 
    }

    $y = 59;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[5][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 14; 
    }

    $y = 59;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[5][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 14; 
    }

    $y = 59;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[5][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 14; 
    } 


    $pdf->SetXY(97, 200); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",(round($perTPPS8)."%")),0,1,'L',0);

    $pdf->SetXY(97, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",(round($perTPMS8)."%")),0,1,'L',0);

    $pdf->SetXY(139, 200); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($ppF)]),0,1,'L',0);

    $pdf->SetXY(139, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($pmF)]),0,1,'L',0);

    $pdf->SetXY(189, 200); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($ppFVous)]),0,1,'L',0);

    $pdf->SetXY(189, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($pmFVous)]),0,1,'L',0);

    $pdf->SetXY(239, 200); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($ppFPairs)]),0,1,'L',0);

    $pdf->SetXY(239, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($pmFPairs)]),0,1,'L',0);

    $pdf->SetXY(279, 200); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($ppFAutres)]),0,1,'L',0);

    $pdf->SetXY(279, 210); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",$arrayReponses[max($pmFAutres)]),0,1,'L',0);

    $qp1 = Question::find($sq_tot[7][$qplus1]["question_id"]);
    $qp2 = Question::find($sq_tot[8][$qplus1]["question_id"]);

    $qp3 = Question::find($sq_tot[7][$qplus2+1]["question_id"]);
    $qp4 = Question::find($sq_tot[8][$qplus2]["question_id"]); 

    $pdf->SetXY(24, 245); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","- ".$qp1->question_fr_ben),0,1,'L',0);
    $pdf->SetXY(204, 245); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","- ".$qp3->question_fr_ben),0,1,'L',0);

    $pdf->SetXY(24, 252); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","- ".$qp2->question_fr_ben),0,1,'L',0);
    $pdf->SetXY(204, 252); // position of text1, numerical, of course, not x1 and y1
    $pdf->Cell(10,0, iconv("UTF-8", "windows-1252","- ".$qp4->question_fr_ben),0,1,'L',0);    

    ////////////////////////////// PAGE 46 /////////////////////////////////
    $pdf->addPage("L");
    $pdf ->useTemplate($tplIdx, null, null, $size["width"], $size["height"],TRUE);
    $pageId = $pdf->importPage(46, PdfReader\PageBoundaries::MEDIA_BOX);
    $pdf->SetFont('RobotoCondensed-Regular','',14);
    $pdf->useImportedPage($pageId);
    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(100, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[6][$i]["pourcentage"]."%")),0,1,'L',0);
      $pdf->SetXY(140, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_tot[6][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(190, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totPairs[6][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    $y = 79;
    for ($i=1; $i < 9; $i++) { 
      $pdf->SetXY(240, $y); // position of text1, numerical, of course, not x1 and y1
      $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totVous[6][$i]["reponse_fr"])),0,1,'L',0);
      $y = $y + 22; 
    }

    // $y = 79;
    // for ($i=1; $i < 9; $i++) { 
    //   $pdf->SetXY(290, $y); // position of text1, numerical, of course, not x1 and y1
    //   $pdf->Cell(10,0, iconv("UTF-8", "windows-1252",($sq_totAutres[6][$i]["reponse_fr"])),0,1,'L',0);
    //   $y = $y + 22; 
    // }

    if($genre == "pdf"){

      $pdf->Output($txtRa.".pdf","I");

    } else {

      $path = base_path().'/public/templates/acti/templates';
      $target_dir = $path."/archive/".$id;
      if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
      }
      $pdf->Output($path."/archive/pptx.pdf",'F');

      $pdf = new PDF_Diag();
      $pagecount = $pdf->setSourceFile($path."/archive/pptx.pdf"); // How many pages?
      
      // Split each page into a new PDF
      for ($i = 1; $i <= $pagecount; $i++) {
        $new_pdf = new PDF_Diag();
        $new_pdf->AddPage();
        $new_pdf->setSourceFile($path."/archive/pptx.pdf");
        $new_pdf->useTemplate($new_pdf->importPage($i), null, null, $size["width"], $size["height"],TRUE);
        
        try {
          $new_filename = $path."/archive/pptx_".$i.".pdf";
          $new_pdf->Output($new_filename, "F");
          
          ini_set('max_execution_time', 400);
          $array=array();
          $actuel_path = $path."/archive/pptx.pdf";
          //exec("convert -density 200 $actuel_path -quality 60 $target_dir2/ppt.jpg", $array);
          $count = $i-1;
          exec("convert -density 200 ".$new_filename." -quality 60 ".$path."/archive/liste_images_".$id."/ppt-".$count.".jpg", $array);

        } catch (Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
      }


      $target_dir2 = $path."/archive/liste_images_".$id;
      if (!file_exists($target_dir2)) {
        mkdir($target_dir2, 0777, true);
      }

      copy($path.'/Maquette_360_FB_FINAL2.pptx',$path."/archive/actiPPTX.zip");
      $filename = $path."/archive/actiPPTX.zip";
      $zip = new ZipArchive;
      $res = $zip->open($filename);
      if ($res === TRUE) {
        // Unzip path
        $newpath = $path."/archive/".$id;
        // Extract file
        $zip->extractTo($newpath);
        $zip->close();

        // ini_set('max_execution_time', 400);
        // $array=array();
        // $actuel_path = $path."/archive/pptx.pdf";
        // //exec("convert -density 200 $actuel_path -quality 60 $target_dir2/ppt.jpg", $array);
        // exec("convert -density 200 ".$path."templates/archive/pptx.pdf -quality 60 ".$path."templates/archive/liste_images/ppt.jpg", $array);
        /////////////// ALL - SLIDES  ////////////////////////////////////////////

        $count = 3;
        for ($i=0; $i < 46; $i++) { 
          copy($path."/archive/liste_images_".$id."/ppt-".$i.".jpg",$path."/archive/".$id."/ppt/media/image".$count.".png");
          $count++; 
        }

        //////////////////////////////////////////////////////////////////////
        $response = ["message" => "Une erreur est survenu lors de l'exportation", "generated" => false];
        header('Content-Type: text/html; charset=ISO-8859-1');
        $this->zipFolder($zip,$newpath,$path."/archive/".$id."/");
        if(!is_dir(base_path()."/public/uploads/acti/".$id."/pptx") == true){
          mkdir(base_path()."/public/uploads/acti/".$id."/pptx",0777,true);   
        }
        $file_url = url("/uploads/acti/".$id."/pptx");
        $dir = base_path()."/public/uploads/acti/".$id."/pptx/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");
        copy($path."/archive/".$id.'/file.zip', $dir);
        if(file_exists($dir)){
          $this->deleteFolder($path."/archive");
          copy($path.'/backup.pptx',$path.'/Maquette_360_FB_FINAL2.pptx');
          $response["generated"] = true;
          $response["doc"] = $file_url."/".iconv('ISO-8859-1', 'UTF-8//IGNORE', $txtRa.".pptx");
          //return response()->json($response);
          //echo json_encode(array("status" => 0, "message" => "Aucun paramètre reçu"));
          echo json_encode($response);
        }
      } 

    }

    // $pdf->Output("pptx.pdf",'F');
    // ini_set('max_execution_time', 300);
    // $array=array();
    // $actuel_path = base_path()."/public/pptx.pdf";
    // $output = base_path()."/public/ici";   
    // exec("convert -density 200 $actuel_path -quality 60 $output/foo-%03d.jpg", $array);

  }

  public function deleteFolder($path)
  {
    if (is_dir($path) === true)
    {
        $files = array_diff(scandir($path), array('.', '..'));

        foreach ($files as $file)
        {
            $this->deleteFolder(realpath($path) . '/' . $file);
        }

        return rmdir($path);
    }

    else if (is_file($path) === true)
    {
        return unlink($path);
    }

    return false;
  }

  public function zipFolder($zip,$path,$path2){
    $rootPath = realpath($path);

    // Initialize archive object
    $zip->open($path2.'file.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($rootPath),
        \RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

    // Zip archive will be created only after closing object
    $zip->close();
  }

  function frequence($fma){

    $fq = ( $fma > 0 && $fma < 2 ? "T.rarement":( $fma > 1 && $fma < 3 ? "Rarement" :( $fma > 2 && $fma < 4 ? "Parfois" :( $fma > 3 && $fma < 5 ? "Souvent" :( $fma > 4 && $fma < 6 ? "Très souvent" : "")))));

    return $fq;
  }

  function maxIndex($array){
    $max = 0;
    $res = 0;
    foreach ($array as $key => $value) {
      if($max < $value){
        $max = $value;
        $res = $key;

      }
    }
    return strval($res);
  }

}




class PDF_Sector extends Fpdi
{
    function Sector($xc, $yc, $r, $a, $b, $style='FD', $cw=true, $o=90)
    {
        $d0 = $a - $b;
        if($cw){
            $d = $b;
            $b = $o - $a;
            $a = $o - $d;
        }else{
            $b += $o;
            $a += $o;
        }
        while($a<0)
            $a += 360;
        while($a>360)
            $a -= 360;
        while($b<0)
            $b += 360;
        while($b>360)
            $b -= 360;
        if ($a > $b)
            $b += 360;
        $b = $b/360*2*M_PI;
        $a = $a/360*2*M_PI;
        $d = $b - $a;
        if ($d == 0 && $d0 != 0)
            $d = 2*M_PI;
        $k = $this->k;
        $hp = $this->h;
        if (sin($d/2))
            $MyArc = 4/3*(1-cos($d/2))/sin($d/2)*$r;
        else
            $MyArc = 0;
        //first put the center
        $this->_out(sprintf('%.2F %.2F m',($xc)*$k,($hp-$yc)*$k));
        //put the first point
        $this->_out(sprintf('%.2F %.2F l',($xc+$r*cos($a))*$k,(($hp-($yc-$r*sin($a)))*$k)));
        //draw the arc
        if ($d < M_PI/2){
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }else{
            $b = $a + $d/4;
            $MyArc = 4/3*(1-cos($d/8))/sin($d/8)*$r;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
            $a = $b;
            $b = $a + $d/4;
            $this->_Arc($xc+$r*cos($a)+$MyArc*cos(M_PI/2+$a),
                        $yc-$r*sin($a)-$MyArc*sin(M_PI/2+$a),
                        $xc+$r*cos($b)+$MyArc*cos($b-M_PI/2),
                        $yc-$r*sin($b)-$MyArc*sin($b-M_PI/2),
                        $xc+$r*cos($b),
                        $yc-$r*sin($b)
                        );
        }
        //terminate drawing
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='b';
        else
            $op='s';
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3 )
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            $x1*$this->k,
            ($h-$y1)*$this->k,
            $x2*$this->k,
            ($h-$y2)*$this->k,
            $x3*$this->k,
            ($h-$y3)*$this->k));
    }
}

class PDF_Diag extends PDF_Sector {
    var $legends;
    var $wLegend;
    var $sum;
    var $NbVal;

    function PieChart($w, $h, $data, $format, $colors=null)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $hLegend = 5;
        $radius = min($w - $margin * 4 - $hLegend - $this->wLegend, $h - $margin * 2);
        $radius = floor($radius / 2);
        $XDiag = $XPage + $margin + $radius;
        $YDiag = $YPage + $margin + $radius;
        if($colors == null) {
            for($i = 0; $i < $this->NbVal; $i++) {
                $gray = $i * round(255 / $this->NbVal);
                $colors[$i] = array($gray,$gray,$gray);
            }
        }

        //Sectors
        $this->SetLineWidth(0.2);
        $angleStart = -90;
        $angleEnd = 270;
        $i = 0;
        foreach($data as $val) {
            $angle = ($val * 360) / doubleval($this->sum);
            if ($angle != 0) {
                $angleEnd = $angleStart + $angle;
                $this->SetDrawColor(255, 255, 255);
                $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
                $this->Sector($XDiag, $YDiag, $radius, $angleStart, $angleEnd);
                $angleStart += $angle;
            }
            $i++;
        }

        //Legends
        // $this->SetFont('Courier', '', 10);
        // $x1 = $XPage + 2 * $radius + 4 * $margin;
        // $x2 = $x1 + $hLegend + $margin;
        // $y1 = $YDiag - $radius + (2 * $radius - $this->NbVal*($hLegend + $margin)) / 2;
        // for($i=0; $i<$this->NbVal; $i++) {
        //     $this->SetFillColor($colors[$i][0],$colors[$i][1],$colors[$i][2]);
        //     $this->Rect($x1, $y1, $hLegend, $hLegend, 'DF');
        //     $this->SetXY($x2,$y1);
        //     $this->Cell(0,$hLegend,$this->legends[$i]);
        //     $y1+=$hLegend + $margin;
        // }
    }

    function BarDiagram($w, $h, $data, $format, $color=null, $maxVal=0, $nbDiv=4)
    {
        $this->SetFont('Courier', '', 10);
        $this->SetLegends($data,$format);

        $XPage = $this->GetX();
        $YPage = $this->GetY();
        $margin = 2;
        $YDiag = $YPage + $margin;
        $hDiag = floor($h - $margin * 2);
        $XDiag = $XPage + $margin * 2 + $this->wLegend;
        $lDiag = floor($w - $margin * 3 - $this->wLegend);
        if($color == null)
            $color=array(155,155,155);
        if ($maxVal == 0) {
            $maxVal = max($data);
        }
        $valIndRepere = ceil($maxVal / $nbDiv);
        $maxVal = $valIndRepere * $nbDiv;
        $lRepere = floor($lDiag / $nbDiv);
        $lDiag = $lRepere * $nbDiv;
        $unit = $lDiag / $maxVal;
        $hBar = floor($hDiag / ($this->NbVal + 1));
        $hDiag = $hBar * ($this->NbVal + 1);
        $eBaton = floor($hBar * 80 / 100);

        $this->SetLineWidth(0.2);
        $this->Rect($XDiag, $YDiag, $lDiag, $hDiag);

        $this->SetFont('Courier', '', 10);
        $this->SetFillColor($color[0],$color[1],$color[2]);
        $i=0;
        foreach($data as $val) {
            //Bar
            $xval = $XDiag;
            $lval = (int)($val * $unit);
            $yval = $YDiag + ($i + 1) * $hBar - $eBaton / 2;
            $hval = $eBaton;
            $this->Rect($xval, $yval, $lval, $hval, 'DF');
            //Legend
            $this->SetXY(0, $yval);
            $this->Cell($xval - $margin, $hval, $this->legends[$i],0,0,'R');
            $i++;
        }

        //Scales
        for ($i = 0; $i <= $nbDiv; $i++) {
            $xpos = $XDiag + $lRepere * $i;
            $this->Line($xpos, $YDiag, $xpos, $YDiag + $hDiag);
            $val = $i * $valIndRepere;
            $xpos = $XDiag + $lRepere * $i - $this->GetStringWidth($val) / 2;
            $ypos = $YDiag + $hDiag - $margin;
            $this->Text($xpos, $ypos, $val);
        }
    }

    function SetLegends($data, $format)
    {
        $this->legends=array();
        $this->wLegend=0;
        $this->sum=array_sum($data);
        $this->NbVal=count($data);
        foreach($data as $l=>$val)
        {
            $p=sprintf('%.2f',$val/$this->sum*100).'%';
            $legend=str_replace(array('%l','%v','%p'),array($l,$val,$p),$format);
            $this->legends[]=$legend;
            $this->wLegend=max($this->GetStringWidth($legend),$this->wLegend);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Type;
use App\Question;
use App\User;
use App\Page;
use App\ReponseRepondant;
use App\TestRepondant;
use App\Terminer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class ActiFrontController extends Controller
{
    use AuthenticatesUsers;

    public function index()
    {
        $question["acti"] = Type::where('name', "acti")->first();
        $actis = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%acti%");
        })->get();

        return view("acti/front/index", ["actis" => $actis, "questions" => $question, "role" => Auth::user()->type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }    

    public function questions_rep($id, $code, $idpage=null, $num=null, $idUser = null){

        $credentials =[];

        if($idUser){
            $repondant = User::find($idUser);
            
            if($repondant && $repondant->code){

                $credentials["email"] = $repondant->email;
                $credentials["password"] = $repondant->code;
               

                $auth = Auth::attempt($credentials);
                
                if(!$auth){
                    return redirect()->route("acti-front");
                }
               
            }else{
                return redirect()->route("acti-front");
            }
        }

        $acti = Test::find($id);
        $fb = Type::where('name', "acti")->first();
        $type = Type::find($acti->type_id);

        $type_rep = TestRepondant::where("test_id","=",$id)->where("repondant_id","=",Auth::user()->id)->first();

        $count = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->count();
        //$count = \DB::table('terminer')->select('id','num_page','progression','page_id','repondant_id')->count();
        $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->select('id','num_page','progression','page_id','repondant_id')->first();

        $firstpage = false;
        if($code == $acti->code){

            if(empty($count)){

                $numero = 1;
                $page = Type::where('name', $type->name)->first()->pages[0];
                $data = ["id" => $id, "code" => $code, "idpage" => $page->id+1];
                $style = "none";
                $col = "col-md-12";
                $firstpage = true;

            } else {

                $data = ["id" => $id, "code" => $code, "idpage" => ($terminer->page_id+1)];
                $page = Page::find($terminer->page_id);
                $numero = $terminer->num_page;
                $style = "block";
                $col = "col-md-6";
            }

            //if(Auth::user()->type == "beneficiaire"){
                return view("acti/front/questions_rep", ["acti" => $acti, "pages" => $page, "lang" => "fr", "data" => $data, "numero" => $numero, "terminer_id"=> empty($terminer->id) ? 0 : $terminer->id, "progression" => empty($terminer->progression) ? 0 : $terminer->progression, "style" => $style, "col"=> $col, "test" => $acti, "firstpage" => $firstpage, "type_rep"=>$type_rep]);
            /*} else {
                return view("acti/front/questions_rep", ["acti" => $acti, "pages" => $page, "lang" => \App::getLocale(), "data" => $data, "numero" => $numero, "terminer_id"=> empty($terminer->id) ? 0 : $terminer->id, "progression" => empty($terminer->progression) ? 0 : $terminer->progression, "style" => $style, "col"=> $col, "test" => $acti, "firstpage" => $firstpage]);
            }*/

        }
        return redirect()->route("acti-front");

    }

    public function saveUsers(Request $request){
        $this->validate($request, [
          'code' => 'required',
        ]);
        $actis = Test::whereHas('type', function ($query) {
            $query->where('name', 'like', "%acti%");
        })->get();
        foreach ($actis as $key => $acti) {
            $count_type = Test::where('type_id', '=' ,$acti->type_id)->count();
            if(!empty($count_type)){
                $all_codes = Test::where('type_id', '=' ,$acti->type_id)->select('id','code')->get();
                foreach ($all_codes as $key => $value) {
                    if($value->code == $request->code){
                        $test_rep = TestRepondant::where('repondant_id', '=', Auth::user()->id)->where('test_id', '=', $value->id)->count();
                        if($test_rep > 0){
                            //if(Auth::user()->role == "repondant"){
                                //var_dump("repondant");
                                return redirect()->route('acti-front-questions', ['id' => $value->id, "code" => $request->code]);
                                break;
                            /*} else {
                                var_dump("benef");
                                //return redirect()->route('acti-front-questions-beneficiaire', ['id' => $value->id, "code" => $request->code]);
                                break;
                            }*/
                        } else {
                            return redirect('/diagnostics/')->with('error_testcode', 'vous n\'êtes pas encore inscrit à ce test!');
                        }
                    }
                }
                die;
                return redirect('/diagnostics/')->with('error_code', 'Auncune test trouvée!');
            } else {
                return redirect('/diagnostics/')->with('error_code', 'Auncune test 360° Performance and Motivations!');
            }
        }
    }

    public function saveReponse(Request $request){

        $acti = Test::find($request->id);
        $type = Type::find($acti->type_id);
     
        if(isset($_POST["next"])){
            $this->validate($request, [
                'reponse_question' => "required|array|min:".$request->nbQuest,
                'reponse_question.*' => "required"
            ]);

            // save personal
            \DB::table('test_repondant')->updateOrInsert(
                ['test_id' => $request->id, 'repondant_id' => Auth::user()->id],
                ['updated_at' =>  (new \DateTime('now'))->format('Y-m-d H:i:s')]
            );

            foreach ($request->reponse_question as $key => $reponse) {
                //foreach ($reponse as $reponse_id => $value) {
                    
                    $count = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id", $request->id)->count();
                    
                    if($count == 0){
                        $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id","=",$acti->id)->updateOrInsert(
                            ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $acti->id, "reponse_id" => $reponse],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    } else {
                        $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id","=",$acti->id)->update(
                            ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $acti->id, "reponse_id" => $reponse],
                            ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                        );
                    }
                //}
            }
            $count = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->count();
            if($count == 0){
                $progress = 100/8;
                \DB::table('terminer')->insert(
                    ['num_page' => $request->numero+1, 'progression' => $progress, 'page_id' => $request->idpage, "repondant_id" => Auth::user()->id, 'test_id' => $acti->id]
                );
            } else {
                $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->select('progression')->first();
                //$prog = $request->progression == 0 ? (100/8) : $request->progression; 
                $progress = ($terminer->progression + (100/8));
                Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->update(
                    ['num_page' => $request->numero+1, 'progression' => $progress, 'page_id' => $request->idpage,'test_id'=> $acti->id]
                );
            }

            //if(Auth::user()->type == "repondant"){
            return redirect()->route('acti-front-questions', ['id' => $request->id, "code" => $request->code, "idpage" => $request->idpage, 'numero' => ($request->numero+1)]);
            /*} else {
                return redirect()->route('acti-front-questions-repondant', ['id' => $request->id, "code" => $request->code, "idpage" => $request->idpage, 'numero' => ($request->numero+1)]);
            }*/

        } else if(isset($_POST["previous"])) {
            
            $terminer = Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->select('progression')->first();
            $progress = ($terminer->progression - $request->progression);
            Terminer::where('repondant_id', '=' ,Auth::user()->id)->where('test_id','=',$acti->id)->update(
                ['num_page' => $request->numero-1, 'progression' => $progress, 'page_id' => ($request->idpage-2), 'test_id' => $acti->id]
            );

            //if(Auth::user()->role == "repondant"){
            return redirect()->route('acti-front-questions', ['id' => $request->id, "code" => $request->code, "idpage" => ($request->idpage-2), 'numero' => ($request->numero-1)]);
            /*}else{
                return redirect()->route('acti-front-questions-repondant', ['id' => $request->id, "code" => $request->code, "idpage" => ($request->idpage-2), 'numero' => ($request->numero-1)]);
            }*/

        } else {

            foreach ($request->reponse_question as $key => $reponse) {
                //foreach ($reponse as $reponse_id => $value) {
                    
                $count = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id","=",$acti->id)->count();
                
                if($count == 0){
                    $repondant_question_id = \DB::table('reponse_repondant')->updateOrInsert(
                        ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $acti->id, "reponse_text" => $reponse],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );
                } else {
                    $repondant_question_id = ReponseRepondant::where('repondant_id', '=' ,Auth::user()->id)->where("question_id", "=", $key)->where("test_id","=",$acti->id)->update(
                        ['question_id' => $key, 'repondant_id' => Auth::user()->id, "test_id" => $acti->id, "reponse_text" => $reponse],
                        ["updated_at" => (new \DateTime('now'))->format('Y-m-d H:i:s')]
                    );
                }
                //}
            }

            Terminer::where([
                    [ "repondant_id", "=", Auth::user()->id],
                ])->where('test_id','=',$acti->id)->delete();
            TestRepondant::where([
                    [ "repondant_id", "=", Auth::user()->id],
                ])->where('test_id','=',$acti->id)->update(['reponse'=> 1]);

            return redirect()->route('diagnostics')->with('success', 'Vous avez terminé le test '.$acti->name.', vos réponses ont été enregistrées correctement!');           

        }
        
    }
}

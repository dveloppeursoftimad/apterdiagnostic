<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property User[] $users
 */
class Groupe extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'groupe';

    /**
     * @var array
     */
    protected $fillable = ['name', 'abbreviation'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $groupe_id
 * @property int $test_id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $create_time
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $organisation
 * @property string $civility
 * @property string $language
 * @property string $role
 * @property string $avatar_url
 * @property string $service
 * @property string $function
 * @property string $manager
 * @property string $age
 * @property string $pays
 * @property string $email_verified_at
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Groupe $groupe
 * @property Test $test
 * @property ConsultantAccess[] $consultantAccesses
 * @property ReponseRepondant[] $reponseRepondants
 * @property Test[] $tests
 * @property TestRepondant[] $testRepondants
 */
class UserSchema extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = ['groupe_id', 'test_id', 'username', 'email', 'password', 'create_time', 'firstname', 'lastname', 'phone', 'organisation', 'civility', 'language', 'role', 'avatar_url', 'service', 'function', 'manager', 'age', 'pays', 'email_verified_at', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function groupe()
    {
        return $this->belongsTo('App\Groupe');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function consultantAccesses()
    {
        return $this->hasMany('App\ConsultantAccess', 'consultant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponseRepondants()
    {
        return $this->hasMany('App\ReponseRepondant', 'repondant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tests()
    {
        return $this->hasMany('App\Test', 'consultant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testRepondants()
    {
        return $this->hasMany('App\TestRepondant', 'repondant_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $type_id
 * @property int $consultant_id
 * @property string $name
 * @property boolean $status
 * @property string $description
 * @property string $langue
 * @property string $code
 * @property Type $type
 * @property User $user
 * @property ReponseRepondant[] $reponseRepondants
 * @property TestRepondant[] $testRepondants
 * @property User[] $users
 */
class Test extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'test';

    /**
     * @var array
     */
    protected $fillable = ['type_id', 'consultant_id', 'name', 'status', 'description', 'langue', 'code','beneficiaire_id', "template_id"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beneficiaire()
    {
        return $this->belongsTo('App\User', "beneficiaire_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consultant()
    {
        return $this->belongsTo('App\User', 'consultant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reponseRepondants()
    {
        return $this->hasMany('App\ReponseRepondant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testRepondants()
    {
        return $this->hasMany('App\TestRepondant');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User','test_repondant','test_id','repondant_id');
    }
}
